(function() {
	var callWithJQuery;

	callWithJQuery = function(pivotModule) {
		if (typeof exports === "object" && typeof module === "object") {
			return pivotModule(require("jquery"));
		} else if (typeof define === "function" && define.amd) {
			return define(["jquery"], pivotModule);
		} else {
			return pivotModule(jQuery);
		}
	};

	callWithJQuery(function($) {
		var frFmt, frFmtInt, frFmtPct, nf, tpl;
		nf = $.pivotUtilities.numberFormat;
		tpl = $.pivotUtilities.aggregatorTemplates;
		frFmt = nf({
			thousandsSep: " ",
			decimalSep: ","
		});
		frFmtInt = nf({
			digitsAfterDecimal: 0,
			thousandsSep: " ",
			decimalSep: ","
		});
		frFmtPct = nf({
			digitsAfterDecimal: 1,
			scaler: 100,
			suffix: "%",
			thousandsSep: " ",
			decimalSep: ","
		});
		return $.pivotUtilities.locales.it = {
			localeStrings: {
				renderError: "Si sono verificati errori nella visualizzazione;.",
				computeError: "Si sono verificati errori nel calcolo.",
				uiRenderError: "Si sono verificati errori nella generazione dell'interfaccia dinamica",
				selectAll: "Seleziona tutto",
				selectNone: "Elimina selezioni",
				tooMany: "(troppi valori)",
				filterResults: "Filtro dei risultati",
				totals: "Totale",
				vs: "su",
				by: "per"
			},
			aggregators: {
				"Conteggio": tpl.count(frFmtInt),
				"Conteggio di valori univoci": tpl.countUnique(frFmtInt),
				"Lista di valori univoci": tpl.listUnique(", "),
				"Somma": tpl.sum(frFmt),
				"Somma di interi": tpl.sum(frFmtInt),
				"Media": tpl.average(frFmt),
				"Valore minimo": tpl.min(frFmt),
				"Valore massimo": tpl.max(frFmt),
				"Rapporto di somme": tpl.sumOverSum(frFmt),
				"80% del simite superiore": tpl.sumOverSumBound80(true, frFmt),
				"80% del simite inferiore": tpl.sumOverSumBound80(false, frFmt),
				"Frazione della somma totale": tpl.fractionOf(tpl.sum(), "total", frFmtPct),
				"Frazione della somma di riga": tpl.fractionOf(tpl.sum(), "row", frFmtPct),
				"Frazione della somma di colonna": tpl.fractionOf(tpl.sum(), "col", frFmtPct),
				"Conteggio della somma totale": tpl.fractionOf(tpl.count(), "total", frFmtPct),
				"Conteggio della somma di riga": tpl.fractionOf(tpl.count(), "row", frFmtPct),
				"Conteggio della somma di colonna": tpl.fractionOf(tpl.count(), "col", frFmtPct)
			},
			renderers: {
				"Tabella": $.pivotUtilities.renderers["Table"],
				"Tabella a barre": $.pivotUtilities.renderers["Table Barchart"],
				"Mappa termica": $.pivotUtilities.renderers["Heatmap"],
				"Mappa termica per riga": $.pivotUtilities.renderers["Row Heatmap"],
				"Mappa termica per colonna": $.pivotUtilities.renderers["Col Heatmap"]
			},
			derivers: $.pivotUtilities.derivers
		};
	});

}).call(this);

//# sourceMappingURL=pivot.fr.js.map
