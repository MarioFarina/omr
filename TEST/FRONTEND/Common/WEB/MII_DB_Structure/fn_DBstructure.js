//**************************************************************************************
//Title:  Struttura Database
//Author: Bruno Rosati
//Date:   08/11/2018
//Vers:   1.0
//**************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Common = jQuery.sap.resources({
    url: "/XMII/CM/Common/res/common.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });

// Function che crea la tabella e ritorna l'oggetto oTable
function createTabDBStructure() {

	var oTable = UI5Utils.init_UI5_Table({
		id: "DBStructureTab",
		exportButton: true,
		properties: {
			visibleRowCount: 15,
			fixedColumnCount: 1,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("DBStructureTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnTableDetails").setEnabled(false);
                }
                else {
                    $.UIbyID("btnTableDetails").setEnabled(true);
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
						text: oLng_Common.getText("Common_Detail"),
						id:   "btnTableDetails",
						icon: 'sap-icon://display-more',
						enabled: false,
						press: function (){
							openTableDetails(fnGetCellValue("DBStructureTab", "TABLE_NAME"));
						}
					})
				],
				rightItems: [
                    new sap.ui.commons.Label({
                        id: "lblRecordNumber",
                        text: oLng_Common.getText("Common_TablesNumber"),
                        textAlign: "Right"
                    }),
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabDBStructure();
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("DBStructureTab",oLng_Common.getText("Common_TablesList"));
						}
					})
				]
			})
		},
		exportButton: false,
		columns: [
			{
				Field: "TABLE_NAME",
				label: oLng_Common.getText("Common_TABLE_NAME"),
                properties: {
					width: "190px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_CATALOG",
				label: oLng_Common.getText("Common_TABLE_CATALOG"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_SCHEMA",
				label: oLng_Common.getText("Common_TABLE_SCHEMA"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_TYPE",
				label: oLng_Common.getText("Common_TABLE_TYPE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "ENGINE",
				label: oLng_Common.getText("Common_ENGINE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "VERSION",
				label: oLng_Common.getText("Common_VERSION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "ROW_FORMAT",
				label: oLng_Common.getText("Common_ROW_FORMAT"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_ROWS",
				label: oLng_Common.getText("Common_TABLE_ROWS"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "AVG_ROW_LENGTH",
				label: oLng_Common.getText("Common_AVG_ROW_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DATA_LENGTH",
				label: oLng_Common.getText("Common_DATA_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "MAX_DATA_LENGTH",
				label: oLng_Common.getText("Common_MAX_DATA_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "INDEX_LENGTH",
				label: oLng_Common.getText("Common_INDEX_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DATA_FREE",
				label: oLng_Common.getText("Common_DATA_FREE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "AUTO_INCREMENT",
				label: oLng_Common.getText("Common_AUTO_INCREMENT"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CREATE_TIME",
				label: oLng_Common.getText("Common_CREATE_TIME"),
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "UPDATE_TIME",
				label: oLng_Common.getText("Common_UPDATE_TIME"),
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "CHECK_TIME",
				label: oLng_Common.getText("Common_CHECK_TIME"),
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "TABLE_COLLATION",
				label: oLng_Common.getText("Common_TABLE_COLLATION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CHECKSUM",
				label: oLng_Common.getText("Common_CHECKSUM"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CREATE_OPTIONS",
				label: oLng_Common.getText("Common_CREATE_OPTIONS"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_COMMENT",
				label: oLng_Common.getText("Common_TABLE_COMMENT"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			}
		]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	/*refreshTabDBStructure();*/
	return oTable;
}

function refreshTabDBStructure() {

	var qParams = {
		data: "Common/MII_DB_Structure/getTablesListQR" + 
            "&Param.1=" + $.UIbyID("CmbEnvironment").getSelectedKey(),
		dataType: "xml"
	};

	$.UIbyID("DBStructureTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		$.UIbyID("DBStructureTab").setBusy(true);
		// carica il model della tabella
		$.UIbyID("DBStructureTab").getModel().setData(data);
	}) // End Done Function
	// on fail
		.fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_Common.getText("Common_DataUpdateError"));
    })
	// always, either on success or fail
		.always(function () {

		// remove busy indicator
		$.UIbyID("DBStructureTab").setBusy(false);
        var rowsetObject = $.UIbyID("DBStructureTab").getModel().getObject("/Rowset/0");
        if(rowsetObject){
            var iRecNumber = rowsetObject.childNodes.length - 1;
            $.UIbyID("lblRecordNumber").setText(oLng_Common.getText("Common_TablesNumber") + " " + iRecNumber);
        }
	});

    $.UIbyID("DBStructureTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}

function openTableDetails(sTableName) {

	//  Crea la finestra di dialogo
	var oTableDetailsDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgTableDetails",
		maxWidth: "1200px",
        maxHeight: "900px",
        title: oLng_Common.getText("Common_Detail") + ": " + sTableName,
		showCloseButton: false
	});
    
    var oTbDetails = UI5Utils.init_UI5_Table({
		id: "ColumnsStructureTab",
		exportButton: true,
		properties: {
			visibleRowCount: 7,
			fixedColumnCount: 1,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){},
			toolbar: new sap.ui.commons.Toolbar({
				items: [],
				rightItems: [
                    new sap.ui.commons.Label({
                        id: "lblColumnsRecordNumber",
                        text: oLng_Common.getText("Common_ColumnsNumber"),
                        textAlign: "Right"
                    }),
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabColumnsStructure(sTableName);
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("ColumnsStructureTab",oLng_Common.getText("Common_ColumnsList") + " " + sTableName);
						}
					})
				]
			})
		},
		exportButton: false,
		columns: [
			{
				Field: "COLUMN_NAME",
				label: oLng_Common.getText("Common_COLUMN_NAME"),
                properties: {
					width: "190px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_NAME",
				label: oLng_Common.getText("Common_TABLE_NAME"),
                properties: {
					width: "190px",
					visible : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_CATALOG",
				label: oLng_Common.getText("Common_TABLE_CATALOG"),
                properties: {
					width: "130px",
					visible : false,
					flexible : false
				}
			},
			{
				Field: "TABLE_SCHEMA",
				label: oLng_Common.getText("Common_TABLE_SCHEMA"),
                properties: {
					width: "130px",
					visible : false,
					flexible : false
				}
			},
			{
				Field: "ORDINAL_POSITION",
				label: oLng_Common.getText("Common_ORDINAL_POSITION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "COLUMN_DEFAULT",
				label: oLng_Common.getText("Common_COLUMN_DEFAULT"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "IS_NULLABLE",
				label: oLng_Common.getText("Common_IS_NULLABLE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DATA_TYPE",
				label: oLng_Common.getText("Common_DATA_TYPE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CHARACTER_MAXIMUM_LENGTH",
				label: oLng_Common.getText("Common_CHARACTER_MAXIMUM_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CHARACTER_OCTET_LENGTH",
				label: oLng_Common.getText("Common_CHARACTER_OCTET_LENGTH"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "NUMERIC_PRECISION",
				label: oLng_Common.getText("Common_NUMERIC_PRECISION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "NUMERIC_SCALE",
				label: oLng_Common.getText("Common_NUMERIC_SCALE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DATETIME_PRECISION",
				label: oLng_Common.getText("Common_DATETIME_PRECISION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "CHARACTER_SET_NAME",
				label: oLng_Common.getText("Common_CHARACTER_SET_NAME"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "COLLATION_NAME",
				label: oLng_Common.getText("Common_COLLATION_NAME"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "COLUMN_TYPE",
				label: oLng_Common.getText("Common_COLUMN_TYPE"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "COLUMN_KEY",
				label: oLng_Common.getText("Common_COLUMN_KEY"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "EXTRA",
				label: oLng_Common.getText("Common_EXTRA"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "PRIVILEGES",
				label: oLng_Common.getText("Common_PRIVILEGES"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "COLUMN_COMMENT",
				label: oLng_Common.getText("Common_COLUMN_COMMENT"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "GENERATION_EXPRESSION",
				label: oLng_Common.getText("Common_GENERATION_EXPRESSION"),
                properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			}
		]
	});

	oModelDetails = new sap.ui.model.xml.XMLModel();

	oTbDetails.setModel(oModelDetails);
	oTbDetails.bindRows("/Rowset/Row");
    
    oTableDetailsDlg.addContent(oTbDetails);
    
    oTableDetailsDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Common.getText("Common_Close"),
		press: function () {
			oTableDetailsDlg.close();
			oTableDetailsDlg.destroy();
		}
	}));
    
    refreshTabColumnsStructure(sTableName)

	oTableDetailsDlg.open();
}

function refreshTabColumnsStructure(sTableName) {

	var qParams = {
		data: "Common/MII_DB_Structure/getColumnsListFromTableQR" + 
            "&Param.1=" + sTableName +
            "&Param.2=" + $.UIbyID("CmbEnvironment").getSelectedKey(),
		dataType: "xml"
	};

	$.UIbyID("ColumnsStructureTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		$.UIbyID("ColumnsStructureTab").setBusy(true);
		// carica il model della tabella
		$.UIbyID("ColumnsStructureTab").getModel().setData(data);
	}) // End Done Function
	// on fail
		.fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_Common.getText("Common_DataUpdateError"));
    })
	// always, either on success or fail
		.always(function () {

		// remove busy indicator
		$.UIbyID("ColumnsStructureTab").setBusy(false);
        var rowsetObject = $.UIbyID("ColumnsStructureTab").getModel().getObject("/Rowset/0");
        if(rowsetObject){
            var iRecNumber = rowsetObject.childNodes.length - 1;
            $.UIbyID("lblColumnsRecordNumber").setText(oLng_Common.getText("Common_ColumnsNumber") + " " + iRecNumber);
        }
	});

    $.UIbyID("ColumnsStructureTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}