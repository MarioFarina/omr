//**************************************************************************************
//Title:  Struttura Database
//Author: Bruno Rosati
//Date:   08/11/2018
//Vers:   1.0
//**************************************************************************************

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdMon = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";
var dataProdSap = "Content-Type=text/XML&QueryTemplate=ProductionMain/Sap/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Confirm/ConfProd/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

console.debug("Linug = " +  sap.ui.getCore().getConfiguration().getLanguage() );

// setta le risorse per la lingua locale
var oLng_Common = jQuery.sap.resources({
    url: "/XMII/CM/Common/res/common.i18n.properties",
	locale: sCurrentLocale
});


jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/Common/MII_DB_Structure/fn_DBstructure"
    ],
    function () {
        $(document).ready(function () {
            
            var oLblEnvironment = new sap.ui.commons.Label().setText(oLng_Common.getText("Common_Environment"));
            
            var jEnvironment = {root:[]};
            jEnvironment.root.push({
                key: 'MESMID',
                text: oLng_Common.getText("Common_Development")
            });
            jEnvironment.root.push({
                key: 'MESMII',
                text: oLng_Common.getText("Common_Production")
            });
            
            var oEnvironmentModel = new sap.ui.model.json.JSONModel();
            oEnvironmentModel.setData(jEnvironment);
            
            var oCmbEnvironment = new sap.ui.commons.ComboBox({
                id: "CmbEnvironment",
                change: function() {
                    refreshTabDBStructure();
                }
            });
            oCmbEnvironment.setModel(oEnvironmentModel);
            var oItemEnvironment = new sap.ui.core.ListItem();
            oItemEnvironment.bindProperty("text", "text");
            oItemEnvironment.bindProperty("key", "key");
            oCmbEnvironment.bindItems("/root", oItemEnvironment);
            
            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblEnvironment,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbEnvironment
                ]
            });
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
                content: [
                    oSeparator,
                    createTabDBStructure()
				],
                width: "100%"
            });
            
            oVerticalLayout.placeAt("MasterCont");
            refreshTabDBStructure();
            $("#splash-screen").hide(); //nasconde l'icona di loading
        });        
    }
);