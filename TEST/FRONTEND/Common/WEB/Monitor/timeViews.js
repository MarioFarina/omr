// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProd = "Content-Type=text/XML&QueryTemplate=Production/PrMaster/";
var icon16 = "/XMII/CM/Common/icons/16x16/";

jQuery.ajaxSetup({
    cache: false
});

Libraries.load(
	["/XMII/CM/Common/MII_core/UI5_utils",
     "/XMII/CM/Common/Monitor/fn_time",
     "/XMII/CM/Common/Monitor/fn_threadsStatus"/*,
     "/XMII/CM/Common/Monitor/fn_usrUsage"*/],
    function () {

        $(document).ready(function () {
            $("#splash-screen").hide();
        });

        // Crea la TabStrip principale
        var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
        oTabMaster.attachClose(function (oEvent) {
            var oTabStrip = oEvent.oSource;
            oTabStrip.closeTab(oEvent.getParameter("index"));
        });


        // 1. tab: Logins
        var oTable1 = createTabTime();
        var oLayout1 = new sap.ui.commons.layout.MatrixLayout("UsingTimeTab", {
            columns: 1
        });
        oLayout1.createRow(oTable1);
        oTabMaster.createTab("Avvio sistema", oLayout1);

        // 2. tab: threads Status
        var oTable2 = createTabThreadStatus();
        var oLayout2 = new sap.ui.commons.layout.MatrixLayout("ThreadsStatusTab", {
            columns: 1
        });
        oLayout2.createRow(oTable2);
        oTabMaster.createTab("Stato dei thread", oLayout2);

        oTabMaster.placeAt("MasterCont");

    }
);

//jQuery.getScript("/XMII/CM/Common/Monitor/fn_usrLogins.js");
//jQuery.getScript("/XMII/CM/Common/Monitor/fn_usrUsage.js" );
/*jQuery.getScript( "/XMII/CM/Production/PrMaster/fn_plants.js" );
jQuery.getScript( "/XMII/CM/Production/PrMaster/fn_prshift.js" );
jQuery.getScript( "/XMII/CM/Production/PrMaster/fn_squadre.js" );*/
/*function  ( data, textStatus, jqxhr ) {
            //console.log( data ); // Data returned
            console.log( textStatus ); // Success
            console.log( jqxhr.status ); // 200
            console.log( "Load was performed." );
	    });
*/

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
/*$(document).ready(function () {


    // Crea la TabStrip principale
    var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
    oTabMaster.attachClose(function (oEvent) {
        var oTabStrip = oEvent.oSource;
        oTabStrip.closeTab(oEvent.getParameter("index"));
    });


    // 1. tab: Logins
    var oTable1 = createTabUsrLogins();
    var oLayout1 = new sap.ui.commons.layout.MatrixLayout("UsrLoginsTab", {
        columns: 1
    });
    oLayout1.createRow(oTable1);
    oTabMaster.createTab("Login degli utenti", oLayout1);

    // 1. tab: Logins
    var oTable2 = createTabUsrUsage();
    var oLayout2 = new sap.ui.commons.layout.MatrixLayout("UsrUsageTab", {
        columns: 1
    });
    oLayout2.createRow(oTable2);
    oTabMaster.createTab("Utilizzo degli utenti", oLayout2);

// 3. tab: Squadre
/* var oTable5 = createTabSquadre();
    var oLayout5 = new sap.ui.commons.layout.MatrixLayout("tabTeams", {columns: 1});
    oLayout5.createRow( oTable5);
    oTabMaster.createTab("Squadre",oLayout5);
    	
    // 4. tab: Aree
    var oTable2 = createTabAree();
    var oLayout2 = new sap.ui.commons.layout.MatrixLayout("tabAree", {columns: 1});
    oLayout2.createRow( oTable2);
    oTabMaster.createTab("Aree",oLayout2);
	
	
    // 5. tab: Presse
    var oTable1 = createTabPresse();
    var oLayout1 = new sap.ui.commons.layout.MatrixLayout("tabPresse", {columns: 1});
    oLayout1.createRow( oTable1);
    oTabMaster.createTab("Presse",oLayout1);

    $("#splash-screen").hide();

    oTabMaster.placeAt("MasterCont");
});*/