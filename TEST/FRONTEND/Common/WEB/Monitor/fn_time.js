/***********************************************************************************************/
// Funzioni ed oggetti per fn_time
/***********************************************************************************************/

var oTableTemp1 = UI5Utils.init_UI5_Table({
	id: "hostInfo"
});
$.UIbyID("hostInfo").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=HostInfo");

var oTableTemp2 = UI5Utils.init_UI5_Table({
	id: "runtimeStatus"
});
$.UIbyID("runtimeStatus").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=JavaRuntimeStatus");

var oTableTemp3 = UI5Utils.init_UI5_Table({
	id: "garbageCollector"
});
$.UIbyID("garbageCollector").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=JavaRunGC");

jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("sap.ui.core.format.DateFormat");

// Functione che crea la tabella usrLogins e ritorna l'oggetto oTable
function createTabTime() {


	var oTable = UI5Utils.init_UI5_Table({
		id: "usingTime",
		properties: {
			title: "Avvio sistema",
			visibleRowCount: 1,
			width: "98%",
			firstVisibleRow: 1,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {}
		},
		exportButton: false,
		toolbarItems: [
			new sap.ui.commons.Button({
				text: "Stato sistema",
				icon: "sap-icon://detail-view",
				tooltip: "Stato sistema",
				press: function () {
					openDetails();
				}
			}),
			new sap.ui.commons.Button({
				text: "esegui GC",
				icon: "sap-icon://cancel-maintenance",
				tooltip: "esegui GC",
				press: function () {
					sap.ui.commons.MessageBox.show("Eseguire il Garbage collector?",
																				 sap.ui.commons.MessageBox.Icon.WARNING,
																				 "Eseguire il Garbage collector?",
																				 [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
																				 function(sResult) {
						if (sResult == 'YES') {
							openGarbage();
						}
					},
																				 sap.ui.commons.MessageBox.Action.YES);
				}
			})
		],
		columns: [
			{
				Field: "StartTime",
				label: "DataOra avvio",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Duration",
				label: "in esecuzione da",
				properties: {
					width: "50px"
				}
			}
		]
	});


	//    addColumnEx(oTable, "Divisione", "PLANT");
	//if (window.console) console.log("Colonna -> " + oPlantF.getFilterProperty());

	/* aggiunta di colonne alla vecchia maniera
    addColumnEx(oTable, "", "StartTime", "50px", "DateTime");
    addColumnEx(oTable, "Durata", "Duration", "50px");
    */

	oModel = new sap.ui.model.xml.XMLModel();


	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabUsingTime();
	return oTable;
}

// Aggiorna la tabella Plants
function refreshTabUsingTime() {
	$("#splash-screen").show();
	$.UIbyID("usingTime").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&mode=UpTime");
	$("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Calendario
function openDetails() {
	/* if (bEdit === undefined) bEdit = false; */

	// contenitore dati lista divisioni
	/*
	var oModel = new sap.ui.model.xml.XMLModel();
	//	oModel.loadData(QService + dataCommon + "GetPlantsQR");
	oModel.loadData(QService + dataMast + "getPlants");
    */

	// Crea la ComboBox per le divisioni
	/*
	var oCmbPlant = new sap.ui.commons.ComboBox("cmbPlants", {selectedKey : bEdit?fnGetCellValue("CalendarTab", "PLANT"):$.UIbyID("filtPlantCalendar").getSelectedKey(),enabled : !bEdit});
	oCmbPlant.setModel(oModel);
	var oItemPlant = new sap.ui.core.ListItem();
	//	oItemPlant.bindProperty("text", "NAME1");
	oItemPlant.bindProperty("text", "PLNAME");
	oItemPlant.bindProperty("key", "PLANT");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);

	oCmbPlant.attachChange(
		function(oEvent){
			var selPlant = $.UIbyID("cmbPlants").getSelectedKey();
			$.UIbyID("cmbPress").getModel().loadData(QService + dataMast + "getPressListbyPlant&Param.1="+selPlant);
			$.UIbyID("oCmbTurno").getModel().loadData(QService + dataMast + "GetShiftListByPlantQR&Param.1="+selPlant);
			$.UIbyID("oCmbCID").getModel().loadData(QService + dataMast + "GetAllCIDbyPlant&Param.1="+selPlant);
		}
	);
    */

	// contenitore dati lista CID
	/*
    var oModel2 = new sap.ui.model.xml.XMLModel();
	var selPlant = $.UIbyID("filtPlantCalendar").getSelectedKey();

	var oCmbTurno = comboBoxQuery( "oCmbTurno", "SHIFT", "SHNAME", "GetShiftListByPlant&Param.1="+selPlant,  bEdit?fnGetCellValue("CalendarTab", "SHNAME"):"" );
	var oCmbCID = comboBoxQuery( "oCmbCID", "PERNR", "PERNAM", "GetAllCIDbyPlant&Param.1="+selPlant,  bEdit?fnGetCellValue("CalendarTab", "CIDNAME"):"" );

	if (bEdit) oCmbCID.setSelectedKey(fnGetCellValue("CalendarTab", "CID"));
    */

	// contenitore dati lista presse
	/*
	var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataMast + "getPressListbyPlant&Param.1="+selPlant);

	// Crea la ComboBox per le presse
	var oCmbPress = new sap.ui.commons.ComboBox("cmbPress", {selectedKey : bEdit?fnGetCellValue("CalendarTab", "PRESS"):""});
	oCmbPress.setModel(oModel3);
	var oItemPress = new sap.ui.core.ListItem();
	oItemPress.bindProperty("text", "DESC");
	oItemPress.bindProperty("key", "PRESS");
	oCmbPress.bindItems("/Rowset/Row", oItemPress);
    */

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({modal:  true,
																					 resizable: true,
																					 id: "dlgDetails",
																					 maxWidth:  "600px",
																					 Width:   "600px",
																					 showCloseButton: false});


	/* var percValue = bEdit?fnGetCellValue("CalendarTab", "PERC"):100; */

	//(fnGetCellValue("CalendarTab", "FixedVal")==1)?true : false

	$.UIbyID("hostInfo").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=HostInfo");
	$.UIbyID("runtimeStatus").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=JavaRuntimeStatus");


	var oString1 = formatThousands($.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/MaxMemory"));
	var oString2 = formatThousands($.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/TotalMemory"));
	var oString3 = formatThousands($.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/FreeMemory"));
	var oString4 = formatThousands($.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/UsedMemory"));    

	var oDate = formatDate($.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/DateTime"));

	/* oTxtHostValueProperty.setText($.UIbyID("hostInfo").getModel().getProperty("/Rowset/Row/PropertyValue")); */

	var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
	var oForm1 = new sap.ui.layout.form.Form({
		title: new sap.ui.core.Title({text: "Stato sistema", icon: "sap-icon://detail-view", tooltip: "Stato sistema"}),
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [ 
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: $.UIbyID("hostInfo").getModel().getProperty("/Rowset/Row/PropertyName") + ":", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("hostInfo").getModel().getProperty("/Rowset/Row/PropertyValue"),
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: $.UIbyID("hostInfo").getModel().getProperty("/Rowset/Row/1/PropertyName") + ":", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("hostInfo").getModel().getProperty("/Rowset/Row/1/PropertyValue"),
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Data:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: oDate,
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Memoria massima:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: oString1 + " Byte",
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Memoria totale:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: oString2 + " Byte",
																									 editable: false
																									})]
					}),                   
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Memoria utilizzata:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: oString4 + " Byte",
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Memoria libera:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: oString3 + " Byte",
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Processori:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/Processors"),
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Thread attivi:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/ActiveThreads"),
																									 editable: false
																									})]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Gruppi attivi:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("runtimeStatus").getModel().getProperty("/Rowset/Row/ActiveGroups"),
																									 editable: false
																									})]
					})
				] 
			})
		]});
	oEdtDlg.addContent(oForm1);
	/*
	oEdtDlg.addButton(new sap.ui.commons.Button({text: oLng_Msg.getText("ConfPage_BtnSaveAndNew"), press:function(){fnSaveCal(bEdit, true);}}));
	oEdtDlg.addButton(new sap.ui.commons.Button({text: oLng_Msg.getText("ConfPage_BtnSave"), press:function(){fnSaveCal(bEdit,false);}}));
    */

	oEdtDlg.addButton(new sap.ui.commons.Button({text: "Aggiorna", press: function(){oEdtDlg.close(); oEdtDlg.destroy(); openDetails();}}));
	oEdtDlg.addButton(new sap.ui.commons.Button({text: "Chiudi", press:function(){oEdtDlg.close(); oEdtDlg.destroy();}}));

	oEdtDlg.open();
}

// funzione per il garbage collector
function openGarbage() {
	var oEdtDlg = new sap.ui.commons.Dialog({modal:  true,
																					 resizable: true,
																					 id: "dlgGarbage",
																					 maxWidth:  "600px",
																					 Width:   "600px",
																					 showCloseButton: false});


	var oLayout2 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
	var oForm2 = new sap.ui.layout.form.Form({
		title: new sap.ui.core.Title({text: "Garbage Collector", icon: "sap-icon://cancel-maintenance", tooltip: "Garbage Collector"}),
		width: "98%",
		layout: oLayout2,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [ 
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({text: "Esito:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
						fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
																									 value: $.UIbyID("garbageCollector").getModel().getProperty("/Messages/Message"),
																									 editable: false
																									})]
					})
				] 
			})
		]});
	oEdtDlg.addContent(oForm2);

	oEdtDlg.addButton(new sap.ui.commons.Button({text: "Chiudi", press:function(){oEdtDlg.close(); oEdtDlg.destroy();}}));

	oEdtDlg.open();
}

function formatThousands(number){
	var numberFormat = sap.ui.core.format.NumberFormat.getFloatInstance({maxFractionDigits:1});
	var text = numberFormat.format(number);
	return text;
}

function formatDate(date){
	var parts = date.match(/(\d+)/g);
	// new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
	dateToBeFormatted = new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]); // months are 0-based
	var dateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "dd/MM/yyyy HH:mm:ss"});
	var text = dateFormat.format(dateToBeFormatted);
	return text;
}