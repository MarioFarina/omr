/***********************************************************************************************/
// Funzioni ed oggetti per fn_time
/***********************************************************************************************/


// Functione che crea la tabella usrLogins e ritorna l'oggetto oTable
function createTabThreadStatus() {


    var oTable = UI5Utils.init_UI5_Table({
        id: "threadsStatus",
        properties: {
            title: "Stato dei thread",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 1,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function (oControlEvent) {
            }
        },
        exportButton: false,
        toolbarItems:[
            new sap.ui.commons.Button({
                text: "Aggiorna",
                icon: "sap-icon://refresh",            
                enabled: true,
                press: function ()
                {
                    refreshTabThreadsStatus();
                }
            })
        ],
        columns: [
            {
                Field: "Name",
                label: "Nome",
                properties: {
                    width: "120px"
                }
			},
            {
                Field: "Group",
                label: "Gruppo",
                properties: {
                    width: "50px"
                }                
			},
            {
                Field: "Priority",
                label: "PrioritÃ ",
                properties: {
                    width: "30px"
                }                
			},
            {
                Field: "Alive",
                label: "Vivo",
                properties: {
                    width: "30px"
                }                
			},
            {
                Field: "Daemon",
                label: "Daemon",
                properties: {
                    width: "30px"
                }                
			}
		],
        /*properties: {
            //fixedColumnCount: nFixedCols,
            //visibleRowCount: 1,
            width: "100%",
            height: "100%",
            selectionMode: sap.ui.table.SelectionMode.Single
        }*/
        toolbar: new sap.ui.commons.Toolbar({
            items: [
/*
          new sap.ui.commons.Button(
          {
            text: "Aggiungi",
            icon: icon16 + "blueprint--plus.png",
            press: function ()
            {
              openPlantEdit(false);
            }
          }),
*/
          /* new sap.ui.commons.Button({
                    text: "Aggiorna",
                    icon: icon16 + "refresh.png",
                    enabled: true,
                    press: function () {
                        refreshTabUsrLog();
                    }
                }),
          new sap.ui.commons.Button({
                    text: "Esporta",
                    id: "btnExportPlant",
                    icon: icon16 + "application-export.png",
                    enabled: true,
                    press: function () {
                        exportTableToCSV("usrLog", "DataTable");
                    }
                })*/
        ]
        })
    });


    //    addColumnEx(oTable, "Divisione", "PLANT");
    //if (window.console) console.log("Colonna -> " + oPlantF.getFilterProperty());

    /* aggiunta di colonne alla vecchia maniera
    addColumnEx(oTable, "", "StartTime", "50px", "DateTime");
    addColumnEx(oTable, "Durata", "Duration", "50px");
    */

    oModel = new sap.ui.model.xml.XMLModel();


    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabThreadsStatus();
    return oTable;
}

// Aggiorna la tabella Plants
function refreshTabThreadsStatus() {
    $("#splash-screen").show();
    $.UIbyID("threadsStatus").getModel().loadData(QService + "Content-Type=text/xml&service=SystemInfo&Mode=JavaThreadStatus");
    $("#splash-screen").hide();
}