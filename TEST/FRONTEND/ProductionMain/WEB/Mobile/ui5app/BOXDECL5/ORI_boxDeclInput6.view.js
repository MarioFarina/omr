// View Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.BOXDECL5.boxDeclInput6", {

	/** Specifies the Controller belonging to this View.
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 */
	getControllerName: function () {
		return "ui5app.BOXDECL5.boxDeclInput6";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	 * Since the Controller is given to this method, its event handlers can be attached right away.
	 */
	createContent: function (oController) {

		var oView = this;

		var idTextFormatter = function (sId, sTxt) {
			return sId + " " + sTxt;
		};

		var oStdQtyTemplate = new sap.ui.core.Item({
			key: "{NMIMB}",
			text: {
				parts: [
					{
						path: 'NMIMB'
					},
					{
						path: 'DESCR'
					},
					{
						path: 'QTY'
					},
					{
						path: 'MATIMB'
					}
],
				formatter: function (sNmimb, sDesc, sQty) {
					return sQty + ' (' + sNmimb + ' ' + sDesc + ')';
				}
			}
		});

		var oCodImbTemplate = new sap.ui.core.Item({
			key: "{MATIMB}",
			text: {
				parts: [
					{
						path: 'MATIMB'
					}
],
				formatter: function (sMatimb) {
					return sMatimb;
				}
			}
		});

		var oStdQtyCombo = new sap.m.ComboBox({
			id: 'boxDeclInputStdQty6',
			width: '130%',
			maxWidth: '130%',
			layoutData: new sap.ui.layout.GridData({
				span: "L5 M5 S12"
			})
		}).bindAggregation("items", "/Rowset/Row", oStdQtyTemplate);

		var oCodImbCombo = new sap.m.ComboBox({
			id: 'boxDeclInputCodImb6',
			width: '130%',
			maxWidth: '130%',
			layoutData: new sap.ui.layout.GridData({
				span: "L5 M5 S12"
			})
		}).bindAggregation("items", "/Rowset/Row", oCodImbTemplate);

		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			content: [
new sap.m.Label({
					id: "boxDeclOrder6",
					text: oLng_Opr.getText("BoxDeclInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
new sap.m.Text({
					id: "boxDeclInputOrder6",
					text: '{/ORDER}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: "boxDeclPhase6",
					text: oLng_Opr.getText("General_CDL") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
new sap.m.Text({
					id: "boxDeclInputPhase6",
					text: {
						parts: [
							{
								path: "/CDLID"
							},
							{
								path: "/CDLDESC"
							}
],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
new sap.ui.core.Icon({
					id: "boxDeclIconPhase6",
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowLineInfo(oView),
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
new sap.m.Label({
					id: "boxDeclLine6",
					text: oLng_Opr.getText("General_Poper") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: false
					})
				}),
new sap.m.Text({
					id: "boxDeclInputLine6",
					text: {
						parts: [
							{
								path: "/POPER"
							},
							{
								path: "/CICTXT"
							}
],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: "boxDeclMaterial6",
					text: oLng_Opr.getText("BoxDeclInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
new sap.m.Text({
					id: "boxDeclInputMaterial6",
					text: {
						parts: [
							{
								path: "/MATERIAL"
							},
							{
								path: "/MAT_DESC"
							}
],
						formatter: idTextFormatter
					},
					maxLines: 2,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
new sap.ui.core.Icon({
					id: "boxDeclIconMaterial6",
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo, {
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
}],
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclUDCLabel6',
					text: oLng_Opr.getText("BoxDeclInput_UDC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
new sap.m.Text({
					id: 'boxDeclUDCText6',
					text: '{/UDCNR}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclDeclQtyLabel6',
					text: oLng_Opr.getText("BoxDeclInput_DeclQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
new sap.m.Text({
					id: 'boxDeclDeclQtyText6',
					text: '{/QTYRES}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclOpLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Operator") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}),
new sap.m.Text({
					id: 'boxDeclOpText6',
					text: '{/LAST_OPENAME}',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclTimeLabel6',
					text: oLng_Opr.getText("BoxDeclInput_Datetime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
new sap.m.Text({
					id: 'boxDeclTimeText6',
					text: {
						parts: [
							{
								path: "/TIMEID"
							}
],
						formatter: dateFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclDater6',
					text: oLng_Opr.getText("BoxDeclInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
new sap.m.MaskInput({
					id: 'boxDeclInputDater6',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'MMYY',
					width: '160px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclBasketTT6',
					text: oLng_Opr.getText("BoxDeclInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),
new sap.m.Input({
					id: 'boxDeclInputBasketTT6',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclWorkTime6',
					text: oLng_Opr.getText("BoxDeclInput_WorkTime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
new NumericInput({
					id: 'boxDeclInputWorkTime6',
					min: 0,
					width: '80px',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclNrPrnt6',
					text: oLng_Opr.getText("BoxDeclInput_Mold") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12"
					})
				}).addStyleClass("inputLabel"),
new sap.m.Input({
					id: 'boxDeclInputNrPrnt6',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
new sap.m.Label({
					id: 'boxDeclQty6',
					text: oLng_Opr.getText("BoxDeclInput_Qty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
new NumericInput({
					id: 'boxDeclInputQty6',
					min: 0,
					width: '80px',
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})

				}).addEventDelegate({
					onfocusin: function (e) {
						$("#boxDeclInputQty6-inner").select();
					}
				}),
new sap.m.Label({
					id: "QtyBuoni6",
					text: "",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					visible: false,
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M8 S12"
					})
				}).addStyleClass("inputLabel"),
				// Destinazione
				new sap.m.Label({
					id: 'boxDeclNextPhase6',
					text: oLng_Opr.getText("RepUdcInput_Destination") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12", linebreak: false
					})
				}).addStyleClass("inputLabel"),
				new sap.m.ComboBox({
					id: 'boxDeclInputNextPhase6',
					width: "110%",
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					}),
					change: oController.onNextPhaseChange
				}).bindAggregation("items", "/Rowset/1/Row",
													 new sap.ui.core.Item({
					key: '{POPER}',
					text: {
						parts: [
							{
								path: 'POPER'
							},
							{
								path: 'CICTXT'
							},
							{
								path: 'COLOR'
							}
						],
						formatter: function (sIdRep, sDesc, sColor) {
							if (sColor) {
								var css = document.createElement("style");
								css.type = "text/css";
								css.innerHTML = "li[data-color='" + sColor + "'] { color: " + sColor + " !important; }";
								document.body.appendChild(css);
							}
							return sIdRep + ' - ' + sDesc;
						}
					},
					customData: [
						new sap.ui.core.CustomData({
							key: "color",
							value: "{COLOR}",
							writeToDom: true
						})
					]
				})
													),
				
				// norma imballo
				new sap.m.Label({
					id: 'boxDeclStdQty6',
					text: oLng_Opr.getText("BoxDeclInput_Nmimb") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L2 M2 S12",  linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.HBox({id: 'boxHboxStdQty6',
												justifyContent: sap.m.FlexJustifyContent.Start,
					width: "100%",
					items: [

oStdQtyCombo,
new sap.ui.core.Icon({
							id: 'boxDeclIconStdQty6',
							src: "sap-icon://value-help",
							size: "18px",
							press: [oController.onSelectNmimb, {
								view: oView,
								combo: oStdQtyCombo,
								comboCodImb: oCodImbCombo
}],
							tooltip: oLng_Opr.getText('BoxDeclInput_Nmimb'),
							layoutData: new sap.ui.layout.GridData({
								span: "L1 M1 S12"
							})
						}).addStyleClass("inputLabel")
],
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12", linebreak: false
					})
				}),
// codice imballo
				new sap.m.Label({
					id: 'boxDeclCodImb6',
					text: oLng_Opr.getText("BoxDeclInput_CodImb") + ':',
					width: '110%',
					minWidth: '110%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M1 S12", linebreak: false
					})
				}).addStyleClass("inputLabel"),
new sap.m.HBox({
					id: 'boxDeclBoxCodImb6',
	justifyContent: sap.m.FlexJustifyContent.Start,
					width: "98%",
					items: [

oCodImbCombo,
new sap.ui.core.Icon({
							id: 'boxDeclIconCodImb6',
							src: "sap-icon://value-help",
							size: "18px",
							visible: false,
							press: [oController.onSelectNmimb, {
								view: oView,
								combo: oCodImbCombo
}],
							tooltip: oLng_Opr.getText('BoxDeclInput_CodImb'),
							layoutData: new sap.ui.layout.GridData({
								span: "L1 M1 S12"
							})
						}).addStyleClass("inputLabel")
],
					layoutData: new sap.ui.layout.GridData({
						span: "L4 M4 S12", linebreak: false
					})
				}),

				// Note
new sap.m.Label({
					id: 'boxDeclLabelNote6',
					text: oLng_Opr.getText("SuspDeclReas_Note") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData: new sap.ui.layout.GridData({
						span: "L1 M3 S12", linebreak: true
					})
				}).addStyleClass("inputLabel"),
new sap.m.Input({
					id: 'boxDeclNote6',
					width: '100%',
					layoutData: new sap.ui.layout.GridData({
						span: "L10 M10 S12",
						linebreak: false
					})
				})
]
		}).addStyleClass("sapUiTinyMarginTop");

		var oPanel = new sap.m.Panel({
			content: oGrid
		});

		var oPage = new sap.m.Page({
			id: "boxDeclInputPage6",
			title: oLng_Opr.getText('BoxDecl5_Good'),
			enableScrolling: false,
			headerContent: [
new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
oPanel,
new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
new sap.m.Button({
							id: 'boxDeclConfirmButton6',
							text: oLng_Opr.getText("BoxDeclInput_Confirm"),
							icon: "sap-icon://accept",
							press: oController.onConfirm
						}).addStyleClass("greenButton bigButton"),
new sap.m.Button({
							id: 'boxDeclPrintButton6',
							enabled: false,
							text: oLng_Opr.getText("BoxDeclInput_Print"),
							icon: "sap-icon://print",
							press: oController.onPrint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
]
				}).addStyleClass("sapUiTinyMarginTop")
],
			footer: new sap.m.Toolbar({
				content: [
new sap.m.Button({
						icon: "sap-icon://date-time",
						text: oLng_Opr.getText("ShiftDecl_ChangeDateShift"),
						press: oController.changeDateShift,
					}).addStyleClass("yellowButton noBorderButton"),
/*new sap.m.Button({
icon: "sap-icon://date-time",
text: oLng_Opr.getText("ShiftDecl_ChangeDateShift"),
press: oController.restoreDateShift,
}).addStyleClass("redButton noBorderButton"),*/
new sap.m.Text({
						id: 'shiftDeclDate6'
					}),
new sap.m.Text({
						id: 'shiftDeclShift6'
					}),
new sap.m.ToolbarSpacer()
]
			})
		});

		return oPage;
	}

});