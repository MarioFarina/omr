// View Conferma chiusura UdC / Quantità Scarti
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.CLOSEUDC.closeUdcConfirm", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.CLOSEUDC.closeUdcConfirm";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oView = this;
		
		var visibleScrapFunc = function() {
			return {
				parts: [
					{path: "/VIEWTYPE" }
				],		     
				formatter: function(sViewType) {
					return sViewType === 'S' || sViewType === 'SG';
				}
			};
		}
		
		var notVisibleScrapGoodsFunc = function() {
			return {
				parts: [
					{path: "/VIEWTYPE" }
				],		     
				formatter: function(sViewType) {
					return sViewType !== 'SG';
				}
			};
		}
		
		var comboChangeFunc = function () {
			const sScrapReas = this.getValue();
			var aScrapItems = this.getItems();
			for(var i=0; i<aScrapItems.length; i++)
			{
				var oCurrentItem = aScrapItems[i];
				if(oCurrentItem.getKey() === sScrapReas)
				{
					this.setSelectedItem(oCurrentItem);
					break;
				}
			}
		};
		
		var oMicroReasCombo = new sap.m.ComboBox({
			id: 'scrapUdcMicroCombo',
			width : "100%",
			visible: visibleScrapFunc(),
			change: comboChangeFunc,
			layoutData : new sap.ui.layout.GridData({
				span: "L3 M3 S12"
			})
		});
		
		var oMicroReasTemplate = new sap.ui.core.Item({
			key : '{microReas>SREASID}',
			text: {
				parts: [
					{path: 'microReas>SREASID'},
					{path: 'microReas>REASTXT'}
				],		     
				formatter: function(sReasId,sReasTxt){
					return sReasId + ' - ' + sReasTxt;
				}
			}
		});
		oMicroReasCombo.bindAggregation("items","microReas>/Rowset/Row",oMicroReasTemplate);
		
		var oMacroReasCombo = new sap.m.ComboBox({
			id: 'scrapUdcMacroCombo',
			width : "100%",
			visible: visibleScrapFunc(),
			change: comboChangeFunc,
			selectionChange: oController.onMacroReasSelectionChange,
			layoutData : new sap.ui.layout.GridData({
				span: "L3 M3 S12"
			})
		});
		
		var oMacroReasTemplate = new sap.ui.core.Item({
			key : "{macroReas>SCTYPE}",
			text: {
				parts: [
					{path: 'macroReas>SCTYPE'},
					{path: 'macroReas>REASTXT'}
				],		     
				formatter: function(sSctype,sReasTxt){
					return sSctype + ' - ' + sReasTxt;
				}
			}
		})
		oMacroReasCombo.bindAggregation("items","macroReas>/Rowset/Row",oMacroReasTemplate);
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: '{/ORDER}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/POPER" },
							{path: "/CICTXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowPoperInfo(oView),
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/IDLINE" },
							{path: "/LINEID" },
							{path: "/LINETXT"},
							{path: "/VIEWTYPE" },
						],		     
						formatter: function(sIdLine,sLineId,sLineTxt,sViewType) {
							if(sViewType === 'SG')
								sIdLine = sLineId;
							return idTextFormatter(sIdLine,sLineTxt);
						}
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/IDLINE',
						description: '/LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/MATERIAL" },
							{path: "/MAT_DESC"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_UDC") + ':',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: '{/UDCNR}',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Qty") + ':',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					text: '{/QTYRES}',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_Dater") + ':',
					width: '100%',
					visible: {
						parts: [
							{path: "/VIEWTYPE" },
							{path: "/ORIGIN" }
						],		     
						formatter: function(sViewType,sOrigin) {
							return sViewType !== 'S' && sViewType !== 'SG' && sOrigin != 'D';
						}
					},
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: '{/DATER}',
					visible: {
						parts: [
							{path: "/VIEWTYPE" },
							{path: "/ORIGIN" }
						],		     
						formatter: function(sViewType,sOrigin) {
							return sViewType !== 'S' && sViewType !== 'SG' && sOrigin != 'D';
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("BoxDeclInput_BasketTT") + ':',
					width: '100%',
					visible: {
						parts: [
							{path: "/VIEWTYPE" },
							{path: "/ORIGIN" }
						],		     
						formatter: function(sViewType,sOrigin) {
							return sViewType !== 'S' && sViewType !== 'SG' && sOrigin != 'D';
						}
					},
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					text: '{/BASKETTT}',
					visible: {
						parts: [
							{path: "/VIEWTYPE" },
							{path: "/ORIGIN" }
						],		     
						formatter: function(sViewType,sOrigin) {
							return sViewType !== 'S' && sViewType !== 'SG' && sOrigin != 'D';
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Operator") + ':',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: '{/LAST_OPENAME}',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Datetime") + ':',
					visible: notVisibleScrapGoodsFunc(),
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/TIMEID"}
						],
						formatter: dateFormatter
					},
					visible: notVisibleScrapGoodsFunc(),
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("CloseUdcConfirm_Type") + ':',
					visible: {
						parts: [
							{path: "/VIEWTYPE" }
						],		     
						formatter: function(sViewType) {
							return sViewType !== 'S' && sViewType !== 'SG';
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/ORIGIN" }
						],		     
						formatter: function(sOrigin) {
							var sOriginTxt;
							switch(sOrigin) {
								case 'M':
									sOriginTxt = oLng_Opr.getText("CloseUdcConfirm_Goods");
									break;
								case 'C':
									sOriginTxt = oLng_Opr.getText("CloseUdcConfirm_Suspended");
									break;
								case 'P':
									sOriginTxt = oLng_Opr.getText("CloseUdcConfirm_PAC");
									break;
								case 'D':
									sOriginTxt = oLng_Opr.getText("CloseUdcConfirm_Scrap");
									break;
								case 'S':
									sOriginTxt = oLng_Opr.getText("CloseUdcConfirm_SAP");
									break;
								default:
									sOriginTxt = sOrigin;
							}
							
							return sOriginTxt;
						}
					},
					visible: {
						parts: [
							{path: "/VIEWTYPE" }
						],		     
						formatter: function(sViewType) {
							return sViewType !== 'S' && sViewType !== 'SG';
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("ReadUdcInput_State") + ':',
					visible: {
						parts: [
							{path: "/VIEWTYPE" }
						],		     
						formatter: function(sViewType) {
							return sViewType === 'P' || sViewType === 'D' || sViewType === 'R';
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/UDCSTAT" }
						],		     
						formatter: function(sUdcStat) {
							var sStateText;
							switch(sUdcStat) {
								case 'O':
									sStateText = oLng_Opr.getText("ReadUdcInput_StateOpen");
									break;
								case 'C':
									sStateText = oLng_Opr.getText("ReadUdcInput_StateClose");
									break;
								case 'E':
									sStateText = oLng_Opr.getText("ReadUdcInput_StateEmpty");
									break;
								default:
									sStateText = sUdcStat;
							}
							return sStateText;
						}
					},
					visible: {
						parts: [
							{path: "/VIEWTYPE" }
						],		     
						formatter: function(sViewType) {
							return sViewType === 'P' || sViewType === 'D' || sViewType === 'R';
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("ScrapUdcInput_MacroReas") + ':',
					width: '100%',
					visible: visibleScrapFunc(),
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				oMacroReasCombo,
				new sap.m.Label({
					text: oLng_Opr.getText("ScrapUdcInput_Qty") + ':',
					visible: visibleScrapFunc(),
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'scrapUdcQty',
					visible: visibleScrapFunc(),
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("ScrapUdcInput_MicroReas") + ':',
					width: '100%',
					visible: visibleScrapFunc(),
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				oMicroReasCombo
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "closeUdcConfirmPage",
			title: {
				parts: [
					{path: "/VIEWTYPE" }
				],		     
				formatter: function(sViewType) {
					var sTitleKey = '';
					switch(sViewType)
					{
						case 'C':
							sTitleKey = 'CloseUdcConfirm_Title';
							break;
						case 'S':
							sTitleKey = 'ScrapUdcInput_Title';
							break;
						case 'SG':
							sTitleKey = 'ScrapGoodsInput_Title';
							break;
						case 'P':
							sTitleKey = 'ReprintUdcConfirm_Title';
							break;
						case 'D':
							sTitleKey = 'ShiftDeclInfo_Title';
							break;
						case 'R':
							sTitleKey = 'ReadUdcInput_Title';
							break;
					}
					return oLng_Opr.getText(sTitleKey);
				}
			},
			enableScrolling: false,
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							id: 'scrapUdcConfirmButton',
							visible: {
								parts: [
									{path: "/VIEWTYPE" },
									{path: "/ORIGIN" },
								],		     
								formatter: function(sViewType, sOrigin) {
									return (sViewType !== 'D' && (sViewType !== 'P' || sOrigin !== 'D'));
								}
							},
							icon: {
								parts: [
									{path: "/VIEWTYPE" }
								],		     
								formatter: function(sViewType) {
									var sIcon = '';
									if(sViewType === 'C')
										sIcon = 'sap-icon://inspect-down';
									else if(sViewType === 'S' || sViewType === 'SG')
										sIcon = 'sap-icon://delete';
									else if(sViewType === 'P' || sViewType === 'R')
										sIcon = 'sap-icon://print';
									return sIcon;
								}
							},
							text: {
								parts: [
									{path: "/VIEWTYPE" }
								],		     
								formatter: function(sViewType) {
									var sText = '';
									switch(sViewType)
									{
										case 'C':
											sText = 'CloseUdcConfirm_Close';
											break;
										case 'S':
											sText = 'ScrapUdcInput_Scrap';
											break;
										case 'SG':
											sText = 'ScrapGoodsInput_Scrap';
											break;
										case 'P':
											sText = 'ReprintUdcConfirm_Reprint';
											break;
										case 'R':
											sText = 'ReadUdcInput_Reprint';
											break;
									}
									return oLng_Opr.getText(sText);
								}
							},
							enabled: {
								parts: [
									{path: "/DISABLED_MODE" }
								],		     
								formatter: function(bDisabledMode) {
									return !bDisabledMode;
								}
							},
							press: oController.onConfirm
						}).addStyleClass("sapUiSmallMarginBegin bigButton"),
						new sap.m.Button({
							id: 'reprintUdcConfirmCancelDecl',
							visible: {
								parts: [
									{path: "/VIEWTYPE" },
									{path: "/DELREQ" }
								],		     
								formatter: function(sViewType, sDelReq) {
									if(parseInt(sDelReq) ===  1)
									{
										this.addStyleClass('yellowButton');
										this.removeStyleClass('redButton');
									}
									else
									{
										this.addStyleClass('redButton');
										this.removeStyleClass('yellowButton');
									}
									return sViewType === 'P';
								}
							},
							icon: {
								parts: [
									{path: "/DELREQ" }
								],		     
								formatter: function(sDelReq) {
									var sIcon;
									if(parseInt(sDelReq) ===  1)
										sIcon = 'sap-icon://undo';
									else
										sIcon = 'sap-icon://delete';
									return sIcon;
								}
							},
							text: {
								parts: [
									{path: "/DELREQ" }
								],		     
								formatter: function(sDelReq) {
									var sText;
									if(parseInt(sDelReq) ===  1)
										sText = oLng_Opr.getText('ReprintUdcConfirm_UndoCancel');
									else
										sText = oLng_Opr.getText('ReprintUdcConfirm_CancelDecl');
									return sText;
								}
							},
							enabled: {
								parts: [
									{path: "/DISABLED_MODE" }
								],		     
								formatter: function(bDisabledMode) {
									return !bDisabledMode;
								}
							},
							press: oController.onCancelDecl
						}).addStyleClass("sapUiSmallMarginBegin bigButton"),
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});