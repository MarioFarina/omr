// Controller Dichiarazioni di Produzione V2 Sospesi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.PRODDECL2.prodDecl2Susp", {
	
	_oArguments: undefined,
	_iSuspendedQty: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("prodDecl2Susp").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr','line','order','phase','good','susp']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		//const sLineId = oArguments.line;
		//const sOrder = oArguments.order;
		//const sPhase = oArguments.phase;
		//const sGood = oArguments.good;
		const sSusp = oArguments.susp;
		
		this._iSuspendedQty = parseInt(sSusp);
		$.UIbyID('prodDecl2SuspQtyText').setText(sSusp);
		
		oAppController.loadSubHeaderInfo($.UIbyID('prodDecl2SuspPage'),sPlant,sPernr);
		
		var oSuspTable = $.UIbyID('prodDecl2SuspTable');
		oSuspTable.destroyItems();
		oSuspTable['idcount'] = 0;
		oSuspTable.addItem(
			new sap.m.ColumnListItem({
				type : sap.m.ListType.Active,
				press: this.insertSuspendedItem,
				cells: [
					new sap.m.HBox({
						items: [
							new sap.ui.core.Icon({
								src: "sap-icon://add"
							}),
							new sap.m.Text({
								text: oLng_Opr.getText("ProdDecl2Susp_Add")
							}).addStyleClass("sapUiTinyMarginBegin")
						]
					}),
					new sap.m.Text(),
					new sap.m.Text()
				]
			})
		);
		this.insertSuspendedItem();
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna a Dichiarazioni di Produzione V2 Selezione Quantità
/***********************************************************************************************/

	onNavBack: function() {	
		var oController = sap.ui.getCore().byId('prodDecl2SuspView').getController();
		
		var oProdDecl2QtyArguments = oController._oArguments;
		if(oProdDecl2QtyArguments.hasOwnProperty('good'))
			delete oProdDecl2QtyArguments.good;
		if(oProdDecl2QtyArguments.hasOwnProperty('susp'))
			delete oProdDecl2QtyArguments.susp;
		
		oAppRouter.navTo('prodDecl2Qty',{
			query: oProdDecl2QtyArguments
		});
	},
	
/***********************************************************************************************/
// Inserimento di un nuovo Item nella tabella
/***********************************************************************************************/
	
	insertSuspendedItem: function() {
		var oController = sap.ui.getCore().byId('prodDecl2SuspView').getController();
		
		var oSuspTable = $.UIbyID('prodDecl2SuspTable');
		var iItemsCount = oSuspTable.getItems().length - 1;
		const sQtyInputId = 'prodDecl2SuspItemQty' + oSuspTable['idcount'];
		
		var sQuantity = '';
		if(iItemsCount === 0)
			sQuantity =  oController._iSuspendedQty;
		
		var oCausalInput = new sap.m.Input();
		
		var oSuspColumnListItem = new sap.m.ColumnListItem({
			//type : sap.m.ListType.Active,
			cells: [
				oCausalInput,
				new sap.m.HBox({
					items: [
						new sap.m.Button({
							icon: "sap-icon://less",
							press:  [oAppController.onChangeQuantity,{
								add:false,
								id: sQtyInputId
							}]
						}),
						new sap.m.Input({
							id: sQtyInputId,
							type: sap.m.InputType.Number,
							textAlign: sap.ui.core.TextAlign.Right,
							width: "80px",
							value: sQuantity
						}),
						new sap.m.Button({
							icon: "sap-icon://add",
							press: [oAppController.onChangeQuantity,{
								add:true,
								id: sQtyInputId
							}]
						})
					]
				}),
				(iItemsCount === 0) ? 
					new sap.m.Text() :
					new sap.m.Button({
						icon: "sap-icon://delete",
						press: function() {	
							oSuspColumnListItem.destroy();
						}
					})
			]
		});
		
		oSuspTable.insertItem(oSuspColumnListItem,iItemsCount);
		oSuspTable['idcount'] += 1;
		
		jQuery.sap.delayedCall(1000, null, function() {
			oCausalInput.focus();
		});
	},
	
/***********************************************************************************************/
// Verifica dei dati inseriti e completa l'operazione
/***********************************************************************************************/
	
	onComplete: function() {
		var oController = sap.ui.getCore().byId('prodDecl2SuspView').getController();
		
		var oSuspTable = $.UIbyID('prodDecl2SuspTable');
		var aSuspTableItems = oSuspTable.getItems();
		
		var sErrorMessage;
		var iTotalQty = 0;
		for(var i=0; i<aSuspTableItems.length-1; i++)
		{
			var aCells = aSuspTableItems[i].getCells();
			var oCausalInput = aCells[0];
			var oQtyInput = aCells[1].getItems()[1];
			var iQtyValue = parseInt(oQtyInput.getValue());
			
			if(!oCausalInput.getValue())
			{
				sErrorMessage = oLng_Opr.getText("ProdDecl2Susp_CausalErrorMsg");
				oCausalInput.setValueStateText(sErrorMessage);
				oCausalInput.setValueState(sap.ui.core.ValueState.Warning);
				break;
			}
			else if(oCausalInput.getValueState() === sap.ui.core.ValueState.Warning)
				oCausalInput.setValueState(sap.ui.core.ValueState.None);
			
			if(isNaN(iQtyValue) || iQtyValue<=0)
			{
				sErrorMessage = oLng_Opr.getText("ProdDecl2Susp_QtyErrorMsg");
				oQtyInput.setValueStateText(sErrorMessage);
				oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
				break;
			}
			else{
				if(oQtyInput.getValueState() === sap.ui.core.ValueState.Warning)
					oQtyInput.setValueState(sap.ui.core.ValueState.None);
				iTotalQty += iQtyValue;
			}
		}
		
		if(iTotalQty !== oController._iSuspendedQty)
			sErrorMessage = oLng_Opr.getText("ProdDecl2Susp_TotalErrorMsg");
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oProdDecl2Arguments = oController._oArguments;
			if(oProdDecl2Arguments.hasOwnProperty('line'))
				delete oProdDecl2Arguments.line;
			if(oProdDecl2Arguments.hasOwnProperty('order'))
				delete oProdDecl2Arguments.order;
			if(oProdDecl2Arguments.hasOwnProperty('phase'))
				delete oProdDecl2Arguments.phase;
			if(oProdDecl2Arguments.hasOwnProperty('good'))
				delete oProdDecl2Arguments.good;
			if(oProdDecl2Arguments.hasOwnProperty('susp'))
				delete oProdDecl2Arguments.susp;
			oAppRouter.navTo('prodDecl2',{
				query: oProdDecl2Arguments
			});
			
			sap.m.MessageToast.show(oLng_Opr.getText("ProdDecl2Susp_Completed"), {
				duration: 3000,
				animationDuration: 1000,
				closeOnBrowserNavigation: false
			});
		}
	}

});