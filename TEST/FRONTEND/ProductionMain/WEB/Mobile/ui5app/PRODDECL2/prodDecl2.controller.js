// Controller Dichiarazioni di Produzione V2
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.PRODDECL2.prodDecl2", {
	
	_oArguments: undefined,
	_sShiftDate: undefined,
	_sShiftId: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("prodDecl2").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		var aShiftInfo = oAppController.loadSubHeaderInfo($.UIbyID('prodDecl2Page'),sPlant,sPernr);
		this._sShiftDate = aShiftInfo[2];
		this._sShiftId = aShiftInfo[1];

		// Combo per la selezione della linea						
		var oLineComboTemplate = new sap.ui.core.Item({
            text: "{LINETXT}",
            key: "{IDLINE}"
        }); 
		  
		var oLineCombo = $.UIbyID('prodDecl2LineCombo');
		oLineCombo.bindAggregation("items", "/Rowset/Row", oLineComboTemplate);
		
		var oLinesModel = new sap.ui.model.xml.XMLModel();
		oLinesModel.loadData(QService + sMasterDataPath + "Lines/getLinesbyPlantSQ&Param.1=" + sPlant,false,false);
		oLineCombo.setModel(oLinesModel);
		
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oSettings = oStorage.get("WORKST_INFO");
		var sSelectedLine;
		if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
		{
			if(oSettings.LINES.length === 1)
			{
				oLineCombo.setEnabled(false);
				sSelectedLine = oSettings.LINES[0];
			}
			else if(oSettings.LINES.length > 1)
			{
				oLineCombo.setEnabled(true);
				var aAllFilters = [];
				for(var i=0; i<oSettings.LINES.length; i++)
				{
					var oFilter = new sap.ui.model.Filter("IDLINE", sap.ui.model.FilterOperator.EQ, oSettings.LINES[i]);
					aAllFilters.push(oFilter);
				}
				var oAllFilters = new sap.ui.model.Filter(aAllFilters); 
				var oBinding = oLineCombo.getBinding("items");
				oBinding.filter(oAllFilters);
			}
		}
		else
		{
			oLineCombo.setEnabled(true);
			oLineCombo.setValue();
		}
		oLineCombo.setSelectedKey(sSelectedLine);
		
		var oProdDecl2OrderPhaseInput = $.UIbyID('prodDecl2OrderPhaseInput');
		oProdDecl2OrderPhaseInput.setValue();
		oProdDecl2OrderPhaseInput.setValueState(sap.ui.core.ValueState.None);
		oProdDecl2OrderPhaseInput.setValueStateText();
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {	
		var oController = sap.ui.getCore().byId('prodDecl2View').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Verifica dei dati inseriti
/***********************************************************************************************/
	
	onProcess: function() {
		var oController = sap.ui.getCore().byId('prodDecl2View').getController();
		
		var oProdDecl2LineCombo = $.UIbyID('prodDecl2LineCombo');
		oProdDecl2LineCombo.setValueState(sap.ui.core.ValueState.None);
		oProdDecl2LineCombo.setValueStateText();
		
		var oProdDecl2OrderPhaseInput = $.UIbyID('prodDecl2OrderPhaseInput');
		oProdDecl2OrderPhaseInput.setValueState(sap.ui.core.ValueState.None);
		oProdDecl2OrderPhaseInput.setValueStateText();
		
		var sLine = oProdDecl2LineCombo.getSelectedKey();		
		var sOrderPhase = oProdDecl2OrderPhaseInput.getValue();
		var sOrder;
		var sPhase;
		
		var sErrorMessage;
		if(!sLine)
		{
			sErrorMessage = oLng_Opr.getText("ProdDecl2_LineErrorMsg");
			oProdDecl2LineCombo.setValueState(sap.ui.core.ValueState.Warning);
			oProdDecl2LineCombo.setValueStateText(sErrorMessage);
		}
		else
		{
			if(sOrderPhase.length < 12)
				sErrorMessage = oLng_Opr.getText("ProdDecl_OrderErrorMsg");
			else
			{
				sOrder = sOrderPhase.substring(0,12);
				sPhase = sOrderPhase.substring(12,sOrderPhase.length);
				
				sap.ui.core.BusyIndicator.show(1000);
				var sQuery = sMasterDataPath + "Orders/checkOrderSQ";
				sQuery += "&Param.1=" + oController._oArguments.plant;
				sQuery += "&Param.2=" + sOrder;
				var iCheckResult = parseInt(fnGetAjaxVal(sQuery,["Answer"],false));
				
				if(!iCheckResult)
					sErrorMessage = oLng_Opr.getText("ProdDecl_OrderErrorMsg");
				else if(!sPhase)
					sErrorMessage = oLng_Opr.getText("ProdDecl_PhaseErrorMsg");
				/*
				else
				{
					var sQuery = sConfirmPath + "ConfProd/checkConfProdSQ";
					sQuery += "&Param.1=" + oController._oArguments.plant;
					sQuery += "&Param.2=" + sLine;
					sQuery += "&Param.3=" + oController._sShiftDate;
					sQuery += "&Param.4=" + oController._sShiftId;
					sQuery += "&Param.5=" + sOrder;
					iCheckResult = parseInt(fnGetAjaxVal(sQuery,["Answer"],false));
					if(!iCheckResult)
						sErrorMessage = oLng_Opr.getText("ProdDecl2_ConfProdErrorMsg");
				}
				*/
				
				sap.ui.core.BusyIndicator.hide();
			}
			
			if(sErrorMessage)
			{
				oProdDecl2OrderPhaseInput.setValueState(sap.ui.core.ValueState.Warning);
				oProdDecl2OrderPhaseInput.setValueStateText(sErrorMessage);
			}
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Attenzione",
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oProdDecl2Arguments = oController._oArguments;
			oProdDecl2Arguments['line'] = sLine;
			oProdDecl2Arguments['order'] = sOrder;
			oProdDecl2Arguments['phase'] = sPhase;

			oAppRouter.navTo('prodDecl2Qty',{
				query: oProdDecl2Arguments
			});
		}
	}

});