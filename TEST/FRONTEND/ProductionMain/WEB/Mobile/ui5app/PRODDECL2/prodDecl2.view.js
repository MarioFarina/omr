// View Dichiarazioni di Produzione V2
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PRODDECL2.prodDecl2", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PRODDECL2.prodDecl2";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oForm = new sap.ui.layout.form.SimpleForm({
			editable: true,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 3,
			labelSpanM: 3,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2_Line")
				}).addStyleClass("sapUiMediumMarginTop"),
				new sap.m.ComboBox({
					id: 'prodDecl2LineCombo',
					layoutData: new sap.ui.layout.form.GridElementData({
						hCells: "6"
					}),
				}).addStyleClass("sapUiMediumMarginTop"),
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2_OrderPhase")
				}).addStyleClass("sapUiSmallMarginBottom"),
				new sap.m.Input({
					id: 'prodDecl2OrderPhaseInput',
					layoutData: new sap.ui.layout.form.GridElementData({
						hCells: "6"
					})
				}).addStyleClass("sapUiSmallMarginBottom")
			]
		});
						  
		var oPage = new sap.m.Page({
			id: "prodDecl2Page",
			enableScrolling: false,
			title: oLng_Opr.getText("ProdDecl2_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oForm,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("ProdDecl2_Confirm"),
							icon: "sap-icon://message-success",
							press: oController.onProcess
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});
		
	  	return oPage;
	}

});