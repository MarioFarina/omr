// View Dichiarazioni di Produzione V2 Selzione Quantità
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PRODDECL2.prodDecl2Qty", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PRODDECL2.prodDecl2Qty";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oFormText = new sap.ui.layout.form.SimpleForm({
			editable: false,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 4,
			labelSpanM: 4,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2Qty_Material")
				}),
				new sap.m.Text({
					id: 'prodDecl2MaterialText'
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2Qty_Proposed")
				}),
				new sap.m.Text({
					id: 'prodDecl2ProposedQtyText',
					text: "-"
				})
			]
		});
		
		var oFormInput = new sap.ui.layout.form.SimpleForm({
			editable: true,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 4,
			labelSpanM: 4,
			emptySpanL: 4,
			emptySpanM: 4,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2Qty_Goods")
				}),
				new sap.m.HBox({
					items: [
						new sap.m.Button({
							icon: "sap-icon://less",
							press:  [oAppController.onChangeQuantity,{
								add:false,
								id: 'prodDecl2GoodQty'
							}]
						}),
						new sap.m.Input({
							id: "prodDecl2GoodQty",
							type: sap.m.InputType.Number,
							textAlign: sap.ui.core.TextAlign.Right,
						}),
						new sap.m.Button({
							icon: "sap-icon://add",
							press: [oAppController.onChangeQuantity,{
								add:true,
								id: 'prodDecl2GoodQty'
							}]
						})
					]
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("ProdDecl2Qty_Suspended")
				}),
				new sap.m.HBox({
					items: [
						new sap.m.Button({
							icon: "sap-icon://less",
							press:  [oAppController.onChangeQuantity,{
								add:false,
								id: 'prodDecl2SuspQty'
							}]
						}),
						new sap.m.Input({
							id: "prodDecl2SuspQty",
							type: sap.m.InputType.Number,
							textAlign: sap.ui.core.TextAlign.Right,
						}),
						new sap.m.Button({
							icon: "sap-icon://add",
							press: [oAppController.onChangeQuantity,{
								add:true,
								id: 'prodDecl2SuspQty'
							}]
						})
					]
				}).addStyleClass("sapUiSmallMarginBottom")
			]
		});
						  
		var oPage = new sap.m.Page({
			id: "prodDecl2QtyPage",
			enableScrolling: false,
			title: oLng_Opr.getText("ProdDecl2Qty_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oFormText,
				oFormInput,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("ProdDecl2Qty_Confirm"),
							icon: "sap-icon://message-success",
							press: oController.onProcess
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});
		
	  	return oPage;
	}

});