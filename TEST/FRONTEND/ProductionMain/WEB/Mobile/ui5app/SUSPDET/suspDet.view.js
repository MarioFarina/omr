// View Dettaglio sospesi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SUSPDET.suspDet", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SUSPDET.suspDet";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oForm = new sap.ui.layout.form.SimpleForm({
			editable: true,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 3,
			labelSpanM: 3,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("SuspDet_DataMatrix")
				}).addStyleClass("sapUiMediumMarginTop"),
				new sap.m.Input({
					id: 'suspDetDMInput'
				}).addStyleClass("sapUiMediumMarginTop sapUiTinyMarginBottom")
			]
		});
		
		var oPage = new sap.m.Page({
			id: "suspDetPage",
			enableScrolling: false,
			title: oLng_Opr.getText("SuspDet_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oForm,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("SuspDet_Repair"),
							icon: "sap-icon://wrench",
							press: [oController.onNext,{navname:'suspDetRep'}]
						}).addStyleClass("sapUiSmallMarginBegin bigButton"),
						new sap.m.Button({
							text: oLng_Opr.getText("SuspDet_Discard"),
							icon: "sap-icon://delete",
							press: [oController.onNext,{navname:'suspDetDisc'}]
						}).addStyleClass("sapUiSmallMarginBegin bigButton"),
						new sap.m.Button({
							text: oLng_Opr.getText("SuspDet_Yeld"),
							icon: "sap-icon://undo",
							press: [oController.onNext,{navname:'suspDetYeld'}]
						}).addStyleClass("sapUiSmallMarginBegin bigButton")
					]
				}).addStyleClass("sapUiSmallMarginTop"),
				new sap.m.HBox().addStyleClass("sapUiMediumMarginTop"),
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("SuspDet_Good"),
							icon: "sap-icon://accept",
							press: [oController.onNext,{navname:'suspDetGood'}]
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiLargeMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});