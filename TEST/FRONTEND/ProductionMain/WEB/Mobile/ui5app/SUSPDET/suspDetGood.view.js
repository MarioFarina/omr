// View Dettaglio sospesi - Ripara
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SUSPDET.suspDetGood", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SUSPDET.suspDetGood";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oPage = new sap.m.Page({
			id: "suspDetGoodPage",
			enableScrolling: false,
			title: oLng_Opr.getText("SuspDetGood_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});