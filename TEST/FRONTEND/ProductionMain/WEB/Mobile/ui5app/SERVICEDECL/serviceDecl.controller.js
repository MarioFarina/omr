// Controller Dichiarazione Servizio
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SERVICEDECL.serviceDecl", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("serviceDecl").attachPatternMatched(this.onObjectMatched, this);

		var nextFunc = this.onNext;
		var oOrderInput = sap.ui.getCore().byId("serviceDeclInput");
		
		oOrderInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('serviceDeclPage'),sPlant,sPernr);
		
		var oOrderInput = sap.ui.getCore().byId("serviceDeclInput");
		oOrderInput.setValue();
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		oOrderInput.setValueStateText();
		
		jQuery.sap.delayedCall(500, null, function() {
			oOrderInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('serviceDeclView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
	onNext: function() {
		var oOrderInput = $.UIbyID('serviceDeclInput');
		const sOrderPhase = oOrderInput.getValue();
		
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		
		const sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase, selezione linea e udc nuovo o incompleto
		{						
			const aSpecialWarningCodes = [2,3];
			
			var oController = sap.ui.getCore().byId('serviceDeclView').getController();
			var oArguments = oController._oArguments;
			
			var sCheckQuery = sMovementPath + "ProdDecl/checkOrderAndPoperQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sOrderPhase;
			sCheckQuery += "&Param.3=";
			sCheckQuery += "&Param.4=0";
			sCheckQuery += "&Param.5=1";
			sCheckQuery += "&Param.6=" + sLanguage;
	
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onCheckSuccess,
				showWarning: false,
				onWarning: oController.onCheckWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,
					successFunction: oController.onCheckSuccess,
					arguments: oArguments,
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant
				},
				results: {
					returnProperties: ['outputLines','cicTxt','orderType']
				}
			});
		}
	},
	
	onCheckWarning: function(oResults,oCallbackParams) // passaggio alla schermata successiva
	{
		if(oCallbackParams.warningCodes.indexOf(parseInt(oResults.code)) > -1)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("ServiceDecl_OrderErrorMessage"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
			oCallbackParams.successFunction(oResults,oCallbackParams);
	},
	
	onCheckSuccess: function(oResults,oCallbackParams) // passaggio alla schermata successiva
	{
		var oFuncData = {
			ORDER: oCallbackParams.order,
			POPER: oCallbackParams.poper,
			CICTXT: oResults.cicTxt
		};
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oFuncData);
		
		var oArguments = oCallbackParams.arguments;
		
		oArguments['order'] = oCallbackParams.order;
		oArguments['poper'] = oCallbackParams.poper;
		
		oAppRouter.navTo('serviceDeclInput',{
			query: oArguments
		});
	}
});