//MF20191220
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.DEPARTMENTLOGIN.departmentlogin", {

	_oArguments: undefined,
	
	pernr: null,
	plant: null,
	department: null,

	onInit: function () {
		oAppRouter.getRoute('departmentlogin').attachPatternMatched(this.onObjectMatched, this);
	},

	onObjectMatched: function (oEvent) {

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put("FUNC_INFO",false);

		var oAppController = sap.ui.getCore().byId('app').getController();

		var oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;

		this.pernr = oArguments.pernr;
		this.plant = oArguments.plant;
		
		var oDepartmentsModelXML = new sap.ui.model.xml.XMLModel();
		
		var sLoadQuery = QService + sMasterDataPath + "Departments/GetDepartmentLogin_XQ"; 
		sLoadQuery += "&Param.1=" + this.plant;
		sLoadQuery += "&Param.2=" + this.pernr;

		oDepartmentsModelXML.loadData(sLoadQuery);
	
		this.getView().setModel(oDepartmentsModelXML);
	},

	onFunctionSelected: function(oEvent) {
		
		var oController = sap.ui.getCore().byId("departmentloginView").getController();
		var oContext = oEvent.getSource().getBindingContext();	
		oController.department = oEvent.oSource.mProperties.info.split("(")[0].trim();
			
		oAppRouter.navTo("linelogin", {
			query: {"pernr": oController.pernr, "plant": oController.plant, "department": oController.department}
		}, true);
	},
	
	onLogout: function () {
		var oController = sap.ui.getCore().byId('departmentloginView').getController();
		oController._aParents = [];
	},
	
	onNavBack: function() {
		var oController = sap.ui.getCore().byId('departmentloginView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
});
