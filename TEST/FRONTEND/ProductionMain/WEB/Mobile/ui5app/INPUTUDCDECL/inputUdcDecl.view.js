// View Dichiarazione UdC in ingresso
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.INPUTUDCDECL.inputUdcDecl", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.INPUTUDCDECL.inputUdcDecl";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oForm = new sap.ui.layout.form.SimpleForm({
			editable: true,
			layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
			labelSpanL: 3,
			labelSpanM: 3,
			emptySpanL: 3,
			emptySpanM: 3,
			columnsL: 1,
			columnsM: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("InputUdcDecl_OrderPhase")
				}).addStyleClass("sapUiMediumMarginTop"),
				new sap.m.Input({
					id: 'inputUdcDeclOrderInput'
				}).addStyleClass("sapUiMediumMarginTop"),
				new sap.m.Label({
					text: oLng_Opr.getText("InputUdcDecl_Udc")
				}).addStyleClass("sapUiSmallMarginBottom"),
				new sap.m.Input({
					id: 'inputUdcDeclUdcInput'
				}).addStyleClass("sapUiSmallMarginBottom")
			]
		});
						  
		var oPage = new sap.m.Page({
			id: "inputUdcDeclPage",
			enableScrolling: false,
			title: oLng_Opr.getText("InputUdcDecl_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oForm,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("InputUdcDecl_Confirm"),
							icon: "sap-icon://message-success",
							press: oController.onProcess
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});
		
	  	return oPage;
	}

});