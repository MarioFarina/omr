// View Riparazione / Rilavorazione Input
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.REPUDC.repUdcInput", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.REPUDC.repUdcInput";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {

		var oView = this;
		
		var idTextFormatter = function(sId,sTxt) {
			return sId + " " + sTxt;
		};
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: '{/REW_ORDER}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/REW_POPER" },
							{path: "/REW_CICTXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/REW_CICTXT',
						description: [
							oLng_Opr.getText('General_Poper') + ': ',
							'{/REW_POPER}',
							'\n',
							'{/REW_CICTXT}',
							'\n\n',
							oLng_Opr.getText('General_CDL') + ': ',
							'{/REW_CDLID}',
							'\n',
							'{/REW_CDLDESC}'
						]
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/REW_LINEID" },
							{path: "/REW_LINETXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/REW_LINEID',
						description: '/REW_LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/REW_MATERIAL" },
							{path: "/REW_MATDESC"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/REW_MATERIAL',
						description: '/REW_MATDESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_UDCFrom") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: '{/UDCNR}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_QtyFrom") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12"
					})
				}),
				new sap.m.Text({
					text: '{/QTYRES}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_UDCTo") + ':',
					visible: {
						parts: [
							{path: "/UDCTO" }
						],		     
						formatter: function(sUdcTo) {
							return (sUdcTo) ? true : false;
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: '{/UDCTO}',
					visible: {
						parts: [
							{path: "/UDCTO" }
						],		     
						formatter: function(sUdcTo) {
							return (sUdcTo) ? true : false;
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: {
						parts: [
							{path: "/ORDTYPE" }
						],		     
						formatter: function(sOrdType) {
							var sTextKey;
							if(sOrdType === "ZRIP")
								sTextKey = "RepUdcInput_RepairState";
							else
								sTextKey = "RepUdcInput_ReworkState";
							return oLng_Opr.getText(sTextKey) + ':';
						}
					},
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.SegmentedButton({
					id: 'repUdcInputState',
					items: [
						new sap.m.SegmentedButtonItem({
							text: '',
							key: '',
							visible:false,
							press:  function() {
								var oConfirmButton = $.UIbyID("repUdcInputConfirmButton");
								oConfirmButton.removeStyleClass('greenButton');
								oConfirmButton.removeStyleClass('yellowButton');
							}
						}),
						new sap.m.SegmentedButtonItem({
							text: {
								parts: [
									{path: "/ORDTYPE" }
								],		     
								formatter: function(sOrdType) {
									var sTextKey;
									if(sOrdType === "ZRIP")
										sTextKey = "RepUdcInput_Repairing";
									else
										sTextKey = "RepUdcInput_Suspended";
									return oLng_Opr.getText(sTextKey);
								}
							},
							key: 'INCOMPLETE',
							press:  function() {
								if(oView.getModel().getProperty("/ORDTYPE") !== "ZRIP")
								{
									var oConfirmButton = $.UIbyID("repUdcInputConfirmButton");
									oConfirmButton.removeStyleClass('greenButton');
									oConfirmButton.addStyleClass('yellowButton');
								}
							}
						}),
						new sap.m.SegmentedButtonItem({
							text: {
								parts: [
									{path: "/ORDTYPE" }
								],		     
								formatter: function(sOrdType) {
									var sTextKey;
									if(sOrdType === "ZRIP")
										sTextKey = "RepUdcInput_Completed";
									else
										sTextKey = "RepUdcInput_Good";
									return oLng_Opr.getText(sTextKey);
								}
							},
							enabled: {
								parts: [
									{path: "/ORDTYPE" },
									{path: "/CTRLOPER" },
									{path: "/NOCOMPLETED" },
								],		     
								formatter: function(sOrdType, sCtrlOper,sNoCompleted) {
									return ((sOrdType === "ZRIP" && sNoCompleted !== "1") || 
										(sOrdType !== "ZRIP" && sCtrlOper === "1"));
								}
							},
							key: 'COMPLETE',
							press:  function() {	
								if(oView.getModel().getProperty("/ORDTYPE") !== "ZRIP")
								{
									var oConfirmButton = $.UIbyID("repUdcInputConfirmButton");
									oConfirmButton.removeStyleClass('yellowButton');
									oConfirmButton.addStyleClass('greenButton');
								}
							}
						})
					],
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'repUdcInputBasketTT',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_WorkTime") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'repUdcInputWorkTime',
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Destination") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M1 S12",
					})
				}).addStyleClass("inputLabel"),
				new sap.m.ComboBox({
					id: 'repUdcInputNextPhase',
					width : "100%",
					selectionChange: function() {
						if(this.getSelectedKey() === 'ALTR')
						{
							this.setValueState(sap.ui.core.ValueState.Warning);
							this.setValueStateText(oLng_Opr.getText("RepUdcInput_OtherDestinationMessage"));
							this.setValue();
							this.setSelectedKey();
						}
						else
						{
							this.setValueState(sap.ui.core.ValueState.None);
							this.setValueStateText();
						}
					},
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).bindAggregation("items","/Rowset/Row",
					new sap.ui.core.Item({
						key : '{ID_REP}',
						text: {
							parts: [
								{path: 'ID_REP'},
								{path: 'DESC'}
							],		     
							formatter: function(sIdRep,sDesc){
								return sIdRep + ' - ' + sDesc;
							}
						}
					})
				),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Quantity") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new NumericInput({
					id: 'repUdcInputQty',
					min: 0,
					width: '80px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("RepUdcInput_Dater") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.MaskInput({
					id: 'repUdcInputDater',
					mask: "9999",
					placeholderSymbol: " ",
					placeholder: 'MMYY',
					width: '160px',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				})
			]
		}).addStyleClass("sapUiTinyMarginTop");
		
		var oStdQtyLabel = new sap.m.Label({
			text: oLng_Opr.getText("RepUdcInput_StdQty") + ':',
			width: '100%',
			textAlign: sap.ui.core.TextAlign.Right,
			layoutData : new sap.ui.layout.GridData({
				span: "L8 M8 S12",
				linebreak: true
			})
		}).addStyleClass("inputLabel");
		oView.addDependent(oStdQtyLabel);
		
		var oStdQtyTemplate = new sap.ui.core.Item({
			key : "{NMIMB}",
			text: {
				parts: [
					{path: 'NMIMB'},
					{path: 'DESCR'},
					{path: 'QTY'}
				],		     
				formatter: function(sNmimb, sDesc, sQty){
					return sQty + ' (' + sNmimb + ' ' + sDesc + ')';
				}
			}
		});
		var oStdQtyCombo = new sap.m.ComboBox({
			id: 'repUdcInputStdQty',
			width: '100%',
			layoutData : new sap.ui.layout.GridData({
				span: "L3 M3 S12"
			})
		}).bindAggregation("items","/Rowset/Row",oStdQtyTemplate);
		oView.addDependent(oStdQtyCombo);
		
		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
		var oStdQtyIcon = new sap.ui.core.Icon({
			src: "sap-icon://value-help",
			size: "18px",
			press: [oBoxDeclInputController.onSelectNmimb,{
				view:oView,
				combo:oStdQtyCombo
			}],
			tooltip: oLng_Opr.getText('RepUdcInput_SelectNmimb'),
			layoutData : new sap.ui.layout.GridData({
				span: "L1 M1 S12"
			})
		}).addStyleClass("inputLabel");
		oView.addDependent(oStdQtyIcon);
		
		oView.attachModelContextChange(function () {
			var oModel = this.getModel();
			if(oModel)
			{
				const sOrdType = oModel.getProperty("/ORDTYPE");
				if(sOrdType === "ZRIP")
				{				
					oGridForm.removeContent(oStdQtyLabel);
					oGridForm.removeContent(oStdQtyCombo);
					oGridForm.removeContent(oStdQtyIcon);
				}
				else
				{
					oGridForm.addContent(oStdQtyLabel);
					oGridForm.addContent(oStdQtyCombo);
					oGridForm.addContent(oStdQtyIcon);
				}
			}
		});
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "repUdcInputPage",
			enableScrolling: false,
			title: {
				parts: [
					{path: "/ORDTYPE" }
				],		     
				formatter: function(sOrdType) {
					var sTextKey;
					if(sOrdType === "ZRIP")
						sTextKey = "RepUdcInput_RepairTitle";
					else
						sTextKey = "RepUdcInput_ReworkTitle";
					return oLng_Opr.getText(sTextKey);
				}
			},
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							id: 'repUdcInputConfirmButton',
							enabled: {
								parts: [
									{path: "/PRINT_MODE" },
									{path: "/DISABLED_MODE" }
								],		     
								formatter: function(bPrintMode, bDisabledMode) {
									return (bDisabledMode) ? false : !bPrintMode;
								}
							},
							text: oLng_Opr.getText("RepUdcInput_Confirm"),
							icon: "sap-icon://accept",
							press: oController.onConfirm
						}).addStyleClass("sapUiSmallMarginBegin bigButton"),
						new sap.m.Button({
							enabled: {
								parts: [
									{path: "/PRINT_MODE" },
									{path: "/DISABLED_MODE" }
								],		     
								formatter: function(bPrintMode, bDisabledMode) {
									return (bDisabledMode || !bPrintMode) ? false : true;
								}
							},
							text: oLng_Opr.getText("RepUdcInput_Print"),
							icon: "sap-icon://print",
							press: oController.onReprint
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
					]
				}).addStyleClass("sapUiTinyMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});