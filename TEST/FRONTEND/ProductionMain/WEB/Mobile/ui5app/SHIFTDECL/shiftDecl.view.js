// View Dichiarazione fine turno
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SHIFTDECL.shiftDecl", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SHIFTDECL.shiftDecl";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oTable = new sap.m.Table({
        	id: 'shiftDeclTable',
    		mode: sap.m.ListMode.SingleSelectMaster/*{
				parts: [
					{path: "view>/VIEWTYPE" }
				],		     
				formatter: function(sViewType){
					return (sViewType == 'P') ? 
						sap.m.ListMode.SingleSelectMaster : sap.m.ListMode.None;
				}
			}*/,
			itemPress: oController.onItemPressed,
    		columns: [
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Order")
					}),
		        	width: "105px"
		        }),
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Phase")
					})/*,
		        	width: "130px"*/
		        }),
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Udc")
					}),
		        	width: "155px"
		        }),
				new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Material")
					})/*,
		        	width: "140px"*/
		        }),
				new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Type")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "70px"
		        }),/*
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_UdcCapacity")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "70px"
		        }),*/
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_QtyDeclared")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "70px"
		        }),
				new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_FullBox")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "60px"
		        })/*,
				new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_Duration")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "50px"
		        }),
		        new sap.m.Column({
		        	header: new sap.m.Text({
						text: oLng_Opr.getText("ShiftDecl_StdPack")
					}),
					hAlign: sap.ui.core.TextAlign.End,
		        	width: "60px"
		        })*/
    		]
        });
		oTable.addStyleClass('mediumSizeTblCell');
		
        var oTemplate = new sap.m.ColumnListItem({
			type : sap.m.ListType.Active/*{
				parts: [
					{path: "view>/VIEWTYPE" }
				],		     
				formatter: function(sViewType){
					return (sViewType == 'P') ? 
						sap.m.ListType.Active : sap.m.ListType.Inactive;
				}
			}*/,
			cells: [
		        new sap.m.Text({
		        	text: "{ORDER}"
		        }),
				new sap.m.Text({
		        	text: "{CICTXT}"
		        }),
				new sap.m.Text({
		        	text: "{UDCNR}"
		        }),
				new sap.m.Text({
		        	text: "{MATERIAL}"
		        }),
				new sap.m.Text({
					text: {
						parts: [
							{path: "ORIGIN" },
							{path: "IDLINE" }
						],		     
						formatter: function(sOrigin,sIdLine) {
							var sOriginTxt;
							switch(sOrigin) {
								case 'M':
									sOriginTxt = oLng_Opr.getText("ShiftDecl_Goods");
									break;
								case 'C':
									sOriginTxt = oLng_Opr.getText("ShiftDecl_Suspended");
									break;
								case 'P':
									sOriginTxt = oLng_Opr.getText("ShiftDecl_PAC");
									break;
								case 'D':
									sOriginTxt = oLng_Opr.getText("ShiftDecl_Scrap");
									break;
								default:
									if(sIdLine === '_SRV')
										sOriginTxt = oLng_Opr.getText("ShiftDecl_Service");
									else
										sOriginTxt = sOrigin;
							}
							
							return sOriginTxt;
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M4 S12"
					})
				}),/*
				new sap.m.Text({
					text: {
						parts: [
							{path: "PZUDC" },
							{path: "IDLINE" }
						],
						formatter: function(sPzUdc,sIdLine){
							var sPzUdcText;
							if(sIdLine === '_SRV')
								sPzUdcText = ' ';
							else
								sPzUdcText = sPzUdc;
							return sPzUdcText;
						}
					}
		        }),*/
				new sap.m.Text({
		        	text: "{QTY}"
		        }),
				new sap.m.Text({
					text: {
						parts: [
							{path: "FULL" },
							{path: "IDLINE" }
						],		     
						formatter: function(sFull,sIdLine){
							var sFullText;
							if(sIdLine === '_SRV')
								sFullText = ' ';
							else
								sFullText = ((sFull == '1') ? 
									oLng_Opr.getText("ShiftDecl_Yes") : 
									oLng_Opr.getText("ShiftDecl_No"));
							return sFullText;
						}
					}
				})/*,
				new sap.m.Text({
		        	text: "{WTDUR}"
		        }),
				new sap.m.Text({
					text: {
						parts: [
							{path: "STDIMB" }
						],		     
						formatter: function(sStdImb){ 
							return (sStdImb == '1') ? 
								oLng_Opr.getText("ShiftDecl_Yes") : 
								oLng_Opr.getText("ShiftDecl_No");
						}
					}
				})*/
			],
			visible: {
				parts: [
					{path: "CONFIRMED" },
					{path: "DELREQ" }
				],
				formatter: function(sConfirmed,sDelReq){
					var sColorClass = '';
					this.removeStyleClass('greenTableRow');
					this.removeStyleClass('redTableRow');
					
					if(parseInt(sConfirmed) === 1)
						sColorClass = 'greenTableRow';					
					if(parseInt(sDelReq) === 1)
						sColorClass = 'redTableRow';
					
					this.addStyleClass(sColorClass);
					return true;
				}
			}
		});
        oTable.bindAggregation("items","/Rowset/Row",oTemplate);
		
		var oPage = new sap.m.Page({
			id: "shiftDeclPage",
			//enableScrolling: false,
			title: {
				parts: [
					{path: "view>/VIEWTYPE" }
				],		     
				formatter: function(sViewType) {
					var sTitleKey = '';
					switch(sViewType)
					{
						case 'D':
							sTitleKey = 'ShiftDecl_Title';
							break;
						case 'P':
							sTitleKey = 'ReprintUdc_Title';
							break;
					}
					return oLng_Opr.getText(sTitleKey);
				}
			},
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oTable
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.Button({
						icon: "sap-icon://date-time",
						text: oLng_Opr.getText("ShiftDecl_ChangeDateShift"),
						press: oController.changeDateShift,
						visible: {
							parts: [
								{path: "view>/VIEWTYPE" }
							],		     
							formatter: function(sViewType) {
								return sViewType === 'P';
							}
						}
					}).addStyleClass("yellowButton noBorderButton"),
					new sap.m.Text({
						id: 'shiftDeclDate',
						visible: {
							parts: [
								{path: "view>/VIEWTYPE" }
							],		     
							formatter: function(sViewType) {
								return sViewType === 'P';
							}
						}
					}),
					new sap.m.Text({
						id: 'shiftDeclShift',
						visible: {
							parts: [
								{path: "view>/VIEWTYPE" }
							],		     
							formatter: function(sViewType) {
								return sViewType === 'P';
							}
						}
					}),
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://accept",
						text: oLng_Opr.getText("ShiftDecl_Confrim"),
						press: oController.onConfirm,
						visible: {
							parts: [
								{path: "view>/VIEWTYPE" }
							],		     
							formatter: function(sViewType) {
								return sViewType === 'D';
							}
						}
					}).addStyleClass("greenButton noBorderButton"),
					new sap.m.Button({
						icon: "sap-icon://print",
						text: {
							parts: [
								{path: "view>/VIEWTYPE" }
							],		     
							formatter: function(sViewType) {
								var sButtonText;
								switch(sViewType) {
									case 'D':
										this.addStyleClass("orangeButton");
										this.removeStyleClass("blueButton");
										sButtonText = oLng_Opr.getText("ShiftDecl_NotConfirm");
										break;
									case 'P':
										this.addStyleClass("blueButton");
										this.removeStyleClass("orangeButton");
										sButtonText = oLng_Opr.getText("ShiftDecl_PrintConfirms");
								}
								return sButtonText;
							}
						},
						press: oController.onPrint
					}).addStyleClass("orangeButton noBorderButton")
				]
			})
		});
		
	  	return oPage;
	}

});