// Controller Dichiarazione fine turno
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SHIFTDECL.shiftDecl", {
	
	_oArguments: undefined,
	_sViewTipe: '', // 'D' Dichiarazione fine turno , 'P' Ristampa
	_currentDate: '',
	_currentShift: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("shiftDecl").attachPatternMatched(this.onShiftDeclMatched, this);
		oAppRouter.getRoute("reprintUdc").attachPatternMatched(this.onReprintMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onShiftDeclMatched: function (oEvent) {
		this._sViewTipe = 'D';
		
		this.onObjectMatched(oEvent);
	},
	
	onReprintMatched: function (oEvent) {
		this._sViewTipe = 'P';
		
		this.onObjectMatched(oEvent);
	},

	onObjectMatched: function (oEvent) {
		
		const sViewType = this._sViewTipe;
		
		var oView = this.getView();
		var oViewModel = oView.getModel();
		if(!oViewModel)
		{
			oViewModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oViewModel,'view');
		}
		oViewModel.setData({VIEWTYPE: sViewType});
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('shiftDeclPage'),sPlant,sPernr);
		
		this.refreshTableData();
		
		$.UIbyID('shiftDeclDate').setText($.UIbyID('subHeaderDateText').getText());
		$.UIbyID('shiftDeclShift').setText($.UIbyID('subHeaderShiftText').getText());
	},
	
	refreshTableData: function(sDate,sShift) {
		
		if(!sDate)
			sDate = oAppController._sDate;
		if(!sShift)
			sShift = oAppController._sShift;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		const oLoginInfo = oSessionStorage.get("LOGIN_INFO");
		var sPrevDateShift = '', sPrevShiftid = '';
		if(oLoginInfo && sDate === oAppController._sDate && sShift === oAppController._sShift)
		{
			if(oLoginInfo.hasOwnProperty('prevDateShift'))
				sPrevDateShift = oLoginInfo.prevDateShift;
			if(oLoginInfo.hasOwnProperty('prevShiftId'))
				sPrevShiftid = oLoginInfo.prevShiftId;
		}
		
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		const sViewType = oController._sViewTipe;
		const oArguments = oController._oArguments;
		
		oController._currentDate = sDate;
		oController._currentShift = sShift;
		
		var sConfirmed = '';
		/*if(sViewType === 'D')
			sConfirmed = '0';*/
		
		var oTable = $.UIbyID('shiftDeclTable');
		var oModel = oTable.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.xml.XMLModel();
			oTable.setModel(oModel);
		}
		
		var sQuery = QService + sConfirmPath + "Oper/getDoperprodAndPzSQ";
		sQuery += "&Param.1=" + oArguments.plant;
		sQuery += "&Param.2=" + sDate;
		sQuery += "&Param.3=" + sShift;
		sQuery += "&Param.4=" + oArguments.pernr;
		sQuery += "&Param.5=" + sConfirmed;
		sQuery += "&Param.6=" + sPrevDateShift;
		sQuery += "&Param.7=" + sPrevShiftid;
		sQuery += "&Param.8=" + sLanguage;
		
		oModel.loadData(sQuery,false,false);
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Conferma dichiarazione
/***********************************************************************************************/

	onConfirm: function(bSummary) {
		
		if(typeof(bSummary) !== 'boolean')
			bSummary = false;
		
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		const oArguments = oController._oArguments;
		
		var sWsName = '';
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oWorkstInfo = oStorage.get('WORKST_INFO');
		if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
			sWsName = oWorkstInfo.WSNAME;
		
		const sDate = oController._currentDate;
		const sShift = oController._currentShift;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		const oLoginInfo = oSessionStorage.get("LOGIN_INFO");
		var sPrevDateShift = '', sPrevShiftid = '';
		if(oLoginInfo && sDate === oAppController._sDate && sShift === oAppController._sShift)
		{
			if(oLoginInfo.hasOwnProperty('prevDateShift'))
				sPrevDateShift = oLoginInfo.prevDateShift;
			if(oLoginInfo.hasOwnProperty('prevShiftId'))
				sPrevShiftid = oLoginInfo.prevShiftId;
		}

		var sSaveQuery = (bSummary ? (sMovementPath + "Print/print_RIEPILOGO_FINE_TURNO_QR") : (sConfirmPath + "Oper/saveConfListOperQR"));
		sSaveQuery += "&Param.1=" + oArguments.plant;
		sSaveQuery += "&Param.2=" + sDate;
		sSaveQuery += "&Param.3=" + sShift;
		sSaveQuery += "&Param.4=" + oArguments.pernr;
		sSaveQuery += "&Param.5=" + (bSummary ? '0' : sWsName);
		sSaveQuery += "&Param.6=" + (bSummary ? sWsName : '');
		sSaveQuery += "&Param.7=" + (bSummary ? sPrevDateShift : '');
		sSaveQuery += "&Param.8=" + (bSummary ? sPrevShiftid : sPrevDateShift);
		sSaveQuery += "&Param.9=" + (bSummary ? '' : sPrevShiftid);
		sSaveQuery += "&Param.10=" + sLanguage;
		
		console.log(sSaveQuery);
		
		var successFunction = function(oResults) {
			if(!bSummary)
				oController.refreshTableData();
			
			if(oResults.hasOwnProperty('pdfString') && oResults.pdfString)
			{
				const sPdfString = oResults.pdfString;
				var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
				oBoxDeclInputController.showPdfDialog(sPdfString,true);
			}
		};
		
		oAppController.handleRequest(sSaveQuery,{
			showSuccess: false,
			onSuccess: successFunction,
			onWarning: successFunction,
			notShowWarning: [1],
			results: ['pdfString'],
			oneButtonWarning: true
		});
			
	},
	
/***********************************************************************************************/
// Stampa resoconto
/***********************************************************************************************/

	onPrint: function() {
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		oController.onConfirm(true);
	},
	
/***********************************************************************************************/
// Selezione di un elemento dalla tabella
/***********************************************************************************************/

	onItemPressed: function(oEvent) {
		
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		
		var oSelectedContext = oEvent.getSource().getSelectedItem().getBindingContext();
		const sUdcNr = oSelectedContext.getProperty("UDCNR");
		if(sUdcNr)
		{
			var sGetInfoQuery = sMovementPath + "UDC/checkUDCStateAndOriginQR";
			sGetInfoQuery += "&Param.1=" + sUdcNr;
			sGetInfoQuery += "&Param.2=";
			sGetInfoQuery += "&Param.3=";
			sGetInfoQuery += "&Param.4=1";
			sGetInfoQuery += "&Param.5=1";
			sGetInfoQuery += "&Param.6=1";
			sGetInfoQuery += "&Param.7=1";
			sGetInfoQuery += "&Param.8=" + sLanguage;
			
			oAppController.handleRequest(sGetInfoQuery,{
				showSuccess: false,
				onSuccess: oController.onGetInfoSuccess,
				results: {
					returnXMLDoc: true
				}
			});
		}
	},
	
	onGetInfoSuccess: function(oResults,oCallbackParams) {
		
		var xmlData = oResults.data.childNodes[0]; // Rowsets
		xmlData = xmlData.childNodes[1]; // Rowset 1
		xmlData = xmlData.childNodes[1]; // Row 1 (Columns 0)
		
		var oUdcObject = getJsonObjectFromXMLObject(xmlData);
		
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		const sViewType = oController._sViewTipe;
		
		oUdcObject.VIEWTYPE = sViewType;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);
		
		var oArguments = oController._oArguments;
		oArguments['udcnr'] = oUdcObject.UDCNR;
		
		var sNavName;
		if(sViewType === 'P')
			sNavName = 'reprintUdcConfirm';
		else if(sViewType === 'D')
			sNavName = 'shiftDeclInfo';
		oAppRouter.navTo(sNavName,{
			query: oArguments
		});
	},
	
	changeDateShift: function() {
		var oController = sap.ui.getCore().byId('shiftDeclView').getController();
		const oArguments = oController._oArguments;
		
		var oDatePicker = new sap.m.DatePicker({
			displayFormat: "dd/MM/yyyy",
			valueFormat: 'yyyy-MM-dd',
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		});
		
		var oShiftTemplate = new sap.ui.core.Item({
			key : "{SHIFT}",
			text: "{SHNAME}"
		});
		
		var oShiftCombo = new sap.m.ComboBox({
			width: "100%",
			layoutData : new sap.ui.layout.GridData({
				span: "L9 M9 S12"
			})
		})
		oShiftCombo.bindAggregation("items","/Rowset/Row",oShiftTemplate);
		
		var oShiftModel = new sap.ui.model.xml.XMLModel();
		var sQuery = QService + sMasterDataPath + "Shifts/getShiftsByPlantSQ";
		sQuery += "&Param.1=" + oArguments.plant;
		oShiftModel.loadData(sQuery,false,false);
		oShiftCombo.setModel(oShiftModel);
		
		var oDateShiftDialog = new sap.m.Dialog({
			title: oLng_Opr.getText('ReprintUdc_SelectDateShift'),
			contentWidth: '500px',
			content: new sap.ui.layout.Grid({
				hSpacing: 1,
				vSpacing: 0.5,
				content: [
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Date') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12"
						})
					}).addStyleClass("inputLabel"),
					oDatePicker,
					new sap.m.Label({
						text: oLng_Opr.getText('ReprintUdc_Shift') + ':',
						width: '100%',
						textAlign: sap.ui.core.TextAlign.Right,
						layoutData : new sap.ui.layout.GridData({
							span: "L3 M3 S12",
							linebreak:true
						})
					}).addStyleClass("inputLabel"),
					oShiftCombo
				]
			}).addStyleClass("sapUiSmallMarginTopBottom"),
			beginButton: new sap.m.Button({
				text:  oLng_Opr.getText("ReprintUdc_Apply"),
				icon: "sap-icon://accept",
				press: function () {
					const sRequestDate = oDatePicker.getValue();
					var oShiftContext = oShiftCombo.getSelectedItem().getBindingContext();
					const sRequestShift = oShiftContext.getProperty("SHIFT");
					oController.refreshTableData(sRequestDate,sRequestShift);
					
					var sDate;
					var aDateParts = sRequestDate.split('-');
					if(aDateParts.length === 3) // da formato YYYY-MM-DD a DD/MM/YYYY
					{
						var sTmp = aDateParts[0];
						aDateParts[0] = aDateParts[2];
						aDateParts[2] = sTmp;
						sDate = aDateParts.join('/');
					}
					$.UIbyID('shiftDeclDate').setText(sDate);
					$.UIbyID('shiftDeclShift').setText(oShiftContext.getProperty("SHNAME"));
					oDateShiftDialog.close();
				}
			}),
			endButton: new sap.m.Button({
				text: oLng_Opr.getText("ReprintUdc_Cancel"),
				icon: "sap-icon://decline",
				press: function() {
					oDateShiftDialog.close();
				}
			}),
			afterClose: function() {
				oDateShiftDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");
		
		oDatePicker.setValue(oController._currentDate);
		oShiftCombo.setSelectedKey(oController._currentShift);
		
		oDateShiftDialog.open();
	}
});