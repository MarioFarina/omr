//MF20191220
sap.ui.jsview("ui5app.LINELOGIN.linelogin", {

	getControllerName : function() {
		return "ui5app.LINELOGIN.linelogin";
	},

	createContent : function(oController) {
		
		var oAppController = sap.ui.getCore().byId('app').getController();
		
		var newLineLoginTileContainer = function() {
			return new sap.m.TileContainer({
				id: "LinesList",
				tiles: {
					path : "/Rowset/Row",
					template : new sap.m.StandardTile({
						title : "{LINETXT}",
						info: "{DATELOGIN}",
						number: "{IDLINE}",
						icon : {
							parts: [
								{path: "ICON"},
								{path: "COLOR"},
							],	
							formatter: function(sIcon,sColor) {
								if(!sIcon)
									sIcon = "machine";
								if(sColor)
								{
									var css = document.createElement("style");
									css.type = "text/css";
									css.innerHTML = 
										"." + sColor +
										"Tile .sapMStdIconMonitor { color: white !important; } " + 
										"." + sColor + 
										"Tile .sapMStdTileTitle { color: " + sColor + " !important;} " +
										"." + sColor + 
										"Tile .sapMStdTileTopRow { background-color: " + sColor + " !important;} " +
										"." + sColor + 
										"Tile .sapMStdTileInfo  { color: " + sColor + " !important;} " +
										"." + sColor + 
										"Tile .sapMStdTileNum  { color: white !important;} " +
										"." + sColor + 
										"Tile .sapMStdTileNumM  { color: white !important;} ";
									
									
									
									document.body.appendChild(css);
									this.addStyleClass(sColor+'Tile');
								}
								return "sap-icon://" + sIcon;
							}
						},
						type : sap.m.StandardTileType.Monitor,
						press: oController.onFunctionSelected
					})
				}
			})
		};
		
		var newLineLoginList = function() {
			return new sap.m.List({
				id: "LinesList",
				items: {
					path : "/Rowset/Row",
					template : new sap.m.ActionListItem({
						text : "{FuncName}",
						press: oController.onFunctionSelected
					})
				}
			});
		};
		
		var oPage = new sap.m.Page({
			id: "LineloginPage",
			title: "Elenco Linee",
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://multiselect-none",
					press: function() {
						oController._bListView = !oController._bListView;
						$.UIbyID('LinesList').destroy();
						var sIdParent = '';
						if(oController._aParents)
						{
							const iParentsCount = oController._aParents.length;
							if(iParentsCount > 0)
							{
								const oParent = oController._aParents[iParentsCount-1];
								sIdParent = oParent.id;
							}
						}
						if(oController._bListView)
						{
							oPage.addContent(newLineLoginList());
							this.setIcon("sap-icon://table-view");
						}
						else
						{
							oPage.addContent(newLineLoginTileContainer());
							this.setIcon("sap-icon://multiselect-none");
						}
						oController.setViewFilter(sIdParent);
					}
				}),
				new sap.m.Button({
					icon: "sap-icon://home",
					press: [oAppController.onLogout,{
						logoutFunc:oController.onLogout
					}]
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: newLineLoginTileContainer(),
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});

	  	return oPage;
		
	}
});
