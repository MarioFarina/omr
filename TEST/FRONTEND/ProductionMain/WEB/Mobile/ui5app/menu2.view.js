// View Menu Operatore

sap.ui.jsview("ui5app.menu", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.menu";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {

		var oAppController = sap.ui.getCore().byId('app').getController();
		
		var oList = new sap.m.List({
			id: "functionsList"
		});
		oList.bindAggregation("items",{
			path : "/Rowset/Row",
			template : new sap.m.ActionListItem({
				text : "{FuncName}",
				press: oController.onFunctionSelected
			})
		});
		
		var oPage = new sap.m.Page({
			id: "menuPage",
			title: oLng_Opr.getText("Menu_Menu"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: [oAppController.onLogout,{
						logoutFunc:oController.onLogout
					}]
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: oList,
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});

	  	return oPage;
	}

});