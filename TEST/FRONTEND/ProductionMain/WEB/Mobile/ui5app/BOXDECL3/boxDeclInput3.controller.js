// +
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL3.boxDeclInput3", {

	_oArguments: undefined,
	_sWsName: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("boxDeclInput3").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArgParams = ['plant','pernr','order','poper','idline'];
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclInputPage3'),sPlant,sPernr);

		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty');
		var oStdQtyModel = oStdQtyCombo.getModel();
		if(!oStdQtyModel)
		{
			oStdQtyModel = new sap.ui.model.xml.XMLModel();
			oStdQtyCombo.setModel(oStdQtyModel);
		}
		var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbByOrderPoperSQ";
		sStdQtyQuery += "&Param.1=" + sPlant;
		sStdQtyQuery += "&Param.2=" + oArguments.order;
		sStdQtyQuery += "&Param.3=" + oArguments.poper;
		oStdQtyModel.loadData(sStdQtyQuery,false,false);

		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oWorkstInfo = oStorage.get('WORKST_INFO');
		if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
			this._sWsName = oWorkstInfo.WSNAME;

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');

		var bDisabledMode = false;
		if(!oUdcObject || !oUdcObject.hasOwnProperty('UDCNR') ||
			(oUdcObject.UDCNR !== oArguments.udc && !(oUdcObject.PRINT_MODE && oArguments.udc === '')))
		{
			oUdcObject = {};
			bDisabledMode = true;
		}

		this.displayInfo(oUdcObject, bDisabledMode);
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/

	displayInfo: function(oUdcObject,bDisabledMode) {

		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		var oView = sap.ui.getCore().byId('boxDeclInput3View');
		var oModel = oView.getModel();

		var oController = oView.getController();
		const oArguments = oController._oArguments;

		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);

		const sNewUdc = oUdcObject.UDCNR === '';

		// Per nuovo UdC il campo UdC non è visibile
		$.UIbyID('boxDeclUDCLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclUDCText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclDeclQtyLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclDeclQtyText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclOpLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclOpText').setVisible(!sNewUdc);
		$.UIbyID('boxDeclTimeLabel').setVisible(!sNewUdc);
		$.UIbyID('boxDeclTimeText').setVisible(!sNewUdc);

		var bPrintMode = false;
		if(oUdcObject.hasOwnProperty('PRINT_MODE'))
			bPrintMode = oUdcObject.PRINT_MODE;

		$.UIbyID('boxDeclInputWorkTime').setValue(oUdcObject.WTDUR);
		$.UIbyID('boxDeclInputNrPrnt').setValue(oUdcObject.NRPRNT);

		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty');
		oStdQtyCombo.clearSelection();
		oStdQtyCombo.setValue();
		oStdQtyCombo.data('NMIMB', undefined);
		const sSelectedNmImb = oUdcObject.NMIMB;
		if(!sSelectedNmImb)
		{
			const sDefaultKey = getValueFromXMLModel(oStdQtyCombo.getModel(),"DEFAULT","1","NMIMB");
			if(sDefaultKey)
				oStdQtyCombo.setSelectedKey(sDefaultKey);
		}
		else
		{
			if(oStdQtyCombo.getItemByKey(sSelectedNmImb))
				oStdQtyCombo.setSelectedKey(sSelectedNmImb);
			else
			{
				oStdQtyCombo.setValue(sSelectedNmImb);
				oStdQtyCombo.data('NMIMB',{
					QTY: 0, // parametro non utilizzato al momento (quantità corrispondente a NMIMB)
					NMIMB: sSelectedNmImb
				});
			}
		}

		$.UIbyID('boxDeclInputBasketTT').setValue(oUdcObject.BASKETTT);
		$.UIbyID('boxDeclInputDater').setValue(oUdcObject.DATER);

		$.UIbyID('boxDeclInputQty').setValue((bPrintMode) ? oUdcObject.QTYDECL : '');

		var qtyBuoniReq = sMovementPath + "ProdDecl/getQtyFromDicSQ";
		qtyBuoniReq += "&Param.1=" + oUdcObject.LINEID;
		qtyBuoniReq += "&Param.2=" + oAppController._sShift;
		qtyBuoniReq += "&Param.3=" + oAppController._sDate;
		qtyBuoniReq += "&Param.4=" + oUdcObject.MATERIAL;
		var qtyBuoni = fnGetAjaxVal(qtyBuoniReq, 'TOTALE', false);

		if(qtyBuoni < 0 || qtyBuoni == "") {
			qtyBuoni = 0;
		}

		$.UIbyID('QtyBuoni').setVisible(true);
		$.UIbyID('QtyBuoni').setText(qtyBuoni);
		if(qtyBuoni == 0)
			$.UIbyID('boxDeclInputQty').setValue("");
		else
			$.UIbyID('boxDeclInputQty').setValue(qtyBuoni);

		$.UIbyID('boxDeclInputQty').focus();

		$.UIbyID('boxDeclConfirmButton').setEnabled(!bDisabledMode && !bPrintMode);
		$.UIbyID('boxDeclPrintButton').setEnabled(!bDisabledMode && bPrintMode);
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclInput3View').getController();
		var oArguments = oController._oArguments;

		delete oArguments.order;
		delete oArguments.poper;
		delete oArguments.idline;
		delete oArguments.udc;

		oAppRouter.navTo('boxDecl3',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Selezione qtà standard (norma di imballo)
/***********************************************************************************************/

	onSelectNmimb: function() {
		var oContext = this;
		var oView = oContext.view;
		var oController = oView.getController();

		var oStdQtyModel = new sap.ui.model.xml.XMLModel();
		var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbsSQ";
		sStdQtyQuery += "&Param.1=" + oController._oArguments.plant;
		oStdQtyModel.loadData(sStdQtyQuery,false,false);

		var oTemplate = new sap.m.ColumnListItem({
			type: sap.m.ListType.Active,
			cells: [
				new sap.m.Text({
					text: "{NMIMB}"
				}),
				new sap.m.Text({
					text: "{DESCR}"
				}),
				new sap.m.Text({
					text: "{MATERIAL}"
				}),
				new sap.m.Text({
					text: "{QTY}"
				})
			]
		});

		var oDeclDialog = new sap.m.Dialog({
			title: oLng_Opr.getText('BoxDeclInput_SelectNmimb'),
			contentWidth: "800px",
			contentHeight: "500px",
			beginButton: new sap.m.Button({
				text: oLng_Opr.getText("PacDm_Close"),
				press: function () {
					oDeclDialog.close();
				}
			}),
			afterClose: function() {
				oDeclDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");

		var oTable = new sap.m.Table({
			mode: sap.m.ListMode.SingleSelectMaster,
			noDataText: oLng_Opr.getText('BoxDeclInput_NoNmimb'),
			columns: [
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_Nmimb")
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbDescr")
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbMaterial")
					}),
					width: '170px'
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("BoxDeclInput_NmimbQty")
					}),
					hAlign: sap.ui.core.TextAlign.End,
					width: '90px'
				})
			],
			itemPress: function(oEvent) {

				var oSelectedContext = oEvent.getSource().getSelectedItem().getBindingContext();
				const sQty = oSelectedContext.getProperty("QTY");
				const sNmImb = oSelectedContext.getProperty("NMIMB");
				const sDescr = oSelectedContext.getProperty("DESCR");

				var oStdQtyCombo = oContext.combo;
				oStdQtyCombo.clearSelection();
				oStdQtyCombo.setValue(sQty + " (" + sNmImb + " " + sDescr + ")");
				oStdQtyCombo.data('NMIMB',{
					QTY: sQty,
					NMIMB: sNmImb
				});

				oDeclDialog.close();
			}
		});
		oTable.bindAggregation("items", "/Rowset/Row", oTemplate);

		oDeclDialog.addContent(oTable);

		oDeclDialog.setSubHeader(new sap.m.Toolbar({
			content: new sap.m.SearchField({
				search: function (oEvent) {
					var sValue = oEvent.getParameter("query");
					var oNmImbFilter = new sap.ui.model.Filter("NMIMB", sap.ui.model.FilterOperator.Contains, sValue);
					var oDescrFilter = new sap.ui.model.Filter("DESCR", sap.ui.model.FilterOperator.Contains, sValue);
					var oMaterialFilter = new sap.ui.model.Filter("MATERIAL", sap.ui.model.FilterOperator.Contains, sValue);
					var oAllFilters = new sap.ui.model.Filter([oNmImbFilter,oDescrFilter,oMaterialFilter]);
					var oBinding = oTable.getBinding("items");
					oBinding.filter(oAllFilters);
				}
			})
		}));

		oView.addDependent(oDeclDialog);

		oDeclDialog.setModel(oStdQtyModel);
		oDeclDialog.open();
	},

/***********************************************************************************************/
// Conferma
/***********************************************************************************************/

	onConfirm: function() {
		var oView = sap.ui.getCore().byId('boxDeclInput3View');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;

		var oBasketTTInput = $.UIbyID('boxDeclInputBasketTT');
		var oDaterInput = $.UIbyID('boxDeclInputDater');
		var oWorkTimeInput = $.UIbyID('boxDeclInputWorkTime');
		var oQtyInput = $.UIbyID('boxDeclInputQty');
		var oStdQtyCombo = $.UIbyID('boxDeclInputStdQty');

		oQtyInput.setValueState(sap.ui.core.ValueState.None);
		oQtyInput.setValueStateText();
		oBasketTTInput.setValueState(sap.ui.core.ValueState.None);
		oBasketTTInput.setValueStateText();
		oDaterInput.setValueState(sap.ui.core.ValueState.None);
		oDaterInput.setValueStateText();
		oWorkTimeInput.setValueState(sap.ui.core.ValueState.None);
		oWorkTimeInput.setValueStateText();
		oStdQtyCombo.setValueState(sap.ui.core.ValueState.None);
		oStdQtyCombo.setValueStateText();

		const sBasketTT = oBasketTTInput.getValue();
		const sDater = oDaterInput.getValue();
		const sQtyDecl = oQtyInput.getValue();
		const iQtyDecl = parseInt(sQtyDecl);
		const sWorkTime = oWorkTimeInput.getValue();
		const sNrPrnt = $.UIbyID('boxDeclInputNrPrnt').getValue();

		var tollinp = sMovementPath + "ProdDecl/getTollinpFromLineSQ";
		tollinp += "&Param.1=" + oArguments.idline;
		var tolleranza = fnGetAjaxVal(tollinp, 'TOLLINP', false);

		var qtyProposta = parseInt($.UIbyID('QtyBuoni').getText());
		//var diff = iQtyDecl - qtyProposta;
		var tollValueMin = Math.round(qtyProposta - (qtyProposta / 100 * tolleranza));
		var tollValueMax = Math.round(qtyProposta + (qtyProposta / 100 * tolleranza));

		var sNmImb = oStdQtyCombo.getSelectedKey();
		if(!sNmImb)
		{
			var oNmImbData = oStdQtyCombo.data('NMIMB');
			if(oNmImbData && oNmImbData.hasOwnProperty('NMIMB'))
				sNmImb = oNmImbData.NMIMB;
		}

		var iQtyRes = parseFloat(oModel.getProperty("/QTYRES"));
		if(isNaN(iQtyRes))
			iQtyRes = 0;

		var iMatPzUdc = parseFloat(oModel.getProperty("/MAT_PZUDC"));
		if(isNaN(iMatPzUdc))
			iMatPzUdc = 0;

		var sErrorMessage;
		var oErrorField;
		const sCurrentYear = parseInt(new Date().getFullYear().toString().substring(2,4));
		if(sDater && (sDater.length != 4 || !sDater.match(/(0[1-9]|1[0-2])([0-9]{2})/))) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterErrorMsg");
			oErrorField = oDaterInput;
		}
		else if(sDater && (sDater.substring(2,4) < sCurrentYear-1 || sDater.substring(2,4) > sCurrentYear+1))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_DaterYearErrorMsg");
			oErrorField = oDaterInput;
		}
		else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z ]+$/))
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_BasketTTErrorMsg");
			oErrorField = oBasketTTInput;
		}
		else if(isNaN(sWorkTime) || sWorkTime<=0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_WorkTimeErrorMsg");
			oErrorField = oWorkTimeInput;
		}
		else if(sWorkTime > 480)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_WorkTimeMaxErrorMsg") + " (480 Min)";
			oErrorField = oWorkTimeInput;
		}
		else if(isNaN(iQtyDecl) || iQtyDecl <= 0)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyErrorMsg");
			oErrorField = oQtyInput;
		}
		else if(iQtyDecl > 15000)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg") + " (15000)";
			oErrorField = oQtyInput;
		}
		else if(iMatPzUdc && iMatPzUdc > 0 && (iQtyRes + iQtyDecl) > iMatPzUdc)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyMaxErrorMsg");
			oErrorField = oQtyInput;
		}
		else if(!sNmImb && parseInt(oModel.getProperty("/IMBOBBL")) === 1)
		{
			sErrorMessage = oLng_Opr.getText("BoxDeclInput_MandatoryStdImb");
			oErrorField = oStdQtyCombo;
		}

		if(!sErrorMessage)
		{
			var supToll = false;
			if(!(iQtyDecl >= tollValueMin && iQtyDecl <= tollValueMax) && qtyProposta != 0) {
				supToll = true;
			}

			var continueFunc = function(sState) {

					if(iMatPzUdc && iMatPzUdc > 0 && (iQtyRes + iQtyDecl === iMatPzUdc)
						&& sState !== 'C')
					{
						var sErrorMessage = oLng_Opr.getText("BoxDeclInput_QtyFullErrorMsg");

						oQtyInput.setValueState(sap.ui.core.ValueState.Warning);
						oQtyInput.setValueStateText(sErrorMessage);
						oQtyInput.focus();

						sap.m.MessageBox.show(sErrorMessage, {
							icon: sap.m.MessageBox.Icon.WARNING,
							title: oLng_Opr.getText("General_WarningText"),
							actions: sap.m.MessageBox.Action.OK
						});

						return;
					}

					const sFIUdC = (sState === 'C') ? 'C' : 'I';
					const sDatetime = getDatetimeString(new Date());

					var sSaveQuery = sMovementPath + "UDC/saveUDCandPrintQR";
					sSaveQuery += "&Param.1=" + oArguments.plant;
					sSaveQuery += "&Param.2=" + oArguments.pernr;
					sSaveQuery += "&Param.3=" + oAppController._sShift;
					sSaveQuery += "&Param.4=" + oAppController._sDate;
					sSaveQuery += "&Param.5=" + oArguments.order + oArguments.poper;
					sSaveQuery += "&Param.6=" + oArguments.idline;
					sSaveQuery += "&Param.7=" + oArguments.udc;
					sSaveQuery += "&Param.8=" + sQtyDecl;
					sSaveQuery += "&Param.9=" + sWorkTime;
					sSaveQuery += "&Param.10=" + sNrPrnt;
					sSaveQuery += "&Param.11=" + sNmImb;
					sSaveQuery += "&Param.12=" + sDater;
					sSaveQuery += "&Param.13=" + sBasketTT;
					sSaveQuery += "&Param.14=" + oController._sWsName;
					sSaveQuery += "&Param.15=" + sDatetime;
					sSaveQuery += "&Param.16=" + sFIUdC;
					sSaveQuery += "&Param.17=";
					sSaveQuery += "&Param.18=" + sLanguage;

					var successFunction = function(oResults) {

						var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
						var oUdcObject = oSessionStorage.get('FUNCDATA');

						if(!oUdcObject)
							oUdcObject = {};

						if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
							oUdcObject.UDCNR = oResults.outputUdc;
						if(oResults.hasOwnProperty('pdfString') && oResults.pdfString)
						{
							oUdcObject.PDFSTRING = oResults.pdfString;
							if(parseInt(oResults.code) === 1) // WARNING = 1 : stampa non automatica, anteprima automatica
								oController.showPdfDialog(oResults.pdfString,true);
						}
						oUdcObject.PRINT_MODE = true;
						oUdcObject.QTYRES = iQtyRes + iQtyDecl;
						oUdcObject.QTYDECL = iQtyDecl;
						oUdcObject.WTDUR = sWorkTime;
						oUdcObject.NRPRNT = sNrPrnt;
						oUdcObject.NMIMB = sNmImb;
						oUdcObject.DATER = sDater;
						oUdcObject.BASKETTT = sBasketTT;
						oUdcObject.TIMEID = sDatetime;
						oUdcObject.LAST_OPEID = oArguments.pernr;
						oUdcObject.STATE = sState;

						const oLoginInfo = oSessionStorage.get('LOGIN_INFO');
						if(oLoginInfo.hasOwnProperty("opeName"))
							oUdcObject.LAST_OPENAME = oLoginInfo.opeName;

						oSessionStorage.put('FUNCDATA',oUdcObject);
						oController.displayInfo(oUdcObject,false);
					};

					oAppController.handleRequest(sSaveQuery,{
						onSuccess: successFunction,
						onWarning: successFunction,
						notShowWarning: [1],
						results: ['outputUdc','pdfString']
					});
				};

				if(supToll) {
					sap.m.MessageBox.confirm(
						oLng_Opr.getText("BoxDeclInput_QtyToleranceErrorMsg"),
						{
							actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
							onClose: function(sAction) {
								if (sAction === sap.m.MessageBox.Action.YES) {
									oController.selectState({
										firstValue: 'O',
										secondValue: 'C',
										onCallback: continueFunc
									});
								}
							}
						}
					);
				} else {
					oController.selectState({
						firstValue: 'O',
						secondValue: 'C',
						onCallback: continueFunc
					})
				}
		}
		else
		{
			if(oErrorField)
			{
				oErrorField.setValueState(sap.ui.core.ValueState.Warning);
				oErrorField.setValueStateText(sErrorMessage);
				oErrorField.focus();
			}

			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},

/***********************************************************************************************/
// Dialog per la selezione dello stato dell'UdC
/***********************************************************************************************/

	selectState: function(oParams) {
		if(!oParams || typeof(oParams.onCallback) !== 'function')
			return;
		if(!oParams.hasOwnProperty('firstText'))
			oParams.firstText = oLng_Opr.getText("BoxDeclInput_Incomplete");
		if(!oParams.hasOwnProperty('firstColorCss'))
			oParams.firstColorCss = 'greyButton';
		if(!oParams.hasOwnProperty('secondText'))
			oParams.secondText = oLng_Opr.getText("BoxDeclInput_Complete");
		if(!oParams.hasOwnProperty('secondColorCss'))
			oParams.secondColorCss = 'greenButton';
		if(!oParams.hasOwnProperty('firstValue'))
			oParams.firstValue = false;
		if(!oParams.hasOwnProperty('secondValue'))
			oParams.secondValue = true;

		var oDialog = new sap.m.Dialog({
			title: oLng_Opr.getText("BoxDeclInput_SelectState"),
			contentWidth: "700px",
			contentHeight: "160px",
			content: [
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oParams.firstText,
							icon: "sap-icon://switch-views",
							press: function() {
								oDialog.close();
								oParams.onCallback(oParams.firstValue);
							}
						}).addStyleClass("bigButton " + oParams.firstColorCss),
						new sap.m.Button({
							text: oParams.secondText,
							icon: "sap-icon://complete",
							press: function() {
								oDialog.close();
								oParams.onCallback(oParams.secondValue);
							}
						}).addStyleClass("sapUiSmallMarginBegin bigButton " + oParams.secondColorCss)
					]
				}).addStyleClass("sapUiSmallMarginTop")
			],
			endButton: new sap.m.Button({
				text: oLng_Opr.getText("BoxDeclInput_Cancel"),
				icon: "sap-icon://decline",
				press: function() {
					oDialog.close();
				}
			}),
			afterClose: function() {
				oDialog.destroy();
			}
		});

		oDialog.open();
	},

/***********************************************************************************************/
// Stampa
/***********************************************************************************************/

	onPrint: function() {
		var oView = sap.ui.getCore().byId('boxDeclInput3View');
		var oController = oView.getController();
		var oModel = oView.getModel();

		const sPdfString = oModel.getProperty("/PDFSTRING");
		/*
		const oArguments = oController._oArguments;

		const sIsFull = (oModel.getProperty("/STATE") === 'C') ? 'true' : 'false';

		var sPrintUrl = "/XMII/Runner?";
		sPrintUrl += "Transaction=ProductionMain/Movement/Print/print_ETI_PROD_TR";
		sPrintUrl += "&OutputParameter=pdfString&Content-Type=application/pdf&isBinary=true";
		sPrintUrl += "&UDCNR=" + oModel.getProperty("/UDCNR");
		sPrintUrl += "&IDLINE=" + oArguments.idline;
		sPrintUrl += "&location=" + oController._sWsName;
		sPrintUrl += "&full=" + sIsFull;
        sPrintUrl += "&Language=" + sLanguage;
		*/

		//window.open(sPrintUrl, '_blank');

		oController.showPdfDialog(sPdfString,true);
	},

	showPdfDialog(sPrintUrl,bBase64,closeFunction) {

		if(bBase64)
			sPrintUrl = 'data:application/pdf;base64,' + sPrintUrl;

		var oHtml = new sap.ui.core.HTML();
		oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');

		var oPrintDialog = new sap.m.Dialog({
			//resizable: true,
			horizontalScrolling : false,
			verticalScrolling : false,
			contentWidth : '800px',
			contentHeight : '600px',
			customHeader: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://decline",
						press: function() {
							oHtml.destroy();
							oPrintDialog.close();
						}
					})
				]
			}),
			content: oHtml,
			afterClose: function() {
				oPrintDialog.destroy();
				if(typeof(closeFunction) === 'function')
					closeFunction();
			}
		}).addStyleClass('noPaddingDialog fullHeightScrollCont');

		oPrintDialog.open();
	}

});
