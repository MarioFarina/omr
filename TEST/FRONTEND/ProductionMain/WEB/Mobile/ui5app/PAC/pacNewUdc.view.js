// View Pac - Lettura UDC
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PAC.pacNewUdc", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PAC.pacNewUdc";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oStdQtyCombo = new sap.m.ComboBox({
			id: 'pacNewUdcStdQty',
			width: '100%',
			layoutData : new sap.ui.layout.GridData({
				span: "L5 M5 S12"
			})
		});
		
		var oStdQtyTemplate = new sap.ui.core.Item({
			key : "{NMIMB}",
			text: {
				parts: [
					{path: 'NMIMB'},
					{path: 'DESCR'},
					{path: 'QTY'}
				],		     
				formatter: function(sNmimb, sDesc, sQty){
					return sNmimb + ' ' + sDesc + ' (' + sQty + ')';
				}
			}
		});
		oStdQtyCombo.bindAggregation("items","/Rowset/Row",oStdQtyTemplate);
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("PacNewUdc_OrderPhase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'pacNewUdcOrder',
					layoutData : new sap.ui.layout.GridData({
						span: "L5 M5 S12"
					}),
					liveChange: oController.onOrderPoperLiveChange
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacNewUdc_StdQty") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L4 M4 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				oStdQtyCombo
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "pacNewUdcPage",
			enableScrolling: false,
			title: oLng_Opr.getText("PacNewUdc_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("PacNewUdc_CreateUdc"),
							icon: "sap-icon://add",
							press: oController.onCreate
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});