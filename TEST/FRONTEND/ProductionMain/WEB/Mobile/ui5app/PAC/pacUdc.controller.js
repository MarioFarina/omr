// Controller Pac - Lettura UDC
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.PAC.pacUdc", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("pacUdc").attachPatternMatched(this.onObjectMatched, this);

		var nextFunc = this.onNext;
		var oUdcCombo = sap.ui.getCore().byId("pacUdcInput");
		oUdcCombo.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		var oController = this;
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		oController._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('pacUdcPage'),sPlant,sPernr);
		
		var oUdcInput = sap.ui.getCore().byId('pacUdcInput');
		oUdcInput.setValueState();
		oUdcInput.setValueStateText();
		oUdcInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			
			//MF20200129 START
			if(oArguments.udc !== "undefined" && oArguments.udc !== "") {
				oUdcInput.setValue(oArguments.udc);
				oArguments.udc = "";
			} else {
				oUdcInput.setValue("");
				oArguments.udc = "";
			}
			//MF20200129 END
			
			oUdcInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
	onNext: function() {
		var oUdcInput = $.UIbyID('pacUdcInput');
		const sSelectedUdc = oUdcInput.getValue();
		
		oUdcInput.setValueState(sap.ui.core.ValueState.None);
		oUdcInput.setValueStateText();
		
		var sErrorMessage;
		if(!sSelectedUdc)
		{
			sErrorMessage = oLng_Opr.getText("PacUdc_UDCErrorMsg");
			oUdcInput.setValueState(sap.ui.core.ValueState.Warning);
			oUdcInput.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var sCheckQuery = sMovementPath + "PAC/checkUDCForPACQR";
			sCheckQuery += "&Param.1=" + sSelectedUdc;
			sCheckQuery += "&Param.2=" + sLanguage;
			
			var oController = sap.ui.getCore().byId('pacUdcView').getController();
			
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onUdcSuccess,
				results: {
					returnXMLDoc: true
				}
			});
		}
	},
	
	onUdcSuccess: function(oResults,oCallbackParams) {
		
		var xmlData = oResults.data.childNodes[0]; // Rowsets
		xmlData = xmlData.childNodes[1]; // Rowset 1
		xmlData = xmlData.childNodes[1]; // Row 1 (Columns 0)
		
		var oUdcObject = getJsonObjectFromXMLObject(xmlData);
		console.log(oUdcObject);
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		var oArguments = oController._oArguments;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);

		oArguments['udcnr'] = oUdcObject.UDCNR;

		oAppRouter.navTo('pacDm',{
			query: oArguments
		});
		
	},
	
	onNewUdC: function() {
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		var oArguments = oController._oArguments;
		
		oAppRouter.navTo('pacNewUdc',{
			query: oArguments
		});
	},
	
	//MF20200129 START
	onLastUdC: function() {
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		var oLastUdcModel = oController.getLastUdcModel();
		oController.selectLastUdcDialog(oLastUdcModel,oController.setLastUdc);
	},
	
	getLastUdcModel: function() {
		var sQuery = QService + sMovementPath + "UDC/getLastUDCSQ";
		var oModel = new sap.ui.model.xml.XMLModel();
		oModel.loadData(sQuery,false,false);
		return oModel;		
	},
	
	setLastUdc: function(oUdcSelected) {
		var oUdcInput = $.UIbyID('pacUdcInput');
		const sSelectedUdc = oUdcInput.setValue(oUdcSelected.UDCNR);
	},
	
	selectLastUdcDialog: function(oModel,nextFunction,oCallbackParams) {
		oAppController.selectFromDialog({
			title: "Seleziona un UdC",
			noDataText: "Seleziona un UdC",
			callback: nextFunction,
			callbackParams: oCallbackParams,
			model: oModel,
			modelPath: "/Rowset/0/Row",
			modelTitle: 'UDCNR',
			modelDesc: 'MATERIAL',
			template: oAppController.customListTemplate({
				titleText: "{UDCNR}",
				bottomLeftText: "{MOV_IDLINE}",
				topRightText: {
					parts: [
						{path: "QTYRES" }
					],		     
					formatter: function(sQtyRes) {
						return oLng_Opr.getText('PacDm_Qty') + ": " + sQtyRes;
					}
				},
				bottomRightText: {
					parts: [
						{path: "TIMEID" }
					],		     
					formatter: dateFormatter
				}
			})
		});
	},
	
	showUdcPdf: function() {
		var oUdcInput = sap.ui.getCore().byId('pacUdcInput');
		
		var sErrorMessage;
		if(!oUdcInput.getValue())
		{
			sErrorMessage = oLng_Opr.getText("PacUdc_UDCErrorMsg");
			oUdcInput.setValueState(sap.ui.core.ValueState.Warning);
			oUdcInput.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
			
			return;
		}
		
		var sPrintUrl = "/XMII/Runner?";
		sPrintUrl += "Transaction=ProductionMain/Movement/Print/Print_ETI_UDC_TR";
		sPrintUrl += "&OutputParameter=pdfString&Content-Type=application/pdf&isBinary=true";
		sPrintUrl += "&udcnr=" + oUdcInput.getValue();
	
		var oHtml = new sap.ui.core.HTML();
		oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');

		var oPrintDialog = new sap.m.Dialog({
			horizontalScrolling: false,
			verticalScrolling: false,
			contentWidth: '60%',
			contentHeight: '60%',
			customHeader: new sap.m.Toolbar({
			content: [
				new sap.m.ToolbarSpacer(),
				new sap.m.Button({
					icon: "sap-icon://decline",
					press: function () {
						oHtml.destroy();
						oPrintDialog.close();
					}
				})
			]
		}),
		content: oHtml,
		afterClose: function () {
			oPrintDialog.destroy();
		}
	});

	oPrintDialog.open();
		//
	}
	//MF20200129 END
});