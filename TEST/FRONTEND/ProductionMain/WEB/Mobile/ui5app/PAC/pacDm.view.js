// View Pac - Lettura DataMatrix
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.PAC.pacDm", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.PAC.pacDm";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {

		var oView = this;

		var oGrid = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: "{/ORDER}",
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					text: {
						parts: [
							{path: "/POPER" },
							{path: "/CICTXT"}
						],		     
						formatter: function(sPoper,sCicTxt){
							return sPoper + " " + sCicTxt;
						}
					}
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: oAppController.onShowPoperInfo(oView),
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					text: {
						parts: [
							{path: "/IDLINE" },
							{path: "/LINETXT"}
						],		     
						formatter: function(sIdLine,sLineTxt){
							return sIdLine + " " + sLineTxt;
						}
					}
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/IDLINE',
						description: '/LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					text: {
						parts: [
							{path: "/MATERIAL" },
							{path: "/MAT_DESC"}
						],		     
						formatter: function(sMaterial,sMatDesc){
							return sMaterial + " " + sMatDesc;
						}
					}
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_UdC") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: "{/UDCNR}",
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L4 M4 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_UdCSusp") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					text: {
						parts: [
							{path: "/UDCSUSP" }
						],
						formatter: function(sSuspUdc){
							if(!sSuspUdc)
								sSuspUdc = oLng_Opr.getText('PacDm_New');
							return sSuspUdc;
						}
					},
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.layout.HorizontalLayout({
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					}),
					content: [
						new sap.ui.core.Icon({
							src: "sap-icon://add",
							size: "18px",
							press: [oController.onSelectSuspUdc,{isNew:true}],
							tooltip: oLng_Opr.getText('PacDm_NewSuspUdc')
						}),
						new sap.ui.core.Icon({
							src: "sap-icon://value-help",
							size: "18px",
							press: [oController.onSelectSuspUdc,{isNew:false}],
							tooltip: oLng_Opr.getText('PacDm_SelectSuspUdc')
						}).addStyleClass("sapUiTinyMarginBegin")
					]
				}),
				new sap.m.Label({
					text: oLng_Opr.getText('PacDm_Goods') + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					text: {
						parts: [
							{path: "/PZUDC" },
							{path: "/Rowset/Row/PZUDC"},
							{path: "goods>/Rowset"}
						],		     
						formatter: function(sJsonTotal,sXmlTotal){
							var sTotalQty;
							if(sJsonTotal)
								sTotalQty = sJsonTotal;
							else
								sTotalQty = sXmlTotal;
							return oController.getRowCount('goods','QTY') + " / " + sTotalQty;
						}
					}
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oController.onShowDeclDialog, {
						goods: true
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText('PacDm_Suspended') + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					}),
					text: {
						parts: [
							{path: "susp>/Rowset"}
						],		     
						formatter: function(){
							return oController.getRowCount('susp');
						}
					}
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oController.onShowDeclDialog, {
						goods: false
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					id: 'pacDmLabel',
					text: oLng_Opr.getText("PacDm_DmCausalLabel") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'pacDmCausalInput',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("PacDm_BasketTT") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'pacDmBasketTT',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				})
			]
		}).addStyleClass("sapUiTinyMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGrid
		});
						  
		var oPage = new sap.m.Page({
			id: "pacDmPage",
			enableScrolling: false,
			title: oLng_Opr.getText("PacDm_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("PacDm_Pause"),
							icon: "sap-icon://pause",
							press: oController.onPause
						}).addStyleClass("sapUiSmallMarginBegin yellowButton bigButton"),
						new sap.m.Button({
							text: oLng_Opr.getText("PacDm_Complete"),
							icon: "sap-icon://message-success",
							press: oController.onComplete
						}).addStyleClass("sapUiSmallMarginBegin blueButton bigButton")
					]
				}).addStyleClass("sapUiSmallMarginTop")
			],
			footer: new sap.m.Toolbar({
				active: true,
				content: [
					new sap.m.ToolbarSpacer()
				]
			})
		});
		
	  	return oPage;
	}

});