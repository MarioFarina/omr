//**************************************************************************************
//Title:  SSETSBACKUP
//Author: Mario Farina
//Date:   20/01/2020
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSBACKUP

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
var dataProdBackup = "Content-Type=text/XML&QueryTemplate=ProductionMain/Backup/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/backup/res/backup_res.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/Backup/SsetSBackup_fn/fn_SsetSBackup"
    ],
    function () {
        $(document).ready(function () {
            
			var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
                content: [
                    createTabBackup()
				],
                width: "100%"
            });
            
            oVerticalLayout.placeAt("MasterCont");
            refreshTabBackup();
			
			$("#splash-screen").hide(); //nasconde l'icona di loading
            
        });
    }
);