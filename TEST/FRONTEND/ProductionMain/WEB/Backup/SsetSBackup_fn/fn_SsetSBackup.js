//**************************************************************************************
//Title:  SSETSBACKUP
//Author: Mario Farina
//Date:   22/01/2020
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSBACKUP

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Backup/res/backup_res.i18n.properties",
	locale: sCurrentLocale
});

function createTabBackup() {
	var oTable = UI5Utils.init_UI5_Table({
        id: "SSetSBackupTab",
        properties: {
            visibleRowCount: 15,
			fixedColumnCount: 0,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetSBackupTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSBackup").setEnabled(false);
        		}
                else {
                    $.UIbyID("btnModSBackup").setEnabled(true);
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Backup_Modify"),
                        id:   "btnModSBackup",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSBackupDlg();
                        }
                    }),
				],
				rightItems: [
					new sap.ui.commons.Button({
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabBackup();
                        }
                    })
				]
			})
        },
        exportButton: false,
        columns: [
            {
				Field: "TABLENAME",
				label: oLng_Settings.getText("Backup_TABLENAME"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "NRLIVEDAYS",
				label: oLng_Settings.getText("Backup_NRLIVEDAYS"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "FIELDNAME",
				label: oLng_Settings.getText("Backup_FIELDNAME"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "MAXRECORDS",
				label: oLng_Settings.getText("Backup_MAXRECORDS"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "SIZE",
				label: oLng_Settings.getText("Backup_SIZE"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "LASTJOBDATE",
				label: oLng_Settings.getText("Backup_LASTJOBDATE"),
				properties: {
					//width: "100px",
                    //flexible : false
				}
            },
			{
				Field: "LASTMESSAGE",
				label: oLng_Settings.getText("Backup_LASTMESSAGE"),
				properties: {
					//width: "100px",
                    //flexible : false
				},
            },
			{
				Field: "ERROR",
				label: oLng_Settings.getText("Backup_ERROR"),
				properties: {
					//width: "100px",
                    //flexible : false
				},
				template: {
	      			type: "checked"
	      		}
            },
			{
				Field: "ENABLED",
				label: oLng_Settings.getText("Backup_ENABLED"),
				properties: {
					//width: "100px",
                    //flexible : false
				},
				template: {
	      			type: "checked"
	      		}
            },
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabBackup();
    return oTable;
};

function refreshTabBackup() {
	$.UIbyID("SSetSBackupTab").getModel().loadData(
    	QService + dataProdBackup + "GetMBACKUP_XQ"
    );
    
    $.UIbyID("SSetSBackupTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
};

function openSBackupDlg() {
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSBackup",
        maxWidth: "800px",
        maxHeight: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: oLng_Settings.getText("Backup_ModifyBackup")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_TABLENAME"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "TABLENAME"),
                                maxLength: 50,
                                id: "txtTABLENAME",
								editable: false
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_NRLIVEDAYS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "NRLIVEDAYS"),
                                maxLength: 5,
                                id: "txtNRLIVEDAYS",
								editable: true
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_FIELDNAME"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "FIELDNAME"),
                                maxLength: 50,
                                id: "txtFIELDNAME",
								editable: true
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_MAXRECORDS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "MAXRECORDS"),
                                maxLength: 5,
                                id: "txtMAXRECORDS",
								editable: true
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_LASTJOBDATE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "LASTJOBDATE"),
                                id: "txtLASTJOBDATE",
								editable: false
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_LASTMESSAGE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextArea({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: fnGetCellValue("SSetSBackupTab", "LASTMESSAGE"),
                                id: "txtLASTMESSAGE",
								editable: false,
								rows: 5,
								cols: 180,
                            })
                        ]
                    }),
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_ERROR"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: fnGetCellValue("SSetSBackupTab", "ERROR") == "0" ? false : true,
                                id: "chkERROR",
								editable: false
                            })
                        ]
                    }),			
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Backup_ENABLED"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                checked: fnGetCellValue("SSetSBackupTab", "ENABLED") == "0" ? false : true,
                                id: "chkENABLED"
                            })
                        ]
                    }),
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Backup_Save"),
        press:function(){
            fnSaveSBackup();
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Backup_Cancel"),
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));    
	oEdtDlg.open();
}

function fnSaveSBackup () {
    var txtTABLENAME = $.UIbyID("txtTABLENAME").getValue();
	var txtNRLIVEDAYS = $.UIbyID("txtNRLIVEDAYS").getValue();
	var txtFIELDNAME = $.UIbyID("txtFIELDNAME").getValue();
	var txtMAXRECORDS = $.UIbyID("txtMAXRECORDS").getValue();
	var chkENABLED = $.UIbyID("chkENABLED").getChecked() === true ? "1" : "0";
	
	var qexe = QService + dataProdBackup + "SetMBACKUP_XQ";
    qexe += "&Param.1=" + txtTABLENAME;
    qexe += "&Param.2=" + txtNRLIVEDAYS;
    qexe += "&Param.3=" + txtFIELDNAME;
    qexe += "&Param.4=" + txtMAXRECORDS;
    qexe += "&Param.5=" + chkENABLED;
    
	// controlli
	if(isNaN(parseInt(txtNRLIVEDAYS))) {
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Backup_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Backup_NRLIVEDAYS") + "'"
        );
        return;
    }
	
	if(parseInt(txtNRLIVEDAYS) <= 0) {
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Backup_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Backup_NRLIVEDAYS") + "'"
        );
        return;
    }
	
	if(txtFIELDNAME === "") {
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Backup_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Backup_FIELDNAME") + "'"
        );
        return;
    }
	
	if(isNaN(parseInt(txtMAXRECORDS))) {
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Backup_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Backup_MAXRECORDS") + "'"
        );
        return;
    }
	
	if(parseInt(txtMAXRECORDS) < 0) {
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Backup_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Backup_MAXRECORDS") + "'"
        );
        return;
    }
	
	var oModelXML = new sap.ui.model.xml.XMLModel();
	oModelXML.loadData(qexe);
	oModelXML.attachRequestCompleted(function () {

		if(oModelXML.oData.documentElement.children["0"].children[1].children["0"].innerHTML === "OK") {
			$.UIbyID("dlgSBackup").close();
        	$.UIbyID("dlgSBackup").destroy();
			refreshTabBackup();
		} else {
			sap.ui.commons.MessageBox.show(oModelXML.oData.documentElement.children["0"].children[1].children["1"].innerHTML,
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Settings.getText("Backup_BackupSavedError")
			[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK);
		}						
	});	
}