/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Funzione comune per selezione plant da combobox
//Author: Elena Andrini
//Date:   14/02/2017
//Vers:   1.1
//**************************************************************************************
/*
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
*/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_CommonTools = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function createToolBarPage() {
    var oLblPlant = new sap.ui.commons.Label()
        .setText(oLng_CommonTools.getText("MasterData_Plant"));

    //Escape per gestione parametro plant da GET/POST
    if ($('#plant').val() === '{plant}') {
        $('#plant').val("2101");
    }

    var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
        selectedKey: $('#plant').val(),
        tooltip: oLng_CommonTools.getText("MasterData_Plant"),
        change: function (oEvent) {}
    });
    
    //Setto il default come chiave selezionata
    if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}' ) { //"") {
        $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
    }

    var oItemPlant = new sap.ui.core.ListItem();
    oItemPlant.bindProperty("key", "PLANT");
    oItemPlant.bindProperty("text", "NAME1");
    oCmbPlant.bindItems("/Rowset/Row", oItemPlant);

    var oPlantsModel = new sap.ui.model.xml.XMLModel();
    oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
    oCmbPlant.setModel(oPlantsModel);

    var oSeparator = new sap.ui.commons.Toolbar({
        items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                displayVisualSeparator: false
            }),
                    oCmbPlant
                ]
    });
    return oSeparator;
}