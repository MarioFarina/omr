//**************************************************************************************
//Title:  SSETSSTEUSVIEWS
//Author: Bruno Rosati
//Date:   06/12/2018
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSSTEUSVIEWS

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

function createTabSSetSSteusViews() {
    
    var oTable = UI5Utils.init_UI5_Table({
        id: "SSetSSteusViewsTab",
        properties: {
            visibleRowCount: 15,
			fixedColumnCount: 0,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetSSteusViewsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSSteusViews").setEnabled(false);
        			$.UIbyID("btnDelSSteusViews").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSSteusViews").setEnabled(true);
                    $.UIbyID("btnDelSSteusViews").setEnabled(true);			
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Add"),
                        id:   "btnAddSSteusViews",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openSSteusViewsDlg(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Modify"),
                        id:   "btnModSSteusViews",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSSteusViewsDlg(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Delete"),
                        id:   "btnDelSSteusViews",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelSSteusViews();
                        }
                    })
				],
				rightItems: [
					new sap.ui.commons.Button({
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabSetSSteusViews();
                        }
                    })
				]
			})
        },
        exportButton: false,
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "STEUS", 
				label: oLng_Settings.getText("Settings_STEUS"),
				properties: {
					width: "130px",
                    flexible : false
				}
            },
            {
				Field: "DESC", 
				label: oLng_Settings.getText("Settings_DESC"),
				properties: {
					width: "250px",
                    flexible : false
				}
            },
            {
				Field: "COLOR", 
				label: oLng_Settings.getText("Settings_COLOR"),
				properties: {
					width: "170px",
                    flexible : false
				}
            },
            {
				Field: "ICON", 
				label: oLng_Settings.getText("Settings_ICON"),
				properties: {
					width: "170px",
                    flexible : false
				}
            },
            {
				Field: "OUTSIDE", 
				label: oLng_Settings.getText("Settings_OUTSIDE"),
				properties: {
					width: "170px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "OPTIONAL", 
				label: oLng_Settings.getText("Settings_OPTIONAL"),
				properties: {
					width: "170px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSetSSteusViews();
    return oTable;
}

function refreshTabSetSSteusViews() {
    
    let sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    $.UIbyID("SSetSSteusViewsTab").getModel().loadData(
        QService + dataProdSett +
        "SteusViews/getSSteusViewsByParamsSQ&Param.1=" + sPlant
    );
    $.UIbyID("SSetSSteusViewsTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function openSSteusViewsDlg(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSSteusViews",
        maxWidth: "800px",
        maxHeight: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyVoice"):oLng_Settings.getText("Settings_NewVoice")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_STEUS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSSteusViewsTab", "STEUS"):"",
                                enabled: !bEdit,
                                maxLength: 4,
                                id: "txtSTEUS"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_DESC"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSSteusViewsTab", "DESC"):"",
                                enabled: true,
                                maxLength: 200,
                                id: "txtDESC"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_COLOR"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSSteusViewsTab", "COLOR"):"",
                                enabled: true,
                                maxLength: 40,
                                id: "txtCOLOR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ICON"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSSteusViewsTab", "ICON"):"",
                                enabled: true,
                                maxLength: 40,
                                id: "txtICON"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_OUTSIDE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSSteusViewsTab", "OUTSIDE") === "1" ? true : false) : false,
                                id: "chkOUTSIDE",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_OPTIONAL"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSSteusViewsTab", "OPTIONAL") === "1" ? true : false) : false,
                                id: "chkOPTIONAL",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"),
        press:function(){
            fnSaveSSteusViews(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"),
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));    
	oEdtDlg.open();
}

function fnSaveSSteusViews(bEdit) 
{
    var sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    var sSteus = $.UIbyID("txtSTEUS").getValue();
    if(sSteus == "" || sSteus == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SetID"));
        return;
    }
    var sDesc = encodeURIComponent($.UIbyID("txtDESC").getValue());
    var sColor = encodeURIComponent($.UIbyID("txtCOLOR").getValue());
    var sIcon = encodeURIComponent($.UIbyID("txtICON").getValue());
    var bOutside = $.UIbyID("chkOUTSIDE").getChecked() ? 1 : 0;
    var bOptional = $.UIbyID("chkOPTIONAL").getChecked() ? 1 : 0;
    
    var qexe = dataProdSett + "SteusViews/";    
    if(bEdit){
        qexe = qexe + "updSSteusViewSQ";
    }
    else{
        qexe = qexe + "addSSteusViewSQ";
    }
    
    qexe = qexe +
         "&Param.1=" + sSteus +
         "&Param.2=" + sPlant +
         "&Param.3=" + sDesc +
         "&Param.4=" + sColor +
         "&Param.5=" + sIcon +
         "&Param.6=" + bOutside +
         "&Param.7=" + bOptional;
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_VoiceSaved"),
                         oLng_Settings.getText("Settings_VoiceSavedError"),
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSSteusViews").close();
        $.UIbyID("dlgSSteusViews").destroy();
        $.UIbyID("SSetSSteusViewsTab").setSelectedIndex(-1);
        $.UIbyID("btnModSSteusViews").setEnabled(false);
        $.UIbyID("btnDelSSteusViews").setEnabled(false);
        refreshTabSetSSteusViews();
    }
}

function fnDelSSteusViews(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_VoiceDeleting"),
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"),
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "SteusViews/delSSteusViewSQ" +
                            "&Param.1=" + fnGetCellValue("SSetSSteusViewsTab", "STEUS") +
                            "&Param.2=" + fnGetCellValue("SSetSSteusViewsTab", "PLANT");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_VoiceDeleted"),
                    oLng_Settings.getText("Settings_VoiceDeletedError"),
                    false
                );
                refreshTabSetSSteusViews();
                $.UIbyID("btnModSSteusViews").setEnabled(false);
                $.UIbyID("btnDelSSteusViews").setEnabled(false);
                $.UIbyID("SSetSSteusViewsTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}