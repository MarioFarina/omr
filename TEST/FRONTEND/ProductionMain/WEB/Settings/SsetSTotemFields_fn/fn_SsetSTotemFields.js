//**************************************************************************************
//Title:  SSETSTOTEMFIELDS
//Author: Bruno Rosati
//Date:   22/11/2018
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSTOTEMFIELDS

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

function createTabSSetSTotemFields() {
    
    var oTable = UI5Utils.init_UI5_Table({
        id: "SSetSTotemFieldsTab",
        properties: {
            visibleRowCount: 15,
			fixedColumnCount: 0,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetSTotemFieldsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSTotemFields").setEnabled(false);
        			$.UIbyID("btnDelSTotemFields").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSTotemFields").setEnabled(true);
                    $.UIbyID("btnDelSTotemFields").setEnabled(true);			
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Add"),
                        id:   "btnAddSTotemFields",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openSTotemFieldsDlg(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Modify"),
                        id:   "btnModSTotemFields",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSTotemFieldsDlg(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Delete"),
                        id:   "btnDelSTotemFields",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelSTotemFields();
                        }
                    })
				],
				rightItems: [
					new sap.ui.commons.Button({
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabSetSTotemFields();
                        }
                    })
				]
			})
        },
        exportButton: false,
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "FLOWID", 
				label: oLng_Settings.getText("Settings_FLOWID"),
				properties: {
					width: "100px",
                    flexible : false
				}
            },
            {
				Field: "FIELDNAME", 
				label: oLng_Settings.getText("Settings_FIELDNAME"),
				properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "FSTATE", 
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "FSTATE_TEXT", 
				label: oLng_Settings.getText("Settings_FSTATE"),
				properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "NOTE", 
				label: oLng_Settings.getText("Settings_NOTE"),
				properties: {
					width: "500px",
                    flexible : false
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSetSTotemFields();
    return oTable;
}

function refreshTabSetSTotemFields() {
    
    let sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    $.UIbyID("SSetSTotemFieldsTab").getModel().loadData(
        QService + dataProdSett +
        "TotemFields/getSTotemFieldsByParamsSQ&Param.1=" + sPlant + "&Param.2=" + sLanguage
    );
    $.UIbyID("SSetSTotemFieldsTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function openSTotemFieldsDlg(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbFState = new sap.ui.commons.ComboBox("cmbFState", {
        enabled: true,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {}
    });
    var oItemFState = new sap.ui.core.ListItem();
    oItemFState.bindProperty("key", "FSTATE");
    oItemFState.bindProperty("text", "FSTATE_TEXT");
    oCmbFState.bindItems("/Rowset/Row", oItemFState);
    var oFStateModel = new sap.ui.model.xml.XMLModel();
    oFStateModel.loadData(QService + dataProdSett + "TotemFields/getFStateByLanguageSQ&Param.1=" + sLanguage);
    oCmbFState.setModel(oFStateModel);
    
    if(bEdit){
        oCmbFState.setSelectedKey(fnGetCellValue("SSetSTotemFieldsTab", "FSTATE"));
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSTotemFields",
        maxWidth: "800px",
        maxHeight: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyVoice"):oLng_Settings.getText("Settings_NewVoice")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_FLOWID"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFieldsTab", "FLOWID"):"",
                                enabled: !bEdit,
                                maxLength: 20,
                                id: "txtFLOWID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_FIELDNAME"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFieldsTab", "FIELDNAME"):"",
                                enabled: !bEdit,
                                maxLength: 100,
                                id: "txtFIELDNAME"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_FSTATE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            oCmbFState
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_NOTE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFieldsTab", "NOTE"):"",
                                maxLength: 200,
                                id: "txtNOTE"
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"),
        press:function(){
            fnSaveSTotemFields(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"),
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbFState.destroy();
        }
    }));    
	oEdtDlg.open();
}

function fnSaveSTotemFields(bEdit) 
{
    var sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    var sFlowId = $.UIbyID("txtFLOWID").getValue();
    if(sFlowId == "" || sFlowId == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SetID"));
        return;
    }
    var sFieldName = encodeURIComponent($.UIbyID("txtFIELDNAME").getValue());
    var sFState = $.UIbyID("cmbFState").getSelectedKey();
    if(sFState == "" || sFState == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectType"));
        return;
    }
    var sNote = encodeURIComponent($.UIbyID("txtNOTE").getValue());
    
    var qexe = dataProdSett + "TotemFields/";    
    if(bEdit){
        qexe = qexe + "updSTotemFieldsSQ";
    }
    else{
        qexe = qexe + "addSTotemFieldsSQ";
    }
    
    qexe = qexe +
         "&Param.1=" + sPlant +
         "&Param.2=" + sFlowId +
         "&Param.3=" + sFieldName +
         "&Param.4=" + sFState +
         "&Param.5=" + sNote;
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_VoiceSaved"),
                         oLng_Settings.getText("Settings_VoiceSavedError"),
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSTotemFields").close();
        $.UIbyID("dlgSTotemFields").destroy();
        $.UIbyID("SSetSTotemFieldsTab").setSelectedIndex(-1);
        $.UIbyID("btnModSTotemFields").setEnabled(false);
        $.UIbyID("btnDelSTotemFields").setEnabled(false);
        refreshTabSetSTotemFields();
    }
}

function fnDelSTotemFields(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_VoiceDeleting"),
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"),
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "TotemFields/delSTotemFieldsSQ" +
                            "&Param.1=" + fnGetCellValue("SSetSTotemFieldsTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("SSetSTotemFieldsTab", "FLOWID") +
                            "&Param.3=" + fnGetCellValue("SSetSTotemFieldsTab", "FIELDNAME");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_VoiceDeleted"),
                    oLng_Settings.getText("Settings_VoiceDeletedError"),
                    false
                );
                refreshTabSetSTotemFields();
                $.UIbyID("btnModSTotemFields").setEnabled(false);
                $.UIbyID("btnDelSTotemFields").setEnabled(false);
                $.UIbyID("SSetSTotemFieldsTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}