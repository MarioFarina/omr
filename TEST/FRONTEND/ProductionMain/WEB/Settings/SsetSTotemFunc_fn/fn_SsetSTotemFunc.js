//**************************************************************************************
//Title:  SSETSTOTEMFUNC
//Author: Bruno Rosati
//Date:   15/1/2018
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSTOTEMFUNC

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

function createTabSSetSTotemFunc() {
    
    var oTable = UI5Utils.init_UI5_Table({
        id: "SSetSTotemFuncTab",
        properties: {
            visibleRowCount: 15,
			fixedColumnCount: 0,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetSTotemFuncTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSTotemFunc").setEnabled(false);
        			$.UIbyID("btnDelSTotemFunc").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSTotemFunc").setEnabled(true);
                    $.UIbyID("btnDelSTotemFunc").setEnabled(true);			
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Add"),
                        id:   "btnAddSTotemFunc",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openSTotemFuncDlg(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Modify"),
                        id:   "btnModSTotemFunc",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSTotemFuncDlg(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Delete"),
                        id:   "btnDelSTotemFunc",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelSTotemFunc();
                        }
                    })
				],
				rightItems: [
					new sap.ui.commons.Button({
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabSetSTotemFunc();
                        }
                    })
				]
			})
        },
        exportButton: false,
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "FLOWID", 
				label: oLng_Settings.getText("Settings_FLOWID"),
				properties: {
					width: "100px",
                    flexible : false
				}
            },
            {
				Field: "ORDTYPE", 
				label: oLng_Settings.getText("Settings_ORDTYPE"),
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "MSTEUS", 
				label: oLng_Settings.getText("Settings_MSTEUS"),
				properties: {
					width: "135px",
                    flexible : false
				}
            },            
            {
				Field: "ORDFLAG", 
				properties: {
					visible: false,
                    flexible : false
				}
            },
            {
				Field: "ORDFLAG_TEXT",
				label: oLng_Settings.getText("Settings_ORDFLAG"),
                properties: {
					width: "135px",
                    flexible : false
				}
            },
            {
				Field: "TPOPE", 
				properties: {
					visible: false,
                    flexible : false
				}
            },
            {
				Field: "TPOPE_TEXT", 
				label: oLng_Settings.getText("Settings_TPOPE"),
				properties: {
					width: "145px",
                    flexible : false
				}
            },
            {
				Field: "NOTE", 
				label: oLng_Settings.getText("Settings_NOTE_STOTEMFUNC"),
				properties: {
					width: "170px",
                    flexible : false
				}
            },
            {
				Field: "UDCFULL", 
				label: oLng_Settings.getText("Settings_UDCFULL"),
				properties: {
					width: "145px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "UDCPRINT", 
				label: oLng_Settings.getText("Settings_UDCPRINT"),
				properties: {
					width: "170px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "QTYMDT", 
				label: oLng_Settings.getText("Settings_QTYMDT"),
                tooltip: oLng_Settings.getText("Settings_QTYMDTtooltip"),
                properties: {
					width: "170px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "COLLECTION", 
				label: oLng_Settings.getText("Settings_COLLECTION"),
				tooltip: oLng_Settings.getText("Settings_COLLECTIONtooltip"),
                properties: {
					width: "165px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "SENDTOSAP", 
				label: oLng_Settings.getText("Settings_SENDTOSAP"),
				properties: {
					width: "160px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "TPOPESAP", 
				label: oLng_Settings.getText("Settings_TPOPESAP"),
				properties: {
					width: "120px",
                    flexible : false
				}
            },
            {
				Field: "OPEREVERSE", 
				label: oLng_Settings.getText("Settings_OPEREVERSE"),
				tooltip: oLng_Settings.getText("Settings_OPEREVERSEtooltip"),
                properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "SAPREVERSE", 
				label: oLng_Settings.getText("Settings_SAPREVERSE"),
				tooltip: oLng_Settings.getText("Settings_SAPREVERSEtooltip"),
                properties: {
					width: "140px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "SKIPSTEUS", 
				label: oLng_Settings.getText("Settings_SKIPSTEUS"),
				tooltip: oLng_Settings.getText("Settings_SKIPSTEUStooltip"),
                properties: {
					width: "200px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            },
            {
				Field: "NSTEUS", 
				label: oLng_Settings.getText("Settings_NSTEUS"),
				tooltip: oLng_Settings.getText("Settings_NSTEUStooltip"),
                properties: {
					width: "185px",
                    flexible : false
				}
            },
            {
				Field: "RSTEUS", 
				label: oLng_Settings.getText("Settings_RSTEUS"),
				tooltip: oLng_Settings.getText("Settings_RSTEUStooltip"),
                properties: {
					width: "170px",
                    flexible : false
				}
            },
            {
				Field: "ENABLED", 
				label: oLng_Settings.getText("Settings_ENABLED"),
				properties: {
					width: "110px",
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSetSTotemFunc();
    return oTable;
}

function refreshTabSetSTotemFunc() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("SSetSTotemFuncTab").getModel().loadData(
            QService + dataProdSett +
            "TotemFunc/getSTotemFuncByParamsSQ&Param.1=" + $('#plant').val() + "&Param.2=" + sLanguage
        );
    }
    else{
        $.UIbyID("SSetSTotemFuncTab").getModel().loadData(
            QService + dataProdSett +
            "TotemFunc/getSTotemFuncByParamsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + sLanguage
        );
    }
    $.UIbyID("SSetSTotemFuncTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function openSTotemFuncDlg(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbOrdFlag = new sap.ui.commons.ComboBox("cmbOrdFlag", {
        enabled: true,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {}
    });
    var oItemOrdFlag = new sap.ui.core.ListItem();
    oItemOrdFlag.bindProperty("key", "ORDFLAG");
    oItemOrdFlag.bindProperty("text", "ORDFLAG_TEXT");
    oCmbOrdFlag.bindItems("/Rowset/Row", oItemOrdFlag);
    var oOrdFlagModel = new sap.ui.model.xml.XMLModel();
    oOrdFlagModel.loadData(QService + dataProdSett + "TotemFunc/getOrdFlagByLanguageSQ&Param.1=" + sLanguage);
    oCmbOrdFlag.setModel(oOrdFlagModel);
    
    var oCmbTpope = new sap.ui.commons.ComboBox("cmbTpope", {
        enabled: true,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {}
    });
    var oItemTpope = new sap.ui.core.ListItem();
    oItemTpope.bindProperty("key", "TPOPE");
    oItemTpope.bindProperty("text", "TPOPE_TEXT");
    oCmbTpope.bindItems("/Rowset/Row", oItemTpope);
    var oTpopeModel = new sap.ui.model.xml.XMLModel();
    oTpopeModel.loadData(QService + dataProdSett + "TotemFunc/getTpopeByLanguageSQ&Param.1=" + sLanguage);
    oCmbTpope.setModel(oTpopeModel);
    
    if(bEdit){
        oCmbOrdFlag.setSelectedKey(fnGetCellValue("SSetSTotemFuncTab", "ORDFLAG"));
        oCmbTpope.setSelectedKey(fnGetCellValue("SSetSTotemFuncTab", "TPOPE"));
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSTotemFunc",
        maxWidth: "1200px",
        maxHeight: "900px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyVoice"):oLng_Settings.getText("Settings_NewVoice")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_FLOWID"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "FLOWID"):"",
                                enabled: !bEdit,
                                maxLength: 20,
                                id: "txtFLOWID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ORDTYPE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "ORDTYPE"):"",
                                maxLength: 4,
                                id: "txtORDTYPE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_MSTEUS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "MSTEUS"):"",
                                maxLength: 100,
                                id: "txtMSTEUS"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ORDFLAG"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbOrdFlag
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_TPOPE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbTpope
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_NOTE_STOTEMFUNC"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "NOTE"):"",
                                maxLength: 200,
                                id: "txtNOTE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_UDCFULL"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "UDCFULL") === "1" ? true : false) : false,
                                id: "chkUDCFULL",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_UDCPRINT"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "UDCPRINT") === "1" ? true : false) : false,
                                id: "chkUDCPRINT",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_QTYMDT"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "QTYMDT") === "1" ? true : false) : false,
                                id: "chkQTYMDT",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_COLLECTION"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "COLLECTION") === "1" ? true : false) : false,
                                id: "chkCOLLECTION",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_SENDTOSAP"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "SENDTOSAP") === "1" ? true : false) : false,
                                id: "chkSENDTOSAP",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_TPOPESAP"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "TPOPESAP"):"",
                                maxLength: 2,
                                id: "txtTPOPESAP"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_OPEREVERSE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "OPEREVERSE"):"",
                                maxLength: 2,
                                id: "txtOPEREVERSE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_SAPREVERSE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "SAPREVERSE") === "1" ? true : false) : false,
                                id: "chkSAPREVERSE",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_SKIPSTEUS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "SKIPSTEUS") === "1" ? true : false) : false,
                                id: "chkSKIPSTEUS",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_NSTEUS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "NSTEUS"):"",
                                maxLength: 100,
                                id: "txtNSTEUS"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_RSTEUS"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSTotemFuncTab", "RSTEUS"):"",
                                maxLength: 100,
                                id: "txtRSTEUS"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ENABLED"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                checked: bEdit ? (fnGetCellValue("SSetSTotemFuncTab", "ENABLED") === "1" ? true : false) : false,
                                id: "chkENABLED",
                                editable: true,
                                change: function () {}
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"),
        press:function(){
            fnSaveSTotemFunc(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"),
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbOrdFlag.destroy();
            oCmbTpope.destroy();
            oCmbStatus.destroy();
        }
    }));    
	oEdtDlg.open();
}

function fnSaveSTotemFunc(bEdit) 
{
    var sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    var sFlowId = $.UIbyID("txtFLOWID").getValue();
    if(sFlowId == "" || sFlowId == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_FLOWID") + "'"
        );
        return;
    }
    var sOrdType = encodeURIComponent($.UIbyID("txtORDTYPE").getValue());
    if(sOrdType == "" || sOrdType == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_ORDTYPE") + "'"
        );
        return;
    }
    var sMSteus = encodeURIComponent($.UIbyID("txtMSTEUS").getValue());
    if(sMSteus == "" || sMSteus == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_MSTEUS") + "'"
        );
        return;
    }
    var sOrdFlag = $.UIbyID("cmbOrdFlag").getSelectedKey();
    if(sOrdFlag == "" || sOrdFlag == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_ORDFLAG") + "'"
        );
        return;
    }
    var sTpope = $.UIbyID("cmbTpope").getSelectedKey();
    if(sTpope == "" || sTpope == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_TPOPE") + "'"
        );
        return;
    }
    var bQtyMdt = $.UIbyID("chkQTYMDT").getChecked() ? 1 : 0;
    var bUdcFull = $.UIbyID("chkUDCFULL").getChecked() ? 1 : 0;
    var bUdcPrint = $.UIbyID("chkUDCPRINT").getChecked() ? 1 : 0;
    var bCollection = $.UIbyID("chkCOLLECTION").getChecked() ? 1 : 0;
    var bSendToSap = $.UIbyID("chkSENDTOSAP").getChecked() ? 1 : 0;
    var sNSteus = encodeURIComponent($.UIbyID("txtNSTEUS").getValue());
    var sRSteus = encodeURIComponent($.UIbyID("txtRSTEUS").getValue());
    var bSkipSteus = $.UIbyID("chkSKIPSTEUS").getChecked() ? 1 : 0;
    var sNote = encodeURIComponent($.UIbyID("txtNOTE").getValue());
    if(sNote == "" || sNote == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_NOTE_STOTEMFUNC") + "'"
        );
        return;
    }
    var sTpopeSap = encodeURIComponent($.UIbyID("txtTPOPESAP").getValue());
    if(sTpopeSap == "" || sTpopeSap == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_TPOPESAP") + "'"
        );
        return;
    }
    var sOpeReverse = encodeURIComponent($.UIbyID("txtOPEREVERSE").getValue());
    var bSapReverse = $.UIbyID("chkSAPREVERSE").getChecked() ? 1 : 0;
    var bEnabled = $.UIbyID("chkENABLED").getChecked() ? 1 : 0;
    
    var qexe = dataProdSett + "TotemFunc/";    
    if(bEdit){
        qexe = qexe + "updSTotemFuncSQ";
    }
    else{
        qexe = qexe + "addSTotemFuncSQ";
    }
    
    qexe = qexe +
         "&Param.1=" + sPlant +
         "&Param.2=" + sFlowId +
         "&Param.3=" + sOrdType +
         "&Param.4=" + sOrdFlag +
         "&Param.5=" + sMSteus +
         "&Param.6=" + sTpope +
         "&Param.7=" + bQtyMdt +
         "&Param.8=" + bUdcFull +
         "&Param.9=" + bUdcPrint +
        "&Param.10=" + bCollection +
        "&Param.11=" + bSendToSap +
        "&Param.12=" + sNSteus +
        "&Param.13=" + sRSteus +
        "&Param.14=" + bSkipSteus +
        "&Param.15=" + sNote +
        "&Param.16=" + sTpopeSap +
        "&Param.17=" + sOpeReverse +
        "&Param.18=" + bSapReverse +
        "&Param.19=" + bEnabled;
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_VoiceSaved"),
                         oLng_Settings.getText("Settings_VoiceSavedError"),
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSTotemFunc").close();
        $.UIbyID("dlgSTotemFunc").destroy();
        $.UIbyID("SSetSTotemFuncTab").setSelectedIndex(-1);
        $.UIbyID("btnModSTotemFunc").setEnabled(false);
        $.UIbyID("btnDelSTotemFunc").setEnabled(false);
        refreshTabSetSTotemFunc();
    }
}

function fnDelSTotemFunc(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_VoiceDeleting"),
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"),
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "TotemFunc/delSTotemFuncSQ" +
                            "&Param.1=" + fnGetCellValue("SSetSTotemFuncTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("SSetSTotemFuncTab", "FLOWID");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_VoiceDeleted"),
                    oLng_Settings.getText("Settings_VoiceDeletedError"),
                    false
                );
                refreshTabSetSTotemFunc();
                $.UIbyID("btnModSTotemFunc").setEnabled(false);
                $.UIbyID("btnDelSTotemFunc").setEnabled(false);
                $.UIbyID("SSetSTotemFuncTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}