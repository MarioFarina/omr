//**************************************************************************************
//Title:  Funzioni per il recupero delle informazioni dei tipi operazione SAP
//Author: Bruno Rosati
//Date:   21/11/2018
//Vers:   1.0
//**************************************************************************************
// Script per il recupero delle informazioni dei tipi operazione SAP

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProdSett = "Content-Type=text/XML&QueryTemplate=ProductionMain/Settings/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

function fnGetTPopeSapTableDialog(sPlant, sSelected, cbFunction) {
	if (!$.UIbyID("dlgTpopeSapTableSelect")) {

		var oTpopeSapModel = new sap.ui.model.xml.XMLModel();
        query = QService + dataProdSett + 
            "TotemFunc/getTPopeSapListSQ&Param.1=" + sPlant;
        oTpopeSapModel.loadData(query);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
                new sap.m.Text({
					text: "",
                    title: ""
				}),
                new sap.m.Text({
					text: "{TPOPESAP}",
                    title: "{}"
				}),
				new sap.m.Text({
					text: "{NOTE}",
                    title: "{}"
				})
			]
		});

		var oDlgTpopeSapSelect = new sap.m.TableSelectDialog("dlgTpopeSapTableSelect", {
			title: oLng_Settings.getText("Settings_TPopeSapList"),
			noDataText: oLng_Settings.getText("Settings_NoInsert"),
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: true,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
            items: [],
			columns:[
				new sap.m.Column({header: new sap.m.Label({
                    text: "",
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_Settings.getText("Settings_TPopeSap"),
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_Settings.getText("Settings_Description"),
                    minScreenWidth: "40em"
                })})
            ],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("TPOPESAP", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("NOTE", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
            cancel: function (oControlEvent){
                $.UIbyID("dlgTpopeSapTableSelect").destroy();
            }
		});

		oDlgTpopeSapSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		oDlgTpopeSapSelect.setModel(oTpopeSapModel);

		oDlgTpopeSapSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgTpopeSapTableSelect").setContentWidth("800px");


	if (sSelected == "" || sSelected == null) 
        $.UIbyID("dlgTpopeSapTableSelect").open();
	else {
		$.UIbyID("dlgTpopeSapTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SOURCE", sap.ui.model.FilterOperator.EQ, sSelected),
				 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgTpopeSapTableSelect").open(sSelected);
	}

	$.UIbyID("dlgTpopeSapTableSelect").focus();
    
}