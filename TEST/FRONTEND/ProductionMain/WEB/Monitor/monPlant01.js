// Script per pagina pannello impianti

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";


jQuery.ajaxSetup({
	cache: false
});
jQuery.sap.includeScript("/XMII/CM/ProductionMain/Monitor/fn_lineDet.js?v=" + Date.now());
jQuery.sap.require("sap.ui.core.format.NumberFormat");

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/

function refreshTabLines(Model) {

	var qParams = {
		data: "ProductionMain/Monitor/getMonitorPlantQR&Param.1=" + $('#plant').val()
		+ "&Param.2=" + $('#group').val()
		+ "&Param.3=" + $('#depart').val()
		+ "&Param.4=" + "it",
		dataType: "json"
	};

	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		// pupulate the model
		Model.setData(data);
		updatePage(Model);
	})
	// on fail
		.fail(function () {
		//sap.ui.commons.MessageBox.alert("Errore nell'aggiornamento dati");
		sap.m.MessageToast.show("Errore nell'aggiornamento dati", {
			duration: 45000,
			at: sap.ui.core.Popup.Dock.CenterTop,
			my: sap.ui.core.Popup.Dock.CenterTop,
		});
		$.UIbyID("appHeader").setLogoText("Cruscotto - (errore in aggiornamento)");
	})
	// always, either on success or fail
		.always(function () {
		// remove busy indicator
		//oTable.setBusy(false);
	});
}


function updatePage(Model) {
	var sTitle = Model.getProperty("/Rowsets/Rowset/0/Row/0/REPTXT");

	if (typeof sTitle != 'undefined') {
		var currentdate = new Date();
		var curTime = ((currentdate.getHours() < 10)?"0":"") +currentdate.getHours()  + ":"
		+ ((currentdate.getMinutes() < 10)?"0":"") + currentdate.getMinutes() + ":"
		+((currentdate.getSeconds() < 10)?"0":"") + currentdate.getSeconds();
		$.UIbyID("appHeader").setLogoText("Cruscotto " + sTitle + " - (aggior.: " +curTime + ")");
	}

	if (window.console) console.log("Refresh data");
	$(".sapUiProgInd").each(function() {
		var curPerc = $.UIbyID($( this ).attr( "id" )).getDisplayValue();
		if (curPerc.indexOf("+")>0) $.UIbyID($( this ).attr( "id" )).setBarColor(sap.ui.core.BarColor.NEGATIVE );
		else $.UIbyID($( this ).attr( "id" )).setBarColor(sap.ui.core.BarColor.NEUTRAL  );
	});
}

$(document).ready(function () {

	var oDataSet = createDatasetLines();

	oDataSet.placeAt("MasterCont");
	$("#splash-screen").hide();

	tabRefr = $.timer(function () {
		refreshTabLines($.UIbyID("dsMon").getModel());
	}, 90 * 1000, false);
	tabRefr.set({
		autostart: true
	});

});






function createDatasetLines() {

	//Create a custom control as template for the Dataset items
	sap.ui.core.Control.extend("ItemLayout", {
		metadata: {
			aggregations: {
				"title": {
					type: "sap.ui.commons.TextView",
					multiple: false
				},
				"image": {
					type: "sap.ui.commons.Image",
					multiple: false
				},
				"form": {
					type: "sap.ui.commons.form.Form",
					multiple: false
				},
				"link": {
					type: "sap.ui.commons.Link",
					multiple: false
				},
			},
			properties: {
				"CustomStyle": {
					type: "string",
					defaultValue: "code"
				}
			}
		},
		renderer: function (rm, ctrl) {
			rm.write("<div");
			rm.writeControlData(ctrl);
			rm.writeAttribute("class", "CustomItemLayout " + ctrl.getCustomStyle());
			//rm.writeAttribute("class", ctrl.getCustomStyle());
			rm.write("><div");
			rm.writeAttribute("class", "CustomItemLayoutInner");
			rm.write("><div");
			rm.writeAttribute("class", "CustomItemLayoutTitle");
			rm.write(">");
			rm.write("<div>");
			rm.renderControl(ctrl.getTitle());
			rm.write("</div>");
			rm.renderControl(ctrl.getImage());
			rm.write("</div><div");
			rm.writeAttribute("class", "CustomItemLayoutCntnt");
			rm.write(">");
			rm.renderControl(ctrl.getForm());
			rm.write("</div></div></div>");
		},
		onBeforeRendering: function () {
			if (this.resizeTimer) {
				clearTimeout(this.resizeTimer);
				this.resizeTimer = null;
			}
		},
		onAfterRendering: function () {
			var $This = this.$();
			if ($This.parent().parent().hasClass("sapUiUx3DSSVSingleRow")) {
				this._resize();
			} else {
				$This.addClass("CustomItemLayoutSmall");
			}

			$(".sapUiProgInd").height(20);
			$(".sapUiProgIndBarPos").height(18);
			$(".sapUiProgIndEndHidden").height(18);
			//$(".sapUiProgIndBorder").height(20);

		},

		_resize: function () {
			if (!this.getDomRef()) {
				return;
			}
			var $This = this.$();
			if ($This.outerWidth() >= 440) {
				$This.removeClass("CustomItemLayoutSmall").addClass("CustomItemLayoutLarge");
			} else {
				$This.removeClass("CustomItemLayoutLarge").addClass("CustomItemLayoutSmall");
			}
			setTimeout(jQuery.proxy(this._resize, this), 300);
			$(".sapUiProgIndBorder").height(20);
			$(".sapUiProgInd").height(20);

		}
	});


	//var oModel = new sap.ui.model.xml.XMLModel();
	var oModel = new sap.ui.model.json.JSONModel();
	refreshTabLines(oModel);
	//oModel.loadData(QService + dataMonitor + "PhMonitor02QR&Param.2="+$('#area').val());
	//oTable.setModel(oModel);

	var oDataSet = new sap.ui.ux3.DataSet({
		id: "dsMon",
		items: {
			path: "/Rowsets/Rowset/0/Row",
			template: new sap.ui.ux3.DataSetItem({
				title: "{IDLINE} / {LINETXT}",
				iconSrc: "{ICONURL}"
			})
		},
		views: [
			new sap.ui.ux3.DataSetSimpleView({
				name: "Floating, non-responsive View",
				icon: "/XMII/CM/Common/Images/tiles.png",
				iconHovered: "/XMII/CM/Common/Images/tiles2_hover.png",
				iconSelected: "/XMII/CM/Common/Images/tiles2_hover.png",
				floating: true,
				responsive: true,
				itemMinWidth: 400,
				template: createTemplate()
			}),
			new sap.ui.ux3.DataSetSimpleView({
				name: "Floating, responsive View",
				icon: "/XMII/CM/Common/Images/tiles.png",
				iconHovered: "/XMII/CM/Common/Images/tiles_hover.png",
				iconSelected: "/XMII/CM/Common/Images/tiles_hover.png",
				floating: true,
				responsive: true,
				itemMinWidth: 350,
				template: createTemplate2()
			})
		],
		search: function search(oEvent) {
			var sQuery = oEvent.getParameter("query");
			var oBinding = oDataSet.getBinding("items");
			oBinding.filter(!sQuery ? [] : [new sap.ui.model.Filter("IDLINE", sap.ui.model.FilterOperator.Contains, sQuery)]);
			oDataSet.setLeadSelection(-1);
		},
		selectionChanged: function search(oEvent) {
			var idx = oEvent.getParameter("newLeadSelectedIndex");
			if (window.console) console.log("Riga -> " + idx);

			//if (idx >= 0) showPrDet(idx, "dsMon", "/Rows");

			//alert($.UIbyID("dsMon").getModel().getProperty("/Rows/"+idx+"/PRESS" ))
		}
	});
	oDataSet.setModel(oModel);
	return oDataSet;

}


//Initialize the Dataset and the layouts
function createTemplate() {
	var c = sap.ui.commons;

	return new ItemLayout({
		title: new c.TextView({
			text: "{LINETXT}",
			tooltip: "{IDLINE}",
			design: sap.ui.commons.TextViewDesign.H2
		}),
		image: new c.Image({
			src: "{ICONURL}"
		}),
		CustomStyle: "{COLAREA}",
		form: new c.form.Form({
			width: "100%",
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [
						new c.form.FormElement({
							label: new c.Label({
								text: "Articolo: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{MATID}",
								design: sap.ui.commons.TextViewDesign.H4
							}), new c.Button({
								icon: "sap-icon://display",
								enabled: true,
								text: "Dett.",
								style: sap.ui.commons.ButtonStyle.Default,
								press: function (oEvent) {
									var idx = oEvent.getParameter("id");
									if (window.console) console.log($.UIbyID(idx).data("LINEID"));
									var sLineID = $.UIbyID(idx).data("LINEID");
									fnShowLineDet(sLineID, 2)
								}
							}).data("LINEID", "{IDLINE}")]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "",
								layoutData: new c.form.GridElementData({
									hCells: "1"
								})
							}),
							fields: [new c.TextView({
								text: "{MAKTX}",
								textAlign: sap.ui.core.TextAlign.Center,
								semanticColor: sap.ui.commons.TextViewColor.Default
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Commessa: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{LOTID}",
								design: sap.ui.commons.TextViewDesign.H4
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Qtà WIP.: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{QTY_WIP}",
								design: sap.ui.commons.TextViewDesign.H4,
								semanticColor: sap.ui.commons.TextViewColor.Critical
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Pezzi Ing. / Usc.:",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{QTY_IN}",
								tooltip: "Pezzi in Ingresso",
								textAlign: sap.ui.core.TextAlign.Right,
								design: sap.ui.commons.TextViewDesign.H4
							}),
							 new c.Label({
								 text: " / ",
								 textAlign: sap.ui.core.TextAlign.Center,
								 layoutData: new c.form.GridElementData({
									 hCells: "2"
								 })
							 }),
							 new c.TextView({
								 text: "{QTY_OUT}",
								 tooltip: "Pezzi in Uscita",
								 design: sap.ui.commons.TextViewDesign.H4
							 })]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Sospesi: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [
								new c.TextView({
									text: "{QTY_SCRAP}",
									tooltip: "Totale MQ Lotto in Ingresso",
									textAlign: sap.ui.core.TextAlign.Left,
									design: sap.ui.commons.TextViewDesign.H4,
									semanticColor: sap.ui.commons.TextViewColor.Negative
								})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Stato Linea: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{STATDESC}",
								design: sap.ui.commons.TextViewDesign.H5,
								semanticColor: sap.ui.commons.TextViewColor.Default
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Ultimo aggiorn.: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{DATE_UPD}",
								design: sap.ui.commons.TextViewDesign.H5,
								semanticColor: sap.ui.commons.TextViewColor.Default
							})]
						})
					]
				})
			]
		})
	});
}

//Initialize the Dataset and the layouts
function createTemplate2() {
	var c = sap.ui.commons;

	return new ItemLayout({
		title: new c.TextView({
			text: "{LINETXT}",
			tooltip: "{IDLINE}",
			design: sap.ui.commons.TextViewDesign.H2
		}),
		image: new c.Image({
			src: "{ICONURL}"
		}),
		CustomStyle: "{COLAREA}",
		form: new c.form.Form({
			width: "100%",
			layout: new c.form.GridLayout(),
			formContainers: [
				new c.form.FormContainer({
					formElements: [
						new c.form.FormElement({
							label: new c.Label({
								text: "Articolo: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{MATID}",
								design: sap.ui.commons.TextViewDesign.H4
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "",
								layoutData: new c.form.GridElementData({
									hCells: "1"
								})
							}),
							fields: [new c.TextView({
								text: "{MAKTX}",
								textAlign: sap.ui.core.TextAlign.Center,
								semanticColor: sap.ui.commons.TextViewColor.Default
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Commessa: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{LOTID}",
								design: sap.ui.commons.TextViewDesign.H4
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Qtà teorica.: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{QTY_TARG}",
								textAlign: sap.ui.core.TextAlign.Left,
								design: sap.ui.commons.TextViewDesign.H4,
								semanticColor: sap.ui.commons.TextViewColor.Critical
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Pezzi prodotti:",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{QTY_OUT}",
								tooltip: "Pezzi in uscita",
								textAlign: sap.ui.core.TextAlign.Left,
								design: sap.ui.commons.TextViewDesign.H4
							})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Sospesi: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [
								new c.TextView({
									text: "{QTY_SCRAP}",
									tooltip: "Totale MQ Lotto in Ingresso",
									textAlign: sap.ui.core.TextAlign.Left,
									design: sap.ui.commons.TextViewDesign.H4,
									semanticColor: sap.ui.commons.TextViewColor.Negative
								})]
						}),
						new c.form.FormElement({
							label: new c.Label({
								text: "Stato Linea: ",
								layoutData: new c.form.GridElementData({
									hCells: "6"
								})
							}),
							fields: [new c.TextView({
								text: "{STATDESC}",
								design: sap.ui.commons.TextViewDesign.H5,
								semanticColor: sap.ui.commons.TextViewColor.Default
							})]
						})
					]
				})
			]
		})
	});
}
