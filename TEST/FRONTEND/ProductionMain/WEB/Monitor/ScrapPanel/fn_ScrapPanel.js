/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor prodotti non conformi (sospesi)
//Author: Bruno Rosati
//Date:   21/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor prodotti non conformi (sospesi)

var oPlantFOperator;
var oCmbCID;


// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabScrapPanel() {
    
    var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom');
    oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerF.setLocale("it");
    oDatePickerF.attachChange(
		function(oEvent){
			refreshTabScrapPanel();
		}
	);

    var oDatePickerT = new sap.ui.commons.DatePicker('dtTo');
    oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerT.setLocale("it");
    oDatePickerT.attachChange(
		function(oEvent){
			refreshTabScrapPanel();
		}
	);
    
    var sSelectedPlantKey;
    if($.UIbyID("filtPlant").getSelectedKey() === ""){
        sSelectedPlantKey = $('#plant').val();
    }
    else{
        sSelectedPlantKey = $.UIbyID("filtPlant").getSelectedKey();
    }
    
    var oModelShift = new sap.ui.model.xml.XMLModel();
	oModelShift.loadData(
        QService + dataProdMD +"Shifts/getShiftsByPlantSQ&Param.1=" + sSelectedPlantKey
    );

	// Crea la ComboBox per le divisioni in input
	var oCmbShift = new sap.ui.commons.ComboBox({
        id: "cmbShift", 
        selectedKey: 1
    });
	oCmbShift.setModel(oModelShift);		
	var oItemShift = new sap.ui.core.ListItem();
	oItemShift.bindProperty("text", "SHNAME");
	oItemShift.bindProperty("key", "SHIFT");	
	oCmbShift.bindItems("/Rowset/Row", oItemShift);
    oCmbShift.attachChange(
		function(oEvent){
            $.UIbyID("delFilter").setEnabled(true);
			refreshTabScrapPanel();
		}
	);
    
    //$.UIbyID("filtPlant").setSelectedKey($('#plant').val());
    
    var oModelLine = new sap.ui.model.xml.XMLModel();
	oModelLine.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + sSelectedPlantKey);

	// Crea la ComboBox per le divisioni in input
	var oCmbLine = new sap.ui.commons.ComboBox({
        id: "cmbLine", 
        selectedKey: 1
    });
	oCmbLine.setModel(oModelLine);		
	var oItemLine = new sap.ui.core.ListItem();
	oItemLine.bindProperty("text", "LINETXT");
	oItemLine.bindProperty("key", "IDLINE");	
	oCmbLine.bindItems("/Rowset/Row", oItemLine);
    oCmbLine.attachChange(
		function(oEvent){
            $.UIbyID("delFilter").setEnabled(true);
			refreshTabScrapPanel();
		}
	);
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "ScrapPanelTab",
        properties: {
            title: oLng_Monitor.getText("Monitor_ScrapDMList"), //"Lista prodotti sospesi",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("ScrapPanelTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnDetScrap").setEnabled(false);
                }
                else {
                    $.UIbyID("btnDetScrap").setEnabled(true);
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Detail"), //"Dettaglio",
                        id: "btnDetScrap",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            const sUDCNR = fnGetCellValue("ScrapPanelTab", "UDCNR");
                            const sIDLINE = fnGetCellValue("ScrapPanelTab", "IDLINE");
                            openScrapDet(sUDCNR, sIDLINE);
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabScrapPanel();
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_From"), //"Da:",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oDatePickerF,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_To"), //"a:",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oDatePickerT,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oCmbShift,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_IDLINE"), //"Linea",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oCmbLine,
                    new sap.ui.commons.Button({
                        icon: "sap-icon://filter",
                        id: "delFilter",
                        enabled: false,
                        press: function () {
                            $.UIbyID("cmbShift").setSelectedKey("");
                            $.UIbyID("cmbLine").setSelectedKey("");
                            refreshTabScrapPanel();
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    resizable : true,
                    flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "IDMACH",
                properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "MACHTXT", 
				label: oLng_Monitor.getText("Monitor_Machine"), //"Macchina",
				properties: {
					width: "75px",
                    visible: false
				}
            },
            {
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    flexible : false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    flexible : false
				}
            },
            {
				Field: "TIME_ID_CALCULATED",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "70px",
                    flexible : false
				},
				template: {
					type: "Time",
					textAlign: "Center"
                }
            },
            {
				Field: "SUBOPER",
                properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "POPER",
                properties: {
					width: "70px",
                    visible: false
				}
            },
            {
                Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"Numero UDC",
				properties: {
					width: "130px"
				}
            },
            {
				Field: "MATERIAL", 
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "130px",
                    flexible : false
				}
            },
            {
				Field: "ORDER", 
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
                    flexible : false
				}
            },
            {
				Field: "QTY", 
				label: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "EXTENDED_TEXT", 
				label: oLng_Monitor.getText("Monitor_Type"), //"Tipo",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "SREASID",
                properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "REASTXT", 
				label: oLng_Monitor.getText("Monitor_MicroScrabReason"), //"Micro-causale di scarto",
				properties: {
					width: "180px",
                    flexible : false
				}
            },
            {
				Field: "OPEID", 
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"PERNR",
				properties: {
					width: "80px",
                    flexible : false
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome operatore",
				properties: {
					width: "130px",
                    flexible : false
				}
            },
            {
				Field: "TIME_ID", 
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "130px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "TIME_MOD", 
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Data di update",
				properties: {
					width: "130px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    /*refreshTabScrapPanel();*/
    return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabScrapPanel() {
    
    var params = "&Param.1=" + dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
    params = params + "&Param.2="  + dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
    params = params + "&Param.3=" + $.UIbyID("cmbShift").getSelectedKey();
    params = params + "&Param.4=" + $.UIbyID("cmbLine").getSelectedKey();
    /*dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()))*/
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("ScrapPanelTab").getModel().loadData(
            QService + dataProdMon +
            "getDScrapsByParamsSQ" + params + "&Param.5=" + $('#plant').val() + "&Param.6=" + sLanguage
        );
    }
    else {
        $.UIbyID("ScrapPanelTab").getModel().loadData(
            QService + dataProdMon +
            "getDScrapsByParamsSQ" + params + "&Param.5=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.6=" + sLanguage
        );
    }
    $.UIbyID("ScrapPanelTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Dettaglio scarti/sospesi
function openScrapDet(sUDCNR, sIDLINE) {
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgScrapdet",
        maxWidth: "1200px",
        maxHeight: "900px",
        //minHeight: "900px",
        title: oLng_Monitor.getText("Monitor_UDCDetail"), //"Dettaglio UDC",
        showCloseButton: false
    });
    
    var oScrapsDetTable = createTabScrapDet(sUDCNR, sIDLINE);
    oLayout0 = new sap.ui.commons.layout.MatrixLayout("tabdetail", {columns: 1});
    oLayout0.createRow(oScrapsDetTable);
    oEdtDlg.addContent(oLayout0);
    
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
        press:function(){
            refreshTabScrapPanel();
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));
	oEdtDlg.open();
}