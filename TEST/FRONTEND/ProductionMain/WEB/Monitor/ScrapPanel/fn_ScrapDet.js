//**************************************************************************************
//Title:  Dettaglio causali di scarto/sospeso
//Author: Bruno Rosati
//Date:   25/07/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Dettaglio causali di scarto/sospeso

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Function che crea la tabella DM Serials e ritorna l'oggetto oTable
function createTabScrapDet(sUDCNR, sIDLINE) {
	
   var iTabVisibleRowCount = 15;
	if(typeof(sUDCNR) === 'undefined' && typeof(sIDLINE) === 'undefined'){
        sUDCNR = '';
        sIDLINE = '';
    }            
    else
        iTabVisibleRowCount = 7;
   
   //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
            id: "ScrapDetTab" + sUDCNR + "_" + sIDLINE,
        properties: {
            title: oLng_Monitor.getText("Monitor_ContentDetails"), //"Dettaglio del contenuto",
            visibleRowCount: iTabVisibleRowCount,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("ScrapDetTab" + sUDCNR + "_" + sIDLINE).getSelectedIndex() == -1){
                    /*$.UIbyID("btnModDM" + sUDCNR + "_" + sIDLINE).setEnabled(false);
                    $.UIbyID("btnDelDM" + sUDCNR + "_" + sIDLINE).setEnabled(false);*/
                }
                else{
                    /*$.UIbyID("btnModDM" + sUDCNR + "_" + sIDLINE).setEnabled(true);
                    $.UIbyID("btnDelDM" + sUDCNR + "_" + sIDLINE).setEnabled(true);*/
                }
            }            
        },
        toolbarItems:[
            /*
            new sap.ui.commons.Button({
                text: oLng_Monitor.getText("Monitor_Add"), //"Aggiungi",
                icon: 'sap-icon://add',
                press: function(){
                    openMachEdit(false,sUDCNR);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Monitor.getText("Monitor_Modify"), //"Modifica",
                id: "btnModDM" + sUDCNR + "_" + sIDLINE,
                icon: 'sap-icon://edit',
                enabled: false,
                press: function(){
                    openMachEdit(true,sUDCNR);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Monitor.getText("Monitor_Delete"), //"Elimina",
                id: "btnDelDM" + sUDCNR + "_" + sIDLINE,
                icon: 'sap-icon://delete',
                enabled: false,
                press: function(){
                    fnDelDMSerial(sUDCNR);
                }
            }),
            */
            new sap.ui.commons.Button({
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function(){
                    refreshTabDMSerials(sUDCNR, sIDLINE);
                }
            })
        ],            
        exportButton: false,
		columns: [
			{
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    resizable : true,
                    flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    flexible : false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    flexible : false
				}
            },
            {
				Field: "TIME_ID",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "70px",
                    flexible : false
				},
				template: {
					type: "Time",
					textAlign: "Center"
                }
            },
            {
                Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"Numero UDC",
				properties: {
					width: "130px"
				}
            },
            {
				Field: "TYPE", 
				label: oLng_Monitor.getText("Monitor_Type"), //"Tipo",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "SREASID",
                properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "REASTXT", 
				label: oLng_Monitor.getText("Monitor_MicroScrabReason"), //"Micro-causale di scarto",
				properties: {
					width: "180px",
                    flexible : false
				}
            },
            {
				Field: "NOTE", 
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "130px",
                    flexible : false
				}
            },
            {
				Field: "OPEID", 
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"PERNR",
				properties: {
					width: "80px",
                    flexible : false
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome operatore",
				properties: {
					width: "130px",
                    flexible : false
				}
            },
            {
				Field: "TIME_ID", 
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "130px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            }
        ]      
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
	
    refreshTabDMSerials(sUDCNR, sIDLINE);
	
	return oTable;
}

// Aggiorna la tabella Macchine
function refreshTabDMSerials(sUDCNR, sIDLINE) {
    var qexe = QService + dataProdMon;   
	if(sUDCNR && sIDLINE){
        qexe += "getDScrapsDetSQ&Param.1=" + sUDCNR + "&Param.2=" + sIDLINE + "&Param.3=" + sLanguage;
        //var qexe2 = QService + dataProdMon + "UDC/getSerialsByUDCnrSQ&Param.1=" + sUDCNR;
        $.UIbyID("ScrapDetTab" + sUDCNR + "_" + sIDLINE).getModel().loadData(qexe);
        //$.UIbyID("ScrapDetTab").getModel().loadData(qexe2);
    }		
	else{
        /*
        qexe += "Machines/getMachinesSQ";
        $.UIbyID("ScrapDetTab" + sUDCNR + "_" + sIDLINE).getModel().loadData(qexe);
        */
    }    
}