/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor UDC
//Author: Bruno Rosati
//Date:   22/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor UDC

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Function che crea la tabella DM Serials e ritorna l'oggetto oTable
function createTabDMSerials(sUDCNR) {
	
   var iTabVisibleRowCount = 15;
	if(typeof(sUDCNR) === 'undefined')
        sUDCNR = '';    
    else
        iTabVisibleRowCount = 7;
   
   //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "DMSerialsTab" + sUDCNR,
        properties: {
            title: oLng_Monitor.getText("Monitor_ContentDetails"), //"Dettaglio del contenuto",
            visibleRowCount: iTabVisibleRowCount,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("DMSerialsTab" + sUDCNR).getSelectedIndex() == -1){
                    //$.UIbyID("btnModDM" + sUDCNR).setEnabled(false);
                    $.UIbyID("btnDelDM" + sUDCNR).setEnabled(false);
                }
                else{
                    //$.UIbyID("btnModDM" + sUDCNR).setEnabled(true);
                    $.UIbyID("btnDelDM" + sUDCNR).setEnabled(true);
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Delete"), //"Elimina",
                        id: "btnDelDM" + sUDCNR,
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function(){
                            fnDelDMSerial(sUDCNR);
                        }
                    })
                ],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabDMSerials(sUDCNR);
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("DMSerialsTab" + sUDCNR,"Produzione");
						}
					})
				]
			})        
        },            
        exportButton: false,
		columns: [
			{
				Field: "OPEID", 
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "65px",
                    visible: false
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome operatore",
				properties: {
					width: "120px",
                    flexible : false
				}
            },
            {
                Field: "UDCPROG",
				label: oLng_Monitor.getText("Monitor_Progressive"), //"Progressivo",
				properties: {
					width: "85px",
                    flexible : false
				}
            },
            {
                Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
                Field: "ORDER",
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "105px",
                    flexible : false
				}
            },
            {
                Field: "QTY",
				label: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
				properties: {
					width: "65px",
                    flexible : false
				}
            },
            {
                Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"Numero UDC",
				properties: {
					width: "120px",
                    flexible : false,
                    visible: false
				}
            },
           /*{
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "90px"
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "60px"
				}
            },*/
            {
				Field: "TIMEID",
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "130px",
                    flexible : false,
                    visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "DATE_UPD",
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Date update",
				properties: {
					width: "130px",
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "SERIAL",
				label: oLng_Monitor.getText("Monitor_FinishedSerial"), //"Seriale Finito",
				properties: {
					width: "135px",
                    flexible : false,
                    visible: false
				}
			},
            {
				Field: "SERIAL2",
				label: oLng_Monitor.getText("Monitor_RoughSerial"), //"Seriale Grezzo",
				properties: {
					width: "135px",
                    flexible : false,
                    visible: false
				}
			},
            {
				Field: "NOTE_DUDCDET",
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "130px",
                    flexible : false,
                    visible: true
				}
            }
        ]      
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
	
    refreshTabDMSerials(sUDCNR);
	
	return oTable;
}

// Aggiorna la tabella Macchine
function refreshTabDMSerials(sUDCNR) {
    var qexe = QService + dataProdMon;   
	if(sUDCNR){
        qexe += "UDC/getSerialsByUDCnrSQ&Param.1=" + sUDCNR;
        //var qexe2 = QService + dataProdMon + "UDC/getSerialsByUDCnrSQ&Param.1=" + sUDCNR;
        $.UIbyID("DMSerialsTab" + sUDCNR).getModel().loadData(qexe);
        //$.UIbyID("DMSerialsTab").getModel().loadData(qexe2);
    }		
	else{
        /*
        qexe += "Machines/getMachinesSQ";
        $.UIbyID("DMSerialsTab" + sUDCNR).getModel().loadData(qexe);
        */
    }    
}

function fnDelDMSerial(sUDCNR){
    var qexe = dataProdMon;
    qexe += "UDC/delDMSerialQR&Param.1=" + sUDCNR;
    qexe += "&Param.2=" + fnGetCellValue("DMSerialsTab" + sUDCNR, "UDCPROG");
    
    var ret = fnExeQuery(qexe, oLng_Monitor.getText("Monitor_DMDeleted"),
                         oLng_Monitor.getText("Monitor_DMDeletedError")
                         /*"Datamatrix rimosso correttamente", "Errore in rimozione Datamatrix"*/,
                         false);
    
    if(ret){
        if($.UIbyID("txtUDCSTAT").getValue() === 'C'){
            $.UIbyID("txtUDCSTAT").setValue('P');
        }
        if($.UIbyID("txtCURRENT_QTY").getValue() > 0){
            $.UIbyID("txtCURRENT_QTY").setValue($.UIbyID("txtCURRENT_QTY").getValue() - 1);
        }
        refreshTabDMSerials(sUDCNR)
    }    
}