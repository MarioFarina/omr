/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor UDC movimentazione
//Author: Bruno Rosati
//Date:   24/08/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor UDC movimentazione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Function che crea la tabella DM Serials e ritorna l'oggetto oTable
function createTabUdCMov(sUDCNR) {
	
   var iTabVisibleRowCount = 15;
	if(typeof(sUDCNR) === 'undefined')
        sUDCNR = '';    
    else
        iTabVisibleRowCount = 7;
   
   //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "UdCMovTab" + sUDCNR,
        properties: {
            title: oLng_Monitor.getText("Monitor_Movements"), //"Movimenti"
            visibleRowCount: iTabVisibleRowCount,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){},
            toolbar: new sap.ui.commons.Toolbar({
				items: [],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabUdCMov(sUDCNR);
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("UdCMovTab" + sUDCNR,"Produzione");
						}
					})
				]
			})            
        },            
        exportButton: false,
		columns: [
			{
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Linea",
				properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    flexible : false,
                    visible: false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "TIMEID",
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "150px",
                    flexible : false,
                    visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "TIMEMOD",
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Date update",
				properties: {
					width: "130px",
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
                Field: "ORDER",
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "105px",
                    flexible : false
				}
            },
            {
				Field: "POPER", 
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
                    //flexible : false
				}
            },
            {
                Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"Numero UDC",
				properties: {
					width: "120px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "EXTENDED_TEXT_STAT", 
				label: oLng_Monitor.getText("Monitor_Direction"), //"Direzione",
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "QTYRES", 
				label: oLng_Monitor.getText("Monitor_MovementQuantity"), //"Q.tà Movimento",
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "110px",
					//resizable : false,
					flexible : false,
                    visible: false
				}
			},
			{
				Field: "MATERIAL_DESC",
				label: oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false,
                    visible: false
				}
			},
			{
				Field: "OPEID",
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "80px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "OPENAME",
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"nome Operatore",
				properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "EXID", 
				label: oLng_Monitor.getText("Monitor_EXID"), //"External ID",
				properties: {
					width: "100px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "NOTE", 
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "140px",
                    flexible : false
				}
            }
        ]      
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
	
    refreshTabUdCMov(sUDCNR);
	
	return oTable;
}

// Aggiorna la tabella di movimentazione
function refreshTabUdCMov(sUDCNR) {
    var qexe = QService + dataProdMon;   
	if(sUDCNR){
        qexe += "UDC/getUdCMovByUDCnrSQ&Param.1=" + sUDCNR + "&Param.2=" + sLanguage;
        $.UIbyID("UdCMovTab" + sUDCNR).getModel().loadData(qexe);
    }		
	else{
        /*
        qexe += "Machines/getMachinesSQ";
        $.UIbyID("UdCMovTab" + sUDCNR).getModel().loadData(qexe);
        */
    }    
}