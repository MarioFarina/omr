//**************************************************************************************
//Title:  Monitor UDC link (UdC collegate ad una selezionata)
//Author: Bruno Rosati
//Date:   01/09/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor UDC link (UdC collegate ad una selezionata)

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Function che crea la tabella UDC link e ritorna l'oggetto oTable
function createTabUDClink(sUDCNR) {
    
    //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "UDClinkTab" + sUDCNR,
        properties: {
            title: oLng_Monitor.getText("Monitor_UDCsConnected"), //"UdC collegate",
            visibleRowCount: 7,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){},
            toolbar: new sap.ui.commons.Toolbar({
				items: [],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabUDClink(sUDCNR);
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("UDClinkTab" + sUDCNR,"Produzione");
						}
					})
				]
			})
        },            
        exportButton: false,
		columns: [
			{
                Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"Numero UDC",
				properties: {
					width: "120px",
                    //flexible : false
				}
            },
            {
				Field: "PLANT", 
				properties: {
					width: "65px",
                    visible: false,
                    //flexible : false
				}
            },
            {
				Field: "NAME1", 
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "80px",
                    visible: false,
                    //flexible : false
				}
            },
            {
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    //flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Linea",
				properties: {
					width: "140px",
                    //flexible : false
				}
            },
            {
				Field: "UDCTYPE", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    visible: false,
                    //flexible : false
				}
            },
            {
				Field: "UDCTYPE_TXT", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
                Field: "UDCIN",
				label: oLng_Monitor.getText("Monitor_UDCIN"), //"UdC d'ingresso",
				properties: {
					width: "120px",
                    //flexible : false
				}
            },
            {
				Field: "UDCTYPEIN", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    visible: false,
                    //flexible : false
				}
            },
            {
				Field: "UDCTYPEIN_TXT", 
				label: oLng_Monitor.getText("Monitor_UDCtypeIN"), //"Tipo UdC d'ingresso",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
				Field: "TIMEID",
				label: oLng_Monitor.getText("Monitor_ConnectDateTime"), //"Data e ora di collegamento",
				properties: {
					width: "200px",
                    //flexible : false,
                    visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            }
        ]      
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
	
    refreshTabUDClink(sUDCNR);
	
	return oTable;
}

// Aggiorna la tabella Macchine
function refreshTabUDClink(sUDCNR) {
    var qexe = QService + dataProdMon;   
	if(sUDCNR){
        qexe += "UDC/getUDClinkByUDCnrSQ&Param.1=" + sUDCNR + "&Param.2=" + sLanguage;
        //var qexe2 = QService + dataProdMon + "UDC/getSerialsByUDCnrSQ&Param.1=" + sUDCNR;
        $.UIbyID("UDClinkTab" + sUDCNR).getModel().loadData(qexe);
        //$.UIbyID("UDClinkTab").getModel().loadData(qexe2);
    }		
	else{
        /*
        qexe += "Machines/getMachinesSQ";
        $.UIbyID("UDClinkTab" + sUDCNR).getModel().loadData(qexe);
        */
    }    
}