var cor = sap.ui.core;
var c = sap.ui.commons;
var fl = sap.ui.layout.form.GridElementData;
var tar = sap.ui.core.TextAlign.Right;
var fc = sap.ui.layout.form.FormContainer;
var fe = sap.ui.layout.form.FormElement;
var twd = c.TextViewDesign;
var twc = sap.ui.commons.TextViewColor;
var dateSelFrom = "";
var dateSelTo = "";
var formatTNAT = "Formato - Tipo Nastro";

function fnShowLineDet(LineID, ShowMode) {
	$("#splash-screen").show();

	// Intestazione Dialog
	var oLayout0 = new c.layout.MatrixLayout("tabdetail", { columns: 1 });
	oLayout0.createRow(CreateTabHead());
	// Crea la TabStrip principale con i dati generali
	var oTabMaster = new c.TabStrip("TabMaster");
	oTabMaster.attachClose(function (oEvent) {
		var oTabStrip = oEvent.oSource;
		oTabStrip.closeTab(oEvent.getParameter("index"));
	});

	// 1. tab: dettaglio linea
	var oDettPr = fldHeadDetLine(LineID);

	var oLayout1 = new c.layout.MatrixLayout("tabDet", { columns: 1 });
	oLayout1.createRow(oDettPr);
	oTabMaster.createTab("Riepilogo WIP", oLayout1);



	//----------------------------------------------------------------------------------------



		// 2. tab: dettaglio lotti per cambio lotto
		var oLotList = createChangeLot(LineID);
		var oLayout2 = new c.layout.MatrixLayout("tabLot", { columns: 1 });
		oLayout2.createRow(oLotList);
		oTabMaster.createTab("Sospesi", oLayout2);



	// 3. tab: Riepilogo  Fermi
	var oTable1 = createStopsReason (LineID);
	var oLayout3 = new c.layout.MatrixLayout("tbStops", { columns: 1 });
	oLayout3.createRow(oTable1);
	oTabMaster.createTab("Riepilogo Fermi", oLayout3);

	// 4. tab: Grafico andamento
	var oChart = drawChartLine(LineID);
	var oLayout4 = new c.layout.MatrixLayout("tabGraph", { columns: 1 });
	oLayout4.createRow(oChart);
	oTabMaster.createTab("Andamento Linea", oLayout4);

	// 5. tab: Storico lavorazioni Linea
	var oTableH = createHistoryTab (LineID);
	var oLayout5 = new c.layout.MatrixLayout("tbHistory", { columns: 1 });
	oLayout5.createRow(oTableH);
	oTabMaster.createTab("Riepilogo produzioni linea", oLayout5);

	oLayout0.createRow(oTabMaster);

	//  Crea la finestra di dialogo
	var oDetDlg = new c.Dialog({
		modal: true,
		resizable: true,
		id: "dlgDetLine",
		title: "Dettaglio dati di Linea: " ,
		minWidth: "900px",
		//		Width: "900px",
		minHeight: "600px",
		showCloseButton: false
	});
	oDetDlg.addContent(oLayout0);


	oDetDlg.addButton(new c.Button({
		text: "Chiudi",
		icon: "sap-icon://decline",
		press: function () {
			oDetDlg.close();
			oDetDlg.destroy();
			//oDetTab.destroy();
			oDettPr.destroy();
		}
	}));

	oDetDlg.open();
	refreshData(LineID, ShowMode);
	$("#splash-screen").hide();
}


function refreshData(LineID, ShowMode){
	$("#splash-screen").show();

	$.UIbyID("ChangeTab").getModel().loadData(QService + dataMonitor + "GetDScapsByLineQR&Param.1=" + LineID , false);
	$.UIbyID("oDetTab").getModel().loadData(QService + dataMonitor + "GetWipByLineQR&Param.1=" + LineID , false);

	var qexe = dataMonitor + "GetLineMonDetQR&Param.1="+$('#plant').val()+"&Param.2=" + LineID;

	var dataExe = fnGetAjaxData(qexe, false);
	var TabRow = $(dataExe).find('Row');
	try {
		$.UIbyID("txtLine").setText( TabRow.children("LINETXT").text());
		$.UIbyID("txtRepDesc").setText( TabRow.children("REPTXT").text());

	}
	catch(err) {
		if (window.console) console.log("Errore: " + err);
	}
	//$.UIbyID("txtLine").setText( TabRow.children("LINETXT").text());


	$(".sapUiRespGridMedia-Std-Phone.sapUiRespGridHSpace0 > .sapUiRespGridSpanS12").css("width", "100%");
	$("#tabdetail").css("width", "100%");
	$("#FC1---Panel").parent().css("width", "33.3%");
	$("#FC2---Panel").parent().css("width", "33.3%");
	$("#FC3---Panel").parent().css("width", "33.3%");

	$("#splash-screen").hide();
}

function CreateTabHead() {


	var oLayoutT = new sap.ui.layout.form.GridLayout();
	var oFormT = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayoutT,
		formContainers: [
			new fc({
				formElements: [
					new fe({
						label: new c.Label({ text: "Linea:", layoutData: new fl({ hCells: "2" }) }),
						fields: [new c.TextView({ layoutData: new fl({ hCells: "auto" }),  design: twd.H5, id: "txtLine" })
										]
					}),
					new fe({
						label: new c.Label({ text: "Reparto:", layoutData: new fl({ hCells: "2" })
															 }),
						fields: [new c.TextView({ layoutData: new fl({ hCells: "auto" }), design: twd.H5, id: "txtRepDesc"
																		})
										]
					})
				]
			})
		]
	});
	return oFormT;
}

function dlgHeadDetLine(LineID) {
	var oLayout1 = new sap.ui.layout.form.ResponsiveGridLayout("L2", {
		labelSpanL: 2,
		labelSpanM: 1,
		labelSpanS: 2,
		emptySpanL: 2,
		emptySpanM: 1,
		emptySpanS: 1,
		columnsL: 3,
		columnsM: 3,
		breakpointL: 300,
		breakpointM: 300
	});
	var oForm1 = new sap.ui.layout.form.Form({
		/*title: new cor.Title({
			text: "Dettaglio dati linea",
			icon: "/images/address.gif",
			tooltip: "Dettaglio dati di linea"
		}),*/
		width: "98%",
		layout: oLayout1,
		formContainers: fldHeadDetLine()
	});
	return oForm1;
}

function fldHeadDetLine() {
	var oDetTab = UI5Utils.init_UI5_Table({
		id: "oDetTab",
		exportButton: false,
		columns: [
			{ Field: "SUBOPER", label: "Operazione", sumCol: false, properties: { width: "100px" }},
			{ Field: "MATERIAL", label: "Materiale", sumCol: false, properties: { width: "100px" }},
			{ Field: "DESC", label: "Desc. Materiale", sumCol: false, properties: { width: "120px" }},
			{ Field: "SUM_QTY", label: "Quantità", sumCol: false, properties: { width: "60px" }},
			{ Field: "MAX_DATE_TO", label: "Ult.Agg.", sumCol: false, properties: { width: "90px" }},
		],
		properties: {
			//fixedColumnCount: nFixedCols,
			width: "98%",
			height:"600px",
			selectionMode:sap.ui.table.SelectionMode.None,
			visibleRowCount: 15,
			firstVisibleRow: 0,
		}
	});
	oModel = new sap.ui.model.xml.XMLModel();
	oDetTab.setModel(oModel);
	oDetTab.bindRows("/Rowset/Row");
	return oDetTab;
}

function createChangeLot(LineID) {
	//Crea L'oggetto Tabella lotti
	var oTable = new sap.ui.table.Table({
		title: "Dettaglio sospesi",
		id: "ChangeTab",
		visibleRowCount: 9,
		width: "98%",
		firstVisibleRow: 1,
		fixedColumnCount: 2,
		selectionMode: sap.ui.table.SelectionMode.Single,
		navigationMode: sap.ui.table.NavigationMode.Scrollbar,
		visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
		rowSelectionChange: function (oControlEvent) {
		}
	});
	addColumnEx(oTable, "Materiale", "MATERIAL", "150px");
	addColumnEx(oTable, "Descr.", "DESC", "300px");
	addColumnEx(oTable, "Datamatrix", "SERIAL", "110px");
	addColumnEx(oTable, "Data/ora", "TIME_MOD", "70px", "DateTime");
	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");

	return	oTable;
}

function createStopsReason (LineID) {

	var yesterday = new Date();
	yesterday.setDate( yesterday.getDate() - 1 );
	var today = new Date();
	today.setDate( today.getDate() );

	var  year = yesterday.getUTCFullYear();
	var  month = yesterday.getUTCMonth() + 1;
	if( month.toString().length == 1 ) month = "0" + month;
	var  day = yesterday.getUTCDate();
	if( day.toString().length == 1 ) day = "0" + day;

	dateSelFrom = year + "-" + month+ "-" + day;

	year = today.getUTCFullYear();
	month = today.getUTCMonth() + 1;
	if( month.toString().length == 1 ) month = "0" + month;
	day = today.getUTCDate();
	if( day.toString().length == 1 ) day = "0" + day;
	dateSelTo = year + "-" + month+ "-" + day;

	odtFrom = new sap.ui.commons.DatePicker('dtFrom');
	odtFrom.setYyyymmdd( dateSelFrom );
	odtFrom.setLocale("it"); // Try with "de" or "fr" instead!

	odtFrom.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(cor.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(cor.ValueState.None);
			}
			var selDate = this.getYyyymmdd();
			dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
			fnRefreshStopTab("stopsTab", LineID);
		}
	);
	odtTo = new sap.ui.commons.DatePicker('dtTo');
	odtTo.setYyyymmdd( dateSelTo );
	odtTo.setLocale("it"); // Try with "de" or "fr" instead!

	odtTo.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(cor.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(cor.ValueState.None);
			}
			var selDate = this.getYyyymmdd();
			dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
			fnRefreshStopTab("stopsTab", LineID);
		}
	);



	//Crea L'oggetto Tabella Fermo
	var oTable = new sap.ui.table.Table({
		title: "Dettaglio fermi",
		id: "stopsTab",
		visibleRowCount: 9,
		width: "98%",
		firstVisibleRow: 0,
		fixedColumnCount: 2,
		selectionMode: sap.ui.table.SelectionMode.Single,
		navigationMode: sap.ui.table.NavigationMode.Scrollbar,
		visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
		rowSelectionChange: function (oControlEvent) {
			if ($.UIbyID("stopsTab").getSelectedIndex() == -1) {
				$.UIbyID("btnStops").setEnabled(false);
			} else {
				$.UIbyID("btnStops").setEnabled(true);
			}
		},
		toolbar: new c.Toolbar({
			items: [
				new c.Button({
					text: "Causale fermo",
					icon: "sap-icon://wrench",
					id: "btnStops",
					style: c.ButtonStyle.Accept,
					enabled: false,
					press: function () {
						fnChangeReason(LineID);
					}
				}),
				new sap.ui.commons.Label("lFrom",{
					text : "da data:",
					tooltip : "Seleziona l'intervallo di date"
				}),odtFrom,
				new sap.ui.commons.Label("lTo",{
					text : "a data:",
					tooltip : "Seleziona l'intervallo di date"
				}),odtTo,
				new c.Button({
					icon: "sap-icon://refresh",
					tooltip : "Aggiorna lista fermi",
					style: c.ButtonStyle.Emph,
					enabled: true,
					press: function () {
						fnRefreshStopTab("stopsTab", LineID);
					}
				}),
				new c.Button({
					//text: "Causale fermo",
					icon: "/XMII/CM/Common/Images/charts-icon.png",
					tooltip : "Visualizza grafico fermi",
					style: c.ButtonStyle.Default,
					enabled: true,
					press: function () {

					}
				})]
		})
	});
	addColumnEx(oTable, "Inizio fermo", "DATE_FROM", "130px", "DateTime");
	addColumnEx(oTable, "Fine fermo", "DATE_TO", "130px", "DateTime");
	addColumnEx(oTable, "Durata (Min)", "DURATION", "60px");
	addColumnEx(oTable, "Causale", "REASTXT", "180px");
	//addColumnEx(oTable, "Attivo", "LASTEVT", "60px", "checked");
	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	//oModel.loadData(QService + dataMonitor + "GetStopsListByLine&Param.1=" + LineID );
	fnRefreshStopTab ("stopsTab", LineID);
	return	oTable;
}

function fnRefreshStopTab (Table, LineID){
	var sQExe = QService + dataMonitor + "GetStopByLineQR&Param.1=" + LineID;
	sQExe += "&Param.2=" + dateSelFrom;
	sQExe += "&Param.3=" + dateSelTo;
	$.UIbyID(Table).getModel().loadData(sQExe );
}

function fnChangeReason (LineID){
	var oModel = new sap.ui.model.xml.XMLModel();
	oModel.loadData(QService + dataMonitor + "getAllReasonsByLine&Param.1=" + LineID);
	// + "&Param.2=" + fnGetCellValue("stopsTab", "REASON"));
	var oReasonList = new sap.ui.commons.ComboBox(
		{
			id: "lstReason",
			width: "200px",
		});
	oReasonList.setModel(oModel);
	var oItemBacth = new cor.ListItem();
	oItemBacth.bindProperty("text", "REASTXT");
	oItemBacth.bindProperty("key", "STOPID");
	oReasonList.bindItems("/Rowset/Row", oItemBacth);
	oReasonList.setSelectedKey(fnGetCellValue("stopsTab", "REASON"));

	//oLayout0.createRow(oReasonList);
	//  Crea la finestra di dialogo
	var oDetDlg = new sap.ui.commons.Dialog(
		{
			modal: true,
			resizable: true,
			id: "dlgReason",
			title: "Seleziona Motivo di fermo " , //+ fnGetCellValue("stopsTab", "REASON"),
			maxWidth: "400px",
			Width: "400px",
			maxHeight: "300px",
			showCloseButton: false
		});
	oDetDlg.addContent(oReasonList);
	//oDetDlg.addButton(new sap.ui.commons.Button({text: "Salva e nuovo", press:function(){fnSaveConf(true);}}));
	oDetDlg.addButton(new sap.ui.commons.Button(
		{
			text: "Seleziona",
			press: function ()
			{
				fnSetReason(LineID);
			}
		}));
	oDetDlg.addButton(new sap.ui.commons.Button(
		{
			text: "Chiudi",
			press: function ()
			{
				oDetDlg.close();
				oDetDlg.destroy();
			}
		}));
	oDetDlg.open();
}

// Aggiorna la tabella movimenti
function fnSetReason(LineID)
{
	if ($.UIbyID("lstReason").getSelectedKey() === "")
	{
		sap.ui.commons.MessageBox.show("Selezionare motivo di fermo!", sap.ui.commons.MessageBox.Icon.ERROR);
		return;
	}
	//fnSetCellValue("stopsTab", "REASON",$.UIbyID("lstReason").getSelectedKey);
	//fnSetCellValue("stopsTab", "REASTXT", $.UIbyID("lstReason").getSelectedText());
	var qexe = dataMonitor+"updStopReasons&Param.1=" + LineID;
	qexe += "&Param.2=" + fnGetCellValue("stopsTab", "DATE_FROM");
	qexe += "&Param.3=" + $.UIbyID("lstReason").getSelectedKey();

	var ret = fnExeQuery(qexe, "","", false);

	$.UIbyID("dlgReason").close();
	$.UIbyID("dlgReason").destroy();

	fnRefreshStopTab ("stopsTab", LineID);

}

function createHistoryTab(LineID) {

	var dateToday = new Date();
	var day = 60 * 60 * 24 * 1000;
	var firstDate = new Date(dateToday.getTime() - (2 * day));
	var secondDate = new Date();

	// data iniziale per comboBox $.UIbyID("dateFrom")
	var mm = firstDate.getMonth() >= 9 ? (firstDate.getMonth() + 1).toString() : "0" + (firstDate.getMonth() + 1).toString();
	var dd = firstDate.getDate().toString() >= 10 ? firstDate.getDate().toString() : "0" + firstDate.getDate().toString();
	var dateFromYyyymmdd = firstDate.getFullYear().toString() + mm + dd;
	var date1 = firstDate.getFullYear().toString() + "-" + mm + "-" + dd;

	// data iniziale per comboBox $.UIbyID("dateTo")
	mm = secondDate.getMonth() >= 9 ? (secondDate.getMonth() + 1).toString() : "0" + (secondDate.getMonth() + 1).toString();
	dd = secondDate.getDate().toString() >= 10 ? secondDate.getDate().toString() : "0" + secondDate.getDate().toString();
	var dateToYyyymmdd = secondDate.getFullYear().toString() + mm + dd;
	var date2 = secondDate.getFullYear().toString() + "-" + mm + "-" + dd;

	var oTab = UI5Utils.init_UI5_Table({
		id: "TabHistory",
		exportButton: false,
		columns: [
			{ Field: "DATE_SHIFT", label: "Data", properties: { width: "50px" },template:{ type: "Date", textAlign: "Begin"}},
			{ Field: "SHIFT", label: "Turno", sumCol: false, properties: { width: "40px" }},
			{ Field: "SHNAME", label: "Desc. Turno", sumCol: false, properties: { width: "90px" }},
			/*{ Field: "LOTID", label: "Lotto",  properties: { width: "50px" },template:{ type: "Number"}},
			{ Field: "MATID", label: "Materiale", tooltipCol: "MAKTX", properties: { width: "50px" },template:{ type: "Number"}},
			{ Field: "MAKTX", label: "Descrizione", sumCol: false, properties: { width: "135px" }},*/
			{ Field: "QTY_IN", label: "Pz Ingresso", properties: { width: "50px" },template:{ type: "Numeric", textAlign: "Right"}},
			{ Field: "QTY_OUT", label: "Pz Uscita", properties: { width: "50px" },template:{ type: "Numeric", textAlign: "Right"}},
			{ Field: "QTY_SCRAP", label: "Pz Sospesi", properties: { width: "50px" },template:{ type: "Numeric", textAlign: "Right"}}
		],
		properties: {
			//fixedColumnCount: nFixedCols,
			width: "98%",
			height:"600px",
			selectionMode:sap.ui.table.SelectionMode.None,
			visibleRowCount: 15,
			firstVisibleRow: 0,


			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Label({
						text: "Da Data:"
					}),
					new sap.ui.commons.DatePicker("dateFrom")
					.setYyyymmdd(dateFromYyyymmdd)
					.attachChange(function(oEvent){
						date1 = $.UIbyID("dateFrom").getYyyymmdd()==="" ? dateFromYyyymmdd : $.UIbyID("dateFrom").getYyyymmdd().substr(0,4) + '-' + $.UIbyID("dateFrom").getYyyymmdd().substr(4,2) + '-' + $.UIbyID("dateFrom").getYyyymmdd().substr(6,2);
						date2 = $.UIbyID("dateTo").getYyyymmdd()==="" ? dateToYyyymmdd : $.UIbyID("dateTo").getYyyymmdd().substr(0,4) + '-' + $.UIbyID("dateTo").getYyyymmdd().substr(4,2) + '-' + $.UIbyID("dateTo").getYyyymmdd().substr(6,2);
						fnHistoryTab("TabHistory", LineID, date1, date2);
					}),
					new sap.ui.commons.Label({
						text: "A Data:"
					}),
					new sap.ui.commons.DatePicker("dateTo")
					.setYyyymmdd(dateToYyyymmdd)
					.attachChange(function(oEvent){
						date1 = $.UIbyID("dateFrom").getYyyymmdd()==="" ? dateFromYyyymmdd : $.UIbyID("dateFrom").getYyyymmdd().substr(0,4) + '-' + $.UIbyID("dateFrom").getYyyymmdd().substr(4,2) + '-' + $.UIbyID("dateFrom").getYyyymmdd().substr(6,2);
						date2 = $.UIbyID("dateTo").getYyyymmdd()==="" ? dateToYyyymmdd : $.UIbyID("dateTo").getYyyymmdd().substr(0,4) + '-' + $.UIbyID("dateTo").getYyyymmdd().substr(4,2) + '-' + $.UIbyID("dateTo").getYyyymmdd().substr(6,2);
						fnHistoryTab("TabHistory", LineID, date1, date2);
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						text: "Aggiorna",
						press: function () {
							fnHistoryTab("TabHistory", LineID, date1, date2);
						}
					}),
				],
				rightItems: [
					new sap.ui.commons.Button(
						{
							text: "Esporta",
							icon: "sap-icon://action",
							enabled: true,
							press: function ()
							{
								exportTableToCSV("TabHistory", "DataTable");
							}
						})
				]
			})
			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------


		}
	});
	fnHistoryTab("TabHistory", LineID, date1, date2);
	return oTab;
}

function fnHistoryTab (Table, LineID, date1, date2){
	var sQExe = QService + dataMonitor + "HistoryLineWork&Param.1=" + LineID + '&Param.2=' + date1 + '&Param.3=' + date2;
	$.UIbyID(Table).getModel().loadData(sQExe );
}


function drawChartLine(LineID) {
	// some business data
	var oModel = new sap.ui.model.xml.XMLModel();

	oModel.loadData(QService + dataMonitor + "GetTotLinexHour&Param.1=" + LineID );
	// A Dataset defines how the model data is mapped to the chart
	var oDataset = new sap.viz.ui5.data.FlattenedDataset({

		// a Bar Chart requires exactly one dimension (x-axis)
		dimensions : [
			{
				axis : 1, // must be one for the x-axis, 2 for y-axis
				name : 'Ora',
				value : "{TH}"
			}
		],

		// it can show multiple measures, each results in a new set of bars in a new color
		measures : [
			// measure 1
			{
				name : 'Uscita',
				value : '{QTY_OUT}'
			} ,
			{
				name : 'Ingresso', // 'name' is used as label in the Legend
				value : '{QTY_IN}' // 'value' defines the binding for the displayed value
			}	,
			{
				name : 'Sospesi', // 'name' is used as label in the Legend
				value : '{QTY_SCRAP}' // 'value' defines the binding for the displayed value
			}
		],

		// 'data' is used to bind the whole data collection that is to be displayed in the chart
		data : {
			path : "/Rowset/Row"
		}

	});

	oBarChart = new sap.viz.ui5.Line({
		width : "100%",
		height : "400px",
		plotArea : {
			//'colorPalette' : d3.scale.category20().range()
		},
		title : {
			visible : true,
			text : 'Andamento linea nel giorno'
		}
	});

	// attach the model to the chart and display it
	oBarChart.setModel(oModel);
	oBarChart.setDataset(oDataset);

	return oBarChart;
}

function fnComboBoxQuery( oParams )
{
	if (typeof(oParams) == "undefined") oParams = {};
	if ( oParams.default === undefined ) oParams.default = "";
	if ( oParams.edit === undefined ) oParams.edit = true;

	if(oParams.query.charAt(oParams.query.length - 1) == '='){
		oParams.query = oParams.query + "FLO";
	}

	// contenitore dati lista
	var oModel = new sap.ui.model.xml.XMLModel({iSizeLimit:300});
	console.log(oParams.query);
	oModel.loadData("/XMII/Illuminator", "Content-Type=text/xml&QueryTemplate=" + oParams.query );

	oModel.setSizeLimit(1000);
	if (oParams.id === undefined) {
		var oComboBox = new sap.ui.commons.ComboBox();
	}
	else {
		var oComboBox = new sap.ui.commons.ComboBox(oParams.id,
																								{liveChange:function (oEvent) {
																									oEvent.getSource().setTooltip(oEvent.getSource().getSelectedKey());}});
	}

	if ( oParams.cbFunction !== undefined ) oComboBox.attachChange(oParams.cbFunction);

	oComboBox.setEditable(oParams.edit);

	oComboBox.setValue(oParams.default);
	oComboBox.setWidth("200px");
	oComboBox.setModel(oModel);
	var oItem = new cor.ListItem();
	oItem.bindProperty("text", oParams.text);
	oItem.bindProperty("key", oParams.key);
	oComboBox.bindItems("/Rowset/Row", oItem);
	if( oParams.default !== "" ) {
		oComboBox.setSelectedKey(oParams.default);
	}
	return oComboBox;


}  // function fnComboBoxQuery

