/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300 , -W098*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Dichiarazione di fine turno
//Author: Bruno Rosati
//Date:   15/10/2018
//Vers:   1.1.56
//Modifiers: Luca Adanti 18/12/2018
//**************************************************************************************
// Script per pagina Fine Turno Produzione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();
var IdxCdl = 0; //Serve per la selezione del CDL
var PathCdl = "";

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var tmFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "HH:mm"
});

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
	cache: false
});

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabEndShiftDeclaration() {

	var bSwitchType = true;
	if ($('#ordType').val() === "R" || $('#ordType').val() === "D") {
		// Nascondo il radio button per scelta layout
		bSwitchType = false;
		$('#ordTypeStatus').val("");
	} else {
		// Mostro il radio button per scelta layout
		bSwitchType = true;
		$('#ordTypeStatus').val("SHOW");
	}

	if ($('#ordTypeStatus').val() === "SHOW") {
		bSwitchType = true;
	}

	var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom', {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "1"
		}),
		width: "105px",
		change: function (oEvent) {
			//$.UIbyID("ColDateShift").getTemplate().setMinDate(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
			//refreshTabEndDeclaration();
		}
	});
	oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerF.setLocale("it");

	var oDatePickerT = new sap.ui.commons.DatePicker('dtTo', {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "1"
		}),
		width: "105px",
		change: function (oEvent) {
			//refreshTabEndDeclaration();
		}
	});
	oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerT.setLocale("it");

	//filtro Turno
	var oCmbShiftFilter = new sap.ui.commons.ComboBox("filtShift", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabEndDeclaration();
		}
	});
	var oItemShift = new sap.ui.core.ListItem();
	oItemShift.bindProperty("key", "SHIFT");
	oItemShift.bindProperty("text", "SHNAME");
	oCmbShiftFilter.bindItems("/Rowset/Row", oItemShift);
	var oShiftFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());
	} else {
		oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
	}
	oCmbShiftFilter.setModel(oShiftFilter);

	var jState = {
		root: []
	};
	jState.root.push({
		key: 'NP',
		text: "Da Elaborare" //oLng_Monitor.getText("Monitor_Error") //"Errore",
	});
	jState.root.push({
		key: 'EL',
		text: "Da confermare" //oLng_Monitor.getText("Monitor_Error") //"Errore",
	});
	jState.root.push({
		key: 'ST',
		text: oLng_Monitor.getText("Monitor_Averted") //"Stornato",
	});
	/*jState.root.push({
		key: 'CONF_N',
		text: "Da confermare" //oLng_Monitor.getText("Monitor_NotAverted") //"Non stornato",
	});*/
	jState.root.push({
		key: 'CONF',
		text: oLng_Monitor.getText("Monitor_Confirmed")
	});
	jState.root.push({
		key: 'ERR',
		text: oLng_Monitor.getText("Monitor_Error") //"Errore",
	});




	var oStateModel = new sap.ui.model.json.JSONModel();
	oStateModel.setData(jState);

	// Crea la ComboBox per selezionare il tipo delle funzioni da visualizzare
	var oCmbStateFilter = new sap.ui.commons.ComboBox({
		id: "CmbStateFilter",
		change: function () {
			$.UIbyID("delFilter").setEnabled(true);

			if ($.UIbyID("CmbStateFilter").getSelectedKey() === "EL") {
				$.UIbyID("Send-To-Sap").setEnabled(true);
			}
			else {
				$.UIbyID("Send-To-Sap").setEnabled(false);
			}
			refreshTabEndDeclaration();
		}
	});

	oCmbStateFilter.setModel(oStateModel);
	var oItemStateFilter = new sap.ui.core.ListItem();
	oItemStateFilter.bindProperty("text", "text");
	oItemStateFilter.bindProperty("key", "key");
	oCmbStateFilter.bindItems("/root", oItemStateFilter); //valido per JSONModel

	oCmbStateFilter.setSelectedKey("EL");
	//filtro Linea
	var oCmbLineFilter = new sap.ui.commons.ComboBox("filtLine", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabEndDeclaration();
		}
	});
	var oItemLine = new sap.ui.core.ListItem();
	oItemLine.bindProperty("key", "IDLINE");
	oItemLine.bindProperty("text", "LINETXT");
	oCmbLineFilter.bindItems("/Rowset/Row", oItemLine);
	var oLineFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
	} else {
		oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1" + $.UIbyID("filtPlant").getSelectedKey());
	}
	oCmbLineFilter.setModel(oLineFilter);

	var jQtyUDC = {
		root: []
	};
	jQtyUDC.root.push({
		key: 'EqualZero',
		text: oLng_Monitor.getText("Monitor_QtyUDCeqalZero") //"Q.tà UdC uguale a zero"
	});
	jQtyUDC.root.push({
		key: 'BigThanZero',
		text: oLng_Monitor.getText("Monitor_QtyUDCbigThanZero") //"Q.tà UdC maggiore di zero"
	});

	var oQtyUDCmodel = new sap.ui.model.json.JSONModel();
	oQtyUDCmodel.setData(jQtyUDC);
	console.log(jQtyUDC);

	// Crea la ComboBox per selezionare la quantità delle UdC
	var oCmbQtyUDCfilter = new sap.ui.commons.ComboBox({
		id: "CmbQtyUDCfilter",
		change: function () {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabEndDeclaration();
		}
	});

	// Crea la ComboBox per selezionare la quantità delle UdC
	var oCmbHierCdl = new sap.ui.commons.ComboBox({
		id: "CmbHierCdlFilter",
		change: function () {

		}
	});

	oCmbQtyUDCfilter.setModel(oQtyUDCmodel);
	var oItemQtyUDCfilter = new sap.ui.core.ListItem();
	oItemQtyUDCfilter.bindProperty("text", "text");
	oItemQtyUDCfilter.bindProperty("key", "key");
	oCmbQtyUDCfilter.bindItems("/root", oItemQtyUDCfilter); //valido per JSONModel

	//Crea L'oggetto Tabella Operatore
	var oTable = new sap.ui.table.Table("EndShiftDeclarationTab", {
		visibleRowCount: 15,
		fixedColumnCount: 10,
		width: "100%",
		firstVisibleRow: 0,
		selectionMode: sap.ui.table.SelectionMode.Single,
		navigationMode: sap.ui.table.NavigationMode.Scrollbar,
		visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
		enableColumnReordering: false,
		columnResize: function (evt) {
			colorGrid();
		},
		columnMove: function (evt) {
			colorGrid();
		},
		columnVisibility: function (evt) {
			colorGrid();
		},
		rowSelectionChange: function (oControlEvent) {
			if ($.UIbyID("EndShiftDeclarationTab").getSelectedIndex() == -1) {
				$.UIbyID("btnDelRowEndShiftDec").setEnabled(false);
				$.UIbyID("btnCopyRowEndShiftDec").setEnabled(false);
				$.UIbyID("Result-From-Sap").setEnabled(false);

			} else {
				$.UIbyID("btnDelRowEndShiftDec").setEnabled(true);
				$.UIbyID("btnCopyRowEndShiftDec").setEnabled(true);

				if (fnGetCellValue("EndShiftDeclarationTab", "CONFIRMED") === "1" || fnGetCellValue("EndShiftDeclarationTab", "STATE") === "ERR") {
					$.UIbyID("Result-From-Sap").setEnabled(true);
				} else {
					$.UIbyID("Result-From-Sap").setEnabled(false);
				}
				if (fnGetCellValue("EndShiftDeclarationTab", "ROWLOCKED") === "1" || fnGetCellValue("EndShiftDeclarationTab", "STATE") === "ERR") {
					$.UIbyID("btnDelRowEndShiftDec").setEnabled(false);
				} else {
					$.UIbyID("btnDelRowEndShiftDec").setEnabled(true);
				}
			}
		}
	});

	oTable.setToolbar(new sap.ui.commons.Toolbar({
		items: [
						new sap.ui.commons.Button({
				text: oLng_Monitor.getText("Monitor_AddRow"),
				id: "btnAddEndShiftDec",
				icon: 'sap-icon://add',
				enabled: true,
				press: function () {
					fnOpenDeclaration(false);
				}
			}),
						new sap.ui.commons.Button({
				text: oLng_Monitor.getText("Monitor_DeleteRow"),
				id: "btnDelRowEndShiftDec",
				icon: 'sap-icon://delete',
				enabled: false,
				press: function () {
					fnQuestionDelRow();
				}
			}),
				new sap.ui.commons.Button({
				text: oLng_Monitor.getText("Monitor_CopyRow"),
				id: "btnCopyRowEndShiftDec",
				icon: 'sap-icon://edit',
				enabled: false,
				press: function () {
					fnQuestionDupplicateRow();
				}
			}),
			new sap.ui.commons.Button({
				text: "Controlla",
				id: "Check-Data",
				icon: 'sap-icon://approvals',
				enabled: false,
				visible: false,
				press: function () {}
			}), //invia a Sap
			new sap.ui.commons.Button({
				text: "Invia a Sap",
				enabled: false,
				id: "Send-To-Sap",
				icon: 'sap-icon://sap-box',
				press: function () {
					send_to_sap();
				}
			}), //Risultati a Sap
			new sap.ui.commons.Button({
				text: "Storni",
				id: "Result-From-Sap",
				icon: 'sap-icon://clinical-tast-tracker',
				enabled: false,
				press: function () {
					openResultDetail();
				}
			})
				],
		rightItems: [
						new sap.ui.commons.Button({
				icon: "sap-icon://refresh",
				press: function () {
					refreshTabEndDeclaration();
				}
			}),
						new sap.ui.commons.Button({
				icon: "sap-icon://excel-attachment",
				press: function () {
					exportTableToCSV("EndShiftDeclarationTab", "Produzione");
				}
			})
				]
	}));

	oTable.addExtension(new sap.ui.commons.layout.VerticalLayout({
		content: [
						new sap.ui.commons.Toolbar({
				items: [
						// Selezione Data Turno da
						new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_DateShiftFrom"), //"Data Turno",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
						oDatePickerF,
					// Selezione Data Turno a
					new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_DateShiftTo"), //"Data Turno",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
					oDatePickerT,
					// Selezione Turno
					new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_Shift") //Turno
					}),
					oCmbShiftFilter,
					// Filtro stato
					new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_ICON") //Stato
					}),
					oCmbStateFilter,
					//Filtro Selezione Fonderia / Seconde Lavorazioni
					new sap.ui.commons.Label({
						text: "Tipo selez.:",
						visible: bSwitchType,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "1"
						})
					}),
					new sap.m.RadioButtonGroup("rdOrdType", {
						columns: 2,
						visible: bSwitchType,
						buttons: [
					new sap.m.RadioButton("rdDisc", {
								text: "Fonderia",
								width: "65px",
								/*selected: true,*/
								select: function (ce) {
									//alert(ce.getParameters().selected);
									switchLayout("D");
								}

							}),
					new sap.m.RadioButton("rdRipet", {
								text: "Sec.Lavoraz.",
								width: "100px",
								select: function (ce) {
									//alert(ce.getParameters().selected);
									switchLayout("R");
								}
							})]
					})
								]
			}),
						new sap.ui.commons.Toolbar({
				items: [
					//Filtro Gerarchia CDL
					new sap.ui.commons.Label({
						text: "Ger. CDL", //"",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "1"
						})
					}),
					oCmbHierCdl,
					// Filtro per CDL
					new sap.ui.commons.Label({
						text: "CDL", //oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
					new sap.ui.commons.TextField("txtCDLFiter", {
						value: "",
						editable: false,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
					new sap.ui.commons.Button("btnCDLFilter", {
						icon: "sap-icon://arrow-down",
						tooltip: "Seleziona CDL", //oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
						enabled: true,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "1"
						}),
						press: function () {
							var oParams = {
								Selected: $.UIbyID("txtCDLFiter").getValue(),
								OrdType: $('#ordType').val(),
								Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
								IDx: 0
							};
							fnGetCDLTableDialog(oParams, setSelectedCDLFilter);
						}
					}),
										new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
										new sap.ui.commons.TextField("txtOpeFiter", {
						value: "",
						editable: false,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
										new sap.ui.commons.Button("btnOpeFilter", {
						icon: "sap-icon://arrow-down",
						tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
						enabled: true,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "1"
						}),
						press: function () {
							fnGetOperatorTableDialog(
								($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
								"",
								setSelectedOperator
							);
						}
					}),
					//Filtro Commessa
					new sap.ui.commons.Label({
						text: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}),
					new sap.ui.commons.TextField("txtOrderFiter", {
						value: "",
						editable: true,
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "2"
						})
					}).attachBrowserEvent('keypress', function (e) {
						if (e.which == 13) {
							//refreshTabEndDeclaration();
						}
					})
								],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://filter",
						enabled: true,
						tooltip: oLng_Monitor.getText("Monitor_RemoveFilters"), //"Rimuovi filtri"
						id: "delFilter",
						press: function () {
							$.UIbyID("CmbStateFilter").setSelectedKey("");
							$.UIbyID("filtShift").setSelectedKey("");
							//$.UIbyID("filtLine").setSelectedKey("");
							$.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
							$.UIbyID("txtOrderFiter").setValue("");
							$.UIbyID("txtCDLFiter").setValue("");
							//$.UIbyID("delFilter").setEnabled(false);

							//set group of table and column to false
							$.UIbyID("EndShiftDeclarationTab").setEnableGrouping(false);
							$.UIbyID("EndShiftDeclarationTab").getColumns()[0].setGrouped(false);
							var oListBinding = $.UIbyID("EndShiftDeclarationTab").getBinding();
							oListBinding.aSorters = null;
							oListBinding.aFilters = null;
							$.UIbyID("EndShiftDeclarationTab").getModel().refresh(true);
							//after reset, set the enableGrouping back to true
							$.UIbyID("EndShiftDeclarationTab").setEnableGrouping(true);
							$.UIbyID("Send-To-Sap").setEnabled(false);
							$.UIbyID("EndShiftDeclarationTab").getColumns().forEach(function (item) {
								item.setSorted(false);
								item.setFiltered(false);
							});
						}
					}),
										new sap.ui.commons.Label({
						id: "lblRecordNumber",
						text: oLng_Monitor.getText("Monitor_RecordNumber"), //"N. record"
						textAlign: "Right"
					})
				]
			})
				]
	}));

	/* -------  Creazione colonne e template colonna ----------*/

	// Colonna Icona/Stato/Semaforo
	var oControl = new sap.ui.core.Icon({
			width: "22px",
			height: "20px",
			useIconTooltip: true,
			tooltipCol: "{statusToolTip}",
			iconColor: "{statusColor}",
			hAlign: sap.ui.core.HorizontalAlign.Left,
		}).bindProperty("src", "statusIcon")
		.setTooltip("{statusToolTip}")
		.setColor("{statusColor}");
	//oControl.addStyleClass("FTColVis");

	var oColumn = new sap.ui.table.Column({
		label: new sap.ui.core.Icon({
			/*text: oLng_Monitor.getText("Monitor_ICON"), //"Stato"*/
			src: "sap-icon://checklist-item",
			tooltip: oLng_Monitor.getText("Monitor_ICON")
		}),
		template: oControl,
		hAlign: sap.ui.core.HorizontalAlign.Left,
		width: "45px",
		height: "30px",
		flexible: false
	});
	oTable.addColumn(oColumn);

	// Colonna Data turno
	oControl = new sap.m.DatePicker({
			displayFormat: "dd/MM",
			valueFormat: "yyyy-MM-ddTHH:mm:ss",
			//minDate: dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()),
			maxDate: new Date(),
			value: {
				path: "DATE_SHIFT",
				constraints: {
					displayFormat: 'yyyy/dd/MM'
				},
				formatOptions: {
					pattern: "yyyy/dd/MM"
				},
				valueFormat: "yyyy-MM-ddTHH:mm:ss"
			},
			change: function (oEvent) {

				$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(oEvent.getSource().oParent.getIndex());
				var newDateShift = oEvent.getParameters().newValue.substring(0, 10);
				var oldDateShift = oEvent.getSource().data("OldDateShift").substring(0, 10);
				//var oldDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT").substring(0,10);
				var oldShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT");
				if (newDateShift !== oldDateShift) {
					updateRowKey(newDateShift, oldDateShift, oldShift, oldShift, oEvent.getSource().oParent.getIndex());
				}
			}
		}).bindProperty(
			"enabled",
			"EDITLOCKED",
			function (val) {
				return BoleanFormatter(val);
			}
		).data("OldDateShift", "{DATE_SHIFT}")
		/*.bindProperty("value",
										"DATE_SHIFT",
									 function(Value){
											if (Value === null) {return;}
											else {return Value.substring(0, 10);}
			})*/
		.addStyleClass("FTColKey");

	var oCol = new sap.ui.table.Column("ColDateShift", {
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
			tooltip: oLng_Monitor.getText("Monitor_DateShift")
		}),
		template: oControl,
		width: "90px",
		flexible: false,
		visible: true,
	});
	oTable.addColumn(oCol);

	// Colonna turno
	oControl = new sap.m.HBox({
		items: [
				 new sap.ui.commons.TextView({
				textAlign: sap.ui.core.TextAlign.Left,
				width: "30px",
			}).bindProperty(
				"text",
				"SHIFT"),
				 new sap.m.Button({
				icon: "sap-icon://arrow-down",
				enabled: true,
				/*type: "{SC_COLOR}",*/
				width: "20px",
				hAlign: sap.ui.core.HorizontalAlign.Left,
				press: function (oEvt) {
					var IdxCdl = oEvt.getSource().oParent.oParent.getIndex();
					$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(IdxCdl);
					var oParams = {
						Selected: fnGetCellValue("EndShiftDeclarationTab", "SHIFT", IdxCdl),
						Plant: fnGetCellValue("EndShiftDeclarationTab", "PLANT", IdxCdl),
					};
					try {
						PathCdl = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
					} catch (err) {
						PathCdl = oEvt.oSource.oBindingContexts.undefined.sPath;
					}
					fnGetShiftTableDialog(oParams, setShiftSelected);
				}
			}).bindProperty(
				"enabled",
				"EDITLOCKED",
				function (val) {
					return BoleanFormatter(val);
				}
				 )
	 ]
	}).addStyleClass("FTColKey");

	oCol = new sap.ui.table.Column("ColShift", {
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
			tooltip: oLng_Monitor.getText("Monitor_Shift")
		}),
		template: oControl,
		width: "70px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Gerarchia CDL
	oControl = new sap.ui.commons.TextView({}).bindProperty(
			"text",
			"HIERCDL"
		).bindProperty(
			"tooltip",
			"HIERREPTXT"
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: "Ger", //oLng_Monitor.getText("Monitor_DateShift"), //"Ger CDL",
			tooltip: "Gerarchia CDL" //oLng_Monitor.getText("Monitor_DateShift")
		}),
		template: oControl,
		width: "60px",
		flexible: false,
		visible: true
	});
	oTable.addColumn(oCol);

	// Colonna CDL
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Center
	}).bindProperty(
		"text",
		"CDLID"
	);
	//.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColCdlid", {
		label: new sap.ui.commons.Label({
			text: "CDL", //oLng_Monitor.getText("Monitor_CDLID"),
			tooltip: oLng_Monitor.getText("Monitor_CDLID")
		}),
		template: oControl,
		width: "60px",
		flexible: false
	});
	oTable.addColumn(oCol);


	// Colonna Apertura dialog scelta CDL
	oControl = new sap.m.Button({
		icon: "sap-icon://arrow-down",
		enabled: true,
		/*type: "{SC_COLOR}",*/
		width: "20px",
		hAlign: sap.ui.core.HorizontalAlign.Left,
		press: function (oEvt) {
			IdxCdl = oEvt.getSource().oParent.getIndex();
			$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(oEvt.getSource().oParent.getIndex());
			var oParams = {
				Selected: fnGetCellValue("EndShiftDeclarationTab", "CDLID", IdxCdl),
				OrdType: "D",
				Plant: fnGetCellValue("EndShiftDeclarationTab", "PLANT", IdxCdl),
				IDx: IdxCdl
			};
			try {
				PathCdl = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
			} catch (err) {
				PathCdl = oEvt.oSource.oBindingContexts.undefined.sPath;
			}
			fnGetCDLTableDialog(oParams, setCDLSelected);
		}
	}).bindProperty(
		"enabled",
		"EDITLOCKED",
		function (val) {
			return BoleanFormatter(val);
		}
	);

	oCol = new sap.ui.table.Column("ColCDLBtn", {
		multiLabels: [
		new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			}),
		new sap.ui.core.Icon({
				src: "sap-icon://tree",
			})
	],
		template: oControl,
		editable: false,
		width: "40px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Ordine/Commessa
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		})
		.bindProperty(
			"text",
			"ORDER"
		)
		.bindText("ORDER", function (value) {
			return parseInt(value);
		})
		.addStyleClass("FTColKey");

	oCol = new sap.ui.table.Column({
		multiLabels: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Order"),
				tooltip: oLng_Monitor.getText("Monitor_Order")
			}),
			new sap.ui.commons.Label({
				text: "" //oLng_Monitor.getText("Monitor_Orig")
			})
		],
		headerSpan: [2, 1],
		template: oControl,
		width: "80px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Fase
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		}).bindProperty(
			"text",
			"POPER"
		).bindProperty(
			"tooltip",
			"CICTXT"
		).bindText("POPER", function (value) {
			return parseInt(value);
		})
		.addStyleClass("FTColKey");

	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Poper")
		}),
		template: oControl,
		width: "40px",
		flexible: false
	});
	oTable.addColumn(oCol);


	// Colonna Materiale
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Begin
		}).bindProperty(
			"text",
			"MATERIAL"
		)
		.bindProperty(
			"tooltip",
			"MATERIAL_DESC"
		)
		.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Material") //"Operatore",
		}),
		template: oControl,
		visible: true,
		width: "120px",
		flexible: false
	});
	oTable.addColumn(oCol);

	//  Colonna Conferma/controllo utente
	oControl = new sap.ui.commons.CheckBox({
			//oControl = new sap.m.CheckBox({
			editable: true,
			layoutData: new sap.ui.layout.form.GridElementData({
				hCells: "1"
			}),
			change: function (oControlEvent) {
				CheckedRow(oControlEvent);
			},
		})
		.bindProperty(
			"checked",
			"USER_CHECK",
			function (val) {
				return !BoleanFormatter(val);
			}
		).bindProperty(
			"enabled",
			"ROWLOCKED",
			function (val) {
				return BoleanFormatter(val);
			}
		);


	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_UserCheck"),
			tooltip: oLng_Monitor.getText("Monitor_UserCheck")
		}),
		template: oControl,
		editable: true,
		width: "34px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Operatore
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Center
		}).bindProperty(
			"text",
			"OPEID"
		).bindProperty(
			"tooltip",
			"OPENAME"
		)
		.addStyleClass("FTColKey");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
			tooltip: oLng_Monitor.getText("Monitor_Operator") //"Operatore",
		}),
		visible: true,
		width: "70px",
		template: oControl,
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Tempo setup
	oControl = new sap.m.Input({
			textAlign: sap.ui.core.TextAlign.Right,
			maxLength: 5,
			undoEnabled: true,
			liveChange: function (ev) {
				EditedCell(ev);
			}
		}).bindProperty(
			"value",
			"TIME_SETUP",
			function (val) {
				return CellFormatterZero(val);
			}
		).bindProperty(
			"editable",
			"EDITLOCKED",
			function (val) {
				return BoleanFormatter(val);
			}
		)
		.addStyleClass("FTColEdit");

	oCol = new sap.ui.table.Column("ColSetup", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Time_SetUp"),
				tooltip: oLng_Monitor.getText("Monitor_Time_SetUp")
			}),
			new sap.ui.commons.Label({
				text: "",
				tooltip: "",
			})],
		headerSpan: [2, 1],
		template: oControl,
		width: "65px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna UM Setup
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Right,
		maxLength: 5,
	}).bindProperty(
		"text",
		"UMSETUP"
	).addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColUMSetup", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: "",
				tooltip: "",
			}),
			new sap.ui.commons.Label({
				text: "UM",
				tooltip: "UM",
			})],
		template: oControl,
		width: "25px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Conteggio macchina
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right,
			maxLength: 5,
		}).bindProperty(
			"text",
			"MACHINE_COUNT",
			function (val) {
				return CellFormatter(val);
			}
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_MachineCount")
		}),
		template: oControl,
		width: "55px",
		flexible: false
	});
	oTable.addColumn(oCol);


	// Colonna Q.tà Buoni proposta
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		}).bindProperty(
			"text",
			"BUQTA_ORI",
			function (val) {
				return CellFormatter(val);
			}
		)
		.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		multiLabels: [
						new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Good")
			}),
						new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Orig")
			})
				],
		template: oControl,
		headerSpan: [2, 1],
		width: "45px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// colonna Q.tà buoni modifica
	oControl = new sap.m.Input({
			textAlign: sap.ui.core.TextAlign.Right,
			maxLength: 5,
			liveChange: function (ev) {
				EditedCell(ev);
			}
		}).bindProperty(
			"value",
			"BUQTA_INS",
			function (val) {
				return CellFormatterZero(val);
			}
		).bindProperty(
			"editable",
			"EDITLOCKED",
			function (val) {
				return BoleanFormatter(val);
			}
		)
		.addStyleClass("FTColEdit");

	oCol = new sap.ui.table.Column({
		multiLabels: [
						new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			}),
						new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Edit")
			})
				],
		template: oControl,
		editable: true,
		width: "65px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Q.tà Sospesi Totale
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		}).bindProperty(
			"text",
			"SOQTA_TOT",
			function (val) {
				return CellFormatter(val);
			}
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColSuspTot", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Suspended")
			}),
														 new sap.ui.commons.Label({
				text: "Tot" //oLng_Monitor.getText("Monitor_Orig")
			})
			],
		headerSpan: [2, 1],
		template: oControl,
		editable: true,
		width: "45px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Apertura dialog Sospesi
	oControl = new sap.m.Button({
		icon: "sap-icon://arrow-down",
		enabled: true,
		type: "{SO_COLOR}",
		width: "20px",
		hAlign: sap.ui.core.HorizontalAlign.Left,
		press: function (oControlEvent) {
			$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(oControlEvent.getSource().oParent.getIndex());
			openEndShiftSDet(
				oLng_Monitor.getText("Monitor_SuspendedReasons"),
				oLng_Monitor.getText("Monitor_SuspendedReason"),
				fnGetCellValue("EndShiftDeclarationTab", "PLANT"),
				fnGetCellValue("EndShiftDeclarationTab", "IDLINE"),
				fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT").substring(0, 10),
				fnGetCellValue("EndShiftDeclarationTab", "SHIFT"),
				fnGetCellValue("EndShiftDeclarationTab", "OPEID"),
				fnGetCellValue("EndShiftDeclarationTab", "ORDER"),
				fnGetCellValue("EndShiftDeclarationTab", "POPER"),
				fnGetCellValue("EndShiftDeclarationTab", "ROWPROG"),
				"SO",
				"EndShiftDeclarationTab",
				fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK"),
				refreshTabEndDeclaration
			);
		}
	});


	oCol = new sap.ui.table.Column("ColSuspBtn", {
		multiLabels: [
				new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			}),
			new sap.ui.core.Icon({
				src: "sap-icon://tree",
			})
				],
		template: oControl,
		editable: false,
		width: "36px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Q.tà scarti Totale
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		}).bindProperty(
			"text",
			"SCQTA_TOT",
			function (val) {
				if (val !== null) {
					val.replace(/[^.\d]/g, '');
				}
				return parseFloat(val).toLocaleString();
			}
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColScrapTot", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Scrap")
			}),
			new sap.ui.commons.Label({
				text: "Tot" //oLng_Monitor.getText("Monitor_Orig")
			})
		],
		headerSpan: [2, 1],
		template: oControl,
		editable: true,
		width: "45px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Apertura dialog scarti
	oControl = new sap.m.Button({
		icon: "sap-icon://arrow-down",
		enabled: true,
		type: "{SC_COLOR}",
		width: "20px",
		hAlign: sap.ui.core.HorizontalAlign.Left,
		/*layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "1"
		}),*/
		press: function (oControlEvent) {
			$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(oControlEvent.getSource().oParent.getIndex());
			openEndShiftSDet(
				oLng_Monitor.getText("Monitor_ScrabReasons"),
				oLng_Monitor.getText("Monitor_ScrabReason"),
				fnGetCellValue("EndShiftDeclarationTab", "PLANT"),
				fnGetCellValue("EndShiftDeclarationTab", "IDLINE"),
				fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT").substring(0, 10),
				fnGetCellValue("EndShiftDeclarationTab", "SHIFT"),
				fnGetCellValue("EndShiftDeclarationTab", "OPEID"),
				fnGetCellValue("EndShiftDeclarationTab", "ORDER"),
				fnGetCellValue("EndShiftDeclarationTab", "POPER"),
				fnGetCellValue("EndShiftDeclarationTab", "ROWPROG"),
				"SC",
				"EndShiftDeclarationTab",
				fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK"),
				refreshTabEndDeclaration
			);
		}
	});

	oCol = new sap.ui.table.Column("ColScrapBtn", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			}),
			new sap.ui.core.Icon({
				src: "sap-icon://tree",
			})
		],
		template: oControl,
		editable: false,
		width: "36px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna progressivo di riga/chiave
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Center
	}).bindProperty(
		"text",
		"ROWPROG"
	);
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Progressive")
		}),
		template: oControl,
		width: "95px",
		flexible: false,
		visible: false
	});
	oTable.addColumn(oCol);

	// Colonna Totale Anime
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Right
		}).bindProperty(
			"text",
			"ANIMEQTA"
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColSoulTot", {
		multiLabels: [
						new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_BrokenSoul")
			}),
						new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			})
				],
		template: oControl,
		headerSpan: [2, 1],
		width: "45px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna apertura dialog Anime
	oControl = new sap.m.Button({
		icon: "sap-icon://arrow-down",
		enabled: true,
		type: "{SOUL_COLOR}",
		width: "20px",
		press: function (oControlEvent) {
			$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(oControlEvent.getSource().oParent.getIndex());
			openSoulDetail(
				fnGetCellValue("EndShiftDeclarationTab", "PLANT"),
				fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT").substring(0, 10),
				fnGetCellValue("EndShiftDeclarationTab", "SHIFT"),
				fnGetCellValue("EndShiftDeclarationTab", "OPEID"),
				fnGetCellValue("EndShiftDeclarationTab", "ORDER"),
				fnGetCellValue("EndShiftDeclarationTab", "POPER"),
				fnGetCellValue("EndShiftDeclarationTab", "ROWPROG"),
				fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK")
			);
		}
	});

	oCol = new sap.ui.table.Column("ColSoulBtn", {
		multiLabels: [
			new sap.ui.commons.Label({
				text: "",
				textAlign: "Right"
			}),
			new sap.ui.core.Icon({
				src: "sap-icon://tree",
			})
				],
		template: oControl,
		editable: false,
		width: "36px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Stampo
	oControl = new sap.ui.commons.TextField({
			textAlign: sap.ui.core.TextAlign.Right,
			maxLength: 20
		}).bindProperty(
			"value",
			"NR_PRNT"
		)
		.addStyleClass("FTColVis");

	oCol = new sap.ui.table.Column("ColPrintNr", {
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_PrintNR")
		}),
		template: oControl,
		width: "70px",
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna MII Guid
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Center
	}).bindProperty(
		"text",
		"MII_GUID"
	);
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_MII_GUID")
		}),
		template: oControl,
		width: "130px",
		visible: false,
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna Sap Guid
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Center
	}).bindProperty(
		"text",
		"SAP_GUID"
	);
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_SAP_GUID")
		}),
		template: oControl,
		width: "130px",
		visible: false,
		flexible: false
	});
	oTable.addColumn(oCol);
	// Colonna mii Guid stornato
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Center
	}).bindProperty(
		"text",
		"MII_GUID_ST"
	);
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_Avert_MII_GUID")
		}),
		template: oControl,
		width: "130px",
		visible: false,
		flexible: false
	});
	oTable.addColumn(oCol);

	// Colonna note
	oControl = new sap.m.Input({
			textAlign: sap.ui.core.TextAlign.Left,
			maxLength: 40,
			change: function (ev) {
				EditedCell(ev);
			}
		}).bindProperty(
			"value",
			"NOTE"
		).bindProperty(
			"editable",
			"EDITLOCKED",
			function (val) {
				return BoleanFormatter(val);
			}
		)
		.addStyleClass("FTColEdit");

	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: "Note", //oLng_Monitor.getText("Monitor_User"),
			tooltip: "Note"
		}),
		template: oControl,
		width: "160px",
		flexible: true
	});
	oTable.addColumn(oCol);

	// Colonna Data Aggiornamento
	oControl = new sap.ui.commons.TextView({
		textAlign: sap.ui.core.TextAlign.Left
	}).bindProperty(
		"text",
		"DATE_UPD"
	);
	oControl.bindText("DATE_UPD", function (value) {
		return fnConvXMLDateTime(value);
	});
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_DateUPD")
		}),
		template: oControl,
		width: "90px",
		flexible: false,
		visible: false,
	});
	oTable.addColumn(oCol);

	// Colonna Utente modifica
	oControl = new sap.ui.commons.TextView({
			textAlign: sap.ui.core.TextAlign.Center
		}).bindProperty(
			"text",
			"USER"
		)
		.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_User"),
			tooltip: oLng_Monitor.getText("Monitor_User")
		}),
		template: oControl,
		width: "100px",
		flexible: true,
		visible: false,
	});
	oTable.addColumn(oCol);

	// Colonna ID riga
	oControl = new sap.ui.commons.TextView({}).bindProperty(
		"text",
		"ROW_IDENTIFIER"
	);
	oControl.addStyleClass("FTColVis");
	oCol = new sap.ui.table.Column({
		template: oControl,
		visible: false,
		flexible: false
	});
	oTable.addColumn(oCol);

	oTable.addStyleClass("sapUiSizeCompact");

	oModel = new sap.ui.model.xml.XMLModel({
		templateShareable: false
	});
	oTable.setModel(oModel);
	oTable.bindRows({
		path: "/0/Rowset/Row",
		templateShareable: false
	});

	$.UIbyID("rdOrdType").setSelectedIndex(0);
	switchLayout();
	return oTable;
}

// Aggiorna la tabella Fine Turno
function refreshTabEndDeclaration() {

	var sDateFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
	var sOperator = ($.UIbyID("txtOpeFiter").data("SelectedKey") === null) ? "" : $.UIbyID("txtOpeFiter").data("SelectedKey");
	var sPlant = "";

	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		sPlant = $('#plant').val();
	} else {
		sPlant = $.UIbyID("filtPlant").getSelectedKey();
	}

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/getEndShiftByParamsQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateFrom +
			"&Param.3=" + sDateTo +
			"&Param.4=" + sOperator +
			"&Param.5=" + $.UIbyID("filtShift").getSelectedKey() +
			"&Param.6=" + $.UIbyID("txtCDLFiter").getValue() +
			"&Param.7=" + $.UIbyID("txtOrderFiter").getValue() +
			"&Param.8=" + $.UIbyID("CmbStateFilter").getSelectedKey() +
			"&Param.9=" + $('#ordType').val() +
			"&Param.10=" + sLanguage,
		dataType: "xml"
	};

	$.UIbyID("EndShiftDeclarationTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("EndShiftDeclarationTab").setBusy(true);
			// carica il model della tabella

			/*
			// imposto ad ogni refresh il model di alimentazione della combo turno in riga tabella
			var cmbRows = $.UIbyID("ColShift").getTemplate();
			var oItemShift = new sap.ui.core.Item();
			oItemShift.bindProperty("key", "SHIFT");
			oItemShift.bindProperty("text", "SHNAME");
			//oItemShift.setModel($.UIbyID("EndShiftDeclarationTab").getModel());
			oItemShift.setModel($.UIbyID("filtShift").getModel());

			//cmbRows.bindProperty("value", "SHIFT");
			cmbRows.bindItems("/Rowset/Row", oItemShift);*/

			$.UIbyID("EndShiftDeclarationTab").getModel().setData(data);

			/*

			//cmbRows.bindProperty("selectedKey", "SHIFT");*/

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {


			var rowsetObject = $.UIbyID("EndShiftDeclarationTab").getModel().getObject("/Rowset/0");
			if (rowsetObject) {
				var iRecNumber = rowsetObject.childNodes.length - 1;
				$.UIbyID("lblRecordNumber").setText(oLng_Monitor.getText("Monitor_RecordNumber") + " " + iRecNumber);
			}

			//$.UIbyID("Send-To-Sap").setEnabled(true);
			//$.UIbyID("EndShiftDeclarationTab").setWidth("99%");
			$.UIbyID("EndShiftDeclarationTab").setBusy(false);
			$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(-1);
			//$.UIbyID("EndShiftDeclarationTab").setWidth("100%");
			// remove busy indicator

			colorGrid();

		});

	//$.UIbyID("EndShiftDeclarationTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
} //refreshTabEndDeclaration

function send_to_sap() {
	var bCheckOK = check_to_sap();

	if (bCheckOK) {

		sap.ui.commons.MessageBox.show(
			"Inviare i dati a SAP?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Richiesta conferma", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
			function (sResult) {
				if (sResult == 'NO') {
					return;
				} else {
					send_conf_sap();
				}
			},
			sap.ui.commons.MessageBox.Action.NO
		);
	}
}

function send_conf_sap() {

	var sDateFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
	var sDateto = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
	var sOperator = ($.UIbyID("txtOpeFiter").data("SelectedKey") === null) ? "" : $.UIbyID("txtOpeFiter").data("SelectedKey");
	var sPlant = "";

	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		sPlant = $('#plant').val();
	} else {
		sPlant = $.UIbyID("filtPlant").getSelectedKey();
	}

	$.UIbyID("EndShiftDeclarationTab").setBusy(true);

	$.UIbyID("CmbStateFilter").setSelectedKey("CONF");

	var qexe = "Content-Type=text/XML&QueryTemplate=ProductionMain/Sap/NextGen/To/sendConfEndShiftQR" +
		"&Param.1=" + sPlant +
		"&Param.2=" + sDateFrom +
		"&Param.3=" + sDateTo +
		"&Param.4=" + sOperator +
		"&Param.5=" + $.UIbyID("filtShift").getSelectedKey() +
		"&Param.6=" + $.UIbyID("txtCDLFiter").getValue() +
		"&Param.7=" + $.UIbyID("txtOrderFiter").getValue() +
		"&Param.8=" + "" +
		"&Param.9=" + $('#ordType').val() +
		"&Param.10=" + sLanguage;

	var ret = fnGetAjaxVal(qexe, ["Code", "Message"], false);

	sap.ui.commons.MessageBox.show(
		ret.Message,
		(ret.Code < 0) ? sap.ui.commons.MessageBox.Icon.WARNING : sap.ui.commons.MessageBox.Icon.SUCCESS,
		"Esito invio", [sap.ui.commons.MessageBox.Action.OK],
		sap.ui.commons.MessageBox.Action.OK
	);

	$.UIbyID("EndShiftDeclarationTab").setBusy(false);

	/*var qParams = {
		data: "ProductionMain/Sap/NextGen/To/sendConfEndShiftQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateFrom +
			"&Param.3=" + $('#ordType').val() +
			"&Param.4=" + sOperator +
			"&Param.5=" + $.UIbyID("filtShift").getSelectedKey() +
			"&Param.6=" + "" +
			"&Param.7=" + $.UIbyID("txtOrderFiter").getValue() +
			"&Param.8=" + $.UIbyID("CmbStateFilter").getSelectedKey() +
			//"&Param.9=" + $.UIbyID("txtMII_GUIDFiter").getValue() +
			"&Param.10=" + sLanguage,
		dataType: "xml"
	};*/

	refreshTabEndDeclaration();
} //send_conf_sap

function check_to_sap() {

	return true;
} //check_to_sap

function fnOpenDeclaration(bEdit) {

	var sCurrentDate;
	if (bEdit) {
		sCurrentDate = dateFormatter(fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT"));
	} else {
		sCurrentDate = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	}
	var oDtStartDate;
	if ($.UIbyID("dtStartDate")) {
		oDtStartDate = $.UIbyID("dtStartDate");
	} else {

		oDtStartDate = new sap.ui.commons.DatePicker('dtStartDate', {
			layoutData: new sap.ui.layout.form.GridElementData({
				hCells: "3"
			})
		});
	}
	oDtStartDate.setEnabled(!bEdit);
	oDtStartDate.setYyyymmdd(sCurrentDate);
	oDtStartDate.setLocale("it");

	var oCmbEditShiftFilter = new sap.ui.commons.ComboBox("filtEditShift", {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "3"
		}),
		selectedKey: bEdit ? fnGetCellValue("EndShiftDeclarationTab", "SHIFT") : "",
		change: function (oEvent) {}
	});
	var oItemEditShift = new sap.ui.core.ListItem();
	oItemEditShift.bindProperty("key", "SHIFT");
	oItemEditShift.bindProperty("text", "SHNAME");
	oCmbEditShiftFilter.bindItems("/Rowset/Row", oItemEditShift);
	var oEditShiftFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oEditShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());
	} else {
		oEditShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
	}
	oCmbEditShiftFilter.setModel(oEditShiftFilter);

	//  Crea la finestra di dialogo
	var oEditEndShiftDecDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgEndShiftDec",
		maxWidth: "600px",
		maxHeight: "600px",
		//minHeight: "900px",
		title: bEdit ? oLng_Monitor.getText("Monitor_Edit") : oLng_Monitor.getText("Monitor_Add"),
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});
	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
										new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_DateShift"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
														oDtStartDate
												]
					}),
										new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Shift"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
														oCmbEditShiftFilter
												]
					}),
										new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								editable: false,
								maxLength: 200,
								id: "txtEndShiftDecOPEID"
							}),
							new sap.ui.commons.Button("btnOpeSelect", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									fnGetOperatorTableDialog(
										($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
										"",
										setSelectedOpeModify
									);
								}
							})
						]
					}),
										new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Order"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
								new sap.ui.commons.TextField({
								editable: false,
								maxLength: 200,
								id: "txtEndShiftORDER"
							}),
								new sap.ui.commons.Button("btnOrdSelect", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectOrder"), //Seleziona ordine
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									var oParams = {
										Selected: $.UIbyID("txtEndShiftORDER").getValue(),
										Material: "",
										Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
										TypeOrdFilter: $('#ordType').val()
									};

									fnGetOrdersTableDialog(oParams, setSelectedOrderModify);
								}
							})
						]
					}),
						new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Poper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: bEdit ? fnGetCellValue("EndShiftDeclarationTab", "POPER") : "",
								editable: false,
								maxLength: 200,
								id: "txtEndShiftPOPER"
							}),
								new sap.ui.commons.Button("btnPOperSelect", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectPOper"), //Seleziona ordine
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									var oParams = {
										Selected: "",
										Order: $.UIbyID("txtEndShiftORDER").getValue(),
										Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
									};

									fnGetOrderOpersTableDialog(
										oParams,
										setSelectedPOperModify
									);
								}
							})
						]
					})

				]
			})
		]
	});

	$.UIbyID("txtEndShiftDecOPEID").setValue(
		fnGetCellValue("EndShiftDeclarationTab", "OPENAME")
	).data("SelectedKey", fnGetCellValue("EndShiftDeclarationTab", "OPEID"));

	oEditEndShiftDecDlg.addContent(oForm1);

	oEditEndShiftDecDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Save"), //"Chiudi",
		press: function () {

			if ($.UIbyID("filtEditShift").getSelectedKey() === null) {
				sap.ui.commons.MessageBox.show(
					//oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
					"Selezionare un turno.",
					sap.ui.commons.MessageBox.Icon.ERROR,
					oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
					[sap.ui.commons.MessageBox.Action.OK],
					sap.ui.commons.MessageBox.Action.OK
				);
				return;
			}

			if ($.UIbyID("txtEndShiftDecOPEID").data("SelectedKey") === null) {
				sap.ui.commons.MessageBox.show(
					//oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
					"Selezionare un operatore.",
					sap.ui.commons.MessageBox.Icon.ERROR,
					oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
					[sap.ui.commons.MessageBox.Action.OK],
					sap.ui.commons.MessageBox.Action.OK
				);
				return;
			}

			if ($.UIbyID("txtEndShiftORDER").data("SelectedKey") === null) {
				sap.ui.commons.MessageBox.show(
					//oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
					"Selezionare una commessa.",
					sap.ui.commons.MessageBox.Icon.ERROR,
					oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
					[sap.ui.commons.MessageBox.Action.OK],
					sap.ui.commons.MessageBox.Action.OK
				);
				return;
			}

			if ($.UIbyID("txtEndShiftPOPER").data("SelectedKey") === null) {
				sap.ui.commons.MessageBox.show(
					//oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
					"Selezionare una fase.",
					sap.ui.commons.MessageBox.Icon.ERROR,
					oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
					[sap.ui.commons.MessageBox.Action.OK],
					sap.ui.commons.MessageBox.Action.OK
				);
				return;
			}


			var oParams = {
				Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
				Shift: $.UIbyID("filtEditShift").getSelectedKey(),
				OpeID: $.UIbyID("txtEndShiftDecOPEID").data("SelectedKey"),
				Order: $.UIbyID("txtEndShiftORDER").data("SelectedKey"),
				Poper: $.UIbyID("txtEndShiftPOPER").data("SelectedKey"),
				DateShift: dtFormat.format(dtSelect.parse($.UIbyID("dtStartDate").getYyyymmdd())),
				QtaGood: 0,
				QtaSetup: 0,
				CdlId: "",
				Note: "",
				Action: "New"
			};

			// Inserisce il record nuovo
			InsertRow(oParams);

			//chiude la dialog  dlgEndShiftDec
			$.UIbyID("dlgEndShiftDec").close();
			$.UIbyID("dlgEndShiftDec").destroy();

		}
	}));

	oEditEndShiftDecDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Cancel"), //"Chiudi",
		press: function () {
			refreshTabEndDeclaration();
			oEditEndShiftDecDlg.close();
			oEditEndShiftDecDlg.destroy();
		}
	}));
	oEditEndShiftDecDlg.open();
}

function fnSendToSap(sMod, sNote, sNewQuantity, sDatario, sCestaTT, sOpeID, iWtDur, sNrPrint) {

	if (sMod == "ST" && fnGetCellValue("EndShiftDeclarationTab", "CONFIRMED") == "0") {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_ConfProdOutOfSap"), //La conferma non è stata inviata a SAP e non può essere stornata
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_AvertError"), //Errore Storno
			[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	if (parseInt(sDatario.substring(0, 2)) > 12) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertCorrectDatario"), //Inserire un datario valido
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
			[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	var currentYear = ((new Date()).getFullYear()).toString();

	if (parseInt(sDatario.substring(2, 4)) > (parseInt(currentYear.substring(2, 4)) + 1) ||
		parseInt(sDatario.substring(2, 4)) < (parseInt(currentYear.substring(2, 4)) - 1)) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertCorrectDatario"), //Inserire un datario valido
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
			[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}


	if (sNote === "" || sNote === null) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
			[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	/*
				--- INVIO CON RICHIESTA DI CONFERMA ---
		var sMsgTitle;
		var sMsgTxt;
		if(sMod == "ST"){
				sMsgTitle = oLng_Monitor.getText("Monitor_AvertValidation"); //"Storno"
				sMsgTxt = oLng_Monitor.getText("Monitor_AvertConfProd"); //"Stornare la conferma selezionata?"
		}
		if(sMod == "RT"){
				sMsgTitle = oLng_Monitor.getText("Monitor_ConfProdModify"); //"Rettifica"
				sMsgTxt = oLng_Monitor.getText("Monitor_ConfProdModifyConfirm"); //"Rettificare la conferma selezionata?"
		}
		if(sMod == "FW"){
				sMsgTitle = oLng_Monitor.getText("Monitor_Forward"); //"Inoltro"
				sMsgTxt = oLng_Monitor.getText("Monitor_ForwardConfProd"); //"Inoltrare nuovamente la conferma selezionata?"
		}

		sap.ui.commons.MessageBox.show(
				sMsgTxt,
				sap.ui.commons.MessageBox.Icon.WARNING,
				sMsgTitle,
				[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
				function(sResult){
						if (sResult == 'YES'){
								var sAnswTitle = ""; //codice imballo, da lasciare vuoto
								var qexe = dataProdSap + "To/renewConfProdToSapQR" +
										"&Param.1=" + fnGetCellValue("EndShiftDeclarationTab", "MII_GUID") +   //GUID di MII
										"&Param.2=" + sMod                                          //Tipologia
										"&Param.3=" + sNote +                                       //note
										"&Param.4=" + sNewQuantity +                                //nuova quantità
										"&Param.5=" + sDatario +                                    //datario
										"&Param.6=" + sCestaTT +                                    //cestaTT
										"&Param.7=" + sOpeID +                                      //id operatore
										"&Param.8=" + iWtDur +                                      //durata in minuti
										"&Param.9=" + sNrPrint;                                     //numero stampo
								var ret = fnGetAjaxVal(qexe,["ResultCode","ResultDescription"],false);
								if(ret.ResultCode == -1){
										sAnswTitle = oLng_Monitor.getText("Monitor_SendError"); //"Errore d'invio"
								}
								else{
										if(sMod == "ST"){
												sAnswTitle = oLng_Monitor.getText("Monitor_AvertMade"); //"Storno effettuato correttamente"
										}
										else{
												if(sMod == "RT"){
														sAnswTitle = oLng_Monitor.getText("Monitor_RetMade"); //"Rettifica effettuata correttamente"
												}
												else{
														sAnswTitle = oLng_Monitor.getText("Monitor_ForwardMade"); //"Inoltro effettuato correttamente"
												}
										}
								}
								sap.ui.commons.MessageBox.show(
										ret.ResultDescription,
										(ret.ResultCode == -1)?sap.ui.commons.MessageBox.Icon.ERROR:sap.ui.commons.MessageBox.Icon.SUCCESS,
										sAnswTitle,
										[sap.ui.commons.MessageBox.Action.OK],
										sap.ui.commons.MessageBox.Action.OK
								);
								if(ret.ResultCode == 0 && sMod == "RT"){
										$.UIbyID("dlgEndShiftDec").close();
										$.UIbyID("dlgEndShiftDec").destroy();
								}
								refreshTabEndDeclaration();
						}
				},
				sap.ui.commons.MessageBox.Action.YES
		);*/

	/* --- INVIO SENZA RICHIESTA DI CONFERMA --- */
	$.UIbyID("dlgEndShiftDec").close();
	$.UIbyID("dlgEndShiftDec").destroy();
	var sAnswTitle = ""; //codice imballo, da lasciare vuoto
	var qexe = dataProdSap + "To/renewConfProdToSapQR" +
		"&Param.1=" + fnGetCellValue("EndShiftDeclarationTab", "MII_GUID") + //GUID di MII
		"&Param.2=" + sMod + //Tipologia
		"&Param.3=" + sNote + //note
		"&Param.4=" + sNewQuantity + //nuova quantità
		"&Param.5=" + sDatario + //datario
		"&Param.6=" + sCestaTT + //cestaTT
		"&Param.7=" + sOpeID + //id operatore
		"&Param.8=" + iWtDur + //durata in minuti
		"&Param.9=" + sNrPrint; //numero stampo

	var ret = fnGetAjaxVal(qexe, ["ResultCode", "ResultDescription"], false);
	if (ret.ResultCode == -1) {
		sAnswTitle = oLng_Monitor.getText("Monitor_SendError"); //"Errore d'invio"
	} else {
		if (sMod == "ST") {
			sAnswTitle = oLng_Monitor.getText("Monitor_AvertMade"); //"Storno effettuato correttamente"
		} else {
			if (sMod == "RT") {
				sAnswTitle = oLng_Monitor.getText("Monitor_RetMade"); //"Rettifica effettuata correttamente"
			} else {
				sAnswTitle = oLng_Monitor.getText("Monitor_ForwardMade"); //"Inoltro effettuato correttamente"
			}
		}
	}
	sap.ui.commons.MessageBox.show(
		ret.ResultDescription,
		(ret.ResultCode == -1) ? sap.ui.commons.MessageBox.Icon.ERROR : sap.ui.commons.MessageBox.Icon.SUCCESS,
		sAnswTitle, [sap.ui.commons.MessageBox.Action.OK],
		sap.ui.commons.MessageBox.Action.OK
	);
	/*
		if(ret.ResultCode == 0 && sMod == "RT"){
				$.UIbyID("dlgEndShiftDec").close();
				$.UIbyID("dlgEndShiftDec").destroy();
		}
		*/
	refreshTabEndDeclaration();
}

function setSelectedOperator(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("BADGE", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtOpeFiter").setValue(sOpeName).data("SelectedKey", sBadge);
	refreshTabEndDeclaration();
	$.UIbyID("dlgOperatorTableSelect").destroy();
	$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedOpeModify(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("PERNR", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtEndShiftDecOPEID").setValue(sOpeName).data("SelectedKey", sBadge);
	$.UIbyID("dlgOperatorTableSelect").destroy();
} //setSelectedOpeModify

function setSelectedOrderModify(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sOrder = oControlEvent.getSource().getModel().getProperty("ORDER", oSelContext);
	//var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtEndShiftORDER").setValue(sOrder).data("SelectedKey", sOrder);
	$.UIbyID("dlgOrdersTableSelect").destroy();
} // setSelectedOrderModify


function setSelectedPOperModify(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sPoper = oControlEvent.getSource().getModel().getProperty("POPER", oSelContext);
	var sPOpeDesc = oControlEvent.getSource().getModel().getProperty("CICTXT", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtEndShiftPOPER").setValue(sPoper + " - " + sPOpeDesc).data("SelectedKey", sPoper);
	$.UIbyID("dlgOrderPopersTableSelect").destroy();
} // setSelectedPOperModify

function setSelectedSoulModify(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sComponent = oControlEvent.getSource().getModel().getProperty("COMPONENT", oSelContext);
	var sDesc = oControlEvent.getSource().getModel().getProperty("DESC", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtEndShiftSoulCode").setValue(sComponent).data("SelectedKey", sComponent);
	$.UIbyID("dlgOrderComponentTableSelect").destroy();
} // setSelectedPOperModify

function setCDLSelected(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sCdlid = oControlEvent.getSource().getModel().getProperty("CDLID", oSelContext);
	//var sDesc = oControlEvent.getSource().getModel().getProperty("DESC", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	//$.UIbyID("txtEndShiftSoulCode").setValue(sComponent).data("SelectedKey", sComponent);


	$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(PathCdl + "/CDLID", sCdlid);
	UpdateRow(IdxCdl);

	$.UIbyID("dlgCDLTableSelect").destroy();
} // setCDLSelected

function setShiftSelected(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sNewShift = oControlEvent.getSource().getModel().getProperty("SHIFT", oSelContext);
	var sOldShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT");
	var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT").substring(0, 10);


	if (sNewShift !== sOldShift) {
		$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(PathCdl + "/SHIFT", sNewShift);
		updateRowKey(sDateShift, sDateShift, sNewShift, sOldShift, $.UIbyID("EndShiftDeclarationTab").getSelectedIndex());
	}

	$.UIbyID("dlgShiftsTableSelect").destroy();
} //


function setSelectedCDLFilter(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sCdlid = oControlEvent.getSource().getModel().getProperty("CDLID", oSelContext);

	$.UIbyID("txtCDLFiter").setValue(sCdlid);

	$.UIbyID("dlgCDLTableSelect").destroy();
} // setSelectedCDLFilter

function dateFormatter(sDate) {
	console.log(sDate);
	if (Date.parse(sDate)) {
		return sDate.substring(0, 10);
	} else {
		return '-';
	}
}

function BoleanFormatter(val) {
	if (val === "1") {
		return false;
	} else if (val === "0") {
		return true;
	}
}

function CellFormatter(val) {
	if (val !== null) {
		val.replace(/[^.\d]/g, '');
	}
	if (val === "") {
		return "";
	} else {
		return parseFloat(val).toLocaleString();
	}
}

function CellFormatterZero(val) {
	if (val !== null) {
		val.replace(/[^.\d]/g, '');
	}
	if (val === "" || val === "0") {
		return "";
	} else {
		return parseFloat(val).toLocaleString();
	}
}

function EditedCell(oEvt) {
	/*
	var oldFocus = this['oldFocus'];
	var oldVal = this['oldValue'];
	if (newval.split(".").length - 1 > 1) {
		newval = oldVal;
		this.setValue(newval);
		this['oldValue'] = newval;
	}

	if (newval === oldVal) {
		this.applyFocusInfo({
			cursorPos: oldFocus.cursorPos
		});
	} else {
		this['oldFocus'] = this.getFocusInfo();
	} */
	var RowIdx = oEvt.getSource().oParent.getIndex();
	var tabPath = "";
	var fieldName = oEvt.oSource.mBindingInfos.value.parts[0].path; //Recupera il nome del campo editato
	var val = oEvt.getParameter('value');
	var newval = val;
	if (fieldName !== "NOTE") {
		newval = val.replace(/[^.\d]/g, '');
	}
	try {
		tabPath = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
	} catch (err) {
		tabPath = oEvt.oSource.oBindingContexts.undefined.sPath;
	}
	console.debug("PathModel:" + tabPath);
	$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(tabPath + "/" + fieldName, newval);

	UpdateRow(RowIdx);
} //EditedCell

function CheckedRow(oEvt) {

	var RowIdx = oEvt.getSource().oParent.getIndex();
	var tabPath = "";
	//var fieldName = oEvt.oSource.mBindingInfos.value.parts[0].path; //Recupera il nome del campo editato
	var val = oEvt.getParameter('checked');

	try {
		tabPath = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
	} catch (err) {
		tabPath = oEvt.oSource.oBindingContexts.undefined.sPath;
	}
	console.debug("PathModel:" + tabPath);
	if (val) {
		$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(tabPath + "/USER_CHECK", "1");
		$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(tabPath + "/EDITLOCK", "1");

		$.UIbyID("Send-To-Sap").setEnabled(true);
		$("sapMInput sapMInputBase").each(function (index) {
			console.debug(index);
		});

	} else {
		$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(tabPath + "/USER_CHECK", "0");
		$.UIbyID("EndShiftDeclarationTab").getModel().setProperty(tabPath + "/EDITLOCK", "0");
	}
	/*$.UIbyID("EndShiftDeclarationTab").getModel().refresh(true);
	$.UIbyID("EndShiftDeclarationTab").getModel().forceNoCache(true);
	$.UIbyID("EndShiftDeclarationTab").getModel().updateBindings(true);*/


	UpdateRowUCheck(RowIdx);
	refreshTabEndDeclaration();
} //CheckedRow

function colorGrid() {
	console.debug("Coloro la griglia");
	$(".FTColVis").parent().parent().addClass("FTCellVis");
	$(".FTColKey").parent().parent().addClass("FTCellKey");
}

function switchLayout(ordType) {
	console.debug($.UIbyID("rdOrdType").getSelectedButton().getId());
	//$.UIbyID("EndShiftDeclarationTab").setWidth("99%");
	if (ordType === "D") {
		//($.UIbyID("rdOrdType").getSelectedIndex() === 1)
		//getSelectedButton().getId() === "rdDisc"
		console.debug("Layout Fonderia");
		$.UIbyID("ColSoulTot").setVisible(true);
		$.UIbyID("ColSoulBtn").setVisible(true);
		$.UIbyID("ColSuspTot").setVisible(false);
		$.UIbyID("ColSuspBtn").setVisible(false);
		$.UIbyID("ColPrintNr").setVisible(true);
		$.UIbyID("ColSetup").setVisible(true);
		$.UIbyID("ColUMSetup").setVisible(true);
		$.UIbyID("ColCDLBtn").setVisible(true);
		$('#ordType').val("D");
	} else if (ordType === "R") {
		//($.UIbyID("rdOrdType").getSelectedIndex() === 0) {
		//getSelectedButton().getId() === "rdRipet"
		console.debug("Layout Seconde lavorazioni");
		$.UIbyID("ColSoulTot").setVisible(false);
		$.UIbyID("ColSoulBtn").setVisible(false);
		$.UIbyID("ColSuspTot").setVisible(true);
		$.UIbyID("ColSuspBtn").setVisible(true);
		$.UIbyID("ColPrintNr").setVisible(false);
		$.UIbyID("ColSetup").setVisible(false);
		$.UIbyID("ColUMSetup").setVisible(false);
		$.UIbyID("ColCDLBtn").setVisible(false);
		$('#ordType').val("R");

		//$.UIbyID("ColCDLBtn").setVisible(false);
	}
	//$.UIbyID("EndShiftDeclarationTab").setWidth("100%");
	colorGrid();
	refreshTabEndDeclaration();

}

function fnDelRow() {
	var RowIdx = $.UIbyID("EndShiftDeclarationTab").getSelectedIndex();
	var sPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx).substring(0, 10);
	var sShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var sOpedId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var sOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var sPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var sProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);
	//var sDateShift = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Movement/NextGen/Confirm/delDEndShiftQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateShift +
			"&Param.3=" + sShift +
			"&Param.4=" + sOpedId +
			"&Param.5=" + sOrder +
			"&Param.6=" + sPoper +
			"&Param.7=" + sProg,
		dataType: "xml"
	};


	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {
			refreshTabEndDeclaration();
			colorGrid();
		});
} //fnDelRow



function fnDuplicateRow() {
	var RowIdx = $.UIbyID("EndShiftDeclarationTab").getSelectedIndex();
	var sPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	var sShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var sOpedId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var sOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var sPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var sProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);
	var sCdlId = fnGetCellValue("EndShiftDeclarationTab", "CDLID", RowIdx);
	var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx).substring(0, 10);

	var qParams = {
		data: "ProductionMain/Movement/NextGen/Confirm/duplicateRowEndShiftQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateShift +
			"&Param.3=" + sShift +
			"&Param.4=" + sOpedId +
			"&Param.5=" + sOrder +
			"&Param.6=" + sPoper +
			"&Param.7=" + sProg +
			"&Param.8=" + sCdlId,
		dataType: "xml"
	};


	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {
			refreshTabEndDeclaration();
			colorGrid();
		});
} //fnDuplicateRow


function fnQuestionDelRow() {
	var RowIdx = $.UIbyID("EndShiftDeclarationTab").getSelectedIndex();
	var sCheck = fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK", RowIdx);

	if (sCheck === "1") {
		sap.ui.commons.MessageBox.show(
			"La riga non è cancellabile",
			sap.ui.commons.MessageBox.Icon.ERROR,
			"Operazione non ammessa",

			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}
	sap.ui.commons.MessageBox.show(
		"Eliminare la riga selezionata?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		"Conferma operazione", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				fnDelRow();
				return;
			}

		},
		sap.ui.commons.MessageBox.Action.YES
	);
} //fnQuestionDelRow



function fnQuestionDupplicateRow() {
	sap.ui.commons.MessageBox.show(
		"Duplicare la riga selezionata?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		"Conferma operazione", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				fnDuplicateRow();
				return;
			}

		},
		sap.ui.commons.MessageBox.Action.YES
	);
} //fnQuestionDupplicateRow

function UpdateRow(RowIdx) {

	var sPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx).substring(0, 10);
	var sShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var sOpedId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var sOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var sPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var sProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);
	var sCdlid = fnGetCellValue("EndShiftDeclarationTab", "CDLID", RowIdx);
	var sQtaBu = fnGetCellValue("EndShiftDeclarationTab", "BUQTA_INS", RowIdx);
	var sSetup = fnGetCellValue("EndShiftDeclarationTab", "TIME_SETUP", RowIdx);
	//var sSetup = fnGetCellValue("EndShiftDeclarationTab", "TIME_SETUP", RowIdx);
	var sCheck = fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK", RowIdx);
	var sNote = fnGetCellValue("EndShiftDeclarationTab", "NOTE", RowIdx);

	//var sDateShift = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/updateEndShiftSapQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateShift +
			"&Param.3=" + sShift +
			"&Param.4=" + sOpedId +
			"&Param.5=" + sOrder +
			"&Param.6=" + sPoper +
			"&Param.7=" + sProg +
			"&Param.8=" + sCdlid +
			"&Param.9=" + sQtaBu +
			"&Param.10=" + sSetup +
			"&Param.11=" + sNote +
			"&Param.12=" + sCheck +
			"&Param.13=" + sLanguage,
		dataType: "xml"
	};


	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			colorGrid();
		});

} //UpdateRow

function UpdateRowUCheck(RowIdx) {

	var sPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx).substring(0, 10);
	var sShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var sOpedId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var sOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var sPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var sProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);
	var sCheck = fnGetCellValue("EndShiftDeclarationTab", "USER_CHECK", RowIdx);

	//var sDateShift = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/updateEndShiftUserCheckSQ" +
			"&Param.1=" + sPlant +
			"&Param.2=" + sDateShift +
			"&Param.3=" + sShift +
			"&Param.4=" + sOpedId +
			"&Param.5=" + sOrder +
			"&Param.6=" + sPoper +
			"&Param.7=" + sProg +
			"&Param.8=" + sCheck +
			"&Param.9=" + sLanguage,
		dataType: "xml"
	};


	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			colorGrid();
		});

} //UpdateRowUCheck

function updateRowKey(newDateShift, oldDateShift, newShift, oldShift, RowIdx) {

	var sPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	//var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx).substring(0, 10);
	//var sShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var sOpedId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var sOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var sPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var sProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);

	if (oldDateShift === newDateShift && oldShift === newShift) {
		return;
	}

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/updateEndShiftSapKeysQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + oldDateShift +
			"&Param.3=" + oldShift +
			"&Param.4=" + sOpedId +
			"&Param.5=" + sOrder +
			"&Param.6=" + sPoper +
			"&Param.7=" + sProg +
			"&Param.8=" + newDateShift +
			"&Param.9=" + newShift +
			"&Param.13=" + sLanguage,
		dataType: "xml"
	};


	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {
			refreshTabEndDeclaration();
		});

} //updateRowKey


// Funzione per inserire una nuova riga
function InsertRow(oParams) {
	/* Elenco campi oParams
	oParams.Plant
	oParams.DateShift
	oParams.Shift
	oParams.OpeID
	oParams.Order
	oParams.Poper
	oParams.QtaGood
	oParams.QtaSetup
	oParams.CdlId
	oParams.Note
	*/

	//var sDateShift = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Movement/NextGen/Confirm/AddUpdDEndShiftSapQR" +
			"&Param.1=" + oParams.Plant +
			"&Param.2=" + oParams.DateShift +
			"&Param.3=" + oParams.Shift +
			"&Param.4=" + oParams.OpeID +
			"&Param.5=" + oParams.Order +
			"&Param.6=" + oParams.Poper +
			"&Param.7=" + oParams.QtaSetup +
			"&Param.8=" + oParams.QtaGood +
			"&Param.9=1" +
			"&Param.10=",
		dataType: "xml"
	};

	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {
			refreshTabEndDeclaration();

		});

}
