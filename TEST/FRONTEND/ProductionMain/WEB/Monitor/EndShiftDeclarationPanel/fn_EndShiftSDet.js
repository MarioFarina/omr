/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300 , -W098*/

//**************************************************************************************
//Title:  Dettaglio Sospesi / Scarti (Dichiarazione di fine turno)
//Author: Bruno Rosati
//Date:   19/10/2018
//Vers:   1.1.52
//Modifiers: Luca Adanti 22/11/2018
//**************************************************************************************
// Script per dettaglio Monitor UDC

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});

function openEndShiftSDet(sTitle, sReasonColName, pPlant, pIdLine, pDateShift, pShift,
	pOpeId, pOrder, pPoper, pRowProg, pStype, sOrigTableName, pLocked, refreshFunc) {

	var bLocked = (pLocked === "1") ? false : true;
	if ($.UIbyID("dlgEndShiftSDet")) {
		$.UIbyID("dlgEndShiftSDet").destroy();
	}

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		autoClose: false,
		id: "dlgEndShiftSDet",
		maxWidth: "700px",
		maxHeight: "900px",
		title: sTitle,
		showCloseButton: false
	});

	//Crea L'oggetto Tabella
	var oTable = UI5Utils.init_UI5_Table({
		id: "EndShiftSDetTab",
		exportButton: false,
		properties: {
			//title: oLng_Monitor.getText("Monitor_ConfProdList"), //"Lista avanzamenti di produzione",
			visibleRowCount: 7,
			fixedColumnCount: 0,
			editable: true,
			width: "100%", //"1285px",//
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function (oControlEvent) {
				try {
					$.UIbyID("dlgUDCdet").destroy();
					$.UIbyID("dlgConfProdErrDet").destroy();
				} catch (err) {}
				if ($.UIbyID("EndShiftSDetTab").getSelectedIndex() == -1) {
					$.UIbyID("btnEditEndShiftSreas").setEnabled(false);
					$.UIbyID("btnDelEndShiftSreas").setEnabled(false);
				} else {
					$.UIbyID("btnEditEndShiftSreas").setEnabled(bLocked);
					$.UIbyID("btnDelEndShiftSreas").setEnabled(bLocked);
				}
			},
			close: function (oControlEvent) {
				oEdtDlg.destroy();
			},
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Add"),
						id: "btnAddEndShiftSreas",
						icon: 'sap-icon://add',
						enabled: bLocked,
						press: function () {
							fnEditEndShiftSDet(false, sReasonColName, pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
						}
					}),
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Modify"),
						id: "btnEditEndShiftSreas",
						icon: 'sap-icon://edit',
						enabled: false,
						press: function () {
							fnEditEndShiftSDet(true, sReasonColName, pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
						}
					}),
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Delete"),
						id: "btnDelEndShiftSreas",
						icon: 'sap-icon://delete',
						enabled: false,
						press: function () {
							fnDelEndShiftDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
						}
					})
				],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
						}
					})
				]
			})
		},
		//exportButton: false,
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "90px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "IDLINE",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"),
				properties: {
					width: "100px",
					visible: false,
					flexible: false
				},
				template: {
					type: "Date",
					textAlign: "Center"
				}
			},
			{
				Field: "SHIFT",
				label: oLng_Monitor.getText("Monitor_Shift"),
				properties: {
					width: "70px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "OPEID",
				label: oLng_Monitor.getText("Monitor_OperatorID"),
				properties: {
					width: "80px",
					flexible: false,
					visible: false
				}
			},
			{
				Field: "ORDER",
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
					//resizable : false,
					visible: false,
					flexible: false
				}
			},
			{
				Field: "POPER",
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
					//resizable : false,
					visible: false,
					flexible: false
				}
			},
			{
				Field: "ROWPROG",
				label: oLng_Monitor.getText("Monitor_UDCNR"),
				properties: {
					width: "140px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"),
				properties: {
					width: "110px",
					//resizable : false,
					visible: false,
					flexible: false
				}
			},
			{
				Field: "MATERIAL_DESC",
				label: oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "180px",
					//resizable : false,
					visible: false,
					flexible: false
				}
			},
			{
				Field: "SREASON",
				label: oLng_Monitor.getText(sReasonColName),
				properties: {
					width: "160px",
					flexible: false
				}
			},
			{
				Field: "SREASON_DESCR",
				label: oLng_Monitor.getText("Monitor_ReasonDesc"),
				properties: {
					width: "160px",
					//resizable : false,
					flexible: false
				}
			},
			{
				Field: "STYPE",
				properties: {
					width: "80px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "QTA_ORI",
				label: oLng_Monitor.getText("Monitor_OrigQuantity"),
				properties: {
					width: "135px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
			}
			/*,
						{
							Field: "QTA_INS",
							label: oLng_Monitor.getText("Monitor_EditQuantity"),
							properties: {
								width: "135px",
								//resizable : false,
								flexible: false
							},
							template: {
								type: "Numeric",
								textAlign: "Right"
							}
						}*/
		]
	});


	oControl = new sap.m.Input({
		textAlign: sap.ui.core.TextAlign.Right,
		maxLength: 5,
		editable: bLocked,
		undoEnabled: true,
		change: function (ev) {
			EditedCellReason(ev);
		}
	}).bindProperty(
		"value",
		"QTA_INS",
		function (val) {
			return CellFormatterZero (val);
		}
	);

	oCol = new sap.ui.table.Column("ColQtaIns", {
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_EditQuantity"),
			tooltip: oLng_Monitor.getText("Monitor_EditQuantity")
		}),
		template: oControl,
		width: "100px",
		flexible: false
	});
	oTable.addColumn(oCol);
	oTable.addStyleClass("sapUiSizeCompact");

	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");

	var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
		content: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Order") + ": " + pOrder,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Poper") + ": " + pPoper,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({}),
			oTable
		],
		width: "100%"
	});

	oEdtDlg.addContent(oVerticalLayout);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Confirm"),
		press: function () {
			refreshFunc();
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));

	refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
	oEdtDlg.open();
}

function openSoulDetail(pPlant, pDateShift, pShift,
	pOpeId, pOrder, pPoper, pRowProg, pLocked) {

	var bLocked = (pLocked === "1") ? false : true;

	if ($.UIbyID("dlgSoulDet")) {
		$.UIbyID("dlgSoulDet").destroy();
	}

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgSoulDet",
		maxWidth: "700px",
		maxHeight: "900px",
		title: "Dettaglio selezione anime",
		showCloseButton: false
	});

	//Crea L'oggetto Tabella
	var oTable = UI5Utils.init_UI5_Table({
		id: "SoulDetTab",
		exportButton: false,
		properties: {
			//title: oLng_Monitor.getText("Monitor_ConfProdList"), //"Lista avanzamenti di produzione",
			visibleRowCount: 7,
			fixedColumnCount: 0,
			width: "100%", //"1285px",//
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function (oControlEvent) {
				try {
					$.UIbyID("dlgUDCdet").destroy();
					$.UIbyID("dlgConfProdErrDet").destroy();
				} catch (err) {}
				if ($.UIbyID("SoulDetTab").getSelectedIndex() == -1) {
					$.UIbyID("btnEditSoul").setEnabled(false);
					$.UIbyID("btnDelSoul").setEnabled(false);
				} else {
					$.UIbyID("btnEditSoul").setEnabled(bLocked);
					$.UIbyID("btnDelSoul").setEnabled(bLocked);
				}
			},
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Add"),
						id: "btnAddSoul",
						icon: 'sap-icon://add',
						enabled: bLocked,
						press: function () {
							fnEditEndShiftSoul(true, pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
						}
					}),
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Modify"),
						id: "btnEditSoul",
						icon: 'sap-icon://edit',
						enabled: false,
						press: function () {
							fnEditEndShiftSoul(false, pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
						}
					}),
					new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_Delete"),
						id: "btnDelSoul",
						icon: 'sap-icon://delete',
						enabled: false,
						press: function () {
							fnDelEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
						}
					})
				],
				/*rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
						}
					})
				]*/
			})
		},
		columns: [
			{
				Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"),
				properties: {
					width: "110px",
					//resizable : false,
					visible: true,
					flexible: false
				}
			},
			{
				Field: "MATERIAL_DESC",
				label: oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "180px",
					//resizable : false,
					visible: true,
					flexible: false
				}
			}
			/*,
						{
							Field: "QTA_INS",
							label: oLng_Monitor.getText("Monitor_OrigQuantity"),
							properties: {
								width: "135px",
								//resizable : false,
								flexible: false
							},
							template: {
								type: "Numeric",
								textAlign: "Right"
							}
						}*/
		]
	});

	oControl = new sap.m.Input({
		textAlign: sap.ui.core.TextAlign.Right,
		editable: bLocked,
		maxLength: 5,
		undoEnabled: true,
		change: function (ev) {
			EditedCellSoul(ev);
		}
	}).bindProperty(
		"value",
		"QTA_INS",
		function (val) {
			return CellFormatterZero (val);
		}
	);

	oCol = new sap.ui.table.Column("ColQtaSoul", {
		label: new sap.ui.commons.Label({
			text: oLng_Monitor.getText("Monitor_OrigQuantity"),
			tooltip: oLng_Monitor.getText("Monitor_OrigQuantity")
		}),
		template: oControl,
		width: "100px",
		flexible: false
	});
	oTable.addColumn(oCol);

	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	oTable.addStyleClass("sapUiSizeCompact");

	var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
		content: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Order") + ": " + pOrder,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Poper") + ": " + pPoper,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({}),
			oTable
		],
		width: "100%"
	});

	oEdtDlg.addContent(oVerticalLayout);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Confirm"),
		press: function () {
			//refreshFunc();
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));

	refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
	oEdtDlg.open();
} //End Function openSoulDetail

function refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {

	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/getEndShiftSoulByParamsSQ" +
			"&Param.1=" + pPlant +
			"&Param.3=" + pDateShift +
			"&Param.4=" + pShift +
			"&Param.5=" + pOpeId +
			"&Param.6=" + pOrder +
			"&Param.7=" + pPoper +
			"&Param.8=" + pRowProg,
		dataType: "xml"
	};

	$.UIbyID("SoulDetTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("SoulDetTab").setBusy(true);
			// carica il model della tabella
			$.UIbyID("SoulDetTab").getModel().setData(data);
		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			// remove busy indicator
			$.UIbyID("SoulDetTab").setBusy(false);
		});
	$.UIbyID("SoulDetTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}


function refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype) {

	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Monitor/EndShiftDeclaration/getEndShiftSreasonByParamsSQ" +
			"&Param.1=" + pPlant +
			"&Param.2=" + pIdLine +
			"&Param.3=" + pDateShift +
			"&Param.4=" + pShift +
			"&Param.5=" + pOpeId +
			"&Param.6=" + pOrder +
			"&Param.7=" + pPoper +
			"&Param.8=" + pRowProg +
			"&Param.9=" + pStype,
		dataType: "xml"
	};

	$.UIbyID("EndShiftSDetTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("EndShiftSDetTab").setBusy(true);
			// carica il model della tabella
			$.UIbyID("EndShiftSDetTab").getModel().setData(data);
		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			// remove busy indicator
			$.UIbyID("EndShiftSDetTab").setBusy(false);
		});
	$.UIbyID("EndShiftSDetTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}

function fnEditEndShiftSDet(bEdit, sReasonColName, pPlant, pIdLine, pDateShift, pShift,
	pOpeId, pOrder, pPoper, pRowProg, pStype) {

	var oCmbEditSreasonFilter = new sap.ui.commons.ComboBox("filtEditSreason", {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "3"
		}),
		selectedKey: bEdit ? fnGetCellValue("EndShiftSDetTab", "SREASON") : "",
		change: function (oEvent) {}
	});
	var oItemEditSreason = new sap.ui.core.ListItem();
	oItemEditSreason.bindProperty("key", "SCTYPE");
	oItemEditSreason.bindProperty("text", "REASTXT");
	oCmbEditSreasonFilter.bindItems("/Rowset/Row", oItemEditSreason);
	var oEditSreasonFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oEditSreasonFilter.loadData(
			QService + dataProdMD + "ScrabReasons/getScrabReasonsGroupsByType2SQ" +
			"&Param.1=" + $('#plant').val() + "&Param.2=" + pStype
		);
	} else {
		oEditSreasonFilter.loadData(
			QService + dataProdMD + "ScrabReasons/getScrabReasonsGroupsByType2SQ" +
			"&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + pStype
		);
	}
	oCmbEditSreasonFilter.setModel(oEditSreasonFilter);
	oCmbEditSreasonFilter.setEnabled(!bEdit);

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgEditEndShiftSDet",
		maxWidth: "600px",
		maxHeight: "600px",
		title: bEdit ? oLng_Monitor.getText("Monitor_Modify") : oLng_Monitor.getText("Monitor_Add"),
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});

	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Order"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: pOrder,
								editable: false,
								maxLength: 12,
								id: "txtEditEndShiftORDER",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Poper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: pPoper,
								editable: false,
								maxLength: 4,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
								id: "txtEditEndShiftPOPER"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText(sReasonColName),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							oCmbEditSreasonFilter
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_OrigQuantity"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: bEdit ? fnGetCellValue("EndShiftSDetTab", "QTA_ORI") : "",
								editable: false,
								maxLength: 5,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
								id: "txtEditEndShiftQTA_ORI",
								liveChange: function (ev) {
									var oldVal = this['oldValue'];
									var val = ev.getParameter('liveValue');
									var oldFocus = this['oldFocus'];
									var newval = val.replace(/[^.\d]/g, '');
									if (newval.split(".").length - 1 > 1) {
										newval = oldVal;
										this.setValue(newval);
										this['oldValue'] = newval;
									}
									if (newval === oldVal) {
										this.applyFocusInfo({
											cursorPos: oldFocus.cursorPos
										});
									} else {
										this['oldFocus'] = this.getFocusInfo();
									}
								}
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_EditQuantity"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: bEdit ? fnGetCellValue("EndShiftSDetTab", "QTA_INS") : "",
								editable: true,
								maxLength: 5,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
								id: "txtEditEndShiftQTA_INS",
								liveChange: function (ev) {
									var oldVal = this['oldValue'];
									var val = ev.getParameter('liveValue');
									var oldFocus = this['oldFocus'];
									var newval = val.replace(/[^.\d]/g, '');
									if (newval.split(".").length - 1 > 1) {
										newval = oldVal;
										this.setValue(newval);
										this['oldValue'] = newval;
									}
									if (newval === oldVal) {
										this.applyFocusInfo({
											cursorPos: oldFocus.cursorPos
										});
									} else {
										this['oldFocus'] = this.getFocusInfo();
									}
								}
							})
						]
					})
				]
			})
		]
	});

	oEdtDlg.addContent(oForm1);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Save"),
		press: function () {
			fnSaveEndShiftDet(bEdit, pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
		}
	}));

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press: function () {
			refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));

	oEdtDlg.open();
} //fnEditEndShiftSDet

// Popup per inserimento modifica anime
function fnEditEndShiftSoul(bEdit, pPlant, pDateShift, pShift,
	pOpeId, pOrder, pPoper, pRowProg) {

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgEditEndShiftSoul",
		maxWidth: "600px",
		maxHeight: "600px",
		title: bEdit ? oLng_Monitor.getText("Monitor_Modify") : oLng_Monitor.getText("Monitor_Add"),
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});

	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Order"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: pOrder,
								editable: false,
								maxLength: 12,
								id: "txtEditEndShiftORDER",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Poper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: pPoper,
								editable: false,
								maxLength: 4,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
								id: "txtEditEndShiftPOPER"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: "Materiale anima",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								editable: false,
								maxLength: 200,
								id: "txtEndShiftSoulCode",
								value: fnGetCellValue("SoulDetTab", "MATERIAL"),
							}),
							new sap.ui.commons.Button("btnSoulSelect", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectOrder"), //Seleziona ordine
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									var oParams = {
										Selected: $.UIbyID("txtEndShiftSoulCode").getValue(),
										Order: pOrder,
										Poper: pPoper,
										Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
									};

									fnGetSoulTableDialog(oParams, setSelectedSoulModify);
								}
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: "Qtà anima",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: fnGetCellValue("SoulDetTab", "QTA_INS"),
								editable: true,
								maxLength: 5,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								}),
								id: "txtEditEndShiftQTA_Soul",
								liveChange: function (ev) {

								}

							})]
					})]
			})]
	});

	oEdtDlg.addContent(oForm1);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Save"),
		press: function () {
			fnSaveEndShiftSoul(bEdit, pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
		}
	}));

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press: function () {
			refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));

	oEdtDlg.open();
}



function fnSaveEndShiftDet(bEdit, pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype) {

	var sMode = bEdit ? "UPD" : "ADD";

	if ($.UIbyID("filtEditSreason").getSelectedKey() === "") {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_SelectReason"),
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);

		return;
	}

	var qtaOri = parseFloat($.UIbyID("txtEditEndShiftQTA_ORI").getValue() === "" ? "0" : $.UIbyID("txtEditEndShiftQTA_ORI").getValue());
	var qtaIns = parseFloat($.UIbyID("txtEditEndShiftQTA_INS").getValue() === "" ? "0" : $.UIbyID("txtEditEndShiftQTA_INS").getValue());

	if (qtaOri === 0 && qtaIns === 0) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertQuantity"),
			sap.ui.commons.MessageBox.Icon.WARNING,
			oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);

		return;
	}
	fnSaveEndShiftDetail(sMode, pPlant, pIdLine, pDateShift,
		pShift, pOpeId, pOrder, pPoper, pRowProg,
		pStype, $.UIbyID("filtEditSreason").getSelectedKey(), qtaOri, qtaIns);

}

function fnSaveEndShiftDetail(sMode, pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg,
	pStype, pReason, qtaOri, qtaIns) {
	var qexe = dataProdMov + "NextGen/Confirm/AddUpdDelDEndShiftDetQR" +
		"&Param.1=" + pPlant +
		"&Param.2=" + pIdLine +
		"&Param.3=" + pDateShift +
		"&Param.4=" + pShift +
		"&Param.5=" + pOpeId +
		"&Param.6=" + pOrder +
		"&Param.7=" + pPoper +
		"&Param.8=" + pRowProg +
		"&Param.9=" + pReason +
		"&Param.10=" + pStype +
		"&Param.11=" + qtaOri +
		"&Param.12=" + qtaIns +
		"&Param.13=" + sMode +
		"&Param.14=" + sLanguage;

	var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
	if (ret.code < 0) {
		sap.ui.commons.MessageBox.show(
			ret.message,
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	} else {
		if ($.UIbyID("dlgEditEndShiftSDet")) {
			refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
			$.UIbyID("dlgEditEndShiftSDet").close();
			$.UIbyID("dlgEditEndShiftSDet").destroy();
		}
	}
}

function fnDelEndShiftDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype) {

	sap.ui.commons.MessageBox.show(
		oLng_Monitor.getText("Monitor_DeleteSelectedReason"),
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_Monitor.getText("Monitor_Confirm"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdMov + "NextGen/Confirm/AddUpdDelDEndShiftDetQR" +
					"&Param.1=" + fnGetCellValue("EndShiftSDetTab", "PLANT") +
					"&Param.2=" + fnGetCellValue("EndShiftSDetTab", "IDLINE") +
					"&Param.3=" + fnGetCellValue("EndShiftSDetTab", "DATE_SHIFT").substring(0,10) +
					"&Param.4=" + fnGetCellValue("EndShiftSDetTab", "SHIFT") +
					"&Param.5=" + fnGetCellValue("EndShiftSDetTab", "OPEID") +
					"&Param.6=" + fnGetCellValue("EndShiftSDetTab", "ORDER") +
					"&Param.7=" + fnGetCellValue("EndShiftSDetTab", "POPER") +
					"&Param.8=" + fnGetCellValue("EndShiftSDetTab", "ROWPROG") +
					"&Param.9=" + fnGetCellValue("EndShiftSDetTab", "SREASON") +
					"&Param.10=" + fnGetCellValue("EndShiftSDetTab", "STYPE") +
					"&Param.11=" + fnGetCellValue("EndShiftSDetTab", "QTA_ORI") +
					"&Param.12=" + fnGetCellValue("EndShiftSDetTab", "QTA_INS") +
					"&Param.13=" + "DEL" +
					"&Param.14=" + sLanguage;

				var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
				if (ret.code < 0) {
					sap.ui.commons.MessageBox.show(
						ret.message,
						sap.ui.commons.MessageBox.Icon.ERROR,
						oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
						sap.ui.commons.MessageBox.Action.OK
					);
					return;
				}

				refreshTabEndShiftSDet(pPlant, pIdLine, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pStype);
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
} // fnDelEndShiftDet



function fnSaveEndShiftSoul(bEdit, pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {

	var sMode = bEdit ? "UPD" : "ADD";


	var MatSoul = $.UIbyID("txtEndShiftSoulCode").getValue();
	var qtaSoul = parseFloat($.UIbyID("txtEditEndShiftQTA_Soul").getValue() === "" ? "0" : $.UIbyID("txtEditEndShiftQTA_Soul").getValue());

	if (qtaSoul === 0) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertQuantity"),
			sap.ui.commons.MessageBox.Icon.WARNING,
			oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);

		return;
	}

	if (MatSoul === "") {
		sap.ui.commons.MessageBox.show(
			"Selezionare un componente anima",
			sap.ui.commons.MessageBox.Icon.WARNING,
			oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);

		return;
	}

	var qexe = dataProdMov + "NextGen/Confirm/AddUpdDEndShiftComponentSQ" +
		"&Param.1=" + pPlant +
		"&Param.3=" + pDateShift +
		"&Param.4=" + pShift +
		"&Param.5=" + pOpeId +
		"&Param.6=" + pOrder +
		"&Param.7=" + pPoper +
		"&Param.8=" + pRowProg +
		"&Param.9=" + MatSoul +
		"&Param.10=" + qtaSoul +
		"&Param.14=" + sLanguage;

	var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
	if (ret.code < 0) {
		sap.ui.commons.MessageBox.show(
			ret.message,
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	} else {
		refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
		$.UIbyID("dlgEditEndShiftSoul").close();
		$.UIbyID("dlgEditEndShiftSoul").destroy();
		refreshTabEndDeclaration();
	}
}

function fnDelEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {

	sap.ui.commons.MessageBox.show(
		"Eliminare L'anima selezionata?", // oLng_Monitor.getText("Monitor_DeleteSelectedReason"),
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_Monitor.getText("Monitor_Confirm"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {

				var MatSoul = fnGetCellValue("SoulDetTab", "MATERIAL");

				var qexe = dataProdMov + "NextGen/Confirm/delDEndShiftComponentSQ" +
					"&Param.1=" + pPlant +
					"&Param.3=" + pDateShift +
					"&Param.4=" + pShift +
					"&Param.5=" + pOpeId +
					"&Param.6=" + pOrder +
					"&Param.7=" + pPoper +
					"&Param.8=" + pRowProg +
					"&Param.9=" + MatSoul +
					"&Param.14=" + sLanguage;

				var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
				if (ret.code < 0) {
					sap.ui.commons.MessageBox.show(
						ret.message,
						sap.ui.commons.MessageBox.Icon.ERROR,
						oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
						sap.ui.commons.MessageBox.Action.OK
					);
					return;
				}
				refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
				refreshTabEndDeclaration(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
} // fnDelEndShiftDet

function openResultDetail() {
	var RowIdx = $.UIbyID("EndShiftDeclarationTab").getSelectedIndex();
	var pPlant = fnGetCellValue("EndShiftDeclarationTab", "PLANT", RowIdx);
	var pShift = fnGetCellValue("EndShiftDeclarationTab", "SHIFT", RowIdx);
	var pOpeId = fnGetCellValue("EndShiftDeclarationTab", "OPEID", RowIdx);
	var pOrder = fnGetCellValue("EndShiftDeclarationTab", "ORDER", RowIdx);
	var pPoper = fnGetCellValue("EndShiftDeclarationTab", "POPER", RowIdx);
	var pRowProg = fnGetCellValue("EndShiftDeclarationTab", "ROWPROG", RowIdx);
	var pDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT",RowIdx).substring(0,10);

	if ($.UIbyID("dlgResultDet")) {
		$.UIbyID("dlgResultDet").destroy();
	}

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		autoClose: false,
		resizable: true,
		id: "dlgResultDet",
		maxWidth: "700px",
		maxHeight: "900px",
		title: "Dettaglio conferme inviate a Sap",
		showCloseButton: false
	});

	//Crea L'oggetto Tabella
	var oTable = UI5Utils.init_UI5_Table({
		id: "ResultDetTab",
		exportButton: false,
		properties: {
			//title: oLng_Monitor.getText("Monitor_ConfProdList"), //"Lista avanzamenti di produzione",
			visibleRowCount: 7,
			fixedColumnCount: 0,
			width: "100%", //"1285px",//
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function (oControlEvent) {

				if ($.UIbyID("ResultDetTab").getSelectedIndex() == -1) {
					$.UIbyID("btnASingleReverse").setEnabled(false);
					$.UIbyID("btnConfError").setEnabled(false);
				} else {


					if ( fnGetCellValue("ResultDetTab", "ERROR") === "1" ){
							$.UIbyID("btnConfError").setEnabled(true);
						$.UIbyID("btnASingleReverse").setEnabled(false);
							}
					else {
						$.UIbyID("btnConfError").setEnabled(false);
						$.UIbyID("btnASingleReverse").setEnabled(true);
					}
				}
			},
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
						text: "Tutti", //oLng_Monitor.getText("Monitor_Add"),
						id: "btnAllReverse",
						icon: 'sap-icon://undo',
						enabled: true,
						press: function () {
							fnAskReverseAll(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
						}
					}),
					new sap.ui.commons.Button({
						text: "Singolo", //oLng_Monitor.getText("Monitor_Modify"),
						id: "btnASingleReverse",
						icon: 'sap-icon://redo',
						enabled: false,
						press: function () {
							var RowIdx = $.UIbyID("ResultDetTab").getSelectedIndex();
							fnAskReverseSingle(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper,
								pRowProg, fnGetCellValue("ResultDetTab", "MII_GUID", RowIdx),
								fnGetCellValue("ResultDetTab", "DELETED", RowIdx));
						}
					}),
					new sap.ui.commons.Button({
						text: "Vis. Errori", //oLng_Monitor.getText("Monitor_Modify"),
						id: "btnConfError",
						icon: 'sap-icon://status-error',
						enabled: false,
						press: function () {
							var RowIdx = $.UIbyID("ResultDetTab").getSelectedIndex();
							openConfProdErrorDet(fnGetCellValue("ResultDetTab", "PLANT",RowIdx), fnGetCellValue("ResultDetTab", "SAP_GUID",RowIdx));
						}
					})
				],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabEndShiftResult(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
						}
					})]
			})
		},
		columns: [
			{
				Field: "STATE",
				label: oLng_Monitor.getText("Monitor_ICON"), //"Stato",
				//tooltipCol: "{statusToolTip}",
				iconColor: "{ICOLOR}",
				properties: {
					width: "70px",
					height: "30px",
					autoResizable: false,
				},
				template: {
					type: "coreIcon",
					textAlign: "Center"
				}
			},
			{
				Field: "TYPE_DESC",
				label: "Tipo", // oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "90px",
					//resizable : false,
					visible: true,
					flexible: false
				}
					},
			{
				Field: "DATE_UPD",
				label: "Data/Ora", //oLng_Monitor.getText("Monitor_OrigQuantity"),
				properties: {
					width: "120px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Right"
				}
					},
			{
				Field: "REASON",
				label: "Causale", //oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "110px",
					//resizable : false,
					visible: true,
					flexible: false
				}
					},
			{
				Field: "MII_GUID",
				label: "MII GUID", //oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "120px",
					//resizable : false,
					visible: true,
					flexible: false
				}
					},
			{
				Field: "SAP_GUID",
				label: "SAP GUID", //oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "150px",
					//resizable : false,
					visible: true,
					flexible: false
				}
					}
					]
	});

	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");

	var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
		content: [
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Order") + ": " + pOrder,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({
				text: oLng_Monitor.getText("Monitor_Poper") + ": " + pPoper,
				layoutData: new sap.ui.layout.form.GridElementData({
					hCells: "2"
				})
			}),
			new sap.ui.commons.Label({}),
			oTable
		],
		width: "100%"
	});

	oEdtDlg.addContent(oVerticalLayout);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"),
		press: function () {
			//refreshFunc();
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));

	refreshTabEndShiftResult(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
	oEdtDlg.open();
} //End Function openResultDetail

function refreshTabEndShiftResult(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {

	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Sap/NextGen/To/getEndShiftResultSQ" +
			"&Param.1=" + pPlant +
			"&Param.2=" + pDateShift +
			"&Param.3=" + pShift +
			"&Param.4=" + pOpeId +
			"&Param.5=" + pOrder +
			"&Param.6=" + pPoper +
			"&Param.7=" + pRowProg,
		dataType: "xml"
	};

	$.UIbyID("ResultDetTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("ResultDetTab").setBusy(true);
			// carica il model della tabella
			$.UIbyID("ResultDetTab").getModel().setData(data);
		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			// remove busy indicator
			$.UIbyID("ResultDetTab").setBusy(false);
		});
	$.UIbyID("ResultDetTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
} //refreshTabEndShiftResult

function fnAskReverseAll(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {
	sap.ui.commons.MessageBox.show(
		"Stornare tutte le conferme?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		"Richiesta conferma", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'NO') {
				return;
			} else {
				fnReverseAll(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
			}
		},
		sap.ui.commons.MessageBox.Action.NO
	);
} //fnAskReverseAll

function fnReverseAll(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg) {
	var sDateFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Sap/NextGen/To/reverseAllEndShiftQR" +
			"&Param.1=" + pPlant +
			"&Param.2=" + sDateFrom +
			"&Param.3=" + pShift +
			"&Param.4=" + pOpeId +
			"&Param.5=" + pOrder +
			"&Param.6=" + pPoper +
			"&Param.7=" + pRowProg +
			"&Param.8=" + sLanguage,
		dataType: "xml"
	};


	$.UIbyID("ResultDetTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("ResultDetTab").setBusy(true);
			// carica il model della tabella

			//$.UIbyID("EndShiftDeclarationTab").getModel().setData(data);
			sap.ui.commons.MessageBox.alert("Richieste di storno inviate a sap"); //Gestire il risultato elaborazione
			refreshTabEndShiftResult(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			$.UIbyID("ResultDetTab").setBusy(false);
			refreshTabEndDeclaration();
		});

}

function fnAskReverseSingle(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pMIIGuid, pDeleted) {

	if (pDeleted === "1") {
		sap.ui.commons.MessageBox.show(
			"La conferma selezionata risulta già stornata.",
			sap.ui.commons.MessageBox.Icon.ERROR,
			"Errore operazione",

			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	sap.ui.commons.MessageBox.show(
		"Stornare la conferma selezionata?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		"Richiesta conferma", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'NO') {
				return;
			} else {
				fnReverseSingle(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pMIIGuid);
			}
		},
		sap.ui.commons.MessageBox.Action.NO
	);
} //fnAskReverseSingle

function fnReverseSingle(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg, pMIIGuid) {
	var sDateFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Sap/NextGen/To/reverseSingleEndShiftQR" +
			"&Param.1=" + pPlant +
			"&Param.2=" + sDateFrom +
			"&Param.3=" + pShift +
			"&Param.4=" + pOpeId +
			"&Param.5=" + pOrder +
			"&Param.6=" + pPoper +
			"&Param.7=" + pRowProg +
			"&Param.8=" + pMIIGuid +
			"&Param.10=" + sLanguage,
		dataType: "xml"
	};


	$.UIbyID("ResultDetTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("ResultDetTab").setBusy(true);
			// carica il model della tabella

			//$.UIbyID("EndShiftDeclarationTab").getModel().setData(data);
			sap.ui.commons.MessageBox.alert("Richiesta di storno inviata a sap"); //Gestire il risultato elaborazione
			refreshTabEndShiftResult(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);

		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			$.UIbyID("ResultDetTab").setBusy(false);
			refreshTabEndDeclaration();
		});

} //fnReverseSingle


function EditedCellReason(oEvt) {

	var RowIdx = oEvt.getSource().oParent.getIndex();
	var tabPath = "";
	var fieldName = oEvt.oSource.mBindingInfos.value.parts[0].path; //Recupera il nome del campo editato
	var val = oEvt.getParameter('value');
	var newval = val;

	newval = val.replace(/[^.\d]/g, '');

	try {
		tabPath = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
	} catch (err) {
		tabPath = oEvt.oSource.oBindingContexts.undefined.sPath;
	}
	console.debug("PathModel:" + tabPath);
	$.UIbyID("EndShiftSDetTab").getModel().setProperty(tabPath + "/" + fieldName, newval);

	UpdateRowReason(RowIdx);
} //EditedCellReason

function EditedCellSoul(oEvt) {

	var RowIdx = oEvt.getSource().oParent.getIndex();
	var tabPath = "";
	var fieldName = oEvt.oSource.mBindingInfos.value.parts[0].path; //Recupera il nome del campo editato
	var val = oEvt.getParameter('value');
	var newval = val;

	newval = val.replace(/[^.\d]/g, '');

	try {
		tabPath = oEvt.oSource.oPropagatedProperties.oBindingContexts.undefined.sPath;
	} catch (err) {
		tabPath = oEvt.oSource.oBindingContexts.undefined.sPath;
	}
	console.debug("PathModel:" + tabPath);
	$.UIbyID("SoulDetTab").getModel().setProperty(tabPath + "/" + fieldName, newval);

	UpdateRowSoul(RowIdx);
} //EditedCellSoul

function UpdateRowReason(RowIdx) {

	var pPlant = fnGetCellValue("EndShiftSDetTab", "PLANT", RowIdx);
	var sDateShift = fnGetCellValue("EndShiftSDetTab", "DATE_SHIFT",RowIdx).substring(0,10);
	var pShift = fnGetCellValue("EndShiftSDetTab", "SHIFT", RowIdx);
	var pOpeId = fnGetCellValue("EndShiftSDetTab", "OPEID", RowIdx);
	var pOrder = fnGetCellValue("EndShiftSDetTab", "ORDER", RowIdx);
	var pPoper = fnGetCellValue("EndShiftSDetTab", "POPER", RowIdx);
	var pRowProg = fnGetCellValue("EndShiftSDetTab", "ROWPROG", RowIdx);
	var pStype = fnGetCellValue("EndShiftSDetTab", "STYPE", RowIdx);
	var pReason = fnGetCellValue("EndShiftSDetTab", "SREASON", RowIdx);
	var qtaOri = fnGetCellValue("EndShiftSDetTab", "QTA_ORI", RowIdx);
	var qtaIns = fnGetCellValue("EndShiftSDetTab", "QTA_INS", RowIdx);
	//var pDateShift = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));

	fnSaveEndShiftDetail("UPD", pPlant, "", sDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg,
		pStype, pReason, qtaOri, qtaIns);

} //UpdateRowReason

function UpdateRowSoul(RowIdx) {

	var pPlant = fnGetCellValue("SoulDetTab", "PLANT", RowIdx);
	//var sDateShift = fnGetCellValue("EndShiftDeclarationTab", "DATE_SHIFT", RowIdx);
	var pShift = fnGetCellValue("SoulDetTab", "SHIFT", RowIdx);
	var pOpeId = fnGetCellValue("SoulDetTab", "OPEID", RowIdx);
	var pOrder = fnGetCellValue("SoulDetTab", "ORDER", RowIdx);
	var pPoper = fnGetCellValue("SoulDetTab", "POPER", RowIdx);
	var pRowProg = fnGetCellValue("SoulDetTab", "ROWPROG", RowIdx);
	var pMaterial = fnGetCellValue("SoulDetTab", "MATERIAL", RowIdx);
	var qtaSoul = fnGetCellValue("SoulDetTab", "QTA_INS", RowIdx);
	var pDateShift = fnGetCellValue("SoulDetTab", "DATE_SHIFT",RowIdx).substring(0,10);

	var qexe = dataProdMov + "NextGen/Confirm/AddUpdDEndShiftComponentSQ" +
		"&Param.1=" + pPlant +
		"&Param.3=" + pDateShift +
		"&Param.4=" + pShift +
		"&Param.5=" + pOpeId +
		"&Param.6=" + pOrder +
		"&Param.7=" + pPoper +
		"&Param.8=" + pRowProg +
		"&Param.9=" + pMaterial +
		"&Param.10=" + qtaSoul +
		"&Param.14=" + sLanguage;

	var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
	if (ret.code < 0) {
		sap.ui.commons.MessageBox.show(
			ret.message,
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	} else {
		refreshTabEndShiftSoul(pPlant, pDateShift, pShift, pOpeId, pOrder, pPoper, pRowProg);
		refreshTabEndDeclaration();
	}

} //UpdateRowSoul
