//**************************************************************************************
//Title:  Errore conferme produzione
//Author: Bruno Rosati
//Date:   04/08/2017
//Vers:   1.0
//**************************************************************************************
// Script per dettaglio Errore conferme produzione

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});

function openConfProdErrorDet(sPlant, sSAP_GUID) {
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgConfProdErrDet",
        maxWidth: "1200px",
        maxHeight: "900px",
        //minHeight: "900px",
        title: oLng_Monitor.getText("Monitor_ErrorsDetail"), //"Dettaglio degli errori",
        showCloseButton: false
    });
    
    var oConfProdErrTable = createTabErrDet(sPlant, sSAP_GUID);
    oEdtDlg.addContent(oConfProdErrTable);    
	
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy()
        }
    }));
	oEdtDlg.open();
}

// Function che crea la tabella con gli errori e ritorna l'oggetto oTable
function createTabErrDet(sPlant, sSAP_GUID) {
    
    var iTabVisibleRowCount = 7;
   
   //Crea L'oggetto Tabella Errori
    var oTable = UI5Utils.init_UI5_Table({
        id: "ConfProdErrTab",
        properties: {
            //title: oLng_Monitor.getText("Monitor_ContentDetails"), //"Dettaglio del contenuto",
            visibleRowCount: iTabVisibleRowCount,
            //fixedColumnCount: 1,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function(oControlEvent){},
            toolbar: new sap.ui.commons.Toolbar({
				items: [],
                rightItems: [
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabErrDet(sPlant, sSAP_GUID);
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "MESSAGE",
				label: oLng_Monitor.getText("Monitor_Message"), //"Messaggio",
				properties: {
					width: "350px"
				}
			},
            {
				Field: "DIRECTIONICON",
				label: oLng_Monitor.getText("Monitor_ICON"), //"Stato",
				tooltipCol: "{DIRECTION_TEXT}",
				iconColor: "{DIRECTIONICONCOLOR}",
				properties: {
					width: "70px",
					height: "30px",
					autoResizable: false,
                    visible: false
				},
				template: {
					type: "coreIcon",
					textAlign: "Center"
				}
			},
            {
				Field: "GUID_TO", 
				label: oLng_Monitor.getText("Monitor_MII_GUID"), //"MII GUID",
				properties: {
					width: "150px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "GUID_FROM", 
				label: oLng_Monitor.getText("Monitor_SAP_GUID"), //"SAP GUID",
				properties: {
					width: "280px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
                Field: "COUNTER",
				label: oLng_Monitor.getText("Monitor_Counter"), //"Contatore",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
                Field: "TYPE",
				label: oLng_Monitor.getText("Monitor_SAPmessageType"), //"Tipo messaggio SAP",
				properties: {
					width: "160px",
                    flexible : false,
                    visible: false
				}
            },
            {
                Field: "ID",
				label: oLng_Monitor.getText("Monitor_SAPmessageID"), //"ID messaggio SAP",
				properties: {
					width: "160px",
                    flexible : false,
                    visible: false
				}
            },
            {
                Field: "NUMBER",
				label: oLng_Monitor.getText("Monitor_SAPmessageNumber"), //"Numero messaggio SAP",
				properties: {
					width: "180px",
                    flexible : false
				}
            },
            {
                Field: "DATAORA",
				label: oLng_Monitor.getText("Monitor_DateTime"), //"Data/Ora",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            }
        ]      
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
	
    refreshTabErrDet(sPlant, sSAP_GUID);
	
	return oTable;
}

// Aggiorna la tabella Errori
function refreshTabErrDet(sPlant, sSAP_GUID) {
    var qexe = QService + dataProdSap;
    qexe += "From/getConfProdErrMessSQ&Param.1=" + sPlant + "&Param.2=" + sSAP_GUID;
    $.UIbyID("ConfProdErrTab").getModel().loadData(qexe);
}