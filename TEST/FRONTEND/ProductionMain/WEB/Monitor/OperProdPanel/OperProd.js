/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor Conferme Produzione
//Author: Bruno Rosati
//Date:   15/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor Conferme Produzione

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdMon = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";
var dataProdSap = "Content-Type=text/XML&QueryTemplate=ProductionMain/Sap/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Confirm/ConfProd/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();


// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});


jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/Monitor/OperProdPanel/fn_OperProd",
        "/XMII/CM/ProductionMain/MasterData/Operators_fn/fn_operatorTools"
    ],
    function () {
        $(document).ready(function () {
            
            var oLblPlant = new sap.ui.commons.Label().setText(oLng_Monitor.getText("Monitor_FilterDivision")); //Filtro divisioni
            
            //Escape per gestione parametro plant da GET/POST
            var qexe = dataProdComm + "getUserPlantQR";
            var sUserPlant = fnGetAjaxVal(qexe,["plant"],false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }
            
            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: oLng_Monitor.getText("Monitor_Plant"),
                change: function (oEvent) {                    
                    $.UIbyID("filtShift").setSelectedKey("");
                    $.UIbyID("filtShift").getModel().loadData(
                        QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
                    );
                    $.UIbyID("filtLine").setSelectedKey("");
                    $.UIbyID("filtLine").getModel().loadData(
                        QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
                    );
                    $.UIbyID("filtTypeConf").setSelectedKey("");
                    $.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
                    refreshTabOpeProd();
                }
            });
            
            //Setto il default come chiave selezionata
            if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}' ) {
                $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            }
            
            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "PLANT");
            oItemPlant.bindProperty("text", "NAME1");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsByUserQR"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);
            
            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant
                ]
            });
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
                content: [
                    oSeparator,
                    createTabOpeProd()
				],
                width: "100%"
            });
            
            oVerticalLayout.placeAt("MasterCont");
            $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            refreshTabOpeProd();
            $("#splash-screen").hide(); //nasconde l'icona di loading
        });
        
        
    }
);