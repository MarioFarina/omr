/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:
//Author:
//Date:
//Vers:   1.0
//**************************************************************************************

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var tmFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "HH:mm"
});

//data odierna
var oCurrentDateTime = new Date();
var year = oCurrentDateTime.getUTCFullYear();
var month = oCurrentDateTime.getUTCMonth() + 1;
if( month.toString().length == 1 ) month = "0" + month;
var day = oCurrentDateTime.getUTCDate();
var sCurrentDate = year + "-" + month+ "-" + day;
var sCurrentTime = oCurrentDateTime.getHours() + ":" + oCurrentDateTime.getMinutes() + ":00";

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabDataMatrix() {

    var oDtCurrentDateFrom = new sap.ui.commons.DatePicker('dtCurrentDateFrom');
		oDtCurrentDateFrom.setYyyymmdd(sCurrentDate);
		oDtCurrentDateFrom.setLocale("it");

		var oDtCurrentDateTo = new sap.ui.commons.DatePicker('dtCurrentDateTo');
		oDtCurrentDateTo.setYyyymmdd(sCurrentDate);
		oDtCurrentDateTo.setLocale("it");

    var serialFilter = new sap.ui.commons.TextField('filtSerial', {
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabDataMatrix();
				}
    });

    //filtro Linea
    var oCmbLineFilter = new sap.ui.commons.ComboBox("filtLine", {
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabDataMatrix();
        }
    });

    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    oItemLine.bindProperty("text", {
        parts: [
            {path: 'IDLINE'},
            {path: 'LINETXT'}
        ],
        formatter: function(sReasId,sReasTxt){
            return sReasId + ' - ' + sReasTxt;
        }
    });

    oCmbLineFilter.bindItems("/Rowset/Row", oItemLine);
    var oLineFilter = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1" + $.UIbyID("filtPlant").getSelectedKey());
    }
    oCmbLineFilter.setModel(oLineFilter);

    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "DataMatrixTab",
        properties: {
            title: "Data matrix", //"Gestione Fermi",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Label({
                        id: "lblRecordNumber",
                        text: oLng_Monitor.getText("Monitor_RecordNumber"), //"N. record"
                        textAlign: "Right"
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabDataMatrix();
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Export"), //"Esporta"
                        id:   "btnExportStop",
                        tooltip: oLng_Monitor.getText("Monitor_ExportInCSV"), //"Esporta i dati estratti in formato .csv"
                        enabled: true,
                        press: function (){
                            exportTableToCSV("DataMatrixTab","DataTable");
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: "Da Data" //Da Data Turno
                    }),
                    oDtCurrentDateFrom,
                    new sap.ui.commons.Label({
                        text: "A Data"
                    }),
                    oDtCurrentDateTo,
                    new sap.ui.commons.Label({
                        text: oLng_Monitor.getText("Monitor_Line") //Linea
                    }),
                    oCmbLineFilter,
										new sap.ui.commons.Label({
                        text: "Data Matrix"
                    }),
										serialFilter,
                    new sap.ui.commons.Button({
                        icon: "sap-icon://filter",
                        enabled: false,
                        id:   "delFilter",
                        press: function () {
                            $.UIbyID("filtLine").setSelectedKey("");
														$.UIbyID("filtSerial").setValue("");
                            $.UIbyID("delFilter").setEnabled(false);
                            refreshTabDataMatrix();
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "IDLINE",
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    //visible: false,
                    flexible : false
				}
            },
            {
				Field: "LINETXT",
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "150px",
                    flexible : false
				}
            },
            {
				Field: "SERIAL",
                label: "Data Matrix", //"Macchina",
				properties: {
					width: "160px"
				}
            },
            {
				Field: "MATERIAL",
				label: "Materiale", //"Macchina",
				properties: {
					width: "160px",
                    flexible : false
				}
            },
						{
		 	 Field: "DESC",
		 	 label: "Descrizione", //"Turno",
		 	 properties: {
		 		 width: "160px",
		 							 //resizable : false,
		 							 flexible : false
		 	 }
		 			 },
            {
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT",
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false
				}
            }
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}

function refreshTabDataMatrix() {

    var sDateShiftFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtCurrentDateFrom").getYyyymmdd()));
		var sDateShiftTo = dtFormat.format(dtSelect.parse($.UIbyID("dtCurrentDateTo").getYyyymmdd()));
    var sPlant = "";
		var serial = $.UIbyID("filtSerial").getValue();

	if ($.UIbyID("filtPlant").getSelectedKey() === ""){
		sPlant = $('#plant').val();
	}
	else{
		sPlant = $.UIbyID("filtPlant").getSelectedKey();
	}

	var qParams = {
		data: "ProductionMain/Rintracciabilita/getDataMatrixQR" +
		"&Param.1=" + sPlant +
		"&Param.2=" + sDateShiftFrom +
		"&Param.3=" + sDateShiftTo +
		"&Param.4=" + $.UIbyID("filtLine").getSelectedKey() +
		"&Param.5=" + serial,
		dataType: "xml"
	};
  $.UIbyID("DataMatrixTab").setBusy(true);
	UI5Utils.getDataModel(qParams).done(function (data) {
		$.UIbyID("DataMatrixTab").setBusy(true);
		// carica il model della tabella
		$.UIbyID("DataMatrixTab").getModel().setData(data);
	}).fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
  }).always(function () {
		$.UIbyID("DataMatrixTab").setBusy(false);
        var rowsetObject = $.UIbyID("DataMatrixTab").getModel().getObject("/Rowset/0");
        if(rowsetObject) {
            var iRecNumber = rowsetObject.childNodes.length - 1;
            $.UIbyID("lblRecordNumber").setText(oLng_Monitor.getText("Monitor_RecordNumber") /*"N. record" */ + " " + iRecNumber);
        }
	});
    $.UIbyID("DataMatrixTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}
