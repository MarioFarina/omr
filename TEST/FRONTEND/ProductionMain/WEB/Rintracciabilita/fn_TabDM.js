/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità - Tab1 Storico DataMatrix #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

//Creazione Tabella
function createTabDM() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "IDTabDM", //#Personalizzare IDTab
        properties: {
            title: oLng_res.getText("res_TabDM"),
            visibleRowCount: 10,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            
            //Abilito o disalibilito i pulsanti se è selezionata la riga
            rowSelectionChange: function (oControlEvent) {
                if ($.UIbyID("IDTabDM").getSelectedIndex() == -1) {
                    $.UIbyID("btnDetailDM").setEnabled(false);
                } else {
                    $.UIbyID("btnDetailDM").setEnabled(true);
                }
            }
            
        },
        exportButton: true,

        //Creo i pulsanti della toolbar
        toolbarItems: [
                new sap.ui.commons.Button({
                text: oLng_res.getText("res_DetailDM"),
                id: "btnDetailDM",
                icon: 'sap-icon://detail-view',
                tooltip: oLng_res.getText("res_DetailDMTooltip"),
                enabled: false,
                press: function () {
                    detailDM();
                }
            })
        ],

        //Creo le colonne della tabella
        columns: [
            //#Personalizzare i campi
            {
                Field: "IDLINE",
                label: oLng_res.getText("res_Line"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "dateshift",
                label: oLng_res.getText("res_Date"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template: {
                    type: "Date",
                }
            },
            {
                Field: "dshift",
                label: oLng_res.getText("res_Shift"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "TYPEDESC",
                label: oLng_res.getText("res_Type"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "serial",
                id: "IDSerial",
                label: oLng_res.getText("res_DataMatrix"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "MATERIAL",
                label: oLng_res.getText("res_Material"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "DESC",
                label: oLng_res.getText("res_DescMaterial"),
                properties: {
                    width: "50px",
                    visible: true
                }
            }

        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}

function detailDM() {
    resetFilter();
    $.UIbyID("IDDataMatrix").setValue(fnGetCellValue("IDTabDM", "serial"));
    searchResult();
    $.UIbyID("IDTabDM").setSelectedIndex(-1);
}