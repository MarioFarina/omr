/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

//Leggo la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

//Setto il percorso del file con le traduzioni
var oLng_res = jQuery.sap.resources({ //#Personalizzare oLng_res
    url: "/XMII/CM/ProductionMain/res/Rintracciabilita.i18n.properties", //#Personalizzare FileName
    locale: sCurrentLocale
});

//Definizione variabili globali
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";

//Carico le librerie
//param1: array di librerie
//param2: function che instanzia gli oggetti
Libraries.load(
    [
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/res/fn_commonTools",
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_Rintracciabilita", //#Personalizzare Foldername/fn_FileName
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_TabDM",
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_TabLine",
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_TabAlarm",
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_TabUdC",
        "/XMII/CM/ProductionMain/Rintracciabilita/fn_TabFermi",
    ],

    function () {
        $(document).ready(function () {
            $("#splash-screen").hide(); //nasconde l'icona di loading

        });

        var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
            content: [
                    createToolBarPage(),
					createForm(),
                    createTabStrip(),
				]
        });

        oVerticalLayout.placeAt("MasterCont");
        $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
    }
);