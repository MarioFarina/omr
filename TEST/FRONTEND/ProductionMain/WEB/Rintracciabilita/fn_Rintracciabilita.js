/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

var QService = "/XMII/Illuminator?";
var dataLoad = "Content-Type=text/XML&QueryTemplate=ProductionMain/Rintracciabilita";

function createForm() {
    var oLayout = new sap.ui.layout.form.GridLayout({
        singleColumn: false
    });

    //Creo la combo linee valorizzata #Personalizzare query
    var oCmbLines = fnCreateCombo("IDCmbLines", "IDLINE", "LINETXT", QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());

    //Creo la combo Turni
    var oCmbShifts = fnCreateCombo("IDCmbShifts", "SHIFT", "SHNAME", QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());

    var oForm = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: oLng_res.getText("res_SelectOptions")
        }),
        width: "98%",
        layout: oLayout,
        formContainers: [
			new sap.ui.layout.form.FormContainer({
                formElements: [
					//#Personalizzare Nuova riga campo input
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_res.getText("res_DataMatrix"),
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
							new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "4"
                                }),
                                editable: true,
                                maxLength: 40, //#Personalizzare lunghezza campo
                                id: "IDDataMatrix"
                            }),
                            new sap.ui.commons.Label({
                                text: oLng_res.getText("res_UdC"),
                                textAlign: "Right",
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                })
                            }),
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                }),
                                editable: true,
                                maxLength: 20, //#Personalizzare lunghezza campo
                                id: "IDUdC"
                            }),
						]
                    }), //End riga
					//#Personalizzare Nuova riga campo input
					new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_res.getText("res_Line"),
                            textAlign: "Right",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
							oCmbLines,
							new sap.ui.commons.Label({
                                text: oLng_res.getText("res_Date"),
                                textAlign: "Right",
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                })
                            }),
							new sap.ui.commons.DatePicker({
                                locale: "it",
                                yyyymmdd: "yyyyMMdd",
                                id: "IDDate"
                            }),

							new sap.ui.commons.Label({
                                text: oLng_res.getText("res_Shift"),
                                textAlign: "Right",
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                })
                            }),
							oCmbShifts
						]
                    }), //End riga

					//#Personalizzare Nuova riga Button
					new sap.ui.layout.form.FormElement({
                        //ho messo una label vuota solo per allineare il bottone sulla seconda colonna
                        label: new sap.ui.commons.Label({
                            textAlign: "Right",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
							new sap.ui.commons.Button({
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                }),
                                text: oLng_res.getText("res_Search"),
                                id: "btnSearch",
                                icon: 'sap-icon://search',
                                enabled: true,
                                press: function () {
                                    searchResult();
                                }
                            }),
							new sap.ui.commons.Button({
                                layoutData: new sap.ui.layout.form.GridElementData({
                                    hCells: "2"
                                }),
                                text: oLng_res.getText("res_ResetFilter"),
                                id: "btnReset",
                                icon: 'sap-icon://delete',
                                enabled: true,
                                press: function () {
                                    resetFilter();
                                }
                            })
						]
                    }) //End riga


				]
            })
		]
    });
    return oForm;
}


//Funzione per creare combobox
function fnCreateCombo(id, key, text, qexe) {
    var oModel = new sap.ui.model.xml.XMLModel();
    oModel.loadData(qexe);

    var oComboBox = new sap.ui.commons.ComboBox({
        id: id
    });
    oComboBox.setModel(oModel);
    var oListItem = new sap.ui.core.ListItem();
    oListItem.bindProperty("key", key);
    oListItem.bindProperty("text", text);
    oComboBox.bindItems("/Rowset/Row", oListItem);

    return oComboBox;
}

//Elimina filtri
function resetFilter() {
    $.UIbyID("IDDataMatrix").setValue("");
    $.UIbyID("IDCmbLines").setSelectedKey("");
    $.UIbyID("IDDate").setValue("");
    $.UIbyID("IDCmbShifts").setSelectedKey("");
    $.UIbyID("IDUdC").setValue("");
}

//Cerca
function searchResult() {
    /*$.UIbyID("IDDataMatrix").setValue("");
    $.UIbyID("IDCmbLines").setSelectedKey("");
    $.UIbyID("IDDate").setValue("");
    $.UIbyID("IDCmbShifts").setSelectedKey("");*/

    $("#splash-screen").hide();
    //numero righe
    if ($.UIbyID("IDDataMatrix").getValue() !== "") {
        Params = "&Param.1=" + $.UIbyID("IDDataMatrix").getValue() + "&RowCount=200";
    } else {
        if ($.UIbyID("IDCmbLines").getSelectedKey() === "" || $.UIbyID("IDDate").getYyyymmdd() === "") {
            sap.ui.commons.MessageBox.alert(oLng_res.getText("res_ReqField"));
            return;
        } else {
            Params = "&Param.2=" + $.UIbyID("IDCmbLines").getSelectedKey() + "&Param.3=" + $.UIbyID("IDDate").getYyyymmdd() + "&RowCount=200";
        }
    }

    var qParamsTab = {
        data: "ProductionMain/Rintracciabilita/getTracDataQR" + Params,
        dataType: "xml"
    };

    $.UIbyID("IDTabDM").setBusy(true);
    UI5Utils.getDataModel(qParamsTab)
        // on success
        .done(function (data) {
            // carica il model della tabella
            $.UIbyID("IDTabDM").setBusy(true);

            var sLine = $(data.childNodes[0].childNodes[1].childNodes[1].childNodes[0]).text();
            var sDate = $(data.childNodes[0].childNodes[1].childNodes[1].childNodes[1]).text();
            var sParams = "&Param.1=" + sLine + "&Param.2=" + sDate;
            $.UIbyID("IDTabAlarm").getModel().loadData(QService + dataLoad + "/getAlarmListSQ" + sParams);
            $.UIbyID("IDTabLine").getModel().loadData(QService + dataLoad + "/getLineTrackSQ" + sParams);
            $.UIbyID("IDTabFermi").getModel().loadData(QService + dataLoad + "/GetStopByLineDateSQ" + sParams);
            if ($.UIbyID("IDDataMatrix").getValue() !== "") {
                $.UIbyID("IDTabUdC").getModel().loadData(QService + dataLoad + "/getUDCbySerialSQ&Param.1=" + $.UIbyID("IDDataMatrix").getValue());
            } else {}
            $.UIbyID("IDTabDM").getModel().setData(data);

        }) // End Done Function
        // on fail
        .fail(function () {
            sap.ui.commons.MessageBox.alert(oLng_res.getText("res_LoadERR", ['val1', 'val2']));
        })
        // always, either on success or fail
        .always(function () {
            // remove busy indicator
            $.UIbyID("IDTabDM").setBusy(false);
        });

}

function createTabStrip() {
    var oTabStrip = new sap.ui.commons.TabStrip("IDTabStrip");
    oTabStrip.attachClose(function (oEvent) {
        var oTabStrip = oEvent.oSource;
        oTabStrip.closeTab(oEvent.getParameter("index"));
    });

    //Tab1 Storico DataMatrix
    var oTableDM = createTabDM();
    var oLayoutDM = new sap.ui.commons.layout.MatrixLayout("IDLayDM", {
        columns: 1
    });
    oLayoutDM.createRow(oTableDM);
    oTabStrip.createTab(
        oLng_res.getText("res_TabDM"),
        oLayoutDM
    );

    //Tab2 Storico Linea
    var oTableLine = createTabLine();
    var oLayoutLine = new sap.ui.commons.layout.MatrixLayout("IDLayLine", {
        columns: 1
    });
    oLayoutLine.createRow(oTableLine);
    oTabStrip.createTab(
        oLng_res.getText("res_TabLine"),
        oLayoutLine
    );

    //Tab3 Storico Allarmi
    var oTableAlarm = createTabAlarm();
    var oLayoutAlarm = new sap.ui.commons.layout.MatrixLayout("IDLayAlarm", {
        columns: 1
    });
    oLayoutAlarm.createRow(oTableAlarm);
    oTabStrip.createTab(
        oLng_res.getText("res_TabAlarm"),
        oLayoutAlarm
    );

    //Tab4 Storico UdC
    var oTableUdC = createTabUdC();
    var oLayoutUdC = new sap.ui.commons.layout.MatrixLayout("IDLayUdC", {
        columns: 1
    });
    oLayoutUdC.createRow(oTableUdC);
    oTabStrip.createTab(
        oLng_res.getText("res_TabUdC"),
        oLayoutUdC
    );

    //Tab5 Storico Fermi
    var oTableFermi = createTabFermi();
    var oLayoutFermi = new sap.ui.commons.layout.MatrixLayout("IDLayFermi", {
        columns: 1
    });
    oLayoutFermi.createRow(oTableFermi);
    oTabStrip.createTab(
        oLng_res.getText("res_TabFermi"),
        oLayoutFermi
    );


    return oTabStrip;
}