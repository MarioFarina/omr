/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità - Tab3 Storico Allarmi #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

//Creazione Tabella
function createTabAlarm() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "IDTabAlarm", //#Personalizzare IDTab
        properties: {
            title: oLng_res.getText("res_TabAlarm"),
            visibleRowCount: 10,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive
        },
        exportButton: true,

        //Creo le colonne della tabella
        columns: [
            //#Personalizzare i campi
            {
                Field: "IDLINE",
                label: oLng_res.getText("res_Line"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "ALMTXT",
                label: oLng_res.getText("res_AlarmText"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "DATE_FROM",
                label: oLng_res.getText("res_DateFrom"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "DateTime",
                }
            },
            {
                Field: "DATE_TO",
                label: oLng_res.getText("res_DateTo"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "DateTime",
                }
            },
            {
                Field: "DATE_SHIFT",
                label: oLng_res.getText("res_DateShift"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "Date",
                }
            }

        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}