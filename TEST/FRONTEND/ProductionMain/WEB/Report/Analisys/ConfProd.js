/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var QPrefix = "Content-Type=text/XML&QueryTemplate=";
var icon16 = "/XMII/CM/Common/icons/16x16/";
var dateSelFrom = "";
var dateSelTo = "";
var dtFormat = "";
var dtSelect = "";

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
$(document).ready(function () {

	/*************************************************************/
	// Creo la tool bar
	/*************************************************************/
	dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
		pattern: "yyyyMMdd"
	});
	dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
		pattern: "yyyy-MM-dd"
	});
	var oToolbar1 = new sap.ui.commons.Toolbar("tb1",{width: "100%"});
	//oToolbar1.setDesign(sap.ui.commons.ToolbarDesign.Standard);

	// Crea la ComboBox per le divisioni
	var oLabelP = new sap.ui.commons.Label("lPlant",{
		text : "Divisione",
		tooltip : "Seleziona la divisione"
	});
	oToolbar1.addItem(oLabelP);

	var oModel = new sap.ui.model.xml.XMLModel();
	oModel.loadData(QService + dataCommon + "GetPlantsQR");
	oCmbPlant = new sap.ui.commons.ComboBox("cmbPlants", {selectedKey : "FLO"});
	oCmbPlant.setModel(oModel);
	//oCmbPlant.bindRows("/Rowset/Row");
	var oItemPlant = new sap.ui.core.ListItem();
	oItemPlant.bindProperty("text", "NAME1");
	oItemPlant.bindProperty("key", "PLANT");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);


	oCmbPlant.attachChange(function() {
		$("#Plant").val( $.UIbyID("cmbPlants").getSelectedKey());
		//window.oCmbRep.getModel().loadData(QService + dataRep + "getDepartmentsByPlant&Param.1=" + $("#Plant").val());
		//refreshTabCons();
	});
	oToolbar1.addItem(oCmbPlant);

	// creazione delle due DtPicker
	var oLabel = new sap.ui.commons.Label("lFrom",{
		text : "Da data:",
		tooltip : "Seleziona l'intervallo di date"
	});
	oToolbar1.addItem(oLabel);

	var yesterday = new Date();
	yesterday.setDate( yesterday.getDate() - 1 );
	var ToDay = new Date();

	var  year = yesterday.getUTCFullYear();
	var  month = yesterday.getUTCMonth() + 1;
	if( month.toString().length == 1 ) month = "0" + month;
	var  day = yesterday.getUTCDate();
	if( day.toString().length == 1 ) day = "0" + day;

	dateSelFrom = year + "-" + month+ "-" + day;

	var fromday = new Date(yesterday.getFullYear(), yesterday.getMonth() + 1, 0);

	year = ToDay.getUTCFullYear();
	month = ToDay.getUTCMonth() + 1;
	if( month.toString().length == 1 ) month = "0" + month;
	day = ToDay.getUTCDate();

	dateSelTo = year + "-" + month+ "-" + day;

	odtFrom = new sap.ui.commons.DatePicker('dtFrom');
	odtFrom.setYyyymmdd( dateSelFrom );
	odtFrom.setLocale("it"); // Try with "de" or "fr" instead!

	odtFrom.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
			}
			var selDate = this.getYyyymmdd();
			dateSelFrom = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
		}
	);
	oToolbar1.addItem(odtFrom);
	var oLabel2 = new sap.ui.commons.Label("lTo",{
		text : "a data:",
		tooltip : "Seleziona l'intervallo di date"
	});
	oToolbar1.addItem(oLabel2);
	odtTo = new sap.ui.commons.DatePicker('dtTo');
	odtTo.setYyyymmdd( dateSelTo );
	odtTo.setLocale("it"); // Try with "de" or "fr" instead!

	odtTo.attachChange(
		function(oEvent){
			if(oEvent.getParameter("invalidValue")){
				oEvent.oSource.setValueState(sap.ui.core.ValueState.Error);
			}else{
				oEvent.oSource.setValueState(sap.ui.core.ValueState.None);
			}
			var selDate = this.getYyyymmdd();
			dateSelTo = selDate.substr(0,4) + "-" + selDate.substr(4,2) + "-" + selDate.substr(6,2);
			//refreshTabAnalys();
		}
	);
	oToolbar1.addItem(odtTo);


	var oButton1 = new sap.ui.commons.Button("b11",{
		icon: "sap-icon://refresh",
		text : "Aggiorna",
		tooltip : "Aggiorna risultato",
		press: function() { renderTable();}

	});
	oToolbar1.addItem(oButton1);

	var oButton5 = new sap.ui.commons.Button("toogle",{
		icon: "sap-icon://resize",
		//		 text : "PNG",
		tooltip : "Mostra/Nascondi barra strumenti",
		press: function() { $(".pvtAxisContainer, .pvtVals").toggle();}
	});

	var oButton2 = new sap.ui.commons.Button("xls",{
		icon: "/XMII/CM/Common/icons/document/xls_16.png",
		//text : "XLS",
		tooltip : "Exporta in excel",
		press: function() { exportTo("xls");}
	});

	var oButton3 = new sap.ui.commons.Button("doc",{
		icon: "/XMII/CM/Common/icons/document/word_16.png",
		//	text : "CSV",
		tooltip : "Exporta in Word",
		press: function() { exportTo("doc");}
	});

	var oButton4 = new sap.ui.commons.Button("pdf",{
		icon: "/XMII/CM/Common/icons/document/pdf_16.png",
		//		 text : "PNG",
		tooltip : "Stampa in PDF",
		press: function() { exportTo("pdf");}
	});

	oToolbar1.addRightItem(oButton5);
	oToolbar1.addRightItem(oButton2);
	//oToolbar1.addRightItem(oButton3);
	//oToolbar1.addRightItem(oButton4);

	$("#splash-screen").hide();
	oToolbar1.placeAt("Toolbar");


	renderTable();
});

function renderTable(){
	var dateFormat = $.pivotUtilities.derivers.dateFormat;
	var sortAs =           $.pivotUtilities.sortAs;

	$("#splash-screen").show();
	var P1 = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var P2 = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
	var PivotPar = {
		PlaceAt: "#Analisys",
		Query: {
			Name: "ConfProdAnalisys1SQ",
			Path: QPrefix + "ProductionMain/Report/",
			Params: "&Param.1=" + $('#Plant').val() + "&Param.5=" + P1 + "&Param.6=" + P2 + "&RowCount=50000"
		},
		Pivot: {
			Rows: ["Operatore", "Dichiarazione"],
			Cols:["Giorno","Turno"],
			Vals:["Qtà Conf."],
			HiddenAttributes:["DATE_SHIFT","IDLINE","SHIFT","MATERIAL","MAKTX","DESC","ORDER","POPER","CICTXT","CDLID","CDLDESC","CESTA_TT","WTDUR","NRPNT",
												"DATARIO","OPENAME","DISEGNO","UDCNR","QTY","OPEID","UDCNR","EXTENDED_TEXT","TYPECONF","SREASID","QTY_PRO","PLANT","PZUDC",
												"DELETED","CONFIRMED","USERMOD","TYPEMOD","NRPRNT","UMDUR","MII_GUID","SAP_GUID","MII_GUID_ST","NOTEMOD","EXTENDED_TEXT",
												"TIME_ID_CALCULATED","UDC_STATE","EXID","QTYUDC","SREASID","LINETXT","TIME_ID","DATE_UPD","SREASTXT","ERROR","CURRENT_QTY","DATE_UPD"],
			DerivedAttributes:{
				"Linea": function(record) {return record.IDLINE;},
				"Turno": function(record) {return record.SHIFT;},
				"Materiale": function(record) {return record.MATERIAL;},
				"Desc.Materiale": function(record) {return record.DESC;},
				//"Divisione": function(record) {return record.NAME1;},
				"Commessa": function(record) {return Number(record.ORDER);},
				"Fase": function(record) {return record.POPER;},
				"Desc.Fase": function(record) {return record.CICTXT;},
				"CdL": function(record) {return record.CDLID;},
				"Desc.CdL": function(record) {return record.CDLDESC;},
				"CestaTT": function(record) {return record.CESTA_TT;},
				"Tempo (Mins)": function(record) {return record.WTDUR;},
				"N.Stampo": function(record) {return record.NRPRNT;},
				"Datario": function(record) {return record.DATARIO;},
				"Matricola": function(record) {return record.OPEID;},
				"Operatore": function(record) {return record.OPENAME;},
				"Disegno": function(record) {return record.DISEGNO;},
				"Stato UDC": function(record) {return record.UDC_STATE;},
				"Caus.Sosp.Scarto": function(record) {return record.SREASTXT;},
				"Cod.Sosp.Scarto": function(record) {return record.SREASID;},
				"Stato UDC": function(record) {return record.UDC_STATE;},
				"N.UDC": function(record) {return record.UDCNR;},
				"Dichiarazione": function(record) {return record.EXTENDED_TEXT;},
				"Qtà Conf.": function(record) {return record.QTY;},
				"Qtà UDC": function(record) {return record.QTYUDC;},
				"Anno":    dateFormat("DATE_SHIFT", "%y", true),
				"Mese":      dateFormat("DATE_SHIFT", "%m", true),
				"Data completa": dateFormat("DATE_SHIFT", "%d/%m/%y", true),
				"Giorno":        dateFormat("DATE_SHIFT", "%d", true),
				"Nome mese": dateFormat("DATE_SHIFT", "%n", true,["Gen","Feb","Mar","Apr", "Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic"]),
				"Nome mese est.": dateFormat("DATE_SHIFT", "%n", true,["Gennaio","Febbraio","Marzo","Aprile", "Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"]),
				"Nome giorno":   dateFormat("DATE_SHIFT", "%w", true,["Lun","Mar","Mer", "Gio","Ven","Sab","Dom"]),
				"Settimana":        dateFormat("DATE_SHIFT", "%k", true),
				"Settimana-Anno":        dateFormat("DATE_SHIFT", "%K", true)
			}
		}
	};

	renderTablePivot(PivotPar);

	$("#splash-screen").hide();

}

