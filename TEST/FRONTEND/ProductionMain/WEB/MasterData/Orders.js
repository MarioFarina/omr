/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Anagrafica commesse
//Author: Elena Andrini
//Date:   14/02/2017
//Vers:   1.0
//**************************************************************************************

//Leggo la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

//Definizione variabili globali
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";

//Carico le librerie
//param1: array di librerie
//param2: function che instanzia gli oggetti
Libraries.load(
    [
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/Orders/fn_orders"/*,
        "/XMII/CM/ProductionMain/res/fn_commonTools"*/
    ],

    function () {
        $(document).ready(function () {
            var oLblPlant = new sap.ui.commons.Label().setText(oLng_MasterData.getText("MasterData_Plant")); //Divisione

            //Escape per gestione parametro plant da GET/POST
            var qexe = dataProdComm + "getUserPlantQR";
            var sUserPlant = fnGetAjaxVal(qexe,["plant"],false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }
            
            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: oLng_MasterData.getText("MasterData_Plant"), //Divisione
                change: function (oEvent) {                    
                    refreshOrdersTab();
                }
            });
			
			//Setto il default come chiave selezionata
            if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}' ) {
                $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            }
            
            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "PLANT");
            oItemPlant.bindProperty("text", "NAME1");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsByUserQR"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);
            
            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant
                ]
            });
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
                content: [
                    oSeparator,
                    createOrdersTab()
				],
                width: "100%"
            });
            
            oVerticalLayout.placeAt("MasterCont");
            $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            refreshOrdersTab();
            $("#splash-screen").hide(); //nasconde l'icona di loading            
        });
    }
);