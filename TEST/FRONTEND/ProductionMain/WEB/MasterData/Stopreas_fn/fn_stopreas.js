var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Stopreasi
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Stoprease e ritorna l'oggetto oTable
function createTabStopreas() {
    
    // contenitore dati lista causali fermo
	/*
    var oModel2 = new sap.ui.model.xml.XMLModel()
	oModel2.loadData(QService + "ProductionMain/MasterData/Stopreas/GetStopreas.SQ");

	// Crea la ComboBox per i CID

	oCmbCID = new sap.ui.commons.ComboBox("cmbCID", {selectedKey : ""});
	oCmbCID.setModel(oModel2);		
	var oItemCID = new sap.ui.core.ListItem();
	oItemCID.bindProperty("text", "EMNAM");
	oItemCID.bindProperty("key", "PERNR");	
	oCmbCID.bindItems("/Rowset/Row", oItemCID);
    */
    
    //Crea L'oggetto Tabella Stoprease
    var oTable = UI5Utils.init_UI5_Table({
        id: "StopreasTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_StopreasList"), //"Lista micro causali fermo",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("StopreasTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModStopreas").setEnabled(false);
        			$.UIbyID("btnDelStopreas").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModStopreas").setEnabled(true);
                    $.UIbyID("btnDelStopreas").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddStopreas",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openStopreasEdit(false, "INSERT");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModStopreas",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openStopreasEdit(true, "UPDATE");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelStopreas",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelStopreas();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabStopreas();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1", // in Sigit è "PLNAME",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "80px",
                    visible: false
				}
            },
            {
				Field: "STTYPE_TXT",
				label: oLng_MasterData.getText("MasterData_MacroStopReasons"), //"Macro causali fermo",
				properties: {
					width: "50px"
				}
            },
            {
				Field: "STOPID",
				label: oLng_MasterData.getText("MasterData_MicroStopReasons"), //"Micro causali fermo",
				properties: {
					width: "50px"
				}
            },
            {
				Field: "REASTXT", 
				label: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "DESCR", 
				label: oLng_MasterData.getText("MasterData_ExtendedText"), //"Testo esteso",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "STTYPE", 
				label: oLng_MasterData.getText("MasterData_CodiceGruppoFermo"), //"STTYPE",
				properties: {
					width: "160px",
					visible: false
				}
            },
            {
				Field: "ENABLED",// in Sigit è "ACTIVE",
				label: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            }
        ],
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabStopreas();
    return oTable;
}

// Aggiorna la tabella Stopreas
function refreshTabStopreas() {
  if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("StopreasTab").getModel().loadData(
            QService + dataProdMD +
            "Stopreas/getStopreasByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("StopreasTab").getModel().loadData(
            QService + dataProdMD +
            "Stopreas/getStopreasByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("StopreasTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Stopreasi
function openStopreasEdit(bEdit, mode) {
    if (bEdit === undefined) bEdit = false;
    if (mode === undefined) mode = "";
    
   // contenitore dati lista divisioni
	var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        enabled: false,
        selectedKey : bEdit?fnGetCellValue("StopreasTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey()
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModel4 = new sap.ui.model.xml.XMLModel();
	oModel4.loadData(
        QService + dataProdMD +
        "Grstreas/getEnabledGrstreasByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
    );

	// Crea la ComboBox per le divisioni in input
	var oCmbStopReasonGroup = new sap.ui.commons.ComboBox({
        id: "oCmbStopReasonGroup", 
        selectedKey : bEdit?fnGetCellValue("StopreasTab", "STTYPE"):""
    });
	oCmbStopReasonGroup.setModel(oModel4);		
	var oItemScrabReasonInput = new sap.ui.core.ListItem();
    // oItemScrabReasonInput.bindProperty("text", "PLNAME");
	oItemScrabReasonInput.bindProperty("text", "REASTXT");
	oItemScrabReasonInput.bindProperty("key", "STTYPE");	
	oCmbStopReasonGroup.bindItems("/Rowset/Row", oItemScrabReasonInput);
    

    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgStopreas",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("StopreasTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyStopreas"):oLng_MasterData.getText("MasterData_NewStopreas"),
            /*"Modifica Stoprease":"Nuovo Stoprease",*/
            //icon: "/images/address.gif",
            tooltip: "Info Stoprease"//oLng_Gen.getText("PrPage_MsgTT_StopreasInfo")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MicroStopReasons"), //"Micro causali fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("StopreasTab", "STOPID"):"",
                                editable: !bEdit,
                                maxLength: 6,
                                id: "STOPID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("StopreasTab", "REASTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "REASTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("StopreasTab", "DESCR"):"",
                                editable: true,
                                maxLength: 60,
                               id: "DESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("StopreasTab", "ENABLED") == "0" ? false : true ) : true,
                                /*bEdit?(fnGetCellValue("StopreasTab", "ACTIVE") == "0" ? false : true ) : false,*/
                                id: "ENABLED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Group"), //"Gruppo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbStopReasonGroup
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveStopreas(mode);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveStopreas(mode ) 
{
    if ( mode === 'undefined' ) mode = "";

    var STOPID = $.UIbyID("STOPID").getValue(); 
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var REASTXT = $.UIbyID("REASTXT").getValue();
    var DESCR = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
//    var STTYPE = $.UIbyID("STTYPE").getValue();
    var STTYPE = $.UIbyID("oCmbStopReasonGroup").getSelectedKey();

    if ( STOPID === "" || plant === "" || STTYPE === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK);
        return;
    }

    var qexe = "";
    switch( mode ) {
        case "INSERT":
            qexe =dataProdMD +"Stopreas/addStopreasSQ";
            break;
        case "UPDATE":
            qexe =dataProdMD +"Stopreas/updStopreasSQ";
            break;
    };

    qexe += "&Param.1=" + plant +
                "&Param.2=" + STOPID +
                "&Param.3=" + REASTXT +
                "&Param.4=" + DESCR +
                "&Param.5=" + active +
                "&Param.6=" + STTYPE;
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_StopreasSaved"),
                         oLng_MasterData.getText("MasterData_StopreasSavedError")
                         /*"Stoprease salvato", "Errore salvataggio Stoprease"*/,
                         /*CONTROLLARE A COSA SERVE L'ULTIMO PARAMETRO, PASSATO FALSE*/
                         false);
    if (ret){
        $.UIbyID("dlgStopreas").close();
        $.UIbyID("dlgStopreas").destroy();
        refreshTabStopreas();
    }
}

function fnDelStopreas(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_StopreasDeleting"), //"Eliminare l'Stoprease selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "Stopreas/delStopreasSQ" +
                            "&Param.1=" + fnGetCellValue("StopreasTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("StopreasTab", "STOPID");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_StopreasDeleted"), //"Stoprease eliminato correttamente",
                    oLng_MasterData.getText("MasterData_StopreasDeletedError"), //"Errore in eliminazione Stoprease",
                    false
                );
                refreshTabStopreas();
                $.UIbyID("btnModStopreas").setEnabled(false);
                $.UIbyID("btnDelStopreas").setEnabled(false);
                 $.UIbyID("StopreasTab").setSelectedIndex(-1);
           }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}




