// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataProdTR = "Content-Type=text/XML&QueryTemplate=ProductionMain/Translate/";
var i = 0;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
jQuery.ajaxSetup({
	cache: false
});

Libraries.load(
	["/XMII/CM/Common/MII_core/UI5_utils",
	 "/XMII/CM/ProductionMain/MasterData/PlantCore_fn/fn_Reparts",
	 "/XMII/CM/ProductionMain/MasterData/PlantCore_fn/fn_Machines",
	 "/XMII/CM/ProductionMain/MasterData/PlantCore_fn/fn_Lines",
	 "/XMII/CM/ProductionMain/MasterData/PlantCore_fn/fn_Plants",
	 "/XMII/CM/ProductionMain/MasterData/PlantCore_fn/fn_Shifts"],
    function() {
        $(document).ready(function() {
			var oLblPlant = new sap.ui.commons.Label().setText(oLng_MasterData.getText("MasterData_Plant")); //Divisione

            //Escape per gestione parametro plant da GET/POST
            var qexe = dataProdComm + "getUserPlantQR";
            var sUserPlant = fnGetAjaxVal(qexe,["plant"],false);
            if ($('#plant').val() === '{plant}') {
                $('#plant').val(sUserPlant.plant);
            }

            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
                selectedKey: $('#plant').val(),
                tooltip: oLng_MasterData.getText("MasterData_Plant"), //Divisione
                change: function (oEvent) {
                    refreshShiftsTab();
                    refreshTabRep();
                    refreshTabLines();
                    //refreshTabMacchine();
                }
            });

			//Setto il default come chiave selezionata
            if ($.UIbyID("filtPlant").getSelectedKey() === '{plant}' ) {
                $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
            }

            var oItemPlant = new sap.ui.core.ListItem();
            oItemPlant.bindProperty("key", "PLANT");
            oItemPlant.bindProperty("text", "NAME1");
            oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
            var oPlantsModel = new sap.ui.model.xml.XMLModel();
            oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsByUserQR"); //getPlantsSQ
            oCmbPlant.setModel(oPlantsModel);

            var oSeparator = new sap.ui.commons.Toolbar({
                items: [
                    oLblPlant,
                    new sap.ui.commons.ToolbarSeparator({
                        displayVisualSeparator: false
                    }),
                    oCmbPlant
                ]
            });

            // Crea la TabStrip principale
            var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
            //oTabMaster.setWidth("1000px");
            //oTabMaster.setHeight("800px");
            oTabMaster.attachClose(function (oEvent) {
                var oTabStrip = oEvent.oSource;
                oTabStrip.closeTab(oEvent.getParameter("index"));
            });

			// 1. Tab Divisioni
            var oTablePlants = createTabPlant();
            var oLayoutPlants = new sap.ui.commons.layout.MatrixLayout("tabPlant", {
                columns: 1
            });
            oLayoutPlants.createRow(oTablePlants);
            oTabMaster.createTab(
                oLng_MasterData.getText("MasterData_Plants"), //"Divisioni",
                oLayoutPlants
            );

			// 2. Tab Turni
            var oTableShifts = createTabShift();
            var oLayoutShifts = new sap.ui.commons.layout.MatrixLayout("tabShift", {
                columns: 1
            });
            oLayoutShifts.createRow(oTableShifts);
            oTabMaster.createTab(
                oLng_MasterData.getText("MasterData_Shifts"), //"Turni",
                oLayoutShifts
            );

			// 3. Tab Reparti
            var oTableReparts = createTabRep();
            var oLayoutReparts = new sap.ui.commons.layout.MatrixLayout("tabRep", {
                columns: 1
            });
            oLayoutReparts.createRow(oTableReparts);
            oTabMaster.createTab(
                oLng_MasterData.getText("MasterData_Departs"), //"Reparti",
                oLayoutReparts
            );

            // 4. Tab Linee
            var oTableLine = createTabLine();
            var oLayoutLine = new sap.ui.commons.layout.MatrixLayout("tabLine", {
                columns: 1,
                width: "100%"
            });
            oLayoutLine.createRow(oTableLine);
            oTabMaster.createTab(
                oLng_MasterData.getText("MasterData_Lines"), //"Linee",
                oLayoutLine
            );

			// 5. Tab Macchine
            /*var oTableMachine = createTabMachine();
            var oLayoutMachine = new sap.ui.commons.layout.MatrixLayout("tabMach", {
                columns: 1,
                width: "100%"
            });
            oLayoutMachine.createRow(oTableMachine);
            oTabMaster.createTab(
                oLng_MasterData.getText("MasterData_Machines"), //"Macchine",
                oLayoutMachine
            );
						*/

			var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					oSeparator,
					oTabMaster
				],
                width: "100%"
			});

			oVerticalLayout.placeAt("MasterCont");
        })
    }
);
