/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Anagrafica Part program
//Author: Bruno Rosati
//Date:   15/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina anagrafiche Part Program

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();


// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});


jQuery.ajaxSetup({
	cache: false
});


Libraries.load(
[
"/XMII/CM/Common/MII_core/UI5_utils",
"/XMII/CM/ProductionMain/res/fn_commonTools"
],
	function () {
		$(document).ready(function () {

			var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					new sap.ui.unified.FileUploader({
						layoutData: new sap.ui.layout.form.GridElementData({
							hCells: "auto"
						}),
						value: "carica file",
						editable: true,
						id: "txtPPROGRAM",
						multiple: false,
						maximumFileSize: 20,
						mimeType: "image,text,*",
						fileType: "jpg,png,txt,*",
						uploadOnChange: false,
						uploadUrl: "/XMII/CM/ProductionMain/MasterData/TMP/TMP_provaUpload_1.jsp?Prova1=tuozioporco!",
						fileSizeExceed: function (oEvent) {
							var sName = oEvent.getParameter("fileName");
							var fSize = oEvent.getParameter("fileSize");
							var fLimit = $.UIbyID("txtPPROGRAM").getMaximumFileSize();
							sap.ui.commons.MessageBox.show("File: " + sName + " is of size " + fSize + " MB which exceeds the file size limit of " + fLimit + " MB.", "ERROR", "File size exceeded");
						},
						typeMissmatch: function (oEvent) {
							var sName = oEvent.getParameter("fileName");
							var sType = oEvent.getParameter("fileType");
							var sMimeType = $.UIbyID("txtPPROGRAM").getMimeType();
							if (!sMimeType) {
								sMimeType = $.UIbyID("txtPPROGRAM").getFileType();
							}
							sap.ui.commons.MessageBox.show("File: " + sName + " is of type " + sType + " .Allowed types are: " + sMimeType + ".", "ERROR", "Wrong File type");
						},
						uploadComplete: function (oEvent) {
							var sResponse = oEvent.getParameter("response");
							console.debug(oEvent.getParameter("response"));
							/*if (sResponse) {
								var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
								if (m[1] == "200") {
									sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "SUCCESS", "Upload Success");
								} else {
									sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "ERROR", "Upload Error");
								}
							}*/
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://send",
						text : "via!",
						style: sap.ui.commons.ButtonStyle.Emph,
						enabled: true,
						press: function () {
							$.UIbyID("txtPPROGRAM").upload();
						}
					})
]
			});

			oVerticalLayout.placeAt("MasterCont");
			$("#splash-screen").hide(); //nasconde l'icona di loading
		});


	}
);
//jQuery.getScript2( "/XMII/CM/Production/PrMaster/fn_operators.js" );
/*function( data, textStatus, jqxhr ) {
//console.log( data ); // Data returned
console.log( textStatus ); // Success
console.log( jqxhr.status ); // 200
console.log( "Load was performed." );
});*/


/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
