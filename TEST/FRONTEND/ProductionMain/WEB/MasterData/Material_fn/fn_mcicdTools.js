//**************************************************************************************
//Title:  Funzioni per il recupero delle informazioni dei centri di lavoro
//Author: Bruno Rosati
//Date:   05/10/2017
//Vers:   1.0
//**************************************************************************************
// Script per il recupero delle informazioni dei centri di lavoro

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function fnGetMcicdTableDialog(sPlant, sSelected, cbFunction, bCicTxt) {
	if (!$.UIbyID("dlgMcicdTableSelect")) {

		var oMatModel = new sap.ui.model.xml.XMLModel();
        query = QService + dataProdMD + 
            (bCicTxt?"Cycles/getMcictxtByPlantSQ&Param.1=":"Cycles/getMcdlByPlantSQ&Param.1=") + sPlant;
        oMatModel.loadData(query);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
                new sap.m.Text({
					text: "",
                    title: ""
				}),
                bCicTxt?new sap.m.Text({
					text: "{POPER}",
                    title: "{}"
				}):new sap.m.Text({
					text: "{CDLID}",
                    title: "{}"
				}),
				bCicTxt?new sap.m.Text({
					text: "{CICTXT}",
                    title: "{}"
				}):new sap.m.Text({
					text: "{CDLDESC}",
                    title: "{}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgMcicdTableSelect", {
			title: bCicTxt? oLng_MasterData.getText("MasterData_OperationList"):
                oLng_MasterData.getText("MasterData_CdlList"), //"Lista fasi" || "Lista centri di lavoro"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: true,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
            items: [],
			columns:[
				new sap.m.Column({header: new sap.m.Label({
                    text: "", //""
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: bCicTxt?
                        oLng_MasterData.getText("MasterData_Operation"):oLng_MasterData.getText("MasterData_Cdl"), 
                        //"Fase" || "Centro di lavoro" 
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione"
                    minScreenWidth: "40em"
                })})
            ],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter((bCicTxt?"POPER":"CDLID"), sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter((bCicTxt?"CICTXT":"CDLDESC"), sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
            cancel: function (oControlEvent){
                $.UIbyID("dlgMcicdTableSelect").destroy();
            }
		});

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oMatModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgMcicdTableSelect").setContentWidth("800px");


	if (sSelected == "" || sSelected == null) 
        $.UIbyID("dlgMcicdTableSelect").open();
	else {
		$.UIbyID("dlgMcicdTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SOURCE", sap.ui.model.FilterOperator.EQ, sSelected),
				 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgMcicdTableSelect").open(sSelected);
	}

	$.UIbyID("dlgMcicdTableSelect").focus();
    
}