var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

/*
var jType = {root:[]};
jType.root.push({
    key: 'SC',
    text: oLng_MasterData.getText("MasterData_Scrap") //'Scarto'
});
jType.root.push({
    key: 'SO',
    text: oLng_MasterData.getText("MasterData_Suspended") //'Sospeso'
});
*/

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabScrabReasons() {
    
    //filtro macro causale
    var oCmbMacroReasonFilter = new sap.ui.commons.ComboBox("cmbMacroReasonFilter", {
        change: function (oEvent) {
            $.UIbyID("cmbTypesFilter").setSelectedKey("");
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabScrabReasons();
        }
    });            
    var oItemMacroReason = new sap.ui.core.ListItem();
    oItemMacroReason.bindProperty("key", "SCTYPE");
    oItemMacroReason.bindProperty("text", "EXTENDED_REAS");
    oCmbMacroReasonFilter.bindItems("/Rowset/Row", oItemMacroReason);
    var oMacroReasonModel = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oMacroReasonModel.loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + $('#plant').val() + "&Param.3=" + sLanguage
        );
    }
    else{
        oMacroReasonModel.loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.3=" + sLanguage
        );
    }    
    oCmbMacroReasonFilter.setModel(oMacroReasonModel);
    
    //filtro tipo scarto (Scarto/Sospeso)
    var oModelTypesFilter = new sap.ui.model.xml.XMLModel();
    oModelTypesFilter.loadData(QService + dataProdMD + "ScrabReasons/getGrScrabReasonsTypeSQ" + "&Param.1=" + sLanguage);
        
    var oCmbTypesFilter = new sap.ui.commons.ComboBox({
        id: "cmbTypesFilter",
        selectedKey : "",
        change: function (oEvent) {
            $.UIbyID("cmbMacroReasonFilter").setSelectedKey("");
            refreshTabScrabReasons();
            $.UIbyID("delFilter").setEnabled(true);
        }        
    });
    oCmbTypesFilter.setModel(oModelTypesFilter);
    var oItemTypesFilter = new sap.ui.core.ListItem();
    oItemTypesFilter.bindProperty("text", "EXTENDED_TEXT");
	oItemTypesFilter.bindProperty("key", "ORIG_VALUE");	
	oCmbTypesFilter.bindItems("/Rowset/Row", oItemTypesFilter);
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "ScrabReasonsTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_ScrabReasList"), //"Lista micro causali scarto",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("ScrabReasonsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModScrabReason").setEnabled(false);
        			$.UIbyID("btnDelScrabReason").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModScrabReason").setEnabled(true);
                    $.UIbyID("btnDelScrabReason").setEnabled(true);			
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                        id:   "btnAddScrabReason",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            $.UIbyID("ScrabReasonsTab").setSelectedIndex(-1);
                            openScrabReasonsEdit(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                        id:   "btnModScrabReason",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openScrabReasonsEdit(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                        id:   "btnDelScrabReason",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelScrabReason();
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabScrabReasons();
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_MasterData.getText("MasterData_MacroReasonFilter") //"Filtro macro causale"
                    }),
                    oCmbMacroReasonFilter,
                    new sap.ui.commons.Label({
                        text: oLng_MasterData.getText("MasterData_ScrapSuspendedFilter") //"Filtro Scarto/Sospeso"
                    }),
                    oCmbTypesFilter,
                    new sap.ui.commons.Button({
                        icon: "sap-icon://filter",
                        enabled: false,
                        tooltip: oLng_MasterData.getText("MasterData_RemoveFilter"), //"Rimuovi filtri",
                        id:   "delFilter",
                        press: function () {
                            $.UIbyID("cmbMacroReasonFilter").setSelectedKey("");
                            $.UIbyID("cmbTypesFilter").setSelectedKey("");
                            refreshTabScrabReasons();
                            $.UIbyID("delFilter").setEnabled(false);
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "NAME1",
                label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "GR_REAS",
				label: oLng_MasterData.getText("MasterData_MacroScrapReasons"), //"Macro causale scarto",
				properties: {
					width: "100px"
				}
            },
            {
				Field: "SREASID", 
				label: oLng_MasterData.getText("MasterData_MicroScrapReasons"), //"Micro causale scarto",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "REASTXT",
				label: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "DESCR", 
				label: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
				properties: {
					width: "130px"
				}
            },
            {
				Field: "ENABLED",
				label: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "SCTYPE", 
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "60px",
                    visible: false
				}
            },
            {
				Field: "GR_DESCR",
				label: oLng_MasterData.getText("MasterData_GroupDescription"), //"Descrizione Gruppo",
				properties: {
					width: "100px",
                    visible: false
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabScrabReasons();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabScrabReasons() {
    
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("ScrabReasonsTab").getModel().loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsQR&Param.1=" + $('#plant').val() +
            "&Param.2=" + $.UIbyID("cmbMacroReasonFilter").getSelectedKey() +
            "&Param.3=" + $.UIbyID("cmbTypesFilter").getSelectedKey()
        );
    }
    else{
        $.UIbyID("ScrabReasonsTab").getModel().loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsQR&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() +
            "&Param.2=" + $.UIbyID("cmbMacroReasonFilter").getSelectedKey() +
            "&Param.3=" + $.UIbyID("cmbTypesFilter").getSelectedKey()
        );
    }
    $.UIbyID("ScrabReasonsTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Operatori
function openScrabReasonsEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        enabled: false,
        selectedKey : bEdit?fnGetCellValue("ScrabReasonsTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey()
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModelSelectTypes = new sap.ui.model.xml.XMLModel();
    oModelSelectTypes.loadData(QService + dataProdMD + "ScrabReasons/getGrScrabReasonsTypeSQ&Param.1=" + sLanguage);        
    var ocmbSelectTypes = new sap.ui.commons.ComboBox({
        id: "cmbSelectTypes",
        selectedKey : bEdit ? fnGetCellValue("ScrabReasonsTab", "TYPE") : "",
        change: function (oEvent) {
            $.UIbyID("cmbScrabReasonGroup").setSelectedKey("");
            $.UIbyID("cmbScrabReasonGroup").getModel().loadData(
                QService + dataProdMD + "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + 
                $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + $.UIbyID("cmbSelectTypes").getSelectedKey());
        } 
    });
    ocmbSelectTypes.setModel(oModelSelectTypes);
    var oItemSelectTypes = new sap.ui.core.ListItem();
    oItemSelectTypes.bindProperty("text", "EXTENDED_TEXT");
	oItemSelectTypes.bindProperty("key", "ORIG_VALUE");	
	ocmbSelectTypes.bindItems("/Rowset/Row", oItemSelectTypes);
    
    var oModel4 = new sap.ui.model.xml.XMLModel();
	oModel4.loadData(
        QService + dataProdMD +
        "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() +
        "&Param.2=" + (bEdit ? fnGetCellValue("ScrabReasonsTab", "TYPE") : $.UIbyID("cmbSelectTypes").getSelectedKey())
    );

	// Crea la ComboBox per le divisioni in input
	var oCmbScrabReasonGroup = new sap.ui.commons.ComboBox({
        id: "cmbScrabReasonGroup", 
        selectedKey : bEdit?fnGetCellValue("ScrabReasonsTab", "SCTYPE"):""
    });
	oCmbScrabReasonGroup.setModel(oModel4);		
	var oItemScrabReasonInput = new sap.ui.core.ListItem();
    // oItemScrabReasonInput.bindProperty("text", "PLNAME");
	oItemScrabReasonInput.bindProperty("text", "EXTENDED_REAS");
	oItemScrabReasonInput.bindProperty("key", "SCTYPE");	
	oCmbScrabReasonGroup.bindItems("/Rowset/Row", oItemScrabReasonInput);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgScrabReason",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("ScrabReasonsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyScrabReason"):oLng_MasterData.getText("MasterData_NewScrabReason"),
            /*"Modifica Causale di scarto":"Nuova Causale di scarto",*/
            //icon: "/images/address.gif"
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MicroScrapReasons"), //"Micro causale scarto",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsTab", "SREASID"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "SREASID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsTab", "REASTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "REASTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsTab", "DESCR"):"",
                                editable: true,
                                maxLength: 60,
                                id: "DESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("ScrabReasonsTab", "ENABLED") == "0" ? false : true ) : true,
                                id: "ENABLED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ReasType"), //"Tipo causale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: ocmbSelectTypes
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MacroScrapReasons"), //"Macro causale scarto",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbScrabReasonGroup
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            if(bEdit){
                fnSaveScrabReason();
            }
            else{
                fnAddScrabReason();
            }
            
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per aggiungere una causale
function fnAddScrabReason() 
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var ID_Causale = $.UIbyID("SREASID").getValue();
    var causale = $.UIbyID("REASTXT").getValue();
    var description = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
    var scrabReason_group = $.UIbyID("cmbScrabReasonGroup").getSelectedKey();
    
    if ( plant === "" || ID_Causale === "" || causale === "" || scrabReason_group === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe =  dataProdMD +
                "ScrabReasons/addScrabReasonWithoutUpdateSQ&Param.1=" + plant +
                "&Param.2=" + ID_Causale +
                "&Param.3=" + causale +
                "&Param.4=" + description +
                "&Param.5=" + active +
                "&Param.6=" + scrabReason_group;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_ScrabReasonSaved"),
                         oLng_MasterData.getText("MasterData_ScrabReasonSavedError")
                         /*"Causale di scarto salvata correttamente", "Errore in salvataggio causale di scarto"*/,
                         false);
    if (ret){
        $.UIbyID("dlgScrabReason").close();
        $.UIbyID("dlgScrabReason").destroy();
        refreshTabScrabReasons();
    }
}

// Funzione per salvare le modifiche alla causale o inserirla
function fnSaveScrabReason() 
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var ID_Causale = $.UIbyID("SREASID").getValue();
    var causale = $.UIbyID("REASTXT").getValue();
    var description = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
    var scrabReason_group = $.UIbyID("cmbScrabReasonGroup").getSelectedKey();
    
    if ( plant === "" || ID_Causale === "" || causale === "" || scrabReason_group === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe =  dataProdMD +
                "ScrabReasons/addScrabReasonSQ&Param.1=" + plant +
                "&Param.2=" + ID_Causale +
                "&Param.3=" + causale +
                "&Param.4=" + description +
                "&Param.5=" + active +
                "&Param.6=" + scrabReason_group;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_ScrabReasonSaved"),
                         oLng_MasterData.getText("MasterData_ScrabReasonSavedError")
                         /*"Causale di scarto salvata correttamente", "Errore in salvataggio causale di scarto"*/,
                         false);
    if (ret){
        $.UIbyID("dlgScrabReason").close();
        $.UIbyID("dlgScrabReason").destroy();
        refreshTabScrabReasons();
    }
}

function fnDelScrabReason(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_ScrabReasonDeleting"), //"Eliminare la causale di scarto selezionata?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "ScrabReasons/delScrabReasonSQ" +
                            "&Param.1=" + fnGetCellValue("ScrabReasonsTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("ScrabReasonsTab", "SREASID");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_ScrabReasonDeleted"), //"Causale di scarto eliminata correttamente",
                    oLng_MasterData.getText("MasterData_ScrabReasonDeletedError"), //"Errore in eliminazione causale di scarto",
                    false
                );
                refreshTabScrabReasons();
                $.UIbyID("btnModScrabReason").setEnabled(false);
                $.UIbyID("btnDelScrabReason").setEnabled(false);
                $.UIbyID("ScrabReasonsTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}