// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/OperatorTypes_fn/fn_operatorTypes"
    ],
    function () {
        $(document).ready(function () {
            
            var oTable1 = createTabOperator();
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					//oCmbPlant,
					oTable1
				]
			});
			
			oVerticalLayout.placeAt("MasterCont");
        });
    }
);