// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Confirm/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MaserData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/ConfDashboard_fn/fn_ConfDashboard"
    ],
    function () {
        $(document).ready(function () {
	var oLayPlant = new sap.ui.layout.HorizontalLayout();
	var oLblPlant = new sap.ui.commons.Label();
	var oLblLine = new sap.ui.commons.Label();

	var oLayEnd = new sap.ui.layout.HorizontalLayout();

    var oBtnToSap = new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_ConfirmModify"), //"Aggiungi dichiarazione",
                id:   "oBtnToSap",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    //
                }
            });

	oLblPlant.setText( oLng_MaserData.getText("MasterData_Plant") + ":");
	oLblLine.setText( oLng_MaserData.getText("MasterData_Line") + ":");

/*
            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlantConfProd", {
				selectedKey: "2101",
				tooltip: oLng_MaserData.getText("MasterData_FilterDivision"), //"Filtro Divisioni",
				change: function(oEvent){
//					$.UIbyID("ShiftTab").filter(oPlantFShift,$("#cmbPlant-input").val());
				}
			});
			
			var oItemPlant = new sap.ui.core.ListItem();
			oItemPlant.bindProperty("text", "NAME1");
			oItemPlant.bindProperty("key", "PLANT");
			oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
			
			var oPlantsModel = new sap.ui.model.xml.XMLModel()
			oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
			oCmbPlant.setModel(oPlantsModel);
            oCmbPlant.setSelectedKey("2101");
*/
	var oCmbPlant = fnCreateCombo( false, "filtPlantConfProd", "PLANT", "NAME1",  QService + dataProdMD + "Plants/getPlantsSQ", "2101");

            
	oLayPlant.addContent(oLblPlant);
	oLayPlant.addContent(oCmbPlant);
//	oLayPlant.addContent(oLblLine);

            oLayEnd.addContent(oBtnToSap);

            var oTable1 = createTabConfdashboard();
            
            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					oLayPlant,
//					oCmbPlant,
					oTable1,
					oLayEnd
				]
			});
			
			oVerticalLayout.placeAt("MasterCont");
        });
    }
);

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/



