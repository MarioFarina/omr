        /*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
        //Impostazioni per gestione errori

        //**************************************************************************************
        //Title:  Linea di lavoro - funzioni
        //Author: Elena Andrini
        //Date:   15/02/2017
        //Vers:   1.1
        //**************************************************************************************

        //Creo la tabella
        function createLineWorkTab() {
            var oTable = UI5Utils.init_UI5_Table({
                id: "LineWorkTab",
                properties: {
                    title: oLng_MasterData.getText("line_List"),
                    visibleRowCount: 15,
                    width: "98%",
                    firstVisibleRow: 0,
                    selectionMode: sap.ui.table.SelectionMode.Single,
                    navigationMode: sap.ui.table.NavigationMode.Scrollbar,
                    visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,

                    //Abilito o disalibilito i pulsanti se è selezionata la riga
                    rowSelectionChange: function (oControlEvent) {
                        if ($.UIbyID("LineWorkTab").getSelectedIndex() == -1) {
                            $.UIbyID("btnModify").setEnabled(false);
                            $.UIbyID("btnDelete").setEnabled(false);
                        } else {
                            $.UIbyID("btnModify").setEnabled(true);
                            $.UIbyID("btnDelete").setEnabled(true);
                        }
                    }

                },
                exportButton: true,

                //Creo i pulsanti della toolbar
                toolbarItems: [
                new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("line_Add"),
                        id: "btnAdd",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function () {
                            modifyLineWork(false, "INSERT");
                        }
                    }),
                new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("line_Modify"),
                        id: "btnModify",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function () {
                            modifyLineWork(true, "UPDATE");
                        }
                    }),
                new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("line_Delete"),
                        id: "btnDelete",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function () {
                            deleteLineWork();
                        }
                    }),
                new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("line_Refresh"),
                        id: "btnRefresh",
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function () {
                            refreshLineWorkTab();
                        }
                    })
                ],

                //Creo le colonne della tabella
                columns: [
                    {
                        Field: "IDLINE",
                        label: oLng_MasterData.getText("line_idLine"),
                        properties: {
                            width: "50px"
                        }
                    },
                    {
                        Field: "ORDER",
                        label: oLng_MasterData.getText("line_Order"),
                        properties: {
                            width: "50px"
                        }
                    },
                    {
                        Field: "POPER",
                        label: oLng_MasterData.getText("line_Operation"),
                        properties: {
                            width: "50px"
                        }
                    },
                    {
                        Field: "MATERIAL",
                        label: oLng_MasterData.getText("line_Material"),
                        properties: {
                            width: "80px"
                        }
                    },
                    {
                        //campo nascosto
                        Field: "PLANT",
                        properties: {
                            width: "50px",
                            visible: false
                        }
                    },
                    {
                        Field: "CONFIRMATION",
                        label: oLng_MasterData.getText("line_Confirmation"),
                        properties: {
                            width: "50px"
                        }
                    }

                ]
            });

            oModel = new sap.ui.model.xml.XMLModel();
            oTable.setModel(oModel);
            oTable.bindRows("/Rowset/Row");

            return oTable;
        }

        //Pulsante Aggiorna
        function refreshLineWorkTab() {
            $.UIbyID("LineWorkTab").getModel().loadData(QService + dataProdMD + "LineWork/getLineWorkWithKeySQ&Param.1=" + $('#plant').val());
        }

        //Pulsante Aggiungi/Modifica
        function modifyLineWork(bEdit, mode) {
            if (bEdit === undefined) bEdit = false;
            if (mode === undefined) mode = "";


            //Creo la combo linee valorizzata
            var oCmbLines = fnCreateCombo("oCmbLines", "IDLINE", "LINETXT", QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());

            if (bEdit) {
                oCmbLines.setSelectedKey(fnGetCellValue("LineWorkTab", "IDLINE"));
                oCmbLines.setEnabled(false);
            }

            //  Crea la finestra di dialogo  
            var oDialog = new sap.ui.commons.Dialog({
                modal: true,
                resizable: true,
                id: "DialogBox",
                maxWidth: "600px",
                Width: "600px",
                showCloseButton: false,
            });

            var oLayout = new sap.ui.layout.form.GridLayout({
                singleColumn: true
            });
            var oForm = new sap.ui.layout.form.Form({
                title: new sap.ui.core.Title({
                    text: bEdit ? oLng_MasterData.getText("line_Edit") : oLng_MasterData.getText("line_New")
                }),
                width: "98%",
                layout: oLayout,
                formContainers: [
                    new sap.ui.layout.form.FormContainer({
                        formElements: [
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_idLine"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: oCmbLines

                            }),
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_Order"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: [
                                    new sap.ui.commons.TextField({
                                        layoutData: new sap.ui.layout.form.GridElementData({
                                            hCells: "auto"
                                        }),
                                        value: bEdit ? fnGetCellValue("LineWorkTab", "ORDER") : "",
                                        editable: !bEdit,
                                        maxLength: 12,
                                        id: "ORDER"
                                    })
                                ]
                            }),
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_Operation"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: [
                                    new sap.ui.commons.TextField({
                                        layoutData: new sap.ui.layout.form.GridElementData({
                                            hCells: "auto"
                                        }),
                                        value: bEdit ? fnGetCellValue("LineWorkTab", "POPER") : "",
                                        editable: !bEdit,
                                        maxLength: 4,
                                        id: "POPER"
                                    })
                                ]
                            }),
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_Material"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: [
                                    new sap.ui.commons.TextField({
                                        layoutData: new sap.ui.layout.form.GridElementData({
                                            hCells: "auto"
                                        }),
                                        value: bEdit ? fnGetCellValue("LineWorkTab", "MATERIAL") : "",
                                        editable: true,
                                        maxLength: 40,
                                        id: "MATERIAL"
                                    })
                                ]
                            }),
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_Plant"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: [
                                    new sap.ui.commons.TextField({
                                        layoutData: new sap.ui.layout.form.GridElementData({
                                            hCells: "auto"
                                        }),
                                        value: $.UIbyID("filtPlant").getSelectedKey(),
                                        editable: false,
                                        maxLength: 4,
                                        id: "PLANT"
                                    })
                                ]
                            }),
                            //Nuova riga
                            new sap.ui.layout.form.FormElement({
                                label: new sap.ui.commons.Label({
                                    text: oLng_MasterData.getText("line_Confirmation"),
                                    layoutData: new sap.ui.layout.form.GridElementData({
                                        hCells: "2"
                                    })
                                }),
                                fields: [
                                    new sap.ui.commons.TextField({
                                        layoutData: new sap.ui.layout.form.GridElementData({
                                            hCells: "auto"
                                        }),
                                        value: bEdit ? fnGetCellValue("LineWorkTab", "CONFIRMATION") : "",
                                        editable: true,
                                        maxLength: 10,
                                        id: "CONFIRMATION"
                                    })
                                ]
                            })
                        ]
                    })
                ]
            });

            oDialog.addContent(oForm);
            oDialog.addButton(new sap.ui.commons.Button({
                text: oLng_MasterData.getText("line_Save"),
                press: function () {
                    fnSave(mode);
                }
            }));
            oDialog.addButton(new sap.ui.commons.Button({
                text: oLng_MasterData.getText("line_Cancel"),
                press: function () {
                    oDialog.close();
                    oDialog.destroy();
                }
            }));
            oDialog.open();
        }

        //Pulsante Salva in Dialog Box
        function fnSave(mode) {
            if (mode === 'undefined') mode = "";

            //Leggo il valore dei campi in input
            //var idline = $.UIbyID("IDLINE").getValue();
            var idline = $.UIbyID("oCmbLines").getSelectedKey();
            var order = $.UIbyID("ORDER").getValue();
            var poper = $.UIbyID("POPER").getValue();
            var material = $.UIbyID("MATERIAL").getValue();
            var plant = $.UIbyID("PLANT").getValue();
            var confirmation = $.UIbyID("CONFIRMATION").getValue();

            //Setto i campi chiave obbligatori
            if (idline === "" || order === "" || poper === "") {
                sap.ui.commons.MessageBox.show(oLng_MasterData.getText("line_RequiredFieldsErr"),
                    sap.ui.commons.MessageBox.Icon.ERROR,
                    oLng_MasterData.getText("line_RequiredFieldsErr")[sap.ui.commons.MessageBox.Action.OK],
                    sap.ui.commons.MessageBox.Action.OK);
                return;
            }

            var qexe = dataProdMD;
            switch (mode) {
            case "INSERT":
                qexe += "LineWork/addLineWorkSQ";
                break;
            case "UPDATE":
                qexe += "LineWork/updLineWorkSQ";
                break;
            }

            qexe += "&Param.1=" + idline +
                "&Param.2=" + order +
                "&Param.3=" + poper +
                "&Param.4=" + material +
                "&Param.5=" + plant +
                "&Param.6=" + confirmation;

            var ret = fnExeQuery(qexe, oLng_MasterData.getText("line_Saved"),
                oLng_MasterData.getText("line_SavedError"),
                false);
            if (ret) {
                $.UIbyID("DialogBox").close();
                $.UIbyID("DialogBox").destroy();
                refreshLineWorkTab();
            }
        }

        //Pulsante Elimina
        function deleteLineWork() {
            sap.ui.commons.MessageBox.show(
                oLng_MasterData.getText("line_Deleting"),
                sap.ui.commons.MessageBox.Icon.WARNING,
                oLng_MasterData.getText("line_DeletingConfirm"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                function (sResult) {
                    if (sResult == 'YES') {
                        var qexe = dataProdMD + "LineWork/delLineWorkSQ" +
                            "&Param.1=" + fnGetCellValue("LineWorkTab", "IDLINE") +
                            "&Param.2=" + fnGetCellValue("LineWorkTab", "ORDER") +
                            "&Param.3=" + fnGetCellValue("LineWorkTab", "POPER");
                        var ret = fnSQLQuery(
                            qexe,
                            oLng_MasterData.getText("line_Deleted"),
                            oLng_MasterData.getText("line_DeletedErr"),
                            false
                        );
                        refreshLineWorkTab();
                        $.UIbyID("btnModify").setEnabled(false);
                        $.UIbyID("btnDelete").setEnabled(false);
                        $.UIbyID("LineWorkTab").setSelectedIndex(-1);
                    }
                },
                sap.ui.commons.MessageBox.Action.YES
            );
        }

        //Funzione per creare combobox
        function fnCreateCombo(id, key, text, qexe) {
            var oModel = new sap.ui.model.xml.XMLModel();
            oModel.loadData(qexe);

            var oComboBox = new sap.ui.commons.ComboBox({
                id: id
            });
            oComboBox.setModel(oModel);
            var oListItem = new sap.ui.core.ListItem();
            oListItem.bindProperty("key", key);
            oListItem.bindProperty("text", text);
            oComboBox.bindItems("/Rowset/Row", oListItem);

            return oComboBox;
        }