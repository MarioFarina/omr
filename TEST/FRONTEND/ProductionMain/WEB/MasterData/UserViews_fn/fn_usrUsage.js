/***********************************************************************************************/
// Funzioni ed oggetti per Utilizzo utenti
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella usrUsage e ritorna l'oggetto oTable
function createTabUsrUsage() {


    var oTable = UI5Utils.init_UI5_Table({
        id: "usrUsage",
        properties: {
            title: oLng_MasterData.getText("MasterData_UsersUseList"), //"Lista utilizzo contente utenti",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 1,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            rowSelectionChange: function (oControlEvent) {
            }
        },
        exportButton: false,
        toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: "sap-icon://refresh",            
                enabled: true,
                press: function ()
                {
                    refreshTabUsrUsage();
                }
            })
        ],
         columns: [
            {
                Field: "RecordedDate",
                label: oLng_MasterData.getText("MasterData_Date"), //"Data",
                properties: {
                    width: "120px"
                },
                template: {
                    type: "Date",
                    textAlign: "Center"
                }
			},
            {
                Field: "Username",
                label: oLng_MasterData.getText("MasterData_User"), //"Utente",
                properties: {
                    width: "120px"
                }                
			},
            {
                Field: "NumberRequests",
                label: oLng_MasterData.getText("MasterData_Execution"), //"Esecuzioni",
                properties: {
                    width: "70px"
                }
			},
            {
                Field: "RowsReturned",
                label: oLng_MasterData.getText("MasterData_Records"), //"Records",
                properties: {
                    width: "120px"
                }
			}
		],
        toolbar: new sap.ui.commons.Toolbar({
            items: []
        })
    });

    oModel = new sap.ui.model.xml.XMLModel();

    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabUsrUsage();
    return oTable;
}

// Aggiorna la tabella Plants
function refreshTabUsrUsage() {
    $("#splash-screen").show();
    $.UIbyID("usrUsage").getModel().loadData(QService + "Content-Type=text/XML&service=Monitoring&Mode=UserQuery");
    $("#splash-screen").hide();
}

