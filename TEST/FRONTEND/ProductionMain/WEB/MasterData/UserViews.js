// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProd = "Content-Type=text/XML&QueryTemplate=Production/PrMaster/";
var icon16 = "/XMII/CM/Common/icons/16x16/";

jQuery.ajaxSetup({
    cache: false
});

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

Libraries.load(
	["/XMII/CM/Common/MII_core/UI5_utils",
     "/XMII/CM/ProductionMain/MasterData/UserViews_fn/fn_usrLogins",
     "/XMII/CM/ProductionMain/MasterData/UserViews_fn/fn_usrUsage",
     "/XMII/CM/ProductionMain/MasterData/UserViews_fn/fn_usrLoginNumbers"
    ],
    function () {

        $(document).ready(function () {
            $("#splash-screen").hide();
        });

        // Crea la TabStrip principale
        var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
        oTabMaster.attachClose(function (oEvent) {
            var oTabStrip = oEvent.oSource;
            oTabStrip.closeTab(oEvent.getParameter("index"));
        });


        // 1. tab: Login Numbers
        var oTable1 = createTabUsrLoginNumbers();
        var oLayout1 = new sap.ui.commons.layout.MatrixLayout("UsrLoginNumbersTab", {
            columns: 1
        });
        oLayout1.createRow(oTable1);
        oTabMaster.createTab(
            oLng_MasterData.getText("MasterData_LoginsNumber"), //"Numero Login effettuati",
            oLayout1
        );

        // 2. tab: Usage
        var oTable2 = createTabUsrUsage();
        var oLayout2 = new sap.ui.commons.layout.MatrixLayout("UsrUsageTab", {
            columns: 1
        });
        oLayout2.createRow(oTable2);
        oTabMaster.createTab(
            oLng_MasterData.getText("MasterData_UserUse"), //"Utilizzo degli utenti",
            oLayout2
        );
        
        // 3. tab: Logins
        var oTable3 = createTabUsrLogins();
        var oLayout3 = new sap.ui.commons.layout.MatrixLayout("UsrLoginsTab", {
            columns: 1
        });
        oLayout3.createRow(oTable3);
        oTabMaster.createTab(
            oLng_MasterData.getText("MasterData_UsersLogin"), //"Login degli utenti",
            oLayout3
        );

        oTabMaster.placeAt("MasterCont");

    }
);