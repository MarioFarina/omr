/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Anagrafica Part program
//Author: Bruno Rosati
//Date:   15/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina anagrafiche Part Program

var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();


// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabPartProgram() {

	//Crea L'oggetto Tabella Operatore
	var oTable = UI5Utils.init_UI5_Table({
		id: "PartProgramTab",
		properties: {
			title: oLng_MasterData.getText("MasterData_PartProgramsList"), //"Lista Part Program",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {
				if ($.UIbyID("PartProgramTab").getSelectedIndex() == -1) {
					$.UIbyID("btnModPartProgram").setEnabled(false);
					$.UIbyID("btnCopyPartProgram").setEnabled(false);
					$.UIbyID("btnShowPartProgram").setEnabled(false);
					$.UIbyID("btnDelPartProgram").setEnabled(false);
					$.UIbyID("btnSendPartProgram").setEnabled(false);
				} else {
					if (!(fnGetCellValue("PartProgramTab", "PPMODE") === "") && fnGetCellValue("PartProgramTab", "PPLEN") > 0) {
						$.UIbyID("btnSendPartProgram").setEnabled(true);
					} else {
						$.UIbyID("btnSendPartProgram").setEnabled(false);
					}
					$.UIbyID("btnModPartProgram").setEnabled(true);
					$.UIbyID("btnCopyPartProgram").setEnabled(true);
					$.UIbyID("btnShowPartProgram").setEnabled(true);
					$.UIbyID("btnDelPartProgram").setEnabled(true);
				}
			}
		},
		exportButton: false,
		toolbarItems: [
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
				id: "btnAddPartProgram",
				icon: 'sap-icon://add',
				enabled: true,
				press: function () {
					$.UIbyID("PartProgramTab").setSelectedIndex(-1);
					openPartProgramEdit(0);
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
				id: "btnModPartProgram",
				icon: 'sap-icon://edit',
				enabled: false,
				press: function () {
					openPartProgramEdit(1);
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Copy"), //"Copia",
				id: "btnCopyPartProgram",
				icon: 'sap-icon://edit',
				enabled: false,
				press: function () {
					openPartProgramEdit(2);
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_PProgramShow"), //"Visualizza Part Program",
				id: "btnShowPartProgram",
				enabled: false,
				press: function () {
					fnShowPProgram();
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
				id: "btnDelPartProgram",
				icon: 'sap-icon://delete',
				enabled: false,
				press: function () {
					fnDelPProgram();
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
				icon: 'sap-icon://refresh',
				enabled: true,
				press: function () {
					refreshTabPartProgram();
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_SendPProgram"), //"Invia Part Program",
				id: "btnSendPartProgram",
				enabled: false,
				press: function () {
					sendPartProgram();
				}
			}),
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_ReadPProgram"), //"Leggi Part Program",
				id: "btnReadPartProgram",
				enabled: false,
				press: function () {}
			})
		],
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
					visible: false,
					flexible: false
				}
			}, {
				Field: "NAME1",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "50px",
					visible: false,
					flexible: false
				}
			}, {
				Field: "IDLINE",
				label: oLng_MasterData.getText("MasterData_LineID"), //"ID Linea",
				properties: {
					width: "70px",
					visible: false,
					flexible: false
				}
			}, {
				Field: "LINETXT",
				label: oLng_MasterData.getText("MasterData_Line"), //"Linea",
				properties: {
					width: "140px",
					flexible: false
				}
			}, {
				Field: "IDMACH",
				properties: {
					width: "100px",
					visible: false,
					flexible: false
				}
			}, {
				Field: "MACHTXT",
				label: oLng_MasterData.getText("MasterData_Machine"), //"Macchina",
				properties: {
					width: "110px",
					flexible: false
				}
			}, {
				Field: "MATERIAL",
				label: oLng_MasterData.getText("MasterData_MaterialCode"), //"Materiale",
				properties: {
					width: "110px",
					flexible: false
				}
			}, {
				Field: "DESC",
				label: oLng_MasterData.getText("MasterData_MaterialDescr"), //"Descrizione materiale",
				properties: {
					width: "160px",
					flexible: false
				}
			}, {
				Field: "VERSION",
				label: oLng_MasterData.getText("MasterData_Version"), //"Versione",
				properties: {
					width: "80px",
					flexible: false
				}
			}, {
				Field: "ALT",
				label: oLng_MasterData.getText("MasterData_Alt"), //"ALT",
				properties: {
					width: "90px",
					flexible: false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
			}, {
				Field: "DEFAULT",
				label: oLng_MasterData.getText("MasterData_Default"), //"Default",
				properties: {
					width: "70px",
					flexible: false
				},
				template: {
					type: "checked",
					textAlign: "Right"
				}
			}, {
				Field: "PRGNUM",
				label: oLng_MasterData.getText("MasterData_PartProgramName"), //"Nome programma",
				properties: {
					width: "140px",
					flexible: false
				}
			}, {
				Field: "PRGVAR",
				label: oLng_MasterData.getText("MasterData_PartProgramVar"), //"Variabili programma",
				properties: {
					width: "150px",
					flexible: false
				}
			}, {
				Field: "DESCR",
				label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
				properties: {
					width: "100px",
					flexible: false
				}
			}, {
				Field: "NOTE",
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "200px",
					flexible: false
				}
			}, {
				Field: "PPMODE",
				label: oLng_MasterData.getText("MasterData_PpMode"),
				properties: {
					width: "70px",
					visible: false,
					flexible: false
				}
			}, {
				Field: "PPLEN",
				label: oLng_MasterData.getText("MasterData_PpLen"),
				properties: {
					width: "70px",
					visible: false,
					flexible: false
				}
			}, {
						Field: "PPROGRAM",
						label: oLng_MasterData.getText("MasterData_PpLen"),
						properties: {
							width: "70px",
							visible: false,
							flexible : false
						}
					}
		]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabPartProgram();
	return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabPartProgram() {
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		$.UIbyID("PartProgramTab").getModel().loadData(
			QService + dataProdMD +
			"PartProgram/getPartProgramsSQ&Param.1=" + $('#plant').val() + "&Param.2=" + $.UIbyID("filtLine").getSelectedKey()
		);
	} else {
		$.UIbyID("PartProgramTab").getModel().loadData(
			QService + dataProdMD +
			"PartProgram/getPartProgramsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + $.UIbyID("filtLine").getSelectedKey()
		);
	}
	$("#splash-screen").hide();
}


// Function per creare la finestra di dialogo PProgram
function openPartProgramEdit(bEdit) {
	if (bEdit === undefined) bEdit = 0;

	var oModelPlant = new sap.ui.model.xml.XMLModel();
	oModelPlant.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
		id: "cmbPlantInput",
		selectedKey: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "PLANT") : $.UIbyID("filtPlant").getSelectedKey(),
		enabled: false,
		change: function () {
			$.UIbyID("cmbPPLine").getModel().loadData(
				QService + dataProdMD +
				"Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("cmbPlantInput").getSelectedKey()
			);
			$.UIbyID("cmbPPLine").setSelectedKey("");
			$.UIbyID("cmbPPMach").setSelectedKey("");
			$.UIbyID("txtMaterial").setValue("").data("SelectedKey", "");
		}
	});
	oCmbPlantInput.setModel(oModelPlant);
	var oItemPlantInput = new sap.ui.core.ListItem();
	// oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);

	var oModelLine = new sap.ui.model.xml.XMLModel();
	oModelLine.loadData(
		QService + dataProdMD +
		"Lines/getLinesbyPlantSQ&Param.1=" +
		((bEdit != 0) ? fnGetCellValue("PartProgramTab", "PLANT") : $.UIbyID("filtPlant").getSelectedKey())
	);
	// Crea la ComboBox per le linee in input
	var oCmbPPLine = new sap.ui.commons.ComboBox({
		id: "cmbPPLine",
		selectedKey: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "IDLINE") : 1,
		change: function () {
			$.UIbyID("cmbPPMach").getModel().loadData(
				QService + dataProdMD +
				"Machines/getMachinesByLineSQ&Param.1=" + $.UIbyID("cmbPPLine").getSelectedKey()
			);
			$.UIbyID("cmbPPMach").setSelectedKey("");
		}
	});
	oCmbPPLine.setModel(oModelLine);
	var oItemPPLine = new sap.ui.core.ListItem();
	oItemPPLine.bindProperty("text", "LINETXT");
	oItemPPLine.bindProperty("key", "IDLINE");
	oCmbPPLine.bindItems("/Rowset/Row", oItemPPLine);

	var oModelMach = new sap.ui.model.xml.XMLModel();
	oModelMach.loadData(
		QService + dataProdMD +
		"Machines/getMachinesByLineSQ&Param.1=" +
		(bEdit ? fnGetCellValue("PartProgramTab", "IDLINE") : $.UIbyID("cmbPPLine").getSelectedKey())
	);
	// Crea la ComboBox per le macchine in input
	var oCmbPPMach = new sap.ui.commons.ComboBox({
		id: "cmbPPMach",
		selectedKey: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "IDMACH") : ""
	});
	oCmbPPMach.setModel(oModelMach);
	var oItemPPMach = new sap.ui.core.ListItem();
	oItemPPMach.bindProperty("text", "MACHTXT");
	oItemPPMach.bindProperty("key", "IDMACH");
	oCmbPPMach.bindItems("/Rowset/Row", oItemPPMach);
    
    var oTxtMaterial = new sap.ui.commons.TextField("txtMaterial", {
        value: "",
        enabled: false,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
    });
    if(bEdit) {
        oTxtMaterial.setValue(
            fnGetCellValue("PartProgramTab", "MATERIAL")
        ).data("SelectedKey", fnGetCellValue("PartProgramTab", "MATERIAL"));
    }

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgPartProgram",
		maxWidth: "600px",
		Width: "600px",
		showCloseButton: false
	});

	//(fnGetCellValue("PartProgramTab", "FixedVal")==1)?true : false
	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});
	 var testCSV = (bEdit != 0) ? btoa(fnGetCellValue("PartProgramTab", "PPROGRAM")) : "";
	var oForm1 = new sap.ui.layout.form.Form({
		title: new sap.ui.core.Title({
			text: (bEdit == 1) ? oLng_MasterData.getText("MasterData_ModifyPProgram") : oLng_MasterData.getText("MasterData_NewPProgram"),
			/*"Modifica Part Program":"Nuovo Part Program",*/
		}),
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: oCmbPlantInput
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_LineID"), //"ID Linea",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						editable: !(bEdit == 1),
						fields: oCmbPPLine.setEnabled(!(bEdit == 1))
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Machine"), //"Macchina",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						editable: !(bEdit == 1),
						fields: oCmbPPMach.setEnabled(!(bEdit == 1))
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_MaterialCode"), //"Materiale",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
                            oTxtMaterial,
                            new sap.ui.commons.Button("btnMatFilter", {
                                icon: "sap-icon://arrow-down",
                                enabled: !(bEdit == 1),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                                press: function (){
                                    fnGetMaterialTableDialog(
                                        $.UIbyID("filtPlant").getSelectedKey() === "" ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
                                        "",
                                        setSelectedMaterial,
                                        false,
                                        false
                                    );
                                }
                            })
                        ]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Version"), //"Versione",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "VERSION") : "",
								editable: !(bEdit == 1),
								maxLength: 3,
								id: "txtVERSION"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Alt"), //"ALT",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit == 1) ? fnGetCellValue("PartProgramTab", "ALT") : "",
								editable: false,
								maxLength: 11,
								id: "txtALT",
								liveChange: function (ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Default"), //"Default",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.CheckBox({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								checked: (bEdit == 1) ? (fnGetCellValue("PartProgramTab", "DEFAULT") == "0" ? false : true) : false,
								id: "chkDEFAULT"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_PartProgram"), //"Part Program",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.unified.FileUploader({
                               layoutData: new sap.ui.layout.form.GridElementData({hCells: "5"}),
                                value: oLng_MasterData.getText("MasterData_UploadFile"), //"Caricare File",
                                editable: true,
                                id: "txtPPROGRAM",
                                multiple: false,
                                maximumFileSize: 20,
                                //mimeType: "image,text,*",
                                //fileType: ["jpg","png","txt","*"],
                                uploadOnChange: false,
                                uploadUrl: "",
                                //uploadUrl: "/XMII/CM/Dev-Area/PartProgram/partProgramUpload_1.jsp",
			                    change : function(e){
                                    var file = e.getParameter("files") && e.getParameter("files")[0];
                                    if(file && window.FileReader){  
                                        var reader = new FileReader();  
                                        var that = this;  
                                        reader.onload = function(evn) {  
                                            testCSV= btoa(evn.target.result); //string in CSV 
                                        };
                                        reader.readAsText(file);  
                                    }
                                },
                                fileSizeExceed: function (oEvent) {
                                    var sName = oEvent.getParameter("fileName");
                                    var fSize = oEvent.getParameter("fileSize");
                                    var fLimit = $.UIbyID("txtPPROGRAM").getMaximumFileSize();
                                    sap.ui.commons.MessageBox.show(
                                        oLng_MasterData.getText("MasterData_FileOutOfSize") //"La dimensione massima di un file è di MB:"
                                        + " " + fSize
                                    );
                                },
                                typeMissmatch: function (oEvent) {
                                    var sName = oEvent.getParameter("fileName");
                                    var sType = oEvent.getParameter("fileType");
                                    var sMimeType = $.UIbyID("txtPPROGRAM").getMimeType();
                                    if (!sMimeType) {
                                        sMimeType = $.UIbyID("txtPPROGRAM").getFileType();
                                    }
                                    sap.ui.commons.MessageBox.show(
                                        oLng_MasterData.getText("MasterData_AllowedFileTypes") //"Tipi di file consentiti:"
                                        + " " + sMimeType
                                    );
                                },
                                uploadComplete: function (oEvent) {
                                        if((bEdit == 1)){
                                            fnSavePartProgram(testCSV);
                                        }
                                        else{
                                            fnAddPartProgram(testCSV);
                                        }
                                }
                            })
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_PartProgramName"), //"Nome programma",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "PRGNUM") : "",
								editable: true,
								maxLength: 50,
								id: "txtPRGNUM"
							})
						]
					}), new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_PartProgramVar"), //"Variabili programma",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit != 0) ? fnGetCellValue("PartProgramTab", "PRGVAR") : "",
								editable: true,
								maxLength: 50,
								id: "txtPRGVAR"
							})
						]
					}), new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit == 1) ? fnGetCellValue("PartProgramTab", "DESCR") : "",
								editable: true,
								maxLength: 30,
								id: "txtDESCR"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
							new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "auto"
								}),
								value: (bEdit == 1) ? fnGetCellValue("PartProgramTab", "NOTE") : "",
								editable: true,
								maxLength: 60,
								id: "txtNOTE"
							})
						]
					})
				]
			})
		]
	});

	oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
		press: function () {
			$.UIbyID("txtPPROGRAM").upload();
			/*
			if((bEdit == 1)){
			fnSavePartProgram();
			}
			else{
			fnAddPartProgram();
			}
			*/
		}
	}));
	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
		press: function () {
			oEdtDlg.close();
			oEdtDlg.destroy();
			oCmbPlantInput.destroy();
			oCmbPPLine.destroy();
			oCmbPPMach.destroy();
		}
	}));
	oEdtDlg.open();
}

// Funzione per aggiungere un part program
function fnAddPartProgram(sPartProgram) {
	var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
	var idLine = $.UIbyID("cmbPPLine").getSelectedKey();
	if (idLine == "" || idLine == null) {
		sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
		return;
	}
	var idMach = $.UIbyID("cmbPPMach").getSelectedKey();
	if (idMach == "" || idMach == null) {
		sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
		return;
	}
	var sMaterial = ($.UIbyID("txtMaterial").data("SelectedKey") === null) ? "" : $.UIbyID("txtMaterial").data("SelectedKey");
	if (sMaterial == "" || sMaterial == null) {
		sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
		return;
	}
	var sVersion = $.UIbyID("txtVERSION").getValue();
	if (sVersion == "" || sVersion == null) {
		sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
		return;
	}
	var sAlt = -1;
	var bDefault = $.UIbyID("chkDEFAULT").getChecked() === true ? "1" : "0";
	var sPProgram = sPartProgram;
	var sPrgNum = $.UIbyID("txtPRGNUM").getValue();
	var sPrgVar = $.UIbyID("txtPRGVAR").getValue();
	var sDescription = $.UIbyID("txtDESCR").getValue();
	var sNote = $.UIbyID("txtNOTE").getValue();

	var qexe = dataProdMD +
			"PartProgram/addPartProgramQR" +
			"&Param.1=" + sMaterial +
			"&Param.2=" + idLine +
			"&Param.3=" + plant +
			"&Param.4=" + idMach +
			"&Param.5=" + sVersion +
			"&Param.6=" + sAlt +
			"&Param.7=" + bDefault +
			"&Param.8=" + sPProgram +
			"&Param.9=" + sDescription +
			"&Param.10=" + sNote +
			"&Param.11=" + sPrgNum +
			"&Param.12=" + sPrgVar;

	var ret = 1;
	var aAddResult = fnGetAjaxVal(qexe, ["addResult", "calculatedALT"], false);
	const iCheckResult = parseInt(aAddResult.addResult);
	sAlt = parseInt(aAddResult.calculatedALT);

	if (iCheckResult === 2) {
		sap.ui.commons.MessageBox.show(
			oLng_MasterData.getText("MasterData_PartProgramAlreadyActive"), //"C'è già un Part Program attivo per questa versione. Attivare questo in fase d'inserimento?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			oLng_MasterData.getText("MasterData_ActivationConfirm"), //"Conferma attivazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
			function (sResult) {
				if (sResult == 'YES') {
					var qexe2 = dataProdMD + "PartProgram/updatePartProgramDefaultStatusQR" +
							"&Param.1=" + sMaterial +
							"&Param.2=" + idLine +
							"&Param.3=" + plant +
							"&Param.4=" + idMach +
							"&Param.5=" + sVersion +
							"&Param.6=" + sAlt;

					ret = fnSQLQuery(
						qexe2,
						oLng_MasterData.getText("MasterData_PartProgramSavedActivated"), //"Part Program aggiunto correttamente e attivato",
						oLng_MasterData.getText("MasterData_PartProgramSaved"), //"Part Program aggiunto correttamente",
						false
					);
					refreshTabPartProgram();
				}
			},
			sap.ui.commons.MessageBox.Action.YES
		);
	} else {
		sap.ui.commons.MessageBox.show(
			oLng_MasterData.getText("MasterData_PartProgramSaved"), //Part Program aggiunto correttamente",
			sap.ui.commons.MessageBox.Icon.INFORMATION,
			oLng_MasterData.getText("MasterData_Saving"), //"Salvataggio",
			[sap.ui.commons.MessageBox.Action.OK],
			function (sResult) {},
			sap.ui.commons.MessageBox.Action.OK
		);
	}

	if (ret) {
		$.UIbyID("dlgPartProgram").close();
		$.UIbyID("dlgPartProgram").destroy();
		refreshTabPartProgram();
	}
}

// Funzione per salvare le modifiche alla causale o inserirla
function fnSavePartProgram(sPartProgram) {
	var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
	var idLine = $.UIbyID("cmbPPLine").getSelectedKey();
	var idMach = $.UIbyID("cmbPPMach").getSelectedKey();
	var sMaterial = ($.UIbyID("txtMaterial").data("SelectedKey") === null) ? "" : $.UIbyID("txtMaterial").data("SelectedKey");
	var sVersion = $.UIbyID("txtVERSION").getValue();
	var sAlt = $.UIbyID("txtALT").getValue();
	var bDefault = $.UIbyID("chkDEFAULT").getChecked() === true ? "1" : "0";
	var sPProgram = sPartProgram.trim();
	var sPrgNum = $.UIbyID("txtPRGNUM").getValue();
	var sPrgVar = $.UIbyID("txtPRGVAR").getValue();
	var sDescription = $.UIbyID("txtDESCR").getValue();
	var sNote = $.UIbyID("txtNOTE").getValue();

	var qexe = dataProdMD +
			"PartProgram/updatePartProgramQR" +
			"&Param.1=" + sMaterial +
			"&Param.2=" + idLine +
			"&Param.3=" + plant +
			"&Param.4=" + idMach +
			"&Param.5=" + sVersion +
			"&Param.6=" + sAlt +
			"&Param.7=" + bDefault +
			"&Param.8=" + sPProgram +
			"&Param.9=" + sDescription +
			"&Param.10=" + sNote +
			"&Param.11=" + sPrgNum +
			"&Param.12=" + encodeURIComponent(sPrgVar);

	var ret = 1;
	//var iCheckResult = parseInt(fnGetAjaxVal(qexe,["updateResult"],false));
	var aUpdResult = fnGetAjaxVal(qexe, ["updateResult"], false);
	const iCheckResult = parseInt(aUpdResult.updateResult);

	if (iCheckResult === 2) {
		sap.ui.commons.MessageBox.show(
			oLng_MasterData.getText("MasterData_PartProgramAlreadyActiveModify"), //"C'è già un Part Program attivo per questa versione. Attivare questo in fase di modifica?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			oLng_MasterData.getText("MasterData_ActivationConfirm"), //"Conferma attivazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
			function (sResult) {
				if (sResult == 'YES') {
					var qexe2 = dataProdMD + "PartProgram/updatePartProgramDefaultStatusQR" +
							"&Param.1=" + sMaterial +
							"&Param.2=" + idLine +
							"&Param.3=" + plant +
							"&Param.4=" + idMach +
							"&Param.5=" + sVersion +
							"&Param.6=" + sAlt;

					ret = fnSQLQuery(
						qexe2,
						oLng_MasterData.getText("MasterData_PartProgramUpdatedActivated"), //"Part Program modificato correttamente e attivato",
						oLng_MasterData.getText("MasterData_PartProgramUpdated"), //"Part Program modificato correttamente",
						false
					);
					refreshTabPartProgram();
				}
			},
			sap.ui.commons.MessageBox.Action.YES
		);
	} else {
		sap.ui.commons.MessageBox.show(
			oLng_MasterData.getText("MasterData_PartProgramUpdated"), //"Part Program modificato correttamente",
			sap.ui.commons.MessageBox.Icon.INFORMATION,
			oLng_MasterData.getText("MasterData_Saving"), //"Salvataggio",
			[sap.ui.commons.MessageBox.Action.OK],
			function (sResult) {},
			sap.ui.commons.MessageBox.Action.OK
		);
	}

	if (ret) {
		$.UIbyID("dlgPartProgram").close();
		$.UIbyID("dlgPartProgram").destroy();
		refreshTabPartProgram();
	}
}

function fnDelPProgram() {

	sap.ui.commons.MessageBox.show(
		oLng_MasterData.getText("MasterData_PProgramDeleting"), //"Eliminare il Part Program selezionato?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
		[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdMD + "PartProgram/delPartProgramSQ" +
						"&Param.1=" + fnGetCellValue("PartProgramTab", "MATERIAL") +
						"&Param.2=" + fnGetCellValue("PartProgramTab", "IDLINE") +
						"&Param.3=" + fnGetCellValue("PartProgramTab", "PLANT") +
						"&Param.4=" + fnGetCellValue("PartProgramTab", "IDMACH") +
						"&Param.5=" + fnGetCellValue("PartProgramTab", "VERSION") +
						"&Param.6=" + fnGetCellValue("PartProgramTab", "ALT");
				var ret = fnSQLQuery(
					qexe,
					oLng_MasterData.getText("MasterData_PProgramDeleted"), //"Part Program eliminato correttamente",
					oLng_MasterData.getText("MasterData_PProgramDeletedError"), //"Errore in eliminazione Part Program",
					false
				);
				refreshTabPartProgram();
				$.UIbyID("btnModPartProgram").setEnabled(false);
				$.UIbyID("btnDelPartProgram").setEnabled(false);
				$.UIbyID("PartProgramTab").setSelectedIndex(-1);
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}

function fnShowPProgram() {

	var qexe = dataProdMD +
			"PartProgram/getPartProgramTextByIdSQ" +
			"&Param.1=" + fnGetCellValue("PartProgramTab", "MATERIAL") +
			"&Param.2=" + fnGetCellValue("PartProgramTab", "IDLINE") +
			"&Param.3=" + fnGetCellValue("PartProgramTab", "PLANT") +
			"&Param.4=" + fnGetCellValue("PartProgramTab", "IDMACH") +
			"&Param.5=" + fnGetCellValue("PartProgramTab", "VERSION") +
			"&Param.6=" + fnGetCellValue("PartProgramTab", "ALT");

	var sPProgram = fnGetAjaxVal(qexe, "PPROGRAM", false);

	//  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgPartProgramText",
		maxWidth: "1024px",
		Width: "900px",
		maxHeight: "600px",
		Height: "500px",
		showCloseButton: false
	});

	var pProgramArea = new sap.ui.commons.TextArea({
		rows: 30,
		cols: 180,
		editable: false,
		value: sPProgram
	});

	oEdtDlg.addContent(pProgramArea);
	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_MasterData.getText("MasterData_Close"), //"Chiudi",
		press: function () {
			oEdtDlg.close();
			oEdtDlg.destroy()
		}
	}));
	oEdtDlg.open();
}

function sendPartProgram() {

	sap.ui.commons.MessageBox.show(
		"Inviare il part program selezionato?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		"Invio part program", [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdPP +
						"PartProgram/SendPP_QR" +
						"&Param.1=" + fnGetCellValue("PartProgramTab", "PLANT") +
						"&Param.2=" + fnGetCellValue("PartProgramTab", "MATERIAL") +
						"&Param.3=" + fnGetCellValue("PartProgramTab", "IDLINE") +
						"&Param.4=" + fnGetCellValue("PartProgramTab", "IDMACH") +
						"&Param.5=" + fnGetCellValue("PartProgramTab", "VERSION") +
						"&Param.6=" + fnGetCellValue("PartProgramTab", "ALT");

				var ret = 1;
				var aAddResult = fnGetAjaxVal(qexe, ["Result_code", "Result_Description"], false);
				const iCheckResult = parseInt(aAddResult.Result_code);
				var resultDescription = aAddResult.Result_Description;

				if (iCheckResult === 0) {
					sap.ui.commons.MessageBox.show(
						oLng_MasterData.getText("MasterData_PartProgramSentOK"),
						sap.ui.commons.MessageBox.Icon.SUCCESS,
						oLng_MasterData.getText("MasterData_PartProgramSentTitleOK"), [sap.ui.commons.MessageBox.Action.OK],
						function (sResult) {},
						sap.ui.commons.MessageBox.Action.OK
					);
				} else {
					sap.ui.commons.MessageBox.show(
						resultDescription,
						sap.ui.commons.MessageBox.Icon.ERROR,
						oLng_MasterData.getText("MasterData_PartProgramSentTitleKO"), [sap.ui.commons.MessageBox.Action.OK],
						function (sResult) {},
						sap.ui.commons.MessageBox.Action.OK
					);
				}
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}

function setSelectedMaterial(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sMaterialCode = oControlEvent.getSource().getModel().getProperty("MATERIAL", oSelContext);
	var sMaterialDesc = oControlEvent.getSource().getModel().getProperty("MATERIAL", oSelContext);
	$.UIbyID("txtMaterial").setValue(sMaterialDesc).data("SelectedKey", sMaterialCode);
	$.UIbyID("dlgMaterialTableSelect").destroy();
}