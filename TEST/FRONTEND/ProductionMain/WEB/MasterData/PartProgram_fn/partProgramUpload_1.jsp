<%@ page import="java.io.*" %>
<%@ page import="com.sap.lhcommon.util.Base64Util" %>

<%
	String saveFile = "";
	String sTruncatePosition = "";
	String sTitle = "";
	String contentType = request.getContentType();
	if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
		DataInputStream in = new DataInputStream(request.getInputStream());
		int formDataLength = request.getContentLength();
		byte dataBytes[] = new byte[formDataLength];
		int byteRead = 0;
		int totalBytesRead = 0;
		while (totalBytesRead < formDataLength) {
			byteRead = in.read(dataBytes, totalBytesRead, formDataLength - totalBytesRead);
			totalBytesRead += byteRead;
		}

		String file = new String(dataBytes);

		sTruncatePosition = file.substring(0, file.indexOf("\n"));
		if (file.indexOf("text/plain") != -1) {
			saveFile = file.substring(file.indexOf("text/plain") + 13);	
		}
		if (file.indexOf("application/vnd.ms-excel") != -1) {
			saveFile = file.substring(file.indexOf("application/vnd.ms-excel") + 24);
		}
		if (file.indexOf("text/csv") != -1) {
			saveFile = file.substring(file.indexOf("text/csv") + 8);
		}
		saveFile = saveFile.substring(0, saveFile.indexOf(sTruncatePosition));

		String base64String = new String(Base64Util.encode(saveFile), "UTF-8");

		String fileName = new String(saveFile);
		int extIndex = fileName.indexOf(".");
		fileName = fileName.substring(extIndex + 1);
		String fileOption = fileName.toUpperCase();

		out.println(base64String.trim());
	}
%>