"use strict";

var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Grstreasi
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Grstrease e ritorna l'oggetto oTable
function createTabGrstreas() {
        
    //Crea L'oggetto Tabella Grstrease
    var oTable = UI5Utils.init_UI5_Table({
        id: "GrstreasTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_GrstreasList"), //"Lista macro causali fermo",
            visibleRowCount: 15,
            width: "100%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("GrstreasTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModGrstreas").setEnabled(false);
        			$.UIbyID("btnDelGrstreas").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModGrstreas").setEnabled(true);
                    $.UIbyID("btnDelGrstreas").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddGrstreas",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openGrstreasEdit(false, "INSERT");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModGrstreas",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openGrstreasEdit(true, "UPDATE");
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelGrstreas",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelGrstreas();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabGrstreas();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "80px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1", 
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "80px",
                    visible: false
				}
            },
            {
				Field: "STTYPE",
				label: oLng_MasterData.getText("MasterData_MacroStopReasons"), //"Macro causali fermo",
				properties: {
					width: "50px"
				}
            },
            {
				Field: "REASTXT", 
				label: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "DESCR", 
				label: oLng_MasterData.getText("MasterData_ExtendedText"), //"Testo esteso",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "ENABLED",// in Sigit è "ACTIVE",
				label: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            }
        ],
    });
    
    var oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabGrstreas();
    return oTable;
}

// Aggiorna la tabella Grstreas
function refreshTabGrstreas() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("GrstreasTab").getModel().loadData(
            QService + dataProdMD +
            "Grstreas/getGrstreasByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("GrstreasTab").getModel().loadData(
            QService + dataProdMD +
            "Grstreas/getGrstreasByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("GrstreasTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Grstreasi
function openGrstreasEdit(bEdit, mode) {
    if (bEdit === undefined) bEdit = false;
    if (mode === undefined) mode = "";
    
   // contenitore dati lista divisioni
	var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        editable: false,
        selectedKey : bEdit?fnGetCellValue("GrstreasTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey()
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgGrstreas",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("GrstreasTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyGrstreas"):oLng_MasterData.getText("MasterData_NewGrstreas"),
            /*"Modifica Grstrease":"Nuovo Grstrease",*/
            //icon: "/images/address.gif",
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MacroStopReasons"), //"Macro causali fermo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("GrstreasTab", "STTYPE"):"",
                                editable: !bEdit,
                                maxLength: 6,
                                id: "STTYPE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("GrstreasTab", "REASTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "REASTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("GrstreasTab", "DESCR"):"",
                                editable: true,
                                maxLength: 60,
                                id: "DESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("GrstreasTab", "ENABLED") == "0" ? false : true ) : true,
                                /*bEdit?(fnGetCellValue("GrstreasTab", "ACTIVE") == "0" ? false : true ) : false,*/
                                id: "ENABLED" /*"ACTIVE"*/
                            })
                        ]
                    })
                ]
            })
        ]});
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveGrstreas(mode);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveGrstreas(mode) 
{
    if ( mode === 'undefined' ) mode = "";

    var STTYPE = $.UIbyID("STTYPE").getValue(); 
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var REASTXT = $.UIbyID("REASTXT").getValue();
    var DESCR = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";

    if ( STTYPE === "" || plant === "" ) {
        sap.ui.commons.MessageBox.show(
            oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        return;
    }

    var qexe = "";
    switch( mode ) {
        case "INSERT":
            qexe = dataProdMD +"Grstreas/addGrstreasSQ";
            break;
        case "UPDATE":
            qexe = dataProdMD +"Grstreas/updGrstreasSQ";
            break;
    };

    qexe += "&Param.1=" + plant +
                "&Param.2=" + STTYPE +
                "&Param.3=" + REASTXT +
                "&Param.4=" + DESCR +
                "&Param.5=" + active;
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_GrstreasSaved"),
                         oLng_MasterData.getText("MasterData_GrstreasSavedError")
                         /*"Grstrease salvato", "Errore salvataggio Grstrease"*/,
                         false);
    if (ret){
        $.UIbyID("dlgGrstreas").close();
        $.UIbyID("dlgGrstreas").destroy();
        refreshTabGrstreas();
    }
}

function fnDelGrstreas(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_GrstreasDeleting"), //"Eliminare l'Grstrease selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult === 'YES'){
                var qexe = dataProdMD + "Grstreas/delGrstreasSQ" +
                            "&Param.1=" + fnGetCellValue("GrstreasTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("GrstreasTab", "STTYPE");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_GrstreasDeleted"), //"Grstrease eliminato correttamente",
                    oLng_MasterData.getText("MasterData_GrstreasDeletedError"), //"Errore in eliminazione Grstrease",
                    false
                );
                refreshTabGrstreas();
                $.UIbyID("btnModGrstreas").setEnabled(false);
                $.UIbyID("btnDelGrstreas").setEnabled(false);
                 $.UIbyID("GrstreasTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}




