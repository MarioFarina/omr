// Funzioni base per Interfacce MII
//*******************************************
// Ver: 1.1.23 2018-04-05
//*******************************************

//Variabili Globali
$ = jQuery.noConflict();

jQuery.sap.require("sap.ui.commons.MessageBox");
jQuery.sap.require("sap.m.BusyDialog");
jQuery.sap.require("sap.m.MessageBox");

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Common = jQuery.sap.resources({
	url: "/XMII/CM/Common/res/common.i18n.properties",
	locale: sCurrentLocale
});

jQuery.extend({
	/**
	 * Funzione per aggiunta dinamica di uno script js.
	 * Utile al posto della standard JQuery per debug e rilevazione problemi codice
	 *
	 * @param   {String} url      URL Script da caricare
	 * @param   {Function} callback Funzione di callbak
	 * @returns {Object} undefined
	 */
	getScript2: function (url, callback) {
		var head = document.getElementsByTagName("head")[0];
		var script = document.createElement("script");
		script.src = url;

		// Handle Script loading
		{
			var done = false;

			// Attach handlers for all browsers
			script.onload = script.onreadystatechange = function () {
				if (!done && (!this.readyState ||
											this.readyState == "loaded" || this.readyState == "complete")) {
					done = true;
					if (callback)
						callback();

					// Handle memory leak in IE
					script.onload = script.onreadystatechange = null;
				}
			};
		}

		head.appendChild(script);

		// We handle everything using the script element injection
		return undefined;
	}
});

var Libraries = {

	// base path for JS libraries
	//_sJSLibsPath: "/XMII/CM/" + _PROJECT_PATH + "/utils/js/",
	// base path for CSS libraries
	//_sCSSLibsPath: "/XMII/CM/" + _PROJECT_PATH + "/utils/css/",
	// base path for pages
	//_sPagePath: "/XMII/CM/" + _PROJECT_PATH + "/pages/",
	// base path for function to be used in the pages
	//_sPagesFnPath: "/XMII/CM/" + _PROJECT_PATH + "/pages/libs/",
	// list of JS libraries to be loaded
	//_aJSLibs: ["AjaxUtils", "DOMUtils", "GENUtils", "LBLUtils"],
	// list of CSS libraries to be loaded
	//_aCSSLibs: ["sapui5", "matrica"],


	/**
	 * Load the libraries and the requested page
	 * @memberOf Libraries
	 */
	init: function() {
		// load libraries and page
		this._loadCSSLibs(this._aCSSLibs);
	},


	/**
	 * Recursively load all CSS libraries
	 * @param {array} aLibs - array of CSS libraries
	 * @memberOf Libraries
	 */
	_loadCSSLibs: function(aLibs) {

		// sap libraries required
		jQuery.sap.require("sap.ui.core.util.Export");
		jQuery.sap.require("sap.ui.core.util.ExportTypeCSV");
		jQuery.sap.require("sap.ui.core.format.NumberFormat");
		jQuery.sap.require("sap.ui.core.format.DateFormat");

		var self = this;
		// check if there are any libs in the array
		if( aLibs.length > 0 ) {
			// get the first lib
			var sLib = aLibs[0];
			// load the lib
			jQuery.sap.includeStyleSheet(
				// script path
				sLib + ".css?v=" + Date.now(),
				// script id
				sLib,
				// success callback
				function() {
					// recursively call the function removing the loaded lib from the list
					self._loadCSSLibs(aLibs.splice(1));
				},
				// error callback
				function() { }
			);

		} else {
			// load all JS libraries and page
			this._loadJSLibs(this._aJSLibs);
		}

	}, // end _loadCSSLibs


	/**
	 * Recursively load all JS libraries
	 * @param {array} aLibs - array of JS libraries
	 * @memberOf Libraries
	 */
	_loadJSLibs: function(aLibs) {

		// sap libraries required
		//		jQuery.sap.require("sap.ui.core.util.Export");
		//		jQuery.sap.require("sap.ui.core.util.ExportTypeCSV");

		var self = this;
		// check if there are any libs in the array
		if( aLibs.length > 0 ) {
			// get the first lib
			var sLib = aLibs[0];
			// load the lib
			jQuery.sap.includeScript(
				// script path
				sLib + ".js?v=" + Date.now(),
				// script id
				sLib,
				// success callback
				function() {
					// recursively call the function removing the loaded lib from the list
					self._loadJSLibs(aLibs.splice(1));
				},
				// error callback
				function() { }
			);

		} else {

		}

	}, // end _loadJSLibs


	/**
	 * Recursively load dependencies for the specific page
	 * @param {array} aLibs - array of JS functions
	 * @param {function} callback - function to be called after libraries are loaded
	 */
	load: function(aLibs, callback) {
		var self = this;
		// check if there are any libs in the array
		if( aLibs.length > 0 ) {
			// get the first lib
			var sLib = aLibs[0];
			// load the lib
			jQuery.sap.includeScript(
				// script path
				sLib + ".js?v=" + Date.now(),
				// script id
				sLib,
				// success callback
				function() {
					// recursively call the function removing the loaded lib from the list
					if (typeof callback != "undefined") self.load( aLibs.splice(1), callback );
					else self.load( aLibs.splice(1));
				},
				// error callback
				function() {
					if (typeof callback != "undefined") self.load( aLibs, callback );
					else self.load( aLibs);
					if (window.console) console.error("Errore nel caricare script" + sLib + ".js");
				}
			);

		} else {
			// execute callback
			if (typeof callback != "undefined"){
				callback();
			}
		}
	}

};

jQuery.sap.includeScript("/XMII/CM/Common/js/jquery.timer.js?v=1.0");
jQuery.sap.includeScript("/XMII/CM/Common/js/jquery.format-1.3.min.js?v=1.0");
jQuery.sap.includeScript("/XMII/CM/Common/js/jquery-ui-1.10.4.custom.min.js?v=1.0");
//jQuery.getScript("/XMII/CM/Common/js/jquery.xpath.min.js");
//jQuery.sap.includeScript("/XMII/CM/Common/js/pivot.js?v=1.0");
//jQuery.sap.includeStyleSheet("/XMII/CM/Common/CSS/pivot.css?v=1.0");
//jQuery.sap.includeScript("/XMII/CM/Common/js/pivot.it.js?v=1.0");
/**
 * Restituisce l'oggetto OpenUi5 in base all'ID
 * (Da usare $.UIbyID("SapUiId") )
 * @param   {String} SapUiId ID oggetto
 * @returns {Object} Oggetto OpenUi5
 */
jQuery.UIbyID = function (SapUiId) {
	try {
		var objID = sap.ui.getCore().byId(SapUiId);
		if ( objID === undefined) {
			if (window.console) console.warn("Oggetto con ID =" + SapUiId +  " non esistente!");
			return ;
		}
		else return objID;
	}

	catch(err) {
		if (window.console) console.warn("Oggetto con ID =" + SapUiId +  " non esistente!");
		return ;
	}
};


/**
 * Gestione e visualizzazione della versione:
 */
var sVersion;
try {
	var oVersionInfo = sap.ui.getVersionInfo();
	sVersion = oVersionInfo.version || sap.ui.version;
} catch (ex) {
	sVersion = sap.ui.version;
}
if (window.console) console.info("Versione UI5 = " + sVersion);



/**
 * Esegue il logOut
 */
function fnLogOut() {
	window.location = "/XMII/Illuminator?service=Logout";
}

/**
 * inizializza l'intestazione
 * @param {String} sTitle    Titolo della pagina
 * @param {String} sDiv      ID del tag Div. defualt pagHeader
 * @param {Boolean} bShowUser Mostra il nome utente collegato
 * @param {Boolean} bShowHead Mostra l'intestazione completa
 */
function fnHeaderPage(sTitle, sDiv, bShowUser, bShowHead) {

	var oTitleLang = jQuery.sap.resources({
		url: "/XMII/CM/Common/MII_core/res/pagesTitle.i18n.properties",
		locale: sCurrentLocale
	});

	document.getElementsByTagName("title")[0].innerHTML = oTitleLang.getText(sTitle);

	var MIIParams = fnGetAjaxData("Content-Type=text/XML&QueryTemplate=Common/getConfig", false);

	if (MIIParams == undefined) location.reload(true);

	//var logoSrc = $(MIIParams).find('Row[Name="Folder"]').children("value").text() + $(MIIParams).find('Row[Name="Folder"]').children("value").text() ;
	var sFolder = "";
	var sLogo = "";
	var sFavicon = "";
	$(MIIParams).find('Row').each(function (i, row) {
		if ($(row).children("name").text() == 'Folder') sFolder = $(row).children("value").text();
		if ($(row).children("name").text() == 'Logo') sLogo = $(row).children("value").text();
		if ($(row).children("name").text() == 'FavIcon') sFavicon = $(row).children("value").text();
	});

	var logoSrc = sFolder + sLogo;
	var iconSrc = sFolder + sFavicon;
	$("#favicon").attr("href", iconSrc);

	if (sDiv == undefined || sDiv == null) sDiv = "pagHeader";
	if (bShowUser == undefined || bShowUser == null) bShowUser = true;
	if (bShowHead == undefined || bShowHead == null) bShowHead = true;

	if (bShowHead) {
		var oAppHeader = new sap.ui.commons.ApplicationHeader("appHeader");
		oAppHeader.setLogoSrc(logoSrc);
		oAppHeader.setLogoText(oTitleLang.getText(sTitle));

		var curUser = fnGetAjaxVal("service=admin&mode=UserAttribList&content-type=text/xml", "FullName", false)
		oAppHeader.setDisplayWelcome(bShowUser);
		oAppHeader.setUserName(curUser);

		//configure the log off area
		oAppHeader.setDisplayLogoff(true);
		oAppHeader.attachLogoff(this, fnLogOut, this);
		oAppHeader.placeAt(sDiv);
	}
}

/**inizializza il footer di pagina
 * @param {String} sDiv ID del tag Div. defualt pagFooter
 */
function fnFooterPage(sDiv) {
	if (sDiv == undefined || sDiv.typeOf() == 'undefined') sDiv = "pagFooter";

	var oNavigationBar = new sap.ui.ux3.NavigationBar({
		items:[
			new sap.ui.ux3.NavigationItem({key: "start", text: oLng_Common.getText("Common_InitialPage")}),
			new sap.ui.ux3.NavigationItem({key: "menu", text: oLng_Common.getText("Common_Menu")}),
		],
		//		selectedItem:oNavigationItem3,
		select: function(oEvent) {
			oEvent.preventDefault();
			if (oEvent.getParameter("item").getKey() == "start") {
				window.location = "/XMII/goService.jsp";
			}
			if (oEvent.getParameter("item").getKey() == "menu") {
				$.UIbyID("dlgMenu").open();
				return;
			}
		}
	});
	CreateDialogMenu();

	//Create 2 divider instances
	var oDivider0 = new sap.ui.commons.HorizontalDivider("divider0", {
		width: "100%",
		type: "Area",
		height: "Medium"
	});
	var oDivider1 = new sap.ui.commons.HorizontalDivider("divider1", {
		width: "99%",
		type: "Area",
		height: "Small"
	});
	var oDivider2 = new sap.ui.commons.HorizontalDivider("divider2", {
		width: "98%",
		type: "Page",
		height: "Small"
	});
	var oLayout = new sap.ui.layout.form.GridLayout();

	var oLink1 = new sap.ui.commons.Link({
		text: oLng_Common.getText("Common_InitialPage"),
		tooltip: oLng_Common.getText("Common_GoToFirstPage"),

		href: "/XMII/goService.jsp"
	});

	oDivider0.placeAt(sDiv);
	oNavigationBar.placeAt(sDiv);
	oDivider1.placeAt(sDiv);
	oDivider2.placeAt(sDiv);

}

/**inizializza la dialog del menù
 */
function CreateDialogMenu() {

	if (!$.UIbyID("dlgMenu")) {
		//Create an instance of the table control
		var oTable = new sap.ui.table.TreeTable({
			id: "treeMenu",
			columns: [
				new sap.ui.table.Column({
					label: oLng_Common.getText("Common_ApplicationMenu"),
					template: new sap.ui.commons.Link({
						press: function() {}
					}).bindProperty("text", "name").bindProperty("href", "description")
				})
			],
			selectionMode: sap.ui.table.SelectionMode.None,
			enableColumnReordering: true,
			expandFirstLevel: false,
			visibleRowCount: 20,
			Width: "80%",
			Height: "90%",
			//minHeight: "550px",
		});

		var oModel = new sap.ui.model.json.JSONModel();
		oTable.setModel(oModel);

		var oDetDlg = new sap.ui.commons.Dialog({
			id: "dlgMenu",
			modal: true,
			resizable: true,
			title: oLng_Common.getText("Common_MIImenu"),
			Width: "80%",
			maxWidth: "800px",
			minHeight: "600px",
			//Width: "600px",
			Height: "80%",
			showCloseButton: true
		});

		oDetDlg.addContent(oTable);
	}

	var qParams = {
		data: "service=admin&mode=CurrentProfile&Content-Type=text/xml"   ,
		url: "/XMII/Illuminator",
		dataType: "xml"
	};
	//debugger

	var data = fnGetAjaxData(
		"Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/Functions/getDFunctionsByUserQR&Param.1=" + sLanguage,false
	);

    var i = 0;
	var sArr = [];
	$.each(data.childNodes[0].childNodes[0].childNodes, function (idx, item) {
		//var fName = item.attributes['Name'].textContent;
		//var fDesc = item.attributes['Description'].textContent;
		if (item.nodeName == "Row"){
			var sParent = item.childNodes[3].textContent;
			if(sParent == ""){
				console.log(item.nodeName + ": " + item.textContent + " Figli: " +item.hasChildNodes());
                console.log(sLanguage);
				sArr[i]=( explodeNavItem (data, item));
				i = i + 1;
			}
		}
	});

	var sData = {
		root: {
			name: "root",
			description: "root description",
			checked: true,
			0: sArr
		}
	};

	console.log(sData);

	$.UIbyID("treeMenu").getModel().setData(sData);
	$.UIbyID("treeMenu").bindRows("/root");
};


function explodeNavItem (data, itemNode) {

	var sNodeID = itemNode.childNodes[0].textContent;
	var sNodeName = itemNode.childNodes[1].textContent;
	var sDescription = itemNode.childNodes[2].textContent;

	var CurNode = {
		name: sNodeName,
		description: sDescription, //itemNode.attributes['target'].textContent,
		checked: true
	};

	$.each(data.childNodes[0].childNodes[0].childNodes, function (idx, item) {
		if (item.nodeName == "Row"){
			var sParent = item.childNodes[3].textContent;
			if(sParent == sNodeID){
				console.log(item.nodeName + ": " + item.textContent + " Figli: " +item.hasChildNodes());
				CurNode[idx] =( explodeNavItem (data, item));
			}
		}
	});
	//jSonData.push(CurNode);
	return CurNode;
}
//funzioni originali
/*
function CreateDialogMenu() {

	if (!$.UIbyID("dlgMenu")) {

	//Create an instance of the table control
	var oTable = new sap.ui.table.TreeTable({
		id: "treeMenu",
		columns: [
			new sap.ui.table.Column({label: "Menù applicativo", template: new sap.ui.commons.Link({
				press: function() {}}).bindProperty("text", "name").bindProperty("href", "description")})
		],
		selectionMode: sap.ui.table.SelectionMode.None,
		enableColumnReordering: true,
		expandFirstLevel: false,
		visibleRowCount: 20,
		Width: "80%",
		Height: "90%",
		//minHeight: "550px",
	});

	var oModel = new sap.ui.model.json.JSONModel();
		oTable.setModel(oModel);

		var oDetDlg = new sap.ui.commons.Dialog(
			{
				id: "dlgMenu",
				modal: true,
				resizable: true,
				title: "Menù MII : ",
				Width: "80%",
				maxWidth: "800px",
				minHeight: "600px",
				//Width: "600px",
				Height: "80%",
				showCloseButton: true
			});
		oDetDlg.addContent(oTable);

	}


	var qParams = {
		data: "service=admin&mode=CurrentProfile&Content-Type=text/xml"   ,
		url: "/XMII/Illuminator",
		dataType: "xml"
	};
	//debugger;
	var data = fnGetAjaxData("service=admin&mode=CurrentProfile&Content-Type=text/xml",false);


		var i = 0;
		var sArr = []
		$.each(data.childNodes[0].childNodes[0].childNodes, function (idx, item) {
			//var fName = item.attributes['Name'].textContent;
			//var fDesc = item.attributes['Description'].textContent;
			if (item.nodeName == "NavigationItem"){
				//console.log(item.nodeName + ": " + item.textContent + " Figli: " +item.hasChildNodes());
				sArr[i]=( explodeNavItem (item));
				i = i + 1;
			}
		});
		var sData = {root: {name: "root",
												description: "root description",
												checked: true,
												0: sArr}};

		console.log(sData);

	$.UIbyID("treeMenu").getModel().setData(sData);
	$.UIbyID("treeMenu").bindRows("/root");


};


function explodeNavItem (itemNode) {
	console.log(itemNode.attributes['label'].textContent + " -> " + itemNode.attributes['target'].textContent);
	var CurNode = { name: itemNode.attributes['label'].textContent,
								 description: itemNode.attributes['target'].textContent,
								 checked: true
								};
	if (itemNode.hasChildNodes()) {
		$.each(itemNode.childNodes, function (idx, item) {
			if (item.nodeName == "NavigationItem"){
				CurNode[idx] = explodeNavItem (item);
			}
		});
	}

	//jSonData.push(CurNode);
	return CurNode;
}
*/
/**
 * Restituisce un parametro da una query Ajax
 * @param   {String} query     Query MII con parametri
 * @param   {String} or {[String]}	params Nome del/i campo/i da restituire
 * @param   {Boolean} bAsync    Esecuzione asincrona. Default false
 * @returns {String} or {[String]} Valore/i campo/i
 */
function fnGetAjaxVal(query, params, bAsync) {
	var dataRet;
	$.ajax({
		"url": "/XMII/Illuminator",
		"data": query,
		"async": bAsync,
		"dataType": "xml",
		"type": "POST",
		"cache": false,
		"success": function (data, textStatus) {
			if(typeof(params) === 'object')
			{
				var aRetProperties;
				var bRetData = false;
				dataRet = {};
				if(Array.isArray(params))
					aRetProperties = params;
				else
				{
					aRetProperties = params.returnProperties;
					bRetData = params.returnXMLDoc;
				}

				if(bRetData)
					dataRet['data'] = data;

				for(var i=0; i<aRetProperties.length; i++)
					dataRet[aRetProperties[i]] = $(data).find('Row').first().children(aRetProperties[i]).text();
			}
			else
				dataRet = $(data).find('Row').children(params).text();
		},
		"error": function(jqXHR, textStatus, errorThrown) {
			if(window.console) console.error(textStatus,errorThrown,jqXHR);
			if (typeof(jqXHR.responseText) != "undefined")  {
				if (jqXHR.responseText.indexOf( "User Management") > 0) location.reload(true);
			}
		}
	});
	return dataRet;
}

/**
 * Restituisce i dati in tabella da una query Ajax
 * @param   {String} query  Query MII con parametri
 * @param   {Boolean} basync   Esecuzione asincrona. Default false
 * @returns {Object} Risultato esecuzione
 */
function fnGetAjaxData(query, basync) {
	var DateRet;
	$.ajax({
		"url": "/XMII/Illuminator",
		"data": query,
		"async": basync,
		"dataType": "xml",
		"type": "POST",
		"cache": false,
		"success": function (data, textStatus) {
			DateRet = data;
			// if (data == undefined) location.reload(true);

		}
	});
	return DateRet;
}

/**
 * Funzione per esecuzione Query Xacute in MII, centralizzata con gestione messaggi
 * @param   {String} qParams  Query MII con parametri
 * @param   {String} tSuccess Testo per risutlato ok
 * @param   {String} tError   Testo per risultato fallito
 * @param   {Boolean} basync   Esecuzione asincrona. Default false
 * @returns {Boolean} Risultato esecuzione
 */
function fnExeQuery(qParams, tSuccess, tError, basync) {
	if (basync == null || basync == undefined) basync = false;
	var exeRet = false;
	$.ajax({
		"url": "/XMII/Illuminator",
		"data": qParams,
		"dataType": "xml",
		"type": "POST",
		"async": basync,
		"cache": false,
		"complete": function (data, textStatus) {
			var serr = $(data.responseText).find('FatalError').text();
			if (textStatus == "success" && serr.length == 0) {
				if ($.trim(tSuccess) != '') sap.ui.commons.MessageBox.alert(tSuccess);
				exeRet = true;
			} else {

				if ($.trim(serr) != '') sap.ui.commons.MessageBox.show(serr, sap.ui.commons.MessageBox.Icon.ERROR);
				else sap.ui.commons.MessageBox.show(tError, sap.ui.commons.MessageBox.Icon.ERROR);
				exeRet = false;
			}

		}
	});
	if (!basync) return exeRet;
}

/**
 * Funzione per esecuzione Query SQL in MII, centralizzata con gestione messaggi mobile
 * @param   {String} qParams  Query MII con parametri
 * @param   {String} tSuccess Testo per risutlato ok
 * @param   {String} tError   Testo per risultato fallito
 * @param   {Boolean} basync   Esecuzione asincrona. Default false
 * @returns {Boolean} Risultato esecuzione
 */
function fnExeQuerym(qParams, tSuccess, tError, basync) {
	if (basync == null || basync == undefined) basync = false;
	var exeRet = false;
	$.ajax({
		"url": "/XMII/Illuminator",
		"data": qParams,
		"dataType": "xml",
		"type": "POST",
		"async": basync,
		"cache": false,
		"complete": function (data, textStatus) {
			var serr = $(data.responseText).find('FatalError').text();
			if (textStatus == "success" && serr.length == 0) {
				if ($.trim(tSuccess) != '') sap.m.MessageToast.show(tSuccess, {
					duration: 800
				});
				exeRet = true;
			} else {

				if ($.trim(serr) != '') sap.m.MessageBox.alert(serr, {
					title: oLng_Common.getText("Common_ExecutionError"),
					icon: sap.m.MessageBox.Icon.ERROR
				});
				else sap.ui.commons.MessageBox.show(tError, {
					title: oLng_Common.getText("Common_ExecutionError"),
					icon: sap.m.MessageBox.Icon.ERROR
				});
				exeRet = false;
			}
		},
		"error": function(jqXHR, textStatus, errorThrown) {
			if(window.console) console.error(textStatus,errorThrown,jqXHR);
			if (typeof(jqXHR.responseText) != "undefined")  {
				if (jqXHR.responseText.indexOf( "User Management") > 0) location.reload(true);
			}
		}
	});
	if (!basync) return exeRet;
}

/**
 * Funzione per esecuzione Query SQL in MII, centralizzata con gestione messaggi
 * @param   {String} qParams  Query MII con parametri
 * @param   {String} tSuccess Testo per risutlato ok
 * @param   {String} tError   Testo per risultato fallito
 * @param   {Boolean} basync   Esecuzione asincrona. Default false
 * @returns {Boolean} Risultato esecuzione
 */
function fnSQLQuery(qParams, tSuccess, tError, basync) {
	if (basync == null || basync == undefined) basync = false;
	var exeRet = false;
	$.ajax({
		"url": "/XMII/Illuminator",
		"data": qParams,
		"dataType": "xml",
		"type": "POST",
		"async": basync,
		"cache": false,
		"complete": function (data, textStatus) {
			var serr = $(data.responseText).find('FatalError').text();
			if (textStatus == "success" && serr.length > 0) {
				if (serr.indexOf("SQLServerException") > 0 && serr.indexOf("PRIMARY KEY") > 0) {
					sap.ui.commons.MessageBox.show(oLng_Common.getText("Common_KeyAlreadyExists"), sap.ui.commons.MessageBox.Icon.ERROR);
				} else sap.ui.commons.MessageBox.show(serr, sap.ui.commons.MessageBox.Icon.ERROR);
				exeRet = false;
			} else if (textStatus == "success") {
				if ($.trim(tSuccess) != '') sap.ui.commons.MessageBox.alert(tSuccess);
				exeRet = true;
			} else {

				if ($.trim(tError) != '') sap.ui.commons.MessageBox.show(tError, sap.ui.commons.MessageBox.Icon.ERROR);
				exeRet = false;
			}

		}

	});
	if (!basync) return exeRet;
}

/**
 * carica (serializzando) i dati di un model
 * @param {String} oModel [[Description]]
 * @param {String} query  [[Description]]
 * @param {Boolean} basync [[Description]]
 */
function fnGetTableData(oModel, query, basync) {
	if (basync == null || basync == undefined) basync = false;
	var data = fnGetAjaxData(query, basync);
	if (data == undefined) location.reload(true);
	oModel.setXML(data.xml ? data.xml : (new XMLSerializer()).serializeToString(data));
}

/**
 * Converte documento XML in JSON
 * @param xml : documento XML
 * @returns: oggetto JSON
 */
function xml2json(xml) {
	var obj = [];
	var rowsets = xml.getElementsByTagName("Rowsets");
	var rowset = rowsets[0].getElementsByTagName("Rowset");
	var row = rowset[0].getElementsByTagName("Row")
	for (var i = 0; i < row.length; i++) {
		var itm = {}
		var children = row[i].children;
		for (var j = 0; j < children.length; j++) {
			itm[children[j].nodeName] = children[j].textContent;
		}
		obj.push(itm);
	}
	return obj;

} // function xml2json( xml )

/**
 * Converte una Query MII da XML a Json
 * @param   {String} query  Url con parametri della query MII
 * @param   {Boolean} basync True per esecuzione asincrona, default false
 * @returns {String} Contenuto joson della query
 */
function fnGetJsonData(query, basync) {
	if (basync == null || basync == undefined) basync = false;
	var xmlData = fnGetAjaxData(query, basync);
	var aTitles = [];
	var aTypes = [];
	var data = {
		Rows: []
	};

	$(xmlData).find("Column").each(function (idx, item) {

		aTitles.push($(item).attr('Name'));
		aTypes.push($(item).attr('SQLDataType'));
	});

	$(xmlData).find("Row").each(function (idx, row) {

		jsonRow = {};
		$(aTitles).each(function (idx, item) {
			switch (aTypes[idx]) {
				case "1":
					jsonRow[item] = $(row).find(item).text();
					break;
				case "4":
					jsonRow[item] = parseInt($(row).find(item).text());
					break;
				case "93":
					jsonRow[item] = $(row).find(item).text();
					break;
				case "8":
					jsonRow[item] = parseFloat($(row).find(item).text());
					break;
				default:
					jsonRow[item] = $(row).find(item).text();
					break;
												 }

		});
		data.Rows.push(jsonRow);
	});
	return data;
}

function fnGetJsonPivot(query, basync) {
	if (basync == null || basync == undefined) basync = false;
	var xmlData = fnGetAjaxData(query, basync);
	var aTitles = [];
	var aTypes = [];
	var data = [];

	$(xmlData).find("Column").each(function (idx, item) {

		aTitles.push($(item).attr('Name'));
		aTypes.push($(item).attr('SQLDataType'));
	});

	$(xmlData).find("Row").each(function (idx, row) {
		jsonRow = {};
		$(aTitles).each(function (idx, item) {
			switch (aTypes[idx]) {
				case "1":
					jsonRow[item] = $(row).find(item).text();
					break;
				case "3":
					jsonRow[item] = parseInt($(row).find(item).text());
					break;
				case "4":
					jsonRow[item] = parseInt($(row).find(item).text());
					break;
				case "93":
					jsonRow[item] = $(row).find(item).text();
					break;
				case "8":
					jsonRow[item] = parseFloat($(row).find(item).text());
					break;
				default:
					jsonRow[item] = $(row).find(item).text();
					break;
												 }

		});
		data.push(jsonRow);
	});
	//console.log (data);
	return data;
}

/**
 * Converte il formato dataora XML yyyy-MM-ddTHH:mm:nn in dataOra leggibile dd/MM/yyyy HH:mm:nn
 * @param   {String} sDate DataOra da convertire
 * @returns {String} DataOra convertita
 */
function fnConvNumeric(svalue) {
	if (svalue == null || svalue == undefined) return "";

	if (svalue.length == 0) return "";

	return parseFloat(svalue).toLocaleString();
}

/**
 * Converte il formato dataora XML yyyy-MM-ddTHH:mm:nn in dataOra leggibile dd/MM/yyyy HH:mm:nn
 * @param   {String} sDate DataOra da convertire
 * @returns {String} DataOra convertita
 */
function fnConvXMLDateTime(sDate) {
	if (sDate == null || sDate == undefined) return "";
	//var ret = sDate.replace("T", " ");
	//ret = $.format.date(ret, 'yyyy-dd-MM HH:mm:ss');
	//ret2 = $.format.date(ret, 'dd/MM/yyyy HH:mm:ss');
	if (sDate.length <= 5) return "";
	ret = sDate.substr(8, 2) + '/' + sDate.substr(5, 2) + '/' + sDate.substr(0, 4) + ' ' + sDate.substr(11, 8);
	return ret;
}

/**
 Converte il formato dataora XML yyyy-MM-ddTHH:mm:nn in data leggibile dd/MM/yyyy
 * @param   {String} sDate DataOra da convertire
 * @returns {String} Data convertita
 */
function fnConvXMLDate(sDate) {
	if (sDate == null || sDate == undefined) return "";
	//var ret = sDate.replace("T00:00:00", " ");
	//ret = $.format.date(ret, 'yyyy-dd-MM');
	//ret = $.format.date(ret, 'dd/MM/yyyy');
	if (sDate.length <= 5) return "";
	ret = sDate.substr(8, 2) + '/' + sDate.substr(5, 2) + '/' + sDate.substr(0, 4);
	return ret.toString();
}

/**
 Converte il formato dataora XML yyyy-MM-ddTHH:mm:nn in ora leggibile HH:mm:nn
 * @param   {String} sDate DataOra da convertire
 * @returns {String} Ora convertita
 */
function fnConvXMLTime(sDate) {
	if (sDate == null || sDate == undefined) return "";
	//var ret = sDate.replace("T00:00:00", " ");
	//ret = $.format.date(ret, 'yyyy-dd-MM');
	//ret = $.format.date(ret, 'dd/MM/yyyy');
	if (sDate.length <= 5) return sDate;
	ret = sDate.substr(11, 8);
	return ret.toString();
}

/**
 * Funzione per leggere il valore di una cella da una tabella
 * @param   {String} sTable     ID Della tabella
 * @param   {String} sColumnBind Nome del campo della tabella
 * @param   {Number} lRow        Riga della tabella, se manca prende la riga corrente
 * @returns {String} Valore del campo letto
 */
function fnGetCellValue(sTable, sColumnBind, lRow) {
	//|| lRow.typeOf() == 'undefined'
	if (lRow == undefined) lRow = $.UIbyID(sTable).getSelectedIndex();
	var tabContext = $.UIbyID(sTable).getContextByIndex(lRow);
	if (window.console) console.debug("Context -> " + tabContext + " -> " + sColumnBind + " -> " + $.UIbyID(sTable).getModel().getProperty(sColumnBind, tabContext));
	return $.UIbyID(sTable).getModel().getProperty(sColumnBind, tabContext);
}

/**
 * Funzione per scrivere il valore di una cella da una tabella
 * @param   {String} sTable      ID Della tabella
 * @param   {String} sColumnBind Nome del campo della tabella
 * @param {String} sValue        Valore del campo da scrivere
 * @param {[[Type]]} lRow        Riga della tabella, se manca prende la riga corrente
 */
function fnSetCellValue(sTable, sColumnBind, sValue, lRow) {
	//|| lRow.typeOf() == 'undefined'
	if (lRow == undefined) lRow = $.UIbyID(sTable).getSelectedIndex();
	var tabContext = $.UIbyID(sTable).getContextByIndex(lRow);
	if (window.console) console.debug("Context -> " + tabContext + " -> " + sColumnBind + " -> " + $.UIbyID(sTable).getModel().setProperty(sColumnBind, tabContext));
	//return
	$.UIbyID(sTable).getModel().setProperty(sColumnBind, sValue, tabContext);
}

/**
 * Funzione per ottenere il valore di un campo del model di un dataset
 * @param   {String} sDataset    ID del dataset
 * @param   {String} sColumnBind Nome del campo
 * @param   {Number} idx         Indice del dataset selezionato
 * @param   {String} ModelPath   Il percorso/context del campo
 * @returns {String} il valore del campo
 */
function fnGetDataSetVal(sDataset, sColumnBind, idx, ModelPath) {
	if (ModelPath == null || ModelPath == undefined) ModelPath = "/Rowset/Row";
	var valRet = $.UIbyID(sDataset).getModel().getProperty(ModelPath + "/" + idx + "/" + sColumnBind);
	return valRet;
}

/**
 * Funzione per ottenere il valore di un campo del model di una combo dal suo ID
 * @param   {String} sCombo      ID della combobox
 * @param   {String} sColumnBind Nome del campo
 * @returns {[[Type]]} Valore della chiave selezionata
 */
function fnGetCombofield(sCombo, sColumnBind) {
	if (window.console) console.log("SKid -> " + $.UIbyID(sCombo).getSelectedItemId());
	var selId = $.UIbyID(sCombo).getSelectedItemId();
	sid = selId.indexOf(sCombo);
	sid = selId.indexOf("-", sid);
	var idx = selId.substring(sid + 1);
	if (window.console) console.debug("context Search -> " + "/Rowset/Row/" + idx + "/" + sColumnBind);

	var valRet = $.UIbyID(sCombo).getModel().getProperty("/Rowset/Row/" + idx + "/" + sColumnBind);
	if (window.console) console.debug("Result -> " + valRet);
	return valRet;
}

/**
 * Funzione per ottenere il valore di un campo di un datamodel generico
 * @param   {Object} oModel      Odetto model Data
 * @param   {String} sColumnBind Nome del campo
 * @param   {String} idx         Indice della riga selezionata
 * @param   {String} ModelPath   Il percorso/context del campo
 * @returns {String} il valore del campo
 */
function fnGetModelVal(oModel, sColumnBind, idx, ModelPath) {
	if (ModelPath == null || ModelPath == undefined) ModelPath = "/Rowset/Row";
	var valRet = oModel.getProperty(ModelPath + "/" + idx + "/" + sColumnBind);
	return valRet;
}

/**
 * Funzione semplice per creare una colonna per un oggetto sap.ui.table.Table
 * @param   {String} label Etichetta colonna
 * @param   {String} field Nome campo del model
 * @param   {String} width Lunghezza
 * @returns {Object} Oggetto colonna creata
 */
function addColumn(label, field, width) {
	var oColumn = new sap.ui.table.Column({
		label: new sap.ui.commons.Label({
			text: label
		}),
		template: new sap.ui.commons.TextView().bindProperty("text", field),
		sortProperty: field,
		filterProperty: field,
		width: width
	});
	return oColumn;
}

/**
 * Funzione estesa per aggiungere una colonna a un oggetto sap.ui.table.Table
 * @param   {String} oTable    ID della tabella
 * @param   {String} label     Etichetta colonna
 * @param   {String} field     Nome campo del model
 * @param   {String} width     Lunghezza
 * @param   {String} fieldType Tipo di visu. default TextView, oppure DateTime, Date, Time, Image, Icon, checked, TextField
 * @param   {Boolean} flex      Colonna flessibile. Delaut false
 * @param   {String} align     Tipo di allineamento. default begin, oppure left, rigth, end, center
 * @param   {String} toolTip   Campo model per il ToolTip della colonna, default field
 * @returns {Object} Oggetto colonna creato
 */
function addColumnEx(oTable, label, field, width, fieldType, flex, align, toolTip) {
	var oAlign;
	if (fieldType == null || fieldType == undefined) {
		fieldType = "TextView";
	}

	if (width == null || width == undefined) {
		width = "100px";
	}

	if (flex == null || flex == undefined) {
		flex = true;
	}

	if (align == null || align == undefined) {
		oAlign = sap.ui.core.TextAlign.Begin;
	} else {
		switch (align) {
			case "left":
				oAlign = sap.ui.core.TextAlign.Left;
				break;
			case "rigth":
				oAlign = sap.ui.core.TextAlign.Right;
				break;
			case "end":
				oAlign = sap.ui.core.TextAlign.End;
				break;
			case "begin":
				oAlign = sap.ui.core.TextAlign.Begin;
				break;
			case "center":
				oAlign = sap.ui.core.TextAlign.Center;
				break;
			default:
				oAlign = sap.ui.core.TextAlign.Begin;
								 }
	}

	if (toolTip == null || toolTip == undefined) {
		toolTip = "none";
	}

	var oField;
	switch (fieldType) {
		case "TextField":
			var CMod = function (oEvent){
				var fieldName = field ; //oEvent.getSource().getBinding("value");
				var newVal = oEvent.getParameters().liveValue;
				fnSetCellValue(oTable.getId(), fieldName,newVal);
			}
			var oTextField = new sap.ui.commons.TextField({width: width,liveChange:CMod }).bindProperty("value", field);
			oTextField.setTextAlign(oAlign);
			//liveChange
			oField = new sap.ui.commons.InPlaceEdit({content: oTextField});
			break;
		case "TextView":
			oField = new sap.ui.commons.TextView().bindProperty("text", field).bindProperty("tooltip", toolTip);
			oField.setTextAlign(oAlign);
			break;
		case "DateTime":
			oField = new sap.ui.commons.TextView().bindProperty("text", field);
			oField.bindText(field, function (value) {
				return fnConvXMLDateTime(value);
			});
			oField.setTextAlign(oAlign);
			break;
		case "Date":
			oField = new sap.ui.commons.TextView().bindProperty("text", field);
			oField.bindText(field, function (value) {
				return fnConvXMLDate(value);
			});
			break;
		case "Numeric":
			oField = new sap.ui.commons.TextView().bindProperty("text", field, function (value) {
				return fnConvNumeric(value);
			});
			oField.setTextAlign(oAlign);
			break;
		case "Time":
			oField = new sap.ui.commons.TextView().bindProperty("text", field);
			oField.bindText(field, function (value) {
				return fnConvXMLTime(value);
			});
			oField.setTextAlign(oAlign);
			break;
		case "Image":
			oField = new sap.ui.commons.Image({
				width: "20px"
			}).bindProperty("src", field);
			break;
		case "Icon":
			oField = new sap.ui.core.Icon({
				width: "20px"
			}).bindProperty("src", field);
			break;
		case "checked":
			//oField = new sap.ui.commons.CheckBox().bindProperty("checked", field);
			oField = new sap.ui.commons.CheckBox().bindChecked(field, function (value) {
				if (value === 'true' || value === '1') return true;
				else return false;
			});
			oField.setEditable(false);
			break;
		default:
			break;
									 }


	var oColumn = new sap.ui.table.Column(oTable.getId() + "-" + field, {
		label: new sap.ui.commons.Label({
			text: label,
			textAlign: oAlign
		}),
		name: fieldType + "-" + field,
		template: oField,
		sortProperty: field,
		filterProperty: field,
		width: width,
		flexible: flex,
		hAlign: sap.ui.core.HorizontalAlign.Center
	});

	oTable.addColumn(oColumn);
	return oColumn;
}

/**
 * Funzione per aggiungere una colonna mobile a un oggetto sap.m.table.Table
 * @param   {String} oTable    ID della tabella
 * @param   {String} label     Etichetta colonna
 * @param   {String} field     Nome campo del model
 * @param   {String} width     Lunghezza
 * @param   {String} fieldType Tipo di visu. default TextView. Oppure checked
 * @returns {Object}  Oggetto colonna creato
 */
function addmColumnEx(oTable, label, field, width, fieldType) {
	if (fieldType == null || fieldType == undefined) {
		fieldType = "TextView";
	}

	if (width == null || width == undefined) {
		width = "100px";
	}

	var oField;
	switch (fieldType) {
		case "TextView":
			oField = new sap.m.Text().bindProperty("text", field);
			break;
		case "checked":
			//oField = new sap.ui.commons.CheckBox().bindProperty("checked", field);
			oField = new sap.m.CheckBox().bindChecked(field, function (value) {
				if (value === 'true') return true;
				else return false;
			});
			oField.setEditable(false);
			break;
		default:

			break;
									 }

	var oColumn = new sap.m.Column({
		header: new sap.m.Label({
			text: label,
			width: width,
			textAlign: sap.ui.core.TextAlign.Right
		}),
		template: oField,
		sortProperty: field,
		filterProperty: field,
		width: width
	});

	oTable.addColumn(oColumn);
}

/**
 * Funzione per ottenere il valore di una cella da una tabella mobile
 * @param   {String} sTable      ID tabella
 * @param   {String} sColumnBind Nome del campo
 * @param   {String} RowContext  Il percorso/context del campo
 * @returns {String} il valore del campo
 */
function fnGetMCellValue(sTable, sColumnBind, RowContext) {
	return $.UIbyID(sTable).getModel().getProperty(sColumnBind, RowContext);
}

/**
 * Permette di esportare il contenuto di una tabella in CSV
 * @param {String} sTable ID della tabella
 * @param {String} sFName Nome del file da esportare
 */
function exportTableToCSV(sTable, sFName) {

	var csvText = "";
	try {
		csvText = generateTableCSV(sTable);
	} catch (err) {
		var jsonData = $.UIbyID(sTable).getModel().getJSON();
		csvText += json2csv(jsonData)
	}

	//console.log(csvText)
	var oLink = createDownloadLink(csvText, sFName);

	var layout = new sap.ui.commons.layout.VerticalLayout("layout", {
		content: [oLink]
	});

	//  Crea la finestra di dialogo
	var oExpDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgExport",
		maxWidth: "900px",
		Width: "900px",
		showCloseButton: false
	});
	oExpDlg.addContent(layout);
	oExpDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Common.getText("Common_Close"),
		press: function () {
			oExpDlg.close();
			oExpDlg.destroy()
		}
	}));
	oExpDlg.open();
	$("#linkExportCsv").attr('download', sFName + '.csv');

}

/**
 * Restituisce il numero di righe contenute in un model di una tabella
 * @param   {String} sTable ID della tabella
 * @returns {Number} Numero record
 */
function getTableRowsCount (sTable) {
	var xmlData = $.UIbyID(sTable).getModel().getXML();

	return $(xmlData).find("Row").length;
}


// Funzioni interne della libreria
/**
 * Export table header and data into a CSV string.
 */
function generateTableCSV(sTable) {
	var info = '';

	for (var i = 0; i < $.UIbyID(sTable).getColumns().length; i++) {
		//info+= encodeURIComponent($.UIbyID(sTable).getColumns()[i].getLabel().getText()) + ';';
		info += $.UIbyID(sTable).getColumns()[i].getLabel().getText() + ';';
		if (window.console) console.debug($.UIbyID(sTable).getColumns()[i].getTemplate().getMetadata().getName());
	}

	info += '\r\n';

	var xmlData = $.UIbyID(sTable).getModel().getXML();

	$(xmlData).find("Row").each(function () {
		for (var i = 0; i < $.UIbyID(sTable).getColumns().length; i++) {
			var valor = "";
			if ($.UIbyID(sTable).getColumns()[i].getTemplate() != undefined) {
				// && $.UIbyID(sTable).getColumns()[i].getTemplate().getBinding('text') != undefined) {
				var control = $.UIbyID(sTable).getColumns()[i].getTemplate().getMetadata().getName();
				var fieldType = $.UIbyID(sTable).getColumns()[i].getTemplate().data("fieldType");
				switch (control) {
					case 'sap.ui.commons.TextView':
						if ($.UIbyID(sTable).getColumns()[i].getName().substring(0, 8) == "DateTime" ||	fieldType == "DateTime")
						{
							valor = fnConvXMLDateTime($(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text());
						} else if ($.UIbyID(sTable).getColumns()[i].getName().substring(0, 4) == "Date" || fieldType == "Date") {
							valor = fnConvXMLDate($(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text());
						} else valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();

						break;
					case 'sap.ui.commons.Image':
						//valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
						break;
					case 'sap.ui.commons.CheckBox':
						valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
						if (valor === 'true' || valor === '1') valor = "SI";
						else valor = "NO";
						break;
					default:

						break;
											 }
				//var valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
				//info += encodeURIComponent(valor) + ';';
				info += valor + ';';
			}
		}
		info += '\r\n';
	});

	/*var Rows = $.UIbyID(sTable).getRows();

	$.each(Rows, function(index,Row){
		for (var i =0; i< $.UIbyID(sTable).getColumns().length; i++) {
			if ($.UIbyID(sTable).getColumns()[i].getTemplate() != undefined &&
			$.UIbyID(sTable).getColumns()[i].getTemplate().getBinding('text') != undefined) {
	var valor = fnGetCellValue (sTable, $.UIbyID(sTable).getColumns()[i].getFilterProperty(), Row.getIndex());
	//info += encodeURIComponent(valor) + ';';
	info += valor + ';';
			}
		}
			info += '\r\n';
	});*/

	return info;
}

function generateTableXLS(sTable) {
	var info = '';
	info += "<tr>"
	for (var i = 0; i < $.UIbyID(sTable).getColumns().length; i++) {
		//info+= encodeURIComponent($.UIbyID(sTable).getColumns()[i].getLabel().getText()) + ';';
		info += "<td>" + $.UIbyID(sTable).getColumns()[i].getLabel().getText() + "</td>\n";
		if (window.console) console.debug($.UIbyID(sTable).getColumns()[i].getTemplate().getMetadata().getName());
	}

	info += '</tr>\n';

	var xmlData = $.UIbyID(sTable).getModel().getXML();

	$(xmlData).find("Row").each(function () {
		info += "<tr>"
		for (var i = 0; i < $.UIbyID(sTable).getColumns().length; i++) {
			var valor = "";
			if ($.UIbyID(sTable).getColumns()[i].getTemplate() != undefined) {
				// && $.UIbyID(sTable).getColumns()[i].getTemplate().getBinding('text') != undefined) {
				var control = $.UIbyID(sTable).getColumns()[i].getTemplate().getMetadata().getName()
				switch (control) {
					case 'sap.ui.commons.TextView':
						if ($.UIbyID(sTable).getColumns()[i].getName().substring(0, 8) == "DateTime") {
							valor = fnConvXMLDateTime($(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text());
						} else if ($.UIbyID(sTable).getColumns()[i].getName().substring(0, 4) == "Date") {
							valor = fnConvXMLDate($(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text());
						} else valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();

						break;
					case 'sap.ui.commons.Image':
						//valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
						break;
					case 'sap.ui.commons.CheckBox':
						valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
						if (valor === 'true' || valor === '1') valor = "SI";
						else valor = "NO";
						break;
					default:

						break;
											 }
				//var valor = $(this).find($.UIbyID(sTable).getColumns()[i].getFilterProperty()).text();
				//info += encodeURIComponent(valor) + ';';
				info +=  "<td>" + valor +  "</td\n>";
			}
		}
		info += "</tr>\n";
	});

	/*var Rows = $.UIbyID(sTable).getRows();

	$.each(Rows, function(index,Row){
		for (var i =0; i< $.UIbyID(sTable).getColumns().length; i++) {
			if ($.UIbyID(sTable).getColumns()[i].getTemplate() != undefined &&
			$.UIbyID(sTable).getColumns()[i].getTemplate().getBinding('text') != undefined) {
	var valor = fnGetCellValue (sTable, $.UIbyID(sTable).getColumns()[i].getFilterProperty(), Row.getIndex());
	//info += encodeURIComponent(valor) + ';';
	info += valor + ';';
			}
		}
			info += '\r\n';
	});*/

	return info;
}
/**
 * Creates a link target to base64 data
 */
function createDownloadLink(b64text, sFName) {
	var oLink = new sap.ui.commons.Link("linkExportCsv", {
		text: 'Download as CSV',
		href: 'data:application/csv;charset=utf-8;base64,' + (Base64.encode(b64text))
	});

	initDownloadAttr(sFName);

	return oLink;
}

/**
 * Creates download attribute to set filename
 */

function initDownloadAttr(sFName) {
	//if ($( "#linkExportCsv" ).length > 0) {
	$("#linkExportCsv").attr('download', sFName + '.csv');
	//} else {
	//setTimeout(initDownloadAttr, 1000);
	//}
}

/**
 *
 *  Base64 encode / decode
 *  http://www.webtoolkit.info/
 *
 **/
var Base64 = {

	// private property
	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode: function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {


			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
				this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
				this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}
		return output;

	},

	// public method for decoding
	decode: function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}


		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode: function (string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {

				utftext += String.fromCharCode(c);

			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode: function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while (i < utftext.length) {

			c = utftext.charCodeAt(i);


			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
};

/**
 * Funzione per trasformare da JSON a CSV
 * @param objArray : oggetto o array JSON
 * @param head : se true scrive intestazione
 * @param sep : separatore
 * @returns: stringa CSV
 */
function json2csv(objArray, head, sep) {
	if (head == undefined) head = true;
	if (sep == undefined) sep = ";";

	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

	var str = '';

	if (head == true) {
		var line = '';
		var header = Object.keys(array[0]);
		for (var i in header) {
			line += header[i] + sep;
		}

		str += line + '\r\n';
	}

	for (var i = 0; i < array.length; i++) {
		var line = '';

		for (var index in array[i]) {
			line += array[i][index] + sep;
		}

		line.slice(0, line.Length - 1);

		str += line + '\r\n';
	}

	return str;
}

/**
 * Funzione che restituisce un valore true o false se l'utente fa parte del gruppo passato come parametro
 * @param gruppo : gruppo da controllare
 * @returns: true o false
 */
function getUserAuth(gruppo) {
	var result = false;
	var dataProdComm = "Content-Type=text/XML&QueryTemplate=ProductionMain/Common/BasicComponents/";
	var qexe =  dataProdComm + "getUserInfoQR";

	if (gruppo == undefined) return result;

	var data = fnGetAjaxData(qexe, false);
	$.each(data.childNodes[0].childNodes[0].childNodes, function (idx, item) {
			if (item.nodeName == "Row") {
				if(item.childNodes[0].textContent == "IllumLoginRoles") {
					if((item.childNodes[1].textContent).indexOf(gruppo) != -1) {
						result = true;
					}
				}
			}
	});
	return result;
}
