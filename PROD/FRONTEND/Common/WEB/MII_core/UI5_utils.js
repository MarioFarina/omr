// Funzioni base per gestione avanzata tabelle SapUI5/OpenUI5
//*******************************************
// Ver: 1.0.34
//*******************************************

/**
 * @class UI5Utils
 * @static
 */

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Common = jQuery.sap.resources({
	url: "/XMII/CM/Common/res/common.i18n.properties",
	locale: sCurrentLocale
});

var UI5Utils = {

	// default properties for the 'sap.ui.table.Table' object
	_defaultTableProperties: {
		visibleRowCount: 25,
		fixedColumnCount: 0,
		firstVisibleRow: 1,
		rowHeight: 24,
		//visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Auto,
		visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
		selectionMode: sap.ui.table.SelectionMode.Single,
		navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
		enableColumnReordering: false,
		editable: false,
		width: "100%",
		height:"100%",
		minHeight:"400px",
		minWidth: "800px",
		columnResize: function (oControlEvent) {
			//oControlEvent.preventDefault();
		}
	},

	// default properties for the 'sap.ui.table.Column' object
	_defaultColumnProperties: {
		visible: true,
		width: "200px",
		hAlign: sap.ui.core.HorizontalAlign.Begin,
		autoResizable: true,
		headerSpan: 1
	},

	// Default Properties for Ajax call
	_defaultAjaxData: {
		contentType: "text/xml",
		dataType: "xml"
	},

	// Default Properties for Ajax TimeOut
	_ajaxTimeout: 240000,

	// define the
	_busyDialog: new sap.m.BusyDialog({
		size:'40px'
	}),  //{customIcon: 'images/synchronise_48.png'}

	/**
	 * Open the busy dialog
	 *
	 * @method openBusyDialog
	 * @memberOf UI5Utils
	 */
	openBusyDialog: function() {
		this._busyDialog.open();
	}, // end openBusyDialog

	/**
	 * Close the busy dialog
	 *
	 * @method closeBusyDialog
	 * @memberOf UI5Utils
	 */
	closeBusyDialog: function() {
		this._busyDialog.close();
	}, // end closeBusyDialog

	/**
	 * Init a new 'sap.ui.table.Table' object
	 * @method init_UI5_Table
	 * @param {object} oParams
	 * @param {string} oParams.id - id of the new table
	 * @param {object} oParams.properties - Standard sap.ui.table.Table properties
	 * @param {boolean} oParams.zebra - enable alternate row
	 * @param {string} oParams.modelType - defaul XML
	 * @param {string} oParams.bindRows  - default "/Rowset/0/Row"
	 * @param {object} oParams.properties - table properties object
	 * @param {boolean} oParams.exportButton - button for export table
	 * @param {object} oParams.toolbarItems - Toolbar items array object
	 (more info at {@link https://sapui5.netweaver.ondemand.com/sdk/docs/api/symbols/sap.ui.table.Table.html#constructor})
	 * @param {array} oParams.columns - list of column objects
	 * @param {object} oParams.column - column object (more info at {@link UI5Utils.addColumn})
	 * @return {object} - sap.ui.table.Table
	 * @memberOf UI5Utils
	 */
	init_UI5_Table: function(oParams) {

		// check if 'oParams.properties' is specified, otherwise set it as an emoty object
		if (typeof(oParams.properties) == "undefined") {
			oParams.properties = {};
		}

		// append default attributes to the 'oParams.properties' object
		for (var attr in this._defaultTableProperties) {
			// do not overwrite an existing attribute
			if(typeof(oParams.properties[attr]) == "undefined") {
				oParams.properties[attr] = this._defaultTableProperties[attr];
			}
		}

		//create an instance of the table control
		var oTable;
		if(typeof(oParams.id)=="undefined") {
			oTable = new sap.ui.table.Table(oParams.properties);
		} else {
			oTable = new sap.ui.table.Table(oParams.id,oParams.properties);
		}

		// loop over columns an add them to the table
		for (var index in oParams.columns) {
			this.addColumn({
				table: oTable,
				column: oParams.columns[index]
			});
		}

		if(typeof(oParams.modelType)=="undefined") {
			oParams.modelType = "XML";}

		if(typeof(oParams.bindRows)=="undefined") {
			oParams.bindRows = "/Rowset/0/Row";}

		if (oParams.modelType == 'XML') {
			var oModel = new sap.ui.model.xml.XMLModel();
			oTable.setModel(oModel);
			oTable.bindRows(oParams.bindRows );
		}
		else {
			var oModel = new sap.ui.model.json.JSONModel();
			oTable.setModel(oModel);
			oTable.bindRows(oParams.bindRows );
		}

		if(typeof(oParams.exportButton)=="undefined") {
			oParams.exportButton = true;}

		if(typeof(oParams.zebra)=="undefined") {
			oParams.zebra = true;}


		if(typeof(oParams.sumColCount)=="undefined") {
			oParams.sumColCount = -1;}

		if (oParams.zebra) 	oTable.addStyleClass('tableZebra');

		if(typeof(oParams.toolbarItems)!="undefined") {
			oTable.setToolbar( new sap.ui.commons.Toolbar(
				{items: [oParams.toolbarItems]}));
		}

		if (oParams.exportButton){
			var eBtn = new sap.ui.commons.Button(
				{
					text: oLng_Common.getText("Common_Export"),
					icon: "sap-icon://action",
					enabled: true,
					press: function ()
					{
						exportTableToCSV(oTable.getId(),"DataTable");
					}
				});
			if(typeof(oParams.toolbarItems)=="undefined"){
				oTable.setToolbar( new sap.ui.commons.Toolbar().addRightItem(eBtn));
			}
			else {
				oTable.getToolbar().addRightItem(eBtn);
			}
		}

		oTable.data("sumCount", oParams.sumColCount);

		oTable.addEventDelegate({
			onAfterRendering : function() {
				if (sap.ui.commons.Tree.prototype.onAfterRendering) {
					sap.ui.commons.Tree.prototype.onAfterRendering.apply(this, arguments);
				}
				var oTable =$.UIbyID(arguments[0].srcControl.getId());
				var oRows = oTable.getRows();
				$.each( oRows, function( key, oRow ) {
					if (oRow.data("rowTotal") != "") $('#'+oRow.getId()).addClass(oRow.data("rowTotal"));
				});
				//console.debug("onAfterRendering");
			}
		});

		return oTable;

	}, // end initTable

	/**
	 * Add a column to the target table
	 * @method addColumn
	 * @param {Object} oParams
	 * @param {Object} oParams.table - table object
	 * @param {object} [oParams.column.properties={}] - column properties object (more info at {@link https://sapui5.netweaver.ondemand.com/sdk/docs/api/symbols/sap.ui.table.Column.html#constructor})
	 * @param {Object} oParams.column - column object
	 * @param {Object} oParams.column.id - id of the column
	 * @param {String} [oParams.column.label] - column name
	 * @param {String} [oParams.labelAlign] - alignment of the label (more info at {@link https://openui5.hana.ondemand.com/docs/api/symbols/sap.ui.core.TextAlign.html})
	 * @param {object} oParams.column.template - template object
	 * @param {String} [oParams.column.template.type="text"] - Template which defines the content of the column, it can be: 'icon', 'text', 'textInput', 'numInput', 'number'
	 * @param {Object} [oParams.column.template.icons] - (only for 'icon' templates) ....
	 * @param {Function} [oParams.column.template.press=null] - (only for 'button' templates) function fired when the button is pressed if 'true', leading zeros are removed before displaying the value
	 * @param {Boolean} [oParams.column.template.removeLeadingZeros=false] - (only for 'text' templates) if 'true', leading zeros are removed before displaying the value
	 * @param {Array} [oParams.column.customMenu] - object array which contains all the elements to be added to the menu of the column
	 * @memberOf UI5Utils
	 */
	addColumn: function(oParams) {

		var oTable  = oParams.table,
				oColumn = oParams.column,
				template;

		if (typeof(oColumn.template) == "undefined") oColumn.template = {};

		// by default a coulmn has a TextView as template
		if (typeof(oColumn.template.type) == "undefined") oColumn.template.type = "TextView";

		// check if 'oColumn.properties' is specified, otherwise set it as an emoty object
		if (typeof(oColumn.properties) == "undefined") {
			oColumn.properties = {};
		}

		if (typeof(oColumn.Field) == "undefined") {
			oColumn.Field = "";
			oColumn.id = oTable.getId() + "-" + oColumn.Field;
		}

		if (typeof(oColumn.tooltipCol) == "undefined") {
			oColumn.tooltipCol = oColumn.Field;
		}

		if (typeof(oColumn.sumCol) == "undefined") {
			oColumn.sumCol = false;
		}


		if (typeof(oColumn.sumText) == "undefined") {
			oColumn.sumText = false;
		}

		if (typeof(oColumn.sumField) == "undefined") {
			oColumn.sumField = "";
		}

		if (typeof(oColumn.template.textAlign) == "undefined") {
			oColumn.template.textAlign = sap.ui.core.TextAlign.Begin;
		}


		else {
			switch (oColumn.template.textAlign) {
				case "Left":
					oColumn.template.textAlign = sap.ui.core.TextAlign.Left;
					break;
				case "Rigth":
					oColumn.template.textAlign = sap.ui.core.TextAlign.Right;
					break;
				case "End":
					oColumn.template.textAlign = sap.ui.core.TextAlign.End;
					break;
				case "Begin":
					oColumn.template.textAlign = sap.ui.core.TextAlign.Begin;
					break;
				case "Center":
					oColumn.template.textAlign = sap.ui.core.TextAlign.Center;
					break;
				default:
					//oColumn.template.textAlign = sap.ui.core.TextAlign.Begin;
																				}
		}

		// set the template
		if( typeof(oColumn.properties.template) == "undefined" ) {

			// check if the template is different from TextView
			switch (oColumn.template.type) {

				case "icon":
					template = new sap.ui.commons.Image({width: "20px", height: "20px"})
						.bindProperty( "src", oColumn.Field );
					//.addStyleClass("infoIcon");
					break;

				case "labelIcon":
					template = new sap.ui.commons.Label()
						.bindProperty( "tooltip", oColumn.tooltipCol )
						.bindProperty( "icon", oColumn.Field )
						.addStyleClass("infoIcon");
					break;

				case "coreIcon":
					console.log(JSON.stringify(oColumn));
					template = new sap.ui.core.Icon({width: "20px", height: "20px", useIconTooltip: true})
						.bindProperty( "src", oColumn.Field );
					template.setTooltip( oColumn.tooltipCol );
					template.setColor( oColumn.iconColor );
					//.addStyleClass("infoIcon");
					break;

				case "colorIcon":
					console.log(JSON.stringify(oColumn));
					template = new sap.ui.core.Icon({width: "20px", height: "20px", useIconTooltip: true});
					template.bindProperty( "src", oColumn.Field );
					template.bindProperty( "tooltip", oColumn.tooltipCol);
					template.bindProperty( "color", oColumn.colorCol);
					template.addStyleClass("infoIcon");
					break;
				case "TextField":
					var CMod = function (oEvent){
						var fieldName = oColumn.Field ; //oEvent.getSource().getBinding("value");
						var newVal = oEvent.getParameters().liveValue;
						fnSetCellValue(oTable.getId(), fieldName,newVal);
					};
					var oTextField = new sap.ui.commons.TextField({width: oColumn.width,liveChange:CMod }).bindProperty("value",     oColumn.tooltipCol);
					oTextField.setTextAlign(oColumn.template.textAlign);
					//liveChange
					template = new sap.ui.commons.InPlaceEdit({content: oTextField});
					template.bindProperty( "tooltip", oColumn.tooltipCol );
					break;

				case "TextView":
					template = new sap.ui.commons.TextView().bindProperty("text", oColumn.Field).bindProperty("tooltip", oColumn.tooltipCol);
					template.setTextAlign(oColumn.template.textAlign);
					template.bindProperty( "tooltip", oColumn.tooltipCol );
					template.data("Tab_Cell_Field", oColumn.Field);
					if (oColumn.sumCol) {
						template.bindProperty("text",oColumn.Field,function (value) { return CellTotalFormatter(this.getId(), value);});
						template.data("sumText", oColumn.sumText);
						template.data("sumField", oColumn.sumField);
					}
					break;

				case "textInput":
					template = new sap.ui.commons.TextField({
						change: function (oEvent) {
							var self = this;
							var sPath = oEvent.oSource.oBindingContexts.undefined.sPath;
							var cellId = oEvent.mParameters.id;
							var oTable = sap.ui.getCore().byId(cellId.substr(0, cellId.indexOf("-")));
							var oData = oTable.getModel().getProperty(sPath);
							template.setTextAlign(oColumn.template.textAlign);
						}
					})
						.bindProperty("value", oColumn.Field);
					break;

				case "numInput":
					template = new sap.ui.commons.TextField({
						liveChange: function(oEvt) {
							isNaN(oEvt.getParameter("liveValue"))				// check if the input is not a number
								? this.setValueState(sap.ui.core.ValueState.Error)	// highlight input as error
							: this.setValueState(sap.ui.core.ValueState.None);	// remove highlight otherwise
						},
						change: function (oEvent) {
							sap.ui.getCore().byId(this.sId).setValue( oEvent.getParameters().newValue );
						}
					})
						.bindProperty("value", oColumn.id);

					break;

				case "button":
					template = new sap.ui.commons.Button({
						press: oColumn.template.press
					})
						.bindProperty("text", oColumn.Field)
						.addStyleClass("sapUiBtnTransparent sapUiBtnWide");
					break;

				case "DateTime":
					template = new sap.ui.commons.TextView().bindProperty("text", oColumn.Field);
					template.bindText(oColumn.Field, function (value) {
						return fnConvXMLDateTime(value);
					});
					//template.setTextAlign(oAlign);
					break;

				case "Date":
					template = new sap.ui.commons.TextView().bindProperty("text", oColumn.Field);
					template.bindText(oColumn.Field, function (value) {
						return fnConvXMLDate(value);
					});
					break;
				case "Time":
					template = new sap.ui.commons.TextView().bindProperty("text", oColumn.Field);
					template.bindText(oColumn.Field, function (value) {
						return fnConvXMLTime(value);
					});
					//template.setTextAlign(oAlign);
					break;

				case "Number":
					template = new sap.ui.commons.TextView()
						.bindProperty(
						"text",
						oColumn.Field,
						function (val) {
							return isNaN(val) ? "-" : Number(val);
						});
					template.setTextAlign(oColumn.template.textAlign);
					template.bindProperty( "tooltip", oColumn.tooltipCol );
					break;

				case "Numeric":
					template = new sap.ui.commons.TextView().bindProperty("text", oColumn.Field, function (value) {
						return fnConvNumeric(value);
					});

					template.setTextAlign(oColumn.template.textAlign);
					template.bindProperty( "tooltip", oColumn.tooltipCol );
					break;

				case "checked":
					if (oColumn.template.enabled) {
						template = new sap.ui.commons.CheckBox({
							editable: true,
							change: oColumn.template.change
						});
					} else {
						template = new sap.ui.commons.CheckBox({
							editable: false
						});
					}
					template.bindChecked(oColumn.Field, function (value) {
						if (value === 'true' || value === '1') return true;
						else return false;
					});
					break;

				default:
					template = new sap.ui.commons.TextView()
						.bindProperty("text", oColumn.Field);
					break;

																	 } // end switch

			oColumn.properties.template = template;
			template.data("Tab_id", oTable.getId());
			template.data("fieldType", oColumn.template.type);

		} // end if

		// set the default label value
		if( typeof(oColumn.properties.name) == "undefined" ) {
			if( typeof(oColumn.properties.label) == "undefined" ) {
				oColumn.properties.label = new sap.ui.commons.Label({
					text: oColumn.label,
					tooltip: oColumn.label,
				});
				// align the label (if required)
				if( typeof(oColumn.labelAlign) != "undefined" ) {
					oColumn.properties.label.setTextAlign( oColumn.labelAlign );
				}
			}
		}

		// set the default sort property
		if( typeof(oColumn.properties.sortProperty) == "undefined" ) {
			oColumn.properties.sortProperty = oColumn.Field;
		}

		// set the default filter property
		if( typeof(oColumn.properties.filterProperty) == "undefined" ) {
			oColumn.properties.filterProperty = oColumn.Field;
		}

		// append default attributes to the 'oColumn.properties' object
		for (var attr in this._defaultColumnProperties) {
			// do not overwrite an existing attribute
			if(typeof(oColumn.properties[attr]) == "undefined") {
				oColumn.properties[attr] = this._defaultColumnProperties[attr];
			}
		}

		// create the new column
		var oColTable = new sap.ui.table.Column(oColumn.properties);
		// add the new column to the target table
		oTable.addColumn(oColTable);

		if ( oColumn.sumCol ) {
			oTable.getEnableColumnReordering(false);
			oColTable.setSorted(false);
			oColTable.setShowSortMenuEntry(false);
		}
		return oColTable;
	}, // end addColumn

	/**
	 * Add a column to the target table
	 * @method getDataModel
	 * @param {object} oParams
	 * @param {string} oParams.data - jQuery.ajax data params
	 * @param {string} oParams.dataType - jQuery.ajax: json or xml
	 * @param {bool} oParams.template - true/false default = true
	 * @memberOf UI5Utils
	 */
	getDataModel: function(oParams) {

		var self = this;

		// check if 'oParams.data' is specified, otherwise set it as an emoty object
		if (typeof(oParams.data) == "undefined") {
			oParams.data = {};
		}


		if (typeof(oParams.template) == "undefined") {
			oParams.template = true;
		}

		// append default attributes to the 'oParams.data' object
		for (var attr in this._defaultAjaxDataData) {
			// do not overwrite an existing attribute
			if(typeof(oParams.data[attr]) == "undefined") {
				oParams.data[attr] = this._defaultAjaxDataData[attr];
			}
		}

		if (oParams.dataType == "xml"){
			var QueryType = "Content-Type=text/xml&";
		}
		else{
			var QueryType = "Content-Type=text/json&";
		}
		if (oParams.template) {
			QueryType = QueryType + "QueryTemplate=";
		}

		return jQuery.ajax({
			type: "POST",
			url: "/XMII/Illuminator",
			data: QueryType + oParams.data,
			dataType: oParams.dataType,
			timeout: this._ajaxTimeout,
			success: function(data, textStatus, jqXHR) {
				if(window.console) console.debug(data, textStatus, jqXHR);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if(window.console) console.error(textStatus,errorThrown,jqXHR);
				if (typeof(jqXHR.responseText) != "undefined")  {
					if (jqXHR.responseText.indexOf( "User Management") > 0) location.reload(true);
				}
			},
		})
			.always(function() {
			if(window.console) {
				//console.info("url", self.getLocationOrigin() + oParams.url);
				console.debug("data", oParams.data);

			}
		});

	} // end getDataModel
};



function CellTotalFormatter (cellId, cellValue){

	var sField = $.UIbyID(cellId).data("Tab_Cell_Field");
	var sTable = $.UIbyID(cellId).data("Tab_id");
	var sumCount = $.UIbyID(sTable).data("sumCount");
	var sumText = $.UIbyID(cellId).data("sumText");
	var sumField = $.UIbyID(cellId).data("sumField");

	//console.log(sField);
	if (sumCount == -1) return cellValue;
	try {
		var rowPath = $.UIbyID(cellId).getBindingContext().sPath;
		var cellVal = cellValue; //$.UIbyID(sTable).getModel().getProperty(rowPath + "/" + sField);
		var RowIndex = Number(rowPath.slice(-4).match(/\d+/g) ) -1;
		var RowNum = Number(cellId.slice(-4).match(/\d+/g) );
		var RowID = sTable+"-rows-row"+RowNum;
		var TotDept = $.UIbyID(sTable).getModel().getProperty(rowPath + "/DrilldownDepth");
		var bTotal = false;
		if (cellVal == '---' ) {
			cellValue = "";
			bTotal = true;
		}

		$('#'+RowID).removeClass("tableRowTotal-0");
		$('#'+RowID).removeClass("tableRowTotal-1");
		$('#'+RowID).removeClass("tableRowTotal-2");
		$('#'+RowID).removeClass("tableRowTotal-3");
		if (Number(TotDept) <= sumCount || bTotal) {
			switch(Number(TotDept)){
				case 0:
					RowColor = 'tableRowTotal-0';
					if (sumText && cellValue == "") { cellValue = "Totale";}

					break;
				default:
					RowColor = 'tableRowTotal-' + Number(TotDept);
					if (sumField != "") {
						var sumValue =	$.UIbyID(sTable).getModel().getProperty(rowPath + "/" + sumField);
						if (sumValue != "---" && cellValue == "" && sumValue.slice(0,4) != "Tot."){ $.UIbyID(sTable).getModel().setProperty(rowPath + "/" + sumField,"Tot. " + sumValue);
																																											 //console.debug(sField + " sumField = " + sumField + " Text = " + sumValue);
																																											}
					}
					break;
														}
			$('#'+RowID).addClass(RowColor);
			//console.debug("Riga " + RowNum + " rowTotal " + RowColor);
			$.UIbyID(RowID).data("rowTotal", RowColor);

		}
		else $.UIbyID(RowID).data("rowTotal", "");
	}

	finally {
		return cellValue;
	}

}
