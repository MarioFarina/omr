// UI5_utils_m estensione di SapUI5/OpenUI5 Mobile
//******************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//******************************************************************************

/**
 * Estensione di SapUI5/OpenUI5 Mobile
 *
 * @author Alex Bonaffini <alex.bonaffini@it-link.it>
 *
 * @namespace UI5_utils_m
 * @requires sap.m
 * @static
 */

var UI5_utils_m = {
	
	/**
	 * Classe customTable estensione di sap.m.Table 
	 * {@link https://openui5.hana.ondemand.com/#docs/api/symbols/sap.m.Table.html}
	 *
	 * @class
	 * @memberOf UI5_utils_m
	 * @param {object} oParams - proprietà di customTable
	 * @param {object} oParams.properties - proprietà standard di sap.m.Table
	 */
	 
	customTable: function(oParams) {
		
		
		if(!oParams)
			oParams = {};
		
		if (!oParams.hasOwnProperty('properties') ||  !oParams.properties) {
			oParams.properties = {};
		}
		
		var table = new sap.m.Table(oParams.properties);
		
		/**
		 * Aggiunge una colonna alla tabella
		 * @function addColumn
		 * @memberOf UI5_utils_m.customTable
		 * @param {object} oParams - proprietà della nuova colonna
		 * @param {object} oParams.properties - proprietà standard di sap.m.ColumnListItem
		 */
		 
		this.addColumn = function(oParams) {
			
			if(!oParams)
				oParams = {};
			
			if (!oParams.hasOwnProperty('properties') ||  !oParams.properties) {
				oParams.properties = {};
			}
			
			var oColumn = new sap.m.ColumnListItem(oParams.properties);
		
			table.addItem(oColumn);
		}
	}
	
}