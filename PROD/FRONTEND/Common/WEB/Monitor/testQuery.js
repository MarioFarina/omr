// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataHeading = "Content-Type=text/XML&QueryTemplate=";

var queryToTest = "";
var queryParams = "";

// create a simple Input field
var oTetxQuery = new sap.ui.commons.TextField("oTextQuery", {
    tooltip: "Inserire la query da testare",
    width: "650px"
});

var oTextParams = new sap.ui.commons.TextField("oTextParams", {
    tooltip: "Inserire i parametri della query",
    width: "450px"
});

var oTextRowCount = new sap.ui.commons.TextField("RowCount",{
    value: "100",
    width: "70px",
});

var nParams = 0;
var params = [];
var testXml = "";    

/***********************************************************************************************/
// Inizializzazione pagina
/*************************************************************************+**********************/
$(document).ready(function () {

    //var loading = $.getScript('myScript.js');

    var oTable4 = createTabMonitor();

    oTable4.placeAt("MasterCont");
    refreshTabData();
    $("#splash-screen").hide();

});

/***********************************************************************************************/
/* ----- */
/*  PRIMA QUERY PROVATA */
/* ----- */
// /XMII/Illuminator?Content-Type=text/XML&QueryTemplate=EnergyMain/Reports/Monitor&Param.1=20160809 04:00:00&Param.2=20160809 11:20:00
/***********************************************************************************************/

/***********************************************************************************************/
// Funzioni ed oggetti per Dati
/***********************************************************************************************/

// Functione che crea la tabella Dati e ritorna l'oggetto oTable
function createTabMonitor() {

    var oTab = UI5Utils.init_UI5_Table({
        id: "TabData",
        properties: {
            //fixedColumnCount: nFixedCols,
            width: "100%",
            height: "100%",
            columnHeaderHeight: 10,
            selectionMode: sap.ui.table.SelectionMode.Single,
            visibleRowCount: 20,
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: "Inserire i parametri:"
                    }),
                    oTextParams,
                    new sap.ui.commons.Button("btnModUnit", {
                        icon: "sap-icon://add",
                        text: "Modifica parametri",
                        press: function () {
                            nParams = 0;
                            addParams();
                        }
                    }),
                    new sap.ui.commons.Button("btnEmpty2", {
                        icon: "sap-icon://delete",
                        text: "Svuota i parametri",
                        id: "btnEmpty2",
                        press: function (){
                            oTextParams.setValue("");
                            params.length = 0;
                        }
                    }),
                    new sap.ui.commons.Button("btnTestXML", {
                        text: "Test XML",
                        id: "btnTestXML",
                        press: function (){
                            oTextParams.setValue("");
                            params.length = 0;
                            OpenTestXMLDialog();
                        }
                    })
                ]
            })
        }
    })

    createToolBar(oTab.getToolbar());

    /*
        ----- PER PROVE
    var aCaso = $.UIbyID("TabData").getModel().columnId;
    */

    /*
    oTab.bindColumns("/columns", addColumn("0", $.UIbyID("TabData").getModel().columnId));
    oTab.bindRows("/rows");
    */

    return oTab;
}

// Aggiorna la tabella Dati
function refreshTabData() {

    var qParamsTab = {
        data: QService + dataHeading + queryToTest + queryParams,
        dataType: "xml"
    };
    
    UI5Utils.getDataModel(qParamsTab)
        // on success
        .done(function (data) {
            // carica il model della tabella
            $.UIbyID("TabData").setBusy(true);
            // Elimina le colonne
            $.UIbyID("TabData").removeAllColumns();

            $.ajax({
                url: qParamsTab.data,
                dataType: 'xml',
                type: "GET",
                async: false,
                success: function (data) {

                    /* QUESTO MODO NON SEMBRA GRADEVOLE
                    var columns = data.getElementsByTagName("Rowsets");
                    */

                    // Ricrea le Colonne prendendole dal rowset columns della query MII
                    // Il percorso corrispode al json data.Rowsets.Rowset[0].Columns.Column
                    $.each(data.childNodes[0].childNodes[0].childNodes[0].childNodes, function (idx, item) {
                        var fName = item.attributes['Name'].textContent;
                        var fDesc = item.attributes['Description'].textContent;
                        
                        if(fDesc == ""){
                            fDesc = fName;
                        }

                        var sType = "TextView";
                        var sAlign = "Left";
                        switch (item.attributes['SQLDataType'].textContent) {
                        case "1":
                            sType = "TextView";
                            sAlign = "Left";
                            break;
                        case "8":
                            sType = "Numeric";
                            sAlign = "Right";
                            break;
                        case "4":
                            sType = "Numeric";
                            sAlign = "Right";
                            break;
                        case "93":
                            sType = "DateTime";
                            sAlign = "Right";
                            break;
                        }

                        UI5Utils.addColumn({
                            table: $.UIbyID("TabData"),
                            column: {
                                Field: fName,
                                label: fDesc,
                                properties: {
                                    width: "80px"
                                },
                                template: {
                                    type: sType,
                                    textAlign: sAlign
                                }
                            }
                        });
                    });

                    // carica il model della tabella
                    $.UIbyID("TabData").getModel().setData(data);
                },
                error: function (result) {
                    /*console.log("male");*/
                }
            });

            $.UIbyID("TabData").getModel().refresh(true)
        }) // End Done Function
        // on fail
        .fail(function () {
            sap.ui.commons.MessageBox.alert("Errore nell'aggiornamento dati");
        })
        // always, either on success or fail
        .always(function () {
            // remove busy indicator
            $.UIbyID("TabData").setBusy(false);
        });
}

// Popola la tool bar con tutte le info necessarie
function createToolBar(oToolBar) {
    
    oToolBar.addItem(new sap.ui.commons.Label({
        text: "Inserire la query:"
    }));

    oToolBar.addItem(oTetxQuery);

    var oButton1 = new sap.ui.commons.Button("b11", {
        icon: "sap-icon://refresh",
        text: "Aggiorna",
        tooltip: "Aggiorna risultato",
        press: function () {
            queryToTest = oTetxQuery.getValue();
            /* controllo che la query non sia vuota */
            if(queryToTest == ""){
                foundError("Inserire una query nel campo indicato.");
            }
            else{
                /* controllo che nel campo QUERY non siano stati inseriti parametri*/
                var catchParamsPositionError = queryToTest.search("&Param.");
                if(catchParamsPositionError != -1){
                    foundError("Inserire i parametri SOLO nel campo indicato.");
                }
                else{
                    /* controllo che nel campo QUERY non sia stato specificato il row count*/
                    catchParamsPositionError = queryToTest.search("&RowCount=");
                    if(catchParamsPositionError != -1){
                        foundError("Inserire i parametri SOLO nel campo indicato.");
                    }
                    else{
                        queryParams = oTextParams.getValue();
                        if(isNaN($.UIbyID("RowCount").getValue()) || $.UIbyID("RowCount").getValue() == ""){
                            $.UIbyID("RowCount").setValue("100");
                        }
                        var pos = queryParams.search("&RowCount=");
                        if(pos != -1){
                            queryParams = queryParams.slice(0, pos);
                            //oTetxQuery.setValue(queryParams + pos);
                        }
                        queryParams = queryParams + "&RowCount=" + $.UIbyID("RowCount").getValue();
                        oTextParams.setValue(queryParams);                        
                        refreshTabData();
                    }
                }
            }
        }
    });
    
    var oButton2 = new sap.ui.commons.Button("btnEmpty", {
        icon: "sap-icon://delete",
        text: "Svuota la tabella",
        id: "btnEmpty",
        press: function (){
            oTetxQuery.setValue("");
            oTextParams.setValue("");
            $.UIbyID("RowCount").setValue("");
            queryToTest = oTetxQuery.getValue();
            queryParams = oTextParams.getValue();
            params.length = 0;
            testXml = "";
            refreshTabData();
        }
    });

    oToolBar.addItem(new sap.ui.commons.Label({
		text: "Record:"
	}));
	oToolBar.addItem(oTextRowCount);
    
    oToolBar.addItem(oButton1);
    oToolBar.addItem(oButton2);

}

function addParams() {

    if (!$.UIbyID("dlgAddParams")) {
        //  Crea la finestra di dialogo
        var oEdtDlg = new sap.ui.commons.Dialog({
            modal: true,
            resizable: true,
            id: "dlgAddParams",
            maxWidth: "600px",
            Width: "600px",
            showCloseButton: false
        });



        var oLayout1 = new sap.ui.layout.form.GridLayout({
            singleColumn: true
        });


        var oForm1 = new sap.ui.layout.form.Form({
            id: "frmAddParams",
            title: new sap.ui.core.Title("ttlAddParams", {
                text: "Modifica parametri",
                icon: "sap-icon://machine",
                tooltip: "Modifica i parametri"
            }),
            width: "98%",
            layout: oLayout1,
            formContainers: [
				new sap.ui.layout.form.FormContainer("formContainer", {
                    formElements: []
                })
			]
        });
        
        
        /* Questo if è per recuperare eventuali parametri inseriti prima */
        if(params.length != 0){
            for (j = 0; j < params.length; j++) {
                nParams++;
                $.UIbyID("formContainer").addFormElement(new sap.ui.layout.form.FormElement({
                    label: new sap.ui.commons.Label("label"+nParams, {
                        text: "Params." + nParams,
                        layoutData: new sap.ui.layout.form.GridElementData({
                            hCells: "2"
                        })
                    }),
                    fields: new sap.ui.commons.TextField("txtParam" + nParams, {
                        "value": params[j],
                    })
                }));
            }
        }

        oEdtDlg.addContent(oForm1);
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: "Aggiungi parametro",
            press: function () {
                addParam();
            }
        }));
        
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: "Ok",
            press: function () {
                takeParams();
            }
        }));
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: "Annulla",
            press: function () {
                oEdtDlg.close();
                oEdtDlg.destroy();
            }
        }));

    } else {
        /*
        $.UIbyID("txtIDUnitsUM").setValue(bAdd ? "" : fnGetCellValue("unitsTab", "um"));
        $.UIbyID("txtDescrUnit").setValue(bAdd ? "" : fnGetCellValue("unitsTab", "description"));
        */
    }

    $.UIbyID("dlgAddParams").open();
}

function addParam(){
    nParams++;
    $.UIbyID("formContainer").addFormElement(new sap.ui.layout.form.FormElement({
        label: new sap.ui.commons.Label("label"+nParams, {
            text: "Param." + nParams,
            layoutData: new sap.ui.layout.form.GridElementData({
                hCells: "2"
            })
        }),
        fields: new sap.ui.commons.TextField("txtParam" + nParams, {
                "value": "",
            })
            /*.data("OrigValue", bAdd ? "" : fnGetCellValue("unitsTab", "um"))*/
    }));
}

function takeParams(){
    
    oTextParams.setValue("");
    params.length = 0;
    var tmpQueryParams = "";
    for (i = 1; i <= nParams; i++) {
        tmpQueryParams = tmpQueryParams + "&Param." + i + "=" + $.UIbyID("txtParam" + i).getValue();
        params.push($.UIbyID("txtParam" + i).getValue());
    }
    if(isNaN($.UIbyID("RowCount").getValue()) || $.UIbyID("RowCount").getValue() == ""){
        $.UIbyID("RowCount").setValue("100");
    }
    tmpQueryParams = tmpQueryParams + "&RowCount=" + $.UIbyID("RowCount").getValue();
    oTextParams.setValue(tmpQueryParams);
    $.UIbyID("dlgAddParams").close();
    $.UIbyID("dlgAddParams").destroy();
}

function foundError(errorToShow){
    sap.ui.commons.MessageBox.show(errorToShow,
    sap.ui.commons.MessageBox.Icon.WARNING,
    "Errore query", [sap.ui.commons.MessageBox.Action.OK],
    function (sResult) {
        oTetxQuery.setValue("");
        oTextParams.setValue("");
        oTextRowCount.setValue("");
        queryToTest = oTetxQuery.getValue();
        queryParams = oTextParams.getValue();
        params.length = 0;
        nParams = 0;
        refreshTabData();
    },
    sap.ui.commons.MessageBox.Action.OK);
}

function OpenTestXMLDialog() {

    if (!$.UIbyID("dlgTestXML")) {
        //  Crea la finestra di dialogo
        var oEdtDlg = new sap.ui.commons.Dialog({
            modal: true,
            resizable: true,
            id: "dlgTestXML",
            title: "Test XML",
            //minWidth: "900px",
            //Width: "900px",
            maxWidth: "900px",
            //minHeight: "600px",
            //Height: "600px",
            maxHeight: "600px",
            showCloseButton: false
        });
        
        var oLayout0 = new sap.ui.commons.layout.MatrixLayout("tabdetail", { columns: 1 });
        
        var oLayoutT = new sap.ui.layout.form.GridLayout();
        var oFormT = new sap.ui.layout.form.Form({
            width: "98%",
            layout: oLayoutT,
            editable: true,
            formContainers: [
                new sap.ui.layout.form.FormContainer({
                    formElements: [
                        //Nome sorgente
                        new sap.ui.layout.form.FormElement({
							label: new sap.ui.commons.Label({
								text: "Fonte XML: ",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "3"
								})
							}),
							fields: new sap.ui.commons.TextArea("txtTestXML", {
								value: testXml,
								cols : 10,
								rows : 10,
								editable : true
							}).data("OrigValue", testXml)
						})
                    ]
                })
            ]
        });
        
        oLayout0.createRow(oFormT);
        
        oEdtDlg.addContent(oLayout0);
                
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: "Ok",
            press: function () {
                testXml = $.UIbyID("txtTestXML").getValue();
                TestXMLabData(testXml);
                //xmlDoc = parser.parseFromString(testXml,"text/xml");
                oEdtDlg.close();
                oEdtDlg.destroy();
            }
        })/*.addStyleClass("customWidth")*/);
        
        oEdtDlg.addButton(new sap.ui.commons.Button({
            text: "Annulla",
            press: function () {
                oEdtDlg.close();
                oEdtDlg.destroy();
            }
        })/*.addStyleClass("customWidth")*/);

    } else {
        /*
        $.UIbyID("txtIDUnitsUM").setValue(bAdd ? "" : fnGetCellValue("unitsTab", "um"));
        $.UIbyID("txtDescrUnit").setValue(bAdd ? "" : fnGetCellValue("unitsTab", "description"));
        */
    }
    
    $.UIbyID("dlgTestXML").open();
}


// Aggiorna la tabella Dati con l'XML inserito dall'utente
function TestXMLabData(xmlToTest) {
    
    /*
    var parser, xmlDoc;
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xmlToTest,"text/xml");
    */
    /*
    var xmlDoc = jQuery.parseXML("<foo>Stuff</foo>");
    if (xmlDoc) {
        window.alert(xmlDoc.documentElement.nodeName);
    }
    */
    
    /*
    var parseXml;

    if (window.DOMParser) {
        parseXml = function(xmlToTest) {
            return ( new window.DOMParser() ).parseFromString(xmlToTest, "text/xml");
        };
    } else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
        parseXml = function(xmlToTest) {
            var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = "false";
            xmlDoc.loadXML(xmlToTest);
            return xmlDoc;
        };
    } else {
        parseXml = function() { return null; }
    }

    var xmlDoc = parseXml("<foo>Stuff</foo>");
    if (xmlDoc) {
        window.alert(xmlDoc.documentElement.nodeName);
    }
    */
    
    var parser = new DOMParser();
    var doc = parser.parseFromString(xmlToTest, "application/xml");
    // returns a Document, but not a SVGDocument nor a HTMLDocument
    
    $.UIbyID("TabData").setBusy(true);
    // Elimina le colonne
    $.UIbyID("TabData").removeAllColumns();
    
    $.each(doc.childNodes[0].childNodes[0].childNodes[0].childNodes, function (idx, item) {
        var fName = item.attributes['Name'].textContent;
        var fDesc = item.attributes['Description'].textContent;

        if (fDesc == "") {
            fDesc = fName;
        }

        var sType = "TextView";
        var sAlign = "Left";
        switch (item.attributes['SQLDataType'].textContent) {
        case "1":
            sType = "TextView";
            sAlign = "Left";
            break;
        case "8":
            sType = "Numeric";
            sAlign = "Right";
            break;
        case "4":
            sType = "Numeric";
            sAlign = "Right";
            break;
        case "93":
            sType = "DateTime";
            sAlign = "Right";
            break;
        }

        UI5Utils.addColumn({
            table: $.UIbyID("TabData"),
            column: {
                Field: fName,
                label: fDesc,
                properties: {
                    width: "80px"
                },
                template: {
                    type: sType,
                    textAlign: sAlign
                }
            }
        });
    });

    // carica il model della tabella
    $.UIbyID("TabData").getModel().setData(xmlDoc);
    $.UIbyID("TabData").getModel().refresh(true);
    $.UIbyID("TabData").setBusy(false);            

}