// Script per pagina lista utenti attivi

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";


Libraries.load(
	["/XMII/CM/Common/MII_core/UI5_utils"],
	function () {

		/***********************************************************************************************/
		// Inizializzazione pagina
		/***********************************************************************************************/
		$(document).ready(function () {$("#splash-screen").hide();});

		var oTable4 = createTabMonitor();

		oTable4.placeAt("MasterCont");
		refreshTabData();


		tabRefr = $.timer(function () { refreshTabDatiMac(); }, 3 * 60 * 1000, false);
		tabRefr.set({ autostart: true });
	}
);




/***********************************************************************************************/
// Funzioni ed oggetti per Dati macchine
/***********************************************************************************************/

// Functione che crea la tabella Dati macchine e ritorna l'oggetto oTable
function createTabMonitor() {


	var oTab = UI5Utils.init_UI5_Table({
		id: "TabData",
		//sumColCount: 1,
		columns: [
			{
				Field: "Nome_di_logon",
				label: "Login",
				//tooltipCol: "IDREP",
				sumCol: false,
				sumText: false,
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Nome_completo",
				label: "Utente",
				sumField: "REPTXT",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Indirizzo_e-mail",
				label: "Email",
				sumField: "REPTXT",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Creato",
				label: "Dt Creazione",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Ora_dell_ultimo_accesso",
				label: "Ultimo login",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Data_scadenza",
				label: "Scadenza",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			}
		],
		properties: {
			//fixedColumnCount: nFixedCols,
			width: "100%",
			height: "100%",
			selectionMode: sap.ui.table.SelectionMode.Single
		}
	})

	createToolBar(oTab.getToolbar());
	return oTab;

}

// Aggiorna la tabella Dati
function refreshTabData() {

	var qParams = {
		data:  "service=admin&mode=SessionList",
		dataType: "xml",
		template: false
	};
	$.UIbyID("TabData").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("TabData").setBusy(true);
			$.UIbyID("TabData").getModel().setData(data);
		}) // End Done Function
		// on fail
		.fail(function (jqXHR, textStatus, errorThrown) {
			sap.ui.commons.MessageBox.alert("Errore nell'aggiornamento dati (motivo: " + textStatus + ")");

		})
		// always, either on success or fail
		.always(function () {

			// remove busy indicator
			$.UIbyID("TabData").setBusy(false);
		});
}


// Popola la tool bar con tutte le info necessarie
function createToolBar(oToolBar) {

	// Pulsante per aggiornamento tabella
	var oButton1 = new sap.ui.commons.Button("b11", {
		icon: "sap-icon://refresh",
		text: "Aggiorna",
		tooltip: "Aggiorna risultato",
		press: function () {
			refreshTabData();
		}
	});

	oToolBar.addItem(oButton1);
}


