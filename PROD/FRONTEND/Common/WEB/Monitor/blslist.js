// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProd = "Content-Type=text/XML&QueryTemplate=Production/Monitor/";
var icon16 = "/XMII/CM/Common/icons/16x16/";

var data;

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
$(document).ready(function () {


	var oTable4 = createTabMonitor();


	oTable4.placeAt("MasterCont");

	tabRefr = $.timer(function () {
		refreshTabBLS();
	}, 150 * 1000, false);
	tabRefr.set({
		autostart: true
	});
	refreshTabBLS()

});


/***********************************************************************************************/
// Funzioni ed oggetti per BLS
/***********************************************************************************************/

// Function che crea la tabella BLS e ritorna l'oggetto oTable
function createTabMonitor() {

	//Crea L'oggetto Tabella BLS
	var oTable = new sap.ui.table.Table({
		title: "Lista Transazioni eseguite",
		id: "blsTab",
		visibleRowCount: 15,
		width: "98%",
		firstVisibleRow: 1,
		selectionMode: sap.ui.table.SelectionMode.Single,
		navigationMode: sap.ui.table.NavigationMode.Scrollbar,
		visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
		rowSelectionChange: function (oControlEvent) {
			/*if ($.UIbyID("blsTab").getSelectedIndex() == -1) {
				$.UIbyID("btnDett").setEnabled(false);
			} else {
				$.UIbyID("btnDett").setEnabled(true);
			}*/
		},
		toolbar: new sap.ui.commons.Toolbar({
			items: [
                new sap.ui.commons.TextField({
                    layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                    id: "txtIDtransaz",
                    liveChange: function(ev) {
                        var val = ev.getParameter('liveValue');
                        var newval = val.replace(/[^\d]/g, '');
                        this.setValue(newval);
                    }
                }),
				new sap.ui.commons.Button({
					text: "Dettaglio",
					id: "btnDett",
					icon: icon16 + "application-detail.png",
					enabled: true,
					press: function () {
						ShowBlsDett();
					}
				}),
				new sap.ui.commons.Button({
					text: "Aggiorna",
					icon: icon16 + "refresh.png",
					enabled: true,
					press: function () {
						refreshTabBLS();
					}
				}),
				new sap.ui.commons.Button({
					text: "Esporta",
					id: "btnExport",
					icon: icon16 + "application-export.png",
					enabled: true,
					press: function () {
						exportTableToCSV("blsTab", "DataTable");
					}
				})
			]
		})
	});


	addColumnEx(oTable, "ID", "ID", "90px");
	addColumnEx(oTable, "Transazione", "NAME", "220px");
	addColumnEx(oTable, "Server", "SERVER", "110px");
	addColumnEx(oTable, "Stato", "STATUS");
	addColumnEx(oTable, "Esecuzione", "RUNTIME");
	addColumnEx(oTable, "Utente", "RUNNING_USER");
	addColumnEx(oTable, "Inizio", "STARTTIME", "120px", "DateTime");
	addColumnEx(oTable, "Fine", "ENDTIME", "120px", "DateTime");
	addColumnEx(oTable, "Durata", "DURATION", "70px");
	addColumnEx(oTable, "Scadenza", "EXPIRATION", "120px", "DateTime");


	oModel = new sap.ui.model.xml.XMLModel();


	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");

	return oTable;
}

// Aggiorna la tabella BLS
function refreshTabBLS() {
	$("#splash-screen").show();
    data = fnGetAjaxData("Service=BLSManager&Mode=List&content-type=text/xml", false);
    var dataModel = new sap.ui.model.xml.XMLModel();
    dataModel.setData(data);
    $.UIbyID("blsTab").setModel(dataModel);
    $.UIbyID("blsTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}

// Function per predisporre la finestra di dialogo File
function ShowBlsDett() {
    
    if ($.UIbyID("blsTab").getSelectedIndex() <= 0) {
        var index = 0;
        var i = 0;
        var sIdToBeFound = $.UIbyID("txtIDtransaz").getValue();
        
        if(sIdToBeFound === "") {
            sap.ui.commons.MessageBox.alert("Selezionare una riga");
            return;
        }
        
        $(data).find("Row").each(function (idx, row) {
            if($(row).find("ID").text() !== sIdToBeFound) {
                i = i + 1;
            }
            else {
                index = i;
            }
        });
        
        if(index == 0){
            sap.ui.commons.MessageBox.show(
                "Transazione non trovata. Controllare le transazioni precedenti?",
                sap.ui.commons.MessageBox.Icon.WARNING,
                "transazione non trovata",
                [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                function(sResult){
                    if (sResult == 'YES'){
                        openBlsDetByID(sIdToBeFound);
                    }
                },
                sap.ui.commons.MessageBox.Action.YES
            );
            return;
        }
        $.UIbyID("blsTab").setSelectedIndex(index);
    }
    
    openBlsDet();
}

// Function per creare la finestra di dialogo File
function openBlsDet() {
    
    var tabDet = fnGetAjaxData("service=BLSManager&Mode=viewlog&content-type=text/xml&id=" + fnGetCellValue("blsTab", "ID"), false);
    var txtDet = "";
    $(tabDet).find("Row").each(function (idx, row) {
        txtDet = txtDet + $(row).find("Message").text() + "\n";
    });
    
    var ctrlArea = new sap.ui.commons.TextArea({
        rows: 30,
        cols: 180,
        value: txtDet
    });
    
    //  Crea la finestra di dialogo
    var oEdtDlg = new sap.ui.commons.Dialog({
        modal: true,
        resizable: true,
        title: "Dettaglio per transazione " + fnGetCellValue("blsTab", "ID"),
        id: "dlgHState",
        maxWidth: "1024px",
        Width: "900px",
        maxHeight: "600px",
        Height: "500px",
        showCloseButton: true
    });
    
    oEdtDlg.addContent(ctrlArea);
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: "Chiudi",
        press: function () {
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));
    oEdtDlg.open();
}

// Function per creare la finestra di dialogo File
function openBlsDetByID(sID) {
    
    var tabDet = fnGetAjaxData("service=BLSManager&Mode=viewlog&content-type=text/xml&id=" + sID, false);
    var txtDet = "";
    $(tabDet).find("Row").each(function (idx, row) {
        txtDet = txtDet + $(row).find("Message").text() + "\n";
    });
    
    var ctrlArea = new sap.ui.commons.TextArea({
        rows: 30,
        cols: 180,
        value: txtDet
    });
    
    //  Crea la finestra di dialogo
    var oEdtDlg = new sap.ui.commons.Dialog({
        modal: true,
        resizable: true,
        title: "Dettaglio per transazione " + fnGetCellValue("blsTab", "ID"),
        id: "dlgHState",
        maxWidth: "1024px",
        Width: "900px",
        maxHeight: "600px",
        Height: "500px",
        showCloseButton: true
    });
    
    oEdtDlg.addContent(ctrlArea);
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: "Chiudi",
        press: function () {
            oEdtDlg.close();
            oEdtDlg.destroy();
        }
    }));
    oEdtDlg.open();
}

// Aggiorna la tabella contenuto
function refreshTabCont() {
	$.UIbyID("blsDett").getModel().loadData(QService + "service=BLSManager&Mode=viewlog&content-type=text/xml&id=" + fnGetCellValue("blsTab", "ID"));
}
