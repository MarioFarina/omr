/***********************************************************************************************/
// Funzioni ed oggetti per fn_time
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella usrLogins e ritorna l'oggetto oTable
function createTabUsrLoginNumbers() {


	var oTable = UI5Utils.init_UI5_Table({

		id: "usrLoginNumbers",
		properties: {
			title: oLng_MasterData.getText("MasterData_LoginsNumber"), //"Numero Login effettuati",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 1,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {
			}
		},
		exportButton: true,
		toolbarItems:[
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
				icon: "sap-icon://refresh",
				enabled: true,
				press: function ()
				{
					refreshTabUsrLoginNumbers();
				}
			})
		],
		columns: [
			{
				Field: "RecordedDate",
				label: oLng_MasterData.getText("MasterData_RecordedDate"), //"Data registrazione",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Username",
				label: oLng_MasterData.getText("MasterData_UserName"), //"Username",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "TotalLogins",
				label: oLng_MasterData.getText("MasterData_TotalLogins"), //"Login Totali",
				properties: {
					width: "50px"
				}
			}
		],
		/*properties: {
						//fixedColumnCount: nFixedCols,
						//visibleRowCount: 1,
						width: "100%",
						height: "100%",
						selectionMode: sap.ui.table.SelectionMode.Single
				}*/
		toolbar: new sap.ui.commons.Toolbar({
			items: []
		})
	});
    
	oModel = new sap.ui.model.xml.XMLModel();
    
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabUsrLoginNumbers();
	return oTable;
}

// Aggiorna la tabella Plants
function refreshTabUsrLoginNumbers() {
	$("#splash-screen").show();
	$.UIbyID("usrLoginNumbers").getModel().loadData(QService + "Content-Type=text/xml&service=Monitoring&Mode=Logins");
	$("#splash-screen").hide();
}
