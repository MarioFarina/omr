/***********************************************************************************************/
// Funzioni ed oggetti per usrLogins
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella usrLogins e ritorna l'oggetto oTable
function createTabUsrLogins() {


	var oTable = UI5Utils.init_UI5_Table({
		id: "usrLog",
		properties: {
			title: oLng_MasterData.getText("MasterData_ConnectedUsers"), //"Utenti connessi",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {
			}
		},
		exportButton: true,
		toolbarItems:[
			new sap.ui.commons.Button({
				text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
				icon: "sap-icon://refresh",
				enabled: true,
				press: function ()
				{
					refreshTabUsrLog();
				}
			})
		],
		columns: [
			{
				Field: "Nome_di_logon",
				label: oLng_MasterData.getText("MasterData_Login"), //"Login",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Nome_completo",
				label: oLng_MasterData.getText("MasterData_User"), //"Utente",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Indirizzo_e-mail",
				label: oLng_MasterData.getText("MasterData_Email"), //"E-mail",
				properties: {
					width: "50px"
				}
			},
			{
				Field: "Creato",
				label: oLng_MasterData.getText("MasterData_CreationDate"), //"Data Creazione",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Ora_dell_ultimo_accesso",
				label: oLng_MasterData.getText("MasterData_LastLogin"), //"Ultimo login",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "Data_scadenza",
				label: oLng_MasterData.getText("MasterData_Expiry"), //"Scadenza",
				properties: {
					width: "50px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			}
		],
		toolbar: new sap.ui.commons.Toolbar({
			items: []
		})
	});
    
	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabUsrLog();
	return oTable;
}

// Aggiorna la tabella Plants
function refreshTabUsrLog() {
	$("#splash-screen").show();
	$.UIbyID("usrLog").getModel().loadData(QService + "Content-Type=text/XML&service=admin&mode=SessionList");
	$("#splash-screen").hide();
}

