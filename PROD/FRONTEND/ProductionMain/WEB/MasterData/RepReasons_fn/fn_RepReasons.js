/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Gestione Causali di riparazione e rilavorazione
//Author: Bruno Rosati
//Date:   05/07/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Gestione Causali di riparazione e rilavorazione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabRepReasons() {
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "RepReasonsTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_RepReasList"), //"Lista causali di recupero",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("RepReasonsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModRepReasons").setEnabled(false);
        			$.UIbyID("btnDelRepReasons").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModRepReasons").setEnabled(true);
                    $.UIbyID("btnDelRepReasons").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                id:   "btnAddRepReasons",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openRepReasonsEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id:   "btnModRepReasons",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openRepReasonsEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                id:   "btnDelRepReasons",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelRepReason();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabRepReasons();
                }
            })
        ],
        columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "NAME1",
                label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "ID_REP", 
				label: oLng_MasterData.getText("MasterData_RepReason"), //"Causale di recupero",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "REASTXT",
				label: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "DESCR", 
				label: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
				properties: {
					width: "130px"
				}
            },
            {
				Field: "TYPE",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "EXTENDED_TEXT", 
				label: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
				properties: {
					width: "130px"
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabRepReasons();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabRepReasons() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("RepReasonsTab").getModel().loadData(
            QService + dataProdMD +
            "RepReasons/getRepReasonsSQ&Param.1=" + $('#plant').val() + "&Param.2=" + sLanguage
        );
    }
    else{
        $.UIbyID("RepReasonsTab").getModel().loadData(
            QService + dataProdMD +
            "RepReasons/getRepReasonsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + sLanguage
        );
    }
    $.UIbyID("RepReasonsTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Causali di recupero
function openRepReasonsEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput", 
        selectedKey : bEdit?fnGetCellValue("RepReasonsTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey(),
        enabled: false
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModelSelectTypes = new sap.ui.model.xml.XMLModel();
    oModelSelectTypes.loadData(QService + dataProdMD + "RepReasons/getRepReasonsTypeSQ&Param.1=" + sLanguage);
        
    var ocmbSelectTypes = new sap.ui.commons.ComboBox({
        id: "cmbSelectTypes",
        selectedKey : bEdit?fnGetCellValue("RepReasonsTab", "TYPE"):""
    });
    ocmbSelectTypes.setModel(oModelSelectTypes);
    var oItemSelectTypes = new sap.ui.core.ListItem();
    oItemSelectTypes.bindProperty("text", "EXTENDED_TEXT");
	oItemSelectTypes.bindProperty("key", "ORIG_VALUE");	
	ocmbSelectTypes.bindItems("/Rowset/Row", oItemSelectTypes);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgRepReasons",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("RepReasonsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyRepReasons"):oLng_MasterData.getText("MasterData_NewRepReasons"),
            /*"Modifica causale di recupero":"Nuova causale di recupero",*/
            //icon: "/images/address.gif"
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_RepReason"), //"Causale di recupero",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("RepReasonsTab", "ID_REP"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "ID_REP"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("RepReasonsTab", "REASTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "REASTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("RepReasonsTab", "DESCR"):"",
                                editable: true,
                                maxLength: 60,
                                id: "DESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: ocmbSelectTypes
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveRepReasons(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy();
            ocmbSelectTypes.destroy();
        }
    }));    
	oEdtDlg.open();
}

// Funzione per inserire una causale
function fnSaveRepReasons(bEdit) 
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var iD_Causale = $.UIbyID("ID_REP").getValue();
    var reasTxt = $.UIbyID("REASTXT").getValue();
    var description = $.UIbyID("DESCR").getValue();
    var type = $.UIbyID("cmbSelectTypes").getSelectedKey();
    
    if ( plant === "" || iD_Causale === "" || reasTxt === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe =  dataProdMD;
    
    if(bEdit){
        qexe =  qexe + "RepReasons/updRepReasonsSQ";
    }
    else{
        qexe =  qexe + "RepReasons/addRepReasonsSQ";
    }
    
    qexe =  qexe +
        "&Param.1=" + plant +
        "&Param.2=" + iD_Causale +
        "&Param.3=" + reasTxt +
        "&Param.4=" + description +
        "&Param.5=" + type;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_RepReasonsSaved"),
                         oLng_MasterData.getText("MasterData_RepReasonsSavedError")
                         /*"Causale di recupero salvata correttamente", "Errore in salvataggio causale di recupero"*/,
                         false);
    if (ret){
        $.UIbyID("dlgRepReasons").close();
        $.UIbyID("dlgRepReasons").destroy();
        refreshTabRepReasons();
    }
}

function fnDelRepReason(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_RepReasonDeleting"), //"Eliminare la causale selezionata?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "RepReasons/delRepReasonSQ" +
                            "&Param.1=" + fnGetCellValue("RepReasonsTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("RepReasonsTab", "ID_REP");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_RepReasonDeleted"), //"Causale di recupero eliminata correttamente",
                    oLng_MasterData.getText("MasterData_RepReasonDeletedError"), //"Errore in eliminazione causale di recupero",
                    false
                );
                refreshTabRepReasons();
                $.UIbyID("btnModRepReasons").setEnabled(false);
                $.UIbyID("btnDelRepReasons").setEnabled(false);
                $.UIbyID("RepReasonsTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}