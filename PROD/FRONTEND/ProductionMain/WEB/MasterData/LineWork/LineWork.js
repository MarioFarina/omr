/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Linea di lavoro
//Author: Elena Andrini
//Date:   15/02/2017
//Vers:   1.0
//**************************************************************************************

//Leggo la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

//Setto il percorso del file con le traduzioni
var oLng_MasterData = jQuery.sap.resources({
    url: "/XMII/CM/ProductionMain/res/line.i18n.properties",
    locale: sCurrentLocale
});

//Definizione variabili globali
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";

//Carico le librerie
//param1: array di librerie
//param2: function che instanzia gli oggetti
Libraries.load(
    [
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/MasterData/LineWork/fn_LineWork",
        "/XMII/CM/ProductionMain/res/fn_commonTools"
    ],

    function () {
        $(document).ready(function () {
            $("#splash-screen").hide(); //nasconde l'icona di loading
            
        });

        var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
            content: [
                    createToolBarPage(),
					createLineWorkTab()
				]
        });

        oVerticalLayout.placeAt("MasterCont");
        $.UIbyID("filtPlant").setSelectedKey($('#plant').val());
        refreshLineWorkTab();
    }
);