var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

/*
var jType = {root:[]};
jType.root.push({
    key: 'SC',
    text: oLng_MasterData.getText("MasterData_Scrap") //'Scarto'
});
jType.root.push({
    key: 'SO',
    text: oLng_MasterData.getText("MasterData_Suspended") //'Sospeso'
});
*/

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabScrabReasonsGroups() {
    
    //filtro tipo scarto (Scarto/Sospeso)
    var oModelTypesFilter = new sap.ui.model.xml.XMLModel();
    oModelTypesFilter.loadData(QService + dataProdMD + "ScrabReasons/getGrScrabReasonsTypeSQ&Param.1=" + sLanguage);
        
    var oCmbTypesFilter = new sap.ui.commons.ComboBox({
        id: "cmbTypesFilter",
        selectedKey : "",
        change: function (oEvent) {
            refreshTabScrabReasonsGroups();
            $.UIbyID("delFilter").setEnabled(true);
        }        
    });
    oCmbTypesFilter.setModel(oModelTypesFilter);
    var oItemTypesFilter = new sap.ui.core.ListItem();
    oItemTypesFilter.bindProperty("text", "EXTENDED_TEXT");
	oItemTypesFilter.bindProperty("key", "ORIG_VALUE");	
	oCmbTypesFilter.bindItems("/Rowset/Row", oItemTypesFilter);
    
    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "ScrabReasonsGroupsTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_ScrabReasGroupList"), //"Lista macro causali scarto",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("ScrabReasonsGroupsTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModScrabReasonsGroup").setEnabled(false);
        			$.UIbyID("btnDelScrabReasonsGroup").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModScrabReasonsGroup").setEnabled(true);
                    $.UIbyID("btnDelScrabReasonsGroup").setEnabled(true);			
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                        id:   "btnAddScrabReasonsGroup",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openScrabReasonsGroupsEdit(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                        id:   "btnModScrabReasonsGroup",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openScrabReasonsGroupsEdit(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina"
                        id:   "btnDelScrabReasonsGroup",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelScrabReasonsGroup();
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabScrabReasonsGroups();
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_MasterData.getText("MasterData_ScrapSuspendedFilter") //"Filtro Scarto/Sospeso"
                    }),
                    oCmbTypesFilter,
                    new sap.ui.commons.Button({
                        icon: "sap-icon://filter",
                        tooltip: oLng_MasterData.getText("MasterData_RemoveFilter"), //"Rimuovi filtri",
                        enabled: false,
                        id:   "delFilter",
                        press: function () {
                            $.UIbyID("cmbTypesFilter").setSelectedKey("");
                            refreshTabScrabReasonsGroups();
                            $.UIbyID("delFilter").setEnabled(false);
                        }
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "NAME1",
                label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "SCTYPE", 
				label: oLng_MasterData.getText("MasterData_MacroScrapReasons"), //"Macro causale scarto",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "REASTXT",
				label: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "DESCR", 
				label: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
				properties: {
					width: "130px"
				}
            },
            {
				Field: "ENABLED",
				label: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
				properties: {
					width: "50px"
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "TYPE",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "GR_TYPE",
				label: oLng_MasterData.getText("MasterData_ReasType"), //"Tipo causale",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "ID_REP",
				properties: {
					width: "100px",
                    visible: false
				}
            },
            {
				Field: "REC_REASTXT",
				label: oLng_MasterData.getText("MasterData_Recovery"), //"Recupero",
				properties: {
					width: "75px"
				}
            },
            {
				Field: "REC_TYPE",
				label: oLng_MasterData.getText("MasterData_RecType"), //"Tipo Recupero",
				properties: {
					width: "75px"
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabScrabReasonsGroups();
    return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabScrabReasonsGroups() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("ScrabReasonsGroupsTab").getModel().loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + $('#plant').val() +
            "&Param.2=" + $.UIbyID("cmbTypesFilter").getSelectedKey() +
            "&Param.3=" + sLanguage
        );
    }
    else{
        $.UIbyID("ScrabReasonsGroupsTab").getModel().loadData(
            QService + dataProdMD +
            "ScrabReasons/getScrabReasonsGroupsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() +
            "&Param.2=" + $.UIbyID("cmbTypesFilter").getSelectedKey() +
            "&Param.3=" + sLanguage
        );
    }
    $.UIbyID("ScrabReasonsGroupsTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Gruppi Causali di scarto
function openScrabReasonsGroupsEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oModel3 = new sap.ui.model.xml.XMLModel();
	oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput", 
        selectedKey : bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey(),
        enabled: false
    });
	oCmbPlantInput.setModel(oModel3);		
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");	
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);
    
    var oModelSelectTypes = new sap.ui.model.xml.XMLModel();
    oModelSelectTypes.loadData(QService + dataProdMD + "ScrabReasons/getGrScrabReasonsTypeSQ&Param.1=" + sLanguage);
        
    var ocmbSelectTypes = new sap.ui.commons.ComboBox({
        id: "cmbSelectTypes",
        selectedKey : bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "TYPE"):"",
        change: function (oEvent) {
            $.UIbyID("cmbRecInput").setSelectedKey("");
            if($.UIbyID("cmbSelectTypes").getSelectedKey() === 'SC'){
                $.UIbyID("cmbRecInput").setEnabled(false);
            }
            else{
                $.UIbyID("cmbRecInput").setEnabled(true);
            }
        } 
    });
    ocmbSelectTypes.setModel(oModelSelectTypes);
    var oItemSelectTypes = new sap.ui.core.ListItem();
    oItemSelectTypes.bindProperty("text", "EXTENDED_TEXT");
	oItemSelectTypes.bindProperty("key", "ORIG_VALUE");	
	ocmbSelectTypes.bindItems("/Rowset/Row", oItemSelectTypes);
    
    var oModelRecovery = new sap.ui.model.xml.XMLModel();
	oModelRecovery.loadData(QService + dataProdMD + "RepReasons/getRepReasonsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + sLanguage);

	// Crea la ComboBox per le divisioni in input
	var oCmbRecInput = new sap.ui.commons.ComboBox({
        id: "cmbRecInput", 
        selectedKey : bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "ID_REP"):"",
        enabled: bEdit?((fnGetCellValue("ScrabReasonsGroupsTab", "TYPE") === "SO")?true:false):true,
    });
	oCmbRecInput.setModel(oModelRecovery);		
	var oItemRecInput = new sap.ui.core.ListItem();
    oItemRecInput.bindProperty("text", "REASTXT");
	oItemRecInput.bindProperty("key", "ID_REP");	
	oCmbRecInput.bindItems("/Rowset/Row", oItemRecInput);
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgScrabReasonsGroup",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    //(fnGetCellValue("ScrabReasonsGroupsTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_MasterData.getText("MasterData_ModifyScrabReasonsGroup"):oLng_MasterData.getText("MasterData_NewScrabReasonsGroup"),
            /*"Modifica Gruppo causali di scarto":"Nuovo Gruppo causali di scarto",*/
            //icon: "/images/address.gif"
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MacroScrapReasons"), //"Macro causale scarto",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "SCTYPE"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "SCTYPE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Text"), //"Descrizione breve",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "REASTXT"):"",
                                editable: true,
                                maxLength: 30,
                                id: "REASTXT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExtendedText"), //"Descrizione estesa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("ScrabReasonsGroupsTab", "DESCR"):"",
                                editable: true,
                                maxLength: 60,
                                id: "DESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: bEdit?(fnGetCellValue("ScrabReasonsGroupsTab", "ENABLED") == "0" ? false : true ) : true,
                                /*bEdit?(fnGetCellValue("ScrabReasonsGroupsTab", "ACTIVE") == "0" ? false : true ) : false,*/
                                id: "ENABLED" /*"ACTIVE"*/
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ReasType"), //"Tipo causale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: ocmbSelectTypes
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Recovery"), //"Recupero",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbRecInput
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            fnSaveScrabReasonsGroup(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy(); 
        }
    }));    
	oEdtDlg.open();
}

// Funzione per inserire un gruppo causale
function fnSaveScrabReasonsGroup(bEdit) 
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var ID_GruppoCausale = $.UIbyID("SCTYPE").getValue();
    var causale = $.UIbyID("REASTXT").getValue();
    var description = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
    var reasonType = $.UIbyID("cmbSelectTypes").getSelectedKey();
    var recovery = $.UIbyID("cmbRecInput").getSelectedKey();
    
    if ( plant === "" || ID_GruppoCausale === "" || causale === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe =  dataProdMD;
    if(bEdit){
        qexe += "ScrabReasons/updScrabReasonsGroupSQ";
    }
    else{
        qexe += "ScrabReasons/addScrabReasonsGroupSQ";
    }
    qexe +=  "&Param.1=" + plant +
        "&Param.2=" + ID_GruppoCausale +
        "&Param.3=" + causale +
        "&Param.4=" + description +
        "&Param.5=" + active +
        "&Param.6=" + reasonType +
        "&Param.7=" + recovery;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_ScrabReasonsGroupSaved"),
                         oLng_MasterData.getText("MasterData_ScrabReasonsGroupSavedError")
                         /*"Gruppo causali di scarto salvato correttamente", "Errore in salvataggio gruppo causali di scarto"*/,
                         false);
    if (ret){
        $.UIbyID("dlgScrabReasonsGroup").close();
        $.UIbyID("dlgScrabReasonsGroup").destroy();
        refreshTabScrabReasonsGroups();
    }
}

// Funzione per salvare le modifiche al gruppo causale o inserirlo
/*
function fnSaveScrabReasonsGroup() 
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var ID_GruppoCausale = $.UIbyID("SCTYPE").getValue();
    var causale = $.UIbyID("REASTXT").getValue();
    var description = $.UIbyID("DESCR").getValue();
    var active = $.UIbyID("ENABLED").getChecked() === true ? "1" : "0";
    
    if ( plant === "" || ID_GruppoCausale === "" || causale === "" ) {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    
    var qexe =  dataProdMD +
                "ScrabReasons/updScrabReasonsGroupSQ&Param.1=" + plant +
                "&Param.2=" + ID_GruppoCausale +
                "&Param.3=" + causale +
                "&Param.4=" + description +
                "&Param.5=" + active;
    
    var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_ScrabReasonsGroupSaved"),
                         oLng_MasterData.getText("MasterData_ScrabReasonsGroupSavedError"),
                         false);
    if (ret){
        $.UIbyID("dlgScrabReasonsGroup").close();
        $.UIbyID("dlgScrabReasonsGroup").destroy();
        refreshTabScrabReasonsGroups();
    }
}*/

function fnDelScrabReasonsGroup(){
    
    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_ScrabReasonsGroupDeleting"), //"Eliminare il gruppo di causali selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "ScrabReasons/delScrabReasonsGroupSQ" +
                            "&Param.1=" + fnGetCellValue("ScrabReasonsGroupsTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("ScrabReasonsGroupsTab", "SCTYPE");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_ScrabReasonsGroupDeleted"), //"Gruppo causali di scarto eliminato correttamente",
                    oLng_MasterData.getText("MasterData_ScrabReasonsGroupDeletedError"), //"Errore in eliminazione gruppo causali di scarto",
                    false
                );
                refreshTabScrabReasonsGroups();
                $.UIbyID("btnModScrabReasonsGroup").setEnabled(false);
                $.UIbyID("btnDelScrabReasonsGroup").setEnabled(false);
                $.UIbyID("ScrabReasonsGroupsTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}