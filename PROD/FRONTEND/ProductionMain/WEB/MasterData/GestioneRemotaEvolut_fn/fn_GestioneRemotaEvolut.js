var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();


// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabPartProgram() {

    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "GestioneRemotaEvolutTab",
        properties: {
           title: oLng_MasterData.getText("MasterData_GestioneEvolutList"), 
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function (oControlEvent) {
            }
        },
        exportButton: false,
				toolbarItems: [
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"),
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function (){
                  refreshTabPartProgram();
                }
            })
        ],
        columns: [
					{
						Field: "ID",
						label: "Id",
						properties: {
							width: "100px",
							flexible : false
						}
          }, {
						Field: "Linea",
            label: "Linea",
						properties: {
							width: "50px",
							flexible : false
						}
          }, {
						Field: "CodiceGrezzo",
						label: "Codice Grezzo",
						properties: {
							width: "140px",
							flexible : false
						}
          }, {
						Field: "CodiceFinito",
						label: "Codice Finito",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "Tipologia",
						label: "Tipologia",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "FlagAttivo",
						label: "Flag Attivo",
						properties: {
							width: "160px",
							flexible : false
						},
						template: {
							type: "checked",
							enabled: true,
							change: function (oEvent) {
								var grezzo = oEvent.getSource().getBindingContext().getProperty("CodiceGrezzo");
								var id = oEvent.getSource().getBindingContext().getProperty("ID");
								var checked = oEvent.getParameter('checked');
								var that = this;
								sap.ui.commons.MessageBox.show(
									"Cambiare abilitazione per grezzo: " + grezzo,
									sap.ui.commons.MessageBox.Icon.WARNING,
									"Abilitazione grezzo",
									[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
									function(sResult) {
											if (sResult == 'YES') {
												var flagAttivo = checked ? 1 : 0;
												var qexe = dataProdPP + "PartProgram/setFlagAttivoProdottiQR&Param.1=" + $.UIbyID("filtLine").getSelectedKey() + "&Param.2=" + id +	"&Param.3=" + flagAttivo;
												var ret = fnGetAjaxVal(qexe, ["resultCode"], false);
												if(ret.resultCode != 0) {
													sap.ui.commons.MessageBox.show("Errore in aggiornamento flag attivo", sap.ui.commons.MessageBox.Icon.ERROR, "Errore", sap.ui.commons.MessageBox.Action.OK);
													if(checked) {
														that.setChecked(false);
													} else {
														that.setChecked(true);
													}
												} else {
													refreshTabPartProgram();
												}
											} else {
												if(checked) {
													that.setChecked(false);
												} else {
													that.setChecked(true);
												}
											}
									},
									sap.ui.commons.MessageBox.Action.YES
								);
							},
							textAlign: "Center"
		        }
	        }, {
						Field: "DigitPerGrezzo",
						label: "DigitPerGrezzo",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "DigitPerCella",
						label: "DigitPerCella",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "DigitPerStampo",
						label: "DigitPerStampo",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "CodiceMercatrice",
						label: "CodiceMercatrice",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "Peso",
						label: "Peso",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "TolleranzaPiu",
						label: "TolleranzaPiu",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "TolleranzaMeno",
						label: "TolleranzaMeno",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "QTY_OUT",
						label: "Qty Out",
						properties: {
							width: "110px",
							flexible : false
						}
          }, {
						Field: "QTY_PLAN",
						label: "Qty Plan",
						properties: {
							width: "110px",
							flexible : false
						}
          }
    		]
    });

    oModel = new sap.ui.model.xml.XMLModel();

    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabPartProgram();
    return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabPartProgram() {
		$.UIbyID("GestioneRemotaEvolutTab").getModel().loadData(
        QService + dataProdPP +
        "PartProgram/getProdottiQR&Param.1=" + $.UIbyID("filtLine").getSelectedKey()
    );
    $("#splash-screen").hide();
}


// Function per creare la finestra di dialogo PProgram
function openPartProgramEdit(bEdit) {
    if (bEdit === undefined) bEdit = 0;

    var oModelPlant = new sap.ui.model.xml.XMLModel();
	oModelPlant.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni in input
	var oCmbPlantInput = new sap.ui.commons.ComboBox({
        id: "cmbPlantInput",
        selectedKey : (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey(),
        enabled: false,
        change: function() {
            $.UIbyID("cmbPPLine").getModel().loadData(
                QService + dataProdMD +
                "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("cmbPlantInput").getSelectedKey()
            );
            $.UIbyID("cmbPPLine").setSelectedKey("");
            $.UIbyID("cmbPPMach").setSelectedKey("");
            $.UIbyID("cmbMaterial").setSelectedKey("");
        }
    });
	oCmbPlantInput.setModel(oModelPlant);
	var oItemPlantInput = new sap.ui.core.ListItem();
    // oItemPlantInput.bindProperty("text", "PLNAME");
	oItemPlantInput.bindProperty("text", "NAME1");
	oItemPlantInput.bindProperty("key", "PLANT");
	oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);

    var oModelLine = new sap.ui.model.xml.XMLModel();
	oModelLine.loadData(
        QService + dataProdMD +
        "Lines/getLinesbyPlantSQ&Param.1=" +
        ((bEdit != 0)? fnGetCellValue("GestioneRemotaEvolutTab", "PLANT") : $.UIbyID("filtPlant").getSelectedKey())
    );
	// Crea la ComboBox per le linee in input
	var oCmbPPLine = new sap.ui.commons.ComboBox({
        id: "cmbPPLine",
        selectedKey : (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "IDLINE"):1,
        change: function() {
            $.UIbyID("cmbPPMach").getModel().loadData(
                QService + dataProdMD +
                "Machines/getMachinesByLineSQ&Param.1=" + $.UIbyID("cmbPPLine").getSelectedKey()
            );
            $.UIbyID("cmbPPMach").setSelectedKey("");
        }
    });
	oCmbPPLine.setModel(oModelLine);
	var oItemPPLine = new sap.ui.core.ListItem();
    oItemPPLine.bindProperty("text", "MACHTXT");
	oItemPPLine.bindProperty("key", "IDMACH");
	oCmbPPLine.bindItems("/Rowset/Row", oItemPPLine);

    var oModelMach = new sap.ui.model.xml.XMLModel();
	oModelMach.loadData(
        QService + dataProdMD +
        "Machines/getMachinesByLineSQ&Param.1=" +
        (bEdit? fnGetCellValue("GestioneRemotaEvolutTab", "IDLINE") : $.UIbyID("cmbPPLine").getSelectedKey())
    );
	// Crea la ComboBox per le macchine in input
	var oCmbPPMach = new sap.ui.commons.ComboBox({
        id: "cmbPPMach",
        selectedKey : (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "IDMACH"):""
    });
	oCmbPPMach.setModel(oModelMach);
	var oItemPPMach = new sap.ui.core.ListItem();
    oItemPPMach.bindProperty("text", "MACHTXT");
	oItemPPMach.bindProperty("key", "IDMACH");
	oCmbPPMach.bindItems("/Rowset/Row", oItemPPMach);

    var oCmbMaterial = new sap.ui.commons.ComboBox("cmbMaterial", {
        change: function (oEvent) {},
        selectedKey : (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "MATERIAL"):""
    });
    var oItemMaterial = new sap.ui.core.ListItem();
    oItemMaterial.bindProperty("key", "MATERIAL");
    oItemMaterial.bindProperty("text", "DESCRIPTION");
    oCmbMaterial.bindItems("/Rowset/Row", oItemMaterial);
    var oMaterialModel = new sap.ui.model.xml.XMLModel();
    oMaterialModel.loadData(
        QService + dataProdMD +
        "Material/getMaterialDescByPlantSQ&Param.1=" +
        ((bEdit != 0)? fnGetCellValue("GestioneRemotaEvolutTab", "PLANT") : $.UIbyID("filtPlant").getSelectedKey())
    );
    oCmbMaterial.setModel(oMaterialModel);

    //  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgPartProgram",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });

    //(fnGetCellValue("GestioneRemotaEvolutTab", "FixedVal")==1)?true : false
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: (bEdit == 1)?oLng_MasterData.getText("MasterData_ModifyPProgram"):oLng_MasterData.getText("MasterData_NewPProgram"),
            /*"Modifica Part Program":"Nuovo Part Program",*/
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbPlantInput
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_LineID"), //"ID Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !(bEdit == 1),
                        fields: oCmbPPLine.setEnabled(!(bEdit == 1))
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Machine"), //"Macchina",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        editable: !(bEdit == 1),
                        fields: oCmbPPMach.setEnabled(!(bEdit == 1))
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_MaterialCode"), //"Materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: oCmbMaterial.setEnabled(!(bEdit == 1))
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Version"), //"Versione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "VERSION"):"",
                                editable: !(bEdit == 1),
                                maxLength: 3,
                                id: "txtVERSION"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Alt"), //"ALT",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit == 1)?fnGetCellValue("GestioneRemotaEvolutTab", "ALT"):"",
                                editable: false,
                                maxLength: 11,
                                id: "txtALT",
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Default"), //"Default",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.CheckBox({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                checked: (bEdit == 1)?(fnGetCellValue("GestioneRemotaEvolutTab", "DEFAULT") == "0" ? false : true ) : false,
                                id: "chkDEFAULT"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PartProgram"), //"Part Program",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.unified.FileUploader({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "5"}),
                                value: oLng_MasterData.getText("MasterData_UploadFile"), //"Caricare File",
                                editable: true,
                                id: "txtPPROGRAM",
                                multiple: false,
                                maximumFileSize: 20,
                                mimeType: "image,text,*",
                                fileType: "jpg,png,txt,*",
                                uploadOnChange: false,
                                uploadUrl: "/XMII/CM/ProductionMain/MasterData/PartProgram_fn/partProgramUpload_1.jsp",
                                /*change: function (oEvent){
                                    var sName = "";
                                    sName = oEvent.getParameter("fileName");
                                    $.UIbyID("txtPPROGRAM").setValue(sName);
                                },*/
                                fileSizeExceed: function (oEvent) {
                                    var sName = oEvent.getParameter("fileName");
                                    var fSize = oEvent.getParameter("fileSize");
                                    var fLimit = $.UIbyID("txtPPROGRAM").getMaximumFileSize();
                                    sap.ui.commons.MessageBox.show(
                                        oLng_MasterData.getText("MasterData_FileOutOfSize") //"La dimensione massima di un file è di MB:"
                                        + " " + fSize
                                    );
                                },
                                typeMissmatch: function (oEvent) {
                                    var sName = oEvent.getParameter("fileName");
                                    var sType = oEvent.getParameter("fileType");
                                    var sMimeType = $.UIbyID("txtPPROGRAM").getMimeType();
                                    if (!sMimeType) {
                                        sMimeType = $.UIbyID("txtPPROGRAM").getFileType();
                                    }
                                    sap.ui.commons.MessageBox.show(
                                        oLng_MasterData.getText("MasterData_AllowedFileTypes") //"Tipi di file consentiti:"
                                        + " " + sMimeType
                                    );
                                },
                                uploadComplete: function (oEvent) {
                                    var sResponse = oEvent.getParameter("response");
                                    console.debug(oEvent.getParameter("response"));
                                    if (sResponse) {
                                        if((bEdit == 1)){
                                            fnSavePartProgram(sResponse);
                                        }
                                        else{
                                            fnAddPartProgram(sResponse);
                                        }
                                        /*
                                        var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
                                        if (m[1] == "200") {
                                            sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "SUCCESS", "Upload Success");
                                        } else {
                                            sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "ERROR", "Upload Error");
                                        }
                                        */
                                    }
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PartProgramName"), //"Nome programma",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "PRGNUM"):"",
                                editable: true,
                                maxLength: 50,
                                id: "txtPRGNUM"
                            })
                        ]
                    }),new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_PartProgramVar"), //"Variabili programma",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit != 0)?fnGetCellValue("GestioneRemotaEvolutTab", "PRGVAR"):"",
                                editable: true,
                                maxLength: 50,
                                id: "txtPRGVAR"
                            })
                        ]
                    }),new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit == 1)?fnGetCellValue("GestioneRemotaEvolutTab", "DESCR"):"",
                                editable: true,
                                maxLength: 30,
                                id: "txtDESCR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: (bEdit == 1)?fnGetCellValue("GestioneRemotaEvolutTab", "NOTE"):"",
                                editable: true,
                                maxLength: 60,
                                id: "txtNOTE"
                            })
                        ]
                    })
                ]
            })
        ]
    });

    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
        press:function(){
            $.UIbyID("txtPPROGRAM").upload();
            /*
            if((bEdit == 1)){
                fnSavePartProgram();
            }
            else{
                fnAddPartProgram();
            }
            */
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlantInput.destroy();
            oCmbPPLine.destroy();
            oCmbPPMach.destroy();
        }
    }));
	oEdtDlg.open();
}

// Funzione per aggiungere un part program
function fnAddPartProgram(sPartProgram)
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var idLine = $.UIbyID("cmbPPLine").getSelectedKey();
    if(idLine == "" || idLine == null){
        sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
        return;
    }
    var idMach = $.UIbyID("cmbPPMach").getSelectedKey();
    if(idMach == "" || idMach == null){
        sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
        return;
    }
    var sMaterial = $.UIbyID("cmbMaterial").getSelectedKey();
    if(sMaterial == "" || sMaterial == null){
        sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
        return;
    }
    var sVersion = $.UIbyID("txtVERSION").getValue();
    if(sVersion == "" || sVersion == null){
        sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("MasterData_MustCompile")); //Compilare i campi obbligatori
        return;
    }
    var sAlt = -1;
    var bDefault = $.UIbyID("chkDEFAULT").getChecked() === true ? "1" : "0";
    var sPProgram = sPartProgram;
    var sPrgNum = $.UIbyID("txtPRGNUM").getValue();
    var sPrgVar = $.UIbyID("txtPRGVAR").getValue();
    var sDescription = $.UIbyID("txtDESCR").getValue();
    var sNote = $.UIbyID("txtNOTE").getValue();

    var qexe =  dataProdMD +
                "PartProgram/addPartProgramQR" +
                "&Param.1=" + sMaterial +
                "&Param.2=" + idLine +
                "&Param.3=" + plant +
                "&Param.4=" + idMach +
                "&Param.5=" + sVersion +
                "&Param.6=" + sAlt +
                "&Param.7=" + bDefault +
                "&Param.8=" + sPProgram +
                "&Param.9=" + sDescription +
                "&Param.10=" + sNote +
                "&Param.11=" + sPrgNum +
                "&Param.12=" + sPrgVar;

    var ret = 1;
    var aAddResult = fnGetAjaxVal(qexe,["addResult","calculatedALT"],false);
    const iCheckResult = parseInt(aAddResult.addResult);
    sAlt = parseInt(aAddResult.calculatedALT);

    if(iCheckResult === 2){
        sap.ui.commons.MessageBox.show(
            oLng_MasterData.getText("MasterData_PartProgramAlreadyActive"), //"C'è già un Part Program attivo per questa versione. Attivare questo in fase d'inserimento?",
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_MasterData.getText("MasterData_ActivationConfirm"), //"Conferma attivazione",
            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
            function(sResult){
                if (sResult == 'YES'){
                    var qexe2 = dataProdMD + "PartProgram/updatePartProgramDefaultStatusQR" +
                            "&Param.1=" + sMaterial +
                            "&Param.2=" + idLine +
                            "&Param.3=" + plant +
                            "&Param.4=" + idMach +
                            "&Param.5=" + sVersion +
                            "&Param.6=" + sAlt;

                    ret = fnSQLQuery(
                        qexe2,
                        oLng_MasterData.getText("MasterData_PartProgramSavedActivated"), //"Part Program aggiunto correttamente e attivato",
                        oLng_MasterData.getText("MasterData_PartProgramSaved"), //"Part Program aggiunto correttamente",
                        false
                    );
                    refreshTabPartProgram();
                }
            },
            sap.ui.commons.MessageBox.Action.YES
        );
    }
    else{
        sap.ui.commons.MessageBox.show(
            oLng_MasterData.getText("MasterData_PartProgramSaved"), //Part Program aggiunto correttamente",
            sap.ui.commons.MessageBox.Icon.INFORMATION,
            oLng_MasterData.getText("MasterData_Saving"), //"Salvataggio",
            [sap.ui.commons.MessageBox.Action.OK],
            function(sResult){},
            sap.ui.commons.MessageBox.Action.OK
        );
    }

    if (ret){
        $.UIbyID("dlgPartProgram").close();
        $.UIbyID("dlgPartProgram").destroy();
        refreshTabPartProgram();
    }
}

// Funzione per salvare le modifiche alla causale o inserirla
function fnSavePartProgram(sPartProgram)
{
    var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
    var idLine = $.UIbyID("cmbPPLine").getSelectedKey();
    var idMach = $.UIbyID("cmbPPMach").getSelectedKey();
    var sMaterial = $.UIbyID("cmbMaterial").getSelectedKey();
    var sVersion = $.UIbyID("txtVERSION").getValue();
    var sAlt = $.UIbyID("txtALT").getValue();
    var bDefault = $.UIbyID("chkDEFAULT").getChecked() === true ? "1" : "0";
    var sPProgram = sPartProgram;
    var sPrgNum = $.UIbyID("txtPRGNUM").getValue();
    var sPrgVar = $.UIbyID("txtPRGVAR").getValue();
    var sDescription = $.UIbyID("txtDESCR").getValue();
    var sNote = $.UIbyID("txtNOTE").getValue();

    var qexe =  dataProdMD +
                "PartProgram/updatePartProgramQR" +
                "&Param.1=" + sMaterial +
                "&Param.2=" + idLine +
                "&Param.3=" + plant +
                "&Param.4=" + idMach +
                "&Param.5=" + sVersion +
                "&Param.6=" + sAlt +
                "&Param.7=" + bDefault +
                "&Param.8=" + sPProgram +
                "&Param.9=" + sDescription +
                "&Param.10=" + sNote +
                "&Param.11=" + sPrgNum +
                "&Param.12=" + encodeURIComponent(sPrgVar);

    var ret = 1;
    //var iCheckResult = parseInt(fnGetAjaxVal(qexe,["updateResult"],false));
    var aUpdResult = fnGetAjaxVal(qexe,["updateResult"],false);
    const iCheckResult = parseInt(aUpdResult.updateResult);

    if(iCheckResult === 2){
        sap.ui.commons.MessageBox.show(
            oLng_MasterData.getText("MasterData_PartProgramAlreadyActiveModify"), //"C'è già un Part Program attivo per questa versione. Attivare questo in fase di modifica?",
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_MasterData.getText("MasterData_ActivationConfirm"), //"Conferma attivazione",
            [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
            function(sResult){
                if (sResult == 'YES'){
                    var qexe2 = dataProdMD + "PartProgram/updatePartProgramDefaultStatusQR" +
                            "&Param.1=" + sMaterial +
                            "&Param.2=" + idLine +
                            "&Param.3=" + plant +
                            "&Param.4=" + idMach +
                            "&Param.5=" + sVersion +
                            "&Param.6=" + sAlt;

                    ret = fnSQLQuery(
                        qexe2,
                        oLng_MasterData.getText("MasterData_PartProgramUpdatedActivated"), //"Part Program modificato correttamente e attivato",
                        oLng_MasterData.getText("MasterData_PartProgramUpdated"), //"Part Program modificato correttamente",
                        false
                    );
                    refreshTabPartProgram();
                }
            },
            sap.ui.commons.MessageBox.Action.YES
        );
    }
    else{
        sap.ui.commons.MessageBox.show(
            oLng_MasterData.getText("MasterData_PartProgramUpdated"), //"Part Program modificato correttamente",
            sap.ui.commons.MessageBox.Icon.INFORMATION,
            oLng_MasterData.getText("MasterData_Saving"), //"Salvataggio",
            [sap.ui.commons.MessageBox.Action.OK],
            function(sResult){},
            sap.ui.commons.MessageBox.Action.OK
        );
    }

    if (ret){
        $.UIbyID("dlgPartProgram").close();
        $.UIbyID("dlgPartProgram").destroy();
        refreshTabPartProgram();
    }
}

function fnDelPProgram(){

    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_PProgramDeleting"), //"Eliminare il Part Program selezionato?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdMD + "PartProgram/delPartProgramSQ" +
                            "&Param.1=" + fnGetCellValue("GestioneRemotaEvolutTab", "MATERIAL") +
                            "&Param.2=" + fnGetCellValue("GestioneRemotaEvolutTab", "IDLINE") +
                            "&Param.3=" + fnGetCellValue("GestioneRemotaEvolutTab", "PLANT") +
                            "&Param.4=" + fnGetCellValue("GestioneRemotaEvolutTab", "IDMACH") +
                            "&Param.5=" + fnGetCellValue("GestioneRemotaEvolutTab", "VERSION") +
                            "&Param.6=" + fnGetCellValue("GestioneRemotaEvolutTab", "ALT");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_PProgramDeleted"), //"Part Program eliminato correttamente",
                    oLng_MasterData.getText("MasterData_PProgramDeletedError"), //"Errore in eliminazione Part Program",
                    false
                );
                refreshTabPartProgram();
                $.UIbyID("btnModPartProgram").setEnabled(false);
                $.UIbyID("btnDelPartProgram").setEnabled(false);
                $.UIbyID("GestioneRemotaEvolutTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnShowPProgram(){

    var qexe = dataProdMD +
        "PartProgram/getPartProgramTextByIdSQ" +
        "&Param.1=" + fnGetCellValue("GestioneRemotaEvolutTab", "MATERIAL") +
        "&Param.2=" + fnGetCellValue("GestioneRemotaEvolutTab", "IDLINE") +
        "&Param.3=" + fnGetCellValue("GestioneRemotaEvolutTab", "PLANT") +
        "&Param.4=" + fnGetCellValue("GestioneRemotaEvolutTab", "IDMACH") +
        "&Param.5=" + fnGetCellValue("GestioneRemotaEvolutTab", "VERSION") +
        "&Param.6=" + fnGetCellValue("GestioneRemotaEvolutTab", "ALT");

    var sPProgram = fnGetAjaxVal(qexe,"PPROGRAM",false);

    //  Crea la finestra di dialogo
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgPartProgramText",
        maxWidth: "1024px",
		Width: "900px",
		maxHeight: "600px",
		Height: "500px",
		showCloseButton: false
    });

    var pProgramArea = new sap.ui.commons.TextArea({
		rows: 30,
		cols: 180,
        editable: false,
		value: sPProgram
	});

	oEdtDlg.addContent(pProgramArea);
    oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_MasterData.getText("MasterData_Close"), //"Chiudi",
		press: function () {
			oEdtDlg.close();
			oEdtDlg.destroy()
		}
	}));
	oEdtDlg.open();
}

function sendPartProgram() {


}
