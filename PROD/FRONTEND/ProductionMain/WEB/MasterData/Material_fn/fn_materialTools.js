//**************************************************************************************
//Title:  Funzioni per il recupero delle informazioni dei materiali
//Author: Bruno Rosati
//Date:   05/10/2017
//Vers:   1.0
//**************************************************************************************
// Script per il recupero delle informazioni dei materiali

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function fnGetMaterialTableDialog(sPlant, sSelected, cbFunction, bDraw, bMultiSelect) {

	if(bMultiSelect === undefined){
		bMultiSelect = true;
	}

	if (!$.UIbyID("dlgMaterialTableSelect")) {

		var oMatModel = new sap.ui.model.xml.XMLModel();
		oMatModel.loadData(QService + dataProdMD + "Material/getMaterialByPlantSQ&Param.1=" + sPlant);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				bMultiSelect ? (new sap.m.Text({
					text: "",
					title: ""
				})) : (null),
				bDraw?(new sap.m.Text({
					text: "{DISEGNO}",
					title: "{}"
				})):(new sap.m.ObjectIdentifier({
					text: "{DESC}",
					title: "{MATERIAL}"
				})),
				bDraw?(new sap.m.ObjectIdentifier({
					text: "{DESC}",
					title: "{MATERIAL}"
				})):(new sap.m.Text({
					text: "{DISEGNO}",
					title: "{}"
				})),
				/*new sap.m.Text({
					text: "{OPDESC}",
                    title: "{}"
				}),*/
				new sap.m.Text({
					text: "{MATL_TYPE}",
					title: "{}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgMaterialTableSelect", {
			title: bDraw? oLng_MasterData.getText("MasterData_DrawsList"):
			oLng_MasterData.getText("MasterData_MaterialList"), //"Lista disegni" || "Lista materiali"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: bMultiSelect,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns:[
				bMultiSelect ? (new sap.m.Column({header: new sap.m.Label({
					text: "", //""
					minScreenWidth: "40em"
				})})) : (null),
				new sap.m.Column({header: new sap.m.Label({
					text: bDraw?
					oLng_MasterData.getText("MasterData_Draw"):oLng_MasterData.getText("MasterData_MaterialCode"), 
					//"Disegno" || "Materiale" 
					minScreenWidth: "40em"
				})}),
				new sap.m.Column({header: new sap.m.Label({
					text: bDraw?
					oLng_MasterData.getText("MasterData_MaterialCode"):oLng_MasterData.getText("MasterData_Draw"), 
					//"Materiale" || "Disegno"
					minScreenWidth: "40em"
				})}),
				/*new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Type"), //"Tipo"
                    minScreenWidth: "Tablet"
                })}),*/
				new sap.m.Column({header: new sap.m.Label({
					text: oLng_MasterData.getText("MasterData_MaterialType"), //"Tipo materiale"
					minScreenWidth: "40em"
				})})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("DESC", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("MATERIAL", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("MATL_TYPE", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("DISEGNO", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
			cancel: function (oControlEvent){
				$.UIbyID("dlgMaterialTableSelect").destroy();
			}
		});

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			length: 10000, 
			template: itemSources
		});

		odlgSourcesSelect.setModel(oMatModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgMaterialTableSelect").setContentWidth("800px");


	if (sSelected == "" || sSelected == null) 
		$.UIbyID("dlgMaterialTableSelect").open();
	else {
		$.UIbyID("dlgMaterialTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SOURCE", sap.ui.model.FilterOperator.EQ, sSelected),
					 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgMaterialTableSelect").open(sSelected);
	}

	$.UIbyID("dlgMaterialTableSelect").focus();

}