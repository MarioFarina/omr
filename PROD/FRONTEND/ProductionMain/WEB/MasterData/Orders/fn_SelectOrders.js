/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300 , -W098*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Funzioni per il recupero degli ordini
//Author: Luca Adanti
//Date:   25/11/2018
//Vers:   1.0.8
//Modifiers: Luca Adanti 08/11/2018
//**************************************************************************************
// Script per il recupero degli ordini

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";


// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function fnGetOrdersTableDialog(oParams, cbFunction) {

	//oParams.Selected
	//oParams.Material
	//oParams.Plant
	//oParams.TypeOrdFilter

	if (!$.UIbyID("dlgOrdersTableSelect")) {

		var oOrderModel = new sap.ui.model.xml.XMLModel();
		oOrderModel.loadData(QService + dataProdMD + "Orders/getOrdersListSQ&Param.1=" + oParams.Plant + "&Param.2=" + oParams.TypeOrdFilter);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{ORDER}",
					//title: "{ORDER}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{MATERIAL}",
					title: "{DESC}"
				}),
				new sap.m.Text({
					text: "{TYPEDESC}",
					title: "{}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgOrdersTableSelect", {
			title: oLng_MasterData.getText("MasterData_Orders"), //"Lista Ordini"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns: [
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_Order"), //"Nome Ordine"
						minScreenWidth: "40em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_MaterialCode"), //"Gruppo"
						minScreenWidth: "0.5em"
					})
				}),
				/*
								new sap.m.Column({
									header: new sap.m.Label({
										text: oLng_MasterData.getText("MasterData_MaterialDescr"), //"Descrizione"
										minScreenWidth: "Tablet"
									})
								}),*/
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_OrdFilType"), //"Note"
						minScreenWidth: "40em"
					})
				})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue !== "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("ORDER", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("MATERIAL", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("DESC", sap.ui.model.FilterOperator.Contains, sValue),
						 //new sap.ui.model.Filter("TYPEDESC", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else {
					oBinding.filter([]);
				}
			},
			confirm: cbFunction,
			cancel: function (oControlEvent) {
				$.UIbyID("dlgOrdersTableSelect").destroy();
			}
		}).data("FilterOrder", oParams.TypeOrdFilter);

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOrderModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgOrdersTableSelect").setContentWidth("800px");

	if ($.UIbyID("dlgOrdersTableSelect").data("FilterOrder") !== oParams.TypeOrdFilter) {
		$.UIbyID("dlgOrdersTableSelect").getModel().loadData(QService + dataProdMD + "Orders/getOrdersListSQ&Param.1=" + oParams.Plant + "&Param.2=" + oParams.TypeOrdFilter);
	}

	if (oParams.Selected !== "" && oParams.Selected !== null) {
		$.UIbyID("dlgOrdersTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("ORDER", sap.ui.model.FilterOperator.EQ, oParams.Selected),
			sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOrdersTableSelect").open(oParams.Selected);
	} else if (oParams.Material !== "" && oParams.Material !== null) {
		$.UIbyID("dlgOrdersTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("MATERIAL", sap.ui.model.FilterOperator.EQ, oParams.Material),
			sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOrdersTableSelect").open(oParams.Material);
	} else {
		$.UIbyID("dlgOrdersTableSelect").open();
	}

	$.UIbyID("dlgOrdersTableSelect").focus();

}

function fnGetOrderOpersTableDialog(oParams, cbFunction) {

	//oParams.Selected
	//oParams.Order
	//oParams.Plant

	if (!$.UIbyID("dlgOrderPopersTableSelect")) {

		var oOrderModel = new sap.ui.model.xml.XMLModel();
		oOrderModel.loadData(QService + dataProdMD + "Cycles/getOrdersOpersListSQ&Param.1=" + oParams.Plant + "&Param.2=" + oParams.Order);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{POPER}",
					title: "{CICTXT}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{CDLID}",
					title: "{CDLDESC}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{STEUS}",
					title: "{STEUS_DESC}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{FORNID}",
					title: "{FORNNAME}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgOrderPopersTableSelect", {
			title: oLng_MasterData.getText("MasterData_POpers"), //"Lista Ordini"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns: [
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_Poper"), //"fase"
						minScreenWidth: "40em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_CDL"), //"cdl"
						minScreenWidth: "20em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_Steus"), //"cdl"
						minScreenWidth: "20em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: oLng_MasterData.getText("MasterData_Vendor"), //"Note"
						minScreenWidth: "20em"
					})
				})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue !== "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("POPER", sap.ui.model.FilterOperator.Contains, sValue),
						 //new sap.ui.model.Filter("TYPEDESC", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else {
					oBinding.filter([]);
				}
			},
			confirm: cbFunction,
			cancel: function (oControlEvent) {
				$.UIbyID("dlgOrderPopersTableSelect").destroy();
			}
		}).data("FilterOrder", oParams.TypeOrdFilter);

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOrderModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgOrderPopersTableSelect").setContentWidth("800px");

	if ($.UIbyID("dlgOrderPopersTableSelect").data("FilterOrder") !== oParams.TypeOrdFilter) {
		$.UIbyID("dlgOrderPopersTableSelect").getModel().loadData(QService + dataProdMD + "Cycles/getOrdersOpersListSQ&Param.1=" + oParams.Plant + "&Param.2=" + oParams.Order);
	}

	if (oParams.Selected !== "" && oParams.Selected !== null) {
		$.UIbyID("dlgOrderPopersTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("POPER", sap.ui.model.FilterOperator.EQ, oParams.Selected),
			sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOrderPopersTableSelect").open(oParams.Selected);
	} else {
		$.UIbyID("dlgOrderPopersTableSelect").open();
	}

	$.UIbyID("dlgOrderPopersTableSelect").focus();

}

function fnGetSoulTableDialog(oParams, cbFunction) {

	//oParams.Selected
	//oParams.Order
	//oParams.Poper
	//oParams.Plant

	if (!$.UIbyID("dlgOrderComponentTableSelect")) {

		var oOrderModel = new sap.ui.model.xml.XMLModel();
		oOrderModel.loadData(QService + dataProdMD + "Cycles/getCyclesCompSoulSQ&Param.1=" + oParams.Plant +
			"&Param.2=" + oParams.Order + "&Param.3=" + oParams.Poper);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{ITEM}",
					//title: "{CICTXT}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{COMPONENT}",
					title: "{DESC}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{UMQTY}",
					//title: "{STEUS_DESC}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{MATL_TYPE}",
					//title: "{FORNNAME}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgOrderComponentTableSelect", {
			title: "Selezione componente Anima", //oLng_MasterData.getText("MasterData_POpers"), //"Lista Ordini"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns: [
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Pos.", //"ITEM"
						minScreenWidth: "40em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Componente", //"cdl"
						minScreenWidth: "20em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "UM", //"cdl"
						minScreenWidth: "5em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Tipo Mat.", //"Note"
						minScreenWidth: "10em"
					})
				})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue !== "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("COMPONENT", sap.ui.model.FilterOperator.Contains, sValue),
						 //new sap.ui.model.Filter("TYPEDESC", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else {
					oBinding.filter([]);
				}
			},
			confirm: cbFunction,
			cancel: function (oControlEvent) {
				$.UIbyID("dlgOrderComponentTableSelect").destroy();
			}
		}).data("FilterOrder", oParams.Order);

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOrderModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgOrderComponentTableSelect").setContentWidth("800px");

	if ($.UIbyID("dlgOrderComponentTableSelect").data("FilterOrder") !== oParams.Order) {
		$.UIbyID("dlgOrderComponentTableSelect").getModel().loadData(QService + dataProdMD + "Cycles/getCyclesCompSoulSQ&Param.1=" + oParams.Plant +
			"&Param.2=" + oParams.Order + "&Param.3=" + oParams.Poper);
	}

	if (oParams.Selected !== "" && oParams.Selected !== null) {
		$.UIbyID("dlgOrderComponentTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("COMPONENT", sap.ui.model.FilterOperator.EQ, oParams.Selected),
			sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOrderComponentTableSelect").open(oParams.Selected);
	} else {
		$.UIbyID("dlgOrderComponentTableSelect").open();
	}

	$.UIbyID("dlgOrderComponentTableSelect").focus();

} //fnGetSoulTableDialog


function fnGetCDLTableDialog(oParams, cbFunction) {

	//oParams.Selected
	//oParams.OrdType
	//oParams.Plant
	//oParams.IDx

	if (!$.UIbyID("dlgCDLTableSelect")) {

		var oOrderModel = new sap.ui.model.xml.XMLModel();
		oOrderModel.loadData(QService + dataProdMD + "Cycles/getCdlListbyOrdTypeSQ&Param.1=" + oParams.Plant +
			"&Param.2=" + oParams.OrdType);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{CDLID}",
					//title: "{CICTXT}"
				}),
				new sap.m.ObjectIdentifier({
					text: "{CDLDESC}",
					title: "{DESC}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgCDLTableSelect", {
			title: "Seleziona Centro di lavoro", //oLng_MasterData.getText("MasterData_POpers"), //"Lista Ordini"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "400px",
			contentHeight: "300px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
			items: [],
			columns: [
				new sap.m.Column({
					header: new sap.m.Label({
						text: "CDL", //"ITEM"
						minScreenWidth: "10em"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Label({
						text: "Descrizione", //"cdl"
						minScreenWidth: "50em"
					})
				})
				],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue !== "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("CDLID", sap.ui.model.FilterOperator.Contains, sValue),
						 //new sap.ui.model.Filter("TYPEDESC", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else {
					oBinding.filter([]);
				}
			},
			confirm: cbFunction,
			cancel: function (oControlEvent) {
				$.UIbyID("dlgCDLTableSelect").destroy();
			}
		}).data("FilterOrder", oParams.OrdType);

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOrderModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("500px");
	}

	$.UIbyID("dlgCDLTableSelect").setContentWidth("500px");

	if ($.UIbyID("dlgCDLTableSelect").data("FilterOrder") !== oParams.OrdType) {
		$.UIbyID("dlgCDLTableSelect").getModel().loadData(QService + dataProdMD + "Cycles/getCdlListbyOrdTypeSQ&Param.1=" + oParams.Plant +
			"&Param.2=" + oParams.OrdType);
	}

	if (oParams.Selected !== "" && oParams.Selected !== null) {
		$.UIbyID("dlgCDLTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("CDLID", sap.ui.model.FilterOperator.EQ, oParams.Selected),
			sap.ui.model.FilterType.Application);
		$.UIbyID("dlgCDLTableSelect").open(oParams.Selected);
	} else {
		$.UIbyID("dlgCDLTableSelect").open();
	}

	$.UIbyID("dlgCDLTableSelect").focus();

} //fnGetSoulTableDialog
