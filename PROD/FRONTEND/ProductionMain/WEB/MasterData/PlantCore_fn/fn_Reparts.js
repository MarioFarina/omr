/***********************************************************************************************/
// Funzioni ed oggetti per Reparti
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella reparti e ritorna l'oggetto oTable
function createTabRep() {

    //Crea L'oggetto Tabella Macchine
    var oTable = UI5Utils.init_UI5_Table({
        id: "RepTab",
        properties: {
            title: oLng_MasterData.getText("MasterData_DepartsList"), //"Lista Reparti",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Paginator,
            rowSelectionChange: function (oControlEvent) {
                if ($.UIbyID("RepTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModRep").setEnabled(false);
                    $.UIbyID("btnDelRep").setEnabled(false);
                } else {
                    $.UIbyID("btnModRep").setEnabled(true);
                    $.UIbyID("btnDelRep").setEnabled(true);
                }
            }
        },
        toolbarItems: [
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Add"), //"Aggiungi",
                icon: 'sap-icon://add',
                press: function () {
                    openRepEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Modify"), //"Modifica",
                id: "btnModRep",
                icon: 'sap-icon://edit',
                enabled: false,
                press: function () {
                    openRepEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Delete"), //"Elimina",
                id: "btnDelRep",
                icon: 'sap-icon://delete',
                enabled: false,
                press: function () {
                    fnDelRep();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_MasterData.getText("MasterData_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',
                enabled: true,
                press: function () {
                    refreshTabRep();
                }
            })
        ],
        columns: [
            {
				Field: "IDREP",
				label: oLng_MasterData.getText("MasterData_DepartID"), //"ID Reparto",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "REPTXT",
				label: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "NAME1",
				label: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "EXIDREP",
				label: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
				properties: {
					width: "100px",
					flexible : false
				}
			},
            {
				Field: "SLOWTIME",
				label: oLng_MasterData.getText("MasterData_Slowtime"), //"SLOWTIME (soglia rallentamento/fermo) in minuti",
				properties: {
					width: "285px",
					flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
			},
            {
				Field: "STOPTRIM",
				label: oLng_MasterData.getText("MasterData_Stoptrim"), //"STOPTRIM (tolleranza) in secondi",
				properties: {
					width: "200px",
					flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
			},
            {
				Field: "NOTES",
				label: oLng_MasterData.getText("MasterData_Notes"), //"Note",
				properties: {
					width: "200px",
					flexible : false
				}
			}
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabRep()
    return oTable;
}

// Aggiorna la tabella reparti
function refreshTabRep() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdMD +
            "Departments/getDepartsByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("RepTab").getModel().loadData(
            QService + dataProdMD +
            "Departments/getDepartsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("RepTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo reparti
function openRepEdit(bEdit) {

    if (bEdit == undefined) bEdit = false;

    //  Crea la finestra di dialogo
    var oEdtDlg = new sap.ui.commons.Dialog({
        title: oLng_MasterData.getText("MasterData_DepartsRegistry"), //"Anagrafica reparto",
        modal: true,
        resizable: true,
        id: "dlgAddRep",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });

    //oEdtDlg.setTitle("Aggiungi Reparto");

    // contenitore dati lista divisioni
    var oModel = new sap.ui.model.xml.XMLModel()
	oModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

	// Crea la ComboBox per le divisioni
	var oCmbPlant = new sap.ui.commons.ComboBox({
		id : "cmbPlantRep",
		selectedKey : bEdit?fnGetCellValue("RepTab", "PLANT"):$.UIbyID("filtPlant").getSelectedKey(),
		enabled: false
	});
	oCmbPlant.setModel(oModel);
	var oItemPlant = new sap.ui.core.ListItem();
	oItemPlant.bindProperty("key", "PLANT");
	oItemPlant.bindProperty("text", "NAME1");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);


    var oLayout1 = new sap.ui.layout.form.GridLayout({
        singleColumn: true
    });
    var oForm1 = new sap.ui.layout.form.Form({
        //title: new sap.ui.core.Title({text: "Anagrafica reparto", icon: 'sap-icon://edit', tooltip: "Info Reparto"}),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: oCmbPlant
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_DepartID"), //"ID Reparto",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                value: bEdit ? fnGetCellValue("RepTab", "IDREP") : "",
                                editable: bEdit ? false : true,
                                maxLength: 5,
                                id: "txtRepId",
																change: function() {
																	UI5Utils.validationTextField(this, /^[A-Za-z0-9+]+$/, "saveRepButton");
																}
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Description"), //"Descrizione",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("RepTab", "REPTXT") : "",
                                maxLength: 25,
                                id: "txtRepTxt"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_ExternarlID"), //"ID Esterno",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("RepTab", "EXIDREP") : "",
                                maxLength: 15,
                                id: "txtEXID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_SlowtimeShort"),
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("RepTab", "SLOWTIME") : "5",
                                maxLength: 3,
                                id: "txtSLOWTIME",
																tooltip: oLng_MasterData.getText("MasterData_SlowtimeTooltip"),
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_StoptrimShort"), //"STOPTRIM",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("RepTab", "STOPTRIM") : "120",
                                maxLength: 3,
                                id: "txtSTOPTRIM",
																tooltip: oLng_MasterData.getText("MasterData_StoptrimTooltip"),
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({
                                hCells: "2"
                            })
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit ? fnGetCellValue("RepTab", "NOTES") : "",
                                maxLength: 255,
                                id: "txtNote"
                            })
                        ]
                    })
					]
            })
			]
    });

    oEdtDlg.addContent(oForm1);
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
				id: "saveRepButton",
        press: function () {
            if (bEdit) fnSaveRep();
            else fnAddRep()
        }
    }));
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
        press: function () {
            oEdtDlg.close();
            oEdtDlg.destroy()
        }
    }));
    oEdtDlg.open();
}

// Funzione per salvare l'inserimento di un reparto
function fnAddRep(oEdtDlg) {
    var sRepId = $.UIbyID("txtRepId").getValue();
    if ( sRepId === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    var sRepTxt = $.UIbyID("txtRepTxt").getValue();
    var iSlowtime = ($.UIbyID("txtSLOWTIME").getValue() === ""?"5":$.UIbyID("txtSLOWTIME").getValue());
    iSlowtime = parseInt(iSlowtime);
    var iStopTrim = ($.UIbyID("txtSTOPTRIM").getValue() === ""?"120":$.UIbyID("txtSTOPTRIM").getValue());
    iStopTrim = parseInt(iStopTrim);


    var qexe = dataProdMD + "Departments/addDepartmentsSQ&Param.1=" + sRepId + "&Param.2=" + sRepTxt;
    qexe += "&Param.3=" + $.UIbyID("txtEXID").getValue();
    qexe += "&Param.4=" + $.UIbyID("cmbPlantRep").getSelectedKey();
    qexe += "&Param.5=" + $.UIbyID("txtNote").getValue();
    qexe += "&Param.6=" + iSlowtime;
    qexe += "&Param.7=" + iStopTrim;

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_DepartAdded"), //"Reparto aggiunto correttamente",
        oLng_MasterData.getText("MasterData_DepartAddedError") //"Errore in aggiunta reparto", false
    );
    if (ret) {
        $.UIbyID("dlgAddRep").close();
        $.UIbyID("dlgAddRep").destroy();
        refreshTabRep();
    }
}

// Funzione per salvare le modifiche al reparto
function fnSaveRep(oEdtDlg) {
    var sRepId = $.UIbyID("txtRepId").getValue();
    if ( sRepId === "") {
        sap.ui.commons.MessageBox.show(oLng_MasterData.getText("MasterData_RequiredFieldsErr"),
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_MasterData.getText("MasterData_InputError")
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK);
        return;
    }
    var sRepTxt = $.UIbyID("txtRepTxt").getValue();
    var iSlowtime = ($.UIbyID("txtSLOWTIME").getValue() === ""?"5":$.UIbyID("txtSLOWTIME").getValue());
    iSlowtime = parseInt(iSlowtime);
    var iStopTrim = ($.UIbyID("txtSTOPTRIM").getValue() === ""?"120":$.UIbyID("txtSTOPTRIM").getValue());
    iStopTrim = parseInt(iStopTrim);

    var qexe = dataProdMD + "Departments/saveDepartmentsSQ&Param.1=" + sRepId
    qexe += "&Param.2=" + sRepTxt;
    qexe += "&Param.3=" + $.UIbyID("txtEXID").getValue();
    qexe += "&Param.4=" + $.UIbyID("cmbPlantRep").getSelectedKey();
    qexe += "&Param.5=" + $.UIbyID("txtNote").getValue();
    qexe += "&Param.6=" + iSlowtime;
    qexe += "&Param.7=" + iStopTrim;

    var ret = fnSQLQuery(
        qexe,
        oLng_MasterData.getText("MasterData_DepartSaved"), //"Reparto salvato correttamente",
        oLng_MasterData.getText("MasterData_DepartSavedError") //"Errore in salvataggio reparto", false
    );
    if (ret) {
        $.UIbyID("dlgAddRep").close();
        $.UIbyID("dlgAddRep").destroy();
        refreshTabRep();
    }
}

// Funzione per cancellare un reparto
function fnDelRep() {
    var sRepId = fnGetCellValue("RepTab", "IDREP");
    var sRepTxt = fnGetCellValue("RepTab", "REPTXT");

    sap.ui.commons.MessageBox.show(
        oLng_MasterData.getText("MasterData_DepartDeletingWithDescription") //"Eliminare il reparto selezionato con descrizione"
        + ":\n'" + sRepTxt + "'?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function (sResult) {
            if (sResult == 'YES') {
                var qexe = dataProdMD + "Departments/delDepartmentsSQ&Param.1=" + sRepId;
                var ret = fnSQLQuery(
                    qexe,
                    oLng_MasterData.getText("MasterData_DepartDeleted"), //"Reparto eliminato correttamente",
                    oLng_MasterData.getText("MasterData_DepartDeletedError"), //"Errore in eliminazione reparto",
                    false
                );
                refreshTabRep();
                $.UIbyID("btnDelRep").setEnabled(false);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}
