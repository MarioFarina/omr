//**************************************************************************************
//Title:  Funzioni per il recupero degli operatori
//Author: Bruno Rosati
//Date:   13/07/2017
//Vers:   1.0
//**************************************************************************************
// Script per il recupero degli operatori

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

function fnGetOperatorTableDialog(sPlant, sSelected, cbFunction) {
	if (!$.UIbyID("dlgOperatorTableSelect")) {

		var oOpeModel = new sap.ui.model.xml.XMLModel();
		oOpeModel.loadData(QService + dataProdMD + "Operators/GetAllCIDbyPlantSQ&Param.1=" + sPlant);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.ObjectIdentifier({
					text: "{OPENAME}",
					title: "{PERNR}"
				}),
				new sap.m.Text({
					text: "{GROUP_ID}",
                    title: "{}"
				}),
				new sap.m.Text({
					text: "{OPDESC}",
                    title: "{}"
				}),
				new sap.m.Text({
					text: "{NOTES}",
                    title: "{}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgOperatorTableSelect", {
			title: oLng_MasterData.getText("MasterData_OperatorsList"), //"Lista operatori"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: false,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
            items: [],
			columns:[
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_OperatorName"), //"Nome Operatore" 
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Group"), //"Gruppo"
                    minScreenWidth: "0.5em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Type"), //"Tipo"
                    minScreenWidth: "Tablet"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Notes"), //"Note"
                    minScreenWidth: "40em"
                })})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("BADGE", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("OPENAME", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("OPDESC", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("GROUP_ID", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
            cancel: function (oControlEvent){
                $.UIbyID("dlgOperatorTableSelect").destroy();
            }
		});

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOpeModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgOperatorTableSelect").setContentWidth("800px");


	if (sSelected == "" || sSelected == null) 
        $.UIbyID("dlgOperatorTableSelect").open();
	else {
		$.UIbyID("dlgOperatorTableSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SOURCE", sap.ui.model.FilterOperator.EQ, sSelected),
				 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOperatorTableSelect").open(sSelected);
	}

	$.UIbyID("dlgOperatorTableSelect").focus();
    
}

function fnGetOperatorMultipleSelectTableDialog(sPlant, sSelected, cbFunction) {
	if (!$.UIbyID("dlgOperatorTableMultipleSelect")) {

		var oOpeModel = new sap.ui.model.xml.XMLModel();
		oOpeModel.loadData(QService + dataProdMD + "Operators/GetAllCIDbyPlantSQ&Param.1=" + sPlant);

		var itemSources = new sap.m.ColumnListItem({
			type: "Inactive",
			press: function (oControlEvent) {
				alert(oControlEvent.getParameters);
			},
			cells: [
				new sap.m.Text({
					text: "",
                    title: ""
				}),
                new sap.m.ObjectIdentifier({
					text: "{OPENAME}",
					title: "{BADGE}"
				}),
				new sap.m.Text({
					text: "{GROUP_ID}",
                    title: "{}"
				}),
				new sap.m.Text({
					text: "{OPDESC}",
                    title: "{}"
				}),
				new sap.m.Text({
					text: "{NOTES}",
                    title: "{}"
				})
			]
		});

		var odlgSourcesSelect = new sap.m.TableSelectDialog("dlgOperatorTableMultipleSelect", {
			title: oLng_MasterData.getText("MasterData_OperatorsList"), //"Lista operatori"
			noDataText: oLng_MasterData.getText("MasterData_NoInsert"), //"Nessun inserimento"
			contentWidth: "800px",
			contentHeight: "600px",
			multiSelect: true,
			strech: false,
			type: "Dialog",
			draggable: true,
			resizable: true,
            items: [],
			columns:[
				new sap.m.Column({header: new sap.m.Label({
                    text: "", //""
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_OperatorName"), //"Nome Operatore" 
                    minScreenWidth: "40em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Group"), //"Gruppo"
                    minScreenWidth: "0.5em"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Type"), //"Tipo"
                    minScreenWidth: "Tablet"
                })}),
				new sap.m.Column({header: new sap.m.Label({
                    text: oLng_MasterData.getText("MasterData_Notes"), //"Note"
                    minScreenWidth: "40em"
                })})],
			search: function (oControlEvent) {
				var sValue = oControlEvent.getParameter("value");
				console.log("Search Value  ->" + sValue);
				var oBinding = oControlEvent.getSource().getBinding("items");
				if (sValue != "") {
					var oFilter = [new sap.ui.model.Filter(
						[new sap.ui.model.Filter("BADGE", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("OPENAME", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("OPDESC", sap.ui.model.FilterOperator.Contains, sValue),
						 new sap.ui.model.Filter("GROUP_ID", sap.ui.model.FilterOperator.Contains, sValue)
						],
						false)];
					oBinding.filter(oFilter, sap.ui.model.FilterType.Application);
				} else oBinding.filter([]);
			},
			confirm: cbFunction,
            cancel: function (oControlEvent){
                $.UIbyID("dlgOperatorTableMultipleSelect").destroy();
            }
		});

		odlgSourcesSelect.bindAggregation("items", {
			path: "/Rowset/Row",
			template: itemSources
		});

		odlgSourcesSelect.setModel(oOpeModel);
		//odlgSourcesSelect.bindObject("/Rowset/Row", itemSources);

		odlgSourcesSelect.setContentWidth("300px");
	}

	$.UIbyID("dlgOperatorTableMultipleSelect").setContentWidth("800px");


	if (sSelected == "" || sSelected == null) 
        $.UIbyID("dlgOperatorTableMultipleSelect").open();
	else {
		$.UIbyID("dlgOperatorTableMultipleSelect").getBinding("items").
		filter(new sap.ui.model.Filter("SOURCE", sap.ui.model.FilterOperator.EQ, sSelected),
				 sap.ui.model.FilterType.Application);
		$.UIbyID("dlgOperatorTableMultipleSelect").open(sSelected);
	}

	$.UIbyID("dlgOperatorTableMultipleSelect").focus();
    
}