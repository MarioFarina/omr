// Controller Dettaglio sospesi - Ripara
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.SUSPDET.suspDetRep", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("suspDetRep").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		var oView = sap.ui.getCore().byId('suspDetRepView');
		var oController = oView.getController();
		
		var oPatternArguments = oEvent.getParameter("arguments");
		var sPlant, sPernr;
		if(oPatternArguments && oPatternArguments.hasOwnProperty('?query') && 
			oPatternArguments['?query'] !== undefined)
		{
			console.log(JSON.stringify(oPatternArguments));
			var oArguments = oPatternArguments['?query'];
			oController._oArguments = oArguments;			
			if(oArguments.hasOwnProperty('plant'))
				sPlant = oArguments.plant;
			if(oArguments.hasOwnProperty('pernr'))
				sPernr = oArguments.pernr;
		}

		oAppController.loadSubHeaderInfo($.UIbyID('suspDetRepPage'),sPlant,sPernr);
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('suspDetRepView').getController();
		oAppRouter.navTo('suspDet',{
			query: oController._oArguments
		});
	},

});