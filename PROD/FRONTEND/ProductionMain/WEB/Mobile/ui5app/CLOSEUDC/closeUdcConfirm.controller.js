// Controller Conferma chiusura UdC / Quantità Scarti
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

var iCountClicked = 0;

sap.ui.controller("ui5app.CLOSEUDC.closeUdcConfirm", {
	
	_oArguments: undefined,
	_sViewType: false, // 'C' Chiusura , 'S' Scarti, 'P' Ristampa, 'SG' Dich. di scarto da buoni, 'D' Info dich. fine turno, 'R' Lettura UDC
	_tConfVer: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("closeUdcConfirm").attachPatternMatched(this.onCloseMatched, this);
		oAppRouter.getRoute("scrapUdcInput").attachPatternMatched(this.onScrapMatched, this);
		oAppRouter.getRoute("reprintUdcConfirm").attachPatternMatched(this.onReprintMatched, this);
		oAppRouter.getRoute("scrapGoodsInput").attachPatternMatched(this.onScrapGoodsMatched, this);
		oAppRouter.getRoute("shiftDeclInfo").attachPatternMatched(this.onShiftDeclMatched, this);
		oAppRouter.getRoute("readUdcInput").attachPatternMatched(this.onReadMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/
    
	onCloseMatched: function (oEvent) {
		this._sViewType = 'C';
		
		this.onObjectMatched(oEvent);
	},
	
	onScrapMatched: function (oEvent) {
		this._sViewType = 'S';
		
		this.onObjectMatched(oEvent);
	},
	
	onReprintMatched: function (oEvent) {
		this._sViewType = 'P';
		
		this.onObjectMatched(oEvent);
	},
	
	onScrapGoodsMatched: function (oEvent) {
		this._sViewType = 'SG';
		
		this.onObjectMatched(oEvent);
	},
	
	onShiftDeclMatched: function (oEvent) {
		this._sViewType = 'D';
		
		this.onObjectMatched(oEvent);
	},
	
	onReadMatched: function (oEvent) {
		this._sViewType = 'R';
		
		this.onObjectMatched(oEvent);
	},
	
	onObjectMatched: function (oEvent) {

		const sViewType = this._sViewType;
		var oArgParams = ['plant','pernr'];
		if(sViewType === 'SG')
			oArgParams.concat(['order','poper','idline']);
		else
			oArgParams.concat(['udcnr']);
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams); 
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('closeUdcConfirmPage'),sPlant,sPernr);
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		
		var oView = this.getView();
		var oModel = oView.getModel();
		
		var bDisabledMode = false;
		if(!oUdcObject || oUdcObject.VIEWTYPE !== sViewType) // !oUdcObject.hasOwnProperty('UDCNR') || oUdcObject.UDCNR !== oArguments.udcnr
		{
			oUdcObject = {
				DISABLED_MODE: true
			};
            bDisabledMode = true;
		}
        
		$.UIbyID('scrapUdcConfirmButton').setEnabled(!bDisabledMode);
		
		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);

		this._tConfVer = oUdcObject.TCONFVER;
		if(this._tConfVer == 'V2')
			$.UIbyID('reprintUdcConfirmCancelDecl').setVisible(false);
		else 
	        $.UIbyID('reprintUdcConfirmCancelDecl').setEnabled(!bDisabledMode);
		
		if(sViewType === 'S' || sViewType === 'SG') // Dichiarazioni di scarto
		{
			var oMacroReasCombo = $.UIbyID('scrapUdcMacroCombo');
			var oMicroReasCombo = $.UIbyID('scrapUdcMicroCombo');
			
			oMacroReasCombo.clearSelection();
			oMacroReasCombo.setValue();
			oMicroReasCombo.clearSelection();
			oMicroReasCombo.setValue();
			
			var oMacroModel = oMacroReasCombo.getModel('macroReas');
			if(!oMacroModel)
			{
				oMacroModel = new sap.ui.model.xml.XMLModel();
				oMacroReasCombo.setModel(oMacroModel,'macroReas');
			}
			
			var sMacroQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsGroupsByTypeSQ";
			sMacroQuery += "&Param.1=" + sPlant;
			sMacroQuery += "&Param.2=SC";
			oMacroModel.loadData(sMacroQuery,false,false);
			
			var oMicroModel = oMicroReasCombo.getModel('microReas');
			if(oMicroModel)
				oMicroModel.destroy();
			oMicroModel = new sap.ui.model.xml.XMLModel();
			oMicroReasCombo.setModel(oMicroModel,'microReas');
		}
		
		// Colore bottone principale
		var oConfirmButton = $.UIbyID('scrapUdcConfirmButton');
		oConfirmButton.removeStyleClass('redButton');
		//oConfirmButton.removeStyleClass('yellowButton');
		oConfirmButton.removeStyleClass('greenButton');
		oConfirmButton.removeStyleClass('blueButton');
		var sStyleClass;
		if(sViewType === 'C')
			sStyleClass = 'greenButton';
		else if(sViewType === 'S' || sViewType === 'SG')
			sStyleClass = 'redButton';
		else if(sViewType === 'P' || sViewType === 'R')
			sStyleClass = 'blueButton';
		oConfirmButton.addStyleClass(sStyleClass);
		
		var oScrapQtyInput = $.UIbyID('scrapUdcQty');
		oScrapQtyInput.setValue();
		oScrapQtyInput.setValueState();
		oScrapQtyInput.setValueStateText();
	},
	
	onMacroReasSelectionChange: function () {
		var sMacroReas  = this.getSelectedKey();
		if(sMacroReas)
		{
			var oController = sap.ui.getCore().byId('closeUdcConfirmView').getController();
			const sPlant = oController._oArguments.plant;
			
			var oMicroReasCombo = $.UIbyID('scrapUdcMicroCombo');
			var oMicroModel = oMicroReasCombo.getModel('microReas');
			
			oMicroReasCombo.clearSelection();
			oMicroReasCombo.setValue();
			var sScrapQuery = QService + sMasterDataPath + "ScrabReasons/getScrabReasonsByGroupSQ"
			sScrapQuery += "&Param.1=" + sPlant;
			sScrapQuery += "&Param.2=" + sMacroReas;
			oMicroModel.loadData(sScrapQuery,false,false);
		}
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
        
        $.UIbyID('scrapUdcConfirmButton').setEnabled(false);
        $.UIbyID('reprintUdcConfirmCancelDecl').setEnabled(false);
        
        var oController = sap.ui.getCore().byId('closeUdcConfirmView').getController();
		var oArguments = oController._oArguments;
        
        iCountClicked = 0;
		
		if(oArguments.hasOwnProperty('udcnr'))
			delete oArguments.udcnr;
		
		var sNavName;
		switch(oController._sViewType)
		{
			case 'C':
				sNavName = 'closeUdc';
				break;
			case 'S':
				sNavName = 'scrapUdc';
				break;
			case 'P':
				sNavName = 'reprintUdc';
				break;
			case 'SG':
				sNavName = 'scrapGoods';
				break;
			case 'D':
				sNavName = 'shiftDecl';
				break;
			case 'R':
				sNavName = 'readUdc';
				break;
		}
		
		oAppRouter.navTo(sNavName,{
			query: oArguments
		});
	},
	
	onConfirm: function() {
        
        $.UIbyID('scrapUdcConfirmButton').setEnabled(false);
        $.UIbyID('reprintUdcConfirmCancelDecl').setEnabled(false);
		
		var oView = sap.ui.getCore().byId('closeUdcConfirmView');
		var oController = oView.getController();
		var oModel = oView.getModel();
		const oArguments = oController._oArguments;
		const sViewType = oController._sViewType;
        
        iCountClicked = iCountClicked + 1;
        console.log(iCountClicked);
        
        if(iCountClicked <= 1){
            if(sViewType === 'S' || sViewType === 'SG') // Scarto
            {			
                var oMicroReasCombo = $.UIbyID('scrapUdcMicroCombo');
                var oScrapQtyInput = $.UIbyID('scrapUdcQty');

                oMicroReasCombo.setValueState();
                oMicroReasCombo.setValueStateText();
                oScrapQtyInput.setValueState();
                oScrapQtyInput.setValueStateText();

                const sScrapReas = oMicroReasCombo.getSelectedKey();
                const sScrapQty = oScrapQtyInput.getValue();
                const sQtyRes = parseInt(oModel.getProperty("/QTYRES"));

                var sErrorMessage;	
                if(!sScrapReas)
                {
                    sErrorMessage = oLng_Opr.getText("ScrapUdcInput_ScrapErrorMessage");
                    oMicroReasCombo.setValueState(sap.ui.core.ValueState.Warning);
                    oMicroReasCombo.setValueStateText(sErrorMessage);
                }

                if(!sErrorMessage) // controllo quantità
                {
                    if(isNaN(sScrapQty) || sScrapQty <= 0)
                        sErrorMessage = oLng_Opr.getText("ScrapUdcInput_QtyErrorMessage");
                    else if(sScrapQty > sQtyRes)
                        sErrorMessage = oLng_Opr.getText("ScrapUdcInput_QtyResErrorMessage");

                    if(sErrorMessage)
                    {
                        oScrapQtyInput.setValueState(sap.ui.core.ValueState.Warning);
                        oScrapQtyInput.setValueStateText(sErrorMessage);
                    }
                }

                if(sErrorMessage)
                {				
                    sap.m.MessageBox.show(sErrorMessage, {
                        icon: sap.m.MessageBox.Icon.WARNING,
                        title: oLng_Opr.getText("General_WarningText"),
                        actions: sap.m.MessageBox.Action.OK
                    });
                    iCountClicked = 0;
					$.UIbyID('scrapUdcConfirmButton').setEnabled(true);
					if(oController._tConfVer == 'V2')
						$.UIbyID('reprintUdcConfirmCancelDecl').setVisible(false);
					else
                    	$.UIbyID('reprintUdcConfirmCancelDecl').setEnabled(true);
                }
                else
                {
                    var sScrapQuery;
                    if(sViewType === 'S' || sViewType === 'SG')
                    {
                        var sLineId;
                        var sUdcNr = '';
                        if(sViewType === 'S')
                        {
                            sLineId = oModel.getProperty("/IDLINE");
                            sUdcNr = oArguments.udcnr;
                        }
                        else
                            sLineId = oModel.getProperty("/LINEID");

                        sScrapQuery = sMovementPath + "Rework/saveReworkAndPrintQR";
                        sScrapQuery += "&Param.1=" + oArguments.plant;
                        sScrapQuery += "&Param.2=" + oAppController._sShift;
                        sScrapQuery += "&Param.3=" + oAppController._sDate;
                        sScrapQuery += "&Param.4=" + oArguments.pernr;
                        sScrapQuery += "&Param.5=" + sLineId;
                        sScrapQuery += "&Param.6=" + sUdcNr;		
                        sScrapQuery += "&Param.7=";
                        sScrapQuery += "&Param.8=" + oModel.getProperty("/ORDER");
                        sScrapQuery += "&Param.9=" + oModel.getProperty("/POPER");
                        sScrapQuery += "&Param.10=" + "D";
                        sScrapQuery += "&Param.11=";
                        sScrapQuery += "&Param.12=";
                        sScrapQuery += "&Param.13=" + oModel.getProperty("/MATERIAL");
                        sScrapQuery += "&Param.14=" + sScrapQty;
                        sScrapQuery += "&Param.15=";
                        sScrapQuery += "&Param.16=";
                        sScrapQuery += "&Param.17=";
                        sScrapQuery += "&Param.18=" + sScrapReas;
                        sScrapQuery += "&Param.19=";
                        sScrapQuery += "&Param.20=";
                        sScrapQuery += "&Param.21=" + getDatetimeString(new Date());
                        sScrapQuery += "&Param.22=";
                        sScrapQuery += "&Param.23=";
                        sScrapQuery += "&Param.24=S";
                        sScrapQuery += "&Param.26=" + sLanguage;
                    }

                    var successFunction = function(oResults) {
                        oController.onNavBack();
                    };

                    oAppController.handleRequest(sScrapQuery,{
                        onSuccess: successFunction
                    });
                }
            }
            else if(sViewType === 'C') // Chiusura
            {
                const sLineId = oModel.getProperty("/IDLINE");

                var sWsName = '';
                var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
                var oWorkstInfo = oStorage.get('WORKST_INFO');
                if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
                    sWsName = oWorkstInfo.WSNAME;

                var sCloseQuery = sMovementPath + "UDC/closeUDCandPrintQR";
                sCloseQuery += "&Param.1=" + oArguments.udcnr;
                sCloseQuery += "&Param.2=" + sLineId;
                sCloseQuery += "&Param.3=" + sWsName;
                sCloseQuery += "&Param.4=" + sLanguage;
                

                var successFunction = function(oResults) {

                    if(parseInt(oResults.code) === 1 && oResults.hasOwnProperty('pdfString') && oResults.pdfString)
                    {
                        var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
                        oBoxDeclInputController.showPdfDialog(oResults.pdfString,true,oController.onNavBack);
                    }
                    else
                        oController.onNavBack();
                };

                oAppController.handleRequest(sCloseQuery,{
                    onSuccess: successFunction,
                    onWarning: successFunction,
                    notShowWarning: [1],
                    results: ['pdfString']
                });
            }
            else if(sViewType === 'P' || sViewType === 'R') // Ristampa
            {
                const sOrigin = oModel.getProperty("/ORIGIN");
                const sUdcNr = oModel.getProperty("/UDCNR");
                const sLineId = oModel.getProperty("/IDLINE");

                var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
                var oWorkstInfo = oStorage.get('WORKST_INFO');
                var sWsName;
                if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
                    sWsName = oWorkstInfo.WSNAME;

                var sPrintQuery;
                sPrintQuery = sMovementPath + "Print/print_ETI_PROD_absolute_QR";
                sPrintQuery += "&Param.1=" + sUdcNr;
                sPrintQuery += "&Param.2=" + sLineId;				
                sPrintQuery += "&Param.3=" + sWsName;
                sPrintQuery += "&Param.4=true";
                sPrintQuery += "&Param.5=" + sLanguage;

                /*
                if(sOrigin === 'M')
                {
                    const sUdcStat = oModel.getProperty("/UDCSTAT");

                    sPrintQuery = sMovementPath + "Print/print_ETI_PROD_QR";
                    sPrintQuery += "&Param.1=" + sUdcNr;
                    sPrintQuery += "&Param.2=" + sLineId;
                    sPrintQuery += "&Param.3=" + sWsName;
                    sPrintQuery += "&Param.4=" + ((sUdcStat === 'C') ? '1' : '0');
                }
                else if(sOrigin === 'C')
                {  
                    sPrintQuery = sMovementPath + "Print/print_ETI_PROD_sosp_QR";
                    sPrintQuery += "&Param.1=" + sUdcNr;
                    sPrintQuery += "&Param.2=" + sLineId;
                    sPrintQuery += "&Param.3=" + sWsName;
                    sPrintQuery += "&Param.4=";
                }
                */
                oAppController.handleRequest(sPrintQuery,{
                    showSuccess: false,
                    onSuccess: oController.onSuccess,
                    onWarning: oController.onSuccess,
                    notShowWarning: [1],
                    results: ['pdfString']
                });
            }
        }
	},
	
	onSuccess: function(oResults) {
		
		const iCode = parseInt(oResults.code);
		if(oResults.hasOwnProperty('pdfString') && (iCode === 0 || iCode === 1))
		{
			var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
			oBoxDeclInputController.showPdfDialog(oResults.pdfString,true);
		}
	},
	
	onCancelDecl: function() {
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		if(!oUdcObject)
			return;
		
		const bDelReq = (parseInt(oUdcObject.DELREQ) === 1);
		
		sap.m.MessageBox.show((bDelReq) ? 
				oLng_Opr.getText("ReprintUdcConfirm_UndoCancQuestion") :
				oLng_Opr.getText("ReprintUdcConfirm_CancelDeclQuestion"), {
			icon: sap.m.MessageBox.Icon.WARNING,
			title: oLng_Opr.getText("General_WarningText"),
			actions: [sap.m.MessageBox.Action.YES,sap.m.MessageBox.Action.NO],
			onClose: function(oAction) {
				if (oAction === sap.m.MessageBox.Action.YES)
				{
					var oView = sap.ui.getCore().byId('closeUdcConfirmView');
					var oModel = oView.getModel();
					
					const sUdcNr = oModel.getProperty("/UDCNR");
					
					const sNewDelReq = ((bDelReq) ? "0" : "1");
					var sRequest = sMovementPath + "UDC/updateUDCDelReqSQ";
					sRequest += "&Param.1=" + sUdcNr;
					sRequest += "&Param.2=" + sNewDelReq;
					var bSuccess = fnExeQuerym(sRequest);
					if(bSuccess)
					{
						oUdcObject.DELREQ = sNewDelReq;
						oSessionStorage.put('FUNCDATA',oUdcObject);
						oModel.setData(oUdcObject);
						
						sap.m.MessageBox.show((bDelReq) ?
							oLng_Opr.getText("ReprintUdcConfirm_UndoCancSuccess") :
							oLng_Opr.getText("ReprintUdcConfirm_CancelDeclSuccess"), {
							icon: sap.m.MessageBox.Icon.SUCCESS,
							title: oLng_Opr.getText("General_SuccessText"),
							actions: sap.m.MessageBox.Action.OK
						});
						/*
						oAppController.showCustomToast({
							type: 'Success',
							message: oLng_Opr.getText("")
						});
						*/
					}
				}
			}
		});
	}

});