// Controller Login Operatore

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.login", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("login").attachPatternMatched(this.onObjectMatched, this);
		
		var nextFunc = this.onEnterPress;
		var oInput = sap.ui.getCore().byId("operatorInput");
		oInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},
	
/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put("FUNC_INFO",false);
		
		/*var sPlant;
		var oArguments = oEvent.getParameter("arguments");		
		if(oArguments && oArguments.hasOwnProperty('?query') && 
			oArguments['?query'] !== undefined && oArguments['?query'].hasOwnProperty('plant'))
		{
			console.log("Arguments: " + JSON.stringify(oArguments));
			sPlant = oArguments['?query'].plant;
			console.log("Plant da URL: " + sPlant);
		}*/
		var sSrcQuery = sMobilePath + "getEnvironmentSrcSQ";
		$.UIbyID('envImage').setSrc(fnGetAjaxVal(sSrcQuery,["SRC"],false).SRC);
		
		var oInput = $.UIbyID("operatorInput");
		oInput.setValue();
		
		jQuery.sap.delayedCall(1700, null, function() {
			oInput.focus();
		});
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/**
 * Visualizzazione opzioni del menu a tendina
 * @function onShowOptions
 * @memberOf ui5app.login
 * @param {event} oEvent
 */
	onShowOptions: function (oEvent) {
		const sOptionsSheetId = 'optionsSheet';
		var oOptionsSheet = $.UIbyID(sOptionsSheetId);
		if(!oOptionsSheet)
		{
			var oView = sap.ui.getCore().byId('loginView');
			var oController = oView.getController();
			
			var oOptionsSheet = new sap.m.ActionSheet({
				id: sOptionsSheetId,
				placement: sap.m.PlacementType.Bottom,
				buttons: [
					new sap.m.Button({
						text: oLng_Opr.getText("Login_Settings"),
						icon: "sap-icon://settings",
						press: oController.onOpenSettings
					}),
					new sap.m.Button({
						text: oLng_Opr.getText("Login_Config"),
						icon: "sap-icon://wrench",
						press: oController.onOpenConfig
					})
				]
			});
			
			oView.addDependent(oOptionsSheet);
		}
		
		var oButton = oEvent.getSource();
		oOptionsSheet.openBy(oButton);
	},
	
/***********************************************************************************************/
// Creazione dialog impostazioni
/***********************************************************************************************/

	getSettingsDialog: function ( fnCallback ) {
		const sDialogId = 'settingsDialog';
		var oSettingsDialog = $.UIbyID(sDialogId);
		if(!oSettingsDialog) {
			
			var oLineMultiCombo = new sap.m.MultiComboBox({
				maxWidth: "376px"
			});
			oLineMultiCombo.setModel(new sap.ui.model.xml.XMLModel());
			
			var oLineComboTemplate = new sap.ui.core.Item({
				key : "{IDLINE}"
			}).bindProperty("text", {
				parts: [
					{path: 'IDLINE'},
					{path: 'LINETXT'}
				],		     
				formatter: function(sIdLine,sLineTxt){
					return sIdLine + ' ' + sLineTxt;
				}
			});
			oLineMultiCombo.bindAggregation("items","/Rowset/Row",oLineComboTemplate);

			var oPlantsModel = new sap.ui.model.xml.XMLModel();
			oPlantsModel.loadData(QService + sMasterDataPath + "Plants/getPlantsSQ");
			
			var oPlantsCombo = new sap.m.ComboBox({
				width : "100%",
				selectionChange: function () {
					var oLinesModel = oLineMultiCombo.getModel();
					var sPlant = oPlantsCombo.getSelectedKey();
					if(sPlant)
					{
						oLineMultiCombo.clearSelection();
						oLinesModel.loadData(QService + sMasterDataPath + "Lines/getLinesbyPlantSQ&Param.1=" + sPlant,false,false);
					}
				}
			});
			oPlantsCombo.setModel(oPlantsModel);
			
			var oPlantsSelectTemplate = new sap.ui.core.Item({
				key : "{PLANT}",
				text : "{NAME1}"
			});
			oPlantsCombo.bindAggregation("items","/Rowset/Row",oPlantsSelectTemplate);
		
			oSettingsDialog = new sap.m.Dialog({
				id: sDialogId,
				title: oLng_Opr.getText("Login_Settings"),
				contentWidth: '600px',
				content: [
					new sap.ui.layout.form.SimpleForm({
						editable: true,
						layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
						labelSpanL: 4,
						labelSpanM: 4,
						labelSpanS: 4,
						emptySpanL: 0,
						emptySpanM: 0,
						emptySpanS: 0,
						columnsL: 1,
						columnsM: 1,
						columnsS: 1,
						content: [
							new sap.m.Label({
								text: oLng_Opr.getText("Login_Plant"),
							}),
							oPlantsCombo,
							new sap.m.Label({
								text: oLng_Opr.getText("Login_Lines"),
							}),
							oLineMultiCombo,
							new sap.m.Label({
								text: oLng_Opr.getText("Login_WorkstName"),
							}),
							new sap.m.Input({
								id: 'workstNameInput',
								placeholder: oLng_Opr.getText("Login_WorkstNamePlaceh"),
								value: "{/Rowset/Row/WSNAME}"
							}),
							new sap.m.Label({
								text: oLng_Opr.getText("Login_WorkstDesc"),
							}),
							new sap.m.Input({
								id: 'workstDescInput',
								placeholder: oLng_Opr.getText("Login_WorkstDescPlaceh"),
								value: "{/Rowset/Row/WSDESC}"
							}),
							new sap.m.Label({
								text: oLng_Opr.getText("Login_IpAddress"),
							}),
							new sap.m.Text({
								id: 'workstationIp',
								editable:false,
								text: "{/Rowset/Row/WSNETIP}"
							})
						]
					})
				],
				beginButton: new sap.m.Button({
					text:  oLng_Opr.getText("Login_Save"),
					icon: "sap-icon://save",
					press: function () {
						
						var sPlant = oPlantsCombo.getSelectedKey();
						var oWorkstNameInput = $.UIbyID("workstNameInput");
						var sWorkstName = oWorkstNameInput.getValue();
						var sWorkstDesc = $.UIbyID("workstDescInput").getValue();
						var sWorkstIP = $.UIbyID("workstationIp").getText();
						var sWorkstMac = '';
						
						if(!sPlant)
						{
							oPlantsCombo.setValueState(sap.ui.core.ValueState.Error);
							oPlantsCombo.setValueStateText(oLng_Opr.getText("Login_PlantEmpty"));
							return;
						}
						else if(oPlantsCombo.getValueState() !== sap.ui.core.ValueState.None)
							oPlantsCombo.setValueState(sap.ui.core.ValueState.None);
						
						if(!sWorkstName)
						{
							oWorkstNameInput.setValueState(sap.ui.core.ValueState.Error);
							oWorkstNameInput.setValueStateText(oLng_Opr.getText("Login_WorkstNameEmpty"));
							return;
						}
						else if(oWorkstNameInput.getValueState() !== sap.ui.core.ValueState.None)
							oWorkstNameInput.setValueState(sap.ui.core.ValueState.None);
						
						var sRequest = sMasterDataPath + "Operators/addWorkstationSQ&Param.1=";
						sRequest += sPlant + "&Param.2=" + sWorkstName + "&Param.3=";
						sRequest += sWorkstDesc + "&Param.4=" + sWorkstIP + "&Param.5=" + sWorkstMac;
						var bSuccess = fnExeQuerym(sRequest);
						if(bSuccess)
						{
							var aItems = oLineMultiCombo.getSelectedItems();
							var aObjectLines = [];
							for(var i=0; i<aItems.length; i++)
							{
								const sLineId = aItems[i].getKey();
								var sLineTxt = aItems[i].getText();
								sLineTxt = sLineTxt.replace(sLineId + ' ','');
								
								aObjectLines.push({
									lineid: sLineId,
									linetxt: sLineTxt
								});
							}
							
							var oWorkstInfo = {
								PLANT: sPlant,
								LINES: aObjectLines,
								WSNAME: sWorkstName
							};
							var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							oStorage.put("WORKST_INFO", oWorkstInfo);
							
							oSettingsDialog.close();
						}
					}
				}),
				endButton: new sap.m.Button({
					text: oLng_Opr.getText("Login_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						var oWorkstNameInput = $.UIbyID("workstNameInput");
						if(oWorkstNameInput.getValueState() !== sap.ui.core.ValueState.None)
							oWorkstNameInput.setValueState(sap.ui.core.ValueState.None);
		
						oSettingsDialog.close();
					}
				}),
				beforeOpen: function() {
					oPlantsCombo.setSelectedKey(oSettingsDialog.getModel().getProperty("/Rowset/Row/PLANT"));
					oPlantsCombo.fireSelectionChange();
					var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
					var oSettings = oStorage.get("WORKST_INFO");
					if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
					{
						const aObjectLines = oSettings.LINES;
						var aLines = [];
						for(var i=0; i<aObjectLines.length; i++)
						{
							const sLineId = aObjectLines[i].lineid;
							if(oLineMultiCombo.getItemByKey(sLineId))
								aLines.push(sLineId);
						}
						oLineMultiCombo.setSelectedKeys(aLines);
					}
				}
			}).addStyleClass("noPaddingDialog");

			oSettingsDialog.setModel(new sap.ui.model.xml.XMLModel());
			
			var oAppView = sap.ui.getCore().byId('app');
			oAppView.addDependent(oSettingsDialog);
		}
		
		if(fnCallback)
		{
			var fnBeforeClose = function() {
				fnCallback();
				oSettingsDialog.detachBeforeClose(fnBeforeClose);
			};
			oSettingsDialog.attachBeforeClose(fnBeforeClose);
		}
		
		return oSettingsDialog;
	},
	
/***********************************************************************************************/
// Apertura dialog impostazioni
/***********************************************************************************************/
		
	onOpenSettings: function (fnCallback) {
		
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oSettings = oStorage.get("WORKST_INFO");
		
		if(typeof(fnCallback) !== 'function')
		{
			if(typeof(this.callback) === 'function')
				fnCallback = this.callback;
			else
				fnCallback = undefined;
		}

		// prendo una nuova istanza del controller perchè potrebbe non essere richiamata dalla stessa view
		var oController = sap.ui.controller('ui5app.login');
		var oSettingsDialog = oController.getSettingsDialog(fnCallback);
		
		var oModel = oSettingsDialog.getModel();
		if(oSettings && oSettings.hasOwnProperty('PLANT') && oSettings.hasOwnProperty('WSNAME'))
		{
			var sPlant = oSettings.PLANT;
			var sWsName = oSettings.WSNAME;
			oModel.loadData(QService + sMasterDataPath + "Operators/getWorkstationbyNameSQ&Param.1=" + sPlant + "&Param.2=" + sWsName,false,false);
		}
		
		if(!oModel.getProperty("/Rowset/Row"))
		{
			sap.ui.core.BusyIndicator.show(1000);
			
			$.ajax({
				url: "/XMII/CM/ProductionMain/Mobile/ui5app/getIPAddress.jsp",
				dataType: "html",
				type: "GET",
				cache: false,
				async: false,
				success: function (data) {
					var sIpAddress = $.trim(data);

					oModel.loadData(QService + sMasterDataPath + "Operators/getWorkstationbyIPSQ&Param.1=" + sIpAddress,false,false);

					if(!oModel.getProperty("/Rowset/Row"))
					{
						var oRowNode = oModel.oData.createElement("Row");
						oRowNode.appendChild(oModel.oData.createElement("WSNAME"));
						oRowNode.appendChild(oModel.oData.createElement("WSDESC"));
						oRowNode.appendChild(oModel.oData.createElement("WSNETIP"));
						var oRowsetNode = oModel.getObject("/Rowset");
						oRowsetNode.appendChild(oRowNode);
						oModel.setProperty("/Rowset/Row/WSNETIP",sIpAddress);
						oModel.refresh();
					}
				},
				complete: function() {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		}
		
		oSettingsDialog.open();
	},

/***********************************************************************************************/
// Apertura dialog configurazione
/***********************************************************************************************/

	onOpenConfig: function () {
		const sDialogId = 'configDialog';
		var oConfigDialog = $.UIbyID(sDialogId);
		if(!oConfigDialog) {
			
			var oAppController = sap.ui.getCore().byId("app").getController();
			
			var oConnectionSegButton = new sap.m.SegmentedButton({
				items: [
					new sap.m.SegmentedButtonItem({
						id: 'connCheckDisabledButton',
						text: oLng_Opr.getText("Login_Disabled"),
						key: 'connCheckDisabled'
					}),
					new sap.m.SegmentedButtonItem({
						id: 'connCheckEnabledButton',
						text: oLng_Opr.getText("Login_Enabled"),
						key: 'connCheckEnabled'
					})
				]
			});
			
			oConfigDialog = new sap.m.Dialog({
				id: sDialogId,
				title: oLng_Opr.getText("Login_Settings"),
				contentWidth: '600px',
				content: [
					new sap.ui.layout.form.SimpleForm({
						editable: true,
						layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
						labelSpanL: 5,
						labelSpanM: 5,
						labelSpanS: 5,
						emptySpanL: 0,
						emptySpanM: 0,
						emptySpanS: 0,
						columnsL: 1,
						columnsM: 1,
						columnsS: 1,
						content: [
							new sap.m.Label({
								text: oLng_Opr.getText("Login_ConnectionCheck")
							}),
							oConnectionSegButton
						]
					})
				],
				beginButton: new sap.m.Button({
					text:  oLng_Opr.getText("Login_Save"),
					icon: "sap-icon://save",
					press: function () {
						
						var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
						var oConfig = oStorage.get('CONFIG_INFO');
						if(!oConfig)
							oConfig = {};
						
						const bEnableCheck = oConnectionSegButton.getSelectedKey() === 'connCheckEnabled';
						oConfig['CONNECTION_CHECK'] = bEnableCheck;
						oStorage.put('CONFIG_INFO',oConfig);
						
						if(bEnableCheck)
							oAppController.startConnectionCheck();
						else
							oAppController.stopConnectionCheck();
					
						oConfigDialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: oLng_Opr.getText("Login_Cancel"),
					icon: "sap-icon://decline",
					press: function() {
						oConfigDialog.close();
					}
				}),
				beforeOpen: function() {
					var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
					var oConfig = oStorage.get('CONFIG_INFO');
					var sButtonItemId;
					if(oConfig && oConfig.hasOwnProperty('CONNECTION_CHECK') && oConfig.CONNECTION_CHECK)
						sButtonItemId = 'connCheckEnabled';
					else
						sButtonItemId = 'connCheckDisabled';
		
					oConnectionSegButton.setSelectedKey(sButtonItemId);
				}
			}).addStyleClass("noPaddingDialog");
		}
		
		oConfigDialog.open();
	},

/***********************************************************************************************/
// Pressione bottoni tastierino numerico
/***********************************************************************************************/

	onKeyboardButtonPress: function (oEvent) {
		var sNewText = '';
		var oInput = $.UIbyID('operatorInput');
		var iInputLenght = oInput.getValue().length;
		
		const sButtonText = oEvent.getSource().getText();
		if (sButtonText === '<') 
		{
			if (iInputLenght > 0)
				sNewText = oInput.getValue().substring(0, iInputLenght - 1);
		}
		else if (sButtonText === '<<')
			sNewText = '';
		else
			sNewText = oInput.getValue() + sButtonText;

		oInput.setValue(sNewText);
		oInput.fireLiveChange({value:sNewText});
		oInput.focus();
	},
	
/***********************************************************************************************/
// Pressione bottone per la pagina successiva
/***********************************************************************************************/
	
	onEnterPress: function () {
		
		var oController = sap.ui.getCore().byId('loginView').getController();
		
		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oSettings = oStorage.get("WORKST_INFO");
		if(oSettings && oSettings.hasOwnProperty('PLANT'))
		{
			const sPlant = oSettings.PLANT;
			const sPernr = $.UIbyID("operatorInput").getValue();
			
			var sLines = '';
			if(oSettings && oSettings.hasOwnProperty('LINES'))
				sLines = oSettings.LINES.join(',');
			
			var sWorkstName = '';
			if(oSettings && oSettings.hasOwnProperty('WSNAME'))
				sWorkstName = oSettings.WSNAME;
			
			var sLoginQuery = sMobilePath + "loginByPernrQR";
			sLoginQuery += "&Param.1=" + sPlant;
			sLoginQuery += "&Param.2=" + sLines;
			sLoginQuery += "&Param.3=" + sPernr;
			sLoginQuery += "&Param.4=" + sWorkstName;
			sLoginQuery += "&Param.5=" + sLanguage;
			
			// callback per handleRequest
			var loginFunction = function(oResults) 
			{			
				if(oResults.hasOwnProperty('code'))
					delete oResults.code;
				if(oResults.hasOwnProperty('message'))
					delete oResults.message;
				oResults['plant'] = sPlant;
				oResults['pernr'] = sPernr;
				
				var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
				oSessionStorage.put("LOGIN_INFO",oResults);
				
				var fTimeoutDelay = parseFloat(oResults.logoutTime);
				if(!isNaN(fTimeoutDelay) && fTimeoutDelay > 0)
					oAppController.startLogoutTimer(fTimeoutDelay);
			
				oAppRouter.navTo("menu",{
					query: {
						plant: sPlant,
						pernr: sPernr
					}
				});
			}
			
			var aResults = ['plantDesc','shiftId','shiftDesc','date','opeName','logoutTime','prevShiftId','prevDateShift'];
			oAppController.handleRequest(sLoginQuery,{
				showSuccess:false,
				onSuccess: loginFunction,
				results: aResults
			});
		}
		else
		{
			const sSettings = oLng_Opr.getText("Login_Settings");
			sap.m.MessageBox.show(oLng_Opr.getText("Login_NoPlantMessage"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: [sap.m.MessageBox.Action.OK, sSettings],
				onClose: function(oAction) {
					if(oAction === sSettings)
						oController.onOpenSettings();
				}
			});
		}
	}

});