// Controller Dichiarazione cassone
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL2.boxDecl", {
	
	_oArguments: undefined,
	_bIsFull: false,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute('boxDeclInc').attachPatternMatched(this.onIncMatched, this);
		oAppRouter.getRoute('boxDeclFull').attachPatternMatched(this.onFullMatched, this);
		
		var oOrderInput = $.UIbyID('boxDeclOrderInput');
		
		var nextFunc = this.onNext;
		oOrderInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onIncMatched: function (oEvent) {
		this._bIsFull = false;
		sap.ui.getCore().byId('boxDeclPage').setTitle(oLng_Opr.getText('BoxDecl_TitleInc'));
		
		this.onObjectMatched(oEvent);
	},
	
	onFullMatched: function (oEvent) {
		this._bIsFull = true;
		sap.ui.getCore().byId('boxDeclPage').setTitle(oLng_Opr.getText('BoxDecl_TitleFull'));
		
		this.onObjectMatched(oEvent);
	},
	
	onObjectMatched: function (oEvent) {
		
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		
		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclPage'),sPlant,sPernr);

		var oOrderInput = sap.ui.getCore().byId('boxDeclOrderInput');
		oOrderInput.setValue();
		
		jQuery.sap.delayedCall(1700, null, function() {
			oOrderInput.focus();
		});
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Avanzamento alla pagina successiva
/***********************************************************************************************/

	onNext: function(){

		var oOrderInput = $.UIbyID('boxDeclOrderInput');
		const sOrderPhase = oOrderInput.getValue();
		
		oOrderInput.setValueState(sap.ui.core.ValueState.None);
		
		const sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
		if(sErrorMessage) {
			oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
			oOrderInput.setValueStateText(sErrorMessage);
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else // Controlla commessa e fase, selezione linea e udc nuovo o incompleto
		{			
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}
			
			const aSpecialWarningCodes = [2,3];
			
			var oController = sap.ui.getCore().byId('boxDeclView').getController();
			var oArguments = oController._oArguments;
			
			var sCheckQuery = sMovementPath + "UDC/checkUDCOutPartLineQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + sOrderPhase;
			sCheckQuery += "&Param.3=" + aLines.join();
			sCheckQuery += "&Param.4=M";
			sCheckQuery += "&Param.5=" + sLanguage;
			
			var navFunction = function(oCallbackParams) 
			{		
				oArguments['full'] = oController._bIsFull;
				oArguments['order'] = oCallbackParams.order;
				oArguments['poper'] = oCallbackParams.poper;
				oArguments['idline'] = oCallbackParams.lineData.lineid;
				oArguments['udc'] = oCallbackParams.udcnr;

				oAppRouter.navTo('boxDeclInput',{
					query: oArguments
				});
			}
	
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onLineSuccess,
				showWarning: aSpecialWarningCodes, 			// array codici di errore per cui visualizza il messaggio
				onWarning: oController.onLineWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,		// come showWarning per recuperarlo in onWarning
					linesObjects: aLinesObjects,			// linee di postazione con linetxt corrispondente (match in onSuccess e onWarning)
					oneLineCallback: oController.onLineSuccess,		// callback per onWarning quando c'e una sola linea
					lineCallback: oController.onUdcSelection,		// callback a seguito della selezione della linea
					udcCallback: oController.onNav,					// callback di salvataggio nello storage dei dati recuperati
					navCallback: navFunction,						// callback di navigazione alla pagina successiva
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant
				},
				results: {
					returnProperties: ['outputLines','cicTxt','material','matDesc','cdlId','cdlDesc'],
					returnXMLDoc: true
				}
			});
		}
	},
	
	onLineSuccess: function(oResults,oCallbackParams) // linea di postazione trovata
	{
		oCallbackParams.returnedData = oResults;
		
		var oLine;
		var sLine = '';
		if(oResults.hasOwnProperty('outputLines') && oResults.outputLines)
		{
			var aOutputLines = oResults.outputLines.split(',');
			sLine = aOutputLines[0];
			if(oCallbackParams.linesObjects.length > 0)
			{
				oLine = selectSingleObject({
					objects: oCallbackParams.linesObjects,
					propertyName: 'lineid',
					propertyValue: sLine
				});
			}
			else // recupero il testo quando linesObjects vuoto
			{
				var sLineRequest = sMasterDataPath + "Lines/getLinebyIdSQ";
				sLineRequest += "&Param.1=" + sLine;
				var sLineTxt = fnGetAjaxVal(sLineRequest,'LINETXT',false);
				
				oLine = {
					lineid: sLine,
					linetxt: sLineTxt
				};
			}
		}
		
		if(!oLine)
		{
			oLine = {
				lineid: sLine,
				linetxt: ''
			};
		}
		
		oCallbackParams.lineCallback(oLine,oCallbackParams);
	},
	
	onLineWarning: function(oResults,oCallbackParams) // Selezione linea
	{		
		var oModel;
		var sModelTitle = '';
		var sModelDesc = '';
		var callbackFunc;
		if(oResults.hasOwnProperty('outputLines'))
		{
			if(oResults.outputLines)
			{
				var aOutputLines = oResults.outputLines.split(",");
				if(aOutputLines.length === 1) // una sola linea disponibile alla selezione
				{
					oCallbackParams.oneLineCallback(oResults,oCallbackParams);
					return;
				}
				else
				{
					var aObjectOutLines = [];
					for(var i=0; i<aOutputLines.length;i++)
					{
						const sCurrLine = aOutputLines[i];
						const oCurrOutLine = selectSingleObject({
							objects: oCallbackParams.linesObjects,
							propertyName: 'lineid',
							propertyValue: sCurrLine
						});
						
						if(oCurrOutLine)
							aObjectOutLines.push(oCurrOutLine);
						else
							aObjectOutLines.push({ // recuperare linetxt tramite query?
								lineid: sCurrLine,
								linetxt: ''
							});
					}
					oModel = new sap.ui.model.json.JSONModel(aObjectOutLines);
					sModelTitle = 'lineid';
					sModelDesc = 'linetxt';
					callbackFunc = oCallbackParams.lineCallback;
				}
			}
			else if(oCallbackParams.warningCodes.indexOf(parseInt(oResults.code)) > -1) // commessa e/o fase non trovate
			{
				oModel = new sap.ui.model.xml.XMLModel();
	
				var sLoadQuery = QService + sMasterDataPath + 'Lines/getLinesbyPlantSQ';
				sLoadQuery += '&Param.1=' + oCallbackParams.plant;
				oModel.loadData(sLoadQuery);
				
				sModelTitle = 'IDLINE';
				sModelDesc = 'LINETXT';
				
				callbackFunc = function(oLineInfo,oCallbackParams)
				{
					oLineInfo = {
						lineid: oLineInfo.IDLINE,
						linetxt: oLineInfo.LINETXT
					};
					oCallbackParams.lineCallback(oLineInfo,oCallbackParams);
				};
			}
		}
		
		oCallbackParams.returnedData = oResults;
		
		oAppController.selectFromDialog({
			title: oLng_Opr.getText("General_SelectLine"),
			noDataText: oLng_Opr.getText("General_NoLine"),
			callback: callbackFunc,
			callbackParams: oCallbackParams,
			model: oModel,
			modelTitle: sModelTitle,
			modelDesc: sModelDesc
		});
	},
	
	onUdcSelection: function(oLineInfo,oCallbackParams) // presenza udc incompleti
	{		
		oCallbackParams.lineData = oLineInfo;
		
		var nextFunction = oCallbackParams.udcCallback;
		
		var oUdcModel = new sap.ui.model.xml.XMLModel(oCallbackParams.returnedData.data);
		var oRowsetObject = oUdcModel.getObject("/Rowset/1/");
		if(oRowsetObject && oRowsetObject.childNodes.length-1>0) //numero di UDC nel modello
		{
			const sExistingTxt = oLng_Opr.getText('BoxDecl_SelectUdcExisting');
			sap.m.MessageBox.show(oLng_Opr.getText('BoxDecl_SelectUdcMessage'), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: [
					sExistingTxt,
					oLng_Opr.getText('BoxDecl_SelectUdcNew')
				],
				onClose: function(oAction) {
					if (oAction === sExistingTxt)
						oAppController.selectFromDialog({
							title: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							noDataText: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							callback: nextFunction,
							callbackParams: oCallbackParams,
							model: oUdcModel,
							modelPath: "/Rowset/1/Row",
							modelTitle: 'UDCNR',
							modelDesc: 'MATERIAL',
							template: oAppController.customListTemplate({
								titleText: "{UDCNR}",
								bottomLeftText: "{MATERIAL}",
								topRightText: {
									parts: [
										{path: "QTYRES" }
									],		     
									formatter: function(sQtyRes) {
										return oLng_Opr.getText('BoxDecl_Qty') + ": " + sQtyRes;
									}
								},
								bottomRightText: {
									parts: [
										{path: "TIMEID" }
									],		     
									formatter: dateFormatter
								}
							})
						});
					else
						nextFunction(undefined,oCallbackParams);
				}
			});
		}
		else
			nextFunction(undefined,oCallbackParams);
	},
	
	onNav: function(oUdcObject,oCallbackParams) // passaggio alla schermata successiva
	{		
		var sUdc = '';
		if(oUdcObject && oUdcObject.hasOwnProperty('UDCNR'))
			sUdc = oUdcObject.UDCNR;
		else
			oUdcObject = {
				ORDER: oCallbackParams.order,
				POPER: oCallbackParams.poper,
				UDCNR: sUdc,
				MATERIAL: oCallbackParams.returnedData.material,
				MAT_DESC: oCallbackParams.returnedData.matDesc
			};
			
		oCallbackParams.udcnr = sUdc;

		oUdcObject.CICTXT = oCallbackParams.returnedData.cicTxt;
		oUdcObject.LINEID = oCallbackParams.lineData.lineid;
		oUdcObject.LINETXT = oCallbackParams.lineData.linetxt;
		oUdcObject.CDLID = oCallbackParams.returnedData.cdlId;
		oUdcObject.CDLDESC = oCallbackParams.returnedData.cdlDesc;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);
	
		oCallbackParams.navCallback(oCallbackParams);
	}
});
