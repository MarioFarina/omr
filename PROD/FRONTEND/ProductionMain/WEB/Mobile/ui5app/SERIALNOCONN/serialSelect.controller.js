// Controller Selezione seriali macchina non collegata
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.SERIALNOCONN.serialSelect", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("serialSelect").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const aArgParams = ['plant','pernr','order','poper','idline','first','last'];
		const oArguments = oAppController.getPatternArguments(oEvent,aArgParams);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('serialSelectPage'),sPlant,sPernr);
		
		const sFirst = oArguments.first;
		const sLast = oArguments.last;
		

		var oList = $.UIbyID('serialSelectList');
		oList.destroyItems();
		var sCurrentSerial = sFirst;
		while(sCurrentSerial <= sLast)
		{
			var oListItem = new sap.m.StandardListItem({
				title: sCurrentSerial
			});
			oList.addItem(oListItem);
			
			sCurrentSerial = incNumString(sCurrentSerial);
		}
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('serialSelectView').getController();
		
		var oArguments = oController._oArguments;
		if(oArguments.hasOwnProperty('first'))
			delete oArguments.first;
		if(oArguments.hasOwnProperty('last'))
			delete oArguments.last;
		
		oAppRouter.navTo('serialCheck',{
			query: oArguments
		});
	},

/***********************************************************************************************/
// Cancellazione seriale dalla lista
/***********************************************************************************************/

	onDelete: function(oEvent) {
		var oItem = oEvent.getParameter("listItem");
		oItem.destroy();
	},

/***********************************************************************************************/
// Conferma ed esecuzione della dichiarazione
/***********************************************************************************************/

	onConfirm: function() {
		var oController = sap.ui.getCore().byId('serialSelectView').getController();
		var oArguments = oController._oArguments;
		
		var oList = $.UIbyID('serialSelectList');
		var aItems = oList.getItems();
		var sErrorMessage;
		if(aItems.length === 0)
			sErrorMessage = oLng_Opr.getText('SerialSelect_EmptyListErrorMessage')
		else
		{
			var sSerialList = '';
			for (var i=0; i<aItems.length; i++) {
				if(sSerialList)
					sSerialList += ","
				sSerialList += aItems[i].getTitle();
			}
			
			var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
			const oFuncData = oSessionStorage.get('FUNCDATA');
			const sMaterial = ((oFuncData && oFuncData.MATERIAL) ? oFuncData.MATERIAL : '');

			var sInsertQuery = sDataLoadPath + "Movement/SetSerialsListQR";
			sInsertQuery += "&Param.1=" + oArguments.plant;
			sInsertQuery += "&Param.2=" + oAppController._sDate;
			sInsertQuery += "&Param.3=" + oAppController._sShift;
			sInsertQuery += "&Param.4=" + oArguments.order;
			sInsertQuery += "&Param.5=" + sMaterial;
			sInsertQuery += "&Param.6=" + sSerialList;
			sInsertQuery += "&Param.7=" + getDatetimeString(new Date());
			
			var successFunction = function() {
				
				if(oArguments.hasOwnProperty('order'))
					delete oArguments.order;
				if(oArguments.hasOwnProperty('poper'))
					delete oArguments.poper;
				if(oArguments.hasOwnProperty('idline'))
					delete oArguments.idline;
				if(oArguments.hasOwnProperty('first'))
					delete oArguments.first;
				if(oArguments.hasOwnProperty('last'))
					delete oArguments.last;
				
				oAppRouter.navTo('serialNoConn',{
					query: oArguments
				});
			};
			
			oAppController.handleRequest(sInsertQuery,{
				onSuccess: successFunction
			});
		}
	},

});