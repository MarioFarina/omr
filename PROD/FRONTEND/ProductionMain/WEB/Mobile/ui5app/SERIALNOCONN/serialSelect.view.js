// View Selezione seriali macchina non collegata
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SERIALNOCONN.serialSelect", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SERIALNOCONN.serialSelect";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oList = new sap.m.List({
			id: 'serialSelectList',
			mode: sap.m.ListMode.Delete,
			delete: oController.onDelete
		});
		
		var oPage = new sap.m.Page({
			id: "serialSelectPage",
			//enableScrolling: false,
			title: oLng_Opr.getText('SerialSelect_Title'),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oList
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://accept",
						text: oLng_Opr.getText("SerialSelect_Confrim"),
						press: oController.onConfirm
					}).addStyleClass("greenButton noBorderButton")
				]
			})
		});
		
	  	return oPage;
	}

});