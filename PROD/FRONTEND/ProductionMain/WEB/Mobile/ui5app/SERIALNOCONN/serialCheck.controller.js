// Controller Dichiarazione seriale con macchina non collegata
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.SERIALNOCONN.serialCheck", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("serialCheck").attachPatternMatched(this.onObjectMatched, this);
		
		var oFirstInput = $.UIbyID('serialCheckFirst');
		var oLastInput = $.UIbyID('serialCheckLast');
		
		oFirstInput.onsapenter = function (oEvent)
		{
			if(oFirstInput.getValue())
				oLastInput.focus();
		};
		
		var nextFunc = this.onNext;
		oLastInput.onsapenter = function (oEvent)
		{
			if(oLastInput.getValue())
				nextFunc();
		};
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const aArgParams = ['plant','pernr','order','poper','idline'];
		const oArguments = oAppController.getPatternArguments(oEvent,aArgParams);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('serialCheckPage'),sPlant,sPernr);
		
		var oView = this.getView();
		var oModel = oView.getModel();
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oFuncData = oSessionStorage.get('FUNCDATA');

		if(!oFuncData)
			oFuncData = {};
		
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oFuncData);
		
		var oFirstInput = $.UIbyID('serialCheckFirst');
		var oLastInput = $.UIbyID('serialCheckLast');
		
		oFirstInput.setValueState(sap.ui.core.ValueState.None);
		oLastInput.setValueState(sap.ui.core.ValueState.None);
		
		oFirstInput.setValue();
		oLastInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oFirstInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('serialCheckView').getController();
		
		var oArguments = oController._oArguments;
		if(oArguments.hasOwnProperty('order'))
			delete oArguments.order;
		if(oArguments.hasOwnProperty('poper'))
			delete oArguments.poper;
		if(oArguments.hasOwnProperty('idline'))
			delete oArguments.idline;
		
		oAppRouter.navTo('serialNoConn',{
			query: oController._oArguments
		});
	},


/***********************************************************************************************/
// Passaggio alla pagina successiva
/***********************************************************************************************/

	onNext: function() {
		var oController = sap.ui.getCore().byId('serialCheckView').getController();
		
		var oFirstInput = $.UIbyID('serialCheckFirst');
		var oLastInput = $.UIbyID('serialCheckLast');
		
		oFirstInput.setValueState(sap.ui.core.ValueState.None);
		oLastInput.setValueState(sap.ui.core.ValueState.None);
		
		const sFirst = oFirstInput.getValue();
		const sLast = oLastInput.getValue();
		const iFirst = parseInt(sFirst);
		const iLast = parseInt(sLast);
			
		var sErrorMessage;
		if(!sFirst || sFirst.length != 20 || isNaN(iFirst))
		{
			sErrorMessage = oLng_Opr.getText("SerialCheck_FirstErrorMsg");
			oFirstInput.setValueState(sap.ui.core.ValueState.Warning);
			oFirstInput.setValueStateText(sErrorMessage);
		}
		else if(!sLast || sLast.length != 20 || isNaN(iLast))
		{
			sErrorMessage = oLng_Opr.getText("SerialCheck_LastErrorMsg");
			oLastInput.setValueState(sap.ui.core.ValueState.Warning);
			oLastInput.setValueStateText(sErrorMessage);
		}
		else if(iFirst > iLast)
		{
			sErrorMessage = oLng_Opr.getText("SerialCheck_CompareErrorMsg");
			oFirstInput.setValueState(sap.ui.core.ValueState.Warning);
			oFirstInput.setValueStateText(sErrorMessage);
			oLastInput.setValueState(sap.ui.core.ValueState.Warning);
			oLastInput.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oArguments = oController._oArguments;
			oArguments['first'] = sFirst;
			oArguments['last'] = sLast;
			
			oAppRouter.navTo('serialSelect',{
				query: oArguments
			});
		}
	},

});