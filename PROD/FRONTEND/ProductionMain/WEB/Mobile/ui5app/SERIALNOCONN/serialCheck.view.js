// View Dichiarazione seriale con macchina non collegata
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SERIALNOCONN.serialCheck", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SERIALNOCONN.serialCheck";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oView = this;
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12"
					})
				}),
				new sap.m.Text({
					text: '{/ORDER}',
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_Phase") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M2 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/POPER" },
							{path: "/CICTXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/POPER',
						description: '/CICTXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_Line") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak:true
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/LINEID" },
							{path: "/LINETXT"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/LINEID',
						description: '/LINETXT'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_Material") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M1 S12"
					})
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "/MATERIAL" },
							{path: "/MAT_DESC"}
						],		     
						formatter: idTextFormatter
					},
					maxLines: 1,
					width: '100%',
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}),
				new sap.ui.core.Icon({
					src: "sap-icon://message-information",
					size: "18px",
					press: [oAppController.onShowInfo,{
						view: oView,
						title: '/MATERIAL',
						description: '/MAT_DESC'
					}],
					layoutData : new sap.ui.layout.GridData({
						span: "L1 M1 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_FirstSerial") + ":",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'serialCheckFirst',
					type: sap.m.InputType.Number,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				}),
				new sap.m.Label({
					text: oLng_Opr.getText("SerialCheck_LastSerial") + ":",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L2 M3 S12",
						linebreak: true
					})
				}).addStyleClass("inputLabel"),
				new sap.m.Input({
					id: 'serialCheckLast',
					type: sap.m.InputType.Number,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				})
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
		
		var oPage = new sap.m.Page({
			id: "serialCheckPage",
			enableScrolling: false,
			title: oLng_Opr.getText("SerialCheck_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("SerialCheck_Next"),
							icon: "sap-icon://accept",
							press: oController.onNext
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar({})
		});
		
	  	return oPage;
	}

});