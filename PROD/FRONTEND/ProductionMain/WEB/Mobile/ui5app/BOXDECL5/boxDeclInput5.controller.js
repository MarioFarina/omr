// +
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.BOXDECL5.boxDeclInput5", {

	_oArguments: undefined,
	_sWsName: '',
	_aParents: [],
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("boxDeclInput5").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		var oAppController = sap.ui.getCore().byId('app').getController();

		const oArgParams = ['plant', 'pernr', 'order', 'poper', 'idline'];
		const oArguments = oAppController.getPatternArguments(oEvent, oArgParams);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('boxDeclInput5Page'), sPlant, sPernr);

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put("FUNC_INFO", false);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		var totem = oSessionStorage.get('FMBTOTEM');

		var tileBU = sap.ui.getCore().byId('tileBU');
		var tileSS = sap.ui.getCore().byId('tileSS');
		var tileSC = sap.ui.getCore().byId('tileSC');
		var tileSV = sap.ui.getCore().byId('tileSV');

		tileBU.setVisible(false);
		tileSS.setVisible(false);
		tileSC.setVisible(false);
		tileSV.setVisible(false);

		$.each(totem, function( i, row ) {
            switch(row.TPOPE) {
				case 'BU': tileBU.setVisible(true);
					break;
				case 'SC': tileSC.setVisible(true);
					break;
				case 'SO': tileSS.setVisible(true);
					break;
				case 'SV': tileSV.setVisible(true);
					break;
			}
        });
				
	},

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('boxDeclInput5View').getController();
		var oArguments = oController._oArguments;

		delete oArguments.order;
		delete oArguments.poper;
		delete oArguments.idline;
		delete oArguments.udc;

		oAppRouter.navTo('boxDecl5',{
			query: oController._oArguments
		});
	},

	pressTile: function() {
		let oController = this.controller;
		let tile = this.tile;
		let oArguments = oController._oArguments;
		let nav;
		let oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		let oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		let datiTotem = oSessionStorage.get('FMBTOTEM');
		let oSettings = oStorage.get("WORKST_INFO");
		let aLines = [];
		let aLinesObjects = [];
		let oper;

		if(tile == 'BU') {
			nav = 'boxDeclInput6';
			oper = 'M';
		} else if(tile == 'SO') {
			nav = 'suspDeclInput6';
			oper = 'C';
		} else if(tile == 'SC') {
			nav = 'closeUdcConfirm6';
			oper = 'X';
		} else if(tile == 'SV') {
			nav = 'serviceDeclInput';
			oper = 'X';
		}
		
		if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
		{
			aLinesObjects = oSettings.LINES;
			for(var i=0; i<aLinesObjects.length; i++)
				aLines.push(aLinesObjects[i].lineid);
		}

		const aSpecialWarningCodes = [2,3];

		var sCheckQuery = sMovementPath + "NextGen/checkUDCOutPartLine_NextGenQR";
		sCheckQuery += "&Param.1=" + oArguments.plant;
		sCheckQuery += "&Param.2=" + oArguments.order + oArguments.poper;
		sCheckQuery += "&Param.3=" + aLines.join();
		sCheckQuery += "&Param.4=" + oper;
		sCheckQuery += "&Param.5=" + sLanguage;

		var navFunction = function(oCallbackParams)
		{
			oArguments['order'] = oCallbackParams.order;
			oArguments['poper'] = oCallbackParams.poper;
			oArguments['idline'] = "";
			oArguments['udc'] = oCallbackParams.udcnr;
			oAppRouter.navTo(oCallbackParams.nav, {
				query: oArguments
			});
		}

		oAppController.handleRequest(sCheckQuery, {
			showSuccess: false,
			onSuccess: oController.onLineSuccess,
			showWarning: aSpecialWarningCodes, 			// array codici di errore per cui visualizza il messaggio
			onWarning: oController.onLineWarning,
			callbackParams: {
				warningCodes: aSpecialWarningCodes,		// come showWarning per recuperarlo in onWarning
				linesObjects: aLinesObjects,			// linee di postazione con linetxt corrispondente (match in onSuccess e onWarning)
				oneLineCallback: oController.onLineSuccess,		// callback per onWarning quando c'e una sola linea
				lineCallback: oController.onUdcSelection,		// callback a seguito della selezione della linea
				udcCallback: oController.onNav,					// callback di salvataggio nello storage dei dati recuperati
				navCallback: navFunction,						// callback di navigazione alla pagina successiva
				order: oArguments.order,
				poper: oArguments.poper,
				plant: oArguments.plant,
				nav: nav,
				datiTotem: datiTotem,
				funzione: tile
			},
			results: {
				returnProperties: [
					'outputLines', 'cicTxt', 'material', 'matDesc', 'cdlId', 'cdlDesc', 'imbObbl'
				],
				returnXMLDoc: true
			}
		});
	},

	onLineSuccess: function(oResults, oCallbackParams)
	{
		oCallbackParams.returnedData = oResults;

		var oLine;
		var sLine = '';
		if(oResults.hasOwnProperty('outputLines') && oResults.outputLines)
		{
			var aOutputLines = oResults.outputLines.split(',');
			sLine = aOutputLines[0];
			if(oCallbackParams.linesObjects.length > 0)
			{
				oLine = selectSingleObject({
					objects: oCallbackParams.linesObjects,
					propertyName: 'lineid',
					propertyValue: sLine
				});
			}
			else // recupero il testo quando linesObjects vuoto
			{
				var sLineRequest = sMasterDataPath + "Lines/getLinebyIdSQ";
				sLineRequest += "&Param.1=" + sLine;
				var sLineTxt = fnGetAjaxVal(sLineRequest, 'LINETXT', false);

				oLine = {
					lineid: sLine,
					linetxt: sLineTxt
				};
			}
		}

		if(!oLine)
		{
			oLine = {
				lineid: sLine,
				linetxt: ''
			};
		}

		oCallbackParams.lineCallback(oLine, oCallbackParams);
	},

	onLineWarning: function(oResults, oCallbackParams) // Selezione linea
	{
		var oModel;
		var sModelTitle = '';
		var sModelDesc = '';
		var callbackFunc;
		if(oResults.hasOwnProperty('outputLines'))
		{
			if(oResults.outputLines)
			{
				var aOutputLines = oResults.outputLines.split(",");
				if(aOutputLines.length === 1) // una sola linea disponibile alla selezione
				{
					oCallbackParams.oneLineCallback(oResults, oCallbackParams);
					return;
				}
				else
				{
					var aObjectOutLines = [];
					for(var i=0; i<aOutputLines.length;i++)
					{
						const sCurrLine = aOutputLines[i];
						const oCurrOutLine = selectSingleObject({
							objects: oCallbackParams.linesObjects,
							propertyName: 'lineid',
							propertyValue: sCurrLine
						});

						if(oCurrOutLine)
							aObjectOutLines.push(oCurrOutLine);
						else
							aObjectOutLines.push({ // recuperare linetxt tramite query?
								lineid: sCurrLine,
								linetxt: ''
							});
					}
					oModel = new sap.ui.model.json.JSONModel(aObjectOutLines);
					sModelTitle = 'lineid';
					sModelDesc = 'linetxt';
					callbackFunc = oCallbackParams.lineCallback;
				}
			}
			else if(oCallbackParams.warningCodes.indexOf(parseInt(oResults.code)) > -1) // commessa e/o fase non trovate
			{
				oModel = new sap.ui.model.xml.XMLModel();

				var sLoadQuery = QService + sMasterDataPath + 'Lines/getLinesbyPlantSQ';
				sLoadQuery += '&Param.1=' + oCallbackParams.plant;
				oModel.loadData(sLoadQuery);

				sModelTitle = 'IDLINE';
				sModelDesc = 'LINETXT';

				callbackFunc = function(oLineInfo, oCallbackParams)
				{
					oLineInfo = {
						lineid: oLineInfo.IDLINE,
						linetxt: oLineInfo.LINETXT
					};
					oCallbackParams.lineCallback(oLineInfo, oCallbackParams);
				};
			}
		}

		oCallbackParams.returnedData = oResults;

		oAppController.selectFromDialog({
			title: oLng_Opr.getText("General_SelectLine"),
			noDataText: oLng_Opr.getText("General_NoLine"),
			callback: callbackFunc,
			callbackParams: oCallbackParams,
			model: oModel,
			modelTitle: sModelTitle,
			modelDesc: sModelDesc
		});
	},

	onUdcSelection: function(oLineInfo, oCallbackParams) // presenza udc incompleti
	{
		oCallbackParams.lineData = oLineInfo;

		var nextFunction = oCallbackParams.udcCallback;

		let udcFull;
		let tpope = oCallbackParams.funzione;
		$.each(oCallbackParams.datiTotem, function(i, row) {
			if(row.TPOPE == tpope) {
				udcFull = row.UDCFULL;
			}
		});		

		var oUdcModel = new sap.ui.model.xml.XMLModel(oCallbackParams.returnedData.data);
		var oRowsetObject = oUdcModel.getObject("/Rowset/1/");
		if(udcFull != 1 && (oRowsetObject && oRowsetObject.childNodes.length -1 > 0)) //numero di UDC nel modello
		{
			const sExistingTxt = oLng_Opr.getText('BoxDecl_SelectUdcExisting');
			sap.m.MessageBox.show(oLng_Opr.getText('BoxDecl_SelectUdcMessage'), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText('General_WarningText'),
				actions: [
					sExistingTxt,
					oLng_Opr.getText('BoxDecl_SelectUdcNew')
				],
				onClose: function(oAction) {
					if (oAction === sExistingTxt)
						oAppController.selectFromDialog({
							title: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							noDataText: oLng_Opr.getText('BoxDecl_SelectUdcTitle'),
							callback: nextFunction,
							callbackParams: oCallbackParams,
							model: oUdcModel,
							modelPath: "/Rowset/1/Row",
							modelTitle: 'UDCNR',
							modelDesc: 'MATERIAL',
							template: oAppController.customListTemplate({
								titleText: "{UDCNR}",
								bottomLeftText: "{MATERIAL}",
								topRightText: {
									parts: [
										{path: "QTYRES" }
									],
									formatter: function(sQtyRes) {
										return oLng_Opr.getText('BoxDecl_Qty') + ": " + sQtyRes;
									}
								},
								bottomRightText: {
									parts: [
										{path: "TIMEID" }
									],
									formatter: dateFormatter
								},
								left: oCallbackParams.leftUdcData,
								right: [
									new sap.m.Label({
										text: "{LAST_OPENAME}",
										width: '100%',
										textAlign: sap.ui.core.TextAlign.Right
									}).addStyleClass("smallLabel sapUiTinyMarginTop")
								]
							})
						});
					else
						nextFunction(undefined, oCallbackParams);
				}
			});
		}
		else
			nextFunction(undefined, oCallbackParams);
	},

	onNav: function(oUdcObject, oCallbackParams) // passaggio alla schermata successiva
	{
		var sUdc = '';
		if(oUdcObject && oUdcObject.hasOwnProperty('UDCNR'))
			sUdc = oUdcObject.UDCNR;
		else
			oUdcObject = {
				ORDER: oCallbackParams.order,
				POPER: oCallbackParams.poper,
				UDCNR: sUdc,
				MATERIAL: oCallbackParams.returnedData.material,
				MAT_DESC: oCallbackParams.returnedData.matDesc
			};

		oCallbackParams.udcnr = sUdc;

		oUdcObject.CICTXT = oCallbackParams.returnedData.cicTxt;
		oUdcObject.IMBOBBL = oCallbackParams.returnedData.imbObbl;
		oUdcObject.LINEID = oCallbackParams.lineData.lineid;
		oUdcObject.LINETXT = oCallbackParams.lineData.linetxt;
		oUdcObject.CDLID = oCallbackParams.returnedData.cdlId;
		oUdcObject.CDLDESC = oCallbackParams.returnedData.cdlDesc;

		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);

		oCallbackParams.navCallback(oCallbackParams);
	}
});
