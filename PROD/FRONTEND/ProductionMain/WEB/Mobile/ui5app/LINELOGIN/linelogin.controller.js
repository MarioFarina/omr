//MF20191220
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.LINELOGIN.linelogin", {

	_oArguments: undefined,
	
	pernr: null,
	plant: null,
	department: null,
	line: null,
	lines: [],
	
	onInit: function () {
		oAppRouter.getRoute('linelogin').attachPatternMatched(this.onObjectMatched, this);
	},

	onObjectMatched: function (oEvent) {

		var oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr','department']);
		this._oArguments = oArguments;

		this.pernr = oArguments.pernr;
		this.plant = oArguments.plant;
		this.department = oArguments.department;
	
		this.getLines();
	},

	getLines : function() {
		var self = this;
		var oLinesModelXML = new sap.ui.model.xml.XMLModel();
		
		var sLoadQuery = QService + sMasterDataPath + "Lines/GetLineLogin_XQ";
		sLoadQuery += "&Param.1=" + this.department;
		sLoadQuery += "&Param.2=" + this.pernr;

		oLinesModelXML.loadData(sLoadQuery);
		
		this.lines = [];
		
		oLinesModelXML.attachRequestCompleted(function () {
			for (var i = 0; i < oLinesModelXML.oData.documentElement.children[0].children.length; i++) {
				if (i === 0)
					continue;
				
				var idline = oLinesModelXML.oData.documentElement.children[0].children[i].children[0].innerHTML;
				var linetxt = oLinesModelXML.oData.documentElement.children[0].children[i].children[1].innerHTML;
				
				self.lines.push({"IDLINE": idline, "LINETXT": linetxt});
			}
		});
		
		this.getView().setModel(oLinesModelXML);
	},
	
	onFunctionSelected: function(oEvent) {
		var oController = sap.ui.getCore().byId('lineloginView').getController();
		
		for (var i = 0; i < oController.lines.length; i++) {
			if(oController.lines[i].LINETXT === oEvent.oSource.mProperties.title) {
				oController.line = oController.lines[i].IDLINE;
				break;
			}
		}
		
		var loginDate = oEvent.oSource.mProperties.info;
		
		if (loginDate === "")
			oController.loginWarning();
		else
			oController.logoutWarning();
	},
	
	onLogout: function () {
		var oController = sap.ui.getCore().byId('lineloginView').getController();
		oController._aParents = [];
	},
	
	onNavBack: function() {
		var oController = sap.ui.getCore().byId('lineloginView').getController();
		
		var sLoadQuery = QService + sMasterDataPath + "Departments/GetDepartmentLogin_XQ"; 
		sLoadQuery += "&Param.1=" + oController.plant;
		sLoadQuery += "&Param.2=" + oController.pernr;

		var oDepartmentsModelXML = new sap.ui.model.xml.XMLModel();
		oDepartmentsModelXML.loadData(sLoadQuery);
		oDepartmentsModelXML.attachRequestCompleted(function () {
			
			if(oDepartmentsModelXML.oData.documentElement.children[0].children.length - 1 === 1) {
				oAppRouter.navTo('menu',{
					query: {"pernr": oController.pernr, "plant": oController.plant}
				});
			} else {
				oAppRouter.navTo('departmentlogin',{
					query: {"pernr": oController.pernr, "plant": oController.plant}
				});
			}
			
		});
	},
	
	loginWarning: function (){
		var oController = sap.ui.getCore().byId('lineloginView').getController();
		var self = this;
		
    	sap.ui.commons.MessageBox.show(
			"Conferma login?", 
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Login", 
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
			function(sResult) {
				if (sResult == 'YES'){					
					var sLoadQuery = QService + sLoginPath + "Login_XQ"; 
					sLoadQuery += "&Param.1=" + oController.pernr;
					sLoadQuery += "&Param.2=" + oController.plant;
					sLoadQuery += "&Param.3=" + oController.line;

					var oLoginXML = new sap.ui.model.xml.XMLModel();
					oLoginXML.loadData(sLoadQuery);
					oLoginXML.attachRequestCompleted(function () {

						if(oLoginXML.oData.documentElement.children["0"].children[1].children["0"].innerHTML === "OK") {
							self.getLines();
						} else {
							sap.ui.commons.MessageBox.show(oLoginXML.oData.documentElement.children["0"].children[1].children["1"].innerHTML,
							sap.ui.commons.MessageBox.Icon.ERROR,
							"Errore Login"
							[sap.ui.commons.MessageBox.Action.OK],
							sap.ui.commons.MessageBox.Action.OK);
						}						
					});	
				}
			},
			sap.ui.commons.MessageBox.Action.YES
    	);
	},
	
	logoutWarning: function (){
		var oController = sap.ui.getCore().byId('lineloginView').getController();
		var self = this;
		
    	sap.ui.commons.MessageBox.show(
			"Conferma logout?", 
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Logout", 
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
			function(sResult) {
				if (sResult == 'YES'){
					var sLoadQuery = QService + sLoginPath + "Logout_XQ"; 
					sLoadQuery += "&Param.1=" + oController.pernr;
					sLoadQuery += "&Param.2=" + oController.plant;
					sLoadQuery += "&Param.3=" + oController.line;

					var oLoginXML = new sap.ui.model.xml.XMLModel();
					oLoginXML.loadData(sLoadQuery);
					oLoginXML.attachRequestCompleted(function () {

						if(oLoginXML.oData.documentElement.children["0"].children[1].children["0"].innerHTML === "OK") {
							self.getLines();
						} else {
							sap.ui.commons.MessageBox.show(oLoginXML.oData.documentElement.children["0"].children[1].children["1"].innerHTML,
							sap.ui.commons.MessageBox.Icon.ERROR,
							"Errore Logout"
							[sap.ui.commons.MessageBox.Action.OK],
							sap.ui.commons.MessageBox.Action.OK);
						}						
					});	
				}
			},
			sap.ui.commons.MessageBox.Action.YES
    	);
	},
});
