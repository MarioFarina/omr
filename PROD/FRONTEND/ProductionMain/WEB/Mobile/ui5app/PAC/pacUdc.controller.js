// Controller Pac - Lettura UDC
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.PAC.pacUdc", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("pacUdc").attachPatternMatched(this.onObjectMatched, this);

		var nextFunc = this.onNext;
		var oUdcCombo = sap.ui.getCore().byId("pacUdcInput");
		oUdcCombo.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		var oController = this;
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		oController._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('pacUdcPage'),sPlant,sPernr);
		
		var oUdcInput = sap.ui.getCore().byId('pacUdcInput');
		oUdcInput.setValueState();
		oUdcInput.setValueStateText();
		oUdcInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oUdcInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
	onNext: function() {
		var oUdcInput = $.UIbyID('pacUdcInput');
		const sSelectedUdc = oUdcInput.getValue();
		
		oUdcInput.setValueState(sap.ui.core.ValueState.None);
		oUdcInput.setValueStateText();
		
		var sErrorMessage;
		if(!sSelectedUdc)
		{
			sErrorMessage = oLng_Opr.getText("PacUdc_UDCErrorMsg");
			oUdcInput.setValueState(sap.ui.core.ValueState.Warning);
			oUdcInput.setValueStateText(sErrorMessage);
		}
		
		if(sErrorMessage)
		{
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var sCheckQuery = sMovementPath + "PAC/checkUDCForPACQR";
			sCheckQuery += "&Param.1=" + sSelectedUdc;
			sCheckQuery += "&Param.2=" + sLanguage;
			
			var oController = sap.ui.getCore().byId('pacUdcView').getController();
			
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oController.onUdcSuccess,
				results: {
					returnXMLDoc: true
				}
			});
		}
	},
	
	onUdcSuccess: function(oResults,oCallbackParams) {
		
		var xmlData = oResults.data.childNodes[0]; // Rowsets
		xmlData = xmlData.childNodes[1]; // Rowset 1
		xmlData = xmlData.childNodes[1]; // Row 1 (Columns 0)
		
		var oUdcObject = getJsonObjectFromXMLObject(xmlData);
		console.log(oUdcObject);
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		var oArguments = oController._oArguments;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		oSessionStorage.put('FUNCDATA',oUdcObject);

		oArguments['udcnr'] = oUdcObject.UDCNR;

		oAppRouter.navTo('pacDm',{
			query: oArguments
		});
		
	},
	
	onNewUdC: function() {
		var oController = sap.ui.getCore().byId('pacUdcView').getController();
		var oArguments = oController._oArguments;
		
		oAppRouter.navTo('pacNewUdc',{
			query: oArguments
		});
	}

});