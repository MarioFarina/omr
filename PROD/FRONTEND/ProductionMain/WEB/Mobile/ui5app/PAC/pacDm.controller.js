// Controller Pac - Lettura DataMatrix
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.PAC.pacDm", {
	
	_oArguments: undefined,
	_oLastDM: undefined,
	
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("pacDm").attachPatternMatched(this.onObjectMatched, this);
		
		var nextFunc = this.processDmCausal;
		var oPacDmCausalInput = sap.ui.getCore().byId("pacDmCausalInput");
		oPacDmCausalInput.onsapenter = function (oEvent)
		{
			nextFunc();
		}
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {
		
		const oArgParams = ['plant','pernr','udcnr'];
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams);
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;
		const sUdcNr = oArguments.udcnr;
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		var oViewModel;
		if(oUdcObject && oUdcObject.hasOwnProperty('UDCNR') && oUdcObject.UDCNR === sUdcNr)
		{
			console.log(JSON.stringify(oUdcObject));
			oViewModel = new sap.ui.model.json.JSONModel(oUdcObject);
		}
		else
		{
			oUdcObject = {};
			/*
			this._sPropertyPath = "/Rowset/0/Row/0";
			oViewModel = new sap.ui.model.xml.XMLModel();
			var sUdcQuery = QService + sMovementPath + "UDC/getDudcMastSQ";
			sUdcQuery += "&Param.1=" + sUdcNr;
			oViewModel.loadData(sUdcQuery,false,false);
			*/
		}
		this.getView().setModel(oViewModel);
		
		this.loadGoods(sUdcNr);
		this.loadSuspended(oUdcObject.UDCSUSP);
		
		oAppController.loadSubHeaderInfo($.UIbyID('pacDmPage'),sPlant,sPernr);
		
		var oPacDmCausalInput = sap.ui.getCore().byId("pacDmCausalInput");
		oPacDmCausalInput.setValue();
		
		jQuery.sap.delayedCall(500, null, function() {
			oPacDmCausalInput.focus();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna a Pac - Lettura UDC
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('pacDmView').getController();
		
		var oArguments = oController._oArguments;
		if(oArguments.hasOwnProperty('udcnr'))
			delete oArguments.udcnr;
		if(oArguments.hasOwnProperty('idline'))
			delete oArguments.idline;
		
		oAppRouter.navTo('pacUdc',{
			query: oArguments
		});
	},
	
/***********************************************************************************************/
// Caricamento Buoni
/***********************************************************************************************/

	loadGoods: function(sUdcNr) {
		var oGoodsModel = this.getView().getModel('goods');
		var bInit = false;
		if(!oGoodsModel)
		{
			bInit = true;
			oGoodsModel = new sap.ui.model.xml.XMLModel();
		}
		
		var sGoodsQuery = QService + sMovementPath + "PAC/getDudcDetSQ";
		sGoodsQuery += "&Param.1=" + sUdcNr;
		oGoodsModel.loadData(sGoodsQuery,false,false);
		
		if(bInit)
			this.getView().setModel(oGoodsModel,'goods');
	},
	
/***********************************************************************************************/
// Caricamento Sospesi
/***********************************************************************************************/

	loadSuspended: function(sUdcNr) {
		var oSuspModel = this.getView().getModel('susp');
		var bInit = false;
		if(!oSuspModel)
		{
			bInit = true;
			oSuspModel = new sap.ui.model.xml.XMLModel();
		}
		
		var sSuspQuery = QService + sMovementPath + "PAC/getDdetScrapSQ";
		sSuspQuery += "&Param.1=" + sUdcNr;
		oSuspModel.loadData(sSuspQuery,false,false);
		
		if(bInit)
			this.getView().setModel(oSuspModel,'susp');
	},
	
/***********************************************************************************************/
// Numero di Row nel Model
/***********************************************************************************************/
	
	getRowCount: function(sModelName,sFieldQtyName) {
		var oView = sap.ui.getCore().byId('pacDmView');
		var oModel = oView.getModel(sModelName);
		var oRowsetObject = oModel.getObject('/Rowset/0/');
		if(oRowsetObject && oRowsetObject.childNodes.length>0)
		{
			const iRowCount = oRowsetObject.childNodes.length - 1;
			if(!sFieldQtyName)
				return iRowCount;
			else
			{
				var iCount = 0;
				for(var i=0; i<iRowCount; i++)
					iCount += parseInt(oModel.getProperty('/Rowset/0/Row/' + i + '/' + sFieldQtyName));
				return iCount;
			}
		}
		else
			return 0;
	},
	
/***********************************************************************************************/
// Numero di Row nel Model
/***********************************************************************************************/

	onSelectSuspUdc: function() {
		var oView = sap.ui.getCore().byId('pacDmView');
		var oController = oView.getController();
		
		var nextFunction = oController.setSuspUdc;
		
		if(this.isNew)
			nextFunction();
		else
		{
			var oUdcSuspModel = oController.getSuspUdcModel();
			oController.selectSuspDialog(oUdcSuspModel,nextFunction);
		}
	},
	
	setSuspUdc: function(oUdcSelected) {
		var sSuspUdc = (oUdcSelected) ? oUdcSelected.UDCNR : '';
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		if(oUdcObject.UDCSUSP !== sSuspUdc)
		{
			oUdcObject.UDCSUSP = sSuspUdc;
			oSessionStorage.put('FUNCDATA',oUdcObject);
			
			var oView = sap.ui.getCore().byId('pacDmView');
			var oViewModel = oView.getModel();
			oViewModel.setData(oUdcObject);
		}
		
		oView.getController().loadSuspended(sSuspUdc);
	},
	
/***********************************************************************************************/
// Elaborazione dei dati inseriti DataMatrix o Causale
/***********************************************************************************************/

	getSuspUdcModel: function() {
		
		var oView = sap.ui.getCore().byId('pacDmView');
		var oViewModel = oView.getModel();
		
		var sQuery = QService + sMovementPath + "UDC/getUDCMovByOriginSQ";
		sQuery += "&Param.1=<> 'C' AND DUDCMAST.UDCSTAT <> ";
		sQuery += "&Param.2=E";
		sQuery += "&Param.3=" + oViewModel.getProperty("/ORDER");
		sQuery += "&Param.4=" + oViewModel.getProperty("/POPER");
		sQuery += "&Param.5==";
		sQuery += "&Param.6=C"; // Sospeso
	
		var oModel = new sap.ui.model.xml.XMLModel();
		oModel.loadData(sQuery,false,false);
		return oModel;		
	},
	
	processDmCausal: function() {
		
		var oPacDmCausalInput = sap.ui.getCore().byId("pacDmCausalInput");
		var sDMCausal = oPacDmCausalInput.getValue();
		if(!sDMCausal)
		{
			sap.m.MessageBox.show(oLng_Opr.getText("PacDm_DMCausalNotValidErrorMsg"), {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: oLng_Opr.getText("General_WarningText"),
					actions: sap.m.MessageBox.Action.OK
				});
			return;
		}
			
		sap.ui.core.BusyIndicator.show(1000);
		
		var oView = sap.ui.getCore().byId('pacDmView');
		var oViewModel = oView.getModel();
		var oController = oView.getController();
		const oArguments = oController._oArguments;
		
		var sCheckQuery = sMasterDataPath + "ScrabReasons/checkScrabReasonSQ";
		sCheckQuery += "&Param.1=" + oArguments.plant;
		sCheckQuery += "&Param.2=" + sDMCausal;
		const oResults = fnGetAjaxVal(sCheckQuery,["checked"],false);
		if(oResults.checked === '1') //Modalità inserimento Causale
		{
			const oLastDM = oController._oLastDM;
			if(oLastDM)
			{							
				var nextFunction = oController.suspendDatamatrix;
				var oCallbackParams = {
					arguments: oArguments,
					lastDm: oLastDM,
					sreasid: sDMCausal,
				};
				
				const sSuspUdc = oViewModel.getProperty("/UDCSUSP");
				if(!sSuspUdc)
				{				
					var oUdcSuspModel = oController.getSuspUdcModel();
					
					var oRowsetObject = oUdcSuspModel.getObject("/Rowset/0/");
					if(oRowsetObject && oRowsetObject.childNodes.length-1>0) //numero di UDC nel modello
					{
						const sExistingTxt = oLng_Opr.getText('PacDm_SelectUdcExisting');
						sap.m.MessageBox.show(oLng_Opr.getText('PacDm_SelectUdcMessage'), {
							icon: sap.m.MessageBox.Icon.WARNING,
							title: oLng_Opr.getText('General_WarningText'),
							actions: [
								sExistingTxt,
								oLng_Opr.getText('PacDm_SelectUdcNew')
							],
							onClose: function(oAction) {
								if (oAction === sExistingTxt)
									oController.selectSuspDialog(oUdcSuspModel,nextFunction,oCallbackParams);
								else
									nextFunction(undefined,oCallbackParams);
							}
						});
					}
					else
						nextFunction(undefined,oCallbackParams);
				}
				else
					nextFunction({UDCNR:sSuspUdc},oCallbackParams);
			}
			else
			{
				sap.m.MessageBox.show(oLng_Opr.getText("PacDm_NoDMCausalErrorMsg"), {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: oLng_Opr.getText("General_WarningText"),
					actions: sap.m.MessageBox.Action.OK
				});
			}
		}
		else //Modalità inserimento DataMatrix
		{
			var sErrorMessage;
			const iTotalQty = parseInt(oViewModel.getProperty("/PZUDC"));
			const iSubQty = parseInt(oController.getRowCount('goods','QTY'));
			if(iSubQty >= iTotalQty)
				sErrorMessage = oLng_Opr.getText("PacDm_CompletedErrorMsg");
			else if(sDMCausal.length < 6)
				sErrorMessage = oLng_Opr.getText("PacDm_DMLengthErrorMsg");
			
			if(sErrorMessage)
			{
				sap.m.MessageBox.show(sErrorMessage, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: oLng_Opr.getText("General_WarningText"),
					actions: sap.m.MessageBox.Action.OK
				});
			}
			else
			{	
				const sUdcNr = oArguments.udcnr;
				
				var sAddQuery = sMovementPath + "PAC/addDatamatrixQR";
				sAddQuery += "&Param.1=" + oArguments.pernr;
				sAddQuery += "&Param.2=" + oAppController._sDate;
				sAddQuery += "&Param.3=" + oAppController._sShift;
				sAddQuery += "&Param.4=" + sUdcNr;
				sAddQuery += "&Param.5=" + sDMCausal;
				sAddQuery += "&Param.6=" + sLanguage;
				
				var successFunction = function(oResults) {
					oController.loadGoods(sUdcNr); // Ricarica i buoni
					
					oController._oLastDM = {
						datamatrix: sDMCausal,
						udcprog: oResults.udcprog
					};
				};
				
				oAppController.handleRequest(sAddQuery,{
					onSuccess: successFunction,
					results: ['udcprog'],
				});
			}
		}
		
		oPacDmCausalInput.setValue();
		oPacDmCausalInput.focus();
			
		sap.ui.core.BusyIndicator.hide();
	},
	
	selectSuspDialog: function(oModel,nextFunction,oCallbackParams) {
		oAppController.selectFromDialog({
			title: oLng_Opr.getText('PacDm_SelectUdcTitle'),
			noDataText: oLng_Opr.getText('PacDm_SelectUdcTitle'),
			callback: nextFunction,
			callbackParams: oCallbackParams,
			model: oModel,
			modelPath: "/Rowset/0/Row",
			modelTitle: 'UDCNR',
			modelDesc: 'MATERIAL',
			template: oAppController.customListTemplate({
				titleText: "{UDCNR}",
				bottomLeftText: "{MOV_IDLINE}",
				topRightText: {
					parts: [
						{path: "QTYRES" }
					],		     
					formatter: function(sQtyRes) {
						return oLng_Opr.getText('PacDm_Qty') + ": " + sQtyRes;
					}
				},
				bottomRightText: {
					parts: [
						{path: "TIMEID" }
					],		     
					formatter: dateFormatter
				}
			})
		});
	},
	
	suspendDatamatrix: function(oUdcSelected,oCallbackParams) {
		
		const sSuspUdc = (oUdcSelected) ? oUdcSelected.UDCNR : '';
		
		var oView = sap.ui.getCore().byId('pacDmView');
		var oViewModel = oView.getModel();
		var oController = oView.getController();
		
		const sUdcNr = oCallbackParams.arguments.udcnr;
		
		var sSuspQuery = sMovementPath + "PAC/suspDatamatrixQR";
		sSuspQuery += "&Param.1=" + oCallbackParams.arguments.plant;
		sSuspQuery += "&Param.2=" + sUdcNr;
		sSuspQuery += "&Param.3=" + oCallbackParams.lastDm.udcprog;
		sSuspQuery += "&Param.4=" + sSuspUdc;
		sSuspQuery += "&Param.5=" + oAppController._sShift;
		sSuspQuery += "&Param.6=" + oAppController._sDate;
		sSuspQuery += "&Param.7=" + oCallbackParams.arguments.pernr;
		sSuspQuery += "&Param.8=" + oViewModel.getProperty("/ORDER");
		sSuspQuery += "&Param.9=" + oViewModel.getProperty("/POPER");
		sSuspQuery += "&Param.10=" + oViewModel.getProperty("/MATERIAL");
		sSuspQuery += "&Param.11=" + oViewModel.getProperty("/IDLINE");
		sSuspQuery += "&Param.12=" + "SO";
		sSuspQuery += "&Param.13=" + oCallbackParams.lastDm.datamatrix;
		sSuspQuery += "&Param.14=" + "1";
		sSuspQuery += "&Param.15=" + oCallbackParams.sreasid;
		sSuspQuery += "&Param.16=" + "";
		sSuspQuery += "&Param.17=" + "";
		sSuspQuery += "&Param.18=" + "";
		sSuspQuery += "&Param.19=" + "";
		sSuspQuery += "&Param.20=" + "";
		sSuspQuery += "&Param.21=" + "";
		sSuspQuery += "&Param.22=" + getDatetimeString(new Date());
		sSuspQuery += "&Param.23=" + sLanguage;
		
		var successFunction = function(oResults) {
			
			oController.loadGoods(sUdcNr); // Ricarica i buoni e sospesi
			oController.setSuspUdc({UDCNR:oResults.outputUdc});
			
			oController._oLastDM = undefined;
		};
		
		oAppController.handleRequest(sSuspQuery,{
			onSuccess: successFunction,
			results: ['outputUdc'],
		});
	},
	
/***********************************************************************************************/
// Pausa dell'UDC
/***********************************************************************************************/
	
	onPause: function() {
		sap.m.MessageBox.show(oLng_Opr.getText("PacDm_PauseMsg"), {
			icon: sap.m.MessageBox.Icon.WARNING,
			title: "Attenzione",
			actions: [sap.m.MessageBox.Action.YES,sap.m.MessageBox.Action.NO],
			onClose: function(oAction) {
				if (oAction === sap.m.MessageBox.Action.YES)
				{
					var oController = sap.ui.getCore().byId('pacDmView').getController();
					oController.confirmPacUdc(false);
				}
			}
		});
	},
	
/***********************************************************************************************/
// Completamento dell'UDC
/***********************************************************************************************/
	
	onComplete: function() {
		var oView = sap.ui.getCore().byId('pacDmView');
		var oController = oView.getController();
		var oViewModel = oView.getModel();
		
		const iSubQty = parseInt(oController.getRowCount('goods','QTY'));
		if(iSubQty > 0)
		{
			const iTotalQty = parseInt(oViewModel.getProperty("/PZUDC"));
			var sQuestion;
			if(iSubQty >= iTotalQty)
				sQuestion = oLng_Opr.getText("PacDm_CompleteQuestion");
			else
				sQuestion = oLng_Opr.getText("PacDm_IncompleteQuestion");
			
			sap.m.MessageBox.show(sQuestion, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: [sap.m.MessageBox.Action.YES,sap.m.MessageBox.Action.NO],
				onClose: function(oAction) {
					if (oAction === sap.m.MessageBox.Action.YES)					
						oController.confirmPacUdc(true);
				}
			});
		}
		else
		{
			sap.m.MessageBox.show(oLng_Opr.getText("PacDm_EmptyErrorMsg"), {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
	},
	
	confirmPacUdc: function(bCompleted) {
		var oController = sap.ui.getCore().byId('pacDmView').getController();
		const sUdcNr = oController._oArguments.udcnr;
		
		var sCompleteQuery = sMovementPath + "PAC/saveUDCForPACQR";
		sCompleteQuery += "&Param.1=" + sUdcNr;
		sCompleteQuery += "&Param.2=" + (bCompleted ? '1':'0');
		sCompleteQuery += "&Param.2=" + sLanguage;
		
		var successFunction = function(oResults) {
			oController.onNavBack();
		};
		
		oAppController.handleRequest(sCompleteQuery,{
			onSuccess: successFunction
		});
	},
	
/***********************************************************************************************/
// Dialog delle dichiarazioni effettuate sull'UDC
/***********************************************************************************************/
	
	onShowDeclDialog: function() {

		const bGoodMode = this.goods;
		var oView = sap.ui.getCore().byId('pacDmView');
		var oModel = oView.getModel(bGoodMode ? 'goods' : 'susp');
		
		var oTemplate = new sap.m.ColumnListItem({
			cells: [
				new sap.m.Text({
					text: "{SERIAL}"
				})
			]
		});
		
		var oTable = new sap.m.Table({
			mode: sap.m.ListMode.None,
			noDataText: oLng_Opr.getText(bGoodMode ? 'PacDm_NoGoods' : 'PacDm_NoSuspended'),
			columns: [
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("PacDm_DataMatrix")})
				})
			]
		});
		
		if(!bGoodMode) // sospesi
		{
			oTemplate.addCell(
				new sap.m.Text({
					text: "{SREASID}"
				})
			);
			
			oTable.addColumn(
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("PacDm_Causal")
					})
				})
			);
		}
		
		oTable.bindAggregation("items", "/Rowset/Row", oTemplate);

		var oController = sap.ui.getCore().byId('pacDmView').getController();
		var iCount = oController.getRowCount(bGoodMode ? 'goods' : 'susp',bGoodMode ? 'QTY' : '');
		
		var sTitle = oLng_Opr.getText(bGoodMode ? 'PacDm_Goods' : 'PacDm_Suspended');
		sTitle += ' (' + iCount + ')';
		
		var oDeclDialog = new sap.m.Dialog({
			id: 'pacDmDialog',
			title: sTitle,
			contentWidth: "400px",
			contentHeight: "400px",
			content: [
				oTable
			],
			beginButton: new sap.m.Button({
				text: oLng_Opr.getText("PacDm_Close"),
				press: function () {
					oDeclDialog.close();
				}
			}),
			afterClose: function() {
				oDeclDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");
		
		oView.addDependent(oDeclDialog);

		oDeclDialog.setModel(oModel);
		oDeclDialog.open();
	}

});