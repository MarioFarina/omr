// Controller Gestione fermi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.controller("ui5app.STOPSLINE.stopsLine", {

	_oArguments: undefined,
	_oLinesData: undefined,
	_sCurrentLine: '',
	_sCurrentLineDescr: '',
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("stopsLine").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']);
		this._oArguments = oArguments;

		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		var oStopsModel = new sap.ui.model.xml.XMLModel();
		var oTable = sap.ui.getCore().byId('stopsLineTable');
		oTable.setModel(oStopsModel);

		var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		var oSettings = oStorage.get("WORKST_INFO");
		var fCallback,oLineModel,sModelTitle,sModelDesc;
		if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES && oSettings.LINES.length > 0)
		{
			oLineModel = new sap.ui.model.json.JSONModel(oSettings.LINES);
			sModelTitle = 'lineid';
			sModelDesc = 'linetxt';
		}
		else
		{
			oLineModel = new sap.ui.model.xml.XMLModel();
			oLineModel.loadData(QService + sMasterDataPath + "Lines/getLinesbyPlantSQ&Param.1=" + sPlant,false,false);
			sModelTitle = 'IDLINE';
			sModelDesc = 'LINETXT';
		}

		this._oLinesData = {
			model: oLineModel,
			modelTitle: sModelTitle,
			modelDesc: sModelDesc
		};

		this.onSelectLine();

		oAppController.loadSubHeaderInfo($.UIbyID('stopsLinePage'),sPlant,sPernr);
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('stopsLineView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},

/***********************************************************************************************/
// Apre la dialog di selezione della linea
/***********************************************************************************************/

	onSelectLine: function() {
		var oController = sap.ui.getCore().byId('stopsLineView').getController();
		var oLinesData = oController._oLinesData;

		var selectFunc = function() {
			sap.ui.core.BusyIndicator.hide();
			oAppController.selectFromDialog({
				title: oLng_Opr.getText("General_SelectLine"),
				noDataText: oLng_Opr.getText("General_NoLine"),
				callback: function(oSelectedLine) {
					const sSelectedLine = oSelectedLine[oLinesData.modelTitle];
					const sSelectedDescription = oSelectedLine[oLinesData.modelDesc];
					oController.loadStops(sSelectedLine, sSelectedDescription);
				},
				model: oLinesData.model,
				modelTitle: oLinesData.modelTitle,
				modelDesc: oLinesData.modelDesc
			});
		};

		if(this.change)
			selectFunc();
		else
		{
			sap.ui.core.BusyIndicator.show();
			jQuery.sap.delayedCall(700, null, selectFunc);
		}
	},

/***********************************************************************************************/
// Caricamento dei fermi per linea
/***********************************************************************************************/

	loadStops: function(sSelectedLine, sSelectedDescription) {
		var oController = sap.ui.getCore().byId('stopsLineView').getController();
		oController._sCurrentLine = sSelectedLine;
		oController._sCurrentLineDescr = sSelectedDescription;

		var oTable = sap.ui.getCore().byId('stopsLineTable');
		var oStopsModel = oTable.getModel();

		var toolbarLineText = sap.ui.getCore().byId('toolbarLineText');
		toolbarLineText.setText(sSelectedLine + " " + sSelectedDescription);

		var sLoadQuery = QService + sProductionPath + 'Stops/getStopsByLineQR';
		sLoadQuery += '&Param.1=' + oController._oArguments.plant;
		sLoadQuery += '&Param.2=' + sSelectedLine;
        sLoadQuery += '&Param.3=' + oAppController._sDate;
        sLoadQuery += '&Param.4=' + oAppController._sShift;
		oStopsModel.loadData(sLoadQuery);

	},

/***********************************************************************************************/
// Aggiunge/modifica un fermo
/***********************************************************************************************/

	onStopPressed: function(oEvent) {
		const bEdit = this.edit;

		var oController = sap.ui.getCore().byId('stopsLineView').getController();
		const oArguments = oController._oArguments;
        const sPernr = oArguments.pernr;

		var oScrapReasCombo = new sap.m.ComboBox({
			width : "100%",
			change: function () {
				const sScrapReas = oScrapReasCombo.getValue();
				var aScrapItems = oScrapReasCombo.getItems();
				for(var i=0; i<aScrapItems.length; i++)
				{
					var oCurrentItem = aScrapItems[i];
					if(oCurrentItem.getKey() === sScrapReas)
					{
						oScrapReasCombo.setSelectedItem(oCurrentItem);
						break;
					}
				}
			}
		});
		var oScrapReasModel = new sap.ui.model.xml.XMLModel();
		oScrapReasCombo.setModel(oScrapReasModel);

		var oScrapReasTemplate = new sap.ui.core.Item({
			key : '{STOPID}'
		}).bindProperty("text", {
			parts: [
				{path: 'STOPID'},
				{path: 'REASTXT'}
			],
			formatter: function(sReasId,sReasTxt){
				return sReasId + ' - ' + sReasTxt;
			}
		});
		oScrapReasCombo.bindAggregation("items","/Rowset/Row",oScrapReasTemplate);

		var oGrstReasModel = new sap.ui.model.xml.XMLModel();
		var sGrstQuery = QService + sMasterDataPath + "Stopreas/getStopReasonsGroupsSQ";
		sGrstQuery += "&Param.1=" + oArguments.plant;
		oGrstReasModel.loadData(sGrstQuery,false,false);

        var oGrstReasSgmBtn = new sap.m.SegmentedButton();
        var rowsetObject = oGrstReasModel.getObject("/Rowset/0");
        var oRows = $(rowsetObject).find('Row');
        var sFirstKey = oRows[0].childNodes[1].textContent;
        for (var i = 0; i < oRows.length; i++) {
            var oCurrentBtn = oGrstReasSgmBtn.createButton(oRows[i].childNodes[1].textContent,null,true);
            oCurrentBtn. attachPress(function () {
                oScrapReasCombo.clearSelection();
                oScrapReasCombo.setValue();
                var sScrapQuery = QService + sMasterDataPath + "Stopreas/getStopReasonsByGroupSQ"
                sScrapQuery += "&Param.1=" + oArguments.plant;
                sScrapQuery += "&Param.2=" + this.getText();
                oScrapReasModel.loadData(sScrapQuery,false,false);
                var aItems = oScrapReasCombo.getItems();
                if(aItems.length === 1)
                    oScrapReasCombo.setSelectedItem(aItems[0]);
                sap.m.MessageToast.show(this.extendedText, {
                    duration: 3000,
                    animationDuration: 1000,
                    closeOnBrowserNavigation: false
                });
            });
            Object.defineProperty(oCurrentBtn, 'idKey', {
                value: oRows[i].childNodes[1].textContent,
                writable: false
            });
            Object.defineProperty(oCurrentBtn, 'extendedText', {
                value: oRows[i].childNodes[1].textContent + " - " + oRows[i].childNodes[2].textContent,
                writable: false
            });
            oCurrentBtn.setTooltip(oRows[i].childNodes[1].textContent + " - " + oRows[i].childNodes[2].textContent);
        }

		var oMachineCombo = new sap.m.ComboBox();
		var oMachineModel = new sap.ui.model.xml.XMLModel();
		oMachineCombo.setModel(oMachineModel);

		var oMachineTemplate = new sap.ui.core.Item({
			key : '{IDMACH}'
		}).bindProperty("text", {
			parts: [
				{path: 'IDMACH'},
				{path: 'MACHTXT'}
			],
			formatter: function(sReasId,sReasTxt){
				return sReasId + ' - ' + sReasTxt;
			}
		});
		oMachineCombo.bindAggregation("items","/Rowset/Row",oMachineTemplate);

		var sMachineQuery = QService + sMasterDataPath + "Machines/getMachinesByLineSQ";
		sMachineQuery += "&Param.1=" + oController._sCurrentLine;
		oMachineModel.loadData(sMachineQuery,false,false);

		var oNoteInput = new sap.m.Input();

		var oStartDateControl;
		var oEndDateControl;
		var oSelectedContext;
		if(bEdit)
		{
			oSelectedContext = oEvent.getSource().getSelectedItem().getBindingContext();

			oStartDateControl = new sap.m.Text({
				text: dateFormatter(
					oSelectedContext.getProperty("DATE_FROM")
				)
			});

			const sDateTo = dateFormatter(
				oSelectedContext.getProperty("DATE_TO")
			);
			if(sDateTo !== '-')
			{
				oEndDateControl = new sap.m.Text({
					text: sDateTo
				});
			}

			const sScrapReas = oSelectedContext.getProperty("REASON");
			var sQuery = sMasterDataPath + "ScrabReasons/getScrabReasonBySreasIdSQ";
			sQuery += "&Param.1=" + oArguments.plant;
			sQuery += "&Param.2=" + sScrapReas;
			const sSctype = fnGetAjaxVal(sQuery,'SCTYPE',false);

            var oSelectedGRreasButton;
            var oGRreasButtons = oGrstReasSgmBtn.getButtons();
            for (var i = 0; i < oGRreasButtons.length; i++){
                if(oGRreasButtons[i].idKey === oSelectedContext.getProperty("STTYPE")) {
                    oSelectedGRreasButton = oGRreasButtons[i];
                }
            }
			oGrstReasSgmBtn.setSelectedButton(oSelectedGRreasButton);
            oScrapReasCombo.clearSelection();
            oScrapReasCombo.setValue();
            var sScrapQuery = QService + sMasterDataPath + "Stopreas/getStopReasonsByGroupSQ";
            sScrapQuery += "&Param.1=" + oArguments.plant;
            sScrapQuery += "&Param.2=" + (oSelectedGRreasButton !== undefined? oSelectedGRreasButton.idKey : sFirstKey);
            oScrapReasModel.loadData(sScrapQuery,false,false);
			oScrapReasCombo.setValue(sScrapReas);
            oScrapReasCombo.fireChange();

			oMachineCombo.setSelectedKey(oSelectedContext.getProperty('IDMACH'));
			oNoteInput.setValue(oSelectedContext.getProperty('NOTE'));
		}
		else
		{
			oStartDateControl = new sap.m.DateTimeInput({
				displayFormat: "dd/MM/yyyy HH:mm"/*"dd/MM/yyyy HH:mm:ss"*/,
				valueFormat: "yyyy-MM-ddTHH:mm"/*"yyyy-MM-ddTHH:mm:ss"*/,
				type: sap.m.DateTimeInputType.DateTime,
				dateValue: new Date()
			});
		}

		var oEndDateBox;
		if(!oEndDateControl)
		{
			oEndDateControl = new sap.m.DateTimeInput({
				displayFormat: "dd/MM/yyyy HH:mm"/*"dd/MM/yyyy HH:mm:ss"*/,
				valueFormat: "yyyy-MM-ddTHH:mm"/*"yyyy-MM-ddTHH:mm:ss"*/,
				type: sap.m.DateTimeInputType.DateTime,
				layoutData : new sap.m.FlexItemData({
					growFactor: 1
				})
			});

			oEndDateBox = new sap.m.HBox({
				items: [
					oEndDateControl,
					new sap.m.Button({
						icon: "sap-icon://decline",
						press: function() {
							oEndDateControl.setValue()
						}
					})
				]
			});
		}

		var oStopDialog = new sap.m.Dialog({
			title: oLng_Opr.getText((bEdit) ? 'StopsLine_EditStop' : 'StopsLine_NewStop') + " - " + oLng_Opr.getText('StopsLine_Line') + ": " + oController._sCurrentLine,
			contentWidth: '800px',
			content: new sap.ui.layout.form.SimpleForm({
				editable: true,
				layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
				labelSpanL: 3,
				labelSpanM: 3,
				labelSpanS: 3,
				emptySpanL: 0,
				emptySpanM: 0,
				emptySpanS: 0,
				columnsL: 1,
				columnsM: 1,
				columnsS: 1,
				content: [
					new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_Start"),
					}),
					oStartDateControl,
					new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_End"),
					}),
					(oEndDateBox) ? oEndDateBox : oEndDateControl,
					new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_GrscReas"),
					}),
                    oGrstReasSgmBtn,
                    new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_ScrapReas"),
					}),
					oScrapReasCombo,
					new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_MachineStation"),
					}),
					oMachineCombo,
					new sap.m.Label({
						text: oLng_Opr.getText("StopsLine_Note"),
					}),
					oNoteInput
				]
			}),
			beginButton: new sap.m.Button({
				text:  oLng_Opr.getText("StopsLine_Save"),
				icon: "sap-icon://save",
				press: function () {

					var sIdLine, sStartDate, sCurrentEndDate, sEndDate, iFlMan, sDateShift, sShift, iSlowTime;
					if(bEdit === true)
					{
						sIdLine = oSelectedContext.getProperty("IDLINE");
						sStartDate = oSelectedContext.getProperty("DATE_FROM");
                        iFlMan = parseInt(oSelectedContext.getProperty("FL_MAN"));
                        sDateShift = oSelectedContext.getProperty("DATE_SHIFT").substring(0, 10);
                        sShift = oSelectedContext.getProperty("SHIFT");
                        iSlowTime = parseInt(oSelectedContext.getProperty("SLOWTIME"));
						sCurrentEndDate = oSelectedContext.getProperty("DATE_TO");
                        sEndDate = '';
						if(!Date.parse(sCurrentEndDate)){
							sEndDate = (oEndDateControl.getValue() === "" ? "" : oEndDateControl.getValue() + ":00");
                        } else {
                            sEndDate = sCurrentEndDate;
                        }

                        var sReason = oSelectedContext.getProperty("REASON");
                        var sOper = oSelectedContext.getProperty("OPEID");
                        if(sEndDate !== '' && sReason !== '' && sOper !== sPernr){
                            sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_PernrNotAllowed"), {
                                icon: sap.m.MessageBox.Icon.WARNING,
                                title: oLng_Opr.getText("General_WarningText"),
                                actions: sap.m.MessageBox.Action.OK
                            });
                            return;
                        }
					}
					else
					{
						sIdLine = oController._sCurrentLine;
                        sDateShift = oAppController._sDate;
                        sShift = oAppController._sShift;
						sStartDate = oStartDateControl.getValue() + ":00";
                        iFlMan = 1;
                        iSlowTime = 5;
						if(!sStartDate)
						{
							oStartDatePicker.setValueState(sap.ui.core.ValueState.Error);
							return;
						}
						sEndDate = (oEndDateControl.getValue() === "" ? "" : oEndDateControl.getValue() + ":00");
					}

                    if(!Date.parse(sStartDate)){
                        sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_ErrorDateFormat"), {
                            icon: sap.m.MessageBox.Icon.WARNING,
                            title: oLng_Opr.getText("General_WarningText"),
                            actions: sap.m.MessageBox.Action.OK
                        });
                        return;
                    }

                    if(!Date.parse(sEndDate) && sEndDate !== ""){
                        sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_ErrorDateFormat"), {
                            icon: sap.m.MessageBox.Icon.WARNING,
                            title: oLng_Opr.getText("General_WarningText"),
                            actions: sap.m.MessageBox.Action.OK
                        });
                        return;
                    }

                    var sStartShift, sEndShift, dToday, iDateDiff;
                    dToday = new Date();

                    switch(oAppController._sShift) {
                        case "T1": {
                            sStartShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 06:00:00";
                            sEndShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 14:00:00";
                        }
                            break;
                        case "T2": {
                            sStartShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 14:00:00";
                            sEndShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 22:00:00";
                        }
                            break;
                        case "T3": {
                            var dYesterday = new Date();
                            dYesterday.setDate(dToday.getDate() - 1);
                            var dTomorrow = new Date();
                            dTomorrow.setDate(dToday.getDate() + 1);

                            if(dToday.getHours() > 6) {
                                sStartShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 22:00:00";
                                sEndShift = dTomorrow.getFullYear() + "-" + (dTomorrow.getMonth() + 1) + "-" + dTomorrow.getDate() + " 06:00:00";
                            } else {
                                sStartShift = dYesterday.getFullYear() + "-" + (dYesterday.getMonth() + 1) + "-" + dYesterday.getDate() + " 22:00:00";
                                sEndShift = dToday.getFullYear() + "-" + (dToday.getMonth() + 1) + "-" + dToday.getDate() + " 06:00:00";
                            }
                        }
                            break;
                        default:
                            break;
                    }

                    if(!bEdit && (new Date(sStartDate) < new Date(sStartShift) || new Date(sStartDate) >= new Date(sEndShift))){
                        sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_ErrorShiftDateTime"), {
                            icon: sap.m.MessageBox.Icon.WARNING,
                            title: oLng_Opr.getText("General_WarningText"),
                            actions: sap.m.MessageBox.Action.OK
                        });
                        return;
                    }

                    if(new Date(sStartDate) >= new Date(sEndDate) && sEndDate !== "") {
                        sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_ErrorDate"), {
                            icon: sap.m.MessageBox.Icon.WARNING,
                            title: oLng_Opr.getText("General_WarningText"),
                            actions: sap.m.MessageBox.Action.OK
                        });
                        return;
                    }

                    iDateDiff = parseInt(((new Date(sEndDate) - new Date(sStartDate)) / (1000*60*60*24)));
                    if (iDateDiff >= 7 && sEndDate !== "") {
                        sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_ErrorRangeDate"), {
                            icon: sap.m.MessageBox.Icon.WARNING,
                            title: oLng_Opr.getText("General_WarningText"),
                            actions: sap.m.MessageBox.Action.OK
                        });
                        return;
                    }

                    var fContinueFunc = function () {
                        if (sDateShift !== oAppController._sDate || sShift !== oAppController._sShift) {
                            if (sEndDate !== "" && oScrapReasCombo.getSelectedKey() !== "") {
                                sap.m.MessageToast.show(oLng_Opr.getText("StopsLine_StopNotShowedAgain"), {
                                    duration: 3000,
                                    animationDuration: 1000,
                                    closeOnBrowserNavigation: false
                                });
                            }
                        }

                        var sRequest = sProductionPath + "Stops/saveStopQR";
                        sRequest += "&Param.1=" + sIdLine; //IDLINE
                        sRequest += "&Param.2=" + sStartDate.replace("T", " "); //DATE_FROM
                        sRequest += "&Param.3=" + sDateShift; //DATE_SHIFT
                        sRequest += "&Param.4=" + sShift; //SHIFT
                        sRequest += "&Param.5=" + sEndDate.replace("T", " "); //DATE_TO
                        sRequest += "&Param.6=" + oScrapReasCombo.getSelectedKey(); //REASON
                        sRequest += "&Param.7=" + ""; //STATE
                        sRequest += "&Param.8=" + ""; //LASTEVT
                        sRequest += "&Param.9=" + oMachineCombo.getSelectedKey(); //IDMACH
                        sRequest += "&Param.10=" + sPernr; //OPEID
                        sRequest += "&Param.11=" + oNoteInput.getValue(); //NOTE
                        sRequest += "&Param.12=" + "it"; //LANGUAGE
                        sRequest += "&Param.13=" + iFlMan; //FL_MAN
                        sRequest += "&Param.14=" + (bEdit ? '1' : '0'); //UPDATE
                        sRequest += "&Param.15=" + "0"; //flag che indica che la chiamata avviene da back-office (per il recupero dell'utente loggato)

                        oAppController.handleRequest(sRequest,{
                            showSuccess: false,
                            onSuccess: function() {
                                oController.loadStops(sIdLine, oController._sCurrentLineDescr);
                                oStopDialog.close();
                            }
                        });
                    }

                    if (sEndDate !== "") {
                        iSlowTime = iSlowTime * 60;
                        var iSecondDiff = parseInt((new Date(sEndDate) - new Date(sStartDate)) / 1000);
                        if (iSecondDiff < iSlowTime) {
                            sap.m.MessageBox.show(oLng_Opr.getText("StopsLine_AddingMicroStop"), {
                                icon: sap.m.MessageBox.Icon.WARNING,
                                title: oLng_Opr.getText("General_WarningText"),
                                actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                                onClose: function(oAction){
                                    if(oAction === sap.m.MessageBox.Action.YES)
                                        fContinueFunc();
                                }
                            });
                        } else {
                            fContinueFunc();
                        }
                    } else {
                        fContinueFunc();
                    }
				}
			}),
			endButton: new sap.m.Button({
				text: oLng_Opr.getText("StopsLine_Cancel"),
				icon: "sap-icon://decline",
				press: function() {
					oStopDialog.close();
				}
			}),
			afterClose: function() {
				oStopDialog.destroy();
			}
		}).addStyleClass("noPaddingDialog");

		oStopDialog.open();
	},

});
