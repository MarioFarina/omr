// View Gestione fermi
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.STOPSLINE.stopsLine", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.STOPSLINE.stopsLine";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oTable = new sap.m.Table({
			id: 'stopsLineTable',
			mode: sap.m.ListMode.SingleSelectMaster,
			itemPress: [oController.onStopPressed,{edit:true}],
			columns: [
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("StopsLine_Line")
					}),
					width: "220px"
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("StopsLine_Start")
					}),
					width: "180px"
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("StopsLine_End")
					}),
					width: "180px"
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: oLng_Opr.getText("StopsLine_Reason")
					}),
					//width: "15%"
				})
			]
		}).addStyleClass('mediumSizeTblCell');;
		
        var oTemplate = new sap.m.ColumnListItem({
			type : sap.m.ListType.Active,
			cells: [
				new sap.m.Text({
					maxLines: 1,
					text: {
						parts: [
							{path: "IDLINE"},
							{path: "LINETXT"}
						],
						formatter: function (sIdLine, sLineTxt) {
							return sIdLine + ' ' + sLineTxt
						}
					},
				}),
		        new sap.m.Text({
					text: {
						parts: [
							{path: "DATE_FROM"}
						],
						formatter: dateFormatter
					}
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "DATE_TO"}
						],
						formatter: dateFormatter
					}
				}),
				new sap.m.Text({
					text: {
						parts: [
							{path: "REASON"},
							{path: "REASTXT"}
						],
						formatter: function (sReason, sReasTxt) {
							return sReason + ' ' + sReasTxt
						}
					}
				})
			]
		});
        oTable.bindAggregation("items","/Rowset/Row",oTemplate);
		
		var oPage = new sap.m.Page({
			id: "stopsLinePage",
			//enableScrolling: false,
			title: oLng_Opr.getText("StopsLine_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oTable
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.Button({
						icon: "sap-icon://value-help",
						text: oLng_Opr.getText("StopsLine_Line"),
						press: [oController.onSelectLine,{change:true}]
					}).addStyleClass("yellowButton noBorderButton"),
					new sap.m.Text({
						id: "toolbarLineText",
						text: ""
					}),				
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://add",
						text: oLng_Opr.getText("StopsLine_New"),
						press: [oController.onStopPressed,{edit:false}]
					}).addStyleClass("greenButton noBorderButton")
				]
			})
		});
		
	  	return oPage;
	}

});