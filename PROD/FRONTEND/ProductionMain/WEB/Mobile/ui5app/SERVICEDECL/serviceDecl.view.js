// View Dichiarazione Servizio
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

sap.ui.jsview("ui5app.SERVICEDECL.serviceDecl", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.SERVICEDECL.serviceDecl";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {
		
		var oGridForm = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 0.5,
			content: [
				new sap.m.Label({
					text: oLng_Opr.getText("ServiceDecl_Order") + ':',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L3 M3 S12"
					})
				}).addStyleClass("inputLabel"),
				new sap.m.HBox({
					items: [
						new sap.m.Input({
							id: 'serviceDeclInput',
							type: sap.m.InputType.Number,
							layoutData: [
								 new sap.m.FlexItemData({
									 growFactor: 1
								 })
							]
						}),
						new sap.m.Button({
							icon: "sap-icon://decline",
							press: function() {
								$.UIbyID('serviceDeclInput').setValue();
							}
						})
					],
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S12"
					})
				})
			]
		}).addStyleClass("sapUiMediumMarginTop");
		
		var oPanel = new sap.m.Panel({
			content: oGridForm
		});
						  
		var oPage = new sap.m.Page({
			id: "serviceDeclPage",
			enableScrolling: false,
			title: oLng_Opr.getText("ServiceDecl_Title"),
			headerContent: [
				new sap.m.Button({
					icon: "sap-icon://home",
					press: oAppController.onLogout
				})
			],
			showNavButton: true,
			navButtonPress: oController.onNavBack,
			content: [
				oPanel,
				new sap.m.HBox({
					justifyContent: sap.m.FlexJustifyContent.Center,
					items: [
						new sap.m.Button({
							text: oLng_Opr.getText("ServiceDecl_Next"),
							icon: "sap-icon://navigation-right-arrow",
							press: oController.onNext
						}).addStyleClass("sapUiSmallMarginBegin greenButton bigButton")
					]
				}).addStyleClass("sapUiMediumMarginTop")
			],
			footer: new sap.m.Toolbar()
		});
		
	  	return oPage;
	}

});