// Controller Dichiarazione Tempo Servizio
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.SERVICEDECL.serviceDeclInput", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("serviceDeclInput").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArgParams = ['plant','pernr','order','poper'];
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams); 
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo(sap.ui.getCore().byId('serviceDeclInputPage'),sPlant,sPernr);
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
		
		this.displayInfo(oUdcObject);
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/
	
	displayInfo: function(oUdcObject) {
		
		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		
		var bDisabledMode = false;
		if(!oUdcObject) // !oUdcObject.hasOwnProperty('UDCNR') || oUdcObject.UDCNR !== oArguments.udcnr
		{
			oUdcObject = {};
			bDisabledMode = true;
		}
		
		var oView = sap.ui.getCore().byId('serviceDeclInputView');
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);
		
		$.UIbyID('serviceDeclInputTime').setValue();
		
		$.UIbyID('serviceDeclInputButton').setEnabled(!bDisabledMode);
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('serviceDeclInputView').getController();
		const oArguments = oController._oArguments;
		
		if(oArguments.hasOwnProperty('idline'))
			delete oArguments.idline;
		
		oAppRouter.navTo('serviceDecl',{
			query: oController._oArguments
		});
	},
	
	onConfirm: function() {

		var oTimeInput = $.UIbyID('serviceDeclInputTime');
		
		oTimeInput.setValueState();
		oTimeInput.setValueStateText();

		const sWorkTime = oTimeInput.getValue();
		
		var sErrorMessage;		
		if(isNaN(sWorkTime) || sWorkTime <= 0)
			sErrorMessage = oLng_Opr.getText("ServiceDeclInput_WorkTimeErrorMessage");
		
		if(sErrorMessage)
		{
			oTimeInput.setValueState(sap.ui.core.ValueState.Warning);
			oTimeInput.setValueStateText(sErrorMessage);
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oView = sap.ui.getCore().byId('serviceDeclInputView');
			var oController = oView.getController();
			const oArguments = oController._oArguments;
			
			var sSaveQuery = sMovementPath + "Service/saveServiceDeclQR";
			sSaveQuery += "&Param.1=" + oArguments.plant;
			sSaveQuery += "&Param.2=" + oAppController._sShift;
			sSaveQuery += "&Param.3=" + oAppController._sDate;
			sSaveQuery += "&Param.4=" + oArguments.pernr;
			sSaveQuery += "&Param.5=" + oArguments.order;
			sSaveQuery += "&Param.6=" + oArguments.poper;
			sSaveQuery += "&Param.7=" + sWorkTime;
			sSaveQuery += "&Param.8=" + getDatetimeString(new Date());
			sSaveQuery += "&Param.9=";
			sSaveQuery += "&Param.10=" + sLanguage;

			
			var successFunction = function(oResults) {
				oController.onNavBack();
			};
			
			oAppController.handleRequest(sSaveQuery,{
				onSuccess: successFunction
			});
		}
	}

});