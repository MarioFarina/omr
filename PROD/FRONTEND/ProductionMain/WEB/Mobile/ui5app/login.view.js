// View Login Operatore

sap.ui.jsview("ui5app.login", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	*/ 
	getControllerName : function() {
		return "ui5app.login";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	*/ 
	createContent : function(oController) {

		var oInput = new sap.m.Input({
			id: 'operatorInput',
			placeholder: oLng_Opr.getText("Login_Operator")
		});
		
		var oPanel = new sap.m.Panel({
			content: oInput
		});
		
		var createKeyboardButton = function(sButtonText) {
			
			if(!sButtonText)
				sButtonText = "";

			var oButton = new sap.m.Button({
				text: sButtonText,
				press: oController.onKeyboardButtonPress
			}).addStyleClass('keyboardBtn');
			
			return oButton;
		};
		
		var oNumKeyboard = new sap.ui.layout.Grid({
			hSpacing: 1,
			vSpacing: 1,
			position: "Center",
			defaultSpan: "L3 M3 S3",
			defaultIndent: "L1 M1 S1",
			content: [
				createKeyboardButton("1"),
				createKeyboardButton("2"),
				createKeyboardButton("3"),
				createKeyboardButton("4"),
				createKeyboardButton("5"),
				createKeyboardButton("6"),
				createKeyboardButton("7"),
				createKeyboardButton("8"),
				createKeyboardButton("9"),
				createKeyboardButton("<<"),
				createKeyboardButton("0"),
				createKeyboardButton("<")
			]
		}).addStyleClass("sapUiTinyMarginTop sapUiLargeMarginBegin");
		
		var oPage = new sap.m.Page({
			id: "loginPage",
			//title: oLng_Opr.getText("Login_UserAccess"),
			customHeader: new sap.m.Toolbar({
				content: [
					new sap.m.Image({
						id: 'envImage',
						//src: 'logo.png',
						height: '36px'
					}).addStyleClass("sapUiSmallMarginBegin"),
					new sap.m.ToolbarSpacer(),
					new sap.m.Text({
						text: oLng_Opr.getText("Login_UserAccess")
					}),
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						icon: "sap-icon://drop-down-list",
						press: oController.onShowOptions
					}).addStyleClass("sapUiLargeMarginBegin")
				]
			}),
			/*headerContent: [
				new sap.m.Button({
					icon: "sap-icon://drop-down-list",
			        press: oController.onShowOptions
			    })
			],*/
			content: [
				oPanel,
				oNumKeyboard
			],
			footer: new sap.m.Toolbar({
				content: [
					new sap.m.ToolbarSpacer(),
					new sap.m.Button({
						id: 'enterButton',
						icon: "sap-icon://begin",
						text: oLng_Opr.getText("Login_Enter"),
						press: oController.onEnterPress
					}).addStyleClass("greenButton noBorderButton")
				]
			})
		});

	  	return oPage;
	}

});
