// Controller Riparazione / Rilavorazione
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.REPUDC.repUdc", {
	
	_oArguments: undefined,
	_sViewType: '', // R = Riparazione , W = Rilavorazione
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("repUdc").attachPatternMatched(this.onRepMatched, this);
		oAppRouter.getRoute("rewUdc").attachPatternMatched(this.onRewMatched, this);

		var nextFunc = this.onNext;
		var oUdcFromInput = sap.ui.getCore().byId("repUdcFromInput");
		var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
		
		/*
		var oOrderInput = sap.ui.getCore().byId("repUdcOrder");
		
		oOrderInput.onsapenter = function (oEvent)
		{
			if(oOrderInput.getValue())
			{
				if(oUdcFromInput.getValue())
					nextFunc();
				else
					oUdcFromInput.focus();
			}
		}*/
		
		oUdcFromInput.onsapenter = function (oEvent)
		{
			if(oUdcFromInput.getValue())
			{
				oUdcFromInput.fireChange();
				oOrderCombo.focus();
			}
		}

	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onRepMatched: function (oEvent) {
		this._sViewType = 'R';
		
		this.onObjectMatched(oEvent);
	},
	
	onRewMatched: function (oEvent) {
		this._sViewType = 'W';
		
		/*
		var nextFunc = this.onNext;
		var oUdcFromInput = sap.ui.getCore().byId("repUdcFromInput");
		var oOrderInput = sap.ui.getCore().byId("repUdcOrder");
		
		oUdcFromInput.onsapenter = function (oEvent)
		{
			if(oUdcFromInput.getValue())
			{
				if(oOrderInput.getValue())
					nextFunc();
				else
					oOrderInput.focus();
			}
		}
		*/
		
		this.onObjectMatched(oEvent);
	},

	onObjectMatched: function (oEvent) {
		
		const sViewType = this._sViewType;
		
		var oView = this.getView();
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData({VIEWTYPE: sViewType});
		//oView.fireModelContextChange();
		
		var oController = this;
		const oArguments = oAppController.getPatternArguments(oEvent,['plant','pernr']); 
		oController._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('repUdcPage'),sPlant,sPernr);
		
		var oUdcFromInput = sap.ui.getCore().byId("repUdcFromInput");
		oUdcFromInput.setValueState();
		oUdcFromInput.setValueStateText();
		oUdcFromInput.setValue();
		
		var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
		oOrderCombo.setValueState();
		oOrderCombo.setValueStateText();
		oOrderCombo.clearSelection();
		oOrderCombo.setValue();
		
		var oOrderModel = oOrderCombo.getModel('repOrders');
		if(oOrderModel)
			oOrderModel.destroy();
		oOrderCombo.setModel(new sap.ui.model.xml.XMLModel(),'repOrders');
		
		/*
		if(sViewType === 'R')
		{
			oUdcFromInput.attachChange(this.onUdcFromChange);
			var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
			oOrderCombo.setValueState();
			oOrderCombo.setValueStateText();
			oOrderCombo.clearSelection();
			oOrderCombo.setValue();
		}
		else
		{
			oUdcFromInput.detachChange(this.onUdcFromChange);
			var oOrderInput = sap.ui.getCore().byId("repUdcOrder");
			oOrderInput.setValueState();
			oOrderInput.setValueStateText();
			oOrderInput.setValue();
		}
		*/
		
		jQuery.sap.delayedCall(500, null, function() {
			oUdcFromInput.focus();
			sap.ui.core.BusyIndicator.hide();
		});
	},


/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oController = sap.ui.getCore().byId('repUdcView').getController();
		oAppRouter.navTo('menu',{
			query: oController._oArguments
		});
	},
	
/***********************************************************************************************/
// Caricamento combo delle commesse/fase di riparazione disponibili
/***********************************************************************************************/
	
	onUdcFromChange: function(oEvent) {
		
		var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
		oOrderCombo.clearSelection();
		oOrderCombo.setValue();
		
		var oModel = oOrderCombo.getModel('repOrders');
		if(!oModel)
		{
			oModel = new sap.ui.model.xml.XMLModel();
			oOrderCombo.setModel(oModel,'repOrders');
		}
		
		var oController = sap.ui.getCore().byId('repUdcView').getController();
		var sQueryName;
		if(oController._sViewType === 'R')
			sQueryName = 'getAvailableRepOrdersSQ';
		else if(oController._sViewType === 'W')
			sQueryName = 'getAvailableRewOrdersSQ';
		
		var sQuery = QService + sMovementPath + "Rework/" + sQueryName;
		sQuery += "&Param.1=" + oEvent.getSource().getValue();
		
		oModel.loadData(sQuery,false,false);
	},
	
/***********************************************************************************************/
// Validazione dei dati e passaggio alla pagina successiva
/***********************************************************************************************/
	
	onNext: function() {
		var oController = sap.ui.getCore().byId('repUdcView').getController();
		const sViewType = oController._sViewType;
		
		var oUdcFromInput = sap.ui.getCore().byId("repUdcFromInput");
		//var oOrderInput = sap.ui.getCore().byId("repUdcOrder");
		var oOrderCombo = sap.ui.getCore().byId("repUdcOrderBox");
		
		oUdcFromInput.setValueState();
		oUdcFromInput.setValueStateText();
		//oOrderInput.setValueState();
		//oOrderInput.setValueStateText();
		oOrderCombo.setValueState();
		oOrderCombo.setValueStateText();
		
		const sUdcNr = oUdcFromInput.getValue();
		//const sOrderPhase = (sViewType === 'R') ? oOrderCombo.getValue().split(" ")[0] : oOrderInput.getValue();
		
		var sOrderPhase = oOrderCombo.getSelectedKey();
		if(!sOrderPhase)
			sOrderPhase = oOrderCombo.getValue().split(" ")[0];
		
		var sErrorMessage;
		if(!sUdcNr)
		{
			sErrorMessage = oLng_Opr.getText("RepUdc_UdcErrorMessage");
			oUdcFromInput.setValueState(sap.ui.core.ValueState.Warning);
			oUdcFromInput.setValueStateText(sErrorMessage);
		}
		else
		{
			sErrorMessage = oAppController.checkOrderPhase(sOrderPhase);
			if(sErrorMessage) {
				/*if(sViewType === 'R')
				{*/
					oOrderCombo.setValueState(sap.ui.core.ValueState.Warning);
					oOrderCombo.setValueStateText(sErrorMessage);
				/*}
				else
				{
					oOrderInput.setValueState(sap.ui.core.ValueState.Warning);
					oOrderInput.setValueStateText(sErrorMessage);
				}*/
			}
			else if(/*sViewType === 'R' && */!oOrderCombo.getItemByKey(sOrderPhase))
			{
				sErrorMessage = oLng_Opr.getText("RepUdc_RepOrderErrorMessage");
				oOrderCombo.setValueState(sap.ui.core.ValueState.Warning);
				oOrderCombo.setValueStateText(sErrorMessage);
			}
		}

		if(sErrorMessage) {
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oSettings = oStorage.get("WORKST_INFO");
			var aLines = [];
			var aLinesObjects = [];
			if(oSettings && oSettings.hasOwnProperty('LINES') && oSettings.LINES)
			{
				aLinesObjects = oSettings.LINES;
				for(var i=0; i<aLinesObjects.length; i++)
					aLines.push(aLinesObjects[i].lineid);
			}
			
			const aSpecialWarningCodes = [2,3];
			
			var oController = sap.ui.getCore().byId('repUdcView').getController();
			var oArguments = oController._oArguments;
		
			var sCheckQuery = sMovementPath + "Rework/checkUDCForReworkQR";
			sCheckQuery += "&Param.1=" + oArguments.plant;
			sCheckQuery += "&Param.2=" + aLines.join();
			sCheckQuery += "&Param.3=" + sUdcNr;
			sCheckQuery += "&Param.4=" + sOrderPhase;
			sCheckQuery += "&Param.5=" + sLanguage;
			
			var oController = sap.ui.getCore().byId('repUdcView').getController();
			var oBoxDeclController = sap.ui.controller("ui5app.BOXDECL.boxDecl");
			
			const aReturnProperties = ['outputLines','cicTxt','destMaterial','destMatDesc',
				'destCdlId','destCdlDesc','orderType','ctrlOper','sreasid','noCompleted','imbObbl'];
			
			oAppController.handleRequest(sCheckQuery,{
				showSuccess: false,
				onSuccess: oBoxDeclController.onLineSuccess,
				showWarning: aSpecialWarningCodes,
				onWarning: oBoxDeclController.onLineWarning,
				callbackParams: {
					warningCodes: aSpecialWarningCodes,
					linesObjects: aLinesObjects,
					oneLineCallback: oBoxDeclController.onLineSuccess,
					lineCallback: oController.onCheckSuccess,
					order: sOrderPhase.substring(0,12),
					poper: sOrderPhase.substring(12,16),
					plant: oArguments.plant
				},
				results: {
					returnProperties: aReturnProperties,
					returnXMLDoc: true
				}
			});
		}
	},
	
	onCheckSuccess: function(oLineInfo,oCallbackParams) {
		
		var confirmNext = function ()
		{
			var oController = sap.ui.getCore().byId('repUdcView').getController();
			
			var xmlData = oCallbackParams.returnedData.data.childNodes[0]; // Rowsets
			xmlData = xmlData.childNodes[1]; // Rowset 1
			xmlData = xmlData.childNodes[1]; // Row 1 (Columns 0)
			
			var oUdcObject = getJsonObjectFromXMLObject(xmlData);
			oUdcObject.REW_ORDER = oCallbackParams.order;
			oUdcObject.REW_POPER = oCallbackParams.poper;
			oUdcObject.REW_CICTXT = oCallbackParams.returnedData.cicTxt;
			oUdcObject.REW_MATERIAL = oCallbackParams.returnedData.destMaterial;
			oUdcObject.REW_MATDESC = oCallbackParams.returnedData.destMatDesc;
			oUdcObject.REW_CDLID = oCallbackParams.returnedData.destCdlId;
			oUdcObject.REW_CDLDESC = oCallbackParams.returnedData.destCdlDesc;
			oUdcObject.REW_LINEID = oLineInfo.lineid;
			oUdcObject.REW_LINETXT = oLineInfo.linetxt;
			oUdcObject.ORDTYPE = oCallbackParams.returnedData.orderType;
			oUdcObject.CTRLOPER = oCallbackParams.returnedData.ctrlOper;
			oUdcObject.NOCOMPLETED = oCallbackParams.returnedData.noCompleted;
			oUdcObject.SREASID = oCallbackParams.returnedData.sreasid;
			oUdcObject.IMBOBBL = oCallbackParams.returnedData.imbObbl;
			oUdcObject.VIEWTYPE = oController._sViewType;
			
			var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
			oSessionStorage.put('FUNCDATA',oUdcObject);

			var oArguments = oController._oArguments;
				
			oArguments['udcfrom'] = oUdcObject.UDCNR;
			oArguments['order'] = oCallbackParams.order;
			oArguments['poper'] = oCallbackParams.poper;

			oAppRouter.navTo('repUdcInput',{
				query: oArguments
			});
		};
		
		var oConfirmDialog = new sap.m.Dialog({
			title: oLng_Opr.getText("RepUdc_ConfirmRequest"),
			contentWidth: "550px",
			contentHeight: "140px",
			type: 'Message',
			content: [
				new sap.ui.layout.VerticalLayout({
					content: [
						new sap.m.Text({
							text: oLng_Opr.getText("RepUdc_ConfirmText1") + " " + parseInt(oCallbackParams.order) + " " +
								oLng_Opr.getText("RepUdc_ConfirmText2") + " " + parseInt(oCallbackParams.poper) + "."
						}),
						new sap.m.Text({
							text: oLng_Opr.getText("RepUdc_ConfirmText3")
						}),
						new sap.m.Text({
							text: oCallbackParams.returnedData.destMaterial + " ?"
						}).addStyleClass('bigText'),
						new sap.m.Text({
							text: oLng_Opr.getText("RepUdc_ConfirmText4")
						}).addStyleClass('redText')
					]
				})
			],
			buttons: [
				new sap.m.Button({
					text: oLng_Opr.getText("RepUdc_YesSure"),
					icon: "sap-icon://accept",
					press: function() {
						oConfirmDialog.close();
						confirmNext();
					}
				}),
				new sap.m.Button({
					text: oLng_Opr.getText("RepUdc_NoChange"),
					icon: "sap-icon://decline",
					press: function() {
						oConfirmDialog.close();
					}
				})
			],
			afterClose: function() {
				oConfirmDialog.destroy();
			}
		});
		
		oConfirmDialog.open();
	}

});