// Controller Riparazione / Rilavorazione Input
//**********************************************************************************************************************
// Ver: 0.1 - Author: Alex Bonaffini
//**********************************************************************************************************************

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

sap.ui.controller("ui5app.REPUDC.repUdcInput", {
	
	_oArguments: undefined,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
*/
	onInit: function() {
		oAppRouter.getRoute("repUdcInput").attachPatternMatched(this.onObjectMatched, this);
	},

/***********************************************************************************************/
// Chiamata alla prima apertura e al cambiamento di un parametro dell'url
/***********************************************************************************************/

	onObjectMatched: function (oEvent) {

		const oArgParams = ['plant','pernr','udcfrom','order','poper'];
		const oArguments = oAppController.getPatternArguments(oEvent,oArgParams); 
		this._oArguments = oArguments;	
		
		const sPlant = oArguments.plant;
		const sPernr = oArguments.pernr;

		oAppController.loadSubHeaderInfo($.UIbyID('repUdcInputPage'),sPlant,sPernr);
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oUdcObject = oSessionStorage.get('FUNCDATA');
				
		if(!oUdcObject) // !oUdcObject.hasOwnProperty('UDCNR') || oUdcObject.UDCNR !== oArguments.udcnr
		{
			oUdcObject = {
				DISABLED_MODE: true
			};
		}
		
		var oNextPhaseCombo = $.UIbyID("repUdcInputNextPhase");
		
		var oNextPhaseModel = oNextPhaseCombo.getModel();
		if(oNextPhaseModel)
			oNextPhaseModel.destroy();
		oNextPhaseModel = new sap.ui.model.xml.XMLModel();
		oNextPhaseCombo.setModel(oNextPhaseModel);
		
		var oStdQtyCombo = $.UIbyID('repUdcInputStdQty');
		var oStdQtyModel = oStdQtyCombo.getModel();
		if(!oStdQtyModel)
		{
			oStdQtyModel = new sap.ui.model.xml.XMLModel();
			oStdQtyCombo.setModel(oStdQtyModel);
		}
		var sStdQtyQuery = QService + sMasterDataPath + "Nmimb/getNmimbByOrderPoperSQ";
		sStdQtyQuery += "&Param.1=" + sPlant;
		sStdQtyQuery += "&Param.2=" + oArguments.order;
		sStdQtyQuery += "&Param.3=" + oArguments.poper;
		oStdQtyModel.loadData(sStdQtyQuery,false,false);
		
		var sNextPhaseQuery = QService + sMasterDataPath + "RepReasons/GetDestinationRepQR";
		sNextPhaseQuery += "&Param.1=" + sPlant;
		sNextPhaseQuery += "&Param.2=" + ((oUdcObject.ORDTYPE === "ZRIP") ? 'REP' : 'REW');
		oNextPhaseModel.loadData(sNextPhaseQuery,false,false);
		
		this.displayInfo(oUdcObject);
	},

/***********************************************************************************************/
// Visualizzazione delle informazioni
/***********************************************************************************************/
	
	displayInfo: function(oUdcObject) {
		
		console.log("Info UdC: " + JSON.stringify(oUdcObject));
		var oView = sap.ui.getCore().byId('repUdcInputView');
		var oModel = oView.getModel();
		if(!oModel)
		{
			oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel);
		}
		oModel.setData(oUdcObject);
		oView.fireModelContextChange();
		
		var oWorkTimeInput = $.UIbyID("repUdcInputWorkTime");
		oWorkTimeInput.setValueState();
		oWorkTimeInput.setValueStateText();
		oWorkTimeInput.setValue(oUdcObject.REW_WTDUR);
		
		var oQtyInput = $.UIbyID("repUdcInputQty");
		oQtyInput.setValueState();
		oQtyInput.setValueStateText();
		oQtyInput.setValue(oUdcObject.QTYDECL);
	
		var oBasketTTInput = $.UIbyID('repUdcInputBasketTT');
		oBasketTTInput.setValueState();
		oBasketTTInput.setValueStateText();
		oBasketTTInput.setValue(oUdcObject.REW_BASKETTT);
		
		var oDaterInput = $.UIbyID('repUdcInputDater');
		oDaterInput.setValueState();
		oDaterInput.setValueStateText();
		oDaterInput.setValue(oUdcObject.REW_DATER);
		
		var oNextPhaseCombo = $.UIbyID("repUdcInputNextPhase");
		oNextPhaseCombo.setValue();
		oNextPhaseCombo.setSelectedKey();
		oNextPhaseCombo.setValueState();
		oNextPhaseCombo.setValueStateText();
		
		var oStdQtyCombo = $.UIbyID('repUdcInputStdQty');
		oStdQtyCombo.clearSelection();
		oStdQtyCombo.setValue();
		oStdQtyCombo.setValueState();
		oStdQtyCombo.setValueStateText();
		oStdQtyCombo.data('NMIMB',undefined);
		const sSelectedNmImb = oUdcObject.NMIMB;
		if(!sSelectedNmImb)
		{
			const sDefaultKey = getValueFromXMLModel(oStdQtyCombo.getModel(),"DEFAULT","1","NMIMB");
			if(sDefaultKey)
				oStdQtyCombo.setSelectedKey(sDefaultKey);
		}
		else
		{
			if(oStdQtyCombo.getItemByKey(sSelectedNmImb))
				oStdQtyCombo.setSelectedKey(sSelectedNmImb);
			else
			{
				oStdQtyCombo.setValue(sSelectedNmImb);
				oStdQtyCombo.data('NMIMB',{
					QTY: 0, // parametro non utilizzato al momento (quantità corrispondente a NMIMB)
					NMIMB: sSelectedNmImb
				});
			}
		}
		
		var oConfirmButton = $.UIbyID("repUdcInputConfirmButton");
		if(oUdcObject.ORDTYPE === "ZRIP" || oUdcObject.CTRLOPER !== '1')
		{
			oConfirmButton.removeStyleClass('greenButton');
			oConfirmButton.addStyleClass('yellowButton');
		}
		
		var oStateSegmented = sap.ui.getCore().byId("repUdcInputState");
		var sSelectedKey = '';		
		if(oUdcObject.hasOwnProperty('REWSTATE'))
			sSelectedKey = oUdcObject.REWSTATE;
		else if((oUdcObject.ORDTYPE === "ZRIP" && oUdcObject.NOCOMPLETED === "1") || 
			(oUdcObject.ORDTYPE !== "ZRIP" && oUdcObject.CTRLOPER !== "1"))
			sSelectedKey = 'INCOMPLETE';
		oStateSegmented.setSelectedKey(sSelectedKey);
		var oSelButton = $.UIbyID(oStateSegmented.getSelectedButton());
		if(oSelButton && oUdcObject.ORDTYPE !== "ZRIP")
			oSelButton.firePress();
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
*/
//	onExit: function() {
//
//	}

/***********************************************************************************************/
// Torna al menu login
/***********************************************************************************************/

	onNavBack: function() {
		var oView = sap.ui.getCore().byId('repUdcInputView');
		var oModel = oView.getModel();
		var oController = sap.ui.getCore().byId('repUdcInputView').getController();
		const oArguments = oController._oArguments;
		
		if(oArguments.hasOwnProperty('udcfrom'))
			delete oArguments.udcfrom;
		if(oArguments.hasOwnProperty('order'))
			delete oArguments.order;
		if(oArguments.hasOwnProperty('poper'))
			delete oArguments.poper;
		if(oArguments.hasOwnProperty('udcto'))
			delete oArguments.udcto;
		
		var sViewName;
		if(oModel.getProperty("/VIEWTYPE") === "W")
			sViewName = 'rewUdc';
		else
			sViewName = 'repUdc';
		
		oAppRouter.navTo(sViewName,{
			query: oController._oArguments
		});
	},
	
	onConfirm: function() {
		var oView = sap.ui.getCore().byId('repUdcInputView');
		var oModel = oView.getModel();
		
		var oBasketTTInput = $.UIbyID('repUdcInputBasketTT');
		var oDaterInput = $.UIbyID('repUdcInputDater');
		var oQtyInput = sap.ui.getCore().byId("repUdcInputQty");
		var oNextPhaseCombo = sap.ui.getCore().byId("repUdcInputNextPhase");
		var oWorkTimeInput = $.UIbyID("repUdcInputWorkTime");
		var oStateSegmented = sap.ui.getCore().byId("repUdcInputState");
		var oStdQtyCombo = $.UIbyID('repUdcInputStdQty');
		
		oBasketTTInput.setValueState(sap.ui.core.ValueState.None);
		oBasketTTInput.setValueStateText();
		oDaterInput.setValueState(sap.ui.core.ValueState.None);
		oDaterInput.setValueStateText();
		oWorkTimeInput.setValueState();
		oWorkTimeInput.setValueStateText();
		oQtyInput.setValueState();
		oQtyInput.setValueStateText();
		oNextPhaseCombo.setValueState();
		oNextPhaseCombo.setValueStateText();
		oStdQtyCombo.setValueState();
		oStdQtyCombo.setValueStateText();

		const sBasketTT = oBasketTTInput.getValue();
		const sDater = oDaterInput.getValue();
		const iQty = parseInt(oQtyInput.getValue());
		const iQtyRes = parseInt(oModel.getProperty("/QTYRES"));
		const sWorkTime = oWorkTimeInput.getValue();
		const sRewState = oStateSegmented.getSelectedKey();
		
		const bIsRepair = oModel.getProperty("/ORDTYPE") === "ZRIP";
		
		var sDestination = oNextPhaseCombo.getSelectedKey();
		if(!sDestination)
			sDestination = oNextPhaseCombo.getValue();
		
		var sNmImb = '';
		if(!bIsRepair)
		{
			sNmImb = oStdQtyCombo.getSelectedKey();
			if(!sNmImb)
			{
				var oNmImbData = oStdQtyCombo.data('NMIMB');
				if(oNmImbData && oNmImbData.hasOwnProperty('NMIMB'))
					sNmImb = oNmImbData.NMIMB;
			}
		}
		
		var sErrorMessage,oErrorField,sWarningMessage;
		const sCurrentYear = parseInt(new Date().getFullYear().toString().substring(2,4));
		if(!sRewState)
			sErrorMessage = oLng_Opr.getText("RepUdcInput_StateErrorMessage")
		else if(sBasketTT && !sBasketTT.match(/^[0-9a-zA-Z ]+$/))
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_BasketTTErrorMsg");
			oErrorField = oBasketTTInput;
		}
		else if(isNaN(sWorkTime) || sWorkTime<=0)
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_WorkTimeErrorMsg");
			oErrorField = oWorkTimeInput;
		}
		else if(sWorkTime > 480)
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_WorkTimeMaxErrorMsg") + " (480 Min)";
			oErrorField = oWorkTimeInput;
		}
		else if(!sDestination && (bIsRepair || (!bIsRepair && sRewState === 'INCOMPLETE')))
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_NextPhaseErrorMessage");
			oErrorField = oNextPhaseCombo;
		}
		else if(sDater && (!sDater.match(/(0[1-9]|1[0-2])([0-9]{2})/) || sDater.length != 4)) // solo caratteri alfanumerici
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_DaterErrorMsg");
			oErrorField = oDaterInput;
		}
		else if(sDater && (sDater.substring(2,4) < sCurrentYear-1 || sDater.substring(2,4) > sCurrentYear+1))
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_DaterYearErrorMsg");
			oErrorField = oDaterInput;
		}
		else if(!bIsRepair && sRewState === 'COMPLETE' && parseInt(oModel.getProperty("/IMBOBBL")) === 1 && !sNmImb)
		{
			sErrorMessage = oLng_Opr.getText("RepUdcInput_MandatoryStdImb");
			oErrorField = oStdQtyCombo;
		}
		else
		{
			if(isNaN(iQty) || iQty <= 0)
				sErrorMessage = oLng_Opr.getText("RepUdcInput_QtyErrorMessage");
			else if(iQty > 1500)
				sErrorMessage = oLng_Opr.getText("RepUdcInput_QtyMaxErrorMsg") + " (1500)";
			else if(iQty > iQtyRes)
				sWarningMessage = oLng_Opr.getText("RepUdcInput_QtyResErrorMessage");
			
			if(sErrorMessage)
				oErrorField = oQtyInput;
		}

		if(sErrorMessage)
		{
			if(oErrorField) {
				oErrorField.setValueState(sap.ui.core.ValueState.Warning);
				oErrorField.setValueStateText(sErrorMessage);
				oErrorField.focus();
			}
			
			sap.m.MessageBox.show(sErrorMessage, {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: oLng_Opr.getText("General_WarningText"),
				actions: sap.m.MessageBox.Action.OK
			});
		}
		else
		{
			var confirmFunc = function() {
                var oController = oView.getController();
                const oArguments = oController._oArguments;

                const bIsCompleted = sRewState === 'COMPLETE';

                var sOrigin;
                if(bIsRepair) // Se di riparazione crea sempre sospesi
                    sOrigin = 'C';
                else
                    sOrigin = bIsCompleted ? 'M' : 'C';

                var sUdcToQuery = QService + sMovementPath + "UDC/getUDCMovByOriginSQ";
                sUdcToQuery += "&Param.1=<> 'C' AND DUDCMAST.UDCSTAT <> ";
                sUdcToQuery += "&Param.2=E";
                sUdcToQuery += "&Param.3=" + oArguments.order;
                sUdcToQuery += "&Param.4=" + oArguments.poper;
                sUdcToQuery += "&Param.5==";
                sUdcToQuery += "&Param.6=" + sOrigin;

                var nextFunction;
                var bNavBackOnSuccess;
                if(!bIsRepair && !bIsCompleted && oModel.getProperty("/CTRLOPER") === '1') // Rilavorazione incompleta con fase di controllo
                {
                    nextFunction = oController.onReas;
                    bNavBackOnSuccess = true;
                }
                else
                {
                    nextFunction = oController.onRework;
                    bNavBackOnSuccess = false;
                }

                const oCallbackParams = {
                    arguments: oArguments,
                    lineidFrom: oModel.getProperty("/IDLINE"),
                    lineidTo: oModel.getProperty("/REW_LINEID"),
                    material: oModel.getProperty("/REW_MATERIAL"),
                    isRepCompleted: bIsRepair && bIsCompleted,
                    rewState: sRewState,
                    destOrigin: sOrigin,
                    destination: sDestination,
                    quantity: iQty.toString(),
                    workTime: sWorkTime,
                    dater: sDater,
                    basketTT: sBasketTT,
                    nmimb: sNmImb,
                    reasid: (bIsRepair && !bIsCompleted) ? oModel.getProperty("/SREASID") : '',
                    navBackOnSuccess: bNavBackOnSuccess,
                    operationType: (bIsRepair) ? "R" : "W"
                };

                var oUdcToModel = new sap.ui.model.xml.XMLModel();
                oUdcToModel.loadData(sUdcToQuery,false,false);
                var oRowsetObject = oUdcToModel.getObject("/Rowset/0/");
                if(oRowsetObject && oRowsetObject.childNodes.length-1>0) //numero di UDC nel modello
                {
                    const sExistingTxt = oLng_Opr.getText('RepUdcInput_SelectUdcExisting');
                    sap.m.MessageBox.show(oLng_Opr.getText('RepUdcInput_SelectUdcMessage'), {
                        icon: sap.m.MessageBox.Icon.WARNING,
                        title: oLng_Opr.getText('General_WarningText'),
                        actions: [
                            sExistingTxt,
                            oLng_Opr.getText('RepUdcInput_SelectUdcNew')
                        ],
                        onClose: function(oAction) {
                            if (oAction === sExistingTxt)
                                oAppController.selectFromDialog({
                                    title: oLng_Opr.getText('RepUdcInput_SelectUdcTitle'),
                                    noDataText: oLng_Opr.getText('RepUdcInput_SelectUdcTitle'),
                                    callback: nextFunction,
                                    callbackParams: oCallbackParams,
                                    model: oUdcToModel,
                                    modelPath: "/Rowset/0/Row",
                                    modelTitle: 'UDCNR',
                                    modelDesc: 'MATERIAL',
                                    template: oAppController.customListTemplate({
                                        titleText: "{UDCNR}",
                                        bottomLeftText: "{MOV_IDLINE}",
                                        topRightText: {
                                            parts: [
                                                {path: "QTYRES" }
                                            ],		     
                                            formatter: function(sQtyRes) {
                                                return oLng_Opr.getText('RepUdcInput_Qty') + ": " + sQtyRes;
                                            }
                                        },
                                        bottomRightText: {
                                            parts: [
                                                {path: "TIMEID" }
                                            ],		     
                                            formatter: dateFormatter
                                        }
                                    })
                                });
                            else
                                nextFunction(undefined,oCallbackParams);
                        }
                    });
                }
                else
                    nextFunction(undefined,oCallbackParams);
            };
            if(sWarningMessage){
                sap.m.MessageBox.show(sWarningMessage, {
                    icon: sap.m.MessageBox.Icon.WARNING,
                    title: oLng_Opr.getText("General_WarningText"),
                    actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    onClose: function(oAction){
                        if(oAction === sap.m.MessageBox.Action.YES)
                            confirmFunc();
                    }
                });
            }
            else{
                confirmFunc();
            }
		}
	},
	
	onReas: function(oDestUdcObject,oCallbackParams) { // passaggio alla schermata di selezione delle causali	
		
		var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
		var oFuncData = oSessionStorage.get('FUNCDATA');
		oFuncData.CALLBACKPARAMS = oCallbackParams;
		oSessionStorage.put('FUNCDATA',oFuncData);
		
		var sDestUdc = '', sLineId = '';		
		if(oDestUdcObject)
		{
			if(oDestUdcObject.hasOwnProperty('UDCNR'))
				sDestUdc = oDestUdcObject.UDCNR;
			if(oDestUdcObject.hasOwnProperty('IDLINE'))
				sLineId = oDestUdcObject.IDLINE;
		}
		
		var oController = sap.ui.getCore().byId('repUdcInputView').getController();
		var oArguments = oController._oArguments;
		
		oArguments['udcto'] = sDestUdc;
		oArguments['idline'] = sLineId;

		oAppRouter.navTo('repUdcReas',{
			query: oArguments
		});
	},
	
	onRework: function(oDestUdcObject,oCallbackParams) { // esecuzione dell'operazione di riparazione / rilavorazione

		console.log("Info UdC destinazione: " + JSON.stringify(oDestUdcObject));
		
		var continueFunc = function(sState) {
			var sDestUdc = '';
			if(oDestUdcObject && oDestUdcObject.hasOwnProperty('UDCNR'))
				sDestUdc = oDestUdcObject.UDCNR;
			
			var sWsName = '';
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oWorkstInfo = oStorage.get('WORKST_INFO');
			if(oWorkstInfo.hasOwnProperty('WSNAME') && oWorkstInfo.WSNAME)
				sWsName = oWorkstInfo.WSNAME;
			
			const oArguments = oCallbackParams.arguments;
		
			var sSaveQuery = sMovementPath + "Rework/saveReworkAndPrintQR";
			sSaveQuery += "&Param.1=" + oArguments.plant;
			sSaveQuery += "&Param.2=" + oAppController._sShift;
			sSaveQuery += "&Param.3=" + oAppController._sDate;
			sSaveQuery += "&Param.4=" + oArguments.pernr;
			sSaveQuery += "&Param.5=" + oCallbackParams.lineidFrom;
			sSaveQuery += "&Param.6=" + oArguments.udcfrom;		
			sSaveQuery += "&Param.7=" + oCallbackParams.lineidTo;
			sSaveQuery += "&Param.8=" + oArguments.order;
			sSaveQuery += "&Param.9=" + oArguments.poper;
			sSaveQuery += "&Param.10=" + oCallbackParams.destOrigin;
			sSaveQuery += "&Param.11=" + sDestUdc;
			sSaveQuery += "&Param.12=" + oCallbackParams.destination;
			sSaveQuery += "&Param.13=" + oCallbackParams.material;
			sSaveQuery += "&Param.14=" + oCallbackParams.quantity;
			sSaveQuery += "&Param.15=" + oCallbackParams.workTime;
			sSaveQuery += "&Param.16=" + oCallbackParams.dater;
			sSaveQuery += "&Param.17=" + oCallbackParams.basketTT;
			sSaveQuery += "&Param.18=" + oCallbackParams.reasid;
			sSaveQuery += "&Param.19=" + ((oCallbackParams.isRepCompleted) ? '1' : '0');
			sSaveQuery += "&Param.20=" + sWsName;
			sSaveQuery += "&Param.21=" + getDatetimeString(new Date());
			sSaveQuery += "&Param.22=";
			sSaveQuery += "&Param.23=" + sState;
			sSaveQuery += "&Param.24=" + oCallbackParams.operationType;
			sSaveQuery += "&Param.25=" + oCallbackParams.nmimb;
			sSaveQuery += "&Param.26=" + sLanguage;
			
			var successFunction = function(oResults) {
				
				var oSessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
				var oUdcObject = oSessionStorage.get('FUNCDATA');
				
				if(!oUdcObject)
					oUdcObject = {};
				
				const iQtyRes = parseInt(oUdcObject.QTYRES);
				
				var navBackFunc = function () {
					oAppRouter.navTo('repUdcInput',{
						query: oArguments
					});
				};
				
				if(oResults.hasOwnProperty('outputUdc') && oResults.outputUdc)
					oUdcObject.UDCTO = oResults.outputUdc;
				if(oResults.hasOwnProperty('pdfString'))
				{
					oUdcObject.PDFSTRING = oResults.pdfString;
					if(parseInt(oResults.code) === 1) // WARNING = 1 : stampa non automatica, anteprima automatica
					{
						var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
						oBoxDeclInputController.showPdfDialog(oResults.pdfString,true,
							(oCallbackParams.navBackOnSuccess) ? navBackFunc : undefined);
					}
				}
				oUdcObject.PRINT_MODE = true;
				oUdcObject.QTYRES = iQtyRes - oCallbackParams.quantity;
				oUdcObject.QTYDECL = oCallbackParams.quantity;
				oUdcObject.REW_WTDUR = oCallbackParams.workTime;
				oUdcObject.REW_DATER = oCallbackParams.dater;
				oUdcObject.REW_BASKETTT = oCallbackParams.basketTT;
				//oUdcObject.DESTORIGIN = oCallbackParams.destOrigin;
				oUdcObject.REWSTATE = oCallbackParams.rewState;
				oUdcObject.NMIMB = oCallbackParams.nmimb;
				
				oSessionStorage.put('FUNCDATA',oUdcObject);
				
				if(oCallbackParams.navBackOnSuccess) // Ritorno dalla pagina delle causali
				{
					if(parseInt(oResults.code) !== 1)
						navBackFunc();
				}
				else // Per i buoni è ancora nella stessa pagina
				{
					var oController = sap.ui.getCore().byId('repUdcInputView').getController();
					oController.displayInfo(oUdcObject,false);
				}
			};
			
			oAppController.handleRequest(sSaveQuery,{
				onSuccess: successFunction,
				onWarning: successFunction,
				notShowWarning: [1],
				results: ['outputUdc','pdfString']
			});
		};
		
		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
			
		oBoxDeclInputController.selectState({
			firstValue: 'O',
			secondValue: 'C',
			secondColorCss: (oCallbackParams.destOrigin === 'M') ? 'greenButton': 'yellowButton',
			onCallback: continueFunc
		})
	},
	
/***********************************************************************************************/
// Ristampa dell'etichetta
/***********************************************************************************************/

	onReprint: function() {
		var oView = sap.ui.getCore().byId('repUdcInputView');
		var oModel = oView.getModel();
		const sPdfString = oModel.getProperty("/PDFSTRING");
		
		var oBoxDeclInputController = sap.ui.controller("ui5app.BOXDECL.boxDeclInput");
		oBoxDeclInputController.showPdfDialog(sPdfString,true);
	}
	
});