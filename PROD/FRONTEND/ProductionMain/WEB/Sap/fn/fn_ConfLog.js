var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Operatori
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
url: "/XMII/CM/ProductionMain/Sap/res/ConfLog.i18n.properties",
locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabConfLog() {

// contenitore dati lista CID
/*
var oModel2 = new sap.ui.model.xml.XMLModel()
oModel2.loadData(QService + dataProdMD + "GetAllCIDQR");

// Crea la ComboBox per i CID

oCmbCID = new sap.ui.commons.ComboBox("cmbCID", {selectedKey : ""});
oCmbCID.setModel(oModel2);
var oItemCID = new sap.ui.core.ListItem();
oItemCID.bindProperty("text", "EMNAM");
oItemCID.bindProperty("key", "PERNR");
oCmbCID.bindItems("/Rowset/Row", oItemCID);
*/

//Crea L'oggetto Tabella Operatore
var oTable = UI5Utils.init_UI5_Table({
id: "logTab",
properties: {
title: oLng_MasterData.getText("ConfLog_Title"), //"Lista log conferme",
visibleRowCount: 15,
width: "98%",
firstVisibleRow: 0,
selectionMode: sap.ui.table.SelectionMode.Single,
navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
rowSelectionChange: function(oControlEvent){
if ($.UIbyID("logTab").getSelectedIndex() == -1) {
$.UIbyID("btnModOperator").setEnabled(false);
$.UIbyID("btnDelOperator").setEnabled(false);
}
else {
$.UIbyID("btnModOperator").setEnabled(true);
$.UIbyID("btnDelOperator").setEnabled(true);
}
}
},
exportButton: true,
toolbarItems:[

new sap.ui.commons.Button({
text: oLng_MasterData.getText("ConfLog_TransLog"), //"Aggiorna",
icon: 'sap-icon://inspect',
enabled: true,
press: function (){
ShowBlsDett();
}
}),
new sap.ui.commons.Button({
text: oLng_MasterData.getText("ConfLog_Refresh"), //"Aggiorna",
icon: 'sap-icon://refresh',
enabled: true,
press: function (){
refreshTabOperator();
}
})
],
columns: [
{
Field: "PLANT",
properties: {
width: "0px",
//campo nascosto
visible: false
}
},
{
Field: "NAME1", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_Plant"), //"Divisione",
properties: {
width: "120px"
}
},
{
Field: "DATAORA",
label: oLng_MasterData.getText("ConfLog_DATAORA"), //"DATAORA",
properties: {
width: "150px"
},
template: {
type: "DateTime",
textAlign: "Center"
}
},
/*
{
Field: "Direction",
label: oLng_MasterData.getText("ConfLog_DIRECTION"), //"Nome Operatore",
properties: {
width: "80px"
}
},
*/
{
Field: "DirectionIcon",
label: oLng_MasterData.getText("ConfLog_Dir"), //"Nome Operatore",
tooltipCol: "{Direction}",
iconColor: "{DirectionIconColor}",
properties: {
width: "50px",
height: "30px",
autoResizable: false,
},
template: {
type: "coreIcon",
textAlign: "Center"
}
},

{
Field: "GUID_FROM",
label: oLng_MasterData.getText("ConfLog_GUID_FROM"), //"Nome Operatore",
properties: {
width: "250px"
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "GUID_TO", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_GUID_TO"), //"Divisione",
properties: {
width: "250px"
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},

{
Field: "ORDER", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_ORDER"), //"Divisione",
properties: {
width: "120px",
visible: false
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "POPER", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_POPER"), //"Divisione",
properties: {
width: "100px",
visible: false
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "UDCNR", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_UDCNR"), //"Divisione",
properties: {
width: "150px",
visible: false
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "MATERIAL", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_MATERIAL"), //"Divisione",
properties: {
width: "100px",
visible: false
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "ConfType",
label: oLng_MasterData.getText("ConfLog_CONFTYPE"), //"Badge",
properties: {
width: "150px"
},
/*
template: {
type: "number",
textAlign: "Right"
}
*/
},
{
Field: "CONFIRMED", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_CONFIRMED"), //"Divisione",
properties: {
width: "100px",
visible: false
},
template: {
type: "checked",
textAlign: "Center"
}
},
{
Field: "DELETED", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_DELETED"), //"Divisione",
properties: {
width: "80px",
visible: false
},
template: {
type: "checked",
textAlign: "Center"
}
},
{
Field: "ERROR", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_ERROR"), //"Divisione",
properties: {
width: "80px",
visible: false
},
template: {
type: "checked",
textAlign: "Center"
}
},
{
Field: "ERR_DESC", // in Sigit è "PLNAME",
label: oLng_MasterData.getText("ConfLog_ERROR_DESC"), //"Divisione",
properties: {
width: "300px"
}/*,
template: {
type: "DateTime",
textAlign: "Center"
}*/
},
{
Field: "COUNTER",
label: oLng_MasterData.getText("ConfLog_COUNTER"), //"Badge",
properties: {
width: "110px",
visible: false
},
template: {
type: "Number",
textAlign: "Right"
}
},
/*
{
Field: "TypeMsg",
label: oLng_MasterData.getText("ConfLog_TYPE"), //"Badge",
properties: {
width: "100px"
}
},
*/
{
Field: "TypeMsgIcon",
label: oLng_MasterData.getText("ConfLog_TYPE"), //"Nome Operatore",
tooltipCol: "{TypeMsgIconTooltip}",
iconColor: "{TypeMsgIconColor}",
properties: {
width: "100px",
height: "30px",
visible: false,
autoResizable: false
},
template: {
type: "coreIcon",
textAlign: "Center",
tooltipCol: "DirectionDescr"
}
},
{
Field: "MESSAGE",
label: oLng_MasterData.getText("ConfLog_MESSAGE"), //"Note",
properties: {
width: "300px",
visible: false
}
}
]
});

var oModel = new sap.ui.model.xml.XMLModel();


if ( $.UIbyID("filtPlantConfProd").getSelectedKey() === "" ) $.UIbyID("filtPlantConfProd").setSelectedKey("2101");
var plant = $.UIbyID("filtPlantConfProd").getSelectedKey() === "" ? encodeURI("2101") : $.UIbyID("filtPlantConfProd").getSelectedKey();

var oCmbShiftsFilter = fnCreateCombo( false, "oCmbShiftsFilter", "SHIFT", "SHNAME",  QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + plant );
var oCmbLinesFilter = fnCreateCombo( false, "oCmbLinesFilter", "IDLINE", "LINETXT",  QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + plant )

/*
var oCmbTypeConfFilter = new sap.ui.commons.ComboBox("oCmbTypeConfFilter");
var oItem = new sap.ui.core.ListItem("Buoni").setKey("BU").setText("Buoni")
oCmbTypeConfFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("Sospesi").setKey("SO").setText("Sospesi");
oCmbTypeConfFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("Rilav").setKey("RL").setText("Rilavorazione");
oCmbTypeConfFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("RilavConclusa").setKey("CL").setText("Rilavorazione completa");
oCmbTypeConfFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("Riparazione").setKey("RP").setText("Riparazione");
oCmbTypeConfFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("RipConclusa").setKey("CP").setText("Riparazione completa");
oCmbTypeConfFilter.addItem(oItem);
*/
//    var oCmbTypeConfFilter = fnCreateCombo( false, "oCmbTypeConfFilter", "SAP_VALUE", "EXTENDED_TEXT",  QService + dataConfProd + "getMtextsSQ&Param.1=it");
var oCmbTypeConfFilter = fnCreateCombo( false, "oCmbTypeConfFilter", "SAP_TYPECONF", "EXTENDED_TEXT",  QService + dataConfProd + "getMtypeconfSQ&Param.1=" + sLanguage);

var oCmbDirectionFilter = new sap.ui.commons.ComboBox("oCmbDirectionFilter").setWidth("100px");
var oItem = new sap.ui.core.ListItem("Entrata").setKey("IN").setText("Entrata")
oCmbDirectionFilter.addItem(oItem);
oItem = new sap.ui.core.ListItem("Uscita").setKey("OUT").setText("Uscita");
oCmbDirectionFilter.addItem(oItem);

var oChkWrong = new sap.ui.commons.CheckBox( "oChkWrong" );

var oDateFrom = new sap.ui.commons.DatePicker('oDateFrom').setYyyymmdd( (new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 6)).toISOString().slice(0,10) ).setWidth("120px");
//    var oDateFrom = new sap.ui.commons.DatePicker('oDateFrom').setYyyymmdd( (new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 6)));
oDateFrom.attachChange(
function(oEvent){
refreshTabOperator();
}
);
var oDateTo = new sap.ui.commons.DatePicker('oDateTo').setYyyymmdd( new Date().toISOString().slice(0,10) ).setWidth("120px");
oDateTo.attachChange(
function(oEvent){
refreshTabOperator();
}
);
oCmbLinesFilter.attachChange(
function(oEvent){
refreshTabOperator();
}
);
oCmbShiftsFilter.attachChange(
function(oEvent){
refreshTabOperator();
}
)
oCmbTypeConfFilter.attachChange(
function(oEvent){
refreshTabOperator();
}
)
oCmbDirectionFilter.attachChange(
function(oEvent){
refreshTabOperator();
}
);
oChkWrong.attachChange(
function(oEvent){
//			$.UIbyID("oCmbTypeConfFilter").setSelectedKey("");
refreshTabOperator();
}
);

var oExtToolbar = new sap.ui.commons.Toolbar({
items: [
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_Plant") ),
$.UIbyID("filtPlantConfProd"),
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_DateFrom") ),
oDateFrom,
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_DateTo") ),
oDateTo,
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_DIRECTION") ),
oCmbDirectionFilter,
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_CONFTYPE") ),
oCmbTypeConfFilter,
new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfLog_Wrong") ),
oChkWrong,
new sap.ui.commons.Button({
icon: "sap-icon://filter",
id:   "delFilter",
press: function () {
$.UIbyID("oCmbDirectionFilter").setSelectedKey("");
$.UIbyID("oCmbTypeConfFilter").setSelectedKey(""); 
oChkWrong.setChecked(false);        
refreshTabOperator();
}
}),
new sap.ui.commons.Label({
id: "lblRecordNumber",
text: oLng_MasterData.getText("ConfLog_RecordNumber"), //"N. record"
textAlign: "Right"
})
]
});

oTable.setModel(oModel);
oTable.addExtension(oExtToolbar);
oTable.bindRows("/Rowset/Row");
refreshTabOperator();
return oTable;
}

// Aggiorna la tabella Operatori
function refreshTabOperator() {

var PLANT = $.UIbyID("filtPlantConfProd").getSelectedKey() == "" ? "2101" : $.UIbyID("filtPlantConfProd").getSelectedKey() ;
var DIRECTION = $.UIbyID("oCmbDirectionFilter").getSelectedKey();
var CONFTYPE = $.UIbyID("oCmbTypeConfFilter").getSelectedKey();
var  DATE_FROM = 	$.UIbyID("oDateFrom").getYyyymmdd();
DATE_FROM = DATE_FROM.indexOf("-") < 0 ? DATE_FROM.substr(0,4) + "-" +  DATE_FROM.substr(4,2) + "-" + DATE_FROM.substr(6,2)  : DATE_FROM;
var  DATE_TO = $.UIbyID("oDateTo").getYyyymmdd();
DATE_TO = DATE_TO.indexOf("-") < 0 ? DATE_TO.substr(0,4) + "-" +  DATE_TO.substr(4,2) + "-" + DATE_TO.substr(6,2) : DATE_TO;

/*
var qexe = $.UIbyID("oChkWrong").getChecked() === false ?  QService + dataConfProd + "getConfLog2SQ" : QService + dataConfProd + "getConfLogWrongSQ";
qexe += "&Param.1=" + PLANT +
"&Param.4=" + DIRECTION +
"&Param.5=" + DATE_FROM + "T00:00:00" +
"&Param.6=" + DATE_TO + "T23:59:59" +
"&Param.7=" + CONFTYPE;

$.UIbyID("logTab").getModel().loadData( qexe );

var rowsetObject = $.UIbyID("logTab").getModel().getObject("/Rowset/0");
if(rowsetObject){
var iRecNumber = rowsetObject.childNodes.length - 1;
$.UIbyID("lblRecordNumber").setText(oLng_MasterData.getText("ConfLog_RecordNumber") + " " + iRecNumber);
}
*/

var qParams = {
data:  ($.UIbyID("oChkWrong").getChecked() === false ?  dataConfProdFormatted + "getConfLog2SQ" : dataConfProdFormatted + "getConfLogWrongSQ" ) +
"&Param.1=" + PLANT +
"&Param.4=" + DIRECTION +
"&Param.5=" + DATE_FROM + "T00:00:00" +
"&Param.6=" + DATE_TO + "T23:59:59" +
"&Param.7=" + CONFTYPE +
"&Param.8=" + sLanguage,        
dataType: "xml"
};

$.UIbyID("logTab").setBusy(true);
UI5Utils.getDataModel(qParams)
// on success
.done(function (data) {
$.UIbyID("logTab").setBusy(true);
// carica il model della tabella
$.UIbyID("logTab").getModel().setData(data);
}) // End Done Function
// on fail
.fail(function () {
sap.ui.commons.MessageBox.alert(oLng_MasterData.getText("ConfLog_DataUpdateError")); //Errore nell'aggiornamento dati
})
// always, either on success or fail
.always(function () {

// remove busy indicator
$.UIbyID("logTab").setBusy(false);
var rowsetObject = $.UIbyID("logTab").getModel().getObject("/Rowset/0");
if(rowsetObject){
var iRecNumber = rowsetObject.childNodes.length - 1;
$.UIbyID("lblRecordNumber").setText(oLng_MasterData.getText("ConfLog_RecordNumber") /*"N. record" */ + " " + iRecNumber);
}  
});

$.UIbyID("logTab").setSelectedIndex(-1);
$("#splash-screen").hide();
}


// Function per creare la finestra di dialogo Operatori
function openOperatorEdit(bEdit) {
if (bEdit === undefined) bEdit = false;

// contenitore dati lista divisioni
var oModel = new sap.ui.model.xml.XMLModel();
oModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

var oModel3 = new sap.ui.model.xml.XMLModel();
oModel3.loadData(QService + dataProdMD + "Plants/getPlantsSQ");

// Crea la ComboBox per le divisioni in input
var oCmbPlantInput = new sap.ui.commons.ComboBox({
id: "cmbPlantInput",
selectedKey : bEdit?fnGetCellValue("logTab", "PLANT"):$.UIbyID("filtPlantOperator").getSelectedKey()
});
oCmbPlantInput.setModel(oModel3);
var oItemPlantInput = new sap.ui.core.ListItem();
// oItemPlantInput.bindProperty("text", "PLNAME");
oItemPlantInput.bindProperty("text", "NAME1");
oItemPlantInput.bindProperty("key", "PLANT");
oCmbPlantInput.bindItems("/Rowset/Row", oItemPlantInput);

//  Crea la finestra di dialogo
var oEdtDlg = new sap.ui.commons.Dialog({
modal:  true,
resizable: true,
id: "dlgOperator",
maxWidth: "600px",
Width: "600px",
showCloseButton: false
});

//(fnGetCellValue("logTab", "FixedVal")==1)?true : false
var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
var oForm1 = new sap.ui.layout.form.Form({
title: new sap.ui.core.Title({
text: bEdit?oLng_MasterData.getText("MasterData_ModifyOperator"):oLng_MasterData.getText("MasterData_NewOperator"),
/*"Modifica Operatore":"Nuovo Operatore",*/
//icon: "/images/address.gif",
tooltip: "Info Operatore"//oLng_Gen.getText("PrPage_MsgTT_OperatorInfo")
}),
width: "98%",
layout: oLayout1,
formContainers: [
new sap.ui.layout.form.FormContainer({
formElements: [
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Register"), //"Matricola",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "PERNR"):"",
editable: !bEdit,
id: "PERNR"
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Plant"), //"Divisione",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
editable: !bEdit,
fields: oCmbPlantInput.setEnabled(!bEdit)
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_OperatorName"), //"Nome Operatore",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "OPENAME" /*"EMNAM"*/):"",
editable: true,
id: "OPENAME" /*"EMNAM"*/
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Badge"), //"Badge",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "BADGE" /*"EMNAM"*/):"",
editable: true,
id: "BADGE" /*"EMNAM"*/
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Notes"), //"Note",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "NOTES" /*"EMNAM"*/):"",
id: "NOTES" /*"EMNAM"*/
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Enabled"), //"Abilitato",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.CheckBox({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
checked: bEdit?(fnGetCellValue("logTab", "ENABLED") == "0" ? false : true ) : true,
/*bEdit?(fnGetCellValue("logTab", "ACTIVE") == "0" ? false : true ) : false,*/
id: "ENABLED" /*"ACTIVE"*/
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Type"), //"Tipo",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "TYPE"):"",
id: "TYPE",
tooltip: oLng_MasterData.getText("MasterData_Max2Characters"), //"Massimo due caratteri"
})
]
}),
new sap.ui.layout.form.FormElement({
label: new sap.ui.commons.Label({
text: oLng_MasterData.getText("MasterData_Group"), //"Gruppo",
layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
}),
fields: [
new sap.ui.commons.TextField({
layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
value: bEdit?fnGetCellValue("logTab", "GROUP_ID"):"",
id: "GROUP_ID",
tooltip: oLng_MasterData.getText("MasterData_Max4Characters"), //"Massimo quattro caratteri"
})
]
})
]
})
]});

oEdtDlg.addContent(oForm1);
oEdtDlg.addButton(new sap.ui.commons.Button({
text: oLng_MasterData.getText("MasterData_Save"), //"Salva",
press:function(){
fnSaveOperator(/*bEdit*/);
}
}));
oEdtDlg.addButton(new sap.ui.commons.Button({
text: oLng_MasterData.getText("MasterData_Cancel"), //"Annulla",
press:function(){
oEdtDlg.close();
oEdtDlg.destroy();
oCmbPlant.destroy();
oCmbPlantInput.destroy();
}
}));
oEdtDlg.open();
}

// Funzione per salvare le modifiche alla pressa o inserirla
function fnSaveOperator(/*bEdit*/)
{
var cid = $.UIbyID("PERNR").getValue();
var plant = $.UIbyID("cmbPlantInput").getSelectedKey();
var opeName = $.UIbyID("OPENAME").getValue();
var badge = $.UIbyID("BADGE").getValue();
var notes = $.UIbyID("NOTES").getValue();
var active = $.UIbyID("ENABLED"/*"ACTIVE"*/).getChecked() === true ? "1" : "0";
var type = $.UIbyID("TYPE").getValue();
var group_id = $.UIbyID("GROUP_ID").getValue();
/*
QUERY CHIAMATA IN SIGIT, ATTENZIONE
var qexe = dataProdMD + "UpdCIDbyPlant&Param.1="  + cid + "&Param.2=" + plant + "&Param.3=" + active;
*/
var qexe =  dataProdMD +
"Operators/addOperatorsSQ&Param.1=" + plant +
"&Param.2=" + opeName +
"&Param.3=" + badge +
"&Param.4=" + notes +
"&Param.5=" + active +
"&Param.6=" + type +
"&Param.7=" + group_id +
"&Param.8=" + cid;
var ret = fnExeQuery(qexe, oLng_MasterData.getText("MasterData_OperatorSaved"),
oLng_MasterData.getText("MasterData_OperatorSavedError")
/*"Operatore salvato", "Errore salvataggio operatore"*/,
/*CONTROLLARE A COSA SERVE L'ULTIMO PARAMETRO, PASSATO FALSE*/
false);
if (ret){
$.UIbyID("dlgOperator").close();
$.UIbyID("dlgOperator").destroy();
refreshTabOperator();
}
}

function fnDelOperator(){

sap.ui.commons.MessageBox.show(
oLng_MasterData.getText("MasterData_OperatorDeleting"), //"Eliminare l'operatore selezionato?",
sap.ui.commons.MessageBox.Icon.WARNING,
oLng_MasterData.getText("MasterData_DeletingConfirm"), //"Conferma eliminazione",
[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
function(sResult){
if (sResult == 'YES'){
var qexe = dataProdMD + "Operators/delOperatorsSQ" +
"&Param.1=" + fnGetCellValue("logTab", "OPEID");
var ret = fnSQLQuery(
qexe,
oLng_MasterData.getText("MasterData_OperatorDeleted"), //"Operatore eliminato correttamente",
oLng_MasterData.getText("MasterData_OperatorDeletedError"), //"Errore in eliminazione operatore",
false
);
refreshTabOperator();
$.UIbyID("btnModOperator").setEnabled(false);
$.UIbyID("btnDelOperator").setEnabled(false);
}
},
sap.ui.commons.MessageBox.Action.YES
);
}


/**
* Crea una combobox con valori da sorgente dati
* @param id : ID univoco
* @param key : chiave
* @param text : testo
* @param source : sorgente dati
* @returns: oggetto sap.ui.commons.ComboBox
*/
//function comboBoxQuery( id, key, text, query, def="", edit=true )
function comboBoxQuery( id, key, text, query, def, edit )
{
if ( def === undefined ) def = "";
if ( edit === undefined ) edit = true;

// contenitore dati lista
var oModel = new sap.ui.model.xml.XMLModel();
oModel.loadData( QService + dataProdMD + query );

var oComboBox = new sap.ui.commons.ComboBox(id,  {selectedKey : ""});
oComboBox.setEditable(edit);
if( def !== "" ) {
oComboBox.setValue(def);
}
oComboBox.setValue(def);
oComboBox.setWidth("300px");
oComboBox.setModel(oModel);
var oItem = new sap.ui.core.ListItem();
oItem.bindProperty("text", text);
oItem.bindProperty("key", key);
oComboBox.bindItems("/Rowset/Row", oItem);
return oComboBox;

}  // function comboBoxQuery( id, key, text, query, def, edit )

function fnCreateCombo( bEdit, id, key, text, query, value ) {
if ( value === 'undefined' ) value = "";

var oModel4 = new sap.ui.model.xml.XMLModel();
oModel4.loadData(query);

// Crea la ComboBox per le divisioni in input
var oComboBox = new sap.ui.commons.ComboBox({
id: id,
selectedKey : bEdit?fnGetCellValue("ConfdashboardTab", key):value,
width: "150px"
});
oComboBox.setModel(oModel4);
var oItemScrabReasonInput = new sap.ui.core.ListItem();
oItemScrabReasonInput.bindProperty("text", text);
oItemScrabReasonInput.bindProperty("key", key);
oComboBox.bindItems("/Rowset/Row", oItemScrabReasonInput);

return oComboBox;
}

// Function per creare la finestra di dialogo File
function ShowBlsDett() {

var tabDet = fnGetAjaxData("service=BLSManager&Mode=viewlog&content-type=text/xml&id=" + fnGetCellValue("logTab", "TRANSID"), false);
var txtDet = "";
$(tabDet).find("Row").each(function (idx, row) {

txtDet = txtDet + $(row).find("Message").text() + "\n";
});

var ctrlArea = new sap.ui.commons.TextArea({
rows: 30,
cols: 180,
value: txtDet
});
//  Crea la finestra di dialogo
var oEdtDlg = new sap.ui.commons.Dialog({
modal: true,
resizable: true,
title: "Dettaglio per transazione " + fnGetCellValue("logTab", "TRANSID"),
id: "dlgHState",
maxWidth: "1024px",
Width: "900px",
maxHeight: "600px",
Height: "500px",
showCloseButton: true
});

oEdtDlg.addContent(ctrlArea);
oEdtDlg.addButton(new sap.ui.commons.Button({
text: "Chiudi",
press: function () {
oEdtDlg.close();
oEdtDlg.destroy()
}
}));
oEdtDlg.open();
}

