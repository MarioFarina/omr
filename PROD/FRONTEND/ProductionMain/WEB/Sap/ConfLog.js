// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataColl = "Content-Type=text/XML&QueryTemplate=DataCollector/";
var dataProdMD = "Content-Type=text/XML&QueryTemplate=ProductionMain/MasterData/";
var dataConfProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/Confirm/ConfProd/";
var dataConfProdFormatted = "ProductionMain/Confirm/ConfProd/";
var dataOrders = "Content-Type=text/XML&QueryTemplate=Production/Orders/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MasterData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/MasterData/res/masterData_res.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
    cache: false
 });


Libraries.load(
	[
        "/XMII/CM/Common/MII_core/UI5_utils",
        "/XMII/CM/ProductionMain/Sap/fn/fn_ConfLog"
    ],
    function () {
        $(document).ready(function () {
            var oCmbPlant = new sap.ui.commons.ComboBox("filtPlantConfProd", {
				selectedKey: "2101",
				tooltip: oLng_MasterData.getText("ConfProd_Plant"), //"Filtro Divisioni",
				change: function(oEvent){
                    /*
					$.UIbyID("ShiftTab").filter(oPlantFShift,$("#cmbPlant-input").val());
                    */
				}
			});
			
			var oItemPlant = new sap.ui.core.ListItem();
			oItemPlant.bindProperty("text", "NAME1");
			oItemPlant.bindProperty("key", "PLANT");
			oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
			
			var oPlantsModel = new sap.ui.model.xml.XMLModel()
			oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
			oCmbPlant.setModel(oPlantsModel);
            oCmbPlant.setSelectedKey("2101");
            
            var oTable1 = createTabConfLog();
            
	oCmbPlant.attachChange(
		function(oEvent){
			refreshTabOperator();
		}
	);

            var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
//					new sap.ui.commons.Label().setText( oLng_MasterData.getText("ConfProd_Plant") ),
//					oCmbPlant,
					oTable1
				],
				width: "102%"
			});
			
			oVerticalLayout.placeAt("MasterCont");
        });
    }
);
//jQuery.getScript2( "/XMII/CM/Production/PrMaster/fn_operators.js" );
		 /*function( data, textStatus, jqxhr ) {
            //console.log( data ); // Data returned
            console.log( textStatus ); // Success
            console.log( jqxhr.status ); // 200
            console.log( "Load was performed." );
	    });*/


/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/


