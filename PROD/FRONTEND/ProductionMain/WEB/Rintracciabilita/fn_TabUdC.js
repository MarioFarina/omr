/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità - Tab4 Storico UdC #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

//Creazione Tabella
function createTabUdC() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "IDTabUdC", //#Personalizzare IDTab
        properties: {
            title: oLng_res.getText("res_TabUdC"),
            visibleRowCount: 10,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
            
            //Abilito o disalibilito i pulsanti se è selezionata la riga
            rowSelectionChange: function (oControlEvent) {
                if ($.UIbyID("IDTabUdC").getSelectedIndex() == -1) {
                    $.UIbyID("btnDetailUdC").setEnabled(false);
                } else {
                    $.UIbyID("btnDetailUdC").setEnabled(true);
                }
            }
            
        },
        exportButton: true,

        //Creo i pulsanti della toolbar
        toolbarItems: [
                new sap.ui.commons.Button({
                text: oLng_res.getText("res_DetailUdC"),
                id: "btnDetailUdC",
                icon: 'sap-icon://detail-view',
                tooltip: oLng_res.getText("res_DetailUdCTooltip"),
                enabled: false,
                press: function () {
                    detailUdC();
                }
            })
        ],
        
        //Creo le colonne della tabella
        columns: [
            //#Personalizzare i campi
            {
                Field: "UDCNR",
                label: oLng_res.getText("res_UdC"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "PZUDC",
                label: oLng_res.getText("res_PzUdC"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "IDLINE",
                label: oLng_res.getText("res_Line"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "ORDER",
                label: oLng_res.getText("res_Order"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "MATERIAL",
                label: oLng_res.getText("res_Material"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "OPENAME",
                label: oLng_res.getText("res_OpeName"),
                properties: {
                    width: "50px",
                    visible: true
                }
            }

        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}

function detailUdC() {
    //Crea la finestra di dialogo  
    var oDialog = new sap.ui.commons.Dialog({
        modal: true,
        resizable: true,
        id: "DialogBox",
        maxWidth: "900px",
        Width: "900px",
        showCloseButton: false,
        title: oLng_res.getText("res_DetailUdC"),
    });

    var oLayout = new sap.ui.layout.form.GridLayout({
        singleColumn: true
    });
    
    var oTable = createTabDetailUdC();
    oDialog.addContent(oTable);
    
    oDialog.addButton(new sap.ui.commons.Button({
        text: oLng_res.getText("res_Esc"),
        press: function () {
            oDialog.close();
            oDialog.destroy();
        }
    }));
    oDialog.open();
}

function createTabDetailUdC() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "IDTabDetailUdC", //#Personalizzare IDTab
        properties: {
            //title: oLng_res.getText("res_DetailUdC"),
            visibleRowCount: 10,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,  
        },
        exportButton: true,
        
        //Creo le colonne della tabella
        columns: [
            //#Personalizzare i campi
            {
                Field: "UDCNR",
                label: oLng_res.getText("res_UdC"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "ORDER",
                label: oLng_res.getText("res_Order"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "MATERIAL",
                label: oLng_res.getText("res_Material"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "SERIAL",
                label: oLng_res.getText("res_DataMatrix"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "QTY",
                label: oLng_res.getText("res_Qty"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "OPEID",
                label: oLng_res.getText("res_Badge"),
                properties: {
                    width: "50px",
                    visible: true
                }
            }
            
        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    getDetailUdC();
    
    return oTable;
}

function getDetailUdC() {
var sParams = fnGetCellValue("IDTabUdC", "UDCNR");
    var qexe = QService + "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/UDC/getSerialsByUDCnrSQ&Param.1=" + sParams;
    $.UIbyID("IDTabDetailUdC").getModel().loadData(qexe);
}
