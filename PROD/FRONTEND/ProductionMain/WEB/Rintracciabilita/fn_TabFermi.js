/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Rintracciabilità - Tab5 Storico Fermi Macchina #Personalizzare
//Author: Elena Andrini
//Date:   28/02/2017
//Vers:   1.0
//**************************************************************************************

//Creazione Tabella
function createTabFermi() {
    var oTable = UI5Utils.init_UI5_Table({
        id: "IDTabFermi", //#Personalizzare IDTab
        properties: {
            title: oLng_res.getText("res_TabFermi"),
            visibleRowCount: 10,
            width: "98%",
            firstVisibleRow: 0,
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive
        },
        exportButton: true,

        //Creo le colonne della tabella
        columns: [
            //#Personalizzare i campi
            {
                Field: "IDLINE",
                label: oLng_res.getText("res_Line"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "SHIFT",
                label: oLng_res.getText("res_Shift"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "DATE_SHIFT",
                label: oLng_res.getText("res_DateShift"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "Date",
                }
            },
            {
                Field: "DATE_FROM",
                label: oLng_res.getText("res_DateFrom"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "DateTime",
                }
            },
            {
                Field: "DATE_TO",
                label: oLng_res.getText("res_DateTo"),
                properties: {
                    width: "50px",
                    visible: true
                },
                template:{
                    type: "DateTime",
                }
            },
            {
                Field: "DURATION",
                label: oLng_res.getText("res_Duration"),
                properties: {
                    width: "50px",
                    visible: true
                }
            },
            {
                Field: "REASTXT",
                label: oLng_res.getText("res_Reastxt"),
                properties: {
                    width: "50px",
                    visible: true
                }
            }

        ]
    });

    oModel = new sap.ui.model.xml.XMLModel();
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");

    return oTable;
}