//**************************************************************************************
//Title:  SSETSORDERSYPE
//Author: Bruno Rosati
//Date:   10/12/2018
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSORDERSYPE

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

function createTabSSetSOrdersType() {
    
    var oTable = UI5Utils.init_UI5_Table({
        id: "SSetSOrdersTypeTab",
        properties: {
            visibleRowCount: 15,
			fixedColumnCount: 0,
			width: "100%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetSOrdersTypeTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSOrdersType").setEnabled(false);
        			$.UIbyID("btnDelSOrdersType").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSOrdersType").setEnabled(true);
                    $.UIbyID("btnDelSOrdersType").setEnabled(true);			
                }
            },
			toolbar: new sap.ui.commons.Toolbar({
				items: [
					new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Add"),
                        id:   "btnAddSOrdersType",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openSOrdersTypeDlg(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Modify"),
                        id:   "btnModSOrdersType",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSOrdersTypeDlg(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Delete"),
                        id:   "btnDelSOrdersType",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelSOrdersType();
                        }
                    })
				],
				rightItems: [
					new sap.ui.commons.Button({
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabSetSOrdersType();
                        }
                    })
				]
			})
        },
        exportButton: false,
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "ORDTYPE", 
				label: oLng_Settings.getText("Settings_ORDTYPE"),
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "LANGID", 
				properties: {
					visible: false,
                    flexible : false
				}
            },
            {
				Field: "LANGID_TEXT", 
				label: oLng_Settings.getText("Settings_LANGID"),
				properties: {
					width: "135px",
                    flexible : false
				}
            },            
            {
				Field: "ORDFILTER", 
				properties: {
					visible: false,
                    flexible : false
				}
            },
            {
				Field: "ORDFILTER_TEXT",
				label: oLng_Settings.getText("Settings_ORDFILTER"),
                properties: {
					width: "135px",
                    flexible : false
				}
            },
            {
				Field: "TYPEDESC", 
				label: oLng_Settings.getText("Settings_TYPEDESC"),
				properties: {
					width: "145px",
                    flexible : false
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSetSOrdersType();
    return oTable;
}

function refreshTabSetSOrdersType() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("SSetSOrdersTypeTab").getModel().loadData(
            QService + dataProdSett +
            "OrdersType/getSOrdersTypeByParamsSQ&Param.1=" + $('#plant').val() + "&Param.2=" + sLanguage
        );
    }
    else{
        $.UIbyID("SSetSOrdersTypeTab").getModel().loadData(
            QService + dataProdSett +
            "OrdersType/getSOrdersTypeByParamsSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + sLanguage
        );
    }
    $.UIbyID("SSetSOrdersTypeTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function openSOrdersTypeDlg(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbLangId = new sap.ui.commons.ComboBox("cmbLangId", {
        enabled: !bEdit,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {}
    });
    var oItemLangId = new sap.ui.core.ListItem();
    oItemLangId.bindProperty("key", "name");
    oItemLangId.bindProperty("text", "value");
    oCmbLangId.bindItems("/Rowset/Row", oItemLangId);
    var oLangIdModel = new sap.ui.model.xml.XMLModel();
    oLangIdModel.loadData(QService + dataProd + "Translate/getLanguagesListQR");
    oCmbLangId.setModel(oLangIdModel);
    
    var oCmbOrdFilter = new sap.ui.commons.ComboBox("cmbOrdFilter", {
        enabled: !bEdit,
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {}
    });
    var oItemOrdFilter = new sap.ui.core.ListItem();
    oItemOrdFilter.bindProperty("key", "ORDFILTER");
    oItemOrdFilter.bindProperty("text", "ORDFILTER_TEXT");
    oCmbOrdFilter.bindItems("/Rowset/Row", oItemOrdFilter);
    var oOrdFilterModel = new sap.ui.model.xml.XMLModel();
    oOrdFilterModel.loadData(QService + dataProdSett + "OrdersType/getOrdFiltersByLanguageSQ&Param.1=" + sLanguage);
    oCmbOrdFilter.setModel(oOrdFilterModel);
    
    if(bEdit){
        oCmbLangId.setSelectedKey(fnGetCellValue("SSetSOrdersTypeTab", "LANGID"));
        oCmbOrdFilter.setSelectedKey(fnGetCellValue("SSetSOrdersTypeTab", "ORDFILTER"));
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSOrdersType",
        maxWidth: "800px",
        maxHeight: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyVoice"):oLng_Settings.getText("Settings_NewVoice")
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ORDTYPE"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue("SSetSOrdersTypeTab", "ORDTYPE"):"",
                                maxLength: 4,
                                id: "txtORDTYPE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_LANGID"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbLangId
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_ORDFILTER"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbOrdFilter
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_TYPEDESC"),
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"}),
                                value: bEdit?fnGetCellValue("SSetSOrdersTypeTab", "TYPEDESC"):"",
                                maxLength: 100,
                                id: "txtTYPEDESC"
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"),
        press:function(){
            fnSaveSOrdersType(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"),
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbOrdFilter.destroy();
            oCmbLangId.destroy();
        }
    }));    
	oEdtDlg.open();
}

function fnSaveSOrdersType(bEdit) 
{
    var sPlant = ($.UIbyID("filtPlant").getSelectedKey() === "") ? $('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
    var sOrdType = encodeURIComponent($.UIbyID("txtORDTYPE").getValue());
    if(sOrdType == "" || sOrdType == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_ORDTYPE") + "'"
        );
        return;
    }
    var sLangId = $.UIbyID("cmbLangId").getSelectedKey();
    if(sLangId == "" || sLangId == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_LANGID") + "'"
        );
        return;
    }
    var sOrdFilter = $.UIbyID("cmbOrdFilter").getSelectedKey();
    if(sOrdFilter == "" || sOrdFilter == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_ORDFILTER") + "'"
        );
        return;
    }
    var sTypeDesc = encodeURIComponent($.UIbyID("txtTYPEDESC").getValue());
    if(sTypeDesc == "" || sTypeDesc == null){
        sap.ui.commons.MessageBox.alert(
            oLng_Settings.getText("Settings_CompileMandatoryField") + " '" + 
            oLng_Settings.getText("Settings_TYPEDESC") + "'"
        );
        return;
    }
    
    var qexe = dataProdSett + "OrdersType/";    
    if(bEdit){
        qexe = qexe + "updSOrdersTypeSQ";
    }
    else{
        qexe = qexe + "addSOrdersTypeSQ";
    }
    
    qexe = qexe +
         "&Param.1=" + sPlant +
         "&Param.2=" + sOrdType +
         "&Param.3=" + sLangId +
         "&Param.4=" + sOrdFilter +
         "&Param.5=" + sTypeDesc;
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_VoiceSaved"),
                         oLng_Settings.getText("Settings_VoiceSavedError"),
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSOrdersType").close();
        $.UIbyID("dlgSOrdersType").destroy();
        $.UIbyID("SSetSOrdersTypeTab").setSelectedIndex(-1);
        $.UIbyID("btnModSOrdersType").setEnabled(false);
        $.UIbyID("btnDelSOrdersType").setEnabled(false);
        refreshTabSetSOrdersType();
    }
}

function fnDelSOrdersType(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_VoiceDeleting"),
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"),
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "OrdersType/delSOrdersTypeSQ" +
                            "&Param.1=" + fnGetCellValue("SSetSOrdersTypeTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("SSetSOrdersTypeTab", "LANGID") +
                            "&Param.3=" + fnGetCellValue("SSetSOrdersTypeTab", "ORDFILTER");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_VoiceDeleted"),
                    oLng_Settings.getText("Settings_VoiceDeletedError"),
                    false
                );
                refreshTabSetSOrdersType();
                $.UIbyID("btnModSOrdersType").setEnabled(false);
                $.UIbyID("btnDelSOrdersType").setEnabled(false);
                $.UIbyID("SSetSOrdersTypeTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}