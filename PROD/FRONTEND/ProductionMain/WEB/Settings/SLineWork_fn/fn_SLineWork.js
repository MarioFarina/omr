/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  SLINEWORK
//Author: Bruno Rosati
//Date:   09/06/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina SLINEWORK

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Material e ritorna l'oggetto oTable
function createTabSLineWork() {
    
    // contenitore dati lista causali fermo
    
    //Crea L'oggetto Tabella Material
    var oTable = UI5Utils.init_UI5_Table({
        id: "SLineWorkTab",
        properties: {
            title: oLng_Settings.getText("Settings_ProductionProg"), //"Programmazione Produzione",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SLineWorkTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSLine").setEnabled(false);
        			$.UIbyID("btnDelSLine").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSLine").setEnabled(true);
                    $.UIbyID("btnDelSLine").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Add"), //"Aggiungi",
                id:   "btnAddSLine",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openSLineWorkEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Modify"), //"Modifica",
                id:   "btnModSLine",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openSLineWorkEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Delete"), //"Elimina"
                id:   "btnDelSLine",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelSLineWork();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabSLineWork();
                }
            })
        ],
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1",
				label: oLng_Settings.getText("Settings_Plant"), //"Divisione",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "IDLINE",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Settings.getText("Settings_Line"), //"Linea",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "ORDER", 
				label: oLng_Settings.getText("Settings_Order"), //"Commessa",
				properties: {
					width: "80px"
				}
            },            
            {
				Field: "POPER", 
				label: oLng_Settings.getText("Settings_Operation"), //"Fase",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "MATERIAL",
				label: oLng_Settings.getText("Settings_MaterialCode"), //"Materiale",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "DESC", 
				label: oLng_Settings.getText("Settings_MaterialDescr"), //"Descrizione materiale",
				properties: {
					width: "200px"
				}
            },
            {
				Field: "STANDARD_TIME", 
				label: oLng_Settings.getText("Settings_StandardTime"), //"Tempo standard",
				properties: {
					width: "80px"
				},
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
				Field: "CYCLE_TIME", 
				label: oLng_Settings.getText("Settings_DetectedTime"), //"Tempo rilevato",
				properties: {
					width: "80px"
				},
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            }
        ],
        /*
        extension: new sap.ui.commons.Toolbar({
            items: []
        })
        */
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSLineWork();
    return oTable;
}

// Aggiorna la tabella Material
function refreshTabSLineWork() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("SLineWorkTab").getModel().loadData(
            QService + dataProdSett +
            "getSLineWorkByPlantSQ&Param.1=" + $('#plant').val()
        );
    }
    else{
        $.UIbyID("SLineWorkTab").getModel().loadData(
            QService + dataProdSett +
            "getSLineWorkByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
        );
    }
    $.UIbyID("SLineWorkTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Funzioni
function openSLineWorkEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbPlant = new sap.ui.commons.ComboBox("cmbPlant", {
        selectedKey: $.UIbyID("filtPlant").getSelectedKey(),
        enabled: false,
        change: function (oEvent) {}
    });
    var oItemPlant = new sap.ui.core.ListItem();
    oItemPlant.bindProperty("key", "PLANT");
    oItemPlant.bindProperty("text", "NAME1");
    oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
    var oPlantsModel = new sap.ui.model.xml.XMLModel();
    oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
    oCmbPlant.setModel(oPlantsModel);
    
    var oCmbLine = new sap.ui.commons.ComboBox("cmbLine", {
        enabled: !bEdit,
        change: function (oEvent) {}
    });
    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    oItemLine.bindProperty("text", "LINETXT");
    oCmbLine.bindItems("/Rowset/Row", oItemLine);
    var oLineModel = new sap.ui.model.xml.XMLModel();
    oLineModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    oCmbLine.setModel(oLineModel);
    
    var oCmbMaterial = new sap.ui.commons.ComboBox("cmbMaterial", {
        change: function (oEvent) {}
    });
    var oItemMaterial = new sap.ui.core.ListItem();
    oItemMaterial.bindProperty("key", "MATERIAL");
    oItemMaterial.bindProperty("text", "DESCRIPTION");
    oCmbMaterial.bindItems({path: "/Rowset/Row", length: 1000, template: oItemMaterial} );
    var oMaterialModel = new sap.ui.model.xml.XMLModel();
    oMaterialModel.loadData(QService + dataProdMD + "Material/getMaterialDescByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    oCmbMaterial.setModel(oMaterialModel);
    
    if(bEdit){
        oCmbPlant.setSelectedKey(fnGetCellValue("SLineWorkTab", "PLANT"));
        oCmbLine.setSelectedKey(fnGetCellValue("SLineWorkTab", "IDLINE"));
        oCmbMaterial.setSelectedKey(fnGetCellValue("SLineWorkTab", "MATERIAL"));
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSLineWork",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyProg"):oLng_Settings.getText("Settings_NewProg"),
            /*"Modifica programmazione":"Nuova programmazione",*/
            //icon: "/images/address.gif",
            /*tooltip: */
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbPlant
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbLine
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Order"), //"Commessa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SLineWorkTab", "ORDER"):"",
                                enabled: !bEdit,
                                maxLength: 12,
                                id: "ORDER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Operation"), //"Fase",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SLineWorkTab", "POPER"):"",
                                enabled: !bEdit,
                                maxLength: 4,
                                id: "POPER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_MaterialCode"), //"Materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbMaterial
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_StandardTime"), //"Tempo standard",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                value: bEdit?parseFloat(fnGetCellValue("SLineWorkTab", "STANDARD_TIME")).toFixed(3):"0",
                                editable: true,
                                maxLength: 11,
                                id: "STANDARD_TIME",
                                liveChange: function(ev){
                                    /* VERSIONE DI ALEX PER LA GESTIONE DEL CAMPO SOLO NUMERICO CON DECIMALI*/
                                    var oldVal = this['oldValue'];
                                    var val = ev.getParameter('liveValue');
                                    var oldFocus = this['oldFocus'];
                                    var newval = val.replace(/[^.\d]/g, '');
                                    if(newval.split(".").length - 1 > 1)
                                        newval = oldVal;
                                    this.setValue(newval);
                                    this['oldValue'] = newval;
                                    //console.log(JSON.stringify(oldFocus));
                                    if(newval === oldVal) {
                                        //console.log(JSON.stringify(newval));
                                        this.applyFocusInfo({cursorPos:oldFocus.cursorPos});
                                    }
                                    else
                                        this['oldFocus'] = this.getFocusInfo();
                                }
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_DetectedTime"), //"Tempo di rilevato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                value: bEdit?parseFloat(fnGetCellValue("SLineWorkTab", "CYCLE_TIME")).toFixed(3):"0",
                                editable: true,
                                maxLength: 11,
                                id: "CYCLE_TIME",
                                liveChange: function(ev){
                                    /* VERSIONE DI ALEX PER LA GESTIONE DEL CAMPO SOLO NUMERICO CON DECIMALI*/
                                    var oldVal = this['oldValue'];
                                    var val = ev.getParameter('liveValue');
                                    var oldFocus = this['oldFocus'];
                                    var newval = val.replace(/[^.\d]/g, '');
                                    if(newval.split(".").length - 1 > 1)
                                        newval = oldVal;
                                    this.setValue(newval);
                                    this['oldValue'] = newval;
                                    //console.log(JSON.stringify(oldFocus));
                                    if(newval === oldVal) {
                                        //console.log(JSON.stringify(newval));
                                        this.applyFocusInfo({cursorPos:oldFocus.cursorPos});
                                    }
                                    else
                                        this['oldFocus'] = this.getFocusInfo();
                                }
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"), //"Salva",
        press:function(){
            fnSaveSLineWork(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlant.destroy();
            oCmbLine.destroy();
            oCmbMaterial.destroy();
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche al record della SLINEWORK o inserire una nuova voce
function fnSaveSLineWork(bEdit) 
{
    var plant = $.UIbyID("cmbPlant").getSelectedKey();
    var line = "";
    line = $.UIbyID("cmbLine").getSelectedKey();
    if(line == "" || line == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectLine")); //Selezionare una linea
        return;
    }
    var order = "";
    order = $.UIbyID("ORDER").getValue();
    if(order == "" || order == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_InsertOrder")); //Inserire una commessa
        return;
    }
    var poper = "";
    poper = $.UIbyID("POPER").getValue();
    var material = "";
    material = $.UIbyID("cmbMaterial").getSelectedKey();
    if(material == "" || material == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectMaterial")); //Selezionare un materiale
        return;
    }
    var cycle_time = $.UIbyID("CYCLE_TIME").getValue();
    if(cycle_time == "" || cycle_time == null){
        cycle_time = "0";
    }
    var standard_time = $.UIbyID("STANDARD_TIME").getValue();
    if(standard_time == "" || standard_time == null){
        standard_time = "0";
    }
    
    var qexe = "";
    
    if(bEdit){
        qexe =  dataProdSett +
            "updSLineWorkSQ&Param.1=" + line +
            "&Param.2=" + order +
            "&Param.3=" + poper +
            "&Param.4=" + material +
            "&Param.5=" + plant +
            "&Param.6=" + cycle_time +
            "&Param.7=" + standard_time;
    }
    else{
        qexe =  dataProdSett +
            "addSLineWorkSQ&Param.1=" + line +
            "&Param.2=" + order +
            "&Param.3=" + poper +
            "&Param.4=" + material +
            "&Param.5=" + plant +
            "&Param.6=" + cycle_time +
            "&Param.7=" + standard_time;
    }
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_ProgSaved"), //"Programmazione salvata correttamente"
                         oLng_Settings.getText("Settings_ProgSavedError"), //"Errore salvataggio programmazione"
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSLineWork").close();
        $.UIbyID("dlgSLineWork").destroy();
        $.UIbyID("SLineWorkTab").setSelectedIndex(-1);
        $.UIbyID("btnModSLine").setEnabled(false);
        $.UIbyID("btnDelSLine").setEnabled(false);
        refreshTabSLineWork();
    }
}

function fnDelSLineWork(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_ProgDeleting"), //"Eliminare la programmazione selezionata?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "delSLineWorkSQ" +
                            "&Param.1=" + fnGetCellValue("SLineWorkTab", "IDLINE") +
                            "&Param.2=" + fnGetCellValue("SLineWorkTab", "ORDER") +
                            "&Param.3=" + fnGetCellValue("SLineWorkTab", "POPER");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_ProgDeleted"), //"Programmazione eliminata correttamente",
                    oLng_Settings.getText("Settings_ProgDeletedError"), //"Errore in eliminazione programmazione",
                    false
                );
                refreshTabSLineWork();
                $.UIbyID("btnModSLine").setEnabled(false);
                $.UIbyID("btnDelSLine").setEnabled(false);
                $.UIbyID("SLineWorkTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}
