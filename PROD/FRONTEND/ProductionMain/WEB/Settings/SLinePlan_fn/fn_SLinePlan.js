/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  SLINEPLAN
//Author: Bruno Rosati
//Date:   12/06/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina SLINEPLAN (Pianificazione Produzione)

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

//Array con le traduzioni dei mesi
var sMonths = [
    oLng_Settings.getText("Settings_January"),oLng_Settings.getText("Settings_February"),oLng_Settings.getText("Settings_March"),
    oLng_Settings.getText("Settings_April"),oLng_Settings.getText("Settings_May"),oLng_Settings.getText("Settings_June"),
    oLng_Settings.getText("Settings_July"),oLng_Settings.getText("Settings_August"),oLng_Settings.getText("Settings_September"),
    oLng_Settings.getText("Settings_October"),oLng_Settings.getText("Settings_November"),oLng_Settings.getText("Settings_December")
];

//Data attuale
var currentDate = new Date();

// Function che crea la tabella SLinePlanTab e ritorna l'oggetto oTable
function createTabSLinePlan() {
    
    var currentPlant;
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        currentPlant = $('#plant').val();
    }
    else{
        currentPlant = $.UIbyID("filtPlant").getSelectedKey();
    }
    var oCmbLineFilter = new sap.ui.commons.ComboBox("CmbLineFilter", {
        change: function (oEvent) {
            refreshTabSLinePlan(false);
        }
    });
    var oItemLineFilter = new sap.ui.core.ListItem();
    oItemLineFilter.bindProperty("key", "IDLINE");
    oItemLineFilter.bindProperty("text", "LINETXT");
    oCmbLineFilter.bindItems("/Rowset/Row", oItemLineFilter);
    var oLineFilterModel = new sap.ui.model.xml.XMLModel();
    oLineFilterModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + currentPlant);
    oCmbLineFilter.setModel(oLineFilterModel);
    
    var jType = {root:[]};
    for(var iMonth = 0; iMonth < sMonths.length; iMonth++){
        jType.root.push({
            key: (iMonth + 1),
            text: sMonths[iMonth]
        });
    }    
  
    var oMonthModelFilter1 = new sap.ui.model.json.JSONModel();
    oMonthModelFilter1.setData(jType);
    console.log(jType);
        
    // Crea la ComboBox per selezionare i mesi dell'anno da filtrare
    var oCmbMonthFilter1 = new sap.ui.commons.ComboBox({
        id: "cmbMonthFilter1",
        selectedKey: currentDate.getMonth() + 1,
        change: function() {}
    });
    oCmbMonthFilter1.setModel(oMonthModelFilter1);
    var oItemMonthFilter1 = new sap.ui.core.ListItem();
    oItemMonthFilter1.bindProperty("text", "text");
	oItemMonthFilter1.bindProperty("key", "key");	
	oCmbMonthFilter1.bindItems("/root", oItemMonthFilter1);
    
    var oMonthModelFilter2 = new sap.ui.model.json.JSONModel();
    oMonthModelFilter2.setData(jType);
    console.log(jType);
        
    // Crea la ComboBox per selezionare i mesi dell'anno da filtrare
    var oCmbMonthFilter2 = new sap.ui.commons.ComboBox({
        id: "cmbMonthFilter2",
        selectedKey: currentDate.getMonth() + 1,
        change: function() {}
    });
    oCmbMonthFilter2.setModel(oMonthModelFilter2);
    var oItemMonthFilter2 = new sap.ui.core.ListItem();
    oItemMonthFilter2.bindProperty("text", "text");
	oItemMonthFilter2.bindProperty("key", "key");	
	oCmbMonthFilter2.bindItems("/root", oItemMonthFilter2);
    
    //Crea L'oggetto Tabella Material
    var oTable = UI5Utils.init_UI5_Table({
        id: "SLinePlanTab",
        properties: {
            title: oLng_Settings.getText("Settings_ProductionPlan"), //"Pianificazione Produzione",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SLinePlanTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSLineP").setEnabled(false);
        			$.UIbyID("btnDelSLineP").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSLineP").setEnabled(true);
                    $.UIbyID("btnDelSLineP").setEnabled(true);			
                }
            },
            toolbar: new sap.ui.commons.Toolbar ({
                items: [
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Add"), //"Aggiungi",
                        id:   "btnAddSLineP",
                        icon: 'sap-icon://add',
                        enabled: true,
                        press: function (){
                            openSLinePlanEdit(false);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Modify"), //"Modifica",
                        id:   "btnModSLineP",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openSLinePlanEdit(true);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Delete"), //"Elimina"
                        id:   "btnDelSLineP",
                        icon: 'sap-icon://delete',
                        enabled: false,
                        press: function (){
                            fnDelLinePlan();
                        }
                    }),
                    oCmbLineFilter,
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_EmptyFilters"), //"Svuota filtri"
                        icon: "sap-icon://filter",
                        id: "delFilter",
                        press: function () {
                            $.UIbyID("CmbLineFilter").setSelectedKey("");
                            $.UIbyID("cmbYearFilter1").setValue("");
                            $.UIbyID("cmbMonthFilter1").setSelectedKey("");
                            $.UIbyID("cmbYearFilter2").setValue("");
                            $.UIbyID("cmbMonthFilter2").setSelectedKey("");
                            refreshTabSLinePlan(false);
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_Refresh"), //"Aggiorna",
                        icon: 'sap-icon://refresh',
                        enabled: true,
                        press: function (){
                            refreshTabSLinePlan(false);
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.Toolbar({
                items: [
                    new sap.ui.commons.Label({
                        text: oLng_Settings.getText("Settings_FromMonthYear"), //"Da mese/anno",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oCmbMonthFilter1,
                    new sap.ui.commons.TextField({
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                        value: currentDate.getFullYear(),
                        maxLength: 4,
                        id: "cmbYearFilter1",
                        liveChange: function(ev) {
                            var val = ev.getParameter('liveValue');
                            var newval = val.replace(/[^\d]/g, '');
                            this.setValue(newval);
                        }
                    }),
                    new sap.ui.commons.Label({
                        text: oLng_Settings.getText("Settings_toMonthYear"), //"a mese/anno",
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                    }),
                    oCmbMonthFilter2,
                    new sap.ui.commons.TextField({
                        layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                        value: currentDate.getFullYear(),
                        maxLength: 4,
                        id: "cmbYearFilter2",
                        liveChange: function(ev) {
                            var val = ev.getParameter('liveValue');
                            var newval = val.replace(/[^\d]/g, '');
                            this.setValue(newval);
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Settings.getText("Settings_FilterByMonthYear"), //"Filtra per mese/anno"
                        id:   "btnFilterByMonthYear",
                        icon: 'sap-icon://detail-view',
                        press: function (){
                            fnFilterByMonthYear();
                        }
                    })
                ]
            })
        },
        exportButton: false,
        columns: [
            {
				Field: "PLANT",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "NAME1",
				label: oLng_Settings.getText("Settings_Plant"), //"Divisione",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "IDLINE",
				properties: {
					width: "100px",
                    //campo nascosto
                    visible: false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Settings.getText("Settings_Line"), //"Linea",
				properties: {
					width: "70px"
				}
            },
            {
				Field: "MATERIAL",
				label: oLng_Settings.getText("Settings_MaterialCode"), //"Materiale",
				properties: {
					width: "100px"
				}
            },
            {
				Field: "DESC", 
				label: oLng_Settings.getText("Settings_MaterialDescr"), //"Descrizione materiale",
				properties: {
					width: "180px"
				}
            },
            {
				Field: "YEAR", 
				label: oLng_Settings.getText("Settings_Year"), //"Anno",
				properties: {
					width: "55px"
				},
                template: {
                    //type: "TextView",
                    textAlign: "Right"
                }
            },
            {
				Field: "MONTH", 
				label: oLng_Settings.getText("Settings_Month"), //"Mese",
                properties: {
					width: "70px"
				}
            },
            {
                /*  Il campo month viene ripetuto con questo nome per avere in output un XML a cui sia possibile sostituire il numero del mese
                    con il proprio nome (viene fatto nel refresh, sostituendo il numero con il rispettivo nome dal foglio delle traduzioni)
                    senza però perdere il numero del mese che va poi recuperato per le varie query
                */
				Field: "MONTHNUMBER", 
				//label: oLng_Settings.getText("Settings_Month"), //"Mese",
				properties: {
					width: "70px",
                    visible: false
				}
            },
            {
                /*  Questo campo recuperà il nome del mese in base al numero direttamente via SQL ma è solo in inglese, al momento è superfluo */
				Field: "MONTHNAME", 
				//label: oLng_Settings.getText("Settings_Month"), //"Mese",
				properties: {
					width: "70px",
                    visible: false
				}
            },
            {
				Field: "QUANTITY", 
				label: oLng_Settings.getText("Settings_Quantity"), //"Quantità",
				properties: {
					width: "70px"
				},
                template: {
                    type: "Numeric",
                    textAlign: "Right"
                }
            },
            {
				Field: "USERNAME", 
				label: oLng_Settings.getText("Settings_Username"), //"Username",
				properties: {
					width: "80px"
				}
            },
            {
				Field: "FULLNAME", 
				label: oLng_Settings.getText("Settings_User"), //"Utente",
				properties: {
					width: "80px"
				}
            },            
            {
				Field: "DATEADD", 
				label: oLng_Settings.getText("Settings_DateAdd"), //"Data aggiunta",
				properties: {
					width: "110px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "DATEMOD", 
				label: oLng_Settings.getText("Settings_DateMod"), //"Data modifica",
				properties: {
					width: "110px"
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSLinePlan(false);
    return oTable;
}

// Aggiorna la tabella SLinePlan
function refreshTabSLinePlan(byDate) {
    
    var queryToBeExecuted = "";
    var queryText = "";
    if(byDate){
        queryText = "getSLinePlanByPlant_DateSQ&Param.1="
    }
    else{
        $.UIbyID("cmbYearFilter1").setValue("");
        $.UIbyID("cmbMonthFilter1").setSelectedKey("");
        $.UIbyID("cmbMonthFilter1").setValue("");
        $.UIbyID("cmbYearFilter2").setValue("");
        $.UIbyID("cmbMonthFilter2").setSelectedKey("");
        $.UIbyID("cmbMonthFilter2").setValue("");
        queryText = "getSLinePlanByPlantSQ&Param.1=";
    }
    
    //var data = new sap.ui.model.xml.XMLModel();
    var data;
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        queryToBeExecuted = dataProdSett + queryText + $('#plant').val() +
            "&Param.2=" + $.UIbyID("CmbLineFilter").getSelectedKey();
        if(byDate){
            queryToBeExecuted += "&Param.3=" + $.UIbyID("cmbMonthFilter1").getSelectedKey() +
                "&Param.4=" + $.UIbyID("cmbYearFilter1").getValue() +
                "&Param.5=" + $.UIbyID("cmbMonthFilter2").getSelectedKey() +
                "&Param.6=" + $.UIbyID("cmbYearFilter2").getValue();
        }
    }
    else{
        queryToBeExecuted = dataProdSett + queryText + $.UIbyID("filtPlant").getSelectedKey() +
            "&Param.2=" + $.UIbyID("CmbLineFilter").getSelectedKey();
        if(byDate){
            queryToBeExecuted += "&Param.3=" + $.UIbyID("cmbMonthFilter1").getSelectedKey() +
                "&Param.4=" + $.UIbyID("cmbYearFilter1").getValue() +
                "&Param.5=" + $.UIbyID("cmbMonthFilter2").getSelectedKey() +
                "&Param.6=" + $.UIbyID("cmbYearFilter2").getValue();
        }
    }
    data = fnGetAjaxData(queryToBeExecuted, false);
    $.each(data.childNodes[0].childNodes[0].childNodes, function (idx, item) {
        //var fName = item.attributes['Name'].textContent;
        //var fDesc = item.attributes['Description'].textContent;
        if (item.nodeName == "Row"){
            //Nell'XML generato dalla query il numero del mese si trova alla quinta posizione
            //per questa ragione si va a controllare l'indice 4
            //prestare attenzione se si cambia la posizione dei campi nella query
            switch(item.childNodes[4].textContent){
                case "1":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_January");
                    break;
                case "2":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_February");
                    break;
                case "3":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_March");
                    break;
                case "4":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_April");
                    break;
                case "5":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_May");
                    break;
                case "6":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_June");
                    break;
                case "7":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_July");
                    break;
                case "8":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_August");
                    break;
                case "9":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_September");
                    break;
                case "10":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_October");
                    break;
                case "11":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_Nobember");
                    break;
                case "12":
                    item.childNodes[4].textContent = oLng_Settings.getText("Settings_December");
                    break;
            }
        }
    });
    var dataModel = new sap.ui.model.xml.XMLModel();
    dataModel.setData(data);
    $.UIbyID("SLinePlanTab").setModel(dataModel);
    $.UIbyID("SLinePlanTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la dialog di aggiunta record
function openSLinePlanEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbPlant = new sap.ui.commons.ComboBox("cmbPlant", {
        selectedKey: $.UIbyID("filtPlant").getSelectedKey(),
        enabled: false,
        change: function (oEvent) {}
    });
    var oItemPlant = new sap.ui.core.ListItem();
    oItemPlant.bindProperty("key", "PLANT");
    oItemPlant.bindProperty("text", "NAME1");
    oCmbPlant.bindItems("/Rowset/Row", oItemPlant);
    var oPlantsModel = new sap.ui.model.xml.XMLModel();
    oPlantsModel.loadData(QService + dataProdMD + "Plants/getPlantsSQ");
    oCmbPlant.setModel(oPlantsModel);
    
    var oCmbLine = new sap.ui.commons.ComboBox("cmbLine", {
        enabled: !bEdit,
        change: function (oEvent) {}
    });
    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    oItemLine.bindProperty("text", "LINETXT");
    oCmbLine.bindItems("/Rowset/Row", oItemLine);
    var oLineModel = new sap.ui.model.xml.XMLModel();
    oLineModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    oCmbLine.setModel(oLineModel);
    
    var oCmbMaterial = new sap.ui.commons.ComboBox("cmbMaterial", {
        enabled: !bEdit,
        change: function (oEvent) {}
    });
    var oItemMaterial = new sap.ui.core.ListItem();
    oItemMaterial.bindProperty("key", "MATERIAL");
    oItemMaterial.bindProperty("text", "DESCRIPTION");
    oCmbMaterial.bindItems("/Rowset/Row", oItemMaterial);
    var oMaterialModel = new sap.ui.model.xml.XMLModel();
    oMaterialModel.loadData(QService + dataProdMD + "Material/getMaterialDescByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    oCmbMaterial.setModel(oMaterialModel);
    
    var jType = {root:[]};
    for(var iMonth = 0; iMonth < sMonths.length; iMonth++){
        jType.root.push({
            key: (iMonth + 1),
            text: sMonths[iMonth]
        });
    }    
  
    var oMonthModel = new sap.ui.model.json.JSONModel();
    oMonthModel.setData(jType);
    console.log(jType);
        
    // Crea la ComboBox per selezionare il tipo delle funzioni da visualizzare
    var oCmbMonth = new sap.ui.commons.ComboBox({
        id: "cmbMonth", 
        enabled: !bEdit,
        change: function() {}
    });
    oCmbMonth.setModel(oMonthModel);
    var oItemMonth = new sap.ui.core.ListItem();
    oItemMonth.bindProperty("text", "text");
	oItemMonth.bindProperty("key", "key");	
	oCmbMonth.bindItems("/root", oItemMonth);
    
    if(bEdit){
        oCmbPlant.setSelectedKey(fnGetCellValue("SLinePlanTab", "PLANT"));
        oCmbLine.setSelectedKey(fnGetCellValue("SLinePlanTab", "IDLINE"));
        oCmbMaterial.setSelectedKey(fnGetCellValue("SLinePlanTab", "MATERIAL"));
        oCmbMonth.setSelectedKey(fnGetCellValue("SLinePlanTab", "MONTHNUMBER"));
    }
    else{
        oCmbMonth.setSelectedKey(currentDate.getMonth() + 1);
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSLinePlan",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifyPlan"):oLng_Settings.getText("Settings_NewPlan"),
            /*"Modifica pianificazione":"Nuova pianificazione",*/
            //icon: "/images/address.gif",
            /*tooltip: */
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Plant"), //"Divisione",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbPlant
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbLine
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_MaterialCode"), //"Materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbMaterial
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Year"), //"Anno",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SLinePlanTab", "YEAR"):currentDate.getFullYear(),
                                enabled: !bEdit,
                                maxLength: 4,
                                id: "YEAR",
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    }),
                     new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Month"), //"Mese",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbMonth
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Quantity"), //"Quantità",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SLinePlanTab", "QUANTITY"):"0",
                                maxLength: 5,
                                id: "QUANTITY",
                                liveChange: function(ev) {
                                    var val = ev.getParameter('liveValue');
                                    var newval = val.replace(/[^\d]/g, '');
                                    this.setValue(newval);
                                }
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"), //"Salva",
        press:function(){
            fnSaveSLinePlan(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbPlant.destroy();
            oCmbLine.destroy();
            oCmbMaterial.destroy();
            oCmbMonth.destroy();
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche al record della SLINEWORK o inserire una nuova voce
function fnSaveSLinePlan(bEdit) 
{
    var plant = $.UIbyID("cmbPlant").getSelectedKey();
    var line = "";
    line = $.UIbyID("cmbLine").getSelectedKey();
    if(line == "" || line == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectLine")); //Selezionare una linea
        return;
    }
    var material = "";
    material = $.UIbyID("cmbMaterial").getSelectedKey();
    if(material == "" || material == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectMaterial")); //Selezionare un materiale
        return;
    }
    var year = "";
    year = $.UIbyID("YEAR").getValue();
    if(year == "" || year == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_InsertYear")); //Inserire un anno
        return;
    }
    var month = "";
    month = $.UIbyID("cmbMonth").getSelectedKey();
    if(month == "" || month == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectMonth")); //Selezionare un mese
        return;
    }
    var quantity = "0";
    quantity = $.UIbyID("QUANTITY").getValue();
    if(quantity == "" || quantity == null){
        quantity = "0";
    }
    
    var qexe = "";
    
    if(bEdit){
        qexe =  dataProdSett +
            "updSLinePlanQR&Param.1=" + plant +
            "&Param.2=" + line +
            "&Param.3=" + material +
            "&Param.4=" + year +
            "&Param.5=" + month +
            "&Param.6=" + quantity;
    }
    else{
        qexe =  dataProdSett +
            "addSLinePlanQR&Param.1=" + plant +
            "&Param.2=" + line +
            "&Param.3=" + material +
            "&Param.4=" + year +
            "&Param.5=" + month +
            "&Param.6=" + quantity;
    }
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_PlanSaved"), //Pianificazione salvata correttamente
                         oLng_Settings.getText("Settings_PlanSavedError"), //"Errore salvataggio pianificazione"
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSLinePlan").close();
        $.UIbyID("dlgSLinePlan").destroy();
        $.UIbyID("SLinePlanTab").setSelectedIndex(-1);
        $.UIbyID("btnModSLineP").setEnabled(false);
        $.UIbyID("btnDelSLineP").setEnabled(false);
        refreshTabSLinePlan(false);
    }
}

function fnDelLinePlan(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_PlanDeleting"), //"Eliminare la pianificazione selezionata?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "delSLinePlanSQ" +
                            "&Param.1=" + fnGetCellValue("SLinePlanTab", "PLANT") +
                            "&Param.2=" + fnGetCellValue("SLinePlanTab", "IDLINE") +
                            "&Param.3=" + fnGetCellValue("SLinePlanTab", "MATERIAL") +
                            "&Param.4=" + fnGetCellValue("SLinePlanTab", "YEAR") +
                            "&Param.5=" + fnGetCellValue("SLinePlanTab", "MONTHNUMBER");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_PlanDeleted"), //"Pianificazione eliminata correttamente",
                    oLng_Settings.getText("Settings_PlanDeletedError"), //"Errore in eliminazione pianificazione",
                    false
                );
                refreshTabSLinePlan(false);
                $.UIbyID("btnModSLineP").setEnabled(false);
                $.UIbyID("btnDelSLineP").setEnabled(false);
                $.UIbyID("SLinePlanTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnFilterByMonthYear(){
    
    if($.UIbyID("cmbYearFilter1").getValue() == "" || $.UIbyID("cmbYearFilter1").getValue() == null){
        $.UIbyID("cmbYearFilter1").setValue(currentDate.getFullYear());
    }
    if($.UIbyID("cmbMonthFilter1").getSelectedKey() == ""){
       $.UIbyID("cmbMonthFilter1").setSelectedKey(currentDate.getMonth() + 1);
    }
    if($.UIbyID("cmbYearFilter2").getValue() == "" || $.UIbyID("cmbYearFilter2").getValue() == null){
        $.UIbyID("cmbYearFilter2").setValue(currentDate.getFullYear());
    }
    if($.UIbyID("cmbMonthFilter2").getSelectedKey() == ""){
       $.UIbyID("cmbMonthFilter2").setSelectedKey(currentDate.getMonth() + 1);
    }
    var iFromYear = $.UIbyID("cmbYearFilter1").getValue();
    var iFromMonth = $.UIbyID("cmbMonthFilter1").getSelectedKey();
    var iToYear = $.UIbyID("cmbYearFilter2").getValue();
    var iToMonth = $.UIbyID("cmbMonthFilter2").getSelectedKey();
    //console.log(iFromYear + "-" + iFromMonth + " " + iToYear + "-" + iToMonth);
    
    if (iToYear < iFromYear){
        iFromYear = iToYear;
        $.UIbyID("cmbYearFilter1").setValue(iFromYear);
    }
    if(iToYear == iFromYear && iToMonth < iFromMonth){
        iFromMonth = iToMonth;
        $.UIbyID("cmbMonthFilter1").setSelectedKey(iFromMonth);
    }
    refreshTabSLinePlan(true);
}
