//**************************************************************************************
//Title:  SSETSTATUS
//Author: Bruno Rosati
//Date:   27/07/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina SSETSTATUS

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Settings = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/Settings/res/settings_res.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Material e ritorna l'oggetto oTable
function createTabSSetStatus() {
    
    // contenitore dati lista causali fermo
    
    //Crea L'oggetto Tabella Material
    var oTable = UI5Utils.init_UI5_Table({
        id: "SSetStatusTab",
        properties: {
            title: oLng_Settings.getText("Settings_SetStatus"), //"Impostazione stati",
            visibleRowCount: 15,
            width: "98%",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            rowSelectionChange: function(oControlEvent){
                if ($.UIbyID("SSetStatusTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnModSStatus").setEnabled(false);
        			$.UIbyID("btnDelSStatus").setEnabled(false);
                }
                else {
                    $.UIbyID("btnModSStatus").setEnabled(true);
                    $.UIbyID("btnDelSStatus").setEnabled(true);			
                }
            }
        },
        exportButton: false,
		toolbarItems:[
            
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Add"), //"Aggiungi",
                id:   "btnAddSStatus",
                icon: 'sap-icon://add',         
                enabled: true,
                press: function (){
                    openSSetStatusEdit(false);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Modify"), //"Modifica",
                id:   "btnModSStatus",
                icon: 'sap-icon://edit',         
                enabled: false,
                press: function (){
                    openSSetStatusEdit(true);
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Delete"), //"Elimina"
                id:   "btnDelSStatus",
                icon: 'sap-icon://delete',         
                enabled: false,
                press: function (){
                    fnDelSSetStatus();
                }
            }),
            new sap.ui.commons.Button({
                text: oLng_Settings.getText("Settings_Refresh"), //"Aggiorna",
                icon: 'sap-icon://refresh',            
                enabled: true,
                press: function (){
                  refreshTabSSetStatus();
                }
            })
        ],
        columns: [
            {
				Field: "IDLINE",
				label: oLng_Settings.getText("Settings_LineID"), //"ID Linea",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Settings.getText("Settings_LineDescr"), //"Descrizione Linea",
				properties: {
					width: "120px",
                    //flexible : false
				}
            },
            {
				Field: "EX_IDSTATE", 
				label: oLng_Settings.getText("Settings_EXID"), //"External ID",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
				Field: "IDMACH", 
				label: oLng_Settings.getText("Settings_MachineID"), //"ID Macchina",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
				Field: "MACHTXT",
				label: oLng_Settings.getText("Settings_StationDescr"), //"Descrizione stazione",
                tooltip: oLng_Settings.getText("Settings_StationDescrLastUpdate"), //"Descrizione stazione (ultimo aggiornamento)",
				properties: {
					width: "120px",
                    //flexible : false
				}
            },
            {
				Field: "SUBOPER", 
				label: oLng_Settings.getText("Settings_Operation"), //"Fase",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },            
            {
				Field: "STATUS", 
				properties: {
					width: "90px",
                    visible: false,
                    //flexible : false
				}
            },
            {
				Field: "EXTENDED_TEXT", 
				label: oLng_Settings.getText("Settings_State"), //"Stato",
				properties: {
					width: "120px",
                    //flexible : false
				}
            }
        ],
        /*
        extension: new sap.ui.commons.Toolbar({
            items: []
        })
        */
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabSSetStatus();
    return oTable;
}

// Aggiorna la tabella delle impostazioni degli stati
function refreshTabSSetStatus() {
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        $.UIbyID("SSetStatusTab").getModel().loadData(
            QService + dataProdSett +
            "getSsetStatusSQ&Param.1=" + $('#plant').val() + "&Param.2=" + sLanguage
        );
    }
    else{
        $.UIbyID("SSetStatusTab").getModel().loadData(
            QService + dataProdSett +
            "getSsetStatusSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey() + "&Param.2=" + sLanguage
        );
    }
    $.UIbyID("SSetStatusTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

// Function per creare la finestra di dialogo Funzioni
function openSSetStatusEdit(bEdit) {
    if (bEdit === undefined) bEdit = false;
    
    var oCmbLine = new sap.ui.commons.ComboBox("cmbLine", {
        enabled: !bEdit,
        change: function (oEvent) {
            $.UIbyID("cmbMachine").getModel().loadData(
                QService + dataProdMD + "Machines/getMachinesByLineSQ&Param.1=" + $.UIbyID("cmbLine").getSelectedKey()
            );
            $.UIbyID("cmbMachine").setSelectedKey("");
        }
    });
    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    oItemLine.bindProperty("text", "LINETXT");
    oCmbLine.bindItems("/Rowset/Row", oItemLine);
    var oLineModel = new sap.ui.model.xml.XMLModel();
    oLineModel.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    oCmbLine.setModel(oLineModel);
    
    var oCmbMachine = new sap.ui.commons.ComboBox("cmbMachine", {
        enabled: !bEdit,
        change: function (oEvent) {}
    });
    var oItemMachine = new sap.ui.core.ListItem();
    oItemMachine.bindProperty("key", "IDMACH");
    oItemMachine.bindProperty("text", "MACHTXT");
    oCmbMachine.bindItems("/Rowset/Row", oItemMachine);
    var oMachineModel = new sap.ui.model.xml.XMLModel();
    oMachineModel.loadData(
        QService + dataProdMD + "Machines/getMachinesByLineSQ&Param.1=" + 
        (bEdit?fnGetCellValue("SSetStatusTab", "IDLINE"):$.UIbyID("cmbLine").getSelectedKey())
    );
    oCmbMachine.setModel(oMachineModel);
    
    var oCmbStatus = new sap.ui.commons.ComboBox("cmbStatus", {
        change: function (oEvent) {}
    });
    var oItemStatus = new sap.ui.core.ListItem();
    oItemStatus.bindProperty("key", "ORIG_VALUE");
    oItemStatus.bindProperty("text", "EXTENDED_TEXT");
    oCmbStatus.bindItems("/Rowset/Row", oItemStatus);
    var oStatusModel = new sap.ui.model.xml.XMLModel();
    oStatusModel.loadData(
        QService + dataProdSett + "getStatusTypesSQ"  + "&Param.1=" + sLanguage
    );
    oCmbStatus.setModel(oStatusModel);
    
    if(bEdit){
        oCmbLine.setSelectedKey(fnGetCellValue("SSetStatusTab", "IDLINE"));
        oCmbMachine.setSelectedKey(fnGetCellValue("SSetStatusTab", "IDMACH"));
        oCmbStatus.setSelectedKey(fnGetCellValue("SSetStatusTab", "STATUS"));
    }
    
    //  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgSSetStatus",
        maxWidth: "600px",
        Width: "600px",
        showCloseButton: false
    });
    
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({
            text: bEdit?oLng_Settings.getText("Settings_ModifySetting"):oLng_Settings.getText("Settings_NewSetting"),
            /*"Modifica impostazione":"Nuova impostazione",*/
            //icon: "/images/address.gif",
            /*tooltip: */
        }),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Line"), //"Linea",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbLine
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_EXID"), //"External ID",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SSetStatusTab", "EX_IDSTATE"):"",
                                enabled: !bEdit,
                                maxLength: 20,
                                id: "EX_IDSTATE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Machine"), //"Macchina",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbMachine
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_Operation"), //"Fase",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                value: bEdit?fnGetCellValue("SSetStatusTab", "SUBOPER"):"",
                                enabled: !bEdit,
                                maxLength: 10,
                                id: "SUBOPER"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Settings.getText("Settings_State"), //"Stato",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                        }),
                        fields: [
                            oCmbStatus
                        ]
                    })
                ]
            })
        ]
    });
    
    oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Save"), //"Salva",
        press:function(){
            fnSaveSsetStatus(bEdit);
        }
    }));
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Settings.getText("Settings_Cancel"), //"Annulla",
        press:function(){
            oEdtDlg.close();
            oEdtDlg.destroy();
            oCmbLine.destroy();
            oCmbMachine.destroy();
            oCmbStatus.destroy();
        }
    }));    
	oEdtDlg.open();
}

// Funzione per salvare le modifiche al record della SLINEWORK o inserire una nuova voce
function fnSaveSsetStatus(bEdit) 
{
    var line = "";
    line = $.UIbyID("cmbLine").getSelectedKey();
    if(line == "" || line == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectLine")); //Selezionare una linea
        return;
    }
    var ex_idstate = $.UIbyID("EX_IDSTATE").getValue();
    if(ex_idstate == "" || ex_idstate == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_InsertEx_IDSTATE")); //Inserire un ID esterno
        return;
    }
    var suboper = $.UIbyID("SUBOPER").getValue();
    var machine = $.UIbyID("cmbMachine").getSelectedKey();
    var status = $.UIbyID("cmbStatus").getSelectedKey();
    if(status == "" || status == null){
        sap.ui.commons.MessageBox.alert(oLng_Settings.getText("Settings_SelectStatus")); //Selezionare uno stato
        return;
    }
    
    var qexe = "";
    
    if(bEdit){
        qexe =  dataProdSett +
            "updSSetStatusSQ" +
            "&Param.1=" + line +
            "&Param.2=" + ex_idstate +
            "&Param.3=" + suboper +
            "&Param.4=" + machine +
            "&Param.5=" + status;
    }
    else{
        qexe =  dataProdSett +
            "addSSetStatusSQ" +
            "&Param.1=" + line +
            "&Param.2=" + ex_idstate +
            "&Param.3=" + suboper +
            "&Param.4=" + machine +
            "&Param.5=" + status;
    }
    
    var ret = fnExeQuery(qexe, oLng_Settings.getText("Settings_SettingSaved"), //"Impostazione salvata correttamente"
                         oLng_Settings.getText("Settings_SettingSavedError"), //"Errore salvataggio impostazione"
                         false, false);
    
    if (ret){
        $.UIbyID("dlgSSetStatus").close();
        $.UIbyID("dlgSSetStatus").destroy();
        $.UIbyID("SSetStatusTab").setSelectedIndex(-1);
        $.UIbyID("btnModSStatus").setEnabled(false);
        $.UIbyID("btnDelSStatus").setEnabled(false);
        refreshTabSSetStatus();
    }
}

function fnDelSSetStatus(){
    
    sap.ui.commons.MessageBox.show(
        oLng_Settings.getText("Settings_SettingDeleting"), //"Eliminare l'impostazione' selezionata?",
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Settings.getText("Settings_DeletingConfirm"), //"Conferma eliminazione",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var qexe = dataProdSett + "delSSetStatusSQ" +
                            "&Param.1=" + fnGetCellValue("SSetStatusTab", "IDLINE") +
                            "&Param.2=" + fnGetCellValue("SSetStatusTab", "EX_IDSTATE") +
                            "&Param.3=" + fnGetCellValue("SSetStatusTab", "SUBOPER") +
                            "&Param.4=" + fnGetCellValue("SSetStatusTab", "IDMACH");
                var ret = fnSQLQuery(
                    qexe,
                    oLng_Settings.getText("Settings_SettingDeleted"), //"Impostazione eliminata correttamente",
                    oLng_Settings.getText("Settings_SettingDeletedError"), //"Errore in eliminazione impostazione",
                    false
                );
                refreshTabSSetStatus();
                $.UIbyID("btnModSStatus").setEnabled(false);
                $.UIbyID("btnDelSStatus").setEnabled(false);
                $.UIbyID("SSetStatusTab").setSelectedIndex(-1);
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}
