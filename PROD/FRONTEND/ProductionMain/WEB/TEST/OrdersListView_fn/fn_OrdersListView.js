//**************************************************************************************
//Title:  Orders List View
//Author: Bruno Rosati
//Date:   10/05/2017
//Last Modifier: Luca Adanti
//Last Date: 28/06/2018
//Vers:   1.4
//**************************************************************************************
// Script per pagina Orders List View

var oPlantFOperator;
var oCmbCID;

/***********************************************************************************************/
// Funzioni ed oggetti per Orders
/***********************************************************************************************/

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();


// setta le risorse per la lingua locale
var oLng_Test = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/TEST/res/test_res.i18n.properties",
	locale: sCurrentLocale
});
var oMon_Test = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella Operatore e ritorna l'oggetto oTable
function createTabOrdersList() {

	//Crea L'oggetto Tabella Operatore
	var oTable = UI5Utils.init_UI5_Table({
		id: "OrdersListTab",
		properties: {
			title: oLng_Test.getText("Test_OrdersList"), //"Lista Commesse",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {},
			toolbar: new sap.ui.commons.Toolbar({
				items: [],
				rightItems: [
										new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabOrdersList();
						}
					})
								]
			})
		},
		exportButton: false,
		columns: [
			{
				Field: "PLANT",
				properties: {
					width: "100px",
					visible: false
				}
						},
			{
				Field: "NAME1",
				label: oLng_Test.getText("Test_Plant"), //"Divisione",
				properties: {
					width: "70px"
				}
						},
			{
				Field: "IDLINE",
				label: oLng_Test.getText("Test_LineID"), //"ID Linea",
				properties: {
					width: "75px"
				}
						},
			{
				Field: "LINETXT",
				label: oLng_Test.getText("Test_Line"), //"Linea",
				properties: {
					width: "75px"
				}
						},
			{
				Field: "ORDER",
				label: oLng_Test.getText("Test_Order"), //"Commessa",
				properties: {
					width: "70px"
				}
						},
			{
				Field: "VERSION",
				label: oLng_Test.getText("Test_Version"), //"Versione",
				properties: {
					width: "70px"
				}
						},
			{
				Field: "POPER",
				label: oLng_Test.getText("Test_Poper"), //"Fase",
				properties: {
					width: "70px"
				}
						},
			{
				Field: "CICTXT",
				label: oLng_Test.getText("Test_PoperDescription"), //"Descrizione fase",
				properties: {
					width: "80px"
				}
						},
			{
				Field: "CDLID",
				label: oLng_Test.getText("Test_CdlDescription"), //"Descrizione Centro di Lavoro",
				properties: {
					width: "60px"
				}
						},
			{
				Field: "MATERIAL",
				label: oLng_Test.getText("Test_Material"), //"Materiale",
				properties: {
					width: "80px"
				}
						},
			{
				Field: "DESC",
				label: oLng_Test.getText("Test_MaterialDescription"), //"Descrizione Materiale",
				properties: {
					width: "100px"
				}
			},
			{
				Field: "CTIME",
				label: oMon_Test.getText("Monitor_CTIME"), //"Tempo ciclo rilevato ",
				properties: {
					width: "50px"
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
			},
			{
				Field: "CTIME_UM",
				label: oMon_Test.getText("Monitor_UM"), //"UM",
				properties: {
					width: "20px"
				}
			},
			{
				Field: "STIME",
				label: oMon_Test.getText("Monitor_STIME"), //"Tempo ciclo standard ",
				properties: {
					width: "50px"
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
			},
			{
				Field: "CTIME_UM",
				label: oMon_Test.getText("Monitor_UM"), //"UM",
				properties: {
					width: "20px"
				}
			}
				]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabOrdersList();
	return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabOrdersList() {
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		$.UIbyID("OrdersListTab").getModel().loadData(
			QService + dataOrders +
			"getOrdersListSQ&Param.1=" + $('#plant').val()
		);
	} else {
		$.UIbyID("OrdersListTab").getModel().loadData(
			QService + dataOrders +
			"getOrdersListSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()
		);
	}
	//$("#splash-screen").hide();
}
