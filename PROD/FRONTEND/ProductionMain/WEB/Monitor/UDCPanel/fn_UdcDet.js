//**************************************************************************************
//Title:  Dettaglio UDC
//Author: Bruno Rosati
//Date:   02/08/2017
//Vers:   1.0
//**************************************************************************************
// Script per dettaglio Monitor UDC

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});

function openUDCdet(bEdit, sOrigTableName, refreshFunc, bEnabledMod) {
    
    if (bEdit == undefined) bEdit = false;
    
    // Crea la ComboBox per selezionare la tipologia di stato dell'UDC
    var oCmbUDCstatus = new sap.ui.commons.ComboBox({
        id: "cmbUDCstatus",
        selectedKey: bEdit?fnGetCellValue(sOrigTableName, "UDC_STATE"):"",
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        enabled: /*bEnabledMod*/false,
        change: function() {}
    });
    var oItemUDCstatus = new sap.ui.core.ListItem();
    oItemUDCstatus.bindProperty("text", "EXTENDED_TEXT");
	oItemUDCstatus.bindProperty("key", "ORIG_VALUE");	
	oCmbUDCstatus.bindItems("/Rowset/Row", oItemUDCstatus);
    var oUdcStatusModel = new sap.ui.model.xml.XMLModel();
    oUdcStatusModel.loadData(QService + dataProdMon + "UDC/getUDCstateTypesSQ&Param.1=" + sLanguage);
    oCmbUDCstatus.setModel(oUdcStatusModel);
    
    // Crea la ComboBox per selezionare la tipologia dell'UDC
    var oModelUdcType = new sap.ui.model.xml.XMLModel();
	oModelUdcType.loadData(QService + dataProdMon +"UDC/getUDCtypesSQ&Param.1=" + sLanguage);
	var oCmbUdcType = new sap.ui.commons.ComboBox({
        id: "cmbSelectedUdcType",
        selectedKey: bEdit?fnGetCellValue(sOrigTableName, "ORIGIN"):"",
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        enabled: /*bEnabledMod*/false
    });
	oCmbUdcType.setModel(oModelUdcType);		
	var oItemUdcType = new sap.ui.core.ListItem();
	oItemUdcType.bindProperty("text", "EXTENDED_TEXT");
	oItemUdcType.bindProperty("key", "ORIG_VALUE");	
	oCmbUdcType.bindItems("/Rowset/Row", oItemUdcType);
  
//  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
        modal:  true,
        resizable: true,
        id: "dlgUDCdet",
        maxWidth: "1200px",
        maxHeight: "900px",
        //minHeight: "900px",
        title: oLng_Monitor.getText("Monitor_UDCDetail") + ": " +
                fnGetCellValue(sOrigTableName, "UDCNR") + " - " +
                oLng_Monitor.getText("Monitor_Order") + ": " +
                parseInt(fnGetCellValue(sOrigTableName, "ORDER")) + " - " +
                oLng_Monitor.getText("Monitor_Poper") + ": " +
                fnGetCellValue(sOrigTableName, "POPER"), //"Dettaglio UDC - - Commessa: - Fase: ",
        showCloseButton: false
    });
    
    var oTabMasterUDCedit = new sap.ui.commons.TabStrip("TabMasterUDCedit");
    oTabMasterUDCedit.attachClose(function (oEvent) {
        var oTabStrip = oEvent.oSource;
        oTabStrip.closeTab(oEvent.getParameter("index"));
    });
    
    var oLayoutDetails = new sap.ui.commons.layout.MatrixLayout("tabEditUDC", {
        columns: 1,
        width: "100%"
    });
    
	var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "UDCNR"):"",
                                editable: !bEdit,
                                maxLength: 20,
                                id: "txtUDCNR"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_UdcState"), //"Stato UDC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: oCmbUDCstatus
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: oCmbUdcType
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_MaxQuantity"), //"Quantità massima",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "PZUDC"):"",
                                editable: !bEdit,
                                maxLength: 11,
                                id: "txtPZUDC"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_CurrentQuantity"), //"Quantità corrente",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "CURRENT_QTY"):"",
                                editable: !bEdit,
                                maxLength: 10,
                                id: "txtCURRENT_QTY"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_OperatorID"), //"PERNR",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "OPEID"):"",
                                editable: !bEdit,
                                maxLength: 12,
                                id: "txtOPEID"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome operatore",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "OPENAME"):"",
                                editable: !bEdit,
                                maxLength: 12,
                                id: "txtOPENAME"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "MATERIAL"):"",
                                editable: !bEdit,
                                maxLength: 40,
                                id: "txtMATERIAL"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "ORDER"):"",
                                editable: !bEdit,
                                maxLength: 12,
                                id: "txtORDER"
                            })
                        ]
                    }),
                    /*new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            text: oLng_Monitor.getText("Monitor_EXID"), //"External ID",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "EXID"):"",
                                editable: !bEdit,
                                maxLength: 20,
                                id: "txtEXID"
                            })
                        ]
                    }),*/
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            //visible: bEnabledMod,
                            text: oLng_Monitor.getText("Monitor_Notes"), //"Note",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.TextField({
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"/*"auto"*/}),
                                value: bEdit?fnGetCellValue(sOrigTableName, "NOTE_DUDCMAST"):"",
                                editable: true/*!bEdit*/,
                                //visible: bEnabledMod,
                                id: "txtNOTE"
                            })
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({
                            //visible: bEnabledMod,
                            text: oLng_Monitor.getText("Monitor_OptionsUDCstate"), //"Opzioni stato UdC",
                            layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})
                        }),
                        fields: [
                            new sap.ui.commons.Button({
                                text: oLng_Monitor.getText("Monitor_CompleteUDC"), //"Completa UdC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"/*"auto"*/}),
                                enabled: fnGetCellValue(sOrigTableName, "UDC_STATE")==='O'?true:false,
                                id: "btnCompleteUDCopt",
                                press:function(){
                                    fnUpdateStatus(sOrigTableName, refreshFunc, "C");
                                }
                            }),
                            new sap.ui.commons.Button({
                                text: oLng_Monitor.getText("Monitor_OpenUDC"), //"Apri UdC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"/*"auto"*/}),
                                enabled: (fnGetCellValue(sOrigTableName, "UDC_STATE")==='C' || 
                                          fnGetCellValue(sOrigTableName, "UDC_STATE")==='E')?true:false,
                                id: "btnOpenUDCopt",
                                press:function(){
                                    fnUpdateStatus(sOrigTableName, refreshFunc, "O");
                                }
                            }),
                            new sap.ui.commons.Button({
                                text: oLng_Monitor.getText("Monitor_EmptyUDC"), //"Vuota UdC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"/*"auto"*/}),
                                enabled: fnGetCellValue(sOrigTableName, "UDC_STATE")==='C'?true:false,
                                id: "btnEmptyUDCopt",
                                press:function(){
                                    //fnUpdateStatus_RemoveDetails(sOrigTableName, refreshFunc);
                                    fnUpdateStatus(sOrigTableName, refreshFunc, "E");
                                }
                            }),
                            new sap.ui.commons.Button({
                                text: oLng_Monitor.getText("Monitor_ConfProdUndoUDC"), //"Storna UdC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"/*"auto"*/}),
                                //enabled: fnGetCellValue(sOrigTableName, "UDC_STATE")==='C'?true:false,
                                id: "btnAvertUDC",
                                press:function(){
                                    $.UIbyID("TabMasterUDCedit").setSelectedIndex(4);
                                }
                            })
                        ]
                    })
                ]
            })
        ]
    });
    
    //creazione dei layout 
    var oLayout0, oLayoutMov, oLayoutLink;    
    if(bEdit){
		const sUDCNR = fnGetCellValue(sOrigTableName, "UDCNR");
        var oDMSerialsTable = createTabDMSerials(sUDCNR);
        var oUdCMovTable = createTabUdCMov(sUDCNR);
        var oUdCLinkTable = createTabUDClink(sUDCNR);
        var oConfProdTable = createTabConfProdPanel(sOrigTableName);
        
        var oLayout0 = new sap.ui.commons.layout.MatrixLayout("tabdetail", {columns: 1});                
        oLayout0.createRow(oDMSerialsTable);
        
        var oLayoutMov = new sap.ui.commons.layout.MatrixLayout("tabMovements", {columns: 1});                
        oLayoutMov.createRow(oUdCMovTable);
        
        var oLayoutLink = new sap.ui.commons.layout.MatrixLayout("tabLinks", {columns: 1});                
        oLayoutLink.createRow(oUdCLinkTable);
        
        var oLayoutConfProd = new sap.ui.commons.layout.MatrixLayout("tabConfProdPanel", {columns: 1});                
        oLayoutConfProd.createRow(oConfProdTable);
    }    
    oLayoutDetails.createRow(oForm1);
    
    //inserimento dei layout nel TabMaster
    oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_UDCData")/*"Dati UDC"*/, oLayoutDetails);
    if(bEdit){
        oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_Content")/*"Contenuto"*/, oLayout0);
        oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_Movements")/*"Movimenti"*/, oLayoutMov);
        oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_UDCsConnected")/*"UdC collegate"*/, oLayoutLink);
        oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_ConfProd")/*"Avanzamenti di produzione"*/, oLayoutConfProd);
    }
    
    oEdtDlg.addContent(oTabMasterUDCedit);
    
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_AlignWithSap"), //"Allinea con SAP"
        icon: 'sap-icon://activate',
        style: sap.ui.commons.ButtonStyle.Accept,
        press:function(){
            sap.ui.commons.MessageBox.show(
                oLng_Monitor.getText("Monitor_UDCaligninigMessage"), //"Allineare la quantità dell'UdC con la quantità indicata in SAP?",
                sap.ui.commons.MessageBox.Icon.QUESTION,
                oLng_Monitor.getText("Monitor_UDCaligninig"), //"Allineamento UdC",
                [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
                function(sResult){
                    if (sResult == 'YES'){
                        var qexe = dataProdSap + "From/UpdateUdcManualQR" +
                            "&Param.1=" + $.UIbyID("txtUDCNR").getValue();
                        
                        console.log("query: " + qexe);
                        /*fnGetAjaxVal(qexe,["code","message"],false);*/
                        var ret = fnExeQuery(
                            qexe, 
                            oLng_Monitor.getText("Monitor_UDCaligned"), //"UdC allineato correttamente", 
                            oLng_Monitor.getText("Monitor_UDCalignedError"), //"Errore nell'allineamento dell'UdC",
                            false
                        );
                        refreshFunc();
                        $.UIbyID(sOrigTableName).setSelectedIndex(-1);
                    }
                },
                sap.ui.commons.MessageBox.Action.YES
            );
        }
    }));
    
    oEdtDlg.addContent(new sap.ui.commons.layout.MatrixLayout({
        layoutFixed : true,
        height : "20px"
    }));
    
    oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Print"), //"Stampa"
        icon: 'sap-icon://print',
        press:function(){
            showPdfDialog(sOrigTableName);
        }
    }));
    
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Save"), //"Salva",
        //visible: bEnabledMod,
        press:function(){
            if (bEdit){
                fnUpdateNotes(sOrigTableName, refreshFunc);
            }                
            else{
                //fnAddLine();
            }                
        }
    }));
    
	oEdtDlg.addButton(new sap.ui.commons.Button({
        text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
        press:function(){
            //refreshFunc();
            oEdtDlg.close();
            oEdtDlg.destroy()
        }
    }));
	oEdtDlg.open();
}

function fnUpdStatusQuery(sTable, refreshFunc, sNewStatus){
    
    var qexe =  dataProdMon +
                "UDC/updUdCMastStatusNotesSQ" + 
                "&Param.1=" + fnGetCellValue(sTable, "UDCNR") +
                "&Param.2=" + sNewStatus +
                "&Param.3=" + fnGetCellValue(sTable, "NOTE_DUDCMAST");
    
    var ret = fnExeQuery(qexe, oLng_Monitor.getText("Monitor_UDCstatusUpdated"),
                         oLng_Monitor.getText("Monitor_UDCstatusUpdatedError")
                         /*"Stato dell'UdC aggiornato correttamente", "Errore nell'aggiornamento stato dell'UdC"*/,
                         false);    
    return ret;
}

function fnUpdateStatus(sTable, refreshFunc, sNewStatus){
    
    var ret = fnUpdStatusQuery(sTable, refreshFunc, sNewStatus);
    
    if (ret){
        refreshFunc();
        $.UIbyID("cmbUDCstatus").setSelectedKey(sNewStatus);
        $.UIbyID("btnCompleteUDCopt").setEnabled(sNewStatus==='O'?true:false);
        $.UIbyID("btnOpenUDCopt").setEnabled((sNewStatus==='C' || sNewStatus==='E')?true:false);
        $.UIbyID("btnEmptyUDCopt").setEnabled(sNewStatus==='C'?true:false);
    }
}

function fnUpdateStatus_RemoveDetails(sTable, refreshFunc){
    
    sap.ui.commons.MessageBox.show(
        oLng_Monitor.getText("Monitor_DetRemovedContinue"), //"I dettagli dell'UdC verranno rimossi, continuare?"
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Monitor.getText("Monitor_Confirm"), //"Conferma"
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                
                var sUDCnr = fnGetCellValue(sTable, "UDCNR");
                var qexe = dataProdMon +
                    "UDC/emptyUDC_QR" +
                    "&Param.1=" + sUDCnr +
                    "&Param.2=" + fnGetCellValue(sTable, "PZUDC") +
                    "&Param.3=" + fnGetCellValue(sTable, "CURRENT_QTY") +
                    "&Param.4=" + fnGetCellValue(sTable, "OPEID") +
                    "&Param.5=" + fnGetCellValue(sTable, "MATERIAL") +
                    "&Param.6=" + fnGetCellValue(sTable, "ORDER") +
                    "&Param.7=" + fnGetCellValue(sTable, "NOTE_DUDCMAST") +
                                " - " + oLng_Monitor.getText("Monitor_HandlyEmpty"); //"Svuotato manualmente"
                
                var ret = fnGetAjaxVal(qexe,["code","message"],false);
                var sAnswTitle = "";
                var sAnswMsg = "";
                
                if(ret.code == 0){
                    sAnswTitle = oLng_Monitor.getText("Monitor_OK"); //"OK"
                    sAnswMsg = oLng_Monitor.getText("Monitor_UDCstatusUpdated"); //"Stato dell'UdC aggiornato correttamente"
                    $.UIbyID("cmbUDCstatus").setSelectedKey("E");
                    $.UIbyID("btnCompleteUDCopt").setEnabled(false);
                    $.UIbyID("btnOpenUDCopt").setEnabled(true);
                    $.UIbyID("btnEmptyUDCopt").setEnabled(false);
                }
                else{
                    sAnswTitle = oLng_Monitor.getText("Monitor_Error"); //"Errore"
                    sAnswMsg = ret.message;
                }
                
                refreshFunc();
                refreshTabDMSerials(sUDCnr);
                
                sap.ui.commons.MessageBox.show(
                    sAnswMsg,
                    (ret.code == 0)?sap.ui.commons.MessageBox.Icon.SUCCESS:sap.ui.commons.MessageBox.Icon.ERROR,
                    sAnswTitle,
                    [sap.ui.commons.MessageBox.Action.OK],
                    sap.ui.commons.MessageBox.Action.OK
                );
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function fnUpdateNotes(sTable, refreshFunc){
    
    var qexe =  dataProdMon +
                "UDC/updUdCMastStatusNotesSQ" + 
                "&Param.1=" + $.UIbyID("txtUDCNR").getValue() +
                "&Param.2=" + $.UIbyID("cmbUDCstatus").getSelectedKey() +
                "&Param.3=" + $.UIbyID("txtNOTE").getValue();
    
    var ret = fnExeQuery(qexe, oLng_Monitor.getText("Monitor_UDCSaved"),
                         oLng_Monitor.getText("Monitor_UDCSavedError")
                         /*"UDC salvato correttamente", "Errore in salvataggio UDC"*/,
                         false);
    if (ret){
        $.UIbyID("dlgUDCdet").close();
        $.UIbyID("dlgUDCdet").destroy();
        refreshFunc();
    }
}

function showPdfDialog(sTable) {
    
    var sPrintUrl = "/XMII/Runner?";
    sPrintUrl += "Transaction=ProductionMain/Movement/Print/print_ETI_PROD_absolute_TR";
    sPrintUrl += "&OutputParameter=pdfString&Content-Type=application/pdf&isBinary=true";
    sPrintUrl += "&UDCNR=" + fnGetCellValue(sTable, "UDCNR");
    sPrintUrl += "&IDLINE=" + fnGetCellValue(sTable, "IDLINE");
    //sPrintUrl += "&location=" + oController._sWsName; //qui non abbiamo una postazione correlata da cui stampare
    sPrintUrl += "&backOffice=true";
    sPrintUrl += "&Language=" + sLanguage;
    /*if(fnGetCellValue(sTable, "UDC_STATE") === 'C'){
        sPrintUrl += "&full=true";
    }
    else{
        sPrintUrl += "&full=false";
    }*/
    
    /*
    if(bBase64)
        sPrintUrl = 'data:application/pdf;base64,' + sPrintUrl;
    */
    
    var oHtml = new sap.ui.core.HTML();
    oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');
    
    var oPrintDialog = new sap.m.Dialog({
        //resizable: true,
        horizontalScrolling : false,
        verticalScrolling : false,
        contentWidth : '100%',
        contentHeight : '100%',
        customHeader: new sap.m.Toolbar({
            content: [
                new sap.m.ToolbarSpacer(),
                new sap.m.Button({
                    icon: "sap-icon://decline",
                    press: function() {
                        oHtml.destroy();
                        oPrintDialog.close();
                    }
                })
            ]
        }),
        content: oHtml,
        afterClose: function() {
            oPrintDialog.destroy();
        }
    })/*.addStyleClass('noPaddingDialog fullHeightScrollCont')*/;
    
    oPrintDialog.open();
}