//**************************************************************************************
//Title:  Dettaglio UDC v.2
//Author: Bruno Rosati
//Date:   31/10/2018
//Vers:   1.0
//**************************************************************************************
// Script per dettaglio Monitor UDC

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});

var dataProdMovement = "Content-Type=text/XML&QueryTemplate=ProductionMain/Movement/";

function openUDCdetV2(bEdit, sOrigTableName, refreshFunc, bEnabledMod) {

	if (bEdit === undefined) bEdit = false;

	//flag per l'abilitazione alla modifica quantità e fase successiva
	var bEnableModify = (fnGetCellValue(sOrigTableName, "TPOPESAP") === "01" || fnGetCellValue(sOrigTableName, "TPOPESAP") === "08");

	// Crea la ComboBox per selezionare la tipologia di stato dell'UDC
	var oCmbUDCstatus = new sap.ui.commons.ComboBox({
		id: "cmbUDCstatus",
		selectedKey: bEdit ? fnGetCellValue(sOrigTableName, "UDC_STATE") : "",
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "2"
		}),
		enabled: /*bEnabledMod*/ false,
		change: function () {}
	});
	var oItemUDCstatus = new sap.ui.core.ListItem();
	oItemUDCstatus.bindProperty("text", "EXTENDED_TEXT");
	oItemUDCstatus.bindProperty("key", "ORIG_VALUE");
	oCmbUDCstatus.bindItems("/Rowset/Row", oItemUDCstatus);
	var oUdcStatusModel = new sap.ui.model.xml.XMLModel();
	oUdcStatusModel.loadData(QService + dataProdMon + "UDC/getUDCstateTypesSQ&Param.1=" + sLanguage);
	oCmbUDCstatus.setModel(oUdcStatusModel);

	// Crea la ComboBox per selezionare la tipologia dell'UDC
	var oModelUdcType = new sap.ui.model.xml.XMLModel();
	oModelUdcType.loadData(QService + dataProdMon + "UDC/getUDCtypesSQ&Param.1=" + sLanguage);
	var oCmbUdcType = new sap.ui.commons.ComboBox({
		id: "cmbSelectedUdcType",
		selectedKey: bEdit ? fnGetCellValue(sOrigTableName, "ORIGIN") : "",
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "2"
		}),
		enabled: /*bEnabledMod*/ false
	});
	oCmbUdcType.setModel(oModelUdcType);
	var oItemUdcType = new sap.ui.core.ListItem();
	oItemUdcType.bindProperty("text", "EXTENDED_TEXT");
	oItemUdcType.bindProperty("key", "ORIG_VALUE");
	oCmbUdcType.bindItems("/Rowset/Row", oItemUdcType);

	//  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgUDCdet",
		maxWidth: "1200px",
		maxHeight: "900px",
		//minHeight: "900px",
		title: oLng_Monitor.getText("Monitor_UDCDetail") + ": " +
			fnGetCellValue(sOrigTableName, "UDCNR") + " - " +
			oLng_Monitor.getText("Monitor_Order") + ": " +
			parseInt(fnGetCellValue(sOrigTableName, "ORDER")) + " - " +
			oLng_Monitor.getText("Monitor_Poper") + ": " +
			fnGetCellValue(sOrigTableName, "POPER"), //"Dettaglio UDC - - Commessa: - Fase: ",
		showCloseButton: false
	});

	var oTabMasterUDCedit = new sap.ui.commons.TabStrip("TabMasterUDCedit");
	oTabMasterUDCedit.attachClose(function (oEvent) {
		var oTabStrip = oEvent.oSource;
		oTabStrip.closeTab(oEvent.getParameter("index"));
	});

	var oLayoutDetails = new sap.ui.commons.layout.MatrixLayout("tabEditUDC", {
		columns: 1,
		width: "100%"
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});
	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
new sap.ui.layout.form.FormContainer({
				formElements: [
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_UDCNR"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "UDCNR") : "",
								editable: !bEdit,
								maxLength: 20,
								id: "txtUDCNR"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_UdcState"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: oCmbUDCstatus
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_UDCtype"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: oCmbUdcType
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_MaxQuantity"),
							visible: false,
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "PZUDC") : "",
								editable: !bEdit,
								maxLength: 11,
								visible: false,
								id: "txtPZUDC"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Quantity"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "CURRENT_QTY") : "",
								editable: !bEdit,
								maxLength: 5,
								id: "txtCURRENT_QTY",
								liveChange: function (ev) {
									var oldVal = this['oldValue'];
									var val = ev.getParameter('liveValue');
									var oldFocus = this['oldFocus'];
									var newval = val.replace(/[^.\d]/g, '');
									if (newval.split(".").length - 1 > 1)
										newval = oldVal;
									this.setValue(newval);
									this['oldValue'] = newval;
									if (newval === oldVal) {
										this.applyFocusInfo({
											cursorPos: oldFocus.cursorPos
										});
									} else
										this['oldFocus'] = this.getFocusInfo();
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_OperatorID"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "OPEID") : "",
								tooltip: bEdit ? fnGetCellValue(sOrigTableName, "OPENAME") : "",
								editable: !bEdit,
								maxLength: 12,
								id: "txtOPEID"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Material"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "MATERIAL") : "",
								editable: !bEdit,
								maxLength: 40,
								id: "txtMATERIAL"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_CurrentOrderPoper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "ORDER") : "",
								editable: !bEdit,
								maxLength: 12,
								id: "txtORDER"
							}),
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "POPER") : "",
								editable: !bEdit,
								maxLength: 4,
								id: "txtPOPER"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_NextOrderPoper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "NORDER") : "",
								editable: !bEdit,
								maxLength: 12,
								id: "txtNORDER"
							}),
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "NPOPER") : "",
								editable: !bEdit,
								maxLength: 4,
								id: "txtNPOPER"
							}),
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_Modify"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: true,
								id: "btnEditQtyNextPoper",
								press: function () {
									openEditQtyNextPoperDlg(bEdit, sOrigTableName, bEnableModify, refreshFunc);
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							//visible: bEnabledMod,
							text: oLng_Monitor.getText("Monitor_Notes"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? fnGetCellValue(sOrigTableName, "NOTE_DUDCMAST") : "",
								editable: true /*!bEdit*/ ,
								//visible: bEnabledMod,
								id: "txtNOTE"
							}),
new sap.ui.commons.Button({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								text: oLng_Monitor.getText("Monitor_Save"),
								press: function () {
									if (bEdit) {
										fnUpdateNotes(sOrigTableName, refreshFunc);
									} else {}
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							//visible: bEnabledMod,
							text: oLng_Monitor.getText("Monitor_OptionsUDCstate"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_CompleteUDC"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: fnGetCellValue(sOrigTableName, "UDC_STATE") === 'O' ? true : false,
								id: "btnCompleteUDCopt",
								press: function () {
									fnUpdateStatus(sOrigTableName, refreshFunc, "C");
								}
							}),
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_OpenUDC"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: (fnGetCellValue(sOrigTableName, "UDC_STATE") === 'C' ||
									fnGetCellValue(sOrigTableName, "UDC_STATE") === 'E') ? true : false,
								id: "btnOpenUDCopt",
								press: function () {
									fnUpdateStatus(sOrigTableName, refreshFunc, "O");
								}
							}),
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_EmptyUDC"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: fnGetCellValue(sOrigTableName, "UDC_STATE") === 'C' ? true : false,
								id: "btnEmptyUDCopt",
								press: function () {
									fnUpdateStatus(sOrigTableName, refreshFunc, "E");
								}
							}),
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_ConfProdUndoUDC"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: false,
								visible: false,
								id: "btnAvertUDC",
								press: function () {
									$.UIbyID("TabMasterUDCedit").setSelectedIndex(4);
								}
							})
]
					})
]
			})
]
	});

	//creazione dei layout 
	var oLayout0, oLayoutMov, oLayoutLink;
	if (bEdit) {
		const sUDCNR = fnGetCellValue(sOrigTableName, "UDCNR");
		var oDMSerialsTable = createTabDMSerials(sUDCNR);
		var oUdCMovTable = createTabUdCMov(sUDCNR);
		var oUdCLinkTable = createTabUDClink(sUDCNR);
		var oConfProdTable = createTabConfProdPanel(sOrigTableName, false);

		var oLayout0 = new sap.ui.commons.layout.MatrixLayout("tabdetail", {
			columns: 1
		});
		oLayout0.createRow(oDMSerialsTable);

		var oLayoutMov = new sap.ui.commons.layout.MatrixLayout("tabMovements", {
			columns: 1
		});
		oLayoutMov.createRow(oUdCMovTable);

		var oLayoutLink = new sap.ui.commons.layout.MatrixLayout("tabLinks", {
			columns: 1
		});
		oLayoutLink.createRow(oUdCLinkTable);

		var oLayoutConfProd = new sap.ui.commons.layout.MatrixLayout("tabConfProdPanel", {
			columns: 1
		});
		oLayoutConfProd.createRow(oConfProdTable);
	}
	oLayoutDetails.createRow(oForm1);

	//inserimento dei layout nel TabMaster
	oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_UDCData") /*"Dati UDC"*/ , oLayoutDetails);
	if (bEdit) {
		oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_Content") /*"Contenuto"*/ , oLayout0);
		oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_Movements") /*"Movimenti"*/ , oLayoutMov);
		oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_UDCsConnected") /*"UdC collegate"*/ , oLayoutLink);
		oTabMasterUDCedit.createTab(oLng_Monitor.getText("Monitor_ConfProd") /*"Avanzamenti di produzione"*/ , oLayoutConfProd);
	}

	oEdtDlg.addContent(oTabMasterUDCedit);

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_AlignWithSap"), //"Allinea con SAP"
		icon: 'sap-icon://activate',
		style: sap.ui.commons.ButtonStyle.Accept,
		enabled: false,
		visible: false,
		press: function () {
			sap.ui.commons.MessageBox.show(
				oLng_Monitor.getText("Monitor_UDCaligninigMessage"), //"Allineare la quantità dell'UdC con la quantità indicata in SAP?",
				sap.ui.commons.MessageBox.Icon.QUESTION,
				oLng_Monitor.getText("Monitor_UDCaligninig"), //"Allineamento UdC",
[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
				function (sResult) {
					if (sResult == 'YES') {
						var qexe = dataProdSap + "From/UpdateUdcManualQR" +
							"&Param.1=" + $.UIbyID("txtUDCNR").getValue();

						console.log("query: " + qexe);
						/*fnGetAjaxVal(qexe,["code","message"],false);*/
						var ret = fnExeQuery(
							qexe,
							oLng_Monitor.getText("Monitor_UDCaligned"), //"UdC allineato correttamente", 
							oLng_Monitor.getText("Monitor_UDCalignedError"), //"Errore nell'allineamento dell'UdC",
							false
						);
						refreshFunc();
						$.UIbyID(sOrigTableName).setSelectedIndex(-1);
					}
				},
				sap.ui.commons.MessageBox.Action.YES
			);
		}
	}));

	oEdtDlg.addContent(new sap.ui.commons.layout.MatrixLayout({
		layoutFixed: true,
		height: "20px"
	}));

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Print"), //"Stampa"
		icon: 'sap-icon://print',
		press: function () {
			showPdfDialogV2(sOrigTableName);
		}
	}));

	oEdtDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press: function () {
			refreshFunc();
			oEdtDlg.close();
			oEdtDlg.destroy();
		}
	}));
	oEdtDlg.open();
}

function fnUpdStatusQuery(sTable, refreshFunc, sNewStatus) {

	var qexe = dataProdMon +
		"UDC/updUdCMastStatusNotesSQ" +
		"&Param.1=" + fnGetCellValue(sTable, "UDCNR") +
		"&Param.2=" + sNewStatus +
		"&Param.3=" + fnGetCellValue(sTable, "NOTE_DUDCMAST");

	var ret = fnExeQuery(qexe, oLng_Monitor.getText("Monitor_UDCstatusUpdated"),
		oLng_Monitor.getText("Monitor_UDCstatusUpdatedError")
		/*"Stato dell'UdC aggiornato correttamente", "Errore nell'aggiornamento stato dell'UdC"*/
		,
		false);
	return ret;
}

function fnUpdateStatus(sTable, refreshFunc, sNewStatus) {

	var ret = fnUpdStatusQuery(sTable, refreshFunc, sNewStatus);

	if (ret) {
		refreshFunc();
		$.UIbyID("cmbUDCstatus").setSelectedKey(sNewStatus);
		$.UIbyID("btnCompleteUDCopt").setEnabled(sNewStatus === 'O' ? true : false);
		$.UIbyID("btnOpenUDCopt").setEnabled((sNewStatus === 'C' || sNewStatus === 'E') ? true : false);
		$.UIbyID("btnEmptyUDCopt").setEnabled(sNewStatus === 'C' ? true : false);
	}
}

function fnUpdateStatus_RemoveDetails(sTable, refreshFunc) {

	sap.ui.commons.MessageBox.show(
		oLng_Monitor.getText("Monitor_DetRemovedContinue"), //"I dettagli dell'UdC verranno rimossi, continuare?"
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_Monitor.getText("Monitor_Confirm"), //"Conferma"
[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {

				var sUDCnr = fnGetCellValue(sTable, "UDCNR");
				var qexe = dataProdMon +
					"UDC/emptyUDC_QR" +
					"&Param.1=" + sUDCnr +
					"&Param.2=" + fnGetCellValue(sTable, "PZUDC") +
					"&Param.3=" + fnGetCellValue(sTable, "CURRENT_QTY") +
					"&Param.4=" + fnGetCellValue(sTable, "OPEID") +
					"&Param.5=" + fnGetCellValue(sTable, "MATERIAL") +
					"&Param.6=" + fnGetCellValue(sTable, "ORDER") +
					"&Param.7=" + fnGetCellValue(sTable, "NOTE_DUDCMAST") +
					" - " + oLng_Monitor.getText("Monitor_HandlyEmpty"); //"Svuotato manualmente"

				var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
				var sAnswTitle = "";
				var sAnswMsg = "";

				if (ret.code == 0) {
					sAnswTitle = oLng_Monitor.getText("Monitor_OK"); //"OK"
					sAnswMsg = oLng_Monitor.getText("Monitor_UDCstatusUpdated"); //"Stato dell'UdC aggiornato correttamente"
					$.UIbyID("cmbUDCstatus").setSelectedKey("E");
					$.UIbyID("btnCompleteUDCopt").setEnabled(false);
					$.UIbyID("btnOpenUDCopt").setEnabled(true);
					$.UIbyID("btnEmptyUDCopt").setEnabled(false);
				} else {
					sAnswTitle = oLng_Monitor.getText("Monitor_Error"); //"Errore"
					sAnswMsg = ret.message;
				}

				refreshFunc();
				refreshTabDMSerials(sUDCnr);

				sap.ui.commons.MessageBox.show(
					sAnswMsg,
					(ret.code == 0) ? sap.ui.commons.MessageBox.Icon.SUCCESS : sap.ui.commons.MessageBox.Icon.ERROR,
					sAnswTitle, [sap.ui.commons.MessageBox.Action.OK],
					sap.ui.commons.MessageBox.Action.OK
				);
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}

function fnUpdateNotes(sTable, refreshFunc) {

	var qexe = dataProdMon +
		"UDC/updUdCMastStatusNotesSQ" +
		"&Param.1=" + $.UIbyID("txtUDCNR").getValue() +
		"&Param.2=" + $.UIbyID("cmbUDCstatus").getSelectedKey() +
		"&Param.3=" + $.UIbyID("txtNOTE").getValue();

	var ret = fnExeQuery(
		qexe,
		oLng_Monitor.getText("Monitor_UDCSaved"),
		oLng_Monitor.getText("Monitor_UDCSavedError"),
		false
	);
}

function openEditQtyNextPoperDlg(bEdit, sOrigTable, bEnableModify, refreshFunc) {

	//  Crea la finestra di dialogo
	var oEditQtyNPoperDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgQtyNextPoper",
		maxWidth: "700px",
		maxHeight: "900px",
		title: oLng_Monitor.getText("Monitor_Modify"),
		showCloseButton: false
	});

	var oLayoutQtyNextPoper = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});
	var oFormQtyNextPoper = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayoutQtyNextPoper,
		formContainers: [
new sap.ui.layout.form.FormContainer({
				formElements: [
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Quantity"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "3"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? $.UIbyID("txtCURRENT_QTY").getValue() : "0",
								editable: bEnableModify,
								maxLength: 5,
								id: "txtEditCURRENT_QTY",
								liveChange: function (ev) {
									var oldVal = this['oldValue'];
									var val = ev.getParameter('liveValue');
									var oldFocus = this['oldFocus'];
									var newval = val.replace(/[^.\d]/g, '');
									if (newval.split(".").length - 1 > 1)
										newval = oldVal;
									this.setValue(newval);
									this['oldValue'] = newval;
									if (newval === oldVal) {
										this.applyFocusInfo({
											cursorPos: oldFocus.cursorPos
										});
									} else
										this['oldFocus'] = this.getFocusInfo();
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_NextOrderPoper"),
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "3"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								}),
								value: bEdit ? $.UIbyID("txtNORDER").getValue() : "",
								editable: !bEdit,
								maxLength: 12,
								id: "txtEditNORDER"
							}),
new sap.ui.commons.TextField({
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								value: bEdit ? $.UIbyID("txtNPOPER").getValue() : "",
								editable: !bEdit,
								maxLength: 4,
								id: "txtEditNPOPER"
							}),
new sap.ui.commons.Button({
								text: oLng_Monitor.getText("Monitor_Modify"),
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								enabled: true,
								id: "btnEditNPoper",
								press: function () {
									var oParams = {
										Selected: /*bEdit?$.UIbyID("txtEditNPOPER").getValue():*/ "",
										Order: $.UIbyID("txtEditNORDER").getValue(),
										Plant: ($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
									};

									fnGetOrderOpersTableDialog(
										oParams,
										setSelectedPoperModifyV2
									);
								}
							})
]
					})
]
			})
]
	});

	oEditQtyNPoperDlg.addContent(oFormQtyNextPoper);

	oEditQtyNPoperDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Confirm"),
		press: function () {
			$.UIbyID("txtCURRENT_QTY").setValue($.UIbyID("txtEditCURRENT_QTY").getValue());
			$.UIbyID("txtNORDER").setValue($.UIbyID("txtEditNORDER").getValue());
			$.UIbyID("txtNPOPER").setValue($.UIbyID("txtEditNPOPER").getValue());
			fnUpdQtyNextPoper(sOrigTable, refreshFunc);
			$.UIbyID("dlgQtyNextPoper").close();
			$.UIbyID("dlgQtyNextPoper").destroy();
		}
	}));

	oEditQtyNPoperDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Cancel"),
		press: function () {
			$.UIbyID("dlgQtyNextPoper").close();
			$.UIbyID("dlgQtyNextPoper").destroy();
		}
	}));

	oEditQtyNPoperDlg.open();
}

function fnUpdQtyNextPoper(sTable, refreshFunc) {

	var qexe = dataProdMovement +
		"UDC/NextGen/updQtyNextPoperQR" +
		"&Param.1=" + fnGetCellValue(sTable, "PLANT") +
		"&Param.2=" + $.UIbyID("txtUDCNR").getValue() +
		"&Param.3=" + $.UIbyID("txtCURRENT_QTY").getValue() +
		"&Param.4=" + $.UIbyID("txtNPOPER").getValue() +
		"&Param.5=" + sLanguage;

	var ret = fnGetAjaxVal(qexe, ["code", "message"], false);

	sap.ui.commons.MessageBox.show(
		(ret.code == 0) ? oLng_Monitor.getText("Monitor_UDCSaved") : ret.message,
		(ret.code == 0) ? sap.ui.commons.MessageBox.Icon.SUCCESS : sap.ui.commons.MessageBox.Icon.ERROR,
		(ret.code == 0) ? oLng_Monitor.getText("Monitor_OK") : oLng_Monitor.getText("Monitor_Error"), [sap.ui.commons.MessageBox.Action.OK],
		sap.ui.commons.MessageBox.Action.OK
	);
}

function setSelectedPoperModifyV2(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sPoper = oControlEvent.getSource().getModel().getProperty("POPER", oSelContext);
	var sPopeDesc = oControlEvent.getSource().getModel().getProperty("CICTXT", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtEditNPOPER").setValue(sPoper /* + " - " + sPopeDesc*/ ).data("SelectedKey", sPoper);
	$.UIbyID("dlgOrderPopersTableSelect").destroy();
}

function showPdfDialogV2(sTable) {

	var sPrintUrl = "/XMII/Runner?";
	sPrintUrl += "Transaction=ProductionMain/Movement/Print/print_ETI_PROD_NextGen_TR";
	sPrintUrl += "&OutputParameter=pdfString&Content-Type=application/pdf&isBinary=true";
	sPrintUrl += "&UDCNR=" + fnGetCellValue(sTable, "UDCNR");
	sPrintUrl += "&Language=" + sLanguage;

	var oHtml = new sap.ui.core.HTML();
	oHtml.setContent('<iframe width=100% height=100% src="' + sPrintUrl + '"></iframe>');

	var oPrintDialog = new sap.m.Dialog({
		//resizable: true,
		horizontalScrolling: false,
		verticalScrolling: false,
		contentWidth: '100%',
		contentHeight: '100%',
		customHeader: new sap.m.Toolbar({
			content: [
new sap.m.ToolbarSpacer(),
new sap.m.Button({
					icon: "sap-icon://decline",
					press: function () {
						oHtml.destroy();
						oPrintDialog.close();
					}
				})
]
		}),
		content: oHtml,
		afterClose: function () {
			oPrintDialog.destroy();
		}
	}) /*.addStyleClass('noPaddingDialog fullHeightScrollCont')*/ ;

	oPrintDialog.open();
}