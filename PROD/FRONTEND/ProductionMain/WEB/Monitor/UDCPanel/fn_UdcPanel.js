/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor UDC
//Author: Bruno Rosati
//Date:   21/02/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Monitor UDC

var oPlantFOperator;
var oCmbCID;

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabUDCPanel() {
    
    var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom');
    oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerF.setLocale("it");
    oDatePickerF.attachChange(
		function(oEvent){
			refreshTabUDCPanel();
		}
	);

    var oDatePickerT = new sap.ui.commons.DatePicker('dtTo');
    oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerT.setLocale("it");
    oDatePickerT.attachChange(
		function(oEvent){
			refreshTabUDCPanel();
		}
	);
    
    var sSelectedPlantKey;
    if($.UIbyID("filtPlant").getSelectedKey() === ""){
        sSelectedPlantKey = $('#plant').val();
    }
    else{
        sSelectedPlantKey = $.UIbyID("filtPlant").getSelectedKey();
    }
    
    var oModelShift = new sap.ui.model.xml.XMLModel();
	oModelShift.loadData(
        QService + dataProdMD +"Shifts/getShiftsByPlantSQ&Param.1=" + sSelectedPlantKey
    );
	// Crea la ComboBox per le divisioni in input
	var oCmbShift = new sap.ui.commons.ComboBox({
        id: "cmbShift"
    });
	oCmbShift.setModel(oModelShift);		
	var oItemShift = new sap.ui.core.ListItem();
	oItemShift.bindProperty("text", "SHNAME");
	oItemShift.bindProperty("key", "SHIFT");	
	oCmbShift.bindItems("/Rowset/Row", oItemShift);
    oCmbShift.attachChange(
		function(oEvent){
            $.UIbyID("delFilter").setEnabled(true);
			refreshTabUDCPanel();
		}
	);
    
    // Crea la ComboBox per selezionare la tipologia dell'UDC
    var oModelUdcType = new sap.ui.model.xml.XMLModel();
	oModelUdcType.loadData(QService + dataProdMon +"UDC/getUDCtypesSQ&Param.1=" + sLanguage);
	var oCmbUdcType = new sap.ui.commons.ComboBox({
        id: "cmbUdcType"
    });
	oCmbUdcType.setModel(oModelUdcType);		
	var oItemUdcType = new sap.ui.core.ListItem();
	oItemUdcType.bindProperty("text", "EXTENDED_TEXT");
	oItemUdcType.bindProperty("key", "ORIG_VALUE");	
	oCmbUdcType.bindItems("/Rowset/Row", oItemUdcType);
    oCmbUdcType.attachChange(
		function(oEvent){
            $.UIbyID("delFilter").setEnabled(true);
			refreshTabUDCPanel();
		}
	);
    
    // Crea la ComboBox per selezionare la tipologia di stato dell'UDC
    var oUdcStatusModel = new sap.ui.model.xml.XMLModel();
    oUdcStatusModel.loadData(QService + dataProdMon + "UDC/getUDCstateTypesSQ&Param.1=" + sLanguage);
    var oCmbUDCstatus = new sap.ui.commons.ComboBox({
        id: "cmbUDCstatusFilter"
    });
    oCmbUDCstatus.setModel(oUdcStatusModel);
    var oItemUDCstatus = new sap.ui.core.ListItem();
    oItemUDCstatus.bindProperty("text", "EXTENDED_TEXT");
	oItemUDCstatus.bindProperty("key", "ORIG_VALUE");	
	oCmbUDCstatus.bindItems("/Rowset/Row", oItemUDCstatus);
    oCmbUDCstatus.attachChange(
		function(oEvent){
            $.UIbyID("delFilter").setEnabled(true);
			refreshTabUDCPanel();
		}
	);
    
    //Crea L'oggetto Tabella Monitor UDC
    var oTable = UI5Utils.init_UI5_Table({
        id: "UDCPanelTab",
        properties: {
            title: oLng_Monitor.getText("Monitor_UDCList"), //"Lista UDC",
            visibleRowCount: 15,
            fixedColumnCount: 3,
            width: "98%", //"1285px",
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Single,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                try{
                    $.UIbyID("dlgUDCdet").destroy();
                }
                catch(err){}
                if ($.UIbyID("UDCPanelTab").getSelectedIndex() == -1) {
                    $.UIbyID("btnDetUDC").setEnabled(false);
                    $.UIbyID("btnPrintUDC").setEnabled(false);                    
                }
                else {
                    $.UIbyID("btnDetUDC").setEnabled(true);
                    $.UIbyID("btnPrintUDC").setEnabled(true);
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_UdcDetail"), //"Dettaglio UdC",
                        id: "btnDetUDC",
                        icon: 'sap-icon://display-more',
                        enabled: false,
                        press: function (){
                            let sVersion = fnGetCellValue("UDCPanelTab", "TCONFVER");
							if (sVersion == "V2") {
								openUDCdetV2(true, "UDCPanelTab", refreshTabUDCPanel, false);
							} else {
								openUDCdet(true, "UDCPanelTab", refreshTabUDCPanel, false);
							}
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Print"), //"Stampa",
                        id: "btnPrintUDC",
                        icon: 'sap-icon://print',
                        enabled: false,
                        press: function (){
                            showPdfDialog("UDCPanelTab");
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabUDCPanel();
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.layout.VerticalLayout({
                content: [
                    new sap.ui.commons.Toolbar({
                        items: [
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_From"), //"Da:",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oDatePickerF,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_To"), //"a:",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oDatePickerT,
                            /*new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oCmbShift,*/
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            new  sap.ui.commons.TextField("txtOpeFiter", {
                                value: "",
                                editable: false,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                            }),
                            new sap.ui.commons.Button("btnOpeFilter", {
                                icon: "sap-icon://arrow-down",
                                tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
                                enabled: true,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                                press: function (){
                                    fnGetOperatorTableDialog(
                                        ($.UIbyID("filtPlant").getSelectedKey() === "")?('#plant').val():$.UIbyID("filtPlant").getSelectedKey(),
                                        "",
                                        setSelectedOperator
                                    );
                                }
                            })
                        ]
                    }),
                    new sap.ui.commons.Toolbar({
                        items: [
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oCmbUdcType,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_UdcState"), //"Stato UDC",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oCmbUDCstatus,
                            new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
								layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
							}),
							new  sap.ui.commons.TextField("txtUDCNRFiter", {
								value: "",
								editable: true,
								layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
							}).attachBrowserEvent('keypress', function(e){
                                if(e.which == 13){
                                    refreshTabUDCPanel();
                                }
                            }),
                            new sap.ui.commons.Button({
                                icon: "sap-icon://filter",
                                id: "delFilter",
                                tooltip: oLng_Monitor.getText("Monitor_RemoveFilters"), //"Rimuovi filtri"
                                enabled: true,
                                press: function () {
                                    $.UIbyID("cmbShift").setSelectedKey("");
                                    $.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
                                    $.UIbyID("cmbUdcType").setSelectedKey("");
                                    $.UIbyID("cmbUDCstatusFilter").setSelectedKey("");
                                    $.UIbyID("txtUDCNRFiter").setValue("");
                                    refreshTabUDCPanel();
                                }
                            })
                        ]
                    })
				],
                width: "100%"
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "UDCNR", 
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
				properties: {
					width: "170px",
                    //resizable: true,
                    flexible : false
				}
            },
            {
				Field: "EXTENDED_TEXT_3", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    //flexible : false
				}
            },
            {
				Field: "EXTENDED_TEXT_2", 
				label: oLng_Monitor.getText("Monitor_UdcState"), //"Stato UDC",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "ORIGIN", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "FLOWID",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "TPOPESAP",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
			},
			{
				Field: "UDC_STATE", 
				label: oLng_Monitor.getText("Monitor_UdcState"), //"Stato UDC",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "EXTENDED_TEXT_1", 
				label: oLng_Monitor.getText("Monitor_Direction"), //"Direzione",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "TIMEMOD", 
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Data di update",
				properties: {
					width: "150px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "PLANT", 
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "80px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Linea",
				properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "ORDER", 
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
                    flexible : false
				}
            },
            {
				Field: "POPER", 
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
                    flexible : false
				}
            },
            {
				Field: "NORDER", 
				properties: {
					width: "100px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "NPOPER", 
				properties: {
					width: "60px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "TIMEID", 
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "150px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "TIMEID",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "90px",
                    flexible : false,
                    visible: false
				},
				template: {
					type: "Time",
					textAlign: "Center"
                }
            },
            {
				Field: "MATERIAL", 
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "130px",
                    //resizable: true,
                    flexible : false
				}
            },
            {
				Field: "UDCSTAT", 
				label: oLng_Monitor.getText("Monitor_State"), //"Stato",
				properties: {
					width: "65px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "PZUDC", 
				label: oLng_Monitor.getText("Monitor_UDCQuantity"), //"Q.tà UDC",
				properties: {
					width: "90px",
                    flexible : false
				}
            },
            {
				Field: "CURRENT_QTY", 
				label: oLng_Monitor.getText("Monitor_GotQuantity"), //"Q.tà contenuto",
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "QTYRES", 
				label: oLng_Monitor.getText("Monitor_MovementQuantity"), //"Q.tà Movimento",
				properties: {
					width: "110px",
                    flexible : false
				}
            },
            {
				Field: "EXID", 
				label: oLng_Monitor.getText("Monitor_EXID"), //"External ID",
				properties: {
					width: "100px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "OPEID", 
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"PERNR",
				properties: {
					width: "80px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"Nome operatore",
				properties: {
					width: "140px",
                    flexible : false
				}
            },
            {
				Field: "TCONFVER", 
				properties: {
					width: "300px",
                    flexible : false,
                    visible: false
				}
            },
            {
				Field: "NOTE_DUDCMAST", 
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "140px",
                    flexible : false
				}
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    /*refreshTabUDCPanel();*/
    return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabUDCPanel() {
    
    var query = QService + dataProdMon + "UDC/getUDCmonitorSQ";
    var sOperator = ($.UIbyID("txtOpeFiter").data("SelectedKey") === null)?"":$.UIbyID("txtOpeFiter").data("SelectedKey");
    
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        query = query + "&Param.1=" + $('#plant').val();
    }
    else{
        query = query + "&Param.1=" +  $.UIbyID("filtPlant").getSelectedKey();
    }
    query += "&Param.2=" + dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
    query += "&Param.3=" + dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
    query += "&Param.4=" + $.UIbyID("cmbShift").getSelectedKey();
    query += "&Param.5=" + sOperator;
    query += "&Param.6=" + $.UIbyID("cmbUdcType").getSelectedKey();
    query += "&Param.7=" + $.UIbyID("cmbUDCstatusFilter").getSelectedKey();
    query += "&Param.8=" + $.UIbyID("txtUDCNRFiter").getValue();
    query += "&Param.9=" + sLanguage;
    
    $.UIbyID("UDCPanelTab").getModel().loadData(query);
    $.UIbyID("UDCPanelTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function setSelectedOperator(oControlEvent){
    var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("BADGE", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
    $.UIbyID("txtOpeFiter").setValue(sOpeName).data("SelectedKey", sBadge);
    refreshTabUDCPanel();
    $.UIbyID("dlgOperatorTableSelect").destroy();
    $.UIbyID("delFilter").setEnabled(true);   
}