/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300, -W098*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor Conferme Produzione
//Author: Bruno Rosati
//Date:   22/06/2017
//Vers:   1.2.20
//Modifiers: Luca Adanti 22/11/2018
//**************************************************************************************
// Script per pagina Monitor Conferme Produzione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "yyyy-MM-dd"
});
var tmFormat = sap.ui.core.format.DateFormat.getDateInstance({
	pattern: "HH:mm"
});

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
	cache: false
});

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabConfProd() {

	var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom', {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "1"
		}),
		change: function (oEvent) {
			//refreshTabConfProd();
		}
	});
	oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerF.setLocale("it");

	var oDatePickerT = new sap.ui.commons.DatePicker('dtTo', {
		layoutData: new sap.ui.layout.form.GridElementData({
			hCells: "1"
		}),
		change: function (oEvent) {
			//refreshTabConfProd();
		}
	});
	oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerT.setLocale("it");

	//filtro Turno
	var oCmbShiftFilter = new sap.ui.commons.ComboBox("filtShift", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});
	var oItemShift = new sap.ui.core.ListItem();
	oItemShift.bindProperty("key", "SHIFT");
	oItemShift.bindProperty("text", "SHNAME");
	oCmbShiftFilter.bindItems("/Rowset/Row", oItemShift);
	var oShiftFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());
	} else {
		oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
	}
	oCmbShiftFilter.setModel(oShiftFilter);

	var jState = {
		root: []
	};
	jState.root.push({
		key: 'ST',
		text: oLng_Monitor.getText("Monitor_Averted") //"Stornato",
	});
	jState.root.push({
		key: 'ST_N',
		text: oLng_Monitor.getText("Monitor_NotAverted") //"Non stornato",
	});
	jState.root.push({
		key: 'CONF',
		text: oLng_Monitor.getText("Monitor_Confirmed")
	});
	jState.root.push({
		key: 'CONF_N',
		text: oLng_Monitor.getText("Monitor_NotConfirmed")
	});
	jState.root.push({
		key: 'ERR',
		text: oLng_Monitor.getText("Monitor_Error") //"Errore",
	});

	var oStateModel = new sap.ui.model.json.JSONModel();
	oStateModel.setData(jState);

	// Crea la ComboBox per selezionare il tipo delle funzioni da visualizzare
	var oCmbStateFilter = new sap.ui.commons.ComboBox({
		id: "CmbStateFilter",
		change: function () {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});
	oCmbStateFilter.setModel(oStateModel);
	var oItemStateFilter = new sap.ui.core.ListItem();
	oItemStateFilter.bindProperty("text", "text");
	oItemStateFilter.bindProperty("key", "key");
	oCmbStateFilter.bindItems("/root", oItemStateFilter); //valido per JSONModel

	//filtro Linea
	var oCmbLineFilter = new sap.ui.commons.ComboBox("filtLine", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});
	var oItemLine = new sap.ui.core.ListItem();
	oItemLine.bindProperty("key", "IDLINE");
	oItemLine.bindProperty("text", "LINETXT");
	oCmbLineFilter.bindItems("/Rowset/Row", oItemLine);
	var oLineFilter = new sap.ui.model.xml.XMLModel();
	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
	} else {
		oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1" + $.UIbyID("filtPlant").getSelectedKey());
	}
	oCmbLineFilter.setModel(oLineFilter);

	//Filtro Buoni/Sospesi
	var oCmbTypeConfFilter = new sap.ui.commons.ComboBox("filtTypeConf", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});
	var oItemTypeConf = new sap.ui.core.ListItem();
	oItemTypeConf.bindProperty("key", "MII_TYPECONF");
	oItemTypeConf.bindProperty("text", "EXTENDED_TEXT");
	oCmbTypeConfFilter.bindItems("/Rowset/Row", oItemTypeConf);
	var oTypeConfModel = new sap.ui.model.xml.XMLModel();
	oTypeConfModel.loadData(QService + dataConfProd + "getMtypeconfSQ&Param.1=" + sLanguage);
	oCmbTypeConfFilter.setModel(oTypeConfModel);

	var oChkShowDelReq = new sap.ui.commons.CheckBox("chkShowDelReq", {
		change: function (oEvent) {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});

	var jQtyUDC = {
		root: []
	};
	jQtyUDC.root.push({
		key: 'EqualZero',
		text: oLng_Monitor.getText("Monitor_QtyUDCeqalZero") //"Q.tà UdC uguale a zero"
	});
	jQtyUDC.root.push({
		key: 'BigThanZero',
		text: oLng_Monitor.getText("Monitor_QtyUDCbigThanZero") //"Q.tà UdC maggiore di zero"
	});

	var oQtyUDCmodel = new sap.ui.model.json.JSONModel();
	oQtyUDCmodel.setData(jQtyUDC);
	console.log(jQtyUDC);

	// Crea la ComboBox per selezionare la quantità delle UdC
	var oCmbQtyUDCfilter = new sap.ui.commons.ComboBox({
		id: "CmbQtyUDCfilter",
		change: function () {
			$.UIbyID("delFilter").setEnabled(true);
			//refreshTabConfProd();
		}
	});
	oCmbQtyUDCfilter.setModel(oQtyUDCmodel);
	var oItemQtyUDCfilter = new sap.ui.core.ListItem();
	oItemQtyUDCfilter.bindProperty("text", "text");
	oItemQtyUDCfilter.bindProperty("key", "key");
	oCmbQtyUDCfilter.bindItems("/root", oItemQtyUDCfilter); //valido per JSONModel

	//Crea L'oggetto Tabella
	var oTable = UI5Utils.init_UI5_Table({
		id: "ConfProdTab",
		exportButton: true,
		properties: {
			//title: oLng_Monitor.getText("Monitor_ConfProdList"), //"Lista avanzamenti di produzione",
			visibleRowCount: 15,
			fixedColumnCount: 3,
			width: "100%", //"1285px",//
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function (oControlEvent) {
				try {
					$.UIbyID("dlgUDCdet").destroy();
					$.UIbyID("dlgConfProdErrDet").destroy();
				} catch (err) {}
				if ($.UIbyID("ConfProdTab").getSelectedIndex() == -1) {
					$.UIbyID("btnDelConfProd").setEnabled(false);
					$.UIbyID("btnModConfProd").setEnabled(false);
					$.UIbyID("btnForwardConfProd").setEnabled(false);
					$.UIbyID("btnErrorDetail").setEnabled(false);
					$.UIbyID("btnUdcDetail").setEnabled(false);
					$.UIbyID("btnDelAvertReq").setEnabled(false);
					$.UIbyID("btnConfDic").setEnabled(false);
					$.UIbyID("btnErrDic").setEnabled(false);
				} else {
					//Se è già stato effettuato lo storno, non può essere nuovamente stornato
					if (fnGetCellValue("ConfProdTab", "DELETED") == "1") {
						$.UIbyID("btnDelConfProd").setEnabled(false);
					} else {
						$.UIbyID("btnDelConfProd").setEnabled(true);
					}
					//Se la conferma è precedente al mese corrente o il mese prima allora lo storno non può essere effettuato
					var iCurrentMonth = (new Date()).getMonth() + 1;
					var iSelectedMonth = parseInt((fnGetCellValue("ConfProdTab", "DATE_SHIFT")).substring(5, 7));
					console.log(iCurrentMonth);
					console.log(iSelectedMonth);
					if ((iSelectedMonth != iCurrentMonth) && (iSelectedMonth != (iCurrentMonth - 1))) {
						$.UIbyID("btnDelConfProd").setEnabled(false);
					}
					//La rettifica può essere effettuata solo se non è già stato fatto lo storno e se non ci sono errori
					if (fnGetCellValue("ConfProdTab", "DELETED") != "1" && fnGetCellValue("ConfProdTab", "ERROR") != "1") {
						$.UIbyID("btnModConfProd").setEnabled(true);
					} else {
						$.UIbyID("btnModConfProd").setEnabled(false);
					}
					//Se la conferma è già stata rettificata non può essere rettificata di nuovo
					if (fnGetCellValue("ConfProdTab", "TYPEMOD") === "R") {
						$.UIbyID("btnModConfProd").setEnabled(false);
					}
					//Se si è verificato un errore, può essere aperto il dettaglio di errore
					if (fnGetCellValue("ConfProdTab", "ERROR") == "1") {
						$.UIbyID("btnErrorDetail").setEnabled(true);
						//Se MII_GUID è vuoto, non può essere nuovamente inviato a SAP
						if (fnGetCellValue("ConfProdTab", "MII_GUID") == "") {
							$.UIbyID("btnForwardConfProd").setEnabled(false);
						} else {
							$.UIbyID("btnForwardConfProd").setEnabled(true);
						}
					} else {
						$.UIbyID("btnForwardConfProd").setEnabled(false);
						$.UIbyID("btnErrorDetail").setEnabled(false);
					}
					//Se non è presente l'UdC, non può essere aperto il dettaglio
					if (fnGetCellValue("ConfProdTab", "UDCNR") === "") {
						$.UIbyID("btnUdcDetail").setEnabled(false);
					} else {
						$.UIbyID("btnUdcDetail").setEnabled(true);
					}
					//La richiesta di storno può essere rimossa solo se presente e se lo storno non è già stato effettuato
					//Deve esistere l'UdC
					if (fnGetCellValue("ConfProdTab", "UDCNR") != "" &&
						fnGetCellValue("ConfProdTab", "DELREQ") != "0" && fnGetCellValue("ConfProdTab", "DELETED") != "1") {
						$.UIbyID("btnDelAvertReq").setEnabled(true);
					} else {
						$.UIbyID("btnDelAvertReq").setEnabled(false);
					}

					if (fnGetCellValue("ConfProdTab", "ERROR") == "0" && fnGetCellValue("ConfProdTab", "CONFIRMED") == "1" && fnGetCellValue("ConfProdTab", "DELETED") == "0") {
						$.UIbyID("btnConfDic").setEnabled(false);
						$.UIbyID("btnErrDic").setEnabled(false);
					} else {
						$.UIbyID("btnConfDic").setEnabled(true);
						$.UIbyID("btnErrDic").setEnabled(true);
					}
				}
			},
			toolbar: new sap.ui.commons.Toolbar({
				items: [
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ConfProdUndo"), //"Storna"
						id: "btnDelConfProd",
						icon: 'sap-icon://reset',
						enabled: false,
						press: function () {
							fnSendConfProdDialog(false, oLng_Monitor.getText("Monitor_ConfProdUndo") /*"Storna"*/ , "ST");
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ConfProdModify"), //"Rettifica",
						id: "btnModConfProd",
						icon: 'sap-icon://edit',
						enabled: false,
						visible: false,
						press: function () {
							fnSendConfProdDialog(true, oLng_Monitor.getText("Monitor_ConfProdModify") /*"Rettifica"*/ , "RT");
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ConfProdForward"), //"Inoltra"
						id: "btnForwardConfProd",
						icon: 'sap-icon://redo',
						enabled: false,
						press: function () {
							fnSendConfProdDialog(false, oLng_Monitor.getText("Monitor_ConfProdForward") /*"Inoltra"*/ , "FW");
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ErrorDetail"), //"Dettaglio Errore"
						id: "btnErrorDetail",
						icon: 'sap-icon://status-error',
						enabled: false,
						press: function () {
							openConfProdErrorDet(fnGetCellValue("ConfProdTab", "PLANT"), fnGetCellValue("ConfProdTab", "SAP_GUID"));
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_UdcDetail"), //"Dettaglio UdC"
						id: "btnUdcDetail",
						icon: 'sap-icon://display-more',
						enabled: false,
						press: function () {
							let sVersion = fnGetCellValue("ConfProdTab", "TCONFVER");
							if (sVersion == "V2") {
								openUDCdetV2(true, "ConfProdTab", refreshTabConfProd, false);
							} else {
								openUDCdet(true, "ConfProdTab", refreshTabConfProd, false);
							}

						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_DelAvertReq"), //"Rimuovi richiesta storno"
						id: "btnDelAvertReq",
						icon: 'sap-icon://minus',
						enabled: false,
						press: function () {
							fnRemoveAvertRequest(fnGetCellValue("ConfProdTab", "UDCNR"));
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ConfDic"),
						id: "btnConfDic",
						icon: 'sap-icon://accept',
						enabled: false,
						visible: getUserAuth('OMII_ADM_CONF'),
						press: function () {
							modificaDichiarazione(0);
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ErrDic"),
						id: "btnErrDic",
						icon: 'sap-icon://cancel',
						enabled: false,
						visible: getUserAuth('OMII_ADM_CONF'),
						press: function () {
							modificaDichiarazione(1);
						}
					}),
new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_CorrDic"),
						id: "btnCorrDic",
						icon: 'sap-icon://activate',
						enabled: true,
						visible: getUserAuth('OMII_ADM_CONF'),
						press: function () {
							modificaDichiarazione(2);
						}
					})
],
				rightItems: [
new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabConfProd();
						}
					}),
new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("ConfProdTab", "Produzione");
						}
					})
]
			}),
			extension: new sap.ui.commons.layout.VerticalLayout({
				content: [
new sap.ui.commons.Toolbar({
						items: [
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_From"), //"Da:",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
oDatePickerF,
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_To"), //"a:",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
oDatePickerT,
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_MII_GUID"), //"numero UDC",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.TextField("txtMII_GUIDFiter", {
								value: "",
								editable: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}).attachBrowserEvent('keypress', function (e) {
								if (e.which == 13) {
									//refreshTabConfProd();
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.TextField("txtUDCNRFiter", {
								value: "",
								editable: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}).attachBrowserEvent('keypress', function (e) {
								if (e.which == 13) {
									//refreshTabConfProd();
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.TextField("txtMaterialFiter", {
								value: "",
								editable: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}).attachBrowserEvent('keypress', function (e) {
								if (e.which == 13) {
									//refreshTabConfProd();
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.TextField("txtOrderFiter", {
								value: "",
								editable: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}).attachBrowserEvent('keypress', function (e) {
								if (e.which == 13) {
									//refreshTabConfProd();
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_UDCQuantity") //"Q.tà UdC"
							}),
oCmbQtyUDCfilter
]
					}),
new sap.ui.commons.Toolbar({
						items: [
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_Shift") //Turno
							}),
oCmbShiftFilter,
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.TextField("txtOpeFiter", {
								value: "",
								editable: false,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.Button("btnOpeFilter", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									fnGetOperatorTableDialog(
										($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
										"",
										setSelectedOperator
									);
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_ICON") //Stato
							}),
oCmbStateFilter,
/* Al momento non è richiesto il filtro per linea
new sap.ui.commons.Label({
text: oLng_Monitor.getText("Monitor_IDLINE") //Linea
}),
oCmbLineFilter, */
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_Declared") //Dichiar.
							}),
oCmbTypeConfFilter,
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_ShowOnlyAvertRequest") //Visualizza solo richieste di storno
							}),
oChkShowDelReq,
new sap.ui.commons.Button({
								icon: "sap-icon://filter",
								enabled: true,
								tooltip: oLng_Monitor.getText("Monitor_RemoveFilters"), //"Rimuovi filtri"
								id: "delFilter",
								press: function () {
									$.UIbyID("CmbStateFilter").setSelectedKey("");
									$.UIbyID("filtShift").setSelectedKey("");
									$.UIbyID("filtLine").setSelectedKey("");
									$.UIbyID("filtTypeConf").setSelectedKey("");
									$.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
									$.UIbyID("chkShowDelReq").setChecked(false);
									$.UIbyID("txtUDCNRFiter").setValue("");
									$.UIbyID("txtMaterialFiter").setValue("");
									$.UIbyID("txtOrderFiter").setValue("");
									$.UIbyID("txtMII_GUIDFiter").setValue("");
									$.UIbyID("CmbQtyUDCfilter").setSelectedKey("");
									$.UIbyID("txtIncludedTpopeSapFilter").setValue("").data("SelectedKey", "");
									$.UIbyID("txtExeptedTpopeSapFilter").setValue("01, 08").data("SelectedKey", "'01','08'");
									//$.UIbyID("delFilter").setEnabled(false);

									//set group of table and column to false
									$.UIbyID("ConfProdTab").setEnableGrouping(false);
									$.UIbyID("ConfProdTab").getColumns()[0].setGrouped(false);
									var oListBinding = $.UIbyID("ConfProdTab").getBinding();
									oListBinding.aSorters = null;
									oListBinding.aFilters = null;
									$.UIbyID("ConfProdTab").getModel().refresh(true);
									//after reset, set the enableGrouping back to true
									$.UIbyID("ConfProdTab").setEnableGrouping(true);

									$.UIbyID("ConfProdTab").getColumns().forEach(function (item) {
										item.setSorted(false);
										item.setFiltered(false);
									});

									//refreshTabConfProd();
								}
							}),
new sap.ui.commons.Label({
								id: "lblRecordNumber",
								text: oLng_Monitor.getText("Monitor_RecordNumber"), //"N. record"
								textAlign: "Right"
							})
]
					}),
new sap.ui.commons.Toolbar({
						items: [
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_IncludedSapOperationType")
							}),
new sap.ui.commons.TextField("txtIncludedTpopeSapFilter", {
								value: "",
								editable: false,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.Button("btnIncludeTpopeSapFilter", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectIncludedSapOperationType"),
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									let sPlant = $.UIbyID("filtPlant").getSelectedKey() === "" ?
										$('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
									fnGetTPopeSapTableDialog(sPlant, "", setSelectedIncludedTPopeSap);
								}
							}),
new sap.ui.commons.Label({
								text: oLng_Monitor.getText("Monitor_ExeptedSapOperationType")
							}),
new sap.ui.commons.TextField("txtExeptedTpopeSapFilter", {
								value: "",
								editable: false,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "2"
								})
							}),
new sap.ui.commons.Button("btnExeptedTpopeSapFilter", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectExeptedSapOperationType"),
								enabled: true,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									let sPlant = $.UIbyID("filtPlant").getSelectedKey() === "" ?
										$('#plant').val() : $.UIbyID("filtPlant").getSelectedKey();
									fnGetTPopeSapTableDialog(sPlant, "", setSelectedExeptedTPopeSap);
								}
							})
]
					})
]
			})
		},
		exportButton: false,
		columns: [
			{
				Field: "statusIcon",
				label: oLng_Monitor.getText("Monitor_ICON"), //"Stato",
				tooltipCol: "{statusToolTip}",
				iconColor: "{statusColor}",
				properties: {
					width: "70px",
					height: "30px",
					autoResizable: false,
				},
				template: {
					type: "coreIcon",
					textAlign: "Center"
				}
},
			{
				Field: "EXTENDED_TEXT",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "DICINFO",
				label: "Info Flusso", //oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				tooltip: "Info Flusso",
				properties: {
					width: "110px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "DELREQ",
				label: oLng_Monitor.getText("Monitor_AvertRequest"), //"Rich. storno",
				tooltip: oLng_Monitor.getText("Monitor_AvertRequestExtended"), //"Richiesta di Storno operatore",
				properties: {
					width: "40px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
},
			{
				Field: "TYPECONF",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				properties: {
					width: "90px",
					visible: false,
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "OPEID",
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "80px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "OPENAME",
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"nome Operatore",
				properties: {
					width: "130px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "Date",
					textAlign: "Center"
				}
},
			{
				Field: "SHIFT",
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "TIME_ID",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "130px",
					visible: false,
					//resizable : false,
					flexible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
},
			{
				Field: "TIME_ID_CALCULATED",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "Time",
					textAlign: "Center"
				}
},
			{
				Field: "PLANT",
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "70px",
					visible: false,
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "IDLINE",
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
					resizable: true,
					flexible: false
				}
},
			{
				Field: "LINETXT",
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "140px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "ORDER",
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "POPER",
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
				properties: {
					width: "140px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "CURRENT_QTY",
				label: oLng_Monitor.getText("Monitor_UDCQuantity"), //"Q.tà UdC",
				properties: {
					width: "80px",
					//resizable : false,
					flexible: false,
					visible: true
				}
},
			{
				Field: "SREASID",
				properties: {
					width: "70px",
					visible: false
					//resizable : false,
					//flexible : false
				}
},
			{
				Field: "SREASTXT",
				label: oLng_Monitor.getText("Monitor_ScrabReason"), //"Causale di scarto",
				properties: {
					width: "140px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "QTY",
				label: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
				properties: {
					width: "80px",
					//resizable : false,
					flexible: false
				}
},
//MF20200107 inizio
			{
				Field: "QTY_CTRL",
				label: oLng_Monitor.getText("Monitor_Quantity_ctrl"), //"Quantità controllata",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false
				}
},
			//MF20200107 fine
			{
				Field: "QTY_PRO",
				label: oLng_Monitor.getText("Monitor_QTYPROD"), //"Dic.Prod",
				properties: {
					width: "75px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "DATARIO",
				label: oLng_Monitor.getText("Monitor_Datary"), //"Datario",
				properties: {
					width: "80px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "WTDUR",
				label: oLng_Monitor.getText("Monitor_Minutes"), //"Minuti",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
},
			{
				Field: "CESTA_TT",
				label: oLng_Monitor.getText("Monitor_CESTA_TT"), //"Cesta TT",
				properties: {
					width: "110px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "TCONFVER",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "NORDER",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "NPOPER",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "DCONFPROD_NORDER",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "DCONFPROD_NPOPER",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "TPOPESAP",
				properties: {
					width: "110px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "NRPRNT",
				label: oLng_Monitor.getText("Monitor_PrintNR"), //"nr. Stampo",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "110px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "MATERIAL_DESC",
				label: oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "150px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "TIME_ID",
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "150px",
					//resizable : false,
					flexible: false,
					visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
},
			{
				Field: "DATE_UPD",
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Data aggiornamento",
				properties: {
					width: "150px",
					//resizable : false,
					flexible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
},
			{
				Field: "CONFIRMED",
				label: oLng_Monitor.getText("Monitor_Confirmed"), //"Confermato",
				properties: {
					width: "100px",
					//resizable : false,
					flexible: false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
},
			{
				Field: "DELETED",
				label: oLng_Monitor.getText("Monitor_Deleted"), //"Cancellato",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
},
			{
				Field: "USERMOD",
				label: oLng_Monitor.getText("Monitor_User"), //"Utente",
				properties: {
					width: "75px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "TYPEMOD",
				label: oLng_Monitor.getText("Monitor_TypeMod"), //"Tipo Modifica",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "EXTENDED_TYPEMOD",
				label: oLng_Monitor.getText("Monitor_TypeMod"), //"Tipo Modifica",
				properties: {
					width: "120px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "UMDUR",
				//label: oLng_Monitor.getText("Monitor_EXID"), //"External ID",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "ERROR",
				label: oLng_Monitor.getText("Monitor_Error"), //"Errore",
				properties: {
					width: "90px",
					//resizable : false,
					flexible: false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
},
			{
				Field: "SAP_GUID",
				label: oLng_Monitor.getText("Monitor_SAP_GUID"), //"SAP GUID",
				properties: {
					width: "280px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "UDC_STATE",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "PZUDC",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "EXID",
				properties: {
					width: "70px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "NOTE",
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "200px",
					//resizable : false,
					flexible: false
				}
},
			{
				Field: "NOTE_DUDCMAST",
				label: oLng_Monitor.getText("Monitor_UDCNotes"), //"Note UdC",
				properties: {
					width: "200px",
					//resizable : false,
					flexible: false,
					visible: false
				}
},
			{
				Field: "ORIGIN",
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
					visible: false,
					flexible: false
				}
},
			{
				Field: "EXTENDED_ORIGIN",
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
					flexible: false,
					visible: false
				}
},
			{
				Field: "MII_GUID",
				label: oLng_Monitor.getText("Monitor_MII_GUID"), //"GUID di MII",
				properties: {
					width: "150px",
					//resizable : false,
					flexible: false,
					//visible: false
				}
},
			{
				Field: "MII_GUID_ST",
				label: oLng_Monitor.getText("Monitor_Avert_MII_GUID"), //"GUID stornato",
				properties: {
					width: "170px",
					//resizable : false,
					flexible: false,
					//visible: false
				}
}
]
	});

	oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");

	$.UIbyID("txtExeptedTpopeSapFilter").setValue("01, 08").data("SelectedKey", "'01','08'");
	/*refreshTabConfProd();*/
	return oTable;
}

function refreshTabConfProd() {

	var sDateFrom = dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
	var sOperator = ($.UIbyID("txtOpeFiter").data("SelectedKey") === null) ? "" : $.UIbyID("txtOpeFiter").data("SelectedKey");
	var sPlant = "";
	var sSelectedIncludedTPopeSap =
		encodeURIComponent(($.UIbyID("txtIncludedTpopeSapFilter").data("SelectedKey") == null) ? "" : $.UIbyID("txtIncludedTpopeSapFilter").data("SelectedKey"));
	var sSelectedExeptedTPopeSap =
		encodeURIComponent(($.UIbyID("txtExeptedTpopeSapFilter").data("SelectedKey") == null) ? "" : $.UIbyID("txtExeptedTpopeSapFilter").data("SelectedKey"));

	if ($.UIbyID("filtPlant").getSelectedKey() === "") {
		sPlant = $('#plant').val();
	} else {
		sPlant = $.UIbyID("filtPlant").getSelectedKey();
	}

	var qParams = {
		data: "ProductionMain/Monitor/ConfProd/getConfProdByParamsQR" +
			"&Param.1=" + sPlant +
			"&Param.2=" + $.UIbyID("filtShift").getSelectedKey() +
			"&Param.3=" + $.UIbyID("filtLine").getSelectedKey() +
			"&Param.4=" + $.UIbyID("filtTypeConf").getSelectedKey() +
			"&Param.5=" + sDateFrom +
			"&Param.6=" + sDateTo +
			"&Param.7=" + sOperator +
			"&Param.8=" + ($.UIbyID("chkShowDelReq").getChecked() ? "0" : "2") +
			"&Param.9=" + $.UIbyID("CmbStateFilter").getSelectedKey() +
			"&Param.10=" + $.UIbyID("txtUDCNRFiter").getValue() +
			"&Param.11=" + $.UIbyID("txtMaterialFiter").getValue() +
			"&Param.12=" + $.UIbyID("txtOrderFiter").getValue() +
			"&Param.13=" + $.UIbyID("CmbQtyUDCfilter").getSelectedKey() +
			"&Param.14=" + $.UIbyID("txtMII_GUIDFiter").getValue() +
			"&Param.15=" + sSelectedIncludedTPopeSap +
			"&Param.16=" + sSelectedExeptedTPopeSap +
			"&Param.17=" + sLanguage,
		dataType: "xml"
	};

	$.UIbyID("ConfProdTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
		// on success
		.done(function (data) {
			$.UIbyID("ConfProdTab").setBusy(true);
			// carica il model della tabella
			$.UIbyID("ConfProdTab").getModel().setData(data);
		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
		})
		// always, either on success or fail
		.always(function () {

			// remove busy indicator
			$.UIbyID("ConfProdTab").setBusy(false);
			var rowsetObject = $.UIbyID("ConfProdTab").getModel().getObject("/Rowset/0");
			if (rowsetObject) {
				var iRecNumber = rowsetObject.childNodes.length - 1;
				$.UIbyID("lblRecordNumber").setText(oLng_Monitor.getText("Monitor_RecordNumber") /*"N. record" */ + " " + iRecNumber);
			}
		});

	$.UIbyID("ConfProdTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}

function fnSendConfProdDialog(bShowFields, sTitle, sSendMode) {

	//controllo se l'Udc è già utilizzata da altre UdC per impedirne la rettifica
	if (sSendMode === "RT") {
		var qexe = dataProdMon + "UDC/checkIfUDCNRisAvertableQR" +
			"&Param.1=" + fnGetCellValue("ConfProdTab", "UDCNR") + "&Param.2=" + sLanguage;
		var ret = fnGetAjaxVal(qexe, ["code", "message"], false);
		if (ret.code == 1) {
			//Se l'UdC è già utilizzata da altre UdC allora non può essere rettificata
			sap.ui.commons.MessageBox.show(
				oLng_Monitor.getText("Monitor_UDCAlreadyUsed"), //L'UdC è già stata consumata e non può essere rettificata
				sap.ui.commons.MessageBox.Icon.ERROR,
				oLng_Monitor.getText("Monitor_Error"), //Errore
[sap.ui.commons.MessageBox.Action.OK],
				sap.ui.commons.MessageBox.Action.OK
			);
			$.UIbyID("ConfProdTab").setSelectedIndex(-1);
			return;
		}
	}

	//Nel caso in cui la quantità del movimento sia maggiore alla quantità corrente, l'UdC non può essere stornata
	if (sSendMode === "ST" &&
		(parseInt(fnGetCellValue("ConfProdTab", "CURRENT_QTY")) < parseInt(fnGetCellValue("ConfProdTab", "QTY")))
	) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_CurrQtyLowerThanQtyAvertError"), //La quantità attuale dell'UdC è inferiore alla quantità del movimento quindi lo storno non può essere effettuato
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_AvertError"), //Errore Storno
[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		$.UIbyID("ConfProdTab").setSelectedIndex(-1);
		return;
	}

	//Se viene rettificata una riparazione o rilavorazione (completa o incompleta)
	//allora la quantità massima non può superare quella già presente
	var maxQty = false;
	if (sSendMode === "RT" && (
			fnGetCellValue("ConfProdTab", "TYPECONF") == "T" || fnGetCellValue("ConfProdTab", "TYPECONF") == "U" ||
			fnGetCellValue("ConfProdTab", "TYPECONF") == "V" || fnGetCellValue("ConfProdTab", "TYPECONF") == "X")) {
		maxQty = true;
	}

	//  Crea la finestra di dialogo
	var oRetConfProdDlg = new sap.ui.commons.Dialog({
		modal: true,
		resizable: true,
		id: "dlgRetConfProd",
		maxWidth: "600px",
		maxHeight: "600px",
		//minHeight: "900px",
		title: sTitle,
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout({
		singleColumn: true
	});
	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
new sap.ui.layout.form.FormContainer({
				formElements: [
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "ORDER"),
								editable: false,
								maxLength: 12,
								id: "txtConfProdORDER"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "POPER"),
								editable: false,
								maxLength: 12,
								id: "txtConfProdPOPER"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "UDCNR"),
								editable: false,
								maxLength: 20,
								id: "txtConfProdUDCNR"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "QTY"),
								editable: true,
								maxLength: 5,
								visible: bShowFields,
								id: "txtConfProdQTY",
								liveChange: function (ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_Datary"), //"Datario",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "DATARIO"),
								editable: true,
								maxLength: 4,
								visible: bShowFields,
								id: "txtConfProdDATARIO",
								liveChange: function (ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_CESTA_TT"), //"Cesta TT",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "CESTA_TT"),
								editable: true,
								maxLength: 40,
								visible: bShowFields,
								id: "txtConfProdCESTA_TT"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_Minutes"), //"Minuti",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "WTDUR"),
								editable: true,
								maxLength: 4,
								visible: bShowFields,
								id: "txtConfProdWTDUR",
								liveChange: function (ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_PrintNR"), //"nr. Stampo",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "NRPRNT"),
								editable: true,
								maxLength: 40,
								visible: bShowFields,
								id: "txtConfProdNRPRNT"
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							visible: bShowFields,
							text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								editable: false,
								maxLength: 200,
								visible: bShowFields,
								id: "txtConfProdOPEID"
							}),
new sap.ui.commons.Button("btnOpeSelect", {
								icon: "sap-icon://arrow-down",
								tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
								enabled: true,
								visible: bShowFields,
								layoutData: new sap.ui.layout.form.GridElementData({
									hCells: "1"
								}),
								press: function () {
									fnGetOperatorTableDialog(
										($.UIbyID("filtPlant").getSelectedKey() === "") ? ('#plant').val() : $.UIbyID("filtPlant").getSelectedKey(),
										"",
										setSelectedOpeModify
									);
								}
							})
]
					}),
new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Notes"), //"Note",
							layoutData: new sap.ui.layout.form.GridElementData({
								hCells: "2"
							})
						}),
						fields: [
new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "NOTE"),
								editable: true,
								maxLength: 200,
								id: "txtNote"
							})
]
					})
]
			})
]
	});

	$.UIbyID("txtConfProdOPEID").setValue(
		fnGetCellValue("ConfProdTab", "OPENAME")
	).data("SelectedKey", fnGetCellValue("ConfProdTab", "OPEID"));

	oRetConfProdDlg.addContent(oForm1);

	oRetConfProdDlg.addButton(new sap.ui.commons.Button({
		text: sTitle,
		press: function () {
			switch (sSendMode) {
				case "RT":
					if (maxQty && (parseInt($.UIbyID("txtConfProdQTY").getValue()) > parseInt(fnGetCellValue("ConfProdTab", "QTY")))) {
						sap.ui.commons.MessageBox.show(
							oLng_Monitor.getText("Monitor_ModifyRetRepQtyMessage"), //"La rettifica della quantità di una riparazione o rilavorazione (completa o incompleta) non può superare la quantità già presente"
							sap.ui.commons.MessageBox.Icon.ERROR,
							oLng_Monitor.getText("Monitor_Error"), //"Errore"
[sap.ui.commons.MessageBox.Action.OK],
							sap.ui.commons.MessageBox.Action.OK
						);
						return;
					}
					fnSendToSap(
						sSendMode,
						$.UIbyID("txtNote").getValue(),
						$.UIbyID("txtConfProdQTY").getValue(),
						$.UIbyID("txtConfProdDATARIO").getValue(),
						$.UIbyID("txtConfProdCESTA_TT").getValue(),
						$.UIbyID("txtConfProdOPEID").data("SelectedKey"),
						$.UIbyID("txtConfProdWTDUR").getValue(),
						$.UIbyID("txtConfProdNRPRNT").getValue()

					);
					break;
				case "ST":
					fnSendToSap(
						sSendMode,
						$.UIbyID("txtNote").getValue(),
						"0",
						fnGetCellValue("ConfProdTab", "DATARIO"),
						fnGetCellValue("ConfProdTab", "CESTA_TT"),
						fnGetCellValue("ConfProdTab", "OPEID"),
						fnGetCellValue("ConfProdTab", "WTDUR"),
						fnGetCellValue("ConfProdTab", "NRPRNT")
					);
					break;
				case "FW":
					fnSendToSap(
						sSendMode,
						$.UIbyID("txtNote").getValue(),
						fnGetCellValue("ConfProdTab", "QTY"),
						fnGetCellValue("ConfProdTab", "DATARIO"),
						fnGetCellValue("ConfProdTab", "CESTA_TT"),
						fnGetCellValue("ConfProdTab", "OPEID"),
						fnGetCellValue("ConfProdTab", "WTDUR"),
						fnGetCellValue("ConfProdTab", "NRPRNT")
					);
					break;
			}

		}
	}));

	oRetConfProdDlg.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press: function () {
			refreshTabConfProd();
			oRetConfProdDlg.close();
			oRetConfProdDlg.destroy();
		}
	}));
	oRetConfProdDlg.open();
}

function fnSendToSap(sMod, sNote, sNewQuantity, sDatario, sCestaTT, sOpeID, iWtDur, sNrPrint) {

	if (sMod == "ST" && fnGetCellValue("ConfProdTab", "CONFIRMED") == "0") {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_ConfProdOutOfSap"), //La conferma non è stata inviata a SAP e non può essere stornata
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_AvertError"), //Errore Storno
[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	if (parseInt(sDatario.substring(0, 2)) > 12) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertCorrectDatario"), //Inserire un datario valido
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	var currentYear = ((new Date()).getFullYear()).toString();

	if (parseInt(sDatario.substring(2, 4)) > (parseInt(currentYear.substring(2, 4)) + 1) ||
		parseInt(sDatario.substring(2, 4)) < (parseInt(currentYear.substring(2, 4)) - 1)) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertCorrectDatario"), //Inserire un datario valido
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}


	if (sNote == "" || sNote == null) {
		sap.ui.commons.MessageBox.show(
			oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
			sap.ui.commons.MessageBox.Icon.ERROR,
			oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
[sap.ui.commons.MessageBox.Action.OK],
			sap.ui.commons.MessageBox.Action.OK
		);
		return;
	}

	/*
	--- INVIO CON RICHIESTA DI CONFERMA ---
	var sMsgTitle;
	var sMsgTxt;
	if(sMod == "ST"){
	sMsgTitle = oLng_Monitor.getText("Monitor_AvertValidation"); //"Storno"
	sMsgTxt = oLng_Monitor.getText("Monitor_AvertConfProd"); //"Stornare la conferma selezionata?"
	}
	if(sMod == "RT"){
	sMsgTitle = oLng_Monitor.getText("Monitor_ConfProdModify"); //"Rettifica"
	sMsgTxt = oLng_Monitor.getText("Monitor_ConfProdModifyConfirm"); //"Rettificare la conferma selezionata?"
	}
	if(sMod == "FW"){
	sMsgTitle = oLng_Monitor.getText("Monitor_Forward"); //"Inoltro"
	sMsgTxt = oLng_Monitor.getText("Monitor_ForwardConfProd"); //"Inoltrare nuovamente la conferma selezionata?"
	}

	sap.ui.commons.MessageBox.show(
	sMsgTxt,
	sap.ui.commons.MessageBox.Icon.WARNING,
	sMsgTitle,
	[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
	function(sResult){
	if (sResult == 'YES'){
	var sAnswTitle = ""; //codice imballo, da lasciare vuoto
	var qexe = dataProdSap + "To/renewConfProdToSapQR" +
	"&Param.1=" + fnGetCellValue("ConfProdTab", "MII_GUID") +   //GUID di MII
	"&Param.2=" + sMod                                          //Tipologia
	"&Param.3=" + sNote +                                       //note
	"&Param.4=" + sNewQuantity +                                //nuova quantità
	"&Param.5=" + sDatario +                                    //datario
	"&Param.6=" + sCestaTT +                                    //cestaTT
	"&Param.7=" + sOpeID +                                      //id operatore
	"&Param.8=" + iWtDur +                                      //durata in minuti
	"&Param.9=" + sNrPrint;                                     //numero stampo
	var ret = fnGetAjaxVal(qexe,["ResultCode","ResultDescription"],false);
	if(ret.ResultCode == -1){
	sAnswTitle = oLng_Monitor.getText("Monitor_SendError"); //"Errore d'invio"
	}
	else{
	if(sMod == "ST"){
	sAnswTitle = oLng_Monitor.getText("Monitor_AvertMade"); //"Storno effettuato correttamente"
	}
	else{
	if(sMod == "RT"){
	sAnswTitle = oLng_Monitor.getText("Monitor_RetMade"); //"Rettifica effettuata correttamente"
	}
	else{
	sAnswTitle = oLng_Monitor.getText("Monitor_ForwardMade"); //"Inoltro effettuato correttamente"
	}
	}
	}
	sap.ui.commons.MessageBox.show(
	ret.ResultDescription,
	(ret.ResultCode == -1)?sap.ui.commons.MessageBox.Icon.ERROR:sap.ui.commons.MessageBox.Icon.SUCCESS,
	sAnswTitle,
	[sap.ui.commons.MessageBox.Action.OK],
	sap.ui.commons.MessageBox.Action.OK
	);
	if(ret.ResultCode == 0 && sMod == "RT"){
	$.UIbyID("dlgRetConfProd").close();
	$.UIbyID("dlgRetConfProd").destroy();
	}
	refreshTabConfProd();
	}
	},
	sap.ui.commons.MessageBox.Action.YES
	);*/

	/* --- INVIO SENZA RICHIESTA DI CONFERMA --- */
	$.UIbyID("dlgRetConfProd").close();
	$.UIbyID("dlgRetConfProd").destroy();
	var sAnswTitle = ""; //codice imballo, da lasciare vuoto
	var qexe = dataProdSap + "To/renewConfProdToSapQR" +
		"&Param.1=" + fnGetCellValue("ConfProdTab", "MII_GUID") + //GUID di MII
		"&Param.2=" + sMod + //Tipologia
		"&Param.3=" + sNote + //note
		"&Param.4=" + sNewQuantity + //nuova quantità
		"&Param.5=" + sDatario + //datario
		"&Param.6=" + sCestaTT + //cestaTT
		"&Param.7=" + sOpeID + //id operatore
		"&Param.8=" + iWtDur + //durata in minuti
		"&Param.9=" + sNrPrint; //numero stampo

	var ret = fnGetAjaxVal(qexe, ["ResultCode", "ResultDescription"], false);
	if (ret.ResultCode < 0) {
		sAnswTitle = oLng_Monitor.getText("Monitor_SendError"); //"Errore d'invio"
	} else if (ret.ResultCode > 0) {
		sAnswTitle = oLng_Monitor.getText("Monitor_Error");
	} else {
		if (sMod == "ST") {
			sAnswTitle = oLng_Monitor.getText("Monitor_AvertMade"); //"Storno effettuato correttamente"			
		} else {
			if (sMod == "RT") {
				sAnswTitle = oLng_Monitor.getText("Monitor_RetMade"); //"Rettifica effettuata correttamente"
			} else {
				sAnswTitle = oLng_Monitor.getText("Monitor_ForwardMade"); //"Inoltro effettuato correttamente"
			}
		}
	}
	sap.ui.commons.MessageBox.show(
		ret.ResultDescription,
		(ret.ResultCode == 0) ? sap.ui.commons.MessageBox.Icon.SUCCESS : sap.ui.commons.MessageBox.Icon.ERROR,
		sAnswTitle, [sap.ui.commons.MessageBox.Action.OK],
		sap.ui.commons.MessageBox.Action.OK
	);
	/*
	if(ret.ResultCode == 0 && sMod == "RT"){
	$.UIbyID("dlgRetConfProd").close();
	$.UIbyID("dlgRetConfProd").destroy();
	}
	*/
	refreshTabConfProd();
}

function fnRemoveAvertRequest(sUDCNR) {

	sap.ui.commons.MessageBox.show(
		oLng_Monitor.getText("Monitor_AvertReqRemoving"), //"Rimuovere la richiesta di storno?",
		sap.ui.commons.MessageBox.Icon.WARNING,
		oLng_Monitor.getText("Monitor_Confirm"), //"Conferma",
[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
		function (sResult) {
			if (sResult == 'YES') {
				var qexe = dataProdMon +
					"UDC/removeAvertReqSQ" +
					"&Param.1=" + sUDCNR;
				var ret = fnSQLQuery(
					qexe,
					oLng_Monitor.getText("Monitor_AvertReqRemoved"), //"Richiesta di storno rimossa correttamente",
					oLng_Monitor.getText("Monitor_AvertReqRemovedError"), //"Errore in rimozione richiesta di storno",
					false
				);
				refreshTabConfProd();
			}
		},
		sap.ui.commons.MessageBox.Action.YES
	);
}

function setSelectedOperator(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("PERNR", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtOpeFiter").setValue(sOpeName).data("SelectedKey", sBadge);
	//refreshTabConfProd();
	$.UIbyID("dlgOperatorTableSelect").destroy();
	$.UIbyID("delFilter").setEnabled(true);
}

function setSelectedOpeModify(oControlEvent) {
	var oSelContext = oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("PERNR", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
	$.UIbyID("txtConfProdOPEID").setValue(sOpeName).data("SelectedKey", sBadge);
	$.UIbyID("dlgOperatorTableSelect").destroy();
}

function setSelectedIncludedTPopeSap(oControlEvent) {
	/* MULTIPLE SELECT */
	var oSelContext = oControlEvent.getParameter("selectedContexts");
	var sTpopeSap = "";
	var sTpopeSapDesc = "";
	for (var i = 0; i < oSelContext.length; i++) {
		sTpopeSap = sTpopeSap + "'" + oControlEvent.getSource().getModel().getProperty("TPOPESAP", oSelContext[i]) + "',";
		sTpopeSapDesc = sTpopeSapDesc + oControlEvent.getSource().getModel().getProperty("TPOPESAP", oSelContext[i]) + ", ";
	}

	if (oSelContext.length > 0) {
		sTpopeSap = sTpopeSap.substring(0, sTpopeSap.length - 1);
		sTpopeSapDesc = sTpopeSapDesc.substring(0, sTpopeSapDesc.length - 2);
	}

	$.UIbyID("txtIncludedTpopeSapFilter").setValue(sTpopeSapDesc).data("SelectedKey", sTpopeSap);;
	$.UIbyID("dlgTpopeSapTableSelect").destroy();
}

function setSelectedExeptedTPopeSap(oControlEvent) {
	/* MULTIPLE SELECT */
	var oSelContext = oControlEvent.getParameter("selectedContexts");
	var sTpopeSap = "";
	var sTpopeSapDesc = "";
	for (var i = 0; i < oSelContext.length; i++) {
		sTpopeSap = sTpopeSap + "'" + oControlEvent.getSource().getModel().getProperty("TPOPESAP", oSelContext[i]) + "',";
		sTpopeSapDesc = sTpopeSapDesc + oControlEvent.getSource().getModel().getProperty("TPOPESAP", oSelContext[i]) + ", ";
	}

	if (oSelContext.length > 0) {
		sTpopeSap = sTpopeSap.substring(0, sTpopeSap.length - 1);
		sTpopeSapDesc = sTpopeSapDesc.substring(0, sTpopeSapDesc.length - 2);
	}

	$.UIbyID("txtExeptedTpopeSapFilter").setValue(sTpopeSapDesc).data("SelectedKey", sTpopeSap);;
	$.UIbyID("dlgTpopeSapTableSelect").destroy();
}

function modificaDichiarazione(oper) {
	var idLine = fnGetCellValue("ConfProdTab", "IDLINE");
	var dateShift = fnGetCellValue("ConfProdTab", "DATE_SHIFT");
	var shift = fnGetCellValue("ConfProdTab", "SHIFT");
	var opeId = fnGetCellValue("ConfProdTab", "OPEID");
	var order = fnGetCellValue("ConfProdTab", "ORDER");
	var pOper = fnGetCellValue("ConfProdTab", "POPER");
	var typeConf = fnGetCellValue("ConfProdTab", "TYPECONF");
	var sReasId = fnGetCellValue("ConfProdTab", "SREASID");
	var udcNr = fnGetCellValue("ConfProdTab", "UDCNR");
	var timeId = fnGetCellValue("ConfProdTab", "TIME_ID");
	var ret = "";
	switch (oper) {
		case 0:
			sap.ui.commons.MessageBox.show(
				oLng_Monitor.getText("Monitor_MsgConfermaDic"),
				sap.ui.commons.MessageBox.Icon.WARNING,
				oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
				function (sResult) {
					if (sResult == 'YES') {
						var qexe = dataProdMon +
							"ConfProd/setFlagConfirmedSQ" +
							"&Param.1=" + idLine +
							"&Param.2=" + dateShift +
							"&Param.3=" + shift +
							"&Param.4=" + opeId +
							"&Param.5=" + order +
							"&Param.6=" + pOper +
							"&Param.7=" + typeConf +
							"&Param.8=" + sReasId +
							"&Param.9=" + udcNr +
							"&Param.10=" + timeId;
						ret = fnExeQuery(qexe, "", oLng_Monitor.getText("Monitor_Error"), false);
						refreshTabConfProd();
					}
				},
				sap.ui.commons.MessageBox.Action.YES
			);
			break;
		case 1:
			sap.ui.commons.MessageBox.show(
				oLng_Monitor.getText("Monitor_MsgErroreDic"),
				sap.ui.commons.MessageBox.Icon.WARNING,
				oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
				function (sResult) {
					if (sResult == 'YES') {
						var qexe = dataProdMon +
							"ConfProd/setFlagError_HsapConfLogDetQR" +
							"&Param.1=" + idLine +
							"&Param.2=" + dateShift +
							"&Param.3=" + shift +
							"&Param.4=" + opeId +
							"&Param.5=" + order +
							"&Param.6=" + pOper +
							"&Param.7=" + typeConf +
							"&Param.8=" + sReasId +
							"&Param.9=" + udcNr +
							"&Param.10=" + timeId;
						ret = fnGetAjaxVal(qexe, ["code", "message"], false);
						if (ret.code != 0) sap.ui.commons.MessageBox.show(oLng_Monitor.getText("Monitor_Error"), sap.ui.commons.MessageBox.Icon.ERROR);
						refreshTabConfProd();
					}
				},
				sap.ui.commons.MessageBox.Action.YES
			);
			break;
		case 2:
			sap.ui.commons.MessageBox.show(
				oLng_Monitor.getText("Monitor_MsgRetryDic"),
				sap.ui.commons.MessageBox.Icon.WARNING,
				oLng_Monitor.getText("Monitor_Warning"), [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
				function (sResult) {
					if (sResult == 'YES') {
						var qexe = dataProdSap + "To/JobRetrayErrorRFCQR";
						ret = fnExeQuery(qexe, "", oLng_Monitor.getText("Monitor_Error"), true);
					}
				},
				sap.ui.commons.MessageBox.Action.YES
			);
			break;
	}
}