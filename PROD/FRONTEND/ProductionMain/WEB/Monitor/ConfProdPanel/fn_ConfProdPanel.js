/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Avanzamenti produzione
//Author: Bruno Rosati
//Date:   14/09/2017
//Vers:   1.0
//**************************************************************************************
// Script per pagina Avanzamenti produzione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

var startDate = "2017-08-01";
var currentDate = new Date();

// Function che crea la tabella per gli avanzamenti produzione e ritorna l'oggetto oTable
function createTabConfProdPanel(sOrigTableName, bAvertButton) {

	if(bAvertButton === undefined){
        bAvertButton = true;
    }
    
    var oTable = UI5Utils.init_UI5_Table({
		id: "ConfProdPanelTab",
		exportButton: true,
		properties: {
			//title: oLng_Monitor.getText("Monitor_ConfProdList"), //"Lista avanzamenti di produzione",
			visibleRowCount: 7,
			fixedColumnCount: 3,
			width: "100%",//"1285px",//
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.None,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){},
			toolbar: new sap.ui.commons.Toolbar({
				items: [
                    new sap.ui.commons.Button({
						text: oLng_Monitor.getText("Monitor_ConfProdUndoUDC"), //"Storna UdC"
						//id:   "btnDelConfProdUDC",
						icon: 'sap-icon://reset',
						enabled: bAvertButton,
                        visible: bAvertButton,
						press: function (){
							fnAvertConfProdUDC(sOrigTableName);
						}
					})
                ],
				rightItems: [
					new sap.ui.commons.Button({
						icon: "sap-icon://refresh",
						press: function () {
							refreshTabConfProdPanel(sOrigTableName);
						}
					}),
					new sap.ui.commons.Button({
						icon: "sap-icon://excel-attachment",
						press: function () {
							exportTableToCSV("ConfProdTab","Produzione");
						}
					})
				]
			})
		},
		exportButton: false,
		columns: [
			{
				Field: "statusIcon",
				label: oLng_Monitor.getText("Monitor_ICON"), //"Stato",
				tooltipCol: "{statusToolTip}",
				iconColor: "{statusColor}",
				properties: {
					width: "70px",
					height: "30px",
					autoResizable: false,
				},
				template: {
					type: "coreIcon",
					textAlign: "Center"
				}
			},
			{
				Field: "EXTENDED_TEXT",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
                properties: {
					width: "90px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DELREQ",
				label: oLng_Monitor.getText("Monitor_AvertRequest"), //"Rich. storno",
				tooltip: oLng_Monitor.getText("Monitor_AvertRequestExtended"), //"Richiesta di Storno operatore",
				properties: {
					width: "110px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
			},
			{
				Field: "TYPECONF",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				properties: {
					width: "90px",
					visible: false,
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "OPEID",
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "80px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "OPENAME",
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"nome Operatore",
				properties: {
					width: "130px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "DATE_SHIFT",
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "Date",
					textAlign: "Center"
				}
			},
			{
				Field: "SHIFT",
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TIME_ID",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "130px",
					visible: false,
					//resizable : false,
					flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "TIME_ID_CALCULATED",
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "90px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "Time",
					textAlign: "Center"
				}
			},
			{
				Field: "PLANT",
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "70px",
					visible: false,
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "IDLINE",
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
					resizable : true,
					flexible : false
				}
			},
			{
				Field: "LINETXT",
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "140px",
					//resizable : false,
					flexible : false
				}
			},
            {
				Field: "MII_GUID",
				label: oLng_Monitor.getText("Monitor_MII_GUID"), //"GUID di MII",
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false,
					//visible: false
				}
			},
			{
				Field: "MII_GUID_ST",
				label: oLng_Monitor.getText("Monitor_Avert_MII_GUID"), //"GUID stornato",
				properties: {
					width: "170px",
					//resizable : false,
					flexible : false,
					//visible: false
				}
			},
			{
				Field: "CURRENT_QTY",
                label: oLng_Monitor.getText("Monitor_UDCQuantity"), //"Q.tà UdC",				
				properties: {
					width: "80px",
					//resizable : false,
					flexible : false,
					visible: true
				}
			},
			{
				Field: "SREASID",
				properties: {
					width: "70px",
					visible: false
					//resizable : false,
					//flexible : false
				}
			},
			{
				Field: "SREASTXT",
				label: oLng_Monitor.getText("Monitor_ScrabReason"), //"Causale di scarto",
				properties: {
					width: "140px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "QTY",
				label: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
				properties: {
					width: "80px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "QTY_PRO",
				label: oLng_Monitor.getText("Monitor_QTYPROD"), //"Dic.Prod",
				properties: {
					width: "75px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "DATARIO",
				label: oLng_Monitor.getText("Monitor_Datary"), //"Datario",
				properties: {
					width: "80px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "WTDUR",
				label: oLng_Monitor.getText("Monitor_Minutes"), //"Minuti",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
				}
			},
			{
				Field: "CESTA_TT",
				label: oLng_Monitor.getText("Monitor_CESTA_TT"), //"Cesta TT",
				properties: {
					width: "110px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "NRPRNT",
				label: oLng_Monitor.getText("Monitor_PrintNR"), //"nr. Stampo",
				properties: {
					width: "90px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "MATERIAL",
				label: oLng_Monitor.getText("Monitor_Material"), //"Materiale",
				properties: {
					width: "110px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "MATERIAL_DESC",
				label: oLng_Monitor.getText("Monitor_MaterialDesc"), //"Descrizione Materiale",
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TIME_ID",
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data inserimento",
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false,
					visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "DATE_UPD",
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Data aggiornamento",
				properties: {
					width: "150px",
					//resizable : false,
					flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "CONFIRMED",
				label: oLng_Monitor.getText("Monitor_Confirmed"), //"Confermato",
				properties: {
					width: "100px",
					//resizable : false,
					flexible : false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
			},
			{
				Field: "DELETED",
				label: oLng_Monitor.getText("Monitor_Deleted"), //"Cancellato",
				properties: {
					width: "90px",
					//resizable : false,
					flexible : false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
			},
			{
				Field: "USERMOD",
				label: oLng_Monitor.getText("Monitor_User"), //"Utente",
				properties: {
					width: "75px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "TYPEMOD",
				label: oLng_Monitor.getText("Monitor_TypeMod"), //"Tipo Modifica",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "EXTENDED_TYPEMOD",
				label: oLng_Monitor.getText("Monitor_TypeMod"), //"Tipo Modifica",
				properties: {
					width: "120px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "UMDUR",
				//label: oLng_Monitor.getText("Monitor_EXID"), //"External ID",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "ERROR",
				label: oLng_Monitor.getText("Monitor_Error"), //"Errore",
				properties: {
					width: "90px",
					//resizable : false,
					flexible : false,
					visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
				}
			},
			{
				Field: "SAP_GUID",
				label: oLng_Monitor.getText("Monitor_SAP_GUID"), //"SAP GUID",
				properties: {
					width: "280px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "UDC_STATE",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "PZUDC",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "EXID",
				properties: {
					width: "70px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "NOTE",
				label: oLng_Monitor.getText("Monitor_Notes"), //"Note",
				properties: {
					width: "200px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "NOTE_DUDCMAST",
				label: oLng_Monitor.getText("Monitor_UDCNotes"), //"Note UdC",
				properties: {
					width: "200px",
					//resizable : false,
					flexible : false,
					visible: false
				}
			},
			{
				Field: "ORIGIN", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    visible: false,
                    flexible : false
				}
            },
            {
				Field: "EXTENDED_ORIGIN", 
				label: oLng_Monitor.getText("Monitor_UDCtype"), //"Tipo UDC",
				properties: {
					width: "90px",
                    flexible : false,
                    visible: false
				}
            },
			{
				Field: "ORDER",
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "POPER",
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
					//resizable : false,
					flexible : false
				}
			},
			{
				Field: "UDCNR",
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
				properties: {
					width: "140px",
					//resizable : false,
					flexible : false
				}
			}
		]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabConfProdPanel(sOrigTableName);
	return oTable;
}

// Aggiorna la tabella di avanzamenti produzione
function refreshTabConfProdPanel(sOrigTableName) {

	var sDateTo = dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));

	var qParams = {
		data: "ProductionMain/Monitor/ConfProd/getConfProdByParamsQR" +
		"&Param.1=" + fnGetCellValue(sOrigTableName, "PLANT") +
		"&Param.2=" + "" +
		"&Param.3=" + "" +
		"&Param.4=" + "" +
		"&Param.5=" + startDate +
		"&Param.6=" + currentDate.getFullYear().toString() + "-" + (currentDate.getMonth() + 1).toString() + "-" +
                      currentDate.getDate().toString() + 
		"&Param.7=" + "" +
		"&Param.8=" + "2" +
		"&Param.9=" + "" +
		"&Param.10=" + fnGetCellValue(sOrigTableName, "UDCNR") +
		"&Param.11=" + "" +
		"&Param.12=" + "" +
		"&Param.13=" + "" +
		"&Param.14=" + "" +
		"&Param.15=" + sLanguage,
		dataType: "xml"
	};

	$.UIbyID("ConfProdPanelTab").setBusy(true);
	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		$.UIbyID("ConfProdPanelTab").setBusy(true);
		// carica il model della tabella
		$.UIbyID("ConfProdPanelTab").getModel().setData(data);
	}) // End Done Function
	// on fail
		.fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_Monitor.getText("Monitor_DataUpdateError")); //Errore nell'aggiornamento dati
    })
	// always, either on success or fail
		.always(function () {

		// remove busy indicator
		$.UIbyID("ConfProdPanelTab").setBusy(false);
	});
	$.UIbyID("ConfProdPanelTab").setSelectedIndex(-1);
	$("#splash-screen").hide();
}

// Apre la finestra per effettuare lo storno di blocco
function fnAvertConfProdUDC(sOrigTableName){
    //Attenzione: vengono considerate anche le colonne, perciò nei cicli bisogna sottrarre 1
    var rowsetObject = $.UIbyID("ConfProdPanelTab").getModel().getObject("/Rowset/0");
    
    var movQty = 0;
    var sTxtNote = "";
    
    for(var i = 0; i < rowsetObject.childNodes.length - 1; i++){
        if(fnGetModelVal($.UIbyID("ConfProdPanelTab").getModel(), "ERROR", i) == 1){
            sap.ui.commons.MessageBox.show(
                oLng_Monitor.getText("Monitor_CheckErrorBeforeAvert"), //"Verificare gli errori prima di stornare"
                sap.ui.commons.MessageBox.Icon.ERROR,
                oLng_Monitor.getText("Monitor_Error"), //"Errore"
                [sap.ui.commons.MessageBox.Action.OK],
                sap.ui.commons.MessageBox.Action.OK
            );
            return;
        }
        
        //calcolo la quantità solo se non è stornato 
        if(parseInt(fnGetModelVal($.UIbyID("ConfProdPanelTab").getModel(), "DELETED", i)) != 1){
            movQty = movQty + parseInt(fnGetModelVal($.UIbyID("ConfProdPanelTab").getModel(), "QTY", i));
        }
    }
    
    console.log(movQty);
    if(movQty > parseInt(fnGetCellValue(sOrigTableName, "CURRENT_QTY"))){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_CheckQtyMovements"), //"La somma delle quantità di ogni conferma è maggiore della quantià attualmente contenuta nell'UdC"
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_Monitor.getText("Monitor_Error"), //"Errore"
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        return;
    }
    
    //  Crea la finestra di dialogo
	var oRetConfProdUDCdlg = new sap.ui.commons.Dialog({
		modal:  true,
		resizable: true,
		id: "dlgRetConfProdUDC",
		maxWidth: "600px",
		maxHeight: "600px",
		//minHeight: "900px",
		title: oLng_Monitor.getText("Monitor_ConfProdUndoUDC"), //"Storna UdC"
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
							layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: fnGetCellValue("ConfProdTab", "UDCNR"),
								editable: false,
								maxLength: 20,
								id: "txtConfProdUDCNR"
							})
						]
					}),
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Notes"), //"Note",
							layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: "",
								editable: true,
								maxLength: 200,
								id: "txtGlobalNotes"
							})
						]
					})
				]
			})
		]
	});
    
    oRetConfProdUDCdlg.addContent(oForm1);

	oRetConfProdUDCdlg.addButton(new sap.ui.commons.Button({
        id: "btnAvertDialog",
		text: oLng_Monitor.getText("Monitor_ConfProdUndoUDC"), //"Storna UdC"
		press:function(){
            if($.UIbyID("txtGlobalNotes").getValue() == "" || $.UIbyID("txtGlobalNotes").getValue() == null){
                sap.ui.commons.MessageBox.show(
                    oLng_Monitor.getText("Monitor_InsertNotes"), //Inserire una nota
                    sap.ui.commons.MessageBox.Icon.ERROR,
                    oLng_Monitor.getText("Monitor_MandatoryField"), //Campo obbligatorio
                    [sap.ui.commons.MessageBox.Action.OK],
                    sap.ui.commons.MessageBox.Action.OK
                );
                return;
            }
            sTxtNote = $.UIbyID("txtGlobalNotes").getValue();
            $.UIbyID("btnAvertDialog").setEnabled(false);
            $.UIbyID("btnCloseDialog").setEnabled(false);
            oRetConfProdUDCdlg.close();
			oRetConfProdUDCdlg.destroy();
            fnAvertUDC(sOrigTableName, sTxtNote);          
        }
	}));

	oRetConfProdUDCdlg.addButton(new sap.ui.commons.Button({
        id: "btnCloseDialog",
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press:function(){
			oRetConfProdUDCdlg.close();
			oRetConfProdUDCdlg.destroy();
		}
	}));
	oRetConfProdUDCdlg.open();
}

function fnAvertUDC(sOrigTableName, sNote){
    
    var qexe = dataProdSap + "To/avertConfProdUDCtoSapQR" +
			"&Param.1=" + fnGetCellValue(sOrigTableName, "UDCNR") +   //nr. UdC
			"&Param.2=" + sNote  +                                    //note
            "&Param.3=" + fnGetCellValue(sOrigTableName, "PLANT") +   //divisione
			"&Param.4=" + startDate  +                                //data inizio
            "&Param.5=" + currentDate.getFullYear().toString() + "-" + (currentDate.getMonth() + 1).toString() + "-" +
                          currentDate.getDate().toString();

	$.UIbyID("ConfProdPanelTab").setBusy(true);
    var ret = fnGetAjaxVal(qexe,["code","message","avertedOkNR","avertedKoNR"],false);
    $.UIbyID("ConfProdPanelTab").setBusy(false);
    console.log(ret.code);
    
    if(ret.code == 0){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_AvertUDCsuccesful") + "\n" + //"Storni dell'UdC eseguiti correttamente"
            oLng_Monitor.getText("Monitor_AvertSuccesfulNr") + " " + //"Numero storni eseguiti correttamente:"
            ret.avertedOkNR,
            sap.ui.commons.MessageBox.Icon.SUCCESS,
            oLng_Monitor.getText("Monitor_ConfProdUndoUDC"), //"Storna UdC"
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        refreshTabConfProdPanel(sOrigTableName);
        return;
    }
    
    if(ret.code == 1){
        sap.ui.commons.MessageBox.show(
            oLng_Monitor.getText("Monitor_AvertSuccesfulNr") + " " + //"Numero storni eseguiti correttamente:"
            ret.avertedOkNR + "\n" +
            oLng_Monitor.getText("Monitor_AvertKoNr") + " " + //"Numero storni non andati a buon fine:"
            ret.avertedKoNR + "\n" + ret.message,
            sap.ui.commons.MessageBox.Icon.WARNING,
            oLng_Monitor.getText("Monitor_Warning"), //"Attenzione"
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        refreshTabConfProdPanel(sOrigTableName);
        return;
    }
    
    if(ret.code == -1){
        sap.ui.commons.MessageBox.show(
            ret.message,
            sap.ui.commons.MessageBox.Icon.ERROR,
            oLng_Monitor.getText("Monitor_Error"), //"Errore"
            [sap.ui.commons.MessageBox.Action.OK],
            sap.ui.commons.MessageBox.Action.OK
        );
        refreshTabConfProdPanel(sOrigTableName);
        return;
    }    
}