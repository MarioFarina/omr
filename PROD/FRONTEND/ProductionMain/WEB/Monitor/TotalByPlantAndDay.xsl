<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
<xsl:output method="xml" media-type="text/xml" encoding="UTF-8"/>
	<!-- Identità -->
	<xsl:template match="@* | node()">
	  <xsl:copy>
		 <xsl:apply-templates select="@* | node()"/>
	  </xsl:copy>
	</xsl:template>
	
	<xsl:key name="group-to-row" match="/Rowsets/Rowset/Row" use="group"/>
	
	<xsl:template match="/Rowsets/Rowset">
		<Rowset>
			<xsl:apply-templates select="/Rowsets/Rowset/Columns" />
			
			<xsl:for-each select="/Rowsets/Rowset/Row">
				<xsl:copy>
					<xsl:apply-templates select="@* | node()"/>
					<DrilldownDepth>1</DrilldownDepth>
				</xsl:copy>
			</xsl:for-each>
			
			<Row>
				<REPTXT>TOTALE</REPTXT>
				<IDLINE></IDLINE>
				<DATE_SHIFT></DATE_SHIFT>
			          <SHIFT></SHIFT>
				<QTY_IN><xsl:value-of select="sum(/Rowsets/Rowset/Row/QTY_IN)"/></QTY_IN>
				<QTY_OUT><xsl:value-of select="sum(/Rowsets/Rowset/Row/QTY_OUT)"/></QTY_OUT>
				<MATID></MATID>
				<MAKTX></MAKTX>
				<LOTID></LOTID>
				<QTY_SCRAP><xsl:value-of select="sum(/Rowsets/Rowset/Row/QTY_SCRAP)"/></QTY_SCRAP>
				<DrilldownDepth>0</DrilldownDepth>
			</Row>

		</Rowset>
		
    </xsl:template>
	
	<!-- Aggiunge un nuovo nodo Column per il campo level, aggiunto ad ogni Row -->
	<xsl:template match="/Rowsets/Rowset/Columns">
		<xsl:copy>
			<xsl:copy-of select="@* | node()" />
			<Column Description="DrilldownDepth" MaxRange="0" MinRange="0" Name="DrilldownDepth" SQLDataType="8" SourceColumn="DrilldownDepth"/>
		</xsl:copy>
	</xsl:template>
   
</xsl:stylesheet>