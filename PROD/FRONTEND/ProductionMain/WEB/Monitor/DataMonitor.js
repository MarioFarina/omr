/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/

//Interfaccia MII per report dati caricati - Monitor
//**********************************************************************************************************************
// Ver: 0.2 - Author: Luca Adanti
//**********************************************************************************************************************


/* Variabili della pagina */
var sc = sap.ui.commons;
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MonitorData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
	cache: false
});


Libraries.load(
	[
		"/XMII/CM/Common/MII_core/UI5_utils",
		/*"/XMII/CM/ProductionMain/MasterData/Grstreas_fn/fn_grstreas"*/
	],
	function () {
		$(document).ready(function () {
			/***********************************************************************************************/
			// Inizializzazione pagina
			/***********************************************************************************************/

			$("#splash-screen").hide();


			//Escape per gestione parametro plant da GET/POST

			if ($('#plant').val() === '{plant}') {
				$('#plant').val("2101");
			}



			var oTable1 = createTabMonitor();

			var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					oTable1
				],
				width: "100%"
			});

			oVerticalLayout.placeAt("MasterCont");
		});
	}
);








// Functione che crea la tabella Grstrease e ritorna l'oggetto oTable
function createTabMonitor() {

	// creo il filtro per divisioni
	var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
		selectedKey: $('#plant').val(),
		tooltip: oLng_MonitorData.getText("Monitor_FilterDivision"), //"Filtro Divisioni",
		change: function (oEvent) {
			/*
					$.UIbyID("ShiftTab").filter(oPlantFShift,$("#cmbPlant-input").val());
					*/
		}
	});

	var oItemPlant = new sap.ui.core.ListItem();
	oItemPlant.bindProperty("text", "NAME1");
	oItemPlant.bindProperty("key", "PLANT");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);

	var oPlantsModel = new sap.ui.model.xml.XMLModel();
	oPlantsModel.loadData(QService + dataProd + "MasterData/Plants/getPlantsSQ");
	oCmbPlant.setModel(oPlantsModel);
	oCmbPlant.setSelectedKey($('#plant').val());

	var lblPlant = new sc.Label({ text:  oLng_MonitorData.getText("Monitor_FilterDivision")});

	//Crea L'oggetto Tabella Grstrease
	var oTable = UI5Utils.init_UI5_Table({
		id: "MonitorTab",
		properties: {
			title: oLng_MonitorData.getText("Monitor_TabTitle"), //"Lista causali fermo",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
			rowSelectionChange: function(oControlEvent){

			}
		},
		exportButton: true,
		toolbarItems:[lblPlant,oCmbPlant,
									new sap.ui.commons.Button({
										text: oLng_MonitorData.getText("Monitor_Refresh"), //"Aggiorna",
										icon: 'sap-icon://refresh',
										enabled: true,
										press: function (){
											refreshTabMonitor();
										}
									})],
		columns: [
			{
				Field: "REPTXT",
				label: oLng_MonitorData.getText("Monitor_REPTXT"), //"Reparto",
				properties: {
					width: "80px",
					autoResizable: false,
				}
			},
			{
				Field: "IDLINE",
				label: oLng_MonitorData.getText("Monitor_IDLINE"), //"STTYPE",
				properties: {
					width: "50px",
					autoResizable: false,
				}
			},
			{
				Field: "LINETXT",
				label: oLng_MonitorData.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "120px",
					autoResizable: false,
				}
			},

			{
				Field: "MATID",
				label: oLng_MonitorData.getText("Monitor_Material"), //"materiale",
				properties: {
					width: "100px",
					autoResizable: false,
				}
			},
			{
				Field: "MAKTX",
				label: oLng_MonitorData.getText("Monitor_MAKTX"), //"desc.materiale",
				properties: {
					width: "150px",
					autoResizable: false,
				}
			},/*
			{
				Field: "LOTID",
				label: oLng_MonitorData.getText("Monitor_Order"), //"commessa",
				properties: {
					width: "110px",
					autoResizable: false,
				}
			},*/
			{
				Field: "STATE_ICON",
				label: oLng_MonitorData.getText("Monitor_ICON"), //"Stato",
				tooltipCol: "STATE_LABEL",
				colorCol: "STATE_COLOR",
				properties: {
					width: "30px",
					height: "30px",
					autoResizable: false,
				},
				template: {
					type: "colorIcon",

				}
			},
			{
				Field: "CTIME",
				label: oLng_MonitorData.getText("Monitor_CTIME"), //"tempo ciclo",
				properties: {
					width: "80px",
					type: "Number",
					autoResizable: false,
				}
			},
			{
				Field: "STIME",
				label: oLng_MonitorData.getText("Monitor_STIME"), //"tempo standard",
				properties: {
					width: "80px",
					type: "Number",
					autoResizable: false,
				}
			},
			{
				Field: "PTIME",
				label: oLng_MonitorData.getText("Monitor_PTIME"), //"tempo produzione",
				properties: {
					width: "80px",
					type: "Number",
					autoResizable: false,
				}
			},
			{
				Field: "ATIME",
				label: oLng_MonitorData.getText("Monitor_ATIME"), //"tempo avanzamento",
				properties: {
					width: "80px",
					type: "Number",
					autoResizable: false,
				}
			},
			{
				Field: "QTY_TARG",
				label: oLng_MonitorData.getText("Monitor_QTYTARG"), //"Quantità dichiarata prod",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_PROD",
				label: oLng_MonitorData.getText("Monitor_QTYPROD"), //"Quantità prod",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_IN",
				label: oLng_MonitorData.getText("Monitor_QTYIN"), //"Quantità ingresso",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_OUT",
				label: oLng_MonitorData.getText("Monitor_QTYOUT"), //"Quantità uscita",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_WIP",
				label: oLng_MonitorData.getText("Monitor_QTYWIP"), //"Quantità wip",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_SCRAP",
				label: oLng_MonitorData.getText("Monitor_QTYSCRAP"), //"Quantità sospesi",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_PROG",
				label: oLng_MonitorData.getText("Monitor_QTY_PROG"), //"Quantità avanzamento",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_IND",
				label: oLng_MonitorData.getText("Monitor_QTY_IND"), //"Quantità disponibilità mancata",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_SLO",
				label: oLng_MonitorData.getText("Monitor_QTY_SLO"), //"Quantità rallentamento",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "T_DUR",
				label: oLng_MonitorData.getText("Monitor_STOPDUR"), //"tempo di fermo",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "T_DISP",
				label: oLng_MonitorData.getText("Monitor_DISP"), //"tempo disponibile",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "KPQ_QUA",
				label: oLng_MonitorData.getText("Monitor_QUA"), //"Ind. Qualità",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "KPP_PER",
				label: oLng_MonitorData.getText("Monitor_PER"), //"Ind. Qualità",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "KP_OEE",
				label: oLng_MonitorData.getText("Monitor_OEE"), //"Ind. OEE",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "DATE_UPD",
				label: oLng_MonitorData.getText("Monitor_DATEUPD"), //"Ultimo aggiornamento",
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "LAST_CHECK",
				label: oLng_MonitorData.getText("Monitor_LASTCHECK"), //"Ultimo controllo",
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Center"
				}
			},
			{
				Field: "T_DUR_MIN",
				label: oLng_MonitorData.getText("Monitor_T_DUR_MIN"), // Durata in minuti
				properties: {
					width: "50px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Left"
				}
			},
			{
				Field: "QTY_TARG_PZ",
				label: oLng_MonitorData.getText("Monitor_QTY_TARG_PZ"), // Qtà target Pz
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Left"
				}
			},
			{
				Field: "OEE_Color",
				label: oLng_MonitorData.getText("Monitor_OEE_Color"), // OEE Color
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Left"
				}
			},
			{
				Field: "OEE_BackColor",
				label: oLng_MonitorData.getText("Monitor_OEE_BackColor"), //OEE BackColor
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					//type: "DateTime",
					textAlign: "Left"
				}
			}
		]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabMonitor();
	return oTable;
}

// Aggiorna la tabella Grstreas
function refreshTabMonitor() {
	//$.UIbyID("MonitorTab").getModel().loadData(QService + dataMonitor + "getMonitorPlantQR" );
	$("#splash-screen").hide();
	//numero righe
	Params = "&Param.1=" + $.UIbyID("filtPlant").getSelectedKey()  + "&RowCount=200";

	var qParamsTab = {
		data:   "ProductionMain/DashBoard/getDashBoardQR" + Params,
		dataType: "xml"
	};

	$.UIbyID("MonitorTab").setBusy(true);
	UI5Utils.getDataModel(qParamsTab)
	// on success
		.done(function (data) {
		// carica il model della tabella
		$.UIbyID("MonitorTab").setBusy(true);

		$.UIbyID("MonitorTab").getModel().setData(data);
		//$.UIbyID("MonitorTab").getModel().refresh(true);
	}) // End Done Function
	// on fail
		.fail(function () {
		sap.ui.commons.MessageBox.alert(oLng_MonitorData.getText("Monitor_LoadERR",['val1','val2']));
	})
	// always, either on success or fail
		.always(function () {
		// remove busy indicator
		$.UIbyID("MonitorTab").setBusy(false);
	});
}








