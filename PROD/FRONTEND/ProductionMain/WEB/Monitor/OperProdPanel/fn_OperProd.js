/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/
//Impostazioni per gestione errori

//**************************************************************************************
//Title:  Monitor Conferme Produzione
//Author: Bruno Rosati
//Date:   22/06/2017
//Vers:   1.1
//**************************************************************************************
// Script per pagina Monitor Conferme Produzione

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
var iCharPos = sCurrentLocale.indexOf("-");
var sLanguage = (iCharPos === -1 ? sCurrentLocale : sCurrentLocale.substring(0, iCharPos)).toUpperCase();

/* format per data e ora */
var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "yyyy-MM-dd"
});
var tmFormat = sap.ui.core.format.DateFormat.getDateInstance({
    pattern: "HH:mm"
});

// setta le risorse per la lingua locale
var oLng_Monitor = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/" + /* "Monitor/" + */ "res/monitor.i18n.properties",
	locale: sCurrentLocale
});

// Functione che crea la tabella e ritorna l'oggetto oTable
function createTabOpeProd() {
    
    var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom', {
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
        change: function (oEvent){
			refreshTabOpeProd();
		}
    });
    oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerF.setLocale("it");

    var oDatePickerT = new sap.ui.commons.DatePicker('dtTo', {
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
        change: function (oEvent){
			refreshTabOpeProd();
		}
    });
    oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
    oDatePickerT.setLocale("it");
    
    //filtro Turno
    var oCmbShiftFilter = new sap.ui.commons.ComboBox("filtShift", {
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabOpeProd();
        }
    });            
    var oItemShift = new sap.ui.core.ListItem();
    oItemShift.bindProperty("key", "SHIFT");
    oItemShift.bindProperty("text", "SHNAME");
    oCmbShiftFilter.bindItems("/Rowset/Row", oItemShift);
    var oShiftFilter = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oShiftFilter.loadData(QService + dataProdMD + "Shifts/getShiftsByPlantSQ&Param.1=" + $.UIbyID("filtPlant").getSelectedKey());
    }    
    oCmbShiftFilter.setModel(oShiftFilter);
    
    //filtro Linea
    var oCmbLineFilter = new sap.ui.commons.ComboBox("filtLine", {
        layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabOpeProd();
        }
    });            
    var oItemLine = new sap.ui.core.ListItem();
    oItemLine.bindProperty("key", "IDLINE");
    oItemLine.bindProperty("text", "LINETXT");
    oCmbLineFilter.bindItems("/Rowset/Row", oItemLine);
    var oLineFilter = new sap.ui.model.xml.XMLModel();
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1=" + $('#plant').val());
    }
    else{
        oLineFilter.loadData(QService + dataProdMD + "Lines/getLinesbyPlantSQ&Param.1" + $.UIbyID("filtPlant").getSelectedKey());
    }    
    oCmbLineFilter.setModel(oLineFilter);
    
    //Filtro Buoni/Sospesi
/*
    var oCmbTypeConfFilter = new sap.ui.commons.ComboBox("filtTypeConf", {
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabOpeProd();
        }
    });
    var oItemTypeConf = new sap.ui.core.ListItem("goodItem").setKey("G").setText(oLng_Monitor.getText("Monitor_Good"));
    oCmbTypeConfFilter.addItem(oItemTypeConf);
    oItemTypeConf = new sap.ui.core.ListItem("suspendedItem").setKey("S").setText(oLng_Monitor.getText("Monitor_Suspended"));
    oCmbTypeConfFilter.addItem(oItemTypeConf);
*/
    var oModelTypeConf = new sap.ui.model.xml.XMLModel();
	oModelTypeConf.loadData(QService + dataConfProd + "getMtypeconfSQ&Param.1=" + sLanguage);

	// Crea la ComboBox per le divisioni in input
	var oCmbTypeConfFilter = new sap.ui.commons.ComboBox("filtTypeConf", {
		layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
		change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            refreshTabOpeProd();
        }
	});
	oCmbTypeConfFilter.setModel(oModelTypeConf);		
	var oItemTypeConf = new sap.ui.core.ListItem();
	oItemTypeConf.bindProperty("text", "EXTENDED_TEXT");
	oItemTypeConf.bindProperty("key", "MII_TYPECONF");	
	oCmbTypeConfFilter.bindItems("/Rowset/Row", oItemTypeConf);
    
    /*
            --- La scelta della DropdownBox per la selezione del filtro operatore è stata abbandonata perché è un oggetto deprecato e
            --- non funzionano correttamente i metodi .getSelectedKey() e .setSelectedKey()
    var oModelOperator = new sap.ui.model.xml.XMLModel();
	oModelOperator.loadData(
        QService + dataProdMD +"Operators/getOperatorsBadgeNameSQ&Param.1=" + 
        (($.UIbyID("filtPlant").getSelectedKey() === "")?$('#plant').val():$.UIbyID("filtPlant").getSelectedKey())
    );
    oModelOperator.setSizeLimit(1000);
    var oDrpDownOpeFilter = new sap.ui.commons.DropdownBox("filtOperator", {
        searchHelpEnabled : true,
        selectedKey: "",
        searchHelp: function () {
            fnGetOperatorTableDialog(
                ($.UIbyID("filtPlant").getSelectedKey() === "")?$('#plant').val():$.UIbyID("filtPlant").getSelectedKey(),
                "",
                setSelectedOperator
            );
        },
        change: function (oEvent) {
            $.UIbyID("delFilter").setEnabled(true);
            console.log($.UIbyID("filtOperator").getSelectedKey());
            console.log($.UIbyID("filtOperator").getValue());
            refreshTabOpeProd();
        }
    });
    oDrpDownOpeFilter.setModel(oModelOperator);
    var oItemOpe = new sap.ui.core.ListItem();
	oItemOpe.bindProperty("text", "OPENAME");
	oItemOpe.bindProperty("key", "PERNR");	
	oDrpDownOpeFilter.bindItems("/Rowset/Row", oItemOpe);
    */

    //Crea L'oggetto Tabella Operatore
    var oTable = UI5Utils.init_UI5_Table({
        id: "OpeProdTab",
        properties: {
            title: oLng_Monitor.getText("Monitor_OperProdList"), //"Lista avanzamenti operatore",
            visibleRowCount: 15,
            fixedColumnCount: 2,
            width: "100%",//"1285px",//
            firstVisibleRow: 0,     
            selectionMode: sap.ui.table.SelectionMode.Multi,
            navigationMode: sap.ui.table.NavigationMode.Scrollbar ,
            visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive ,
            enableColumnReordering: true,
			rowSelectionChange: function(oControlEvent){
                var aSelectedCount = $.UIbyID("OpeProdTab").getSelectedIndices();
                if (aSelectedCount.length > 0) {
                    $.UIbyID("btnConfirm").setEnabled(true);
                }
                else {
                    $.UIbyID("btnConfirm").setEnabled(false);
                }
                if (aSelectedCount.length == 1) {
                    $.UIbyID("btnMod").setEnabled(true);
                }
                else {
                    $.UIbyID("btnMod").setEnabled(false);
                }
            },
            toolbar: new sap.ui.commons.Toolbar({
				items: [
                    /*new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_ConfProdAdd"), //"Aggiungi dichiarazione",
                        id:   "btnAddConfProd",
                        icon: 'sap-icon://add',
                        enabled: false,
                        press: function (){}
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_ConfProdModify"), //"Rettifica",
                        id:   "btnModConfProd",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){}
                    }),*/
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Confirm"), //"Conferma"
                        id:   "btnConfirm",
                        icon: 'sap-icon://add-activity',
                        enabled: false,
                        press: function (){
                            fnConfirm();
                        }
                    }),
                    new sap.ui.commons.Button({
                        text: oLng_Monitor.getText("Monitor_Modify"), //"Modifica",
                        id:   "btnMod",
                        icon: 'sap-icon://edit',
                        enabled: false,
                        press: function (){
                            openOperProdEdit();
                        }
                    })
                ],
                rightItems: [
                    new sap.ui.commons.Button({
                        icon: "sap-icon://refresh",
                        press: function () {
                            refreshTabOpeProd();
                        }
                    })
                ]
            }),
            extension: new sap.ui.commons.layout.VerticalLayout({
                content: [
                    new sap.ui.commons.Toolbar({
                        items: [
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_From"), //"Da:",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oDatePickerF,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_To"), //"a:",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            oDatePickerT,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_Shift") //Turno
                            }),
                            oCmbShiftFilter,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_Operator"), //"Operatore",
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
                            }),
                            new  sap.ui.commons.TextField("txtOpeFiter", {
                                value: "",
                                editable: false,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2" })
                            }),
                            new sap.ui.commons.Button("btnOpeFilter", {
                                icon: "sap-icon://arrow-down",
                                tooltip: oLng_Monitor.getText("Monitor_SelectOperator"), //Seleziona operatore
                                enabled: true,
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "1" }),
                                press: function (){
                                    fnGetOperatorTableDialog(
                                        ($.UIbyID("filtPlant").getSelectedKey() === "")?('#plant').val():$.UIbyID("filtPlant").getSelectedKey(),
                                        "",
                                        setSelectedOperator
                                    );
                                }
                            })
                        ]
                    }),
                    new sap.ui.commons.Toolbar({
                        items: [
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_IDLINE") //Linea
                            }),
                            oCmbLineFilter,
                            new sap.ui.commons.Label({
                                text: oLng_Monitor.getText("Monitor_Declared") //Dichiar.
                            }),
                            oCmbTypeConfFilter,
                            new sap.ui.commons.Button({
                                icon: "sap-icon://filter",
                                enabled: false,
                                id:   "delFilter",
                                press: function () {
                                    $.UIbyID("filtShift").setSelectedKey("");
                                    $.UIbyID("filtLine").setSelectedKey("");
                                    $.UIbyID("filtTypeConf").setSelectedKey("");
                                    $.UIbyID("txtOpeFiter").setValue("").data("SelectedKey", "");
                                    $.UIbyID("delFilter").setEnabled(false);
                                    refreshTabOpeProd();
                                }
                            })
                        ]
                    })
                ]
            })
        },
        exportButton: false,
		columns: [
			{
				Field: "CONFIRMED", 
				label: oLng_Monitor.getText("Monitor_Confirmed"), //"Confermato",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "EXTENDED_TEXT",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "TYPECONF",
				label: oLng_Monitor.getText("Monitor_Declared"), //"Dichiar.",
				properties: {
					width: "90px",
                    visible: false,
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "OPEID", 
				label: oLng_Monitor.getText("Monitor_OperatorID"), //"nr. Badge",
				properties: {
					width: "80px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "OPENAME", 
				label: oLng_Monitor.getText("Monitor_OperatorName"), //"nome Operatore",
				properties: {
					width: "130px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "DATE_SHIFT", 
				label: oLng_Monitor.getText("Monitor_DateShift"), //"Data Turno",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false,
                    //visible: false
				},
				template: {
					type: "Date",
					textAlign: "Center"
                }
            },
            {
				Field: "SHIFT", 
				label: oLng_Monitor.getText("Monitor_Shift"), //"Turno",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "TIME_ID", 
				label: oLng_Monitor.getText("Monitor_Time"), //"Ora",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
				template: {
					type: "Time",
					textAlign: "Center"
                }
            },
            {
				Field: "PLANT", 
				label: oLng_Monitor.getText("Monitor_Division"), //"Divisione",
				properties: {
					width: "70px",
                    visible: false,
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "IDLINE", 
				label: oLng_Monitor.getText("Monitor_LineID"), //"ID Linea",
				properties: {
					width: "80px",
                    resizable : true,
                    flexible : false
				}
            },
            {
				Field: "LINETXT", 
				label: oLng_Monitor.getText("Monitor_LINETXT"), //"Descrizione Linea",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "ORDER", 
				label: oLng_Monitor.getText("Monitor_Order"), //"Commessa",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "POPER",
				properties: {
					width: "60px",
                    visible: false,
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "POPER_CUT",
				label: oLng_Monitor.getText("Monitor_Poper"), //"Fase",
				properties: {
					width: "60px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "UDCNR", 
				label: oLng_Monitor.getText("Monitor_UDCNR"), //"numero UDC",
				properties: {
					width: "125px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "SREASID", 
				properties: {
					width: "70px",
                    visible: false
                    //resizable : false,
                    //flexible : false
				}
            },
            {
				Field: "SREASTXT", 
				label: oLng_Monitor.getText("Monitor_ScrabReason"), //"Causale di scarto",
				properties: {
					width: "140px",
                    //resizable : false,
                    flexible : false,
                    //visible: false
				}
            },
            {
				Field: "QTY", 
				label: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
				properties: {
					width: "80px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
            },
            {
				Field: "WTDUR", 
				label: oLng_Monitor.getText("Monitor_WTDUR"), //"Durata",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "Numeric",
					textAlign: "Right"
                }
            },
            {
				Field: "USERMOD", 
				label: oLng_Monitor.getText("Monitor_User"), //"Utente",
				properties: {
					width: "75px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "NRPRNT", 
				label: oLng_Monitor.getText("Monitor_PrintNR"), //"nr. Stampo",
				properties: {
					width: "90px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "TYPEMOD", 
				label: oLng_Monitor.getText("Monitor_Type"), //"Tipo",
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false
				}
            },
            {
				Field: "TIME_ID", 
				label: oLng_Monitor.getText("Monitor_DateInsert"), //"Data Inserimento",
				properties: {
					width: "150px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "DATE_UPD", 
				label: oLng_Monitor.getText("Monitor_DateUPD"), //"Date update",
				properties: {
					width: "150px",
                    //resizable : false,
                    flexible : false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
                }
            },
            {
				Field: "STDIMB", 
				label: oLng_Monitor.getText("Monitor_STDIMB"), //"Imballo standard",
				properties: {
					width: "130px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "FULL", 
				label: oLng_Monitor.getText("Monitor_UDCfull"), //"UDC piena",
				properties: {
					width: "100px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
				template: {
					type: "checked",
					textAlign: "Center"
                }
            },
            {
				Field: "UMDUR", 
				properties: {
					width: "70px",
                    //resizable : false,
                    flexible : false,
                    visible: false
				},
            }
        ]
    });
    
    oModel = new sap.ui.model.xml.XMLModel();
    
    oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    /*refreshTabOpeProd();*/
    return oTable;
}

// Aggiorna la tabella Part Program
function refreshTabOpeProd() {
    var query = QService + dataProdMon + "OperProd/getOperProdSQ";
    
    if ($.UIbyID("filtPlant").getSelectedKey() === ""){
        query = query + "&Param.1=" + $('#plant').val();
    }
    else{
        query = query + "&Param.1=" +  $.UIbyID("filtPlant").getSelectedKey();
    }
    query += "&Param.2=" + dtFormat.format(dtSelect.parse($.UIbyID("dtFrom").getYyyymmdd()));
    query += "&Param.3=" + dtFormat.format(dtSelect.parse($.UIbyID("dtTo").getYyyymmdd()));
    query += "&Param.4=" + $.UIbyID("filtShift").getSelectedKey();
    query += "&Param.5=" + $.UIbyID("filtLine").getSelectedKey();
    query += "&Param.6=" + $.UIbyID("filtTypeConf").getSelectedKey();
    
    var sOperator = ($.UIbyID("txtOpeFiter").data("SelectedKey") === null)?"":$.UIbyID("txtOpeFiter").data("SelectedKey");
    query += "&Param.7=" + sOperator;    
    query += "&Param.8=" + sLanguage;
    
    $.UIbyID("OpeProdTab").getModel().loadData(query);
    $.UIbyID("OpeProdTab").setSelectedIndex(-1);
    $("#splash-screen").hide();
}

function fnConfirm(){
    
    var aSelectedIndices = $.UIbyID("OpeProdTab").getSelectedIndices();
    
    var sTxtAlreadyConfirmed = "";
    var sTxtConfirmActivity = "";
    var sTxtActivityConfirmedOk = "";
    if(aSelectedIndices.length > 1){
        sTxtAlreadyConfirmed = oLng_Monitor.getText("Monitor_AlreadyConfirmedPlur"); //"Una delle attività selezionate è già stata confermata"
        sTxtConfirmActivity = oLng_Monitor.getText("Monitor_ConfirmOperatorActivityPlur"); //"Confermare le attività selezionate?"
        sTxtActivityConfirmedOk = oLng_Monitor.getText("Monitor_ActivityConfirmedOkPlur"); //"Le attività sono state confermate correttamente"
    }
    else{
        sTxtAlreadyConfirmed = oLng_Monitor.getText("Monitor_AlreadyConfirmed"); //"L'attività selezionata è già stata confermata"
        sTxtConfirmActivity = oLng_Monitor.getText("Monitor_ConfirmOperatorActivity"); //"Confermare l'attività dell'operatore?"
        sTxtActivityConfirmedOk = oLng_Monitor.getText("Monitor_ActivityConfirmedOk"); //"L'attività è stata confermata correttamente"
    }
    
    for(var i = 0; i < aSelectedIndices.length; i++){
        if(fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "CONFIRMED", aSelectedIndices[i]) == 1){
            sap.ui.commons.MessageBox.show(
                sTxtAlreadyConfirmed,
                sap.ui.commons.MessageBox.Icon.ERROR,
                oLng_Monitor.getText("Monitor_Error") //"Errore"
                [sap.ui.commons.MessageBox.Action.OK],
                sap.ui.commons.MessageBox.Action.OK
            );
            return;
        }
    }
    
    sap.ui.commons.MessageBox.show(
        sTxtConfirmActivity,
        sap.ui.commons.MessageBox.Icon.WARNING,
        oLng_Monitor.getText("Monitor_Confirm"), //"Conferma",
        [sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO],
        function(sResult){
            if (sResult == 'YES'){
                var ret;
                for(var i = 0; i < aSelectedIndices.length; i++){
                    var qexe = dataProdMon + "OperProd/updOperProdConfirmedQR";
                    var sUdcNr = fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "UDCNR", aSelectedIndices[i]);
                    qexe += "&Param.1=" + fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "IDLINE", aSelectedIndices[i]);
                    qexe += "&Param.2=" + fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "DATE_SHIFT", aSelectedIndices[i]);
                    qexe += "&Param.3=" + fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "SHIFT", aSelectedIndices[i]);
                    qexe += "&Param.4=" + fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "OPEID", aSelectedIndices[i]);
                    qexe += "&Param.5=" + fnGetModelVal($.UIbyID("OpeProdTab").getModel(), "TIME_ID", aSelectedIndices[i]);
                    qexe += "&Param.6=" + sUdcNr;
                    qexe += "&Param.7=" + "1";
                    
                    ret = fnGetAjaxVal(qexe,["code","message"],false);
                    if(ret.code == -1){
                        sap.ui.commons.MessageBox.show(
                            oLng_Monitor.getText("Monitor_UdcSendError") + " " + sUdcNr, //Errore nella conferma dell'attività con Udc
                            sap.ui.commons.MessageBox.Icon.ERROR,
                            oLng_Monitor.getText("Monitor_SendError"), //"Errore d'invio"
                            [sap.ui.commons.MessageBox.Action.OK],
                            sap.ui.commons.MessageBox.Action.OK
                        );
                        return;
                    }
                    else{
                        qexe = "";
                    }
                }
                sap.ui.commons.MessageBox.show(
                    sTxtActivityConfirmedOk,
                    sap.ui.commons.MessageBox.Icon.SUCCESS,
                    oLng_Monitor.getText("Monitor_Confirm"), //"Conferma"
                    [sap.ui.commons.MessageBox.Action.OK],
                    sap.ui.commons.MessageBox.Action.OK
                );
                refreshTabOpeProd();
            }
        },
        sap.ui.commons.MessageBox.Action.YES
    );
}

function openOperProdEdit(){
    
    //  Crea la finestra di dialogo
	var oDlgOperProdEdit = new sap.ui.commons.Dialog({
		modal:  true,
		resizable: true,
		id: "dlgOperProdEdit",
		maxWidth: "300px",
		maxHeight: "200px",
		//minHeight: "900px",
		title: oLng_Monitor.getText("Monitor_ModifyOpeActivity"), //"Modifica attività operatore",
		showCloseButton: false
	});

	var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
	var oForm1 = new sap.ui.layout.form.Form({
		width: "98%",
		layout: oLayout1,
		formContainers: [
			new sap.ui.layout.form.FormContainer({
				formElements: [
					new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_Quantity"), //"Quantità",
							layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: fnGetCellValue("OpeProdTab", "QTY"),
								layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
								editable: true,
								maxLength: 4,
								id: "txtOperProdQTY",
								liveChange: function(ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
						]
					}),
                    new sap.ui.layout.form.FormElement({
						label: new sap.ui.commons.Label({
							text: oLng_Monitor.getText("Monitor_WTDUR"), //"Dutata",
							layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})
						}),
						fields: [
							new sap.ui.commons.TextField({
								value: fnGetCellValue("OpeProdTab", "WTDUR"),
                                layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
								editable: true,
								maxLength: 4,
								id: "txtOperProdWTDUR",
								liveChange: function(ev) {
									var val = ev.getParameter('liveValue');
									var newval = val.replace(/[^\d]/g, '');
									this.setValue(newval);
								}
							})
						]
					})
				]
			})
		]
	});

	oDlgOperProdEdit.addContent(oForm1);
    oDlgOperProdEdit.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Save"), //"Salva",
		press:function(){
			fnSaveOperProd();
		}
	}));
    oDlgOperProdEdit.addButton(new sap.ui.commons.Button({
		text: oLng_Monitor.getText("Monitor_Close"), //"Chiudi",
		press:function(){
			refreshTabOpeProd();
			oDlgOperProdEdit.close();
			oDlgOperProdEdit.destroy();
		}
	}));
	oDlgOperProdEdit.open();
}

// Funzione per salvare le modifiche all'attività dell'operatore
function fnSaveOperProd() 
{
    var qexe = dataProdMon + "OperProd/updOperProdSQ";
    qexe += "&Param.1=" + fnGetCellValue("OpeProdTab", "IDLINE");
    qexe += "&Param.2=" + fnGetCellValue("OpeProdTab", "DATE_SHIFT");
    qexe += "&Param.3=" + fnGetCellValue("OpeProdTab", "SHIFT");
    qexe += "&Param.4=" + fnGetCellValue("OpeProdTab", "OPEID");
    qexe += "&Param.5=" + fnGetCellValue("OpeProdTab", "TIME_ID");
    qexe += "&Param.6=" + fnGetCellValue("OpeProdTab", "UDCNR");
    qexe += "&Param.7=" + $.UIbyID("txtOperProdQTY").getValue();
    qexe += "&Param.8=" + $.UIbyID("txtOperProdWTDUR").getValue();
    
    var ret = fnExeQuery(qexe, oLng_Monitor.getText("Monitor_OpeActivityModified"),
                         oLng_Monitor.getText("Monitor_OpeActivityModifiedError")
                         /*"L'attività dell'operatore è stata modificata correttamente", "Errore in modifica attività operatore"*/,
                         false);
    if (ret){
        refreshTabOpeProd();
        $.UIbyID("dlgOperProdEdit").close();
        $.UIbyID("dlgOperProdEdit").destroy();
    }
}

function setSelectedOperator(oControlEvent){
    var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];

	var sBadge = oControlEvent.getSource().getModel().getProperty("BADGE", oSelContext);
	var sOpeName = oControlEvent.getSource().getModel().getProperty("OPENAME", oSelContext);
	//var oSelContext =  oControlEvent.getParameter("selectedContexts")[0];
    $.UIbyID("txtOpeFiter").setValue(sOpeName).data("SelectedKey", sBadge);
    refreshTabOpeProd();
    $.UIbyID("dlgOperatorTableSelect").destroy();
    $.UIbyID("delFilter").setEnabled(true);   
}

