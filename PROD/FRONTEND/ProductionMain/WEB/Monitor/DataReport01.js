/*jslint white: true, sloppy: true, sub: true, undef: true, nomen: true, eqeqeq: true, maxerr: 300*/

//Interfaccia MII per report dati caricati - Monitor
//**********************************************************************************************************************
// Ver: 0.2 - Author: Luca Adanti
//**********************************************************************************************************************


/* Variabili della pagina */
var sc = sap.ui.commons;
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataProd = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";

// legge la lingua locale
var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

// setta le risorse per la lingua locale
var oLng_MonitorData = jQuery.sap.resources({
	url: "/XMII/CM/ProductionMain/res/monitor.i18n.properties",
	locale: sCurrentLocale
});

jQuery.ajaxSetup({
	cache: false
});


Libraries.load(
	[
		"/XMII/CM/Common/MII_core/UI5_utils",
		/*"/XMII/CM/ProductionMain/MasterData/Grstreas_fn/fn_grstreas"*/
	],
	function () {
		$(document).ready(function () {
			/***********************************************************************************************/
			// Inizializzazione pagina
			/***********************************************************************************************/

			$("#splash-screen").hide();


			//Escape per gestione parametro plant da GET/POST

			if ($('#plant').val() === '{plant}') {
				$('#plant').val("2101");
			}



			var oTable1 = createTabMonitor();

			var oVerticalLayout = new sap.ui.commons.layout.VerticalLayout({
				content: [
					oTable1
				]
			});

			oVerticalLayout.placeAt("MasterCont");
		});
	}
);








// Functione che crea la tabella Grstrease e ritorna l'oggetto oTable
function createTabMonitor() {

	// creo il filtro per divisioni
	var oCmbPlant = new sap.ui.commons.ComboBox("filtPlant", {
		selectedKey: $('#plant').val(),
		tooltip: oLng_MonitorData.getText("Monitor_FilterDivision"), //"Filtro Divisioni",
		change: function (oEvent) {
			/*
					$.UIbyID("ShiftTab").filter(oPlantFShift,$("#cmbPlant-input").val());
					*/
		}
	});

	var oItemPlant = new sap.ui.core.ListItem();
	oItemPlant.bindProperty("text", "NAME1");
	oItemPlant.bindProperty("key", "PLANT");
	oCmbPlant.bindItems("/Rowset/Row", oItemPlant);

	var oPlantsModel = new sap.ui.model.xml.XMLModel();
	oPlantsModel.loadData(QService + dataProd + "MasterData/Plants/getPlantsSQ");
	oCmbPlant.setModel(oPlantsModel);
	oCmbPlant.setSelectedKey($('#plant').val());

	var lblPlant = new sc.Label({
		text: oLng_MonitorData.getText("Monitor_FilterDivision")
	});

	/* format per data e ora */
	var dtFormat = sap.ui.core.format.DateFormat.getDateInstance({
		pattern: "yyyyMMdd"
	});
	var dtSelect = sap.ui.core.format.DateFormat.getDateInstance({
		pattern: "yyyy-dd-MM"
	});

	var oDatePickerF = new sap.ui.commons.DatePicker('dtFrom');
	oDatePickerF.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerF.setLocale("it");
	oDatePickerF.attachChange(
		function (oEvent) {
			//refreshTabScrapPanel();
		}
	);

	var oDatePickerT = new sap.ui.commons.DatePicker('dtTo');
	oDatePickerT.setYyyymmdd(dtFormat.format(new Date()));
	oDatePickerT.setLocale("it");
	oDatePickerT.attachChange(
		function (oEvent) {
			//refreshTabScrapPanel();
		}
	);

	var lblDtFrom = new sc.Label({
		text: oLng_MonitorData.getText("Monitor_DateFrom")
	});
	var lblDtTo = new sc.Label({
		text: oLng_MonitorData.getText("Monitor_DateTo")
	});

	//Crea L'oggetto Tabella Grstrease
	var oTable = UI5Utils.init_UI5_Table({
		id: "MonitorTab",
		properties: {
			title: oLng_MonitorData.getText("Report01_TabTitle"), //"Lista causali fermo",
			visibleRowCount: 15,
			width: "98%",
			firstVisibleRow: 0,
			selectionMode: sap.ui.table.SelectionMode.Single,
			navigationMode: sap.ui.table.NavigationMode.Scrollbar,
			visibleRowCountMode: sap.ui.table.VisibleRowCountMode.Interactive,
			rowSelectionChange: function (oControlEvent) {

			}
		},
		sumColCount: 1,
		exportButton: true,
		toolbarItems: [lblPlant, oCmbPlant, lblDtFrom, oDatePickerF, lblDtTo, oDatePickerT,
									new sap.ui.commons.Button({
				text: oLng_MonitorData.getText("Monitor_Refresh"), //"Aggiorna",
				icon: 'sap-icon://refresh',
				enabled: true,
				press: function () {
					refreshTabMonitor();
				}
			})],
		columns: [


			{
				Field: "REPTXT",
				label: oLng_MonitorData.getText("Monitor_REPTXT"), //"Reparto",
				sumCol: true,
				properties: {
					width: "80px",
					autoResizable: false,
				}
			},
			{
				Field: "IDLINE",
				label: oLng_MonitorData.getText("Monitor_IDLINE"), //"STTYPE",
				properties: {
					width: "50px",
					autoResizable: false,
				}
				/*,
								template: {
									type: "DateTime",
									textAlign: "Center"
												}*/
			},
			{
				Field: "MATID",
				label: oLng_MonitorData.getText("Monitor_MATID"), //"Reparto",
				properties: {
					width: "100px",
					autoResizable: false,
				}
			},
			{
				Field: "MAKTX",
				label: oLng_MonitorData.getText("Monitor_MAKTX"), //"Reparto",
				properties: {
					width: "150px",
					autoResizable: false,
				}
			},
			{
				Field: "LOTID",
				label: oLng_MonitorData.getText("Monitor_LOTID"), //"Reparto",
				properties: {
					width: "110px",
					autoResizable: false,
				}
			},
			{
				Field: "DATE_SHIFT",
				label: oLng_MonitorData.getText("Monitor_DateShift"), //"Reparto",
				sumCol: true,
				properties: {
					width: "110px",
					autoResizable: false,
				},
				template: {
					type: "Date",
					textAlign: "Center"
				}
			},
			{
				Field: "SHIFT",
				label: oLng_MonitorData.getText("Monitor_Shift"), //"Reparto",
				properties: {
					width: "110px",
					autoResizable: false,
				}
			},
			/*
						{
							Field: "QTY_PROD",
							label: oLng_MonitorData.getText("Monitor_QTYPROD"), //"Quantità dichiarata prod",
							properties: {
								width: "70px",
								autoResizable: false,
							},
							template: {
								type: "Number",
								textAlign: "Right"
								}
						},*/
			{
				Field: "QTY_IN",
				label: oLng_MonitorData.getText("Monitor_QTYIN"), //"Quantità ingresso",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			{
				Field: "QTY_OUT",
				label: oLng_MonitorData.getText("Monitor_QTYOUT"), //"Quantità uscita",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			/*
						{
							Field: "QTY_WIP",
							label: oLng_MonitorData.getText("Monitor_QTYWIP"), //"Quantità uscita",
							properties: {
								width: "70px",
								autoResizable: false,
							},
							template: {
								type: "Number",
								textAlign: "Right"
							}
						},*/
			{
				Field: "QTY_SCRAP",
				label: oLng_MonitorData.getText("Monitor_QTYSCRAP"), //"Quantità uscita",
				properties: {
					width: "70px",
					autoResizable: false,
				},
				template: {
					type: "Number",
					textAlign: "Right"
				}
			},
			/*
						{
							Field: "DATE_UPD",
							label: oLng_MonitorData.getText("Monitor_DATEUPD"), //"Ultimo aggiornamento",
							properties: {
								width: "90px",
								autoResizable: false
							},
							template: {
								type: "DateTime",
								textAlign: "Center"
							}
						},
			{
				Field: "LAST_CHECK",
				label: oLng_MonitorData.getText("Monitor_LASTCHECK"), //"Ultimo controllo",
				properties: {
					width: "90px",
					autoResizable: false
				},
				template: {
					type: "DateTime",
					textAlign: "Center"
				}
			}*/
		]
	});

	oModel = new sap.ui.model.xml.XMLModel();

	oTable.setModel(oModel);
	oTable.bindRows("/Rowset/Row");
	refreshTabMonitor();
	return oTable;
}

// Aggiorna la tabella Grstreas
function refreshTabMonitor() {
	//$.UIbyID("MonitorTab").getModel().loadData(QService + dataMonitor + "getMonitorPlantQR" );
	$("#splash-screen").hide();
	//numero righe
	Params = "&Param.3=" + $.UIbyID("filtPlant").getSelectedKey() + "&RowCount=200";
	Params = Params + "&Param.1=" + $.UIbyID("dtFrom").getYyyymmdd();
	Params = Params + "&Param.2=" + $.UIbyID("dtTo").getYyyymmdd();

	var qParamsTab = {
		data: "ProductionMain/Monitor/dataReport01SQ" + Params,
		dataType: "xml"
	};

	$.UIbyID("MonitorTab").setBusy(true);
	UI5Utils.getDataModel(qParamsTab)
		// on success
		.done(function (data) {
			// carica il model della tabella
			$.UIbyID("MonitorTab").setBusy(true);

			$.UIbyID("MonitorTab").getModel().setData(data);
			//$.UIbyID("MonitorTab").getModel().refresh(true);
		}) // End Done Function
		// on fail
		.fail(function () {
			sap.ui.commons.MessageBox.alert(oLng_MonitorData.getText("Monitor_LoadERR", ['val1', 'val2']));
		})
		// always, either on success or fail
		.always(function () {
			// remove busy indicator
			$.UIbyID("MonitorTab").setBusy(false);
		});
}
