// Script per pagina pannello impianti
// test luca by FTP

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/Monitor/";
var PrecLine = 1;

jQuery.ajaxSetup({
	cache: false
});
//jQuery.sap.includeScript("/XMII/CM/ProductionMain/Monitor/DashBoard/DashBoardItem.js?v=" + Date.now());  // Dialog dettaglio per linea
jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("sap.suite.ui.microchart.RadialMicroChart");
/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/

function refreshTabLines() {

	var qParams = {
		data: "ProductionMain/DashBoard/getDashBoardQR&Param.1=" + $('#plant').val()
		//+ "&Param.2=" + $('#group').val()
		//+ "&Param.3=" + $('#depart').val()
		+ "&Param.4=" + "it",
		dataType: "json"
	};

	UI5Utils.getDataModel(qParams)
	// on success
		.done(function (data) {
		// pupulate the model
		var oModel = $.UIbyID("dashboardMon").getModel();
		oModel.setData(data);
		updatePage(oModel);
	})
	// on fail
		.fail(function () {
		//sap.ui.commons.MessageBox.alert("Errore nell'aggiornamento dati");
		sap.m.MessageToast.show("Errore nell'aggiornamento dati", {
			duration: 45000,
			at: sap.ui.core.Popup.Dock.CenterTop,
			my: sap.ui.core.Popup.Dock.CenterTop,
		});
		$.UIbyID("appHeader").setLogoText("Cruscotto - (errore in aggiornamento)");
	})
	// always, either on success or fail
		.always(function () {
		// remove busy indicator
		//oTable.setBusy(false);
	});
}


function updatePage(oModel) {
	var sTitle = oModel.getProperty("/Rowsets/Rowset/0/Row/0/REPTXT");

	if (typeof sTitle != 'undefined') {
		var currentdate = new Date();
		var curTime = ((currentdate.getHours() < 10)?"0":"") +currentdate.getHours()  + ":"
		+ ((currentdate.getMinutes() < 10)?"0":"") + currentdate.getMinutes() + ":"
		+((currentdate.getSeconds() < 10)?"0":"") + currentdate.getSeconds();
		$.UIbyID("appHeader").setLogoText("Cruscotto produzione Rezzato - (aggior.: " +curTime + ")");
	}

	if (window.console) console.log("Refresh data");
	$(".sapUiProgInd").each(function() {
		var curPerc = $.UIbyID($( this ).attr( "id" )).getDisplayValue();
		if (curPerc.indexOf("+")>0) $.UIbyID($( this ).attr( "id" )).setBarColor(sap.ui.core.BarColor.NEGATIVE );
		else $.UIbyID($( this ).attr( "id" )).setBarColor(sap.ui.core.BarColor.NEUTRAL  );
	});
}

$(document).ready(function () {

	var oDataSet = createDashboard();
	oDataSet.placeAt("MasterCont");

	$("#splash-screen").hide();

	tabRefr = $.timer(function () {
		refreshTabLines();
	}, 180 * 1000, false);

	tabRefr.set({
		autostart: true
	});

});






function createDashboard() {

	var oTileContainer = new sap.m.TileContainer({
		id: "dashboardMon",
		size: sap.m.Size.XS,
		height: '65rem',
		tiles: {
			path : "/Rowsets/Rowset/0/Row",
			template : createCustomTile()
		}
	});
	oTileContainer.setModel(new sap.ui.model.json.JSONModel());

	refreshTabLines();

	return new sap.m.Panel({
		height: '100%',
		content: oTileContainer
	});

}


//Initialize the Dataset and the layouts
function createCustomTile() {
	jQuery.sap.declare("itlink.control.DashBoardTile");


	//....custom tile created and extended
	sap.m.CustomTile
		.extend(
		"itlink.control.DashBoardTile",
		{

			metadata : {
				properties : {
					"OEEBackColor" : {
						type : "string",
						defaultValue : "OEE_C"
					}
				},
			},

			init : function() {	},

			renderer : {},

			onAfterRendering : function(rm, ctrl) {
				sap.m.CustomTile.prototype.onAfterRendering
					.call(this);
				var $This = this.$();
				$This.addClass(this.getOEEBackColor());
			}

		});

	return new itlink.control.DashBoardTile({
		width: "12rem",
		height: "30rem",
		OEEBackColor: "{OEE_BackColor}" ,/*{
			parts: [
				{path: "OEE_BackColor"},
			],
			formatter: function(sBackColor) {
				if(sBackColor)
				{
					this.addStyleClass(sBackColor);
				}
				return sBackColor;
			}
		},*/
		content: createCustomContent()
	}).addStyleClass("CustomTileBorder").addStyleClass("dbTileSize");

}


function createCustomContent() {

	return [
		new sap.ui.layout.Grid({
			width: "100%",
			height: "30rem",
			content: [
				new sap.m.HBox({
					displayInline: false,
					alignItems: "End",
					justifyContent: "End",
					alignContent: "End" /*sap.m.FlexAlignContent.Center*/,
					items: [
						new sap.m.Label({
							text: "{LINE_LABEL}",
							tooltip: "{IDLINE}",
							design: "Bold",
							width: '11.9em',
							textAlign: sap.ui.core.TextAlign.Left,
							layoutData : new sap.ui.layout.GridData({
								span: "L12 M12 S12",
								linebreak:false
							})
						}).data("LineID","{IDLINE}"),
						new sap.m.Label({
							text: ' ',
							width: '0.3em'
						}),
						new sap.ui.core.Icon({
							src: "{STATE_ICON}",
							size:"24px",
							color: "white",
							layoutData : new sap.ui.layout.GridData({
								span: "L12 M12 S12",
								linebreak:true
							})
						})],
					layoutData : new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak:true
					})}).addStyleClass("BorderLine"),
				new sap.m.Label({
					text: "{MATERIAL_LABEL}",
					tooltip: "{MATDESC_LABEL}",
					width: '240px',
					design: "Bold",
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData : new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak:true
					})
				}),
				new sap.m.HBox({
					width: "100%",
					displayInline: true,
					alignItems: "Center",
					justifyContent: "Center",
					alignContent: "Center" /*sap.m.FlexAlignContent.Center*/,
					items: [
						new sap.ui.core.Icon({
							src: "sap-icon://circle-task-2",
							size:"10px",
							color: "{STATE_COLOR}",
							press: function(oEvent){
								var idx = oEvent.getParameter("id");
							}
						}).addStyleClass("iconAlign"),
						new sap.m.Label({
							text: ' ',
							width: '4px'
						}),
						new sap.m.Label({
							text: '{STATE_LABEL}',
							design: "Bold",
							width: '100%',
							textAlign: sap.ui.core.TextAlign.Center,

						})],
					layoutData : new sap.ui.layout.GridData({
						span: "L11 M11 S11",
						linebreak:true
					})
				}),
				new sap.m.Label({
					text: 'Prodotti',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S6",
						linebreak:true
					})
				}),
				new sap.m.Label({
					text: 'Target',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S6"
					})
				}),
				new sap.m.Label({
					text: '{QTY_OUT_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S6",
						linebreak:true
					})
				}),
				new sap.m.Label({
					text: '{QTY_TARG_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L6 M6 S6"
					})
				}),
				new sap.m.HBox({
					width: "10rem",
					height: "8rem",
					displayInline: false,
					alignItems: "Center",
					justifyContent: "Center",
					alignContent: "Center" /*sap.m.FlexAlignContent.Center*/,
					layoutData : new sap.ui.layout.GridData({
						span: "L11 M11 S11",
						linebreak:true
					}),
					items: [
						new sap.suite.ui.microchart.RadialMicroChart(
						{//total : 200,
						//fraction: 5,
						percentage:  "{KP_OEE}",
						width: "7rem",
						size: sap.m.Size.XS,
						valueColor:"{OEE_Color}",
						press: function(oEvent){
						var idx = oEvent.getParameter("id");
							//alert ($.UIbyID(idx).data("LineID"));
							ShowDashBoardItem($.UIbyID(idx).data("LineID"));
						}
						}).data("LineID","{IDLINE}")
					 ]}),
				new sap.m.Label({
					text: '{QTY_SCRAP_PZ}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData : new sap.ui.layout.GridData({
						span: "L4 M4 S4",
						linebreak:true
					})
				}),
				new sap.m.Label({
					text: 'OEE ',
					design: "Bold",
					width: '105%',
					textAlign: sap.ui.core.TextAlign.Center,
					layoutData : new sap.ui.layout.GridData({
						span:  "L3 M3 S3"
					})
				}),
				new sap.m.Label({
					text: '{T_DUR_MIN}',
					design: "Bold",
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span:  "L4 M4 S4",
					})
				}),
				new sap.m.Label({
					text: 'Sospesi',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Left,
					layoutData : new sap.ui.layout.GridData({
						span: "L5 M5 S5",
						linebreak:true
					})
				}),
				new sap.m.Label({
					text: 'Non disponibile',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					layoutData : new sap.ui.layout.GridData({
						span: "L7 M7 S7",
						linebreak:false
					})
				}),
				new sap.m.HBox({
					width: "100%",
					displayInline: false,
					alignItems: "End",
					justifyContent: "End",
					alignContent: "End" /*sap.m.FlexAlignContent.Center*/,
					layoutData : new sap.ui.layout.GridData({
						span: "L12 M12 S12",
						linebreak:false
					}),
					items: [
					new sap.m.Label({
						text: '{DATE_UPD}',
					width: '100%',
					textAlign: sap.ui.core.TextAlign.Right,
					})]
				}).addStyleClass("BorderLineTop")
			]

		})
	];
}

function ShowDashBoardItem(sIdLine) {

	var sLink = "/XMII/CM/ProductionMain/Monitor/DashBoard/DashBoardDet.irpt";
	sLink += "?plant=" + $('#plant').val();
	sLink += "&idline=" + sIdLine;

	var oHtml = new sap.ui.core.HTML();
	oHtml.setContent('<iframe width=100% height=100% style=" border: 0px; width: calc(100% + 1.5rem);" src="' +  sLink + '"></iframe>');

						// Crea una dialog con contenuto la pagina HTML creata da DashBoardDet.irpt
	var oItemDialog = new sap.m.Dialog({
		title: 'Dettaglio per linea '+ sIdLine,
		contentWidth: "950px",
		contentHeight: "420px",
		//resizable: true,
		horizontalScrolling: false,
		verticalScrolling: false,
		content: oHtml,
		buttons: [
			new sap.m.Button({
				text: 'Apri pagina',
				press: function () {
					var win = window.open(sLink, '_blank'); //Apre DashBoardDet.irpt in un'altra scheda
					win.focus();
				}
			}),
			new sap.m.Button({
				text: 'Chiudi',
				press: function () {
					oHtml.destroy();
					oItemDialog.close();
				}
			})
		],
		beforeOpen: function () {
			//sap.ui.core.BusyIndicator.hide();
		},
		afterOpen() {
			$("#splash-screen").hide();
		},
		afterClose: function () {
			oItemDialog.destroy();
		}
	}).addStyleClass('noPaddingDialog');

	oItemDialog.open();
}
