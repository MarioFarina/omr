/* jshint -W117, -W098 */
function ShowDashBoardItem(sIdLine) {
	var dataMonitor = "Content-Type=text/XML&QueryTemplate=ProductionMain/";
	const sItemDialogId = 'itemDialog';
	var oItemDialog = $.UIbyID(sItemDialogId);
	if(!oItemDialog)
	{
		sap.ui.core.BusyIndicator.show(1000);

		var percentageFunc = function(sKpField, sColorFIeld, oText) {
			return {
				parts: [
					{path: sKpField },
					{path: sColorFIeld }
				],
				formatter: function(sPercentage, sColor){
					var sStyle;
					switch(sColor) {
						case "Good":
							sStyle = 'greenText';
							break;
						case "Critical":
							sStyle = 'yellowText';
							break;
						case "Error":
							sStyle = 'redText';
							break;
											 }
					oText.addStyleClass(sStyle);
					var fPercentage = parseFloat(sPercentage).toFixed();
					return parseFloat(fPercentage);
				}
			};
		};

		var valueFunc = function(sValueField) {
			return {
				parts: [
					{path: sValueField }
				],
				formatter: function(sValue){
					return parseFloat(sValue);
				}
			};
		};

		var oOEEText = new sap.m.Text({
			width: "100%",
			text: "OEE",
			textAlign: sap.ui.core.TextAlign.Center
		}).addStyleClass('boldText');

		var oOEERadial = new sap.suite.ui.microchart.RadialMicroChart({
			valueColor: "{model_1>/Rowset/Row/OEE_Color}"
		}).bindProperty("percentage", percentageFunc("model_1>/Rowset/Row/KP_OEE","model_1>/Rowset/Row/OEE_Color",oOEEText));

		var oUTIText = new sap.m.Text({
			width: "100%",
			text: "UTI",
			textAlign: sap.ui.core.TextAlign.Center
		}).addStyleClass('boldText yellowText');

		var oUTIRadial = new sap.suite.ui.microchart.RadialMicroChart({
			valueColor: "{model_1>/Rowset/Row/EFF_Color}"
		}).bindProperty("percentage", percentageFunc("model_1>/Rowset/Row/KPE_EFF","model_1>/Rowset/Row/EFF_Color",oUTIText));

		var oDISText = new sap.m.Text({
			width: "100%",
			text: "DIS",
			textAlign: sap.ui.core.TextAlign.Center
		}).addStyleClass('boldText');

		var oDISRadial = new sap.suite.ui.microchart.RadialMicroChart({
			valueColor: "{model_1>/Rowset/Row/DIS_Color}"
		}).bindProperty("percentage", percentageFunc("model_1>/Rowset/Row/KPT_DISP","model_1>/Rowset/Row/DIS_Color",oDISText));

		var oRENText = new sap.m.Text({
			width: "100%",
			text: "REN",
			textAlign: sap.ui.core.TextAlign.Center
		}).addStyleClass('boldText');

		var oRENRadial = new sap.suite.ui.microchart.RadialMicroChart({
			valueColor: "{model_1>/Rowset/Row/PER_Color}"
		}).bindProperty("percentage", percentageFunc("model_1>/Rowset/Row/KPP_PER","model_1>/Rowset/Row/PER_Color",oRENText));

		var oQUAText = new sap.m.Text({
			width: "100%",
			text: "QUA",
			textAlign: sap.ui.core.TextAlign.Center
		}).addStyleClass('boldText');

		var oQUARadial = new sap.suite.ui.microchart.RadialMicroChart({
			valueColor: "{model_1>/Rowset/Row/QUA_Color}"
		}).bindProperty("percentage", percentageFunc("model_1>/Rowset/Row/KPQ_QUA","model_1>/Rowset/Row/QUA_Color",oQUAText));

		var oMonthComparison = new sap.suite.ui.microchart.ComparisonMicroChart({
			width: "100%",
			minValue : 0,
			maxValue : 50000
		});

		var oCompDataTemplate = new sap.suite.ui.microchart.ComparisonMicroChartData()
		.bindProperty('title',{
			parts: [
				{path: "model_3>MATERIAL" },
				{path: "model_3>QTY_PROD" }
			],
			formatter: function(sMaterial, sQty){
				return sMaterial + " attuale:" + sQty;
			}
		}).bindProperty('value',valueFunc("model_3>QTY_PROD"))
		.bindProperty('displayValue',{
			parts: [
				{path: "model_3>QTY_TARG" }
			],
			formatter: function(sQty){
				return "pianificato: " + parseFloat(sQty).toFixed();
			}
		});

		oMonthComparison.bindAggregation("data", "model_3>/Rowset/Row", oCompDataTemplate);

		var oTrendTable = new sap.m.Table({
			mode: sap.m.ListMode.None,
			columns: [
				new sap.m.Column({
					header: new sap.m.Text({
						text: ""
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: "Prod"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: "Wip"
					})
				}),
				new sap.m.Column({
					header: new sap.m.Text({
						text: "Sosp"
					})
				})
			]
		});

		var oTrendTemplate = new sap.m.ColumnListItem({
			type : sap.m.ListType.Inactive,
			cells: [
				new sap.m.Text({
					text: "{model_4>PERIOD}"
				}),
				new sap.m.Text({
					text: "{model_4>PROD_TARG}"
				}),
				new sap.m.Text({
					text: "{model_4>WIP}"
				}),
				new sap.m.Text({
					text: "{model_4>SCRAP}"
				})
			]
		});
		oTrendTable.bindAggregation("items","model_4>/Rowset/Row",oTrendTemplate);

		oItemDialog = new sap.m.Dialog({
			id: sItemDialogId,
			title: 'Dettaglio per linea '+ sIdLine,
			contentWidth: "900px",
			contentHeight: "600px",
			resizable: true,
			horizontalScrolling: false,
			verticalScrolling: false,
			content: [
				new sap.ui.layout.Grid ({
					hSpacing: 1,
					vSpacing: 0.5,
					content: [
						new sap.m.Panel({
							content: [
								new sap.ui.layout.Grid ({
									content: [
										new sap.m.Text({
											width: "100%",
											text: "{model_1>/Rowset/Row/LINE_LABEL}",
											textAlign: sap.ui.core.TextAlign.Left,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										}).addStyleClass('boldText'),
										new sap.m.Text({
											width: "100%",
											text: "{model_1>/Rowset/Row/DATE_UPD}",
											textAlign: sap.ui.core.TextAlign.Right,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										})
										/*
										new sap.ui.layout.VerticalLayout({
											width: "100%",
											content: [
												new sap.m.Text({
													width: "100%",
													text: "{/Rowset/Row/DATE_UPD}",
													textAlign: sap.ui.core.TextAlign.Right
												}).addStyleClass('smallText'),
												new sap.m.Text({
													width: "100%",
													text: "13:00:00",
													textAlign: sap.ui.core.TextAlign.Right
												}).addStyleClass('smallText')
											],
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										})*/
									]
								})
							],
							layoutData : new sap.ui.layout.GridData({
								span: "L9 M9 S9"
							})
						}).addStyleClass('smallPanelPadding'),
						new sap.m.Panel({
							content: [
								new sap.m.Text({
									width: "100%",
									text: "{model_1>/Rowset/Row/STATE_LABEL}",
									textAlign: sap.ui.core.TextAlign.Right
								}).addStyleClass('whiteText boldText'),
								/*
								new sap.m.Text({
									width: "100%",
									text: "da 2 min",
									textAlign: sap.ui.core.TextAlign.Right
								}).addStyleClass('whiteText smallText')
								*/
							],
							layoutData : new sap.ui.layout.GridData({
								span: "L3 M3 S3"
							})
						}).addStyleClass('bluePanel smallPanelPadding'),
						new sap.m.Panel({
							height: "297px",
							content: [
								new sap.ui.layout.Grid ({
									content: [
										new sap.ui.layout.VerticalLayout({
											content: [
												new  sap.m.FlexBox({
													width: "170px",
													height: "170px",
													items: [
														new sap.ui.layout.VerticalLayout({
															width: "100%",
															content: [
																oOEEText,
																oOEERadial
															],
															layoutData: new sap.m.FlexItemData({
																growFactor: 1
															})
														})
													]
												}),
												new  sap.m.FlexBox({
													height: "110px",
													width: "90px",
													items: [
														new sap.ui.layout.VerticalLayout({
															width: "100%",
															content: [
																oUTIRadial,
																oUTIText
															],
															layoutData: new sap.m.FlexItemData({
																growFactor: 1
															})
														})
													]
												})
											],
											layoutData : new sap.ui.layout.GridData({
												span: "L4 M4 S4"
											})
										}),
										new sap.ui.layout.VerticalLayout({
											content: [
												new sap.ui.layout.Grid ({
													content: [
														new  sap.m.FlexBox({
															width: "110px",
															height: "130px",
															items: [
																new sap.ui.layout.VerticalLayout({
																	width: "100%",
																	content: [
																		oDISText,
																		oDISRadial
																	]
																})
															],
															layoutData : new sap.ui.layout.GridData({
																span: "L4 M4 S4"
															})
														}),
														new  sap.m.FlexBox({
															width: "110px",
															height: "130px",
															items: [
																new sap.ui.layout.VerticalLayout({
																	width: "100%",
																	content: [
																		oRENText,
																		oRENRadial
																	]
																})
															],
															layoutData : new sap.ui.layout.GridData({
																span: "L4 M4 S4"
															})
														}),
														new  sap.m.FlexBox({
															width: "110px",
															height: "130px",
															items: [
																new sap.ui.layout.VerticalLayout({
																	width: "100%",
																	content: [
																		oQUAText,
																		oQUARadial
																	]
																})
															],
															layoutData : new sap.ui.layout.GridData({
																span: "L4 M4 S4"
															})
														})
													]
												}),
												new sap.m.Panel({
													headerText: "PIANO di PRODUZIONE [mese]",
													backgroundDesign: sap.m.BackgroundDesign.Transparent,
													content: oMonthComparison
												})
											],
											layoutData : new sap.ui.layout.GridData({
												span: "L8 M8 S8"
											})
										})
									]
								})
							],
							layoutData : new sap.ui.layout.GridData({
								span: "L9 M9 S9",
								linebreak: true
							})
						}).addStyleClass('smallPanelPadding'),
						new sap.m.Panel({
							height: "297px",
							content: [
								new sap.m.Text({
									width: "100%",
									text: "Rendimento"
								}).addStyleClass('boldText'),
								new sap.ui.layout.Grid ({
									width: "100%",
									content: [
										new sap.m.Text({
											text: 'Prodotti',
											width: '100%',
											textAlign: sap.ui.core.TextAlign.Left,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										}).addStyleClass('smallText'),
										new sap.m.Text({
											text: 'Target',
											width: '100%',
											textAlign: sap.ui.core.TextAlign.Right,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										}).addStyleClass('smallText'),
										new sap.m.Text({
											text: "{model_1>/Rowset/Row/QTY_OUT_PZ}",
											width: '100%',
											textAlign: sap.ui.core.TextAlign.Left,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6",
												linebreak:true
											})
										}).addStyleClass('boldText'),
										new sap.m.Text({
											text: "{model_1>/Rowset/Row/QTY_TARG_PZ}",
											width: '100%',
											textAlign: sap.ui.core.TextAlign.Right,
											layoutData : new sap.ui.layout.GridData({
												span: "L6 M6 S6"
											})
										}).addStyleClass('boldText'),
									]
								}),
								new  sap.m.FlexBox({
									items: [
										new sap.suite.ui.microchart.BulletMicroChart({
											//width: "100%",
											minValue: 0,
											maxValue: "{model_1>/Rowset/Row/QTY_TARG}",
											showActualValue: false,
											actual: new sap.suite.ui.microchart.BulletMicroChartData({
												//value: "{model_2>/Rowset/Row/QTY_OUT}",
												color: "Good"
											}).bindProperty("value", valueFunc("model_1>/Rowset/Row/QTY_OUT")),
											thresholds: [
												new sap.suite.ui.microchart.BulletMicroChartData({
													//value: "{model_2>/Rowset/Row/QTY_TARG}",
													color: "Error"
												}).bindProperty("value", valueFunc("model_1>/Rowset/Row/QTY_TARG"))
											]
										})
									]
								})/*,
								new sap.m.Text({
									width: "100%",
									text: "Andamento"
								}).addStyleClass('boldText')*/
							],
							layoutData : new sap.ui.layout.GridData({
								span: "L3 M3 S3"
							})
						}).addStyleClass('smallPanelPadding'),
						new sap.m.Text({
							width: "100%",
							text: "Andamento"
						}).addStyleClass('boldText'),
						new sap.m.Panel({
							height: "145px",
							content: [
								oTrendTable
							],
							layoutData : new sap.ui.layout.GridData({
								span: "L12 M12 S12",
								linebreak:true
							})
						}).addStyleClass('smallPanelPadding')
					]
				})
			],
			beginButton: new sap.m.Button({
				text: 'Chiudi',
				press: function () {
					oItemDialog.close();
				}
			}),
			beforeOpen: function () {
				sap.ui.core.BusyIndicator.hide();
			},
			afterClose: function () {
				oItemDialog.destroy();
			}
		});

		var oModel_1 = new sap.ui.model.xml.XMLModel();
		var sQuery_1 = QService + dataMonitor + "DashBoard/getDBItemQR";
		sQuery_1 += "&Param.1=" + sIdLine;
		oModel_1.loadData(sQuery_1,false,false);
		oItemDialog.setModel(oModel_1,'model_1');

		var oModel_2 = new sap.ui.model.xml.XMLModel();
		var sQuery_2 = QService + dataMonitor + "DashBoard/getDashBoardQR";
		sQuery_2 += "&Param.2=" + sIdLine;
		oModel_2.loadData(sQuery_2,false,false);
		oItemDialog.setModel(oModel_2,'model_2');

		var oModel_3 = new sap.ui.model.xml.XMLModel();
		var sQuery_3 = QService + dataMonitor + "DashBoard/getPlanQtyMonthDBItemQR";
		sQuery_3 += "&Param.1=" + sIdLine;
		oModel_3.loadData(sQuery_3,false,false);
		oItemDialog.setModel(oModel_3,'model_3');

		var oModel_4 = new sap.ui.model.xml.XMLModel();
		var sQuery_4 = QService + dataMonitor + "DashBoard/getTrendByLineForDayShiftsQR";
		sQuery_4 += "&Param.1=" + sIdLine;
		oModel_4.loadData(sQuery_4,false,false);
		oTrendTable.setModel(oModel_4,'model_4');

		oItemDialog.open();
		$("#splash-screen").hide();
	}
}
